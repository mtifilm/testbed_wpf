#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <iostream>
#include "IniFile.h"
#include "LineEngine.h"
#include "MTImalloc.h"
#include "font8x10.h"

/*
typedef struct {
        char row[10];
} letter;

static letter numeral[13] = {
                                { 0x00, 0x3e,0x41,0x43,0x45,0x49,0x51,0x61,0x41,0x3e }, // 0
                                { 0x00, 0x08,0x18,0x08,0x08,0x08,0x08,0x08,0x08,0x3e }, // 1
                                { 0x00, 0x3e,0x01,0x01,0x3e,0x40,0x40,0x40,0x40,0x3e }, // 2
                                { 0x00, 0x3e,0x01,0x01,0x3e,0x01,0x01,0x01,0x01,0x3e }, // 3
                                { 0x00, 0x21,0x21,0x21,0x3f,0x01,0x01,0x01,0x01,0x01 }, // 4
                                { 0x00, 0x7e,0x40,0x40,0x7e,0x01,0x01,0x01,0x01,0x7e }, // 5
                                { 0x00, 0x3e,0x40,0x40,0x7e,0x41,0x41,0x41,0x41,0x3e }, // 6
                                { 0x00, 0x3f,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01 }, // 7
                                { 0x00, 0x3e,0x41,0x41,0x3e,0x41,0x41,0x41,0x41,0x3e }, // 8
                                { 0x00, 0x3e,0x41,0x41,0x3f,0x01,0x01,0x01,0x01,0x01 }, // 9
                                { 0x00, 0x00,0x1c,0x1c,0x00,0x00,0x00,0x1c,0x1c,0x00 }, // :
                                { 0x00, 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1c,0x1c }, // .
                                { 0x00, 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 }, // space
                        };
*/

int CLineEngine::
getFieldSize() const
{
   return(fldSize);
}

// the standard Cohn-Sutherland tic-tac-toe codes
int CLineEngine::tCode(int x, int y)
{
    unsigned char tcode = 0;

   if (x < 0)         tcode |= TLFT;
   if (y < 0)         tcode |= TTOP;
   if (x > (dwide-1)) tcode |= TRGT;
   if (y > (dhigh-1)) tcode |= TBOT;

   return(tcode);
}

// sizes are 8x10, 16x20, 24x30, etc.
void CLineEngine::
setFontSize(int mult)
{
   rowMult    = mult;
   pelMult    = mult;
   fontWidth  = 8 * mult;
   fontHeight = 10 * mult;
}

int CLineEngine::
getFontWidth() const
{
   return(fontWidth);
}

int CLineEngine::
getFontHeight() const
{
   return(fontHeight);
}

// fill the field
void CLineEngine::
fillField()
{
   drawRectangle(0,0,(dwide-1),(dhigh-1));
}

void CLineEngine::
drawString(int x, int y, const char *str)
{
   while (*str) {

      int num = *str;

/*
      int num = -1;
      if ((*str>='0')&&(*str<='9')) {
         num = *str-'0';
       }
       else if (*str==':') num = 10;
       else if (*str=='.') num = 11;
       else if (*str==' ') num = 12;

       if (num>=0) {
          drawNumeral(x,y,num);
       }
*/

       drawNumeral(x,y,num);

       str++;
       x += fontWidth;
   }
}

void CLineEngine::
drawSpot(int x, int y)
{
   moveTo(x&0xfffffffe,y);
   lineTo((x&0xfffffffe)+2,y);
}

void CLineEngine::
hiliteString(int x, int y, const char *str)
{
   int  cnt = 0;
   while (*str++) cnt++;

   drawRectangle(x-pelMult,y-2*rowMult,x+cnt*fontWidth-1,y+fontHeight-rowMult-1);
}

void CLineEngine::
moveTo(int x, int y)
{
   VCurX = x; VCurY = y;
   if ((tcur = tCode(VCurX, VCurY))==0)
      movTo(VCurX, VCurY);
}

void CLineEngine::
lineTo(int x, int y)
{
   VPreX = VCurX; VPreY = VCurY;
   tpre = tcur;

   VCurX = x; VCurY = y;
   tcur = tCode(VCurX, VCurY);

   if ((tcur&tpre)==0) { // not trivially rejected

      if (VCurY==VPreY) { // horz

         if (VCurX>VPreX) { // rightward

            if (tpre&TLFT) { // crossing on lft edge
               movTo(0,VCurY);
            }
            if (tcur==0) { // VCur in middle
               linTo(VCurX,VCurY);
            }
            else { // crossing on rgt edge
               linTo(dwide-1,VCurY);
            }
         }
         else if (VCurX<VPreX) { // leftward

            if (tpre&TRGT) { // crossing on rgt edge
               movTo(dwide-1,VCurY);
            }
            if (tcur==0) { // VCur in middle
               linTo(VCurX,VCurY);
            }
            else { // crossing on lft edge
               linTo(0,VCurY);
            }
         }

      } // end horz

      else if (VCurX==VPreX) { // vert

         if (VCurY>VPreY) { // downward

            if (tpre&TTOP) { // crossing on top edge
               movTo(VCurX,0);
            }
            if (tcur==0) { // VCur in middle
               linTo(VCurX,VCurY);
            }
            else { // crossing on bot edge
               linTo(VCurX,dhigh-1);
            }
         }
         else if (VCurY<VPreY) { // upward

            if (tpre&TBOT) { // crossing on bot edge
               movTo(VCurX,dhigh-1);
            }
            if (tcur==0) { // VCur in middle
               linTo(VCurX,VCurY);
            }
            else { // crossing on top edge
               linTo(VCurX,0);
            }
         }

      } // end vert

      else { // oblique edge

         if ((tpre|tcur)!=0) { // there's a crossing

            delxi = (double)(VCurX - VPreX);
            delyi = (double)(VCurY - VPreY);
            lftxi = (double)( - VPreX);
            rgtxi = (double)((dwide-1) - VPreX);
            botyi = (double)((dhigh-1) - VPreY);
            topyi = (double)( - VPreY);
         }

         if (tpre!=0) { // pre is clipped

            if (tpre&TLFT) { // clip pre to lft edge
               YCrs = (int)(VPreY + delyi*lftxi/delxi);
               if (YCrs < 0) {
                  tpre |= TTOP;
                  if (tpre&tcur) goto doncur;
               }
               else if (YCrs > (dhigh-1)) {
                  tpre |= TBOT;
                  if (tpre&tcur) goto doncur;
               }
               else { // crossing on lft edge
                  movTo(0,YCrs);
                  goto donpre;
               }
            }

            if (tpre&TBOT) { // clip pre to bot edge
               XCrs = (int)(VPreX + delxi*botyi/delyi);
               if (XCrs < 0) {
                  tpre |= TLFT;
                  if (tpre&tcur) goto doncur;
               }
               else if (XCrs > (dwide-1)) {
                  tpre |= TRGT;
                  if (tpre&tcur) goto doncur;
               }
               else { // crossing on bot edge
                  movTo(XCrs,(dhigh-1));
                  goto donpre;
               }
            }

            if (tpre&TRGT) { // clip pre to rgt edge
               YCrs = (int)(VPreY + delyi*rgtxi/delxi);
               if (YCrs < 0) {
                  tpre |= TTOP;
                  if (tpre&tcur) goto doncur;
               }
               else if (YCrs > (dhigh-1)) {
                  tpre |= TBOT;
                  if (tpre&tcur) goto doncur;
               }
               else { // crossing on rgt edge
                  movTo(dwide-1,YCrs);
                  goto donpre;
               }
            }

            if (tpre&TTOP) { // clip pre to top edge
               XCrs = (int)(VPreX + delxi*topyi/delyi);
               if (XCrs < 0) {
                  tpre |= TLFT;
                  if (tpre&tcur) goto doncur;
               }
               else if (XCrs > (dwide-1)) {
                  tpre |= TRGT;
                  if (tpre&tcur) goto doncur;
               }
               else { // crossing on top edge
                  movTo(XCrs,0);
                  goto donpre;
               }
            }
donpre:;
         }

         if (tcur==0) { // cur is not clipped
            linTo(VCurX,VCurY);
         }
         else {

            if (tcur&TLFT) { // clip cur to lft edge
               YCrs = (int)(VPreY + delyi*lftxi/delxi);
               if ((YCrs>=0)&&(YCrs<=(dhigh-1))) {
                  linTo(0,YCrs);
                  goto doncur;
               }
            }

            if (tcur&TBOT) { // clip cur to bot edge
               XCrs = (int)(VPreX + delxi*botyi/delyi);
               if ((XCrs>=0)&&(XCrs<=(dwide-1))) {
                  linTo(XCrs,(dhigh-1));
                  goto doncur;
               }
            }

            if (tcur&TRGT) { // clip cur to rgt edge
               YCrs = (int)(VPreY + delyi*rgtxi/delxi);
               if ((YCrs>=0)&&(YCrs<=(dhigh-1))) {
                  linTo((dwide-1),YCrs);
                  goto doncur;
               }
            }

            if (tcur&TTOP) { // clip cur to top edge
               XCrs = (int)(VPreX + delxi*topyi/delyi);
               if ((XCrs>=0)&&(XCrs<=(dwide-1))) {
                  linTo(XCrs,0);
                  goto doncur;
               }
            }
doncur:;
         }

      } // end oblique

   } // end not trivially rejected
}

void CLineEngine::
splineTo(int x1, int y1, // nxt ctrl point, current  vertex
         int x2, int y2, // pre ctrl point, terminal vertex
         int x3, int y3) // terminal vertex
{
   int x0 = VCurX;
   int y0 = VCurY;

   // coefficients for X
   double u0 = x0;
   double u1 = 3*(x1 - x0);
   double u2 = 3*(x2 - 2*x1 + x0);
   double u3 = (x3 - 3*x2 + 3*x1 - x0);

   // coefficients for Y
   double v0 = y0;
   double v1 = 3*(y1 - y0);
   double v2 = 3*(y2 - 2*y1 + y0);
   double v3 = (y3 - 3*y2 + 3*y1 - y0);

   // "length" of spline
   int delx, dely;

   int length = 0;
   if ((delx = x1 - x0)<0) delx = -delx;
   length += delx;
   if ((dely = y1 - y0)<0) dely = -dely;
   length += dely;
   if ((delx = x2 - x1)<0) delx = -delx;
   length += delx;
   if ((dely = y2 - y1)<0) dely = -dely;
   length += dely;
   if ((delx = x3 - x2)<0) delx = -delx;
   length += delx;
   if ((dely = y3 - y2)<0) dely = -dely;
   length += dely;

   // number of segments
   int nseg = length / 4;

   // must be at least one
   if (nseg == 0) nseg = 1;

   // value of delt
   double delt = 1.0 / nseg;

   // now draw the spline, beginning at (curx, cury)
   for (double t=delt;t<1.0;t+=delt)
      lineTo((int) ((((u3*t+u2)*t+u1)*t+u0)+.5),
             (int) ((((v3*t+v2)*t+v1)*t+v0)+.5));

   // get the last chord
   lineTo(x3, y3);
}

static void TransformRGBtoYUV(MTI_UINT16 r,MTI_UINT16 g,MTI_UINT16 b,
                         MTI_UINT16 *y,MTI_UINT16 *u,MTI_UINT16 *v)
{
   double fy = (double)r*.25678906 +
               (double)g*.50412891 +
               (double)b*.09790625 +  16.0 * 256.;
   double fu = (double)r*-.14822266 +
               (double)g*-.29099219 +
               (double)b*.43921484 + 128.0 * 256.;
   double fv = (double)r*.43921484 +
               (double)g*-.36778906 +
               (double)b*-.07142578 + 128.0 * 256.;

   *y = (MTI_UINT16)fy;
   *u = (MTI_UINT16)fu;
   *v = (MTI_UINT16)fv;
}

CLineEngine *CLineEngineFactory::makePixelEng(CImageFormat const &imageFormat)
{
   CLineEngine* pixelEng = 0;

   if(imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_YUV422)
      {
      switch(imageFormat.getPixelPacking())
         {
         case IF_PIXEL_PACKING_8Bits_IN_1Byte :
            pixelEng = new CLineEngYUV8;
            break;

         case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes :   // Typical 10bit YUV422
            pixelEng = new CLineEngYUV10;
            break;

         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS :  // DVS 10-Bit YUV422
            pixelEng = new CLineEngDVS10;
            break;

         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L :  // 10-Bit YUV422 Clipster
            pixelEng = new CLineEngDVS10BE;
            break;

         case IF_PIXEL_PACKING_INVALID :

         default:
            pixelEng = 0;
            break;
         }
      }
   else if (imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_YUV444)
      {
      switch(imageFormat.getPixelPacking())
         {

         case IF_PIXEL_PACKING_8Bits_IN_1Byte: // 8-Bit YUV 444
            pixelEng = new CLineEngUYV8;
            break;

         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R: // 10-Bit YUV 444
            pixelEng = new CLineEngUYV10;
            break;

         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE : // 16-Bit YUV 444
            pixelEng = new CLineEngYUV16;
            break;

         case IF_PIXEL_PACKING_INVALID :
         default:
            pixelEng = 0;
            break;
         }
      }
   else if (imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_RGB)
      {
      switch(imageFormat.getPixelPacking())
         {
         case IF_PIXEL_PACKING_8Bits_IN_1Byte :          // 8-Bit RGB
            pixelEng = new CLineEngRGB8;
            break;

         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L :    // 10-Bit RGB
            pixelEng = new CLineEngRGB10;
            break;

         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE :  // 16-Bit RGB
            pixelEng = new CLineEngRGB16BE;             // Big-Endian
            break;

         case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L :   // 10-Bit RGB in 16 bits
         case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R :   // 10-Bit RGB in 16 bits
         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE :  // 16-Bit RGB
         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT : // 16-Bit RGB
         case IF_PIXEL_PACKING_1_Half_IN_2Bytes:        // 16-bit float
            pixelEng = new CLineEngRGB16LE;             // Little-Endian
            break;

         case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes :    // 12-Bit padded
            pixelEng = new CLineEngRGB12;
            break;

         case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR : // reverses spec
            pixelEng = new CLineEngRGB12LR;
            break;

         case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE :  // 12-Bit Left, Little-Endian
            pixelEng = new CLineEngRGB12LLE;
            break;

         case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE :  // 12-Bit Left, Big-Endian
            pixelEng = new CLineEngRGB12LBE;
            break;

         case IF_PIXEL_PACKING_INVALID :
         default:
            pixelEng = 0;
            break;
         }
      }
   else if(imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_BGR)
      {
      switch(imageFormat.getPixelPacking())
         {
         case IF_PIXEL_PACKING_8Bits_IN_1Byte :          // 8-Bit BGR
            pixelEng = new CLineEngBGR8;
            break;

         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L :    // 10-Bit BGR
            pixelEng = new CLineEngRGB10;                // QQQ MAKE BGR VERSION
            break;

         // QQQ MAKE 10 BIT VERSIONS
         case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L :   // 10-Bit RGB in 16 bits
         case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R :   // 10-Bit RGB in 16 bits
         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE :  // 16-Bit RGB Little-Endian
         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT : // 16-Bit RGB
         case IF_PIXEL_PACKING_1_Half_IN_2Bytes:        // 16-bit float
				pixelEng = new CLineEngRGB16LE;             // QQQ MAKE BGR VERSION
            break;

         case IF_PIXEL_PACKING_INVALID :
         default:
            pixelEng = 0;
            break;
         }
      }
   else if(imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA
        || imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_BGRA)
      {
	  switch(imageFormat.getPixelPacking())
         {
         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L :    // 10-Bit RGB
            pixelEng = new CLineEngRGBA10;
	    		break;

         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE :  // 16-Bit RGB
            pixelEng = new CLineEngRGBA16BE;             // Big-Endian
            break;

         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE :  // 16-Bit RGB
            pixelEng = new CLineEngRGBA16LE;             // Little-Endian
            break;

         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT :  // 16-Bit RGB
#if (ENDIAN == MTI_BIG_ENDIAN)
            pixelEng = new CLineEngRGBA16BE;             // Big-Endian
#else
            pixelEng = new CLineEngRGBA16LE;             // Little-Endian
#endif
            break;

         case IF_PIXEL_PACKING_1_Half_IN_2Bytes:      // 16-bit float
            pixelEng = new CLineEngRGBAHalf;          // Little-Endian
            break;

		 default:
		 	break;
         }
      }
   else if(/* imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_LUMINANCE
        || */ imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_Y
        || imageFormat.getPixelComponents() == IF_PIXEL_COMPONENTS_YYY)
      {
      switch(imageFormat.getPixelPacking())
         {
         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L :    // 10-Bit MONO
         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R :    // 10-bit MONO 3 10's
            pixelEng = new CLineEngMON10;
            break;

         case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:      // 10-bit MONO 4 10's
            pixelEng = new CLineEngMON10P;
            break;

         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE :  // 16-Bit RGB
            pixelEng = new CLineEngMON16BE;             // Big-Endian
            break;

         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE :  // 16-Bit RGB
            pixelEng = new CLineEngMON16LE;             // Little-Endian
            break;

         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT :  // 16-Bit RGB
#if (ENDIAN == MTI_BIG_ENDIAN)
            pixelEng = new CLineEngMON16BE;             // Big-Endian
#else
            pixelEng = new CLineEngMON16LE;             // Little-Endian
#endif
            break;

         case IF_PIXEL_PACKING_INVALID :
         default:
            pixelEng = 0;
            break;
         }
      }


   if (pixelEng == 0)
      {
      TRACE_0(errout << "Error: Cannot create CLineEngine for this image type "
                   << CImageInfo::queryImageFormatTypeName(
                       imageFormat.getImageFormatType())
                   << " (" << imageFormat.getImageFormatType() << ")");
      return 0;  // Return NULL pointer on error
      }

   // Initialize the new CLineEngine
   pixelEng->setFrameBufferDimensions(imageFormat.getPixelsPerLine(),
                                      imageFormat.getLinesPerFrame(),
                                      imageFormat.getFramePitch(),
                                      imageFormat.getInterlaced());

   return(pixelEng);
}

void CLineEngineFactory::destroyPixelEng(CLineEngine *pixelEng)
{
    delete pixelEng;
}



//////////////////////////////////////////////////////////
//
//		YUV 8-bit
//
//////////////////////////////////////////////////////////

CLineEngYUV8::
CLineEngYUV8()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL; 
}

CLineEngYUV8::
~CLineEngYUV8()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngYUV8::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                        )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch;
   intrlc  = il;
   fldSize = dpitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned char[dpitch];

   // basic font dimensions

   setFontSize(1);
}

int CLineEngYUV8::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngYUV8::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngYUV8::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x80;
         fgColor[1] = 0x19;
         fgColor[2] = 0x80;
         fgColor[3] = 0x19;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0x80;
         fgColor[1] = 0xc8;
         fgColor[2] = 0x80;
         fgColor[3] = 0xc8;
         fgndColor = col;
      break;

      default: 
      break;
   }
}

void CLineEngYUV8::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   fgColor[0] = u>>8;
   fgColor[1] = y>>8;
   fgColor[2] = v>>8;
   fgColor[3] = y>>8;
}

void CLineEngYUV8::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x80;
         bgColor[1] = 0x19;
         bgColor[2] = 0x80;
         bgColor[3] = 0x19;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0x80;
         bgColor[1] = 0xc8;
         bgColor[2] = 0x80;
         bgColor[3] = 0xc8;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngYUV8::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   bgColor[0] = u>>8;
   bgColor[1] = y>>8;
   bgColor[2] = v>>8;
   bgColor[3] = y>>8;
}

void CLineEngYUV8::
movTo(
        int x,
        int y
      )
{
   curx = x;
   uvphase = (x&1);
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + (x<<1);
}

void CLineEngYUV8::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {

            if (uvphase==0) {

               *curptr     = fgColor[0]; // U
               *(curptr+1) = fgColor[1]; // Y
               *(curptr+2) = fgColor[2]; // V

            }
            else {

               *(curptr-2) = fgColor[0]; // U
               *curptr     = fgColor[2]; // V
               *(curptr+1) = fgColor[3]; // Y

            }

            curptr += 2;
            curx++;
            uvphase ^= 1;
         }
      }
      else { // lft
         while (delx++!=0) {

            if (uvphase==0) {

               *curptr     = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];

            }
            else {

               *(curptr-2) = fgColor[0];
               *curptr     = fgColor[2];
               *(curptr+1) = fgColor[3];

            }

            curptr -= 2;
            curx--;
            uvphase ^= 1;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            if (uvphase==0) {

               while (dely--!=0) {

                  *curptr     = fgColor[0];
                  *(curptr+1) = fgColor[1];
                  *(curptr+2) = fgColor[2];

                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;

               }

            }
            else {

               while (dely--!=0) {

                  *(curptr-2) = fgColor[0];
                  *curptr     = fgColor[2];
                  *(curptr+1) = fgColor[3];

                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4 + 2;

               }

            }

         }
         else { // up

            if (uvphase==0) {

               while (dely++!=0) {

                  *curptr     = fgColor[0];
                  *(curptr+1) = fgColor[1];
                  *(curptr+2) = fgColor[2];

                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;

               }

            }
            else {

               while (dely++!=0) {

                  *(curptr-2) = fgColor[0];
                  *curptr     = fgColor[2];
                  *(curptr+1) = fgColor[3];

                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4 + 2;

               }

            }

         }

      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *curptr     = fgColor[2];
                        *(curptr+1) = fgColor[3];

                     }

                     curptr += 2;
                     curx++;
                     uvphase ^= 1;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;
                        if (uvphase==1) curptr += 2;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *curptr     = fgColor[2];
                        *(curptr+1) = fgColor[3];

                     }

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;
                     if (uvphase==1) curptr += 2;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr += 2;
                        curx++;
                        uvphase ^= 1;
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *curptr     = fgColor[2];
                        *(curptr+1) = fgColor[3];

                     }

                     curptr -= 2;
                     curx--;
                     uvphase ^= 1;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;
                        if (uvphase==1) curptr += 2;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *curptr     = fgColor[2];
                        *(curptr+1) = fgColor[3];

                     }

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;
                     if (uvphase==1) curptr += 2;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr -= 2;
                        curx--;
                        uvphase ^= 1;
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *curptr     = fgColor[2];
                        *(curptr+1) = fgColor[3];

                     }

                     curptr += 2;
                     curx++;
                     uvphase ^= 1;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;
                        if (uvphase==1) curptr += 2;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *curptr     = fgColor[2];
                        *(curptr+1) = fgColor[3];

                     }

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;
                     if (uvphase==1) curptr += 2;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr += 2;
                        curx++;
                        uvphase ^= 1;
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *curptr     = fgColor[2];
                        *(curptr+1) = fgColor[3];

                     }

                     curptr -= 2;
                     curx--;
                     uvphase ^= 1;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;
                        if (uvphase==1) curptr += 2;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *curptr     = fgColor[2];
                        *(curptr+1) = fgColor[3];

                     }

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*4;
                     if (uvphase==1) curptr += 2;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr -= 2;
                        curx--;
                        uvphase ^= 1;
                     }
                  }
               }
            }
         }
      }
   }
}

void CLineEngYUV8::
drawDot()
{
   if (uvphase==0) {
     *curptr     = fgColor[0]; // U
     *(curptr+1) = fgColor[1]; // Y
   }
   else {
     *curptr     = fgColor[2]; // V
     *(curptr+1) = fgColor[1]; // Y
   }
}

void CLineEngYUV8::
drawSpot(int x, int y)
{
   movTo(x&0xfffffffe,y);
   curptr[0] = fgColor[0];
   curptr[1] = fgColor[1];
   curptr[2] = fgColor[2];
   curptr[3] = fgColor[3];
}

void CLineEngYUV8::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int p = (rgt - lft + 1);

   int uvphase = (lft&1)<<1;
   unsigned char *dst = linebuf;

   for (int i=0;i<p;i++)
   {
      *dst++ = fgColor[0+uvphase];
      *dst++ = fgColor[1+uvphase];
      uvphase ^= 2;
   }

   p *= 2;
   for (int i=top;i<=bot;i++)
   {
      unsigned char *rowbeg = frameBuffer[rowTbl[i].rowfld] + rowTbl[i].rowoff + (lft<<1);
      MTImemcpy(rowbeg,linebuf,p);
   }
}

void CLineEngYUV8::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + (x<<1);
         uvphase = x&1;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  if (uvphase==0) {

                     *curptr     = fgColor[0]; // U
                     *(curptr+1) = fgColor[1]; // Y
                     *(curptr+2) = fgColor[2]; // V

                  }
                  else {

                     *(curptr-2) = fgColor[0]; // U
                     *curptr     = fgColor[2]; // V
                     *(curptr+1) = fgColor[3]; // Y

                  }

                  curptr += 2;
                  uvphase ^= 1;

               }

            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     if (uvphase==0) {

                        *curptr     = bgColor[0]; // U
                        *(curptr+1) = bgColor[1]; // Y
                        *(curptr+2) = bgColor[2]; // V

                     }
                     else {

                        *(curptr-2) = bgColor[0]; // U
                        *curptr     = bgColor[2]; // V
                        *(curptr+1) = bgColor[3]; // Y

                     }
                  }

                  curptr += 2;
                  uvphase ^= 1;

               }

            }

            fontscan = fontscan >> 1;

         }
      }
   }
}


//////////////////////////////////////////////////////////
//
//		YUV 10-bit
//
//////////////////////////////////////////////////////////

CLineEngYUV10::
CLineEngYUV10()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngYUV10::
~CLineEngYUV10()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngYUV10::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                         )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch;
   intrlc  = il;
   fldSize = dpitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned char[dpitch];

   // basic font dimensions

   setFontSize(1);

}

int CLineEngYUV10::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngYUV10::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngYUV10::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x80;
         fgColor[1] = 0x06;
         fgColor[2] = 0x48;
         fgColor[3] = 0x00;
         fgColor[4] = 0x64;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0x80;
         fgColor[1] = 0x32;
         fgColor[2] = 0x08;
         fgColor[3] = 0x03;
         fgColor[4] = 0x20;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngYUV10::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   fgColor[0] = (u>>8);
   fgColor[1] = (u&0xc0)+(y>>10);
   fgColor[2] = ((y&0x3c0)>>2)+(v>>12);
   fgColor[3] = ((v&0xfc0)>>4)+(y>>14);
   fgColor[4] = (y>>6);
}


void CLineEngYUV10::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x80;
         bgColor[1] = 0x06;
         bgColor[2] = 0x48;
         bgColor[3] = 0x00;
         bgColor[4] = 0x64;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0x80;
         bgColor[1] = 0x32;
         bgColor[2] = 0x08;
         bgColor[3] = 0x03;
         bgColor[4] = 0x20;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngYUV10::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   bgColor[0] = (u>>8);
   bgColor[1] = (u&0xc0)+(y>>10);
   bgColor[2] = ((y&0x3c0)>>2)+(v>>12);
   bgColor[3] = ((v&0xfc0)>>4)+(y>>14);
   bgColor[4] = (y>>6);
}


void CLineEngYUV10::
movTo(
        int x,
        int y
      )
{
   curx = x;
   uvphase = (x&1);
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + (x>>1)*5;
   if (uvphase==1) curptr +=2;
}

void CLineEngYUV10::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {

            if (uvphase==0) {

               *curptr     = fgColor[0]; // U
               *(curptr+1) = fgColor[1]; // Y
               *(curptr+2) = fgColor[2]; // Y
               *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc); // V

            }
            else {

               *(curptr-2) = fgColor[0]; // U
               *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0); // U
               *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f); //V
               *(curptr+1) = fgColor[3]; // Y
               *(curptr+2) = fgColor[4]; // Y       

            }

            curptr += 2 + uvphase;
            curx++;
            uvphase ^= 1;
         }
      }
      else { // lft
         while (delx++!=0) {

            if (uvphase==0) {

               *curptr     = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];
               *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

            }
            else {

               *(curptr-2) = fgColor[0];
               *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
               *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
               *(curptr+1) = fgColor[3];
               *(curptr+2) = fgColor[4];

            }

            curptr -= 3 - uvphase;
            curx--;
            uvphase ^= 1;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            if (uvphase==0) {

               while (dely--!=0) {

                  *curptr     = fgColor[0];
                  *(curptr+1) = fgColor[1];
                  *(curptr+2) = fgColor[2];
                  *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;

               }

            }
            else {

               while (dely--!=0) { 

                  *(curptr-2) = fgColor[0];
                  *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                  *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                  *(curptr+1) = fgColor[3];
                  *(curptr+2) = fgColor[4];

                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5 + 2;

               }

            }

         }
         else { // up

            if (uvphase==0) {

               while (dely++!=0) {

                 *curptr     = fgColor[0];
                 *(curptr+1) = fgColor[1];
                 *(curptr+2) = fgColor[2];
                 *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;

               }

            }
            else {

               while (dely++!=0) {

                  *(curptr-2) = fgColor[0];
                  *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                  *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                  *(curptr+1) = fgColor[3];
                  *(curptr+2) = fgColor[4];

                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5 + 2;

               }

            }

         }

      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];
                        *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                        *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                        *(curptr+1) = fgColor[3];
                        *(curptr+2) = fgColor[4];

                     }

                     curptr += 2 + uvphase;
                     curx++;
                     uvphase ^= 1;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;
                        if (uvphase==1) curptr += 2;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];
                        *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                        *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                        *(curptr+1) = fgColor[3];
                        *(curptr+2) = fgColor[4];

                     }

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;
                     if (uvphase==1) curptr += 2;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr += 2 + uvphase;
                        curx++;
                        uvphase ^= 1;
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];
                        *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                        *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                        *(curptr+1) = fgColor[3];
                        *(curptr+2) = fgColor[4];

                     }

                     curptr -= 3 - uvphase;
                     curx--;
                     uvphase ^= 1;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;
                        if (uvphase==1) curptr += 2;
                     }
                  }


               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];
                        *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                        *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                        *(curptr+1) = fgColor[3];
                        *(curptr+2) = fgColor[4];

                     }

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;
                     if (uvphase==1) curptr += 2;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr -= 3 - uvphase;
                        curx--;
                        uvphase ^= 1;
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];
                        *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                        *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                        *(curptr+1) = fgColor[3];
                        *(curptr+2) = fgColor[4];

                     }

                     curptr += 2 + uvphase;
                     curx++;
                     uvphase ^= 1;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;
                        if (uvphase==1) curptr += 2;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];
                        *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                        *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                        *(curptr+1) = fgColor[3];
                        *(curptr+2) = fgColor[4];

                     }

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;
                     if (uvphase==1) curptr += 2;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr += 2 + uvphase;
                        curx++;
                        uvphase ^= 1;
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];
                        *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                        *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                        *(curptr+1) = fgColor[3];
                        *(curptr+2) = fgColor[4];

                     }

                     curptr -= 3 - uvphase;
                     curx--;
                     uvphase ^= 1;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;
                        if (uvphase==1) curptr += 2;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     if (uvphase==0) {

                        *curptr     = fgColor[0];
                        *(curptr+1) = fgColor[1];
                        *(curptr+2) = fgColor[2];
                        *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc);

                     }
                     else {

                        *(curptr-2) = fgColor[0];
                        *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0);
                        *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f);
                        *(curptr+1) = fgColor[3];
                        *(curptr+2) = fgColor[4];

                     }

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + (curx>>1)*5;
                     if (uvphase==1) curptr += 2;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr -= 3 - uvphase;
                        curx--;
                        uvphase ^= 1;
                     }
                  }
               }
            }
         }
      }
   }
}

void CLineEngYUV10::
drawDot()
{
   if (uvphase==0) { // U

      *(curptr)   = fgColor[0];
      *(curptr+1) = fgColor[1];
      *(curptr+2) = (*(curptr+2)&0x0f)+(fgColor[2]&0xf0);

   }
   else { // V

      *(curptr)   = (*(curptr)&0xf0)+(fgColor[2]&0x0f);
      *(curptr+1) = fgColor[3];
      *(curptr+2) = fgColor[4];

   }

}

void CLineEngYUV10::
drawSpot(int x, int y)
{
   movTo(x&0xfffffffe,y);
   curptr[0] = fgColor[0];
   curptr[1] = fgColor[1];
   curptr[2] = fgColor[2];
   curptr[3] = fgColor[3];
   curptr[4] = fgColor[4];
}

void CLineEngYUV10::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int loff = (lft >> 1);
   int roff = (rgt >> 1);

   int prs  = roff - loff - 1;

   if ((lft&1)==0) {
      prs++;
   }
   if ((rgt&1)==1) {
      prs++;
   }

   unsigned char *dst = linebuf;

   for (int i=0;i<prs;i++) {
      *dst++ = fgColor[0];
      *dst++ = fgColor[1];
      *dst++ = fgColor[2];
      *dst++ = fgColor[3];
      *dst++ = fgColor[4];
   }   

   prs *= 5;
   for (int i=top;i<=bot;i++)
   {
      unsigned char *rowbeg = frameBuffer[rowTbl[i].rowfld] + rowTbl[i].rowoff + (loff*5);
      if ((lft&1)==1) {
           rowbeg += 2;
         *rowbeg   = (*rowbeg&0xf0) + (fgColor[2]&0x0f);
           rowbeg++;
         *rowbeg++ = fgColor[3];
         *rowbeg++ = fgColor[4];
      }
      MTImemcpy(rowbeg,linebuf,prs);
      rowbeg += prs;
      if ((rgt&1)==0) {

         *rowbeg++ = fgColor[0];
         *rowbeg++ = fgColor[1];
         *rowbeg   = (*rowbeg&0x0f) + (fgColor[2]&0xf0);

      }
   }
}

void CLineEngYUV10::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';
   
   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         uvphase = x&1;
         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + (x>>1)*5;
         if (uvphase==1) curptr +=2;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  if (uvphase==0) {

                     *curptr     = fgColor[0]; // U
                     *(curptr+1) = fgColor[1]; //
                     *(curptr+2) = fgColor[2]; //
                     *(curptr+3) = (*(curptr+3)&0x03) + (fgColor[3]&0xfc); // V

                  }
                  else {

                     *(curptr-2) = fgColor[0]; // U
                     *(curptr-1) = (*(curptr-1)&0x3f) + (fgColor[1]&0xc0); // U
                     *curptr     = (*curptr&0xf0) + (fgColor[2]&0x0f); //V
                     *(curptr+1) = fgColor[3]; // Y
                     *(curptr+2) = fgColor[4]; // Y

                  }

                  curptr += 2 + uvphase;
                  uvphase ^= 1;

               }

            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     if (uvphase==0) {

                        *curptr     = bgColor[0]; // U
                        *(curptr+1) = bgColor[1]; // Y
                        *(curptr+2) = bgColor[2]; // Y
                        *(curptr+3) = (*(curptr+3)&0x03) + (bgColor[3]&0xfc); // V

                     }
                     else {

                        *(curptr-2) = bgColor[0]; // U
                        *(curptr-1) = (*(curptr-1)&0x3f) + (bgColor[1]&0xc0); // U
                        *curptr     = (*curptr&0xf0) + (bgColor[2]&0x0f); //V
                        *(curptr+1) = bgColor[3]; // Y
                        *(curptr+2) = bgColor[4]; // Y

                     }
                  }

                  curptr += 2 + uvphase;
                  uvphase ^= 1;

               }

            }

            fontscan = fontscan >> 1;

         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//		DVS 10-bit
//
//////////////////////////////////////////////////////////

#define TOP 0x3ff00000
#define MID 0x000ffc00
#define LOW 0x000003ff

#define U 0
#define V 1
#define Y 2

CLineEngDVS10::
CLineEngDVS10()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngDVS10::
~CLineEngDVS10()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngDVS10::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                         )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch;
   intrlc  = il;
   fldSize = dpitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned char[dpitch];

   // basic font dimensions

   setFontSize(1);

}

int CLineEngDVS10::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngDVS10::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngDVS10::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[U] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         fgColor[V] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         fgColor[Y] = (0x19<<22)+(0x19<<12)+(0x19<<2);
         fgndColor = col;
      break;

      case 1:
         fgColor[U] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         fgColor[V] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         fgColor[Y] = (0xc8<<22)+(0xc8<<12)+(0xc8<<2);
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngDVS10::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   int iu = u>>6;
   fgColor[U] = (iu<<20)+(iu<<10)+(iu);
   int iv = v>>6;
   fgColor[V] = (iv<<20)+(iv<<10)+(iv);
   int iy = y>>6;
   fgColor[Y] = (iy<<20)+(iy<<10)+(iy);
}

void CLineEngDVS10::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[U] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         bgColor[V] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         bgColor[Y] = (0x19<<22)+(0x19<<12)+(0x19<<2);
         bgndColor = col;
      break;

      case 1:
         bgColor[U] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         bgColor[V] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         bgColor[Y] = (0xc8<<22)+(0xc8<<12)+(0xc8<<2);
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngDVS10::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   int iu = u>>6;
   bgColor[U] = (iu<<20)+(iu<<10)+(iu);
   int iv = v>>6;
   bgColor[V] = (iv<<20)+(iv<<10)+(iv);
   int iy = y>>6;
   bgColor[Y] = (iy<<20)+(iy<<10)+(iy<<0);
}

// sets Y and EITHER U or V
void CLineEngDVS10::
setFGY0()
{
    int unpack;
   unpack = (curptr[3]<<24)+(curptr[2]<<16);
   unpack = (unpack&TOP)+(fgColor[Y]&MID)+(fgColor[U]&LOW);
   curptr[0] = (unpack    );
   curptr[1] = (unpack>> 8);
   curptr[2] = (unpack>>16);
   curptr[3] = (unpack>>24);
   return;
}

void CLineEngDVS10::
setFGY1()
{
    int unpack;
   unpack = (curptr[2]<<16);
   unpack = (fgColor[V]&TOP)+(unpack&(MID+LOW));
   curptr[2] = (unpack>>16);
   curptr[3] = (unpack>>24);
   unpack = (curptr[5]<<8);
   unpack = (unpack&(TOP+MID))+(fgColor[Y]&LOW);
   curptr[4] = (unpack    );
   curptr[5] = (unpack>> 8);
   return;
}

void CLineEngDVS10::
setFGY2()
{
    int unpack;
   unpack = (curptr[5]<<8);
   unpack = (fgColor[Y]&TOP)+(fgColor[U]&MID)+(unpack&LOW);
   curptr[5] = (unpack>> 8);
   curptr[6] = (unpack>>16);
   curptr[7] = (unpack>>24);
   return;
}

void CLineEngDVS10::
setFGY3()
{
    int unpack;
   unpack = (curptr[10]<<16);
   unpack = (unpack&TOP)+(fgColor[Y]&MID)+(fgColor[V]&LOW);
   curptr[8]  = (unpack    );
   curptr[9]  = (unpack>> 8);
   curptr[10] = (unpack>>16);
   return;
}

void CLineEngDVS10::
setFGY4()
{
    int unpack;
   unpack = (curptr[10]<<16);
   unpack = (fgColor[U]&TOP)+(unpack&(MID+LOW));
   curptr[10] = (unpack>>16);
   curptr[11] = (unpack>>24);
   unpack = (curptr[13]<<8);
   unpack = (unpack&(TOP+MID))+(fgColor[Y]&LOW);
   curptr[12] = (unpack    );
   curptr[13] = (unpack>> 8);
   return;
}

void CLineEngDVS10::
setFGY5()
{
    int unpack;
   unpack = (curptr[13]<< 8);
   unpack = (fgColor[Y]&TOP)+(fgColor[V]&MID)+(unpack&LOW);
   curptr[13] = (unpack>> 8);
   curptr[14] = (unpack>>16);
   curptr[15] = (unpack>>24);
   return;
}

// sets Y and EITHER U or V
void CLineEngDVS10::
setBGY0()
{
    int unpack;
   unpack = (curptr[3]<<24)+(curptr[2]<<16);
   unpack = (unpack&TOP)+(bgColor[Y]&MID)+(bgColor[U]&LOW);
   curptr[0] = (unpack    );
   curptr[1] = (unpack>> 8);
   curptr[2] = (unpack>>16);
   curptr[3] = (unpack>>24);
   return;
}

void CLineEngDVS10::
setBGY1()
{
    int unpack;
   unpack = (curptr[2]<<16);
   unpack = (bgColor[V]&TOP)+(unpack&(MID+LOW));
   curptr[2] = (unpack>>16);
   curptr[3] = (unpack>>24);
   unpack = (curptr[5]<<8);
   unpack = (unpack&(TOP+MID))+(bgColor[Y]&LOW);
   curptr[4] = (unpack    );
   curptr[5] = (unpack>> 8);
   return;
}

void CLineEngDVS10::
setBGY2()
{
    int unpack;
   unpack = (curptr[5]<<8);
   unpack = (bgColor[Y]&TOP)+(bgColor[U]&MID)+(unpack&LOW);
   curptr[5] = (unpack>> 8);
   curptr[6] = (unpack>>16);
   curptr[7] = (unpack>>24);
   return;
}

void CLineEngDVS10::
setBGY3()
{
    int unpack;
   unpack = (curptr[10]<<16);
   unpack = (unpack&TOP)+(bgColor[Y]&MID)+(bgColor[V]&LOW);
   curptr[8]  = (unpack    );
   curptr[9]  = (unpack>> 8);
   curptr[10] = (unpack>>16);
   return;
}

void CLineEngDVS10::
setBGY4()
{
    int unpack;
   unpack = (curptr[10]<<16);
   unpack = (bgColor[U]&TOP)+(unpack&(MID+LOW));
   curptr[10] = (unpack>>16);
   curptr[11] = (unpack>>24);
   unpack = (curptr[13]<<8);
   unpack = (unpack&(TOP+MID))+(bgColor[Y]&LOW);
   curptr[12] = (unpack    );
   curptr[13] = (unpack>> 8);
   return;
}

void CLineEngDVS10::
setBGY5()
{
    int unpack;
   unpack = (curptr[13]<< 8);
   unpack = (bgColor[Y]&TOP)+(bgColor[V]&MID)+(unpack&LOW);
   curptr[13] = (unpack>> 8);
   curptr[14] = (unpack>>16);
   curptr[15] = (unpack>>24);
   return;
}

void CLineEngDVS10::
movTo(
        int x,
        int y
      )
{
   curx = x;
   uvphase = (x%6);
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + (x/6)*16;
}

void CLineEngDVS10::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt

         if (uvphase==0) goto hr0;
         if (uvphase==1) goto hr1;
         if (uvphase==2) goto hr2;
         if (uvphase==3) goto hr3;
         if (uvphase==4) goto hr4;
         goto hr5;

         while (true) {

	    hr0:;

               if (delx--==0) break;
               setFGY0();

	    hr1:;

               if (delx--==0) break;
               setFGY1();

	    hr2:;

               if (delx--==0) break;
               setFGY2();

	    hr3:;

               if (delx--==0) break;
               setFGY3();

	    hr4:

               if (delx--==0) break;
               setFGY4();

	    hr5:

               if (delx--==0) break;
               setFGY5();
               curptr += 16;  
         }
      }
      else  { // lft

         if (uvphase==5) goto hl5;
         if (uvphase==4) goto hl4;
         if (uvphase==3) goto hl3;
         if (uvphase==2) goto hl2;
         if (uvphase==1) goto hl1;
         goto hl0;

         while (true) {

	    hl5:;

               if (delx++==0) break;
               setFGY5();

	    hl4:;

               if (delx++==0) break;
               setFGY4();

	    hl3:;

               if (delx++==0) break;
               setFGY3();

	    hl2:;

               if (delx++==0) break;
               setFGY2();

	    hl1:

               if (delx++==0) break;
               setFGY1();

	    hl0:

               if (delx++==0) break;
               setFGY0();
               curptr -= 16;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

	    if (uvphase==0) {
	       while (dely--) {
                  setFGY0();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
	    else if (uvphase==1) {
	       while (dely--) {
                  setFGY1();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==2) {
	       while (dely--) {
                  setFGY2();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==3) {
	       while (dely--) {
                  setFGY3();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==4) {
	       while (dely--) {
                  setFGY4();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==5) {
	       while (dely--) {
                  setFGY5();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
         }
         else { // up

	    if (uvphase==0) {
	       while (dely++) {
                  setFGY0();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
	    else if (uvphase==1) {
	       while (dely++) {
                  setFGY1();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==2) {
	       while (dely++) {
                  setFGY2();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==3) {
	       while (dely++) {
                  setFGY3();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==4) {
               while (dely++) {
                  setFGY4();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==5) {
               while (dely++) {
                  setFGY5();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow

                  pen = delx;

                  if (uvphase==0) goto drsh0;
                  if (uvphase==1) goto drsh1;
                  if (uvphase==2) goto drsh2;
                  if (uvphase==3) goto drsh3;
                  if (uvphase==4) goto drsh4;
                  goto drsh5;

                  while (true) {

                     drsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx += 6;
                        curptr += 16;

                  }
               }
               else { // dn-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx++;
                        uvphase++;
                        if (uvphase==6) {
                           uvphase = 0;
                           curptr += 16;
                        }
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow

                  pen = delx;

                  if (uvphase==5) goto dlsh5;
                  if (uvphase==4) goto dlsh4;
                  if (uvphase==3) goto dlsh3;
                  if (uvphase==2) goto dlsh2;
                  if (uvphase==1) goto dlsh1;
                  goto dlsh0;

                  while (true) {

                     dlsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx -= 6;
                        curptr -= 16;
                  }
               }
               else { // dn-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx--;
                        uvphase--;
                        if (uvphase==-1) {
                           uvphase = 5;
                           curptr -= 16;
                        }
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow

                  pen = delx;

                  if (uvphase==0) goto ursh0;
                  if (uvphase==1) goto ursh1;
                  if (uvphase==2) goto ursh2;
                  if (uvphase==3) goto ursh3;
                  if (uvphase==4) goto ursh4;
                  goto ursh5;

                  while (true) {

                     ursh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx += 6;
                        curptr += 16;
                  }
               }
               else { // up-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx++;
                        uvphase++;
                        if (uvphase==6) {
                           uvphase = 0;
                           curptr += 16;
                        }
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow

                  pen = delx;

                  if (uvphase==5) goto ulsh5;
                  if (uvphase==4) goto ulsh4;
                  if (uvphase==3) goto ulsh3;
                  if (uvphase==2) goto ulsh2;
                  if (uvphase==1) goto ulsh1;
                  goto ulsh0;

                  while (true) {

                     ulsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx -= 6;
                        curptr -= 16;
                  }

               }
               else { // up-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx--;
                        uvphase--;
                        if (uvphase==-1) {
                           uvphase = 5;
                           curptr -= 16;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   curx = x;
   uvphase = (x%6);
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + (x/6)*16;
}

void CLineEngDVS10::
drawDot()
{
   switch(uvphase) {

      case 0:
         setFGY0();
      break;

      case 1:
         setFGY1();
      break;

      case 2:
         setFGY2();
      break;

      case 3:
         setFGY3();
      break;

      case 4:
         setFGY4();
      break;

      case 5:
         setFGY5();
      break;
   }
}

void CLineEngDVS10::
drawSpot(int x, int y)
{
   movTo(x&0xfffffffe,y);

   switch(uvphase) {

      case 0:

         setFGY0();
         setFGY1();

      break;

      case 2:

         setFGY2();
         setFGY3();

      break;

      case 4:

         setFGY4();
         setFGY5();

      break;
   }
}


void CLineEngDVS10::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int loff = (lft / 6);
   int roff = (rgt / 6);
   int sixes  = roff - loff - 1;

   unsigned int w0 = (fgColor[V]&TOP)+(fgColor[Y]&MID)+(fgColor[U]&LOW);
   unsigned int w1 = (fgColor[Y]&TOP)+(fgColor[U]&MID)+(fgColor[Y]&LOW);
   unsigned int w2 = (fgColor[U]&TOP)+(fgColor[Y]&MID)+(fgColor[V]&LOW);
   unsigned int w3 = (fgColor[Y]&TOP)+(fgColor[V]&MID)+(fgColor[Y]&LOW);

   unsigned char b0 = w0;
   unsigned char b1 = w0>> 8;
   unsigned char b2 = w0>>16;
   unsigned char b3 = w0>>24;

   unsigned char b4 = w1;
   unsigned char b5 = w1>> 8;
   unsigned char b6 = w1>>16;
   unsigned char b7 = w1>>24;

   unsigned char b8  = w2;
   unsigned char b9  = w2>> 8;
   unsigned char b10 = w2>>16;
   unsigned char b11 = w2>>24;

   unsigned char b12 = w3;
   unsigned char b13 = w3>> 8;
   unsigned char b14 = w3>>16;
   unsigned char b15 = w3>>24;

   unsigned char *dst = linebuf;
   for (int i=0;i<sixes;i++) {
      *dst++ = b0;
      *dst++ = b1;
      *dst++ = b2;
      *dst++ = b3;
      *dst++ = b4;
      *dst++ = b5;
      *dst++ = b6;
      *dst++ = b7;
      *dst++ = b8;
      *dst++ = b9;
      *dst++ = b10;
      *dst++ = b11;
      *dst++ = b12;
      *dst++ = b13;
      *dst++ = b14;
      *dst++ = b15;
   }

   sixes *= 16;

   if (sixes < 0) {

      for (int i=top;i<=bot;i++) {

         movTo(lft,i);

         if ((lft%6<=0)&&(rgt%6>=0)) setFGY0();
         if ((lft%6<=1)&&(rgt%6>=1)) setFGY1();
         if ((lft%6<=2)&&(rgt%6>=2)) setFGY2();
         if ((lft%6<=3)&&(rgt%6>=3)) setFGY3();
         if ((lft%6<=4)&&(rgt%6>=4)) setFGY4();
         if ((lft%6<=5)&&(rgt%6>=5)) setFGY5();
      }
   }
   else {

      for (int i=top;i<=bot;i++) {

         movTo(lft,i);

         if (lft%6<=0) setFGY0();
         if (lft%6<=1) setFGY1();
         if (lft%6<=2) setFGY2();
         if (lft%6<=3) setFGY3();
         if (lft%6<=4) setFGY4();
         if (lft%6<=5) setFGY5();
         curptr += 16;

         MTImemcpy(curptr,linebuf,sixes);
         curptr += sixes;

         if (rgt%6>=0) setFGY0();
         if (rgt%6>=1) setFGY1();
         if (rgt%6>=2) setFGY2();
         if (rgt%6>=3) setFGY3();
         if (rgt%6>=4) setFGY4();
         if (rgt%6>=5) setFGY5();

      }
   }
}

void CLineEngDVS10::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         uvphase = x%6;
         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + ((x/6)<<4);

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  if (uvphase==0) setFGY0();
                  else if (uvphase==1) setFGY1();
                  else if (uvphase==2) setFGY2();
                  else if (uvphase==3) setFGY3();
                  else if (uvphase==4) setFGY4();
                  else if (uvphase==5) setFGY5();

                  uvphase++;
                  if (uvphase==6) {
                     uvphase=0;
                     curptr += 16;
                  }
               }
            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     if (uvphase==0) setBGY0();
                     else if (uvphase==1) setBGY1();
                     else if (uvphase==2) setBGY2();
                     else if (uvphase==3) setBGY3();
                     else if (uvphase==4) setBGY4();
                     else if (uvphase==5) setBGY5();

                  }

                  uvphase++;
                  if (uvphase==6) {
                     uvphase=0;
                     curptr += 16;
                  }
               }
            }

            fontscan = fontscan >> 1;

         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//		DVS 10-bit CLIPSTER
//
//////////////////////////////////////////////////////////

#ifdef TOP
#undef TOP
#endif
#ifdef MID
#undef MID
#endif
#ifdef LOW
#undef LOW
#endif
#define TOP 0xffc00000
#define MID 0x003ff000
#define LOW 0x00000ffc

#define U 0
#define V 1
#define Y 2

CLineEngDVS10BE::
CLineEngDVS10BE()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngDVS10BE::
~CLineEngDVS10BE()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngDVS10BE::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                         )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch;
   intrlc  = il;
   fldSize = dpitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned char[dpitch];

   // basic font dimensions

   setFontSize(1);

}

int CLineEngDVS10BE::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngDVS10BE::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngDVS10BE::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[U] = (0x80<<24)+(0x80<<14)+(0x80<<4);
         fgColor[V] = (0x80<<24)+(0x80<<14)+(0x80<<4);
         fgColor[Y] = (0x19<<24)+(0x19<<14)+(0x19<<4);
         fgndColor = col;
      break;

      case 1:
         fgColor[U] = (0x80<<24)+(0x80<<14)+(0x80<<4);
         fgColor[V] = (0x80<<24)+(0x80<<14)+(0x80<<4);
         fgColor[Y] = (0xc8<<24)+(0xc8<<14)+(0xc8<<4);
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngDVS10BE::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   int iu = u>>6;
   fgColor[U] = (iu<<22)+(iu<<12)+(iu<<2);
   int iv = v>>6;
   fgColor[V] = (iv<<22)+(iv<<12)+(iv<<2);
   int iy = y>>6;
   fgColor[Y] = (iy<<22)+(iy<<12)+(iy<<2);
}

void CLineEngDVS10BE::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[U] = (0x80<<24)+(0x80<<14)+(0x80<<4);
         bgColor[V] = (0x80<<24)+(0x80<<14)+(0x80<<4);
         bgColor[Y] = (0x19<<24)+(0x19<<14)+(0x19<<4);
         bgndColor = col;
      break;

      case 1:
         bgColor[U] = (0x80<<24)+(0x80<<14)+(0x80<<4);
         bgColor[V] = (0x80<<24)+(0x80<<14)+(0x80<<4);
         bgColor[Y] = (0xc8<<24)+(0xc8<<14)+(0xc8<<4);
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngDVS10BE::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   int iu = u>>6;
   bgColor[U] = (iu<<22)+(iu<<12)+(iu<<2);
   int iv = v>>6;
   bgColor[V] = (iv<<22)+(iv<<12)+(iv<<2);
   int iy = y>>6;
   bgColor[Y] = (iy<<22)+(iy<<12)+(iy<<2);
}

// sets Y and EITHER U or V
void CLineEngDVS10BE::
setFGY0()
{
    int unpack;
   unpack = (curptr[2]<<8)+(curptr[3]);
   unpack = (fgColor[U]&TOP)+(fgColor[Y]&MID)+(unpack&LOW);
   curptr[0] = (unpack>>24);
   curptr[1] = (unpack>>16);
   curptr[2] = (unpack>> 8);
   curptr[3] = (unpack    );
   return;
}

void CLineEngDVS10BE::
setFGY1()
{
    int unpack;
   unpack = (curptr[2]<<8);
   unpack = (unpack&(TOP+MID))+(fgColor[V]&LOW);
   curptr[2] = (unpack>>8);
   curptr[3] = (unpack   );
   unpack = (curptr[5]<<16);
   unpack = (fgColor[Y]&TOP)+(unpack&(MID+LOW));
   curptr[4] = (unpack>>24);
   curptr[5] = (unpack>>16);
   return;
}

void CLineEngDVS10BE::
setFGY2()
{
    int unpack;
   unpack = (curptr[5]<<16);
   unpack = (unpack&TOP)+(fgColor[U]&MID)+(fgColor[Y]&LOW);
   curptr[5] = (unpack>>16);
   curptr[6] = (unpack>> 8);
   curptr[7] = (unpack    );
   return;
}

void CLineEngDVS10BE::
setFGY3()
{
    int unpack;
   unpack = (curptr[10]<<8);
   unpack = (fgColor[V]&TOP)+(fgColor[Y]&MID)+(unpack&LOW);
   curptr[8]  = (unpack>>24);
   curptr[9]  = (unpack>>16);
   curptr[10] = (unpack>> 8);
   return;
}

void CLineEngDVS10BE::
setFGY4()
{
    int unpack;
   unpack = (curptr[10]<<8);
   unpack = (unpack&(TOP+MID))+(fgColor[U]&LOW);
   curptr[10] = (unpack>> 8);
   curptr[11] = (unpack    );
   unpack = (curptr[13]<<16);
   unpack = (fgColor[Y]&TOP)+(unpack&(MID+LOW));
   curptr[12] = (unpack>>24);
   curptr[13] = (unpack>>16);
   return;
}

void CLineEngDVS10BE::
setFGY5()
{
    int unpack;
   unpack = (curptr[13]<<16);
   unpack = (unpack&TOP)+(fgColor[V]&MID)+(fgColor[Y]&LOW);
   curptr[13] = (unpack>>16);
   curptr[14] = (unpack>> 8);
   curptr[15] = (unpack    );
   return;
}

// sets Y and EITHER U or V
void CLineEngDVS10BE::
setBGY0()
{
    int unpack;
   unpack = (curptr[2]<<8)+(curptr[3]);
   unpack = (bgColor[U]&TOP)+(bgColor[Y]&MID)+(unpack&LOW);
   curptr[0] = (unpack>>24);
   curptr[1] = (unpack>>16);
   curptr[2] = (unpack>> 8);
   curptr[3] = (unpack    );
   return;
}

void CLineEngDVS10BE::
setBGY1()
{
    int unpack;
   unpack = (curptr[2]<<8);
   unpack = (unpack&(TOP+MID))+(bgColor[V]&LOW);
   curptr[2] = (unpack>>8);
   curptr[3] = (unpack   );
   unpack = (curptr[5]<<16);
   unpack = (bgColor[Y]&TOP)+(unpack&(MID+LOW));
   curptr[4] = (unpack>>24);
   curptr[5] = (unpack>>16);
   return;
}

void CLineEngDVS10BE::
setBGY2()
{
    int unpack;
   unpack = (curptr[5]<<16);
   unpack = (unpack&TOP)+(bgColor[U]&MID)+(bgColor[Y]&LOW);
   curptr[5] = (unpack>>16);
   curptr[6] = (unpack>> 8);
   curptr[7] = (unpack    );
   return;
}

void CLineEngDVS10BE::
setBGY3()
{
    int unpack;
   unpack = (curptr[10]<<8);
   unpack = (bgColor[V]&TOP)+(bgColor[Y]&MID)+(unpack&LOW);
   curptr[8]  = (unpack>>24);
   curptr[9]  = (unpack>>16);
   curptr[10] = (unpack>> 8);
   return;
}

void CLineEngDVS10BE::
setBGY4()
{
    int unpack;
   unpack = (curptr[10]<<8);
   unpack = (unpack&(TOP+MID))+(bgColor[U]&LOW);
   curptr[10] = (unpack>> 8);
   curptr[11] = (unpack    );
   unpack = (curptr[13]<<16);
   unpack = (bgColor[Y]&TOP)+(unpack&(MID+LOW));
   curptr[12] = (unpack>>24);
   curptr[13] = (unpack>>16);
   return;
}

void CLineEngDVS10BE::
setBGY5()
{
    int unpack;
   unpack = (curptr[13]<<16);
   unpack = (unpack&TOP)+(bgColor[V]&MID)+(bgColor[Y]&LOW);
   curptr[13] = (unpack>>16);
   curptr[14] = (unpack>> 8);
   curptr[15] = (unpack    );
   return;
}

void CLineEngDVS10BE::
movTo(
        int x,
        int y
      )
{
   curx = x;
   uvphase = (x%6);
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + (x/6)*16;
}

void CLineEngDVS10BE::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt

         if (uvphase==0) goto hr0;
         if (uvphase==1) goto hr1;
         if (uvphase==2) goto hr2;
         if (uvphase==3) goto hr3;
         if (uvphase==4) goto hr4;
         goto hr5;

         while (true) {

	    hr0:;

               if (delx--==0) break;
               setFGY0();

	    hr1:;

               if (delx--==0) break;
               setFGY1();

	    hr2:;

               if (delx--==0) break;
               setFGY2();

	    hr3:;

               if (delx--==0) break;
               setFGY3();

	    hr4:

               if (delx--==0) break;
               setFGY4();

	    hr5:

               if (delx--==0) break;
               setFGY5();
               curptr += 16;  
         }
      }
      else  { // lft

         if (uvphase==5) goto hl5;
         if (uvphase==4) goto hl4;
         if (uvphase==3) goto hl3;
         if (uvphase==2) goto hl2;
         if (uvphase==1) goto hl1;
         goto hl0;

         while (true) {

	    hl5:;

               if (delx++==0) break;
               setFGY5();

	    hl4:;

               if (delx++==0) break;
               setFGY4();

	    hl3:;

               if (delx++==0) break;
               setFGY3();

	    hl2:;

               if (delx++==0) break;
               setFGY2();

	    hl1:

               if (delx++==0) break;
               setFGY1();

	    hl0:

               if (delx++==0) break;
               setFGY0();
               curptr -= 16;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

	    if (uvphase==0) {
	       while (dely--) {
                  setFGY0();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
	    else if (uvphase==1) {
	       while (dely--) {
                  setFGY1();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==2) {
	       while (dely--) {
                  setFGY2();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==3) {
	       while (dely--) {
                  setFGY3();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==4) {
	       while (dely--) {
                  setFGY4();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==5) {
	       while (dely--) {
                  setFGY5();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
         }
         else { // up

	    if (uvphase==0) {
	       while (dely++) {
                  setFGY0();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
	    else if (uvphase==1) {
	       while (dely++) {
                  setFGY1();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==2) {
	       while (dely++) {
                  setFGY2();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==3) {
	       while (dely++) {
                  setFGY3();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==4) {
               while (dely++) {
                  setFGY4();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==5) {
               while (dely++) {
                  setFGY5();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow

                  pen = delx;

                  if (uvphase==0) goto drsh0;
                  if (uvphase==1) goto drsh1;
                  if (uvphase==2) goto drsh2;
                  if (uvphase==3) goto drsh3;
                  if (uvphase==4) goto drsh4;
                  goto drsh5;

                  while (true) {

                     drsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx += 6;
                        curptr += 16;

                  }
               }
               else { // dn-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx++;
                        uvphase++;
                        if (uvphase==6) {
                           uvphase = 0;
                           curptr += 16;
                        }
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow

                  pen = delx;

                  if (uvphase==5) goto dlsh5;
                  if (uvphase==4) goto dlsh4;
                  if (uvphase==3) goto dlsh3;
                  if (uvphase==2) goto dlsh2;
                  if (uvphase==1) goto dlsh1;
                  goto dlsh0;

                  while (true) {

                     dlsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx -= 6;
                        curptr -= 16;
                  }
               }
               else { // dn-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx--;
                        uvphase--;
                        if (uvphase==-1) {
                           uvphase = 5;
                           curptr -= 16;
                        }
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow

                  pen = delx;

                  if (uvphase==0) goto ursh0;
                  if (uvphase==1) goto ursh1;
                  if (uvphase==2) goto ursh2;
                  if (uvphase==3) goto ursh3;
                  if (uvphase==4) goto ursh4;
                  goto ursh5;

                  while (true) {

                     ursh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx += 6;
                        curptr += 16;
                  }
               }
               else { // up-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx++;
                        uvphase++;
                        if (uvphase==6) {
                           uvphase = 0;
                           curptr += 16;
                        }
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow

                  pen = delx;

                  if (uvphase==5) goto ulsh5;
                  if (uvphase==4) goto ulsh4;
                  if (uvphase==3) goto ulsh3;
                  if (uvphase==2) goto ulsh2;
                  if (uvphase==1) goto ulsh1;
                  goto ulsh0;

                  while (true) {

                     ulsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx -= 6;
                        curptr -= 16;
                  }

               }
               else { // up-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx--;
                        uvphase--;
                        if (uvphase==-1) {
                           uvphase = 5;
                           curptr -= 16;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   curx = x;
   uvphase = (x%6);
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + (x/6)*16;
}

void CLineEngDVS10BE::
drawDot()
{
   switch(uvphase) {

      case 0:
         setFGY0();
      break;

      case 1:
         setFGY1();
      break;

      case 2:
         setFGY2();
      break;

      case 3:
         setFGY3();
      break;

      case 4:
         setFGY4();
      break;

      case 5:
         setFGY5();
      break;
   }
}

void CLineEngDVS10BE::
drawSpot(int x, int y)
{
   movTo(x&0xfffffffe,y);

   switch(uvphase) {

      case 0:

         setFGY0();
         setFGY1();

      break;

      case 2:

         setFGY2();
         setFGY3();

      break;

      case 4:

         setFGY4();
         setFGY5();

      break;
   }
}


void CLineEngDVS10BE::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int loff = (lft / 6);
   int roff = (rgt / 6);
   int sixes  = roff - loff - 1;

   unsigned int w0 = (fgColor[U]&TOP)+(fgColor[Y]&MID)+(fgColor[V]&LOW);
   unsigned int w1 = (fgColor[Y]&TOP)+(fgColor[U]&MID)+(fgColor[Y]&LOW);
   unsigned int w2 = (fgColor[V]&TOP)+(fgColor[Y]&MID)+(fgColor[U]&LOW);
   unsigned int w3 = (fgColor[Y]&TOP)+(fgColor[V]&MID)+(fgColor[Y]&LOW);

   unsigned char b0 = w0>>24;
   unsigned char b1 = w0>>16;
   unsigned char b2 = w0>> 8;
   unsigned char b3 = w0;

   unsigned char b4 = w1>>24;
   unsigned char b5 = w1>>16;
   unsigned char b6 = w1>> 8;
   unsigned char b7 = w1;

   unsigned char b8  = w2>>24;
   unsigned char b9  = w2>>16;
   unsigned char b10 = w2>> 8;
   unsigned char b11 = w2;

   unsigned char b12 = w3>>24;
   unsigned char b13 = w3>>16;
   unsigned char b14 = w3>> 8;
   unsigned char b15 = w3;

   unsigned char *dst = linebuf;
   for (int i=0;i<sixes;i++) {
      *dst++ = b0;
      *dst++ = b1;
      *dst++ = b2;
      *dst++ = b3;
      *dst++ = b4;
      *dst++ = b5;
      *dst++ = b6;
      *dst++ = b7;
      *dst++ = b8;
      *dst++ = b9;
      *dst++ = b10;
      *dst++ = b11;
      *dst++ = b12;
      *dst++ = b13;
      *dst++ = b14;
      *dst++ = b15;
   }

   sixes *= 16;

   if (sixes < 0) {

      for (int i=top;i<=bot;i++) {

         movTo(lft,i);

         if ((lft%6<=0)&&(rgt%6>=0)) setFGY0();
         if ((lft%6<=1)&&(rgt%6>=1)) setFGY1();
         if ((lft%6<=2)&&(rgt%6>=2)) setFGY2();
         if ((lft%6<=3)&&(rgt%6>=3)) setFGY3();
         if ((lft%6<=4)&&(rgt%6>=4)) setFGY4();
         if ((lft%6<=5)&&(rgt%6>=5)) setFGY5();
      }
   }
   else {

      for (int i=top;i<=bot;i++) {

         movTo(lft,i);

         if (lft%6<=0) setFGY0();
         if (lft%6<=1) setFGY1();
         if (lft%6<=2) setFGY2();
         if (lft%6<=3) setFGY3();
         if (lft%6<=4) setFGY4();
         if (lft%6<=5) setFGY5();
         curptr += 16;

         MTImemcpy(curptr,linebuf,sixes);
         curptr += sixes;

         if (rgt%6>=0) setFGY0();
         if (rgt%6>=1) setFGY1();
         if (rgt%6>=2) setFGY2();
         if (rgt%6>=3) setFGY3();
         if (rgt%6>=4) setFGY4();
         if (rgt%6>=5) setFGY5();

      }
   }
}

void CLineEngDVS10BE::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         uvphase = x%6;
         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + ((x/6)<<4);

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  if (uvphase==0) setFGY0();
                  else if (uvphase==1) setFGY1();
                  else if (uvphase==2) setFGY2();
                  else if (uvphase==3) setFGY3();
                  else if (uvphase==4) setFGY4();
                  else if (uvphase==5) setFGY5();

                  uvphase++;
                  if (uvphase==6) {
                     uvphase=0;
                     curptr += 16;
                  }
               }
            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     if (uvphase==0) setBGY0();
                     else if (uvphase==1) setBGY1();
                     else if (uvphase==2) setBGY2();
                     else if (uvphase==3) setBGY3();
                     else if (uvphase==4) setBGY4();
                     else if (uvphase==5) setBGY5();

                  }

                  uvphase++;
                  if (uvphase==6) {
                     uvphase=0;
                     curptr += 16;
                  }
               }
            }

            fontscan = fontscan >> 1;

         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//		DVSa 10-bit - oldest format
//
//////////////////////////////////////////////////////////

#define TOP2 0x30
#define MID2 0x0C
#define LOW2 0x03

#define U 0
#define V 1
#define Y 2

CLineEngDVS10a::
CLineEngDVS10a()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngDVS10a::
~CLineEngDVS10a()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngDVS10a::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                         )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch;
   intrlc  = il;
   fldSize = dpitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned char[dpitch];

   // basic font dimensions

   setFontSize(1);

}

int CLineEngDVS10a::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngDVS10a::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngDVS10a::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[U] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         fgColor[V] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         fgColor[Y] = (0x19<<22)+(0x19<<12)+(0x19<<2);
         fgndColor = col;
      break;

      case 1:
         fgColor[U] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         fgColor[V] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         fgColor[Y] = (0xc8<<22)+(0xc8<<12)+(0xc8<<2);
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngDVS10a::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   int iu = u>>6;
   fgColor[U] = (iu<<20)+(iu<<10)+(iu);
   int iv = v>>6;
   fgColor[V] = (iv<<20)+(iv<<10)+(iv);
   int iy = y>>6;
   fgColor[Y] = (iy<<20)+(iy<<10)+(iy);
}

void CLineEngDVS10a::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[U] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         bgColor[V] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         bgColor[Y] = (0x19<<22)+(0x19<<12)+(0x19<<2);
         bgndColor = col;
      break;

      case 1:
         bgColor[U] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         bgColor[V] = (0x80<<22)+(0x80<<12)+(0x80<<2);
         bgColor[Y] = (0xc8<<22)+(0xc8<<12)+(0xc8<<2);
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngDVS10a::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   int iu = u>>6;
   bgColor[U] = (iu<<20)+(iu<<10)+(iu);
   int iv = v>>6;
   bgColor[V] = (iv<<20)+(iv<<10)+(iv);
   int iy = y>>6;
   bgColor[Y] = (iy<<20)+(iy<<10)+(iy<<0);
}

// sets Y and EITHER U or V
void CLineEngDVS10a::
setFGY0()
{
   curptr[0] = fgColor[U]>>2;
   curptr[1] = fgColor[Y]>>2;
   curptr[3] = (curptr[3]&LOW2)+ ((fgColor[Y]<<2)&MID2)+((fgColor[U]<<4)&TOP2);
   return;
}

void CLineEngDVS10a::
setFGY1()
{
   curptr[2] = fgColor[V]>>2;
   curptr[3] = (curptr[3]&(TOP2+MID2))+(fgColor[V]&LOW2);
   curptr[4] = fgColor[Y]>>2;
   curptr[7] = (curptr[7]&(MID2+LOW2))+((fgColor[Y]<<4)&TOP2);
   return;
}

void CLineEngDVS10a::
setFGY2()
{
   curptr[5] = fgColor[U]>>2;
   curptr[6] = fgColor[Y]>>2;
   curptr[7] = (curptr[7]&TOP2)+((fgColor[U]<<2)&MID2)+(fgColor[Y]&LOW2);
   return;
}

void CLineEngDVS10a::
setFGY3()
{
   curptr[8] = fgColor[V]>>2;
   curptr[9] = fgColor[Y]>>2;
   curptr[11] = (curptr[11]&LOW2)+((fgColor[V]<<4)&TOP2)+((fgColor[Y]<<2)&MID2);
   return;
}

void CLineEngDVS10a::
setFGY4()
{
   curptr[10] = fgColor[U]>>2;
   curptr[11] = (curptr[11]&(TOP2+MID2))+(fgColor[U]&LOW2);
   curptr[12] = fgColor[Y]>>2;
   curptr[15] = (curptr[15]&(MID2+LOW2))+((fgColor[Y]<<4)&TOP2);
   return;
}

void CLineEngDVS10a::
setFGY5()
{
   curptr[13] = fgColor[V]>>2;
   curptr[14] = fgColor[Y]>>2;
   curptr[15] = (curptr[15]&TOP2)+((fgColor[V]<<2)&MID2)+(fgColor[Y]&LOW2);
   return;
}

// sets Y and EITHER U or V

void CLineEngDVS10a::
setBGY0()
{
   curptr[0] = bgColor[U]>>2;
   curptr[1] = bgColor[Y]>>2;
   curptr[4] = (curptr[4]&LOW2)+ ((bgColor[Y]<<2)&MID2)+((bgColor[U]<<4)&TOP2);
   return;
}

void CLineEngDVS10a::
setBGY1()
{
   curptr[2] = bgColor[V]>>2;
   curptr[3] = (curptr[3]&(TOP2+MID2))+(bgColor[V]&LOW2);
   curptr[4] = bgColor[Y]>>2;
   curptr[7] = (curptr[7]&(MID2+LOW2))+((bgColor[Y]<<4)&TOP2);
   return;
}

void CLineEngDVS10a::
setBGY2()
{
   curptr[5] = bgColor[U]>>2;
   curptr[6] = bgColor[Y]>>2;
   curptr[7] = (curptr[7]&TOP2)+((bgColor[U]<<2)&MID2)+(bgColor[Y]&LOW2);
   return;
}

void CLineEngDVS10a::
setBGY3()
{
   curptr[8] = bgColor[V]>>2;
   curptr[9] = bgColor[Y]>>2;
   curptr[11] = (curptr[11]&LOW2)+((bgColor[V]<<4)&TOP2)+((bgColor[Y]<<2)&MID2);
   return;
}

void CLineEngDVS10a::
setBGY4()
{
   curptr[10] = bgColor[U]>>2;
   curptr[11] = (curptr[11]&(TOP2+MID2))+(bgColor[U]&LOW2);
   curptr[12] = bgColor[Y]>>2;
   curptr[15] = (curptr[15]&(MID2+LOW2))+((bgColor[Y]<<4)&TOP2);
   return;
}

void CLineEngDVS10a::
setBGY5()
{
   curptr[13] = bgColor[V]>>2;
   curptr[14] = bgColor[Y]>>2;
   curptr[15] = (curptr[15]&TOP2)+((bgColor[V]<<2)&MID2)+(bgColor[Y]&LOW2);
   return;
}

void CLineEngDVS10a::
movTo(
        int x,
        int y
      )
{
   curx = x;
   uvphase = (x%6);
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + (x/6)*16;
}

void CLineEngDVS10a::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt

         if (uvphase==0) goto hr0;
         if (uvphase==1) goto hr1;
         if (uvphase==2) goto hr2;
         if (uvphase==3) goto hr3;
         if (uvphase==4) goto hr4;
         goto hr5;

         while (true) {

	    hr0:;

               if (delx--==0) break;
               setFGY0();

	    hr1:;

               if (delx--==0) break;
               setFGY1();

	    hr2:;

               if (delx--==0) break;
               setFGY2();

	    hr3:;

               if (delx--==0) break;
               setFGY3();

	    hr4:

               if (delx--==0) break;
               setFGY4();

	    hr5:

               if (delx--==0) break;
               setFGY5();
               curptr += 16;
         }
      }
      else  { // lft

         if (uvphase==5) goto hl5;
         if (uvphase==4) goto hl4;
         if (uvphase==3) goto hl3;
         if (uvphase==2) goto hl2;
         if (uvphase==1) goto hl1;
         goto hl0;

         while (true) {

	    hl5:;

               if (delx++==0) break;
               setFGY5();

	    hl4:;

               if (delx++==0) break;
               setFGY4();

	    hl3:;

               if (delx++==0) break;
               setFGY3();

	    hl2:;

               if (delx++==0) break;
               setFGY2();

	    hl1:

               if (delx++==0) break;
               setFGY1();

	    hl0:

               if (delx++==0) break;
               setFGY0();
               curptr -= 16;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

	    if (uvphase==0) {
	       while (dely--) {
                  setFGY0();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
	    else if (uvphase==1) {
	       while (dely--) {
                  setFGY1();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==2) {
	       while (dely--) {
                  setFGY2();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==3) {
	       while (dely--) {
                  setFGY3();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==4) {
	       while (dely--) {
                  setFGY4();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==5) {
	       while (dely--) {
                  setFGY5();
                  rowy = rowtbl + (++cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
         }
         else { // up

	    if (uvphase==0) {
	       while (dely++) {
                  setFGY0();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
	    else if (uvphase==1) {
	       while (dely++) {
                  setFGY1();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==2) {
	       while (dely++) {
                  setFGY2();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==3) {
	       while (dely++) {
                  setFGY3();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==4) {
               while (dely++) {
                  setFGY4();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
            else if (uvphase==5) {
               while (dely++) {
                  setFGY5();
                  rowy = rowtbl + (--cury);
                  curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
               }
            }
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow

                  pen = delx;

                  if (uvphase==0) goto drsh0;
                  if (uvphase==1) goto drsh1;
                  if (uvphase==2) goto drsh2;
                  if (uvphase==3) goto drsh3;
                  if (uvphase==4) goto drsh4;
                  goto drsh5;

                  while (true) {

                     drsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     drsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx += 6;
                        curptr += 16;

                  }
               }
               else { // dn-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx++;
                        uvphase++;
                        if (uvphase==6) {
                           uvphase = 0;
                           curptr += 16;
                        }
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow

                  pen = delx;

                  if (uvphase==5) goto dlsh5;
                  if (uvphase==4) goto dlsh4;
                  if (uvphase==3) goto dlsh3;
                  if (uvphase==2) goto dlsh2;
                  if (uvphase==1) goto dlsh1;
                  goto dlsh0;

                  while (true) {

                     dlsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     dlsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           rowy = rowtbl + (++cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx -= 6;
                        curptr -= 16;
                  }
               }
               else { // dn-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx--;
                        uvphase--;
                        if (uvphase==-1) {
                           uvphase = 5;
                           curptr -= 16;
                        }
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow

                  pen = delx;

                  if (uvphase==0) goto ursh0;
                  if (uvphase==1) goto ursh1;
                  if (uvphase==2) goto ursh2;
                  if (uvphase==3) goto ursh3;
                  if (uvphase==4) goto ursh4;
                  goto ursh5;

                  while (true) {

                     ursh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ursh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx += 6;
                        curptr += 16;
                  }
               }
               else { // up-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx++;
                        uvphase++;
                        if (uvphase==6) {
                           uvphase = 0;
                           curptr += 16;
                        }
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow

                  pen = delx;

                  if (uvphase==5) goto ulsh5;
                  if (uvphase==4) goto ulsh4;
                  if (uvphase==3) goto ulsh3;
                  if (uvphase==2) goto ulsh2;
                  if (uvphase==1) goto ulsh1;
                  goto ulsh0;

                  while (true) {

                     ulsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                     ulsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           rowy = rowtbl + (--cury);
                           curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);
                        }

                        curx -= 6;
                        curptr -= 16;
                  }

               }
               else { // up-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (uvphase==0) setFGY0();
                     else if (uvphase==1) setFGY1();
                     else if (uvphase==2) setFGY2();
                     else if (uvphase==3) setFGY3();
                     else if (uvphase==4) setFGY4();
                     else if (uvphase==5) setFGY5();

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + ((curx/6)<<4);

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx--;
                        uvphase--;
                        if (uvphase==-1) {
                           uvphase = 5;
                           curptr -= 16;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   curx = x;
   uvphase = (x%6);
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + (x/6)*16;
}

void CLineEngDVS10a::
drawDot()
{
   switch(uvphase) {

      case 0:
         setFGY0();
      break;

      case 1:
         setFGY1();
      break;

      case 2:
         setFGY2();
      break;

      case 3:
         setFGY3();
      break;

      case 4:
         setFGY4();
      break;

      case 5:
         setFGY5();
      break;
   }
}

void CLineEngDVS10a::
drawSpot(int x, int y)
{
   movTo(x&0xfffffffe,y);

   switch(uvphase) {

      case 0:

         setFGY0();
         setFGY1();

      break;

      case 2:

         setFGY2();
         setFGY3();

      break;

      case 4:

         setFGY4();
         setFGY5();

      break;
   }
}


void CLineEngDVS10a::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int loff = (lft / 6);
   int roff = (rgt / 6);
   int sixes  = roff - loff - 1;

   unsigned char b0  = (fgColor[U]>>2);
   unsigned char b1  = (fgColor[Y]>>2);
   unsigned char b2  = (fgColor[V]>>2);
   unsigned char b3  = ((fgColor[U]<<4)&TOP2)+((fgColor[Y]<<2)&MID2)+((fgColor[V])&LOW2);

   unsigned char b4  = (fgColor[Y]>>2);
   unsigned char b5  = (fgColor[U]>>2);
   unsigned char b6  = (fgColor[Y]>>2);
   unsigned char b7  = ((fgColor[Y]<<4)&TOP2)+((fgColor[U]<<2)&MID2)+((fgColor[Y])&LOW2);

   unsigned char b8  = (fgColor[V]>>2);
   unsigned char b9  = (fgColor[Y]>>2);
   unsigned char b10 = (fgColor[U]>>2);
   unsigned char b11 = ((fgColor[V]<<4)&TOP2)+((fgColor[Y]<<2)&MID2)+((fgColor[U])&LOW2);

   unsigned char b12 = (fgColor[Y]>>2);
   unsigned char b13 = (fgColor[V]>>2);
   unsigned char b14 = (fgColor[Y]>>2);
   unsigned char b15 = ((fgColor[Y]<<4)&TOP2)+((fgColor[V]<<2)&MID2)+((fgColor[Y])&LOW2);

   unsigned char *dst = linebuf;
   for (int i=0;i<sixes;i++) {
      *dst++ = b0;
      *dst++ = b1;
      *dst++ = b2;
      *dst++ = b3;
      *dst++ = b4;
      *dst++ = b5;
      *dst++ = b6;
      *dst++ = b7;
      *dst++ = b8;
      *dst++ = b9;
      *dst++ = b10;
      *dst++ = b11;
      *dst++ = b12;
      *dst++ = b13;
      *dst++ = b14;
      *dst++ = b15;
   }

   sixes *= 16;

   if (sixes < 0) {

      for (int i=top;i<=bot;i++) {

         movTo(lft,i);

         if ((lft%6<=0)&&(rgt%6>=0)) setFGY0();
         if ((lft%6<=1)&&(rgt%6>=1)) setFGY1();
         if ((lft%6<=2)&&(rgt%6>=2)) setFGY2();
         if ((lft%6<=3)&&(rgt%6>=3)) setFGY3();
         if ((lft%6<=4)&&(rgt%6>=4)) setFGY4();
         if ((lft%6<=5)&&(rgt%6>=5)) setFGY5();
      }
   }
   else {

      for (int i=top;i<=bot;i++) {

         movTo(lft,i);

         if (lft%6<=0) setFGY0();
         if (lft%6<=1) setFGY1();
         if (lft%6<=2) setFGY2();
         if (lft%6<=3) setFGY3();
         if (lft%6<=4) setFGY4();
         if (lft%6<=5) setFGY5();
         curptr += 16;

         MTImemcpy(curptr,linebuf,sixes);
         curptr += sixes;

         if (rgt%6>=0) setFGY0();
         if (rgt%6>=1) setFGY1();
         if (rgt%6>=2) setFGY2();
         if (rgt%6>=3) setFGY3();
         if (rgt%6>=4) setFGY4();
         if (rgt%6>=5) setFGY5();

      }
   }
}

void CLineEngDVS10a::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         uvphase = x%6;
         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + ((x/6)<<4);

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  if (uvphase==0) setFGY0();
                  else if (uvphase==1) setFGY1();
                  else if (uvphase==2) setFGY2();
                  else if (uvphase==3) setFGY3();
                  else if (uvphase==4) setFGY4();
                  else if (uvphase==5) setFGY5();

                  uvphase++;
                  if (uvphase==6) {
                     uvphase=0;
                     curptr += 16;
                  }
               }
            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     if (uvphase==0) setBGY0();
                     else if (uvphase==1) setBGY1();
                     else if (uvphase==2) setBGY2();
                     else if (uvphase==3) setBGY3();
                     else if (uvphase==4) setBGY4();
                     else if (uvphase==5) setBGY5();

                  }

                  uvphase++;
                  if (uvphase==6) {
                     uvphase=0;
                     curptr += 16;
                  }
               }
            }

            fontscan = fontscan >> 1;

         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//              SGI 8-bit
//
//////////////////////////////////////////////////////////

CLineEngRGB8::
CLineEngRGB8()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngRGB8::
~CLineEngRGB8()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngRGB8::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                        )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch;
   intrlc  = il;
   fldSize = pitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned char[dpitch];

   // basic font dimensions

   setFontSize(1);
}

int CLineEngRGB8::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngRGB8::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngRGB8::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x00;
         fgColor[1] = 0x00;
         fgColor[2] = 0x00;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xff;
         fgColor[1] = 0xff;
         fgColor[2] = 0xff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB8::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   fgColor[0] = r>>8;
   fgColor[1] = g>>8;
   fgColor[2] = b>>8;
}

void CLineEngRGB8::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x00;
         bgColor[1] = 0x00;
         bgColor[2] = 0x00;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xff;
         bgColor[1] = 0xff;
         bgColor[2] = 0xff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB8::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   bgColor[0] = r>>8;
   bgColor[1] = g>>8;
   bgColor[2] = b>>8;
}

void CLineEngRGB8::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + x*3;
}

void CLineEngRGB8::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {

            *(curptr)   = fgColor[0];
            *(curptr+1) = fgColor[1];
            *(curptr+2) = fgColor[2];

            curptr += 3;
            curx++;
         }
      }
      else { // lft
         while (delx++!=0) {

            *(curptr)   = fgColor[0];
            *(curptr+1) = fgColor[1];
            *(curptr+2) = fgColor[2];

            curptr -= 3;
            curx--;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            while (dely--!=0) {

               *(curptr)   = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];

               rowy = rowtbl + (++cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

            }

         }
         else { // up

            while (dely++!=0) {

               *(curptr)   = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];

               rowy = rowtbl + (--cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

            }

         }

      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr += 3;
                     curx++;

                     if ((pen-=bry) <= 0) {
                        pen += brx;

                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr += 3;
                        curx++;
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr -= 3;
                     curx--;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr -= 3;
                        curx--;
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr += 3;
                     curx++;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr += 3;
                        curx++;
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr -= 3;
                     curx--;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr -= 3;
                        curx--;
                     }
                  }
               }
            }
         }
      }
   }
}

void CLineEngRGB8::
drawDot()
{
   *(curptr)   = fgColor[0];
   *(curptr+1) = fgColor[1];
   *(curptr+2) = fgColor[2];
}

void CLineEngRGB8::
drawSpot(int x, int y)
{
   movTo(x,y);
   curptr[0] = fgColor[0];
   curptr[1] = fgColor[1];
   curptr[2] = fgColor[2];
}

void CLineEngRGB8::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int pxls = rgt - lft + 1;

   unsigned char *dst = linebuf;

   for (int i=0;i<pxls;i++) {
      *dst++ = fgColor[0];
      *dst++ = fgColor[1];
      *dst++ = fgColor[2];
   }

   pxls *= 3;
   for (int i=top;i<=bot;i++)
   {
      unsigned char *rowbeg = (unsigned char *)(frameBuffer[rowTbl[i].rowfld] + rowTbl[i].rowoff + lft*3);
      MTImemcpy((unsigned char *)rowbeg,linebuf,pxls);
   }
}

void CLineEngRGB8::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';
   
   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + x*3;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  *(curptr)   = fgColor[0];
                  *(curptr+1) = fgColor[1];
                  *(curptr+2) = fgColor[2];

                  curptr += 3;

               }
            }
            else {

               if (bgndColor==-1) {

                  curptr += 3*pelMult;
               }
               else {

                  for (int k=0;k<pelMult;k++) {

                     *curptr     = bgColor[0];
                     *(curptr+1) = bgColor[1];
                     *(curptr+2) = bgColor[2];

                     curptr += 3;

                  }
               }
            }

            fontscan = fontscan >> 1;

         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//              DPX 10-bit
//
//////////////////////////////////////////////////////////

CLineEngRGB10::
CLineEngRGB10()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngRGB10::
~CLineEngRGB10()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngRGB10::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                        )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch>>2;
   intrlc  = il;
   fldSize = pitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned int[dpitch];

   // basic font dimensions

   setFontSize(1);
}

int CLineEngRGB10::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = (unsigned int *)new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = (unsigned int *)new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngRGB10::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = (unsigned int *)frmbuf[0];
   frameBuffer[1] = (unsigned int *)frmbuf[1];
}

void CLineEngRGB10::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor = 0x00000000;
         fgndColor = col;
      break;

      case 1:
         fgColor = 0xfffffffc;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB10::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int fgCol = (((unsigned int)r>>6)<<22)+
                        (((unsigned int)g>>6)<<12)+
                        (((unsigned int)b>>6)<< 2);

#if ENDIAN == MTI_LITTLE_ENDIAN // xform BE to LE
   fgCol = ((fgCol&0xff)<<24)+
           ((fgCol&0xff00)<<8)+
           ((fgCol&0xff0000)>>8)+
           ((fgCol&0xff000000)>>24);
#endif

   fgColor = fgCol;
}

void CLineEngRGB10::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor = 0x00000000;
         bgndColor = col;
      break;

      case 1:
         bgColor = 0xfffffffc;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB10::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int bgCol = (((unsigned int)r>>6)<<22)+
                        (((unsigned int)g>>6)<<12)+
                        (((unsigned int)b>>6)<< 2);

#if ENDIAN == MTI_LITTLE_ENDIAN // xform BE to LE
   bgCol = ((bgCol&0xff)<<24)+
           ((bgCol&0xff00)<<8)+
           ((bgCol&0xff0000)>>8)+
           ((bgCol&0xff000000)>>24);
#endif

   bgColor = bgCol;
}

void CLineEngRGB10::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + x;
}

void CLineEngRGB10::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {

            *curptr = fgColor;
            curptr++;
            curx++;
         }
      }
      else { // lft
         while (delx++!=0) {

            *curptr = fgColor;
            curptr--;
            curx--;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            while (dely--!=0) {

               *curptr = fgColor;
               rowy = rowtbl + (++cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

            }

         }
         else { // up

            while (dely++!=0) {

               *curptr = fgColor;
               rowy = rowtbl + (--cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

            }

         }

      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *curptr = fgColor;
                     curptr++;
                     curx++;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *curptr = fgColor;
                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr++;
                        curx++;
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *curptr = fgColor;
                     curptr--;
                     curx--;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *curptr = fgColor;

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr--;
                        curx--;
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *curptr = fgColor;
                     curptr++;
                     curx++;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *curptr = fgColor;

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr++;
                        curx++;
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *curptr = fgColor;
                     curptr--;
                     curx--;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *curptr = fgColor;

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr--;
                        curx--;
                     }
                  }
               }
            }
         }
      }
   }
}

void CLineEngRGB10::
drawDot()
{
   *curptr = fgColor;
}

void CLineEngRGB10::
drawSpot(int x, int y)
{
   movTo(x,y);
   *curptr = fgColor;
}

void CLineEngRGB10::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int pxls = rgt - lft + 1;

   unsigned int *dst = linebuf;

   for (int i=0;i<pxls;i++) {
      *dst++ = fgColor;
   }

   pxls *= 4;
   for (int i=top;i<=bot;i++)
   {
      unsigned char *rowbeg = (unsigned char *)(frameBuffer[rowTbl[i].rowfld] + rowTbl[i].rowoff + lft);
      MTImemcpy((unsigned char *)rowbeg,linebuf,pxls);
   }
}

void CLineEngRGB10::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';
   
   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + x;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  *curptr = fgColor;
                  curptr++;

               }
            }
            else {

               if (bgndColor==-1) {

                  curptr += pelMult;
               }
               else {

                  for (int k=0;k<pelMult;k++) {

                     *curptr     = bgColor;
                     curptr++;

                  }
               }
            }

            fontscan = fontscan >> 1;

         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//		RGB 12-bit DPX (rows padded to 4-bytes)
//
//////////////////////////////////////////////////////////

#define R 0
#define G 1
#define B 2

CLineEngRGB12::
CLineEngRGB12()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngRGB12::
~CLineEngRGB12()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngRGB12::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                         )
{
   dwide   = w;
   dhigh   = h;

   // this format is row-padded, so it has a
   // well-defined pitch and no phase change
   // with vertical movement
   dpitch  = ((w*36+31)/32)*4;
   //dphase  = w%8;

   // we only support a non-interlaced frame buffer
   intrlc  = false;

   // calculate the frame buffer size
   fldSize = dpitch*h;

   // basic font dimensions
   setFontSize(1);
}

int CLineEngRGB12::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngRGB12::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngRGB12::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[R] = 0;
         fgColor[G] = 0;
         fgColor[B] = 0;
         fgndColor = col;
      break;

      case 1:
         fgColor[R] = 0xfff;
         fgColor[G] = 0xfff;
         fgColor[B] = 0xfff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   fgColor[R] = r>>4;
   fgColor[G] = g>>4;
   fgColor[B] = b>>4;
}

void CLineEngRGB12::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[R] = 0;
         bgColor[G] = 0;
         bgColor[B] = 0;
         bgndColor = col;
      break;

      case 1:
         bgColor[R] = 0xfff;
         bgColor[G] = 0xfff;
         bgColor[B] = 0xfff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   bgColor[R] = r>>4;
   bgColor[G] = g>>4;
   bgColor[B] = b>>4;
}

void CLineEngRGB12::
setFGY0()
{
   curptr[0] = fgColor[B];
   curptr[1] = (fgColor[G]>>4);
   curptr[2] = (fgColor[G]<<4)+(fgColor[R]>>8);;
   curptr[3] = fgColor[R];
   curptr[7] = (curptr[7]&0xf0)+(fgColor[B]>>8);
   return;
}

void CLineEngRGB12::
setFGY1()
{
   curptr[4]  = (fgColor[B]<<4)+(fgColor[G]>>8);
   curptr[5]  = (fgColor[G]);
   curptr[6]  = (fgColor[R]>>4);
   curptr[7]  = (fgColor[R]<<4)+(curptr[7]&0xf);
   curptr[11] = (fgColor[B]>>4);
   return;
}

void CLineEngRGB12::
setFGY2()
{
   curptr[8]  = (fgColor[G]>>4);
   curptr[9]  = (fgColor[G]<<4)+(fgColor[R]>>8);
   curptr[10] = fgColor[R];
   curptr[14] = (curptr[14]&0xf0)+(fgColor[B]>>8);
   curptr[15] = fgColor[B];
   return;
}

void CLineEngRGB12::
setFGY3()
{
   curptr[12] = fgColor[G];
   curptr[13] = (fgColor[R]>>4);
   curptr[14] = (fgColor[R]<<4)+(curptr[14]&0xf);
   curptr[18] = (fgColor[B]>>4);
   curptr[19] = (fgColor[B]<<4)+(fgColor[G]>>8);
   return;
}

void CLineEngRGB12::
setFGY4()
{
   curptr[16] = (fgColor[G]<<4)+(fgColor[R]>>8);
   curptr[17] = fgColor[R];
   curptr[21] = (curptr[21]&0xf0)+(fgColor[B]>>8);
   curptr[22] = fgColor[B];
   curptr[23] = (fgColor[G]>>4);
   return;
}

void CLineEngRGB12::
setFGY5()
{
   curptr[20] = (fgColor[R]>>4);
   curptr[21] = (fgColor[R]<<4)+(curptr[21]&0xf);
   curptr[25] = (fgColor[B]>>4);
   curptr[26] = (fgColor[B]<<4)+(fgColor[G]>>8);
   curptr[27] = fgColor[G];
   return;
}

void CLineEngRGB12::
setFGY6()
{
   curptr[24] = fgColor[R];
   curptr[28] = (curptr[28]&0xf0)+(fgColor[B]>>8);
   curptr[29] = fgColor[B];
   curptr[30] = (fgColor[G]>>4);
   curptr[31] = (fgColor[G]<<4)+(fgColor[R]>>8);
   return;
}

void CLineEngRGB12::
setFGY7()
{
   curptr[28] = (fgColor[R]<<4)+(curptr[28]&0xf);
   curptr[32] = (fgColor[B]>>4);
   curptr[33] = (fgColor[B]<<4)+(fgColor[G]>>8);
   curptr[34] = fgColor[G];
   curptr[35] = (fgColor[R]>>4);
   return;
}

void CLineEngRGB12::
setBGY0()
{
   curptr[0] = bgColor[B];
   curptr[1] = (bgColor[G]>>4);
   curptr[2] = (bgColor[G]<<4)+(bgColor[R]>>8);;
   curptr[3] = bgColor[R];
   curptr[7] = (curptr[7]&0xf0)+(bgColor[B]>>8);
   return;
}

void CLineEngRGB12::
setBGY1()
{
   curptr[4]  = (bgColor[B]<<4)+(bgColor[G]>>8);
   curptr[5]  = (bgColor[G]);
   curptr[6]  = (bgColor[R]>>4);
   curptr[7]  = (bgColor[R]<<4)+(curptr[7]&0xf);
   curptr[11] = (bgColor[B]>>4);
   return;
}

void CLineEngRGB12::
setBGY2()
{
   curptr[8]  = (bgColor[G]>>4);
   curptr[9]  = (bgColor[G]<<4)+(bgColor[R]>>8);
   curptr[10] = bgColor[R];
   curptr[14] = (curptr[14]&0xf0)+(bgColor[B]>>8);
   curptr[15] = bgColor[B];
   return;
}

void CLineEngRGB12::
setBGY3()
{
   curptr[12] = bgColor[G];
   curptr[13] = (bgColor[R]>>4);
   curptr[14] = (bgColor[R]<<4)+(curptr[14]&0xf);
   curptr[18] = (bgColor[B]>>4);
   curptr[19] = (bgColor[B]<<4)+(bgColor[G]>>8);
   return;
}

void CLineEngRGB12::
setBGY4()
{
   curptr[16] = (bgColor[G]<<4)+(bgColor[R]>>8);
   curptr[17] = bgColor[R];
   curptr[21] = (curptr[21]&0xf0)+(bgColor[B]>>8);
   curptr[22] = bgColor[B];
   curptr[23] = (bgColor[G]>>4);
   return;
}

void CLineEngRGB12::
setBGY5()
{
   curptr[20] = (bgColor[R]>>4);
   curptr[21] = (bgColor[R]<<4)+(curptr[21]&0xf);
   curptr[25] = (bgColor[B]>>4);
   curptr[26] = (bgColor[B]<<4)+(bgColor[G]>>8);
   curptr[27] = bgColor[G];
   return;
}

void CLineEngRGB12::
setBGY6()
{
   curptr[24] = bgColor[R];
   curptr[28] = (curptr[28]&0xf0)+(bgColor[B]>>8);
   curptr[29] = bgColor[B];
   curptr[30] = (bgColor[G]>>4);
   curptr[31] = (bgColor[G]<<4)+(bgColor[R]>>8);
   return;
}

void CLineEngRGB12::
setBGY7()
{
   curptr[28] = (bgColor[R]<<4)+(curptr[28]&0xf);
   curptr[32] = (bgColor[B]>>4);
   curptr[33] = (bgColor[B]<<4)+(bgColor[G]>>8);
   curptr[34] = bgColor[G];
   curptr[35] = (bgColor[R]>>4);
   return;
}

void CLineEngRGB12::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   curptr = frameBuffer[0] + y*dpitch + (x/8)*36;
   rgbphase = x%8;
}

void CLineEngRGB12::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen,pel;
    int *rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt

         if (rgbphase==0) goto hr0;
         if (rgbphase==1) goto hr1;
         if (rgbphase==2) goto hr2;
         if (rgbphase==3) goto hr3;
         if (rgbphase==4) goto hr4;
         if (rgbphase==5) goto hr5;
         if (rgbphase==6) goto hr6;
         goto hr7;

         while (true) {

	    hr0:;

               if (delx--==0) break;
               setFGY0();
               curx++;

	    hr1:;

               if (delx--==0) break;
               setFGY1();
               curx++;

	    hr2:;

               if (delx--==0) break;
               setFGY2();
               curx++;

	    hr3:;

               if (delx--==0) break;
               setFGY3();
               curx++;

	    hr4:;

               if (delx--==0) break;
               setFGY4();
               curx++;

	    hr5:;

               if (delx--==0) break;
               setFGY5();
               curx++;

	    hr6:;

               if (delx--==0) break;
               setFGY6();
               curx++;

	    hr7:;

               if (delx--==0) break;
               setFGY7();
               curx++;

               curptr += 36;
         }
      }
      else  { // lft

         if (rgbphase==7) goto hl7;
         if (rgbphase==6) goto hl6;
         if (rgbphase==5) goto hl5;
         if (rgbphase==4) goto hl4;
         if (rgbphase==3) goto hl3;
         if (rgbphase==2) goto hl2;
         if (rgbphase==1) goto hl1;
         goto hl0;

         while (true) {

	    hl7:;

               if (delx++==0) break;
               setFGY7();
               curx--;

	    hl6:;

               if (delx++==0) break;
               setFGY6();
               curx--;

	    hl5:;

               if (delx++==0) break;
               setFGY5();
               curx--;

	    hl4:;

               if (delx++==0) break;
               setFGY4();
               curx--;

	    hl3:;

               if (delx++==0) break;
               setFGY3();
               curx--;

	    hl2:;

               if (delx++==0) break;
               setFGY2();
               curx--;

	    hl1:

               if (delx++==0) break;
               setFGY1();
               curx--;

	    hl0:

               if (delx++==0) break;
               setFGY0();
               curx--;

               curptr -= 36;
         }
      }
   }
   else {

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

	    if (rgbphase==0) {
	       while (dely--) {
                  setFGY0();
                  curptr += dpitch;
               }
            }
	    else if (rgbphase==1) {
	       while (dely--) {
                  setFGY1();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==2) {
	       while (dely--) {
                  setFGY2();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==3) {
	       while (dely--) {
                  setFGY3();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==4) {
	       while (dely--) {
                  setFGY4();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==5) {
	       while (dely--) {
                  setFGY5();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==6) {
	       while (dely--) {
                  setFGY6();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==7) {
	       while (dely--) {
                  setFGY7();
                  curptr += dpitch;
               }
            }
         }
         else { // up

	    if (rgbphase==0) {
	       while (dely++) {
                  setFGY0();
                  curptr -= dpitch;
               }
            }
	    else if (rgbphase==1) {
	       while (dely++) {
                  setFGY1();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==2) {
	       while (dely++) {
                  setFGY2();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==3) {
	       while (dely++) {
                  setFGY3();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==4) {
	       while (dely++) {
                  setFGY4();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==5) {
	       while (dely++) {
                  setFGY5();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==6) {
	       while (dely++) {
                  setFGY6();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==7) {
	       while (dely++) {
                  setFGY7();
                  curptr -= dpitch;
               }
            }
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow

                  pen = delx;

                  if (rgbphase==0) goto drsh0;
                  if (rgbphase==1) goto drsh1;
                  if (rgbphase==2) goto drsh2;
                  if (rgbphase==3) goto drsh3;
                  if (rgbphase==4) goto drsh4;
                  if (rgbphase==5) goto drsh5;
                  if (rgbphase==6) goto drsh6;
                  goto drsh7;

                  while (true) {

                     drsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                        curx += 8;
                        curptr += 36;

                  }
               }
               else { // dn-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else setFGY7();

                     curptr += dpitch;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx++;
                        rgbphase++;
                        if (rgbphase == 8) {
                           rgbphase = 0;;
                           curptr += 36;
                        }
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow

                  pen = delx;

                  if (rgbphase==7) goto dlsh7;
                  if (rgbphase==6) goto dlsh6;
                  if (rgbphase==5) goto dlsh5;
                  if (rgbphase==4) goto dlsh4;
                  if (rgbphase==3) goto dlsh3;
                  if (rgbphase==2) goto dlsh2;
                  if (rgbphase==1) goto dlsh1;
                  goto dlsh0;

                  while (true) {

                     dlsh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                        curx -= 8;
                        curptr -= 36;
                  }
               }
               else { // dn-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else setFGY7();

                     curptr += dpitch;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx--;
                        rgbphase--;
                        if (rgbphase == -1) {
                           rgbphase = 7;
                           curptr -= 36;
                        }
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow

                  pen = delx;

                  if (rgbphase==0) goto ursh0;
                  if (rgbphase==1) goto ursh1;
                  if (rgbphase==2) goto ursh2;
                  if (rgbphase==3) goto ursh3;
                  if (rgbphase==4) goto ursh4;
                  if (rgbphase==5) goto ursh5;
                  if (rgbphase==6) goto ursh6;
                  goto ursh7;

                  while (true) {

                     ursh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                        curx += 8;
                        curptr += 36;
                  }
               }
               else { // up-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else if (rgbphase==7) setFGY7();

                     curptr -= dpitch;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx++;
                        rgbphase++;
                        if (rgbphase == 8) {
                           rgbphase = 0;;
                           curptr += 36;
                        }
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow

                  pen = delx;

                  if (rgbphase==7) goto ulsh7;
                  if (rgbphase==6) goto ulsh6;
                  if (rgbphase==5) goto ulsh5;
                  if (rgbphase==4) goto ulsh4;
                  if (rgbphase==3) goto ulsh3;
                  if (rgbphase==2) goto ulsh2;
                  if (rgbphase==1) goto ulsh1;
                  goto ulsh0;

                  while (true) {

                     ulsh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                        curx -= 8;
                        curptr -= 36;
                  }

               }
               else { // up-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else if (rgbphase==7) setFGY7();

                     curptr -= dpitch;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx--;
                        rgbphase--;
                        if (rgbphase == -1) {
                           rgbphase = 7;
                           curptr-= 36;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   movTo(x, y);
}

void CLineEngRGB12::
drawDot()
{
   switch(rgbphase) {

      case 0:
         setFGY0();
      break;

      case 1:
         setFGY1();
      break;

      case 2:
         setFGY2();
      break;

      case 3:
         setFGY3();
      break;

      case 4:
         setFGY4();
      break;

      case 5:
         setFGY5();
      break;

      case 6:
         setFGY6();
      break;

      case 7:
         setFGY7();
      break;
   }
}

void CLineEngRGB12::
drawSpot(int x, int y)
{
   movTo(x&0xfffffffe,y);

   switch(rgbphase) {

      case 0:

         setFGY0();
         setFGY1();

      break;

      case 1:

         setFGY1();
         setFGY2();

      break;

      case 2:

         setFGY2();
         setFGY3();

      break;

      case 3:

         setFGY3();
         setFGY4();

      break;

      case 4:

         setFGY4();
         setFGY5();

      break;

      case 5:

         setFGY5();
         setFGY6();

      break;

      case 6:

         setFGY6();
         setFGY7();

      break;

      case 7:

         setFGY7();
         curptr += 16;
         setFGY0();
         curptr -= 16;

      break;
   }
}


void CLineEngRGB12::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   for (int i=top;i<=bot;i++) {
      movTo(lft  , i);
      linTo(rgt+1, i);
   }
}

void CLineEngRGB12::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         movTo(x, cury);

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  if      (rgbphase==0) setFGY0();
                  else if (rgbphase==1) setFGY1();
                  else if (rgbphase==2) setFGY2();
                  else if (rgbphase==3) setFGY3();
                  else if (rgbphase==4) setFGY4();
                  else if (rgbphase==5) setFGY5();
                  else if (rgbphase==6) setFGY6();
                  else                  setFGY7();

                  rgbphase++;
                  if (rgbphase==8) {
                     rgbphase=0;
                     curptr += 36;
                  }
               }
            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     if (rgbphase==0) setBGY0();
                     else if (rgbphase==1) setBGY1();
                     else if (rgbphase==2) setBGY2();
                     else if (rgbphase==3) setBGY3();
                     else if (rgbphase==4) setBGY4();
                     else if (rgbphase==5) setBGY5();
                     else if (rgbphase==6) setBGY6();
                     else setBGY7();
                  }

                  rgbphase++;
                  if (rgbphase==8) {
                     rgbphase=0;
                     curptr += 36;
                  }
               }
            }

            fontscan = fontscan >> 1;

         }

         curptr += dpitch;
      }
   }
}

//////////////////////////////////////////////////////////
//
//		RGB 12-bit DPX (rows padded to 4-bytes)
//              REVERSE of SPEC
//
//////////////////////////////////////////////////////////

#define R 0
#define G 1
#define B 2

CLineEngRGB12LR::
CLineEngRGB12LR()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngRGB12LR::
~CLineEngRGB12LR()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngRGB12LR::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                         )
{
   dwide   = w;
   dhigh   = h;

   // this format is row-padded, so it has a
   // well-defined pitch and no phase change
   // with vertical movement
   dpitch  = ((w*36+31)/32)*4;
   //dphase  = w%8;

   // we only support a non-interlaced frame buffer
   intrlc  = false;

   // calculate the frame buffer size
   fldSize = dpitch*h;

   // basic font dimensions
   setFontSize(1);
}

int CLineEngRGB12LR::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngRGB12LR::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngRGB12LR::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[R] = 0;
         fgColor[G] = 0;
         fgColor[B] = 0;
         fgndColor = col;
      break;

      case 1:
         fgColor[R] = 0xfff;
         fgColor[G] = 0xfff;
         fgColor[B] = 0xfff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12LR::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   fgColor[R] = r>>4;
   fgColor[G] = g>>4;
   fgColor[B] = b>>4;
}

void CLineEngRGB12LR::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[R] = 0;
         bgColor[G] = 0;
         bgColor[B] = 0;
         bgndColor = col;
      break;

      case 1:
         bgColor[R] = 0xfff;
         bgColor[G] = 0xfff;
         bgColor[B] = 0xfff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12LR::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   bgColor[R] = r>>4;
   bgColor[G] = g>>4;
   bgColor[B] = b>>4;
}

void CLineEngRGB12LR::
setFGY0()
{
   curptr[0] = (fgColor[R]>>4);
   curptr[1] = (fgColor[R]<<4)+(fgColor[G]>>8);
   curptr[2] = fgColor[G];
   curptr[3] = (fgColor[B]>>4);
   curptr[4] = (fgColor[B]<<4)+(curptr[4]&0x0f);
   return;
}

void CLineEngRGB12LR::
setFGY1()
{
   curptr[4] = (curptr[4]&0xf0)+(fgColor[R]>>8);
   curptr[5] = fgColor[R];
   curptr[6] = (fgColor[G]>>4);
   curptr[7] = (fgColor[G]<<4)+(fgColor[B]>>8);
   curptr[8] = fgColor[B];
   return;
}

void CLineEngRGB12LR::
setFGY2()
{
   curptr[9]  = (fgColor[R]>>4);
   curptr[10] = (fgColor[R]<<4)+(fgColor[G]>>8);
   curptr[11] = fgColor[G];
   curptr[12] = (fgColor[B]>>4);
   curptr[13] = (fgColor[B]<<4)+(curptr[13]&0x0f);
   return;
}

void CLineEngRGB12LR::
setFGY3()
{
   curptr[13] = (curptr[13]&0xf0)+(fgColor[R]>>8);
   curptr[14] = fgColor[R];
   curptr[15] = (fgColor[G]>>4);
   curptr[16] = (fgColor[G]<<4)+(fgColor[B]>>8);
   curptr[17] = fgColor[B];
   return;
}

void CLineEngRGB12LR::
setFGY4()
{
   curptr[18] = (fgColor[R]>>4);
   curptr[19] = (fgColor[R]<<4)+(fgColor[G]>>8);
   curptr[20] = fgColor[G];
   curptr[21] = (fgColor[B]>>4);
   curptr[22] = (fgColor[B]<<4)+(curptr[22]&0x0f);
   return;
}

void CLineEngRGB12LR::
setFGY5()
{
   curptr[22] = (curptr[22]&0xf0)+(fgColor[R]>>8);
   curptr[23] = fgColor[R];
   curptr[24] = (fgColor[G]>>4);
   curptr[25] = (fgColor[G]<<4)+(fgColor[B]>>8);
   curptr[26] = fgColor[B];
   return;
}

void CLineEngRGB12LR::
setFGY6()
{
   curptr[27] = (fgColor[R]>>4);
   curptr[28] = (fgColor[R]<<4)+(fgColor[G]>>8);
   curptr[29] = fgColor[G];
   curptr[30] = (fgColor[B]>>4);
   curptr[31] = (fgColor[B]<<4)+(curptr[31]&0x0f);
   return;
}

void CLineEngRGB12LR::
setFGY7()
{
   curptr[31] = (curptr[31]&0xf0)+(fgColor[R]>>8);
   curptr[32] = fgColor[R];
   curptr[33] = (fgColor[G]>>4);
   curptr[34] = (fgColor[G]<<4)+(fgColor[B]>>8);
   curptr[35] = fgColor[B];
   return;
}

void CLineEngRGB12LR::
setBGY0()
{
   curptr[0] = (bgColor[R]>>4);
   curptr[1] = (bgColor[R]<<4)+(bgColor[G]>>8);
   curptr[2] = bgColor[G];
   curptr[3] = (bgColor[B]>>4);
   curptr[4] = (bgColor[B]<<4)+(curptr[4]&0x0f);
   return;
}

void CLineEngRGB12LR::
setBGY1()
{
   curptr[4] = (curptr[4]&0xf0)+(bgColor[R]>>8);
   curptr[5] = bgColor[R];
   curptr[6] = (bgColor[G]>>4);
   curptr[7] = (bgColor[G]<<4)+(bgColor[B]>>8);
   curptr[8] = bgColor[B];
   return;
}

void CLineEngRGB12LR::
setBGY2()
{
   curptr[9]  = (bgColor[R]>>4);
   curptr[10] = (bgColor[R]<<4)+(bgColor[G]>>8);
   curptr[11] = bgColor[G];
   curptr[12] = (bgColor[B]>>4);
   curptr[13] = (bgColor[B]<<4)+(curptr[13]&0x0f);
   return;
}

void CLineEngRGB12LR::
setBGY3()
{
   curptr[13] = (curptr[13]&0xf0)+(bgColor[R]>>8);
   curptr[14] = bgColor[R];
   curptr[15] = (bgColor[G]>>4);
   curptr[16] = (bgColor[G]<<4)+(bgColor[B]>>8);
   curptr[17] = bgColor[B];
   return;
}

void CLineEngRGB12LR::
setBGY4()
{
   curptr[18] = (bgColor[R]>>4);
   curptr[19] = (bgColor[R]<<4)+(bgColor[G]>>8);
   curptr[20] = bgColor[G];
   curptr[21] = (bgColor[B]>>4);
   curptr[22] = (bgColor[B]<<4)+(curptr[22]&0x0f);
   return;
}

void CLineEngRGB12LR::
setBGY5()
{
   curptr[22] = (curptr[22]&0xf0)+(bgColor[R]>>8);
   curptr[23] = bgColor[R];
   curptr[24] = (bgColor[G]>>4);
   curptr[25] = (bgColor[G]<<4)+(bgColor[B]>>8);
   curptr[26] = bgColor[B];
   return;
}

void CLineEngRGB12LR::
setBGY6()
{
   curptr[27] = (bgColor[R]>>4);
   curptr[28] = (bgColor[R]<<4)+(bgColor[G]>>8);
   curptr[29] = bgColor[G];
   curptr[30] = (bgColor[B]>>4);
   curptr[31] = (bgColor[B]<<4)+(curptr[31]&0x0f);
   return;
}

void CLineEngRGB12LR::
setBGY7()
{
   curptr[31] = (curptr[31]&0xf0)+(bgColor[R]>>8);
   curptr[32] = bgColor[R];
   curptr[33] = (bgColor[G]>>4);
   curptr[34] = (bgColor[G]<<4)+(bgColor[B]>>8);
   curptr[35] = bgColor[B];
   return;
}

void CLineEngRGB12LR::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   curptr = frameBuffer[0] + y*dpitch + (x/8)*36;
   rgbphase = x%8;
}

void CLineEngRGB12LR::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen,pel;
    int *rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt

         if (rgbphase==0) goto hr0;
         if (rgbphase==1) goto hr1;
         if (rgbphase==2) goto hr2;
         if (rgbphase==3) goto hr3;
         if (rgbphase==4) goto hr4;
         if (rgbphase==5) goto hr5;
         if (rgbphase==6) goto hr6;
         goto hr7;

         while (true) {

	    hr0:;

               if (delx--==0) break;
               setFGY0();
               curx++;

	    hr1:;

               if (delx--==0) break;
               setFGY1();
               curx++;

	    hr2:;

               if (delx--==0) break;
               setFGY2();
               curx++;

	    hr3:;

               if (delx--==0) break;
               setFGY3();
               curx++;

	    hr4:;

               if (delx--==0) break;
               setFGY4();
               curx++;

	    hr5:;

               if (delx--==0) break;
               setFGY5();
               curx++;

	    hr6:;

               if (delx--==0) break;
               setFGY6();
               curx++;

	    hr7:;

               if (delx--==0) break;
               setFGY7();
               curx++;

               curptr += 36;
         }
      }
      else  { // lft

         if (rgbphase==7) goto hl7;
         if (rgbphase==6) goto hl6;
         if (rgbphase==5) goto hl5;
         if (rgbphase==4) goto hl4;
         if (rgbphase==3) goto hl3;
         if (rgbphase==2) goto hl2;
         if (rgbphase==1) goto hl1;
         goto hl0;

         while (true) {

	    hl7:;

               if (delx++==0) break;
               setFGY7();
               curx--;

	    hl6:;

               if (delx++==0) break;
               setFGY6();
               curx--;

	    hl5:;

               if (delx++==0) break;
               setFGY5();
               curx--;

	    hl4:;

               if (delx++==0) break;
               setFGY4();
               curx--;

	    hl3:;

               if (delx++==0) break;
               setFGY3();
               curx--;

	    hl2:;

               if (delx++==0) break;
               setFGY2();
               curx--;

	    hl1:

               if (delx++==0) break;
               setFGY1();
               curx--;

	    hl0:

               if (delx++==0) break;
               setFGY0();
               curx--;

               curptr -= 36;
         }
      }
   }
   else {

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

	    if (rgbphase==0) {
	       while (dely--) {
                  setFGY0();
                  curptr += dpitch;
               }
            }
	    else if (rgbphase==1) {
	       while (dely--) {
                  setFGY1();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==2) {
	       while (dely--) {
                  setFGY2();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==3) {
	       while (dely--) {
                  setFGY3();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==4) {
	       while (dely--) {
                  setFGY4();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==5) {
	       while (dely--) {
                  setFGY5();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==6) {
	       while (dely--) {
                  setFGY6();
                  curptr += dpitch;
               }
            }
            else if (rgbphase==7) {
	       while (dely--) {
                  setFGY7();
                  curptr += dpitch;
               }
            }
         }
         else { // up

	    if (rgbphase==0) {
	       while (dely++) {
                  setFGY0();
                  curptr -= dpitch;
               }
            }
	    else if (rgbphase==1) {
	       while (dely++) {
                  setFGY1();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==2) {
	       while (dely++) {
                  setFGY2();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==3) {
	       while (dely++) {
                  setFGY3();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==4) {
	       while (dely++) {
                  setFGY4();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==5) {
	       while (dely++) {
                  setFGY5();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==6) {
	       while (dely++) {
                  setFGY6();
                  curptr -= dpitch;
               }
            }
            else if (rgbphase==7) {
	       while (dely++) {
                  setFGY7();
                  curptr -= dpitch;
               }
            }
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow

                  pen = delx;

                  if (rgbphase==0) goto drsh0;
                  if (rgbphase==1) goto drsh1;
                  if (rgbphase==2) goto drsh2;
                  if (rgbphase==3) goto drsh3;
                  if (rgbphase==4) goto drsh4;
                  if (rgbphase==5) goto drsh5;
                  if (rgbphase==6) goto drsh6;
                  goto drsh7;

                  while (true) {

                     drsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     drsh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                        curx += 8;
                        curptr += 36;

                  }
               }
               else { // dn-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else setFGY7();

                     curptr += dpitch;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx++;
                        rgbphase++;
                        if (rgbphase == 8) {
                           rgbphase = 0;;
                           curptr += 36;
                        }
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow

                  pen = delx;

                  if (rgbphase==7) goto dlsh7;
                  if (rgbphase==6) goto dlsh6;
                  if (rgbphase==5) goto dlsh5;
                  if (rgbphase==4) goto dlsh4;
                  if (rgbphase==3) goto dlsh3;
                  if (rgbphase==2) goto dlsh2;
                  if (rgbphase==1) goto dlsh1;
                  goto dlsh0;

                  while (true) {

                     dlsh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                     dlsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                        }

                        curx -= 8;
                        curptr -= 36;
                  }
               }
               else { // dn-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else setFGY7();

                     curptr += dpitch;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx--;
                        rgbphase--;
                        if (rgbphase == -1) {
                           rgbphase = 7;
                           curptr -= 36;
                        }
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow

                  pen = delx;

                  if (rgbphase==0) goto ursh0;
                  if (rgbphase==1) goto ursh1;
                  if (rgbphase==2) goto ursh2;
                  if (rgbphase==3) goto ursh3;
                  if (rgbphase==4) goto ursh4;
                  if (rgbphase==5) goto ursh5;
                  if (rgbphase==6) goto ursh6;
                  goto ursh7;

                  while (true) {

                     ursh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ursh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                        curx += 8;
                        curptr += 36;
                  }
               }
               else { // up-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else if (rgbphase==7) setFGY7();

                     curptr -= dpitch;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx++;
                        rgbphase++;
                        if (rgbphase == 8) {
                           rgbphase = 0;;
                           curptr += 36;
                        }
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow

                  pen = delx;

                  if (rgbphase==7) goto ulsh7;
                  if (rgbphase==6) goto ulsh6;
                  if (rgbphase==5) goto ulsh5;
                  if (rgbphase==4) goto ulsh4;
                  if (rgbphase==3) goto ulsh3;
                  if (rgbphase==2) goto ulsh2;
                  if (rgbphase==1) goto ulsh1;
                  goto ulsh0;

                  while (true) {

                     ulsh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                     ulsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                        }

                        curx -= 8;
                        curptr -= 36;
                  }

               }
               else { // up-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else if (rgbphase==7) setFGY7();

                     curptr -= dpitch;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx--;
                        rgbphase--;
                        if (rgbphase == -1) {
                           rgbphase = 7;
                           curptr-= 36;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   movTo(x, y);
}

void CLineEngRGB12LR::
drawDot()
{
   switch(rgbphase) {

      case 0:
         setFGY0();
      break;

      case 1:
         setFGY1();
      break;

      case 2:
         setFGY2();
      break;

      case 3:
         setFGY3();
      break;

      case 4:
         setFGY4();
      break;

      case 5:
         setFGY5();
      break;

      case 6:
         setFGY6();
      break;

      case 7:
         setFGY7();
      break;
   }
}

void CLineEngRGB12LR::
drawSpot(int x, int y)
{
   movTo(x&0xfffffffe,y);

   switch(rgbphase) {

      case 0:

         setFGY0();
         setFGY1();

      break;

      case 1:

         setFGY1();
         setFGY2();

      break;

      case 2:

         setFGY2();
         setFGY3();

      break;

      case 3:

         setFGY3();
         setFGY4();

      break;

      case 4:

         setFGY4();
         setFGY5();

      break;

      case 5:

         setFGY5();
         setFGY6();

      break;

      case 6:

         setFGY6();
         setFGY7();

      break;

      case 7:

         setFGY7();
         curptr += 16;
         setFGY0();
         curptr -= 16;

      break;
   }
}


void CLineEngRGB12LR::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   for (int i=top;i<=bot;i++) {
      movTo(lft  , i);
      linTo(rgt+1, i);
   }
}

void CLineEngRGB12LR::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         movTo(x, cury);

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  if      (rgbphase==0) setFGY0();
                  else if (rgbphase==1) setFGY1();
                  else if (rgbphase==2) setFGY2();
                  else if (rgbphase==3) setFGY3();
                  else if (rgbphase==4) setFGY4();
                  else if (rgbphase==5) setFGY5();
                  else if (rgbphase==6) setFGY6();
                  else                  setFGY7();

                  rgbphase++;
                  if (rgbphase==8) {
                     rgbphase=0;
                     curptr += 36;
                  }
               }
            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     if (rgbphase==0) setBGY0();
                     else if (rgbphase==1) setBGY1();
                     else if (rgbphase==2) setBGY2();
                     else if (rgbphase==3) setBGY3();
                     else if (rgbphase==4) setBGY4();
                     else if (rgbphase==5) setBGY5();
                     else if (rgbphase==6) setBGY6();
                     else setBGY7();
                  }

                  rgbphase++;
                  if (rgbphase==8) {
                     rgbphase=0;
                     curptr += 36;
                  }
               }
            }

            fontscan = fontscan >> 1;

         }

         curptr += dpitch;
      }
   }
}

//////////////////////////////////////////////////////////
//
//		RGB 12-bit DPX packed
//
//////////////////////////////////////////////////////////

#define R 0
#define G 1
#define B 2

CLineEngRGB12P::
CLineEngRGB12P()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngRGB12P::
~CLineEngRGB12P()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngRGB12P::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                         )
{
   dwide   = w;
   dhigh   = h;

   // this format is row-packed, so we'll want the
   // byte-length and phase-penalty for each row in
   // order to do the line algorithm incrementally
   dpitch  = (w/8)*36;
   dphase  = w%8;

   // we only support a non-interlaced frame buffer
   intrlc  = false;

   // calculate the frame buffer size
   fldSize = ((w*h+7)/8)*36;

   // basic font dimensions
   setFontSize(1);
}

int CLineEngRGB12P::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngRGB12P::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngRGB12P::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[R] = 0;
         fgColor[G] = 0;
         fgColor[B] = 0;
         fgndColor = col;
      break;

      case 1:
         fgColor[R] = 0xfff;
         fgColor[G] = 0xfff;
         fgColor[B] = 0xfff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12P::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   fgColor[R] = r>>4;
   fgColor[G] = g>>4;
   fgColor[B] = b>>4;
}

void CLineEngRGB12P::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[R] = 0;
         bgColor[G] = 0;
         bgColor[B] = 0;
         bgndColor = col;
      break;

      case 1:
         bgColor[R] = 0xfff;
         bgColor[G] = 0xfff;
         bgColor[B] = 0xfff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12P::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   bgColor[R] = r>>4;
   bgColor[G] = g>>4;
   bgColor[B] = b>>4;
}

void CLineEngRGB12P::
setFGY0()
{
   curptr[0] = fgColor[B];
   curptr[1] = (fgColor[G]>>4);
   curptr[2] = (fgColor[G]<<4)+(fgColor[R]>>8);;
   curptr[3] = fgColor[R];
   curptr[7] = (curptr[7]&0xf0)+(fgColor[B]>>8);
   return;
}

void CLineEngRGB12P::
setFGY1()
{
   curptr[4]  = (fgColor[B]<<4)+(fgColor[G]>>8);
   curptr[5]  = (fgColor[G]);
   curptr[6]  = (fgColor[R]>>4);
   curptr[7]  = (fgColor[R]<<4)+(curptr[7]&0xf);
   curptr[11] = (fgColor[B]>>4);
   return;
}

void CLineEngRGB12P::
setFGY2()
{
   curptr[8]  = (fgColor[G]>>4);
   curptr[9]  = (fgColor[G]<<4)+(fgColor[R]>>8);
   curptr[10] = fgColor[R];
   curptr[14] = (curptr[14]&0xf0)+(fgColor[B]>>8);
   curptr[15] = fgColor[B];
   return;
}

void CLineEngRGB12P::
setFGY3()
{
   curptr[12] = fgColor[G];
   curptr[13] = (fgColor[R]>>4);
   curptr[14] = (fgColor[R]<<4)+(curptr[14]&0xf);
   curptr[18] = (fgColor[B]>>4);
   curptr[19] = (fgColor[B]<<4)+(fgColor[G]>>8);
   return;
}

void CLineEngRGB12P::
setFGY4()
{
   curptr[16] = (fgColor[G]<<4)+(fgColor[R]>>8);
   curptr[17] = fgColor[R];
   curptr[21] = (curptr[21]&0xf0)+(fgColor[B]>>8);
   curptr[22] = fgColor[B];
   curptr[23] = (fgColor[G]>>4);
   return;
}

void CLineEngRGB12P::
setFGY5()
{
   curptr[20] = (fgColor[R]>>4);
   curptr[21] = (fgColor[R]<<4)+(curptr[21]&0xf);
   curptr[25] = (fgColor[B]>>4);
   curptr[26] = (fgColor[B]<<4)+(fgColor[G]>>8);
   curptr[27] = fgColor[G];
   return;
}

void CLineEngRGB12P::
setFGY6()
{
   curptr[24] = fgColor[R];
   curptr[28] = (curptr[28]&0xf0)+(fgColor[B]>>8);
   curptr[29] = fgColor[B];
   curptr[30] = (fgColor[G]>>4);
   curptr[31] = (fgColor[G]<<4)+(fgColor[R]>>8);
   return;
}

void CLineEngRGB12P::
setFGY7()
{
   curptr[28] = (fgColor[R]<<4)+(curptr[28]&0xf);
   curptr[32] = (fgColor[B]>>4);
   curptr[33] = (fgColor[B]<<4)+(fgColor[G]>>8);
   curptr[34] = fgColor[G];
   curptr[35] = (fgColor[R]>>4);
   return;
}

void CLineEngRGB12P::
setBGY0()
{
   curptr[0] = bgColor[B];
   curptr[1] = (bgColor[G]>>4);
   curptr[2] = (bgColor[G]<<4)+(bgColor[R]>>8);;
   curptr[3] = bgColor[R];
   curptr[7] = (curptr[7]&0xf0)+(bgColor[B]>>8);
   return;
}

void CLineEngRGB12P::
setBGY1()
{
   curptr[4]  = (bgColor[B]<<4)+(bgColor[G]>>8);
   curptr[5]  = (bgColor[G]);
   curptr[6]  = (bgColor[R]>>4);
   curptr[7]  = (bgColor[R]<<4)+(curptr[7]&0xf);
   curptr[11] = (bgColor[B]>>4);
   return;
}

void CLineEngRGB12P::
setBGY2()
{
   curptr[8]  = (bgColor[G]>>4);
   curptr[9]  = (bgColor[G]<<4)+(bgColor[R]>>8);
   curptr[10] = bgColor[R];
   curptr[14] = (curptr[14]&0xf0)+(bgColor[B]>>8);
   curptr[15] = bgColor[B];
   return;
}

void CLineEngRGB12P::
setBGY3()
{
   curptr[12] = bgColor[G];
   curptr[13] = (bgColor[R]>>4);
   curptr[14] = (bgColor[R]<<4)+(curptr[14]&0xf);
   curptr[18] = (bgColor[B]>>4);
   curptr[19] = (bgColor[B]<<4)+(bgColor[G]>>8);
   return;
}

void CLineEngRGB12P::
setBGY4()
{
   curptr[16] = (bgColor[G]<<4)+(bgColor[R]>>8);
   curptr[17] = bgColor[R];
   curptr[21] = (curptr[21]&0xf0)+(bgColor[B]>>8);
   curptr[22] = bgColor[B];
   curptr[23] = (bgColor[G]>>4);
   return;
}

void CLineEngRGB12P::
setBGY5()
{
   curptr[20] = (bgColor[R]>>4);
   curptr[21] = (bgColor[R]<<4)+(curptr[21]&0xf);
   curptr[25] = (bgColor[B]>>4);
   curptr[26] = (bgColor[B]<<4)+(bgColor[G]>>8);
   curptr[27] = bgColor[G];
   return;
}

void CLineEngRGB12P::
setBGY6()
{
   curptr[24] = bgColor[R];
   curptr[28] = (curptr[28]&0xf0)+(bgColor[B]>>8);
   curptr[29] = bgColor[B];
   curptr[30] = (bgColor[G]>>4);
   curptr[31] = (bgColor[G]<<4)+(bgColor[R]>>8);
   return;
}

void CLineEngRGB12P::
setBGY7()
{
   curptr[28] = (bgColor[R]<<4)+(curptr[28]&0xf);
   curptr[32] = (bgColor[B]>>4);
   curptr[33] = (bgColor[B]<<4)+(bgColor[G]>>8);
   curptr[34] = bgColor[G];
   curptr[35] = (bgColor[R]>>4);
   return;
}

void CLineEngRGB12P::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   int pel = (y * dwide + x);
   curptr = frameBuffer[0] + ((pel/8)*36);
   rgbphase = pel%8;
}

void CLineEngRGB12P::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen,pel;
    int *rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt

         if (rgbphase==0) goto hr0;
         if (rgbphase==1) goto hr1;
         if (rgbphase==2) goto hr2;
         if (rgbphase==3) goto hr3;
         if (rgbphase==4) goto hr4;
         if (rgbphase==5) goto hr5;
         if (rgbphase==6) goto hr6;
         goto hr7;

         while (true) {

	    hr0:;

               if (delx--==0) break;
               setFGY0();
               curx++;

	    hr1:;

               if (delx--==0) break;
               setFGY1();
               curx++;

	    hr2:;

               if (delx--==0) break;
               setFGY2();
               curx++;

	    hr3:;

               if (delx--==0) break;
               setFGY3();
               curx++;

	    hr4:;

               if (delx--==0) break;
               setFGY4();
               curx++;

	    hr5:;

               if (delx--==0) break;
               setFGY5();
               curx++;

	    hr6:;

               if (delx--==0) break;
               setFGY6();
               curx++;

	    hr7:;

               if (delx--==0) break;
               setFGY7();
               curx++;

               curptr += 36;
         }
      }
      else  { // lft

         if (rgbphase==7) goto hl7;
         if (rgbphase==6) goto hl6;
         if (rgbphase==5) goto hl5;
         if (rgbphase==4) goto hl4;
         if (rgbphase==3) goto hl3;
         if (rgbphase==2) goto hl2;
         if (rgbphase==1) goto hl1;
         goto hl0;

         while (true) {

	    hl7:;

               if (delx++==0) break;
               setFGY7();
               curx--;

	    hl6:;

               if (delx++==0) break;
               setFGY6();
               curx--;

	    hl5:;

               if (delx++==0) break;
               setFGY5();
               curx--;

	    hl4:;

               if (delx++==0) break;
               setFGY4();
               curx--;

	    hl3:;

               if (delx++==0) break;
               setFGY3();
               curx--;

	    hl2:;

               if (delx++==0) break;
               setFGY2();
               curx--;

	    hl1:

               if (delx++==0) break;
               setFGY1();
               curx--;

	    hl0:

               if (delx++==0) break;
               setFGY0();
               curx--;

               curptr -= 36;
         }
      }
   }
   else {

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            while (dely-- > 0) {

               if      (rgbphase==0) setFGY0();
               else if (rgbphase==1) setFGY1();
               else if (rgbphase==2) setFGY2();
               else if (rgbphase==3) setFGY3();
               else if (rgbphase==4) setFGY4();
               else if (rgbphase==5) setFGY5();
               else if (rgbphase==6) setFGY6();
               else                  setFGY7();

               curptr += dpitch;
               rgbphase += dphase;
               if (rgbphase > 7) {
                  rgbphase -= 8;
                  curptr += 36;
               }
            }
         }
         else { // up

            while (dely++ < 0) {

               if      (rgbphase==0) setFGY0();
               else if (rgbphase==1) setFGY1();
               else if (rgbphase==2) setFGY2();
               else if (rgbphase==3) setFGY3();
               else if (rgbphase==4) setFGY4();
               else if (rgbphase==5) setFGY5();
               else if (rgbphase==6) setFGY6();
               else                  setFGY7();

               curptr -= dpitch;
               rgbphase -= dphase;
               if (rgbphase < 0) {
                  rgbphase += 8;
                  curptr -= 36;
               }
            }
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow

                  pen = delx;

                  if (rgbphase==0) goto drsh0;
                  if (rgbphase==1) goto drsh1;
                  if (rgbphase==2) goto drsh2;
                  if (rgbphase==3) goto drsh3;
                  if (rgbphase==4) goto drsh4;
                  if (rgbphase==5) goto drsh5;
                  if (rgbphase==6) goto drsh6;
                  goto drsh7;

                  while (true) {

                     drsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     drsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     drsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     drsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     drsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     drsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     drsh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     drsh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                        curx += 8;
                        curptr += 36;

                  }
               }
               else { // dn-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else setFGY7();

                     curptr += dpitch;
                     rgbphase += dphase;
                     if (rgbphase > 7) {
                        rgbphase -= 8;
                        curptr += 36;
                     }

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx++;
                        rgbphase++;
                        if (rgbphase==8) {
                           rgbphase = 0;
                           curptr += 36;
                        }
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow

                  pen = delx;

                  if (rgbphase==7) goto dlsh7;
                  if (rgbphase==6) goto dlsh6;
                  if (rgbphase==5) goto dlsh5;
                  if (rgbphase==4) goto dlsh4;
                  if (rgbphase==3) goto dlsh3;
                  if (rgbphase==2) goto dlsh2;
                  if (rgbphase==1) goto dlsh1;
                  goto dlsh0;

                  while (true) {

                     dlsh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     dlsh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     dlsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     dlsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     dlsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     dlsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     dlsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                     dlsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 7) {
                              rgbphase -= 8;
                              curptr += 36;
                           }
                        }

                        curx -= 8;
                        curptr -= 36;
                  }
               }
               else { // dn-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else setFGY7();

                     curptr += dpitch;
                     rgbphase += dphase;
                     if (rgbphase > 7) {
                        rgbphase -= 8;
                        curptr += 36;
                     }

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx--;
                        rgbphase--;
                        if (rgbphase==-1) {
                           rgbphase = 7;
                           curptr -= 36;
                        }
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow

                  pen = delx;

                  if (rgbphase==0) goto ursh0;
                  if (rgbphase==1) goto ursh1;
                  if (rgbphase==2) goto ursh2;
                  if (rgbphase==3) goto ursh3;
                  if (rgbphase==4) goto ursh4;
                  if (rgbphase==5) goto ursh5;
                  if (rgbphase==6) goto ursh6;
                  goto ursh7;

                  while (true) {

                     ursh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ursh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ursh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ursh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ursh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ursh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ursh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ursh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                        curx += 8;
                        curptr += 36;
                  }
               }
               else { // up-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else if (rgbphase==7) setFGY7();

                     curptr -= dpitch;
                     rgbphase -= dphase;
                     if (rgbphase < 0) {
                        rgbphase += 8;
                        curptr -= 36;
                     }

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx++;
                        rgbphase++;
                        if (rgbphase==8) {
                           rgbphase = 0;
                           curptr += 36;
                        }
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow

                  pen = delx;

                  if (rgbphase==7) goto ulsh7;
                  if (rgbphase==6) goto ulsh6;
                  if (rgbphase==5) goto ulsh5;
                  if (rgbphase==4) goto ulsh4;
                  if (rgbphase==3) goto ulsh3;
                  if (rgbphase==2) goto ulsh2;
                  if (rgbphase==1) goto ulsh1;
                  goto ulsh0;

                  while (true) {

                     ulsh7:;

                        if (delx--==0) break;
                        setFGY7();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ulsh6:;

                        if (delx--==0) break;
                        setFGY6();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ulsh5:;

                        if (delx--==0) break;
                        setFGY5();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ulsh4:;

                        if (delx--==0) break;
                        setFGY4();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ulsh3:;

                        if (delx--==0) break;
                        setFGY3();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ulsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ulsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                     ulsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 8;
                              curptr -= 36;
                           }
                        }

                        curx -= 8;
                        curptr -= 36;
                  }

               }
               else { // up-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();
                     else if (rgbphase==3) setFGY3();
                     else if (rgbphase==4) setFGY4();
                     else if (rgbphase==5) setFGY5();
                     else if (rgbphase==6) setFGY6();
                     else if (rgbphase==7) setFGY7();

                     curptr -= dpitch;
                     rgbphase -= dphase;
                     if (rgbphase < 0) {
                        rgbphase += 8;
                        curptr -= 36;
                     }

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx--;
                        rgbphase--;
                        if (rgbphase==-1) {
                           rgbphase = 7;
                           curptr -= 36;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   movTo(x, y);
}

void CLineEngRGB12P::
drawDot()
{
   switch(rgbphase) {

      case 0:
         setFGY0();
      break;

      case 1:
         setFGY1();
      break;

      case 2:
         setFGY2();
      break;

      case 3:
         setFGY3();
      break;

      case 4:
         setFGY4();
      break;

      case 5:
         setFGY5();
      break;

      case 6:
         setFGY6();
      break;

      case 7:
         setFGY7();
      break;
   }
}

void CLineEngRGB12P::
drawSpot(int x, int y)
{
   movTo(x&0xfffffffe,y);

   switch(rgbphase) {

      case 0:

         setFGY0();
         setFGY1();

      break;

      case 1:

         setFGY1();
         setFGY2();

      break;

      case 2:

         setFGY2();
         setFGY3();

      break;

      case 3:

         setFGY3();
         setFGY4();

      break;

      case 4:

         setFGY4();
         setFGY5();

      break;

      case 5:

         setFGY5();
         setFGY6();

      break;

      case 6:

         setFGY6();
         setFGY7();

      break;

      case 7:

         setFGY7();
         curptr += 16;
         setFGY0();
         curptr -= 16;

      break;
   }
}


void CLineEngRGB12P::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   for (int i=top;i<=bot;i++) {
      movTo(lft  , i);
      linTo(rgt+1, i);
   }
}

void CLineEngRGB12P::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         movTo(x, cury);

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  if (rgbphase==0) setFGY0();
                  else if (rgbphase==1) setFGY1();
                  else if (rgbphase==2) setFGY2();
                  else if (rgbphase==3) setFGY3();
                  else if (rgbphase==4) setFGY4();
                  else if (rgbphase==5) setFGY5();
                  else if (rgbphase==6) setFGY6();
                  else setFGY7();

                  rgbphase++;
                  if (rgbphase==8) {
                     rgbphase=0;
                     curptr += 36;
                  }
               }
            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     if (rgbphase==0) setBGY0();
                     else if (rgbphase==1) setBGY1();
                     else if (rgbphase==2) setBGY2();
                     else if (rgbphase==3) setBGY3();
                     else if (rgbphase==4) setBGY4();
                     else if (rgbphase==5) setBGY5();
                     else if (rgbphase==6) setBGY6();
                     else setBGY7();
                  }

                  rgbphase++;
                  if (rgbphase==8) {
                     rgbphase=0;
                     curptr += 36;
                  }
               }
            }

            fontscan = fontscan >> 1;

         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//    RGB 12-bit, left justified in 16 bits, little-endian
//
//////////////////////////////////////////////////////////

void CLineEngRGB12LLE::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x0000;
         fgColor[1] = 0x0000;
         fgColor[2] = 0x0000;
         fgndColor = col;
      break;

      case 1:
#if ENDIAN == MTI_BIG_ENDIAN
         fgColor[0] = 0xf0ff;     // 12 bits left justified & byte-swapped
         fgColor[1] = 0xf0ff;
         fgColor[2] = 0xf0ff;
#else
         fgColor[0] = 0xfff0;     // 12 bits left justified
         fgColor[1] = 0xfff0;
         fgColor[2] = 0xfff0;
#endif
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12LLE::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   // left-justify the colors
   r <<= 4;
   g <<= 4;
   b <<= 4;

#if ENDIAN == MTI_BIG_ENDIAN
   fgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   fgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   fgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#else
   fgColor[0] = r;
   fgColor[1] = g;
   fgColor[2] = b;
#endif
}

void CLineEngRGB12LLE::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x0000;
         bgColor[1] = 0x0000;
         bgColor[2] = 0x0000;
         bgndColor = col;
      break;

      case 1:
#if ENDIAN == MTI_BIG_ENDIAN
         bgColor[0] = 0xf0ff;     // 12 bits left justified & byte-swapped
         bgColor[1] = 0xf0ff;
         bgColor[2] = 0xf0ff;
#else
         bgColor[0] = 0xfff0;     // 12 bits left justified
         bgColor[1] = 0xfff0;
         bgColor[2] = 0xfff0;
#endif
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12LLE::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   // left-justify the colors
   r <<= 4;
   g <<= 4;
   b <<= 4;

#if ENDIAN == MTI_BIG_ENDIAN
   bgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   bgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   bgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#else
   bgColor[0] = r;
   bgColor[1] = g;
   bgColor[2] = b;
#endif
}


//////////////////////////////////////////////////////////
//
//    RGB 12-bit, left justified in 16 bits, big-endian
//
//////////////////////////////////////////////////////////

void CLineEngRGB12LBE::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x0000;
         fgColor[1] = 0x0000;
         fgColor[2] = 0x0000;
         fgndColor = col;
      break;

      case 1:
#if ENDIAN == MTI_LITTLE_ENDIAN
         fgColor[0] = 0xf0ff;     // 12 bits left justified & byte-swapped
         fgColor[1] = 0xf0ff;
         fgColor[2] = 0xf0ff;
#else
         fgColor[0] = 0xfff0;     // 12 bits left justified
         fgColor[1] = 0xfff0;
         fgColor[2] = 0xfff0;
#endif
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12LBE::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   // left-justify the colors
   r <<= 4;
   g <<= 4;
   b <<= 4;

#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   fgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   fgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#else
   fgColor[0] = r;
   fgColor[1] = g;
   fgColor[2] = b;
#endif
}

void CLineEngRGB12LBE::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x0000;
         bgColor[1] = 0x0000;
         bgColor[2] = 0x0000;
         bgndColor = col;
      break;

      case 1:
#if ENDIAN == MTI_LITTLE_ENDIAN
         bgColor[0] = 0xf0ff;     // 12 bits left justified & byte-swapped
         bgColor[1] = 0xf0ff;
         bgColor[2] = 0xf0ff;
#else
         bgColor[0] = 0xfff0;     // 12 bits left justified
         bgColor[1] = 0xfff0;
         bgColor[2] = 0xfff0;
#endif
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB12LBE::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   // left-justify the colors
   r <<= 4;
   g <<= 4;
   b <<= 4;

#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   bgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   bgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#else
   bgColor[0] = r;
   bgColor[1] = g;
   bgColor[2] = b;
#endif
}

//////////////////////////////////////////////////////////
//
//              RGB 16-bit
//
//////////////////////////////////////////////////////////

CLineEngRGB16::
CLineEngRGB16()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngRGB16::
~CLineEngRGB16()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngRGB16::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                        )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch>>1;
   intrlc  = il;
   fldSize = pitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned short[dpitch];

   // basic font dimensions

   setFontSize(1);
}

int CLineEngRGB16::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = (unsigned short *)new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = (unsigned short *)new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngRGB16::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = (unsigned short *) frmbuf[0];
   frameBuffer[1] = (unsigned short *) frmbuf[1];
}

void CLineEngRGB16::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x0000;
         fgColor[1] = 0x0000;
         fgColor[2] = 0x0000;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xffff;
         fgColor[1] = 0xffff;
         fgColor[2] = 0xffff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB16::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   fgColor[0] = r&0xffff;
   fgColor[1] = g&0xffff;
   fgColor[2] = b&0xffff;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   fgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   fgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
}

void CLineEngRGB16::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x0000;
         bgColor[1] = 0x0000;
         bgColor[2] = 0x0000;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xffff;
         bgColor[1] = 0xffff;
         bgColor[2] = 0xffff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB16::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   bgColor[0] = r&0xffff;
   bgColor[1] = g&0xffff;
   bgColor[2] = b&0xffff;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   bgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   bgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
}

void CLineEngRGB16::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + x*3;
}

void CLineEngRGB16::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {

            *(curptr)   = fgColor[0];
            *(curptr+1) = fgColor[1];
            *(curptr+2) = fgColor[2];

            curptr += 3;
            curx++;
         }
      }
      else { // lft
         while (delx++!=0) {

            *(curptr)   = fgColor[0];
            *(curptr+1) = fgColor[1];
            *(curptr+2) = fgColor[2];

            curptr -= 3;
            curx--;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            while (dely--!=0) {

               *(curptr)   = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];

               rowy = rowtbl + (++cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

            }

         }
         else { // up

            while (dely++!=0) {

               *(curptr)   = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];

               rowy = rowtbl + (--cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

            }

         }

      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr += 3;
                     curx++;

                     if ((pen-=bry) <= 0) {
                        pen += brx;

                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr += 3;
                        curx++;
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr -= 3;
                     curx--;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr -= 3;
                        curx--;
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr += 3;
                     curx++;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr += 3;
                        curx++;
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr -= 3;
                     curx--;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr -= 3;
                        curx--;
                     }
                  }
               }
            }
         }
      }
   }
}

void CLineEngRGB16::
drawDot()
{
   *(curptr)   = fgColor[0];
   *(curptr+1) = fgColor[1];
   *(curptr+2) = fgColor[2];
}

void CLineEngRGB16::
drawSpot(int x, int y)
{
   movTo(x,y);
   *(curptr)   = fgColor[0];
   *(curptr+1) = fgColor[1];
   *(curptr+2) = fgColor[2];
}

void CLineEngRGB16::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int pxls = rgt - lft + 1;

   unsigned short *dst = linebuf;

   for (int i=0;i<pxls;i++) {
      *dst++ = fgColor[0];
      *dst++ = fgColor[1];
      *dst++ = fgColor[2];
   }

   pxls *= 6;
   for (int i=top;i<=bot;i++)
   {
      unsigned char *rowbeg = (unsigned char *)(frameBuffer[rowTbl[i].rowfld] + rowTbl[i].rowoff + lft*3);
      MTImemcpy((unsigned char *)rowbeg,linebuf,pxls);
   }
}

void CLineEngRGB16::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + x*3;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  *(curptr)   = fgColor[0];
                  *(curptr+1) = fgColor[1];
                  *(curptr+2) = fgColor[2];

                  curptr += 3;

               }
            }
            else {

               if (bgndColor==-1) {

                  curptr += 3*pelMult;
               }
               else {

                  for (int k=0;k<pelMult;k++) {

                     *curptr     = bgColor[0];
                     *(curptr+1) = bgColor[1];
                     *(curptr+2) = bgColor[2];

                     curptr += 3;

                  }
               }
            }

            fontscan = fontscan >> 1;
         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//              RGB 16-bit Big-endian
//
//////////////////////////////////////////////////////////

void CLineEngRGB16BE::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x0000;
         fgColor[1] = 0x0000;
         fgColor[2] = 0x0000;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xffff;
         fgColor[1] = 0xffff;
         fgColor[2] = 0xffff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB16BE::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   fgColor[0] = r;
   fgColor[1] = g;
   fgColor[2] = b;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   fgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   fgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
}

void CLineEngRGB16BE::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x0000;
         bgColor[1] = 0x0000;
         bgColor[2] = 0x0000;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xffff;
         bgColor[1] = 0xffff;
         bgColor[2] = 0xffff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB16BE::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   bgColor[0] = r;
   bgColor[1] = g;
   bgColor[2] = b;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   bgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   bgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
}

//////////////////////////////////////////////////////////
//
//              RGB 16-bit Little-endian
//
//////////////////////////////////////////////////////////

void CLineEngRGB16LE::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x0000;
         fgColor[1] = 0x0000;
         fgColor[2] = 0x0000;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xffff;
         fgColor[1] = 0xffff;
         fgColor[2] = 0xffff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB16LE::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   fgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   fgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   fgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor[0] = r;
   fgColor[1] = g;
   fgColor[2] = b;
#endif
}

void CLineEngRGB16LE::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x0000;
         bgColor[1] = 0x0000;
         bgColor[2] = 0x0000;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xffff;
         bgColor[1] = 0xffff;
         bgColor[2] = 0xffff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGB16LE::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   bgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   bgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   bgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor[0] = r;
   bgColor[1] = g;
   bgColor[2] = b;
#endif
}

//////////////////////////////////////////////////////////
//
//              RGBA 16-bit
//
//////////////////////////////////////////////////////////

CLineEngRGBA16::
CLineEngRGBA16()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngRGBA16::
~CLineEngRGBA16()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngRGBA16::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                        )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch>>1;
   intrlc  = il;
   fldSize = pitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned short[dpitch];

   // basic font dimensions

   setFontSize(1);
}

int CLineEngRGBA16::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = (unsigned short *)new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = (unsigned short *)new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngRGBA16::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = (unsigned short *) frmbuf[0];
   frameBuffer[1] = (unsigned short *) frmbuf[1];
}

void CLineEngRGBA16::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x0000;
         fgColor[1] = 0x0000;
         fgColor[2] = 0x0000;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xffff;
         fgColor[1] = 0xffff;
         fgColor[2] = 0xffff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBA16::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   fgColor[0] = r&0xffff;
   fgColor[1] = g&0xffff;
   fgColor[2] = b&0xffff;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   fgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   fgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
}

void CLineEngRGBA16::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x0000;
         bgColor[1] = 0x0000;
         bgColor[2] = 0x0000;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xffff;
         bgColor[1] = 0xffff;
         bgColor[2] = 0xffff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBA16::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   bgColor[0] = r&0xffff;
   bgColor[1] = g&0xffff;
   bgColor[2] = b&0xffff;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   bgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   bgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
}

void CLineEngRGBA16::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + x*4;
}

void CLineEngRGBA16::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {

            *(curptr)   = fgColor[0];
            *(curptr+1) = fgColor[1];
            *(curptr+2) = fgColor[2];
            *(curptr+3) = 0;

            curptr += 4;
            curx++;
         }
      }
      else { // lft
         while (delx++!=0) {

            *(curptr)   = fgColor[0];
            *(curptr+1) = fgColor[1];
            *(curptr+2) = fgColor[2];
            *(curptr+3) = 0;

            curptr -= 4;
            curx--;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            while (dely--!=0) {

               *(curptr)   = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];
               *(curptr+3) = 0;

               rowy = rowtbl + (++cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;

            }

         }
         else { // up

            while (dely++!=0) {

               *(curptr)   = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];
               *(curptr+3) = 0;

               rowy = rowtbl + (--cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;

            }

         }

      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];
                     *(curptr+3) = 0;

                     curptr += 4;
                     curx++;

                     if ((pen-=bry) <= 0) {
                        pen += brx;

                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];
                     *(curptr+3) = 0;

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr += 4;
                        curx++;
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];
                     *(curptr+3) = 0;

                     curptr -= 4;
                     curx--;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];
                     *(curptr+3) = 0;

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr -= 4;
                        curx--;
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];
                     *(curptr+3) = 0;

                     curptr += 4;
                     curx++;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];
                     *(curptr+3) = 0;

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr += 4;
                        curx++;
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];
                     *(curptr+3) = 0;

                     curptr -= 4;
                     curx--;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];
                     *(curptr+3) = 0;

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*4;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr -= 4;
                        curx--;
                     }
                  }
               }
            }
         }
      }
   }
}

void CLineEngRGBA16::
drawDot()
{
   *(curptr)   = fgColor[0];
   *(curptr+1) = fgColor[1];
   *(curptr+2) = fgColor[2];
   *(curptr+3) = 0;
}

void CLineEngRGBA16::
drawSpot(int x, int y)
{
   movTo(x,y);
   *(curptr)   = fgColor[0];
   *(curptr+1) = fgColor[1];
   *(curptr+2) = fgColor[2];
   *(curptr+3) = 0;
}

void CLineEngRGBA16::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int pxls = rgt - lft + 1;

   unsigned short *dst = linebuf;

   for (int i=0;i<pxls;i++) {
      *dst++ = fgColor[0];
      *dst++ = fgColor[1];
      *dst++ = fgColor[2];
      *dst++ = 0;
   }

   pxls *= 8;
   for (int i=top;i<=bot;i++)
   {
      unsigned char *rowbeg = (unsigned char *)(frameBuffer[rowTbl[i].rowfld] + rowTbl[i].rowoff + lft*4);
      MTImemcpy((unsigned char *)rowbeg,linebuf,pxls);
   }
}

void CLineEngRGBA16::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + x*4;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  *(curptr)   = fgColor[0];
                  *(curptr+1) = fgColor[1];
                  *(curptr+2) = fgColor[2];
                  *(curptr+3) = 0;

                  curptr += 4;

               }
            }
            else {

               if (bgndColor==-1) {

                  curptr += 4*pelMult;
               }
               else {

                  for (int k=0;k<pelMult;k++) {

                     *curptr     = bgColor[0];
                     *(curptr+1) = bgColor[1];
                     *(curptr+2) = bgColor[2];
                     *(curptr+3) = 0;

                     curptr += 4;

                  }
               }
            }

            fontscan = fontscan >> 1;
         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//              RGBA 16-bit Big-endian
//
//////////////////////////////////////////////////////////

void CLineEngRGBA16BE::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x0000;
         fgColor[1] = 0x0000;
         fgColor[2] = 0x0000;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xffff;
         fgColor[1] = 0xffff;
         fgColor[2] = 0xffff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBA16BE::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   fgColor[0] = r;
   fgColor[1] = g;
   fgColor[2] = b;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   fgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   fgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
}

void CLineEngRGBA16BE::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x0000;
         bgColor[1] = 0x0000;
         bgColor[2] = 0x0000;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xffff;
         bgColor[1] = 0xffff;
         bgColor[2] = 0xffff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBA16BE::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   bgColor[0] = r;
   bgColor[1] = g;
   bgColor[2] = b;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   bgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   bgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
}

//////////////////////////////////////////////////////////
//
//              RGBA 16-bit Little-endian
//
//////////////////////////////////////////////////////////

void CLineEngRGBA16LE::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x0000;
         fgColor[1] = 0x0000;
         fgColor[2] = 0x0000;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xffff;
         fgColor[1] = 0xffff;
         fgColor[2] = 0xffff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBA16LE::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   fgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   fgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   fgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor[0] = r;
   fgColor[1] = g;
   fgColor[2] = b;
#endif
}

void CLineEngRGBA16LE::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x0000;
         bgColor[1] = 0x0000;
         bgColor[2] = 0x0000;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xffff;
         bgColor[1] = 0xffff;
         bgColor[2] = 0xffff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBA16LE::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   bgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   bgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   bgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor[0] = r;
   bgColor[1] = g;
   bgColor[2] = b;
#endif
}

//////////////////////////////////////////////////////////
//
//              RGBA Half (16-bit float)
//
//////////////////////////////////////////////////////////

void CLineEngRGBAHalf::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x0000;
         fgColor[1] = 0x0000;
         fgColor[2] = 0x0000;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xbc00;   // 1.0
         fgColor[1] = 0xbc00;   // 1.0
         fgColor[2] = 0xbc00;   // 1.0
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBAHalf::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   fgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   fgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   fgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor[0] = r;
   fgColor[1] = g;
   fgColor[2] = b;
#endif
}

void CLineEngRGBAHalf::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x0000;
         bgColor[1] = 0x0000;
         bgColor[2] = 0x0000;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xbc00;   // 1.0
         bgColor[1] = 0xbc00;   // 1.0
         bgColor[2] = 0xbc00;   // 1.0
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBAHalf::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
#if ENDIAN == MTI_BIG_ENDIAN
   bgColor[0] = ((r&0xff)<<8)+((r&0xff00)>>8);
   bgColor[1] = ((g&0xff)<<8)+((g&0xff00)>>8);
   bgColor[2] = ((b&0xff)<<8)+((b&0xff00)>>8);
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor[0] = r;
   bgColor[1] = g;
   bgColor[2] = b;
#endif
}

//////////////////////////////////////////////////////////
//
//              YUV 16-bit (as in intermediate)
//
//////////////////////////////////////////////////////////

void CLineEngYUV16::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         TransformRGBtoYUV(0x0000,0x0000,0x0000,
           &fgColor[0],&fgColor[1],&fgColor[2]);
         fgndColor = col;
      break;

      case 1:
         TransformRGBtoYUV(0xffff,0xffff,0xffff,
           &fgColor[0],&fgColor[1],&fgColor[2]);
         fgndColor = col;
      break;

      default:
      break;
   }

   fgColor[0] = fgColor[0]>>8;
   fgColor[1] = fgColor[1]>>8;
   fgColor[2] = fgColor[2]>>8;
}

void CLineEngYUV16::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   TransformRGBtoYUV(r,g,b,
     &fgColor[0],&fgColor[1],&fgColor[2]);

   fgColor[0] = fgColor[0]>>8;
   fgColor[1] = fgColor[1]>>8;
   fgColor[2] = fgColor[2]>>8;
}


void CLineEngYUV16::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         TransformRGBtoYUV(0x0000,0x0000,0x0000,
           &bgColor[0],&bgColor[1],&bgColor[2]);
         bgndColor = col;
      break;

      case 1:
         TransformRGBtoYUV(0xffff,0xffff,0xffff,
           &bgColor[0],&bgColor[1],&bgColor[2]);
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }

   bgColor[0] = bgColor[0]>>8;
   bgColor[1] = bgColor[1]>>8;
   bgColor[2] = bgColor[2]>>8; 
}

void CLineEngYUV16::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   TransformRGBtoYUV(r,g,b,
     &bgColor[0],&bgColor[1],&bgColor[2]);

   bgColor[0] = bgColor[0]>>8;
   bgColor[1] = bgColor[1]>>8;
   bgColor[2] = bgColor[2]>>8; 
}

//////////////////////////////////////////////////////////
//
//            TGA 8-bit is derived from RGB8
//
//////////////////////////////////////////////////////////

CLineEngBGR8::
CLineEngBGR8()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngBGR8::
~CLineEngBGR8()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngBGR8::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x00;
         fgColor[1] = 0x00;
         fgColor[2] = 0x00;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xff;
         fgColor[1] = 0xff;
         fgColor[2] = 0xff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngBGR8::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   fgColor[0] = b>>8;
   fgColor[1] = g>>8;
   fgColor[2] = r>>8;
}

void CLineEngBGR8::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x00;
         bgColor[1] = 0x00;
         bgColor[2] = 0x00;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xff;
         bgColor[1] = 0xff;
         bgColor[2] = 0xff;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngBGR8::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   bgColor[0] = b>>8;
   bgColor[1] = g>>8;
   bgColor[2] = r>>8;
}

//////////////////////////////////////////////////////////
//
//		RGBA 10-bit (INTERLEAVED Alpha)
//
//////////////////////////////////////////////////////////

#ifdef TOP
#undef TOP
#endif
#ifdef MID
#undef MID
#endif
#ifdef LOW
#undef LOW
#endif
#define TOP 0xffc00000
#define MID 0x003ff000
#define LOW 0x00000ffc

#define R 0
#define G 1
#define B 2

CLineEngRGBA10::
CLineEngRGBA10()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngRGBA10::
~CLineEngRGBA10()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngRGBA10::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                         )
{
   dwide   = w;
   dhigh   = h;

   // this format is row-packed, so we'll want the
   // byte-length and phase-penalty for each row in
   // order to do the line algorithm incrementally
   dpitch  = (w/3)*16;
   dphase  = w%3;

   // we only support a non-interlaced frame buffer
   intrlc  = false;

   // calculate the frame buffer size
   fldSize = ((w*h+2)/3)*4;

   // basic font dimensions
   setFontSize(1);
}

int CLineEngRGBA10::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngRGBA10::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngRGBA10::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[R] = 0;
         fgColor[G] = 0;
         fgColor[B] = 0;
         fgndColor = col;
      break;

      case 1:
         fgColor[R] = 0xfffffffc;
         fgColor[G] = 0xfffffffc;
         fgColor[B] = 0xfffffffc;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBA10::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   int ir = r>>6;
   fgColor[R] = (ir<<22)+(ir<<12)+(ir<<2);
   int ig = g>>6;
   fgColor[G] = (ig<<22)+(ig<<12)+(ig<<2);
   int ib = b>>6;
   fgColor[B] = (ib<<22)+(ib<<12)+(ib<<2);
}

void CLineEngRGBA10::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[R] = 0;
         bgColor[G] = 0;
         bgColor[B] = 0;
         bgndColor = col;
      break;

      case 1:
         bgColor[R] = 0xfffffffc;
         bgColor[G] = 0xfffffffc;
         bgColor[B] = 0xfffffffc;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngRGBA10::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   int ir = r>>6;
   bgColor[R] = (ir<<22)+(ir<<12)+(ir<<2);
   int ig = g>>6;
   bgColor[G] = (ig<<22)+(ig<<12)+(ig<<2);
   int ib = b>>6;
   bgColor[B] = (ib<<22)+(ib<<12)+(ib<<2);
}

void CLineEngRGBA10::
setFGY0()
{
    int unpack;
   unpack = (fgColor[R]&TOP)+(fgColor[G]&MID)+(fgColor[B]&LOW);
   curptr[0] = (unpack>>24);
   curptr[1] = (unpack>>16);
   curptr[2] = (unpack>> 8);
   curptr[3] = (unpack    );
   return;
}

void CLineEngRGBA10::
setFGY1()
{
    int unpack;
   unpack = (curptr[5]<<16);
   unpack = (unpack&TOP)+(fgColor[R]&MID)+(fgColor[G]&LOW);
   curptr[5] = (unpack>>16);
   curptr[6] = (unpack>> 8);
   curptr[7] = (unpack    );
   unpack = (curptr[9]<<16);
   unpack = (fgColor[B]&TOP)+(unpack&MID);
   curptr[8] = (unpack>>24);
   curptr[9] = (unpack>>16);
   return;
}

void CLineEngRGBA10::
setFGY2()
{
    int unpack;
   unpack = (curptr[10]<<8);
   unpack = (unpack&MID)+(fgColor[R]&LOW);
   curptr[10] = (unpack>> 8);
   curptr[11] = (unpack    );
   unpack = (curptr[14]<<8);
   unpack = (fgColor[G]&TOP)+(fgColor[B]&MID)+(unpack&LOW);
   curptr[12] = (unpack>>24);
   curptr[13] = (unpack>>16);
   curptr[14] = (unpack>> 8);
   return;
}

void CLineEngRGBA10::
setBGY0()
{
    int unpack;
   unpack = (bgColor[R]&TOP)+(bgColor[G]&MID)+(bgColor[B]&LOW);
   curptr[0] = (unpack>>24);
   curptr[1] = (unpack>>16);
   curptr[2] = (unpack>> 8);
   curptr[3] = (unpack    );
   return;
}

void CLineEngRGBA10::
setBGY1()
{
    int unpack;
   unpack = (curptr[5]<<16);
   unpack = (unpack&TOP)+(bgColor[R]&MID)+(bgColor[G]&LOW);
   curptr[5] = (unpack>>16);
   curptr[6] = (unpack>> 8);
   curptr[7] = (unpack    );
   unpack = (curptr[9]<<16);
   unpack = (bgColor[B]&TOP)+(unpack&MID);
   curptr[8] = (unpack>>24);
   curptr[9] = (unpack>>16);
   return;
}

void CLineEngRGBA10::
setBGY2()
{
    int unpack;
   unpack = (curptr[10]<<8);
   unpack = (unpack&MID)+(bgColor[R]&LOW);
   curptr[10] = (unpack>> 8);
   curptr[11] = (unpack    );
   unpack = (curptr[14]<<8);
   unpack = (bgColor[G]&TOP)+(bgColor[B]&MID)+(unpack&LOW);
   curptr[12] = (unpack>>24);
   curptr[13] = (unpack>>16);
   curptr[14] = (unpack>> 8);
   return;
}

void CLineEngRGBA10::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   int pel = (y * dwide + x);
   curptr = frameBuffer[0] + ((pel/3)<<4);
   rgbphase = pel%3;
}

void CLineEngRGBA10::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen,pel;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt

         if (rgbphase==0) goto hr0;
         if (rgbphase==1) goto hr1;
         goto hr2;

         while (true) {

	    hr0:;

               if (delx--==0) break;
               setFGY0();
               curx++;

	    hr1:;

               if (delx--==0) break;
               setFGY1();
               curx++;

	    hr2:;

               if (delx--==0) break;
               setFGY2();
               curx++;

               curptr += 16;
         }
      }
      else  { // lft

         if (rgbphase==2) goto hl2;
         if (rgbphase==1) goto hl1;
         goto hl0;

         while (true) {

	    hl2:;

               if (delx++==0) break;
               setFGY2();
               curx--;

	    hl1:

               if (delx++==0) break;
               setFGY1();
               curx--;

	    hl0:

               if (delx++==0) break;
               setFGY0();
               curx--;
               curptr -= 16;
         }
      }
   }
   else {

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

	    if (rgbphase==0) {
	       while (dely--) {
                  setFGY0();
                  curptr += dpitch;
                  rgbphase += dphase;
                  if (rgbphase > 2) {
                     rgbphase -= 3;
                     curptr += 16;
                  }
               }
            }
	    else if (rgbphase==1) {
	       while (dely--) {
                  setFGY1();
                  curptr += dpitch;
                  rgbphase += dphase;
                  if (rgbphase > 2) {
                     rgbphase -= 3;
                     curptr += 16;
                  }
               }
            }
            else if (rgbphase==2) {
	       while (dely--) {
                  setFGY2();
                  curptr += dpitch;
                  rgbphase += dphase;
                  if (rgbphase > 2) {
                     rgbphase -= 3;
                     curptr += 16;
                  }
               }
            }
         }
         else { // up

	    if (rgbphase==0) {
	       while (dely++) {
                  setFGY0();
                  curptr -= dpitch;
                  rgbphase -= dphase;
                  if (rgbphase < 0) {
                     rgbphase += 3;
                     curptr -= 16;
                  }
               }
            }
	    else if (rgbphase==1) {
	       while (dely++) {
                  setFGY1();
                  curptr -= dpitch;
                  rgbphase -= dphase;
                  if (rgbphase < 0) {
                     rgbphase += 3;
                     curptr -= 16;
                  }
               }
            }
            else if (rgbphase==2) {
	       while (dely++) {
                  setFGY2();
                  curptr -= dpitch;
                  rgbphase -= dphase;
                  if (rgbphase < 0) {
                     rgbphase += 3;
                     curptr -= 16;
                  }
               }
            }
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow

                  pen = delx;

                  if (rgbphase==0) goto drsh0;
                  if (rgbphase==1) goto drsh1;
                  goto drsh2;

                  while (true) {

                     drsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 2) {
                              rgbphase -= 3;
                              curptr += 16;
                           }
                        }

                     drsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 2) {
                              rgbphase -= 3;
                              curptr += 16;
                           }
                        }

                     drsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 2) {
                              rgbphase -= 3;
                              curptr += 16;
                           }
                        }

                        curx += 3;
                        curptr += 16;

                  }
               }
               else { // dn-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();

                     curptr += dpitch;
                     rgbphase += dphase;
                     if (rgbphase > 2) {
                        rgbphase -= 3;
                        curptr += 16;
                     }

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx++;
                        rgbphase++;
                        if (rgbphase==3) {
                           rgbphase = 0;
                           curptr += 16;
                        }
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow

                  pen = delx;

                  if (rgbphase==2) goto dlsh2;
                  if (rgbphase==1) goto dlsh1;
                  goto dlsh0;

                  while (true) {

                     dlsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 2) {
                              rgbphase -= 3;
                              curptr += 16;
                           }
                        }

                     dlsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 2) {
                              rgbphase -= 3;
                              curptr += 16;
                           }
                        }

                     dlsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) <= 0) {
                           pen += brx;
                           curptr += dpitch;
                           rgbphase += dphase;
                           if (rgbphase > 2) {
                              rgbphase -= 3;
                              curptr += 16;
                           }
                        }

                        curx -= 3;
                        curptr -= 16;
                  }
               }
               else { // dn-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();

                     curptr += dpitch;
                     rgbphase += dphase;
                     if (rgbphase > 2) {
                        rgbphase -= 3;
                        curptr += 16;
                     }

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx--;
                        rgbphase--;
                        if (rgbphase==-1) {
                           rgbphase = 2;
                           curptr -= 16;
                        }
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow

                  pen = delx;

                  if (rgbphase==0) goto ursh0;
                  if (rgbphase==1) goto ursh1;
                  goto ursh2;

                  while (true) {

                     ursh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 3;
                              curptr -= 16;
                           }
                        }

                     ursh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 3;
                              curptr -= 16;
                           }
                        }

                     ursh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 3;
                              curptr -= 16;
                           }
                        }

                        curx += 3;
                        curptr += 16;
                  }
               }
               else { // up-rgt steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();

                     curptr -= dpitch;
                     rgbphase -= dphase;
                     if (rgbphase < 0) {
                        rgbphase += 3;
                        curptr -= 16;
                     }

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx++;
                        rgbphase++;
                        if (rgbphase==3) {
                           rgbphase = 0;
                           curptr += 16;
                        }
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow

                  pen = delx;

                  if (rgbphase==2) goto ulsh2;
                  if (rgbphase==1) goto ulsh1;
                  goto ulsh0;

                  while (true) {

                     ulsh2:;

                        if (delx--==0) break;
                        setFGY2();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 3;
                              curptr -= 16;
                           }
                        }

                     ulsh1:;

                        if (delx--==0) break;
                        setFGY1();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 3;
                              curptr -= 16;
                           }
                        }

                     ulsh0:;

                        if (delx--==0) break;
                        setFGY0();
                        if ((pen-=bry) < 0) {
                           pen += brx;
                           curptr -= dpitch;
                           rgbphase -= dphase;
                           if (rgbphase < 0) {
                              rgbphase += 3;
                              curptr -= 16;
                           }
                        }

                        curx -= 3;
                        curptr -= 16;
                  }

               }
               else { // up-lft steep

                  pen = dely;
                  while (dely--!=0) {

                     if      (rgbphase==0) setFGY0();
                     else if (rgbphase==1) setFGY1();
                     else if (rgbphase==2) setFGY2();

                     curptr -= dpitch;
                     rgbphase -= dphase;
                     if (rgbphase < 0) {
                        rgbphase += 3;
                        curptr -= 16;
                     }

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx--;
                        rgbphase--;
                        if (rgbphase==-1) {
                           rgbphase = 2;
                           curptr -= 16;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   curx = x;
   cury = y;
}

void CLineEngRGBA10::
drawDot()
{
   switch(rgbphase) {

      case 0:
         setFGY0();
      break;

      case 1:
         setFGY1();
      break;

      case 2:
         setFGY2();
      break;
   }
}

void CLineEngRGBA10::
drawSpot(int x, int y)
{
   movTo(x&0xfffffffe,y);

   switch(rgbphase) {

      case 0:

         setFGY0();
         setFGY1();

      break;

      case 1:

         setFGY1();
         setFGY2();

      break;

      case 2:

         setFGY2();
         curptr += 16;
         setFGY0();
         curptr -= 16;

      break;
   }
}


void CLineEngRGBA10::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   for (int i=top;i<=bot;i++) {
      movTo(lft  , i);
      linTo(rgt+1, i);
   }
}

void CLineEngRGBA10::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         rgbphase = x%3;
         int pel = (cury * dwide + x);
         curptr = frameBuffer[0] + ((pel/3)<<4);

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  if (rgbphase==0) setFGY0();
                  else if (rgbphase==1) setFGY1();
                  else if (rgbphase==2) setFGY2();

                  rgbphase++;
                  if (rgbphase==3) {
                     rgbphase=0;
                     curptr += 16;
                  }
               }
            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     if (rgbphase==0) setBGY0();
                     else if (rgbphase==1) setBGY1();
                     else if (rgbphase==2) setBGY2();
                  }

                  rgbphase++;
                  if (rgbphase==3) {
                     rgbphase=0;
                     curptr += 16;
                  }
               }
            }

            fontscan = fontscan >> 1;

         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//              DPX MONO 10-bit
//
//////////////////////////////////////////////////////////

CLineEngMON10::
CLineEngMON10()
{
   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngMON10::
~CLineEngMON10()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
}

void CLineEngMON10::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                        )
{
   dwide   = w;
   dhigh   = h;

   // this format is row-packed, so we'll want the
   // byte-length and phase-penalty for each row in
   // order to do the line algorithm incrementally
   dpitch  = (w/3)*4;
   dphase  = w%3;

   // we only support a non-interlaced frame buffer
   intrlc  = false;

   // calculate the frame buffer size
   fldSize = ((w*h+2)/3)*4;

   // basic font dimensions
   setFontSize(1);
}

int CLineEngMON10::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];

   return(0);
}

void CLineEngMON10::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
}

void CLineEngMON10::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x00;
         fgColor[1] = 0x00;
         fgColor[2] = 0x00;
         fgColor[3] = 0x00;
         fgColor[4] = 0x00;
         fgColor[5] = 0x00;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0x3f;
         fgColor[1] = 0xf0;
         fgColor[2] = 0x0f;
         fgColor[3] = 0xfc;
         fgColor[4] = 0x03;
         fgColor[5] = 0xff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngMON10::
setFGColor(MTI_UINT16  r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
   fgColor[0] = (y>>10)&0x3f;
   fgColor[1] = (y>>2)&0xf0;
   fgColor[2] = (y>>12)&0x0f;
   fgColor[3] = (y>>4)&0xfc;
   fgColor[4] = (y>>14)&0x03;
   fgColor[5] = (y>>6);
}

void CLineEngMON10::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x00;
         bgColor[1] = 0x00;
         bgColor[2] = 0x00;
         bgColor[3] = 0x00;
         bgColor[4] = 0x00;
         bgColor[5] = 0x00;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0x3f;
         bgColor[1] = 0xf0;
         bgColor[2] = 0x0f;
         bgColor[3] = 0xfc;
         bgColor[4] = 0x03;
         bgColor[5] = 0xff;
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngMON10::
setBGColor(MTI_UINT16  r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
   bgColor[0] = (y>>10)&0x3f;
   bgColor[1] = (y>>2)&0xf0;
   bgColor[2] = (y>>12)&0x0f;
   bgColor[3] = (y>>4)&0xfc;
   bgColor[4] = (y>>14)&0x03;
   bgColor[5] = (y>>6);
}

void CLineEngMON10::
setFGY0()
{
   curptr[2] = (curptr[2]&0xfc)+fgColor[4];
   curptr[3] = fgColor[5];
}

void CLineEngMON10::
setFGY1()
{
   curptr[1] = (curptr[1]&0xf0)+fgColor[2];
   curptr[2] = fgColor[3] + (curptr[2]&0x03);
}

void CLineEngMON10::
setFGY2()
{
   curptr[0] = fgColor[0];
   curptr[1] = fgColor[1]+(curptr[1]&0x0f);
}

void CLineEngMON10::
setBGY0()
{
   curptr[2] = (curptr[2]&0xfc)+bgColor[4];
   curptr[3] = bgColor[5];
}

void CLineEngMON10::
setBGY1()
{
   curptr[1] = (curptr[1]&0xf0)+bgColor[2];
   curptr[2] = bgColor[3] + (curptr[2]&0x03);
}

void CLineEngMON10::
setBGY2()
{
   curptr[0] = bgColor[0];
   curptr[1] = bgColor[1]+(curptr[1]&0x0f);
}


void CLineEngMON10::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   int pel = (y * dwide + x);
   curptr = frameBuffer[0] + ((pel/3)<<2);
   phase = pel%3;
}

void CLineEngMON10::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt

         if (phase==0) goto hr0;
         if (phase==1) goto hr1;
         goto hr2;

         while (true) {

	    hr0:;

               if (delx--==0) break;
               setFGY0();

	    hr1:;

               if (delx--==0) break;
               setFGY1();

	    hr2:;

               if (delx--==0) break;
               setFGY2();
               curptr += 4;
         }
      }
      else { // lft

         if (phase==2) goto hl2;
         if (phase==1) goto hl1;
         goto hl0;

         while (true) {

	    hl2:;

               if (delx++==0) break;
               setFGY2();

	    hl1:

               if (delx++==0) break;
               setFGY1();

	    hl0:

               if (delx++==0) break;
               setFGY0();
               curptr -= 4;
         }
      }
   }
   else {

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            while (dely-- > 0) {

               if      (phase==0) setFGY0();
               else if (phase==1) setFGY1();
               else               setFGY2();

               curptr += dpitch;
               phase += dphase;
               if (phase > 2) {
                  phase -= 3;
                  curptr += 4;
               }
            }
         }
         else { // up

            while (dely++ < 0) {

               if      (phase==0) setFGY0();
               else if (phase==1) setFGY1();
               else               setFGY2();

               curptr -= dpitch;
               phase -= dphase;
               if (phase < 0) {
                  phase += 3;
                  curptr -= 4;
               }
            }
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow

                  pen = delx;

                  while (delx-- > 0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else               setFGY2();

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        curptr += dpitch;
                        phase += dphase;
                        if (phase > 2) {
                           phase -= 3;
                           curptr += 4;
                        }
                     }
                     phase++;
                     if (phase > 2) {
                        phase -= 3;
                        curptr += 4;
                     }

                  }
               }
               else { // dn-rgt steep

                  pen = dely;

                  while (dely--!=0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else               setFGY2();

                     curptr += dpitch;
                     phase += dphase;
                     if (phase > 2) {
                        phase -= 3;
                        curptr += 4;
                     } 
                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        phase++;
                        if (phase > 2) {
                           phase -= 3;
                           curptr += 4;
                        }
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow

                  pen = delx;

                  while (delx-- > 0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else               setFGY2();

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        curptr += dpitch;
                        phase += dphase;
                        if (phase > 2) {
                           phase -= 3;
                           curptr += 4;
                        }
                     }
                     phase--;
                     if (phase < 0) {
                        phase += 3;
                        curptr -= 4;
                     }

                  }

               }
               else { // dn-lft steep

                  pen = dely;

                  while (dely--!=0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else               setFGY2();

                     curptr += dpitch;
                     phase += dphase;
                     if (phase > 2) {
                        phase -= 3;
                        curptr += 4;
                     }
                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        phase--;
                        if (phase < 0) {
                           phase += 3;
                           curptr -= 4;
                        }
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow

                  pen = delx;

                  while (delx-- > 0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else               setFGY2();

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        curptr -= dpitch;
                        phase -= dphase;
                        if (phase <0) {
                           phase += 3;
                           curptr -= 4;
                        }
                     }
                     phase++;
                     if (phase > 2) {
                        phase -= 3;
                        curptr += 4;
                     }

                  }

               }
               else { // up-rgt steep

                  pen = dely;

                  while (dely--!=0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else               setFGY2();

                     curptr -= dpitch;
                     phase -= dphase;
                     if (phase < 0) {
                        phase += 3;
                        curptr -= 4;
                     }
                     if ((pen-=brx) < 0) {
                        pen += bry;
                        phase++;
                        if (phase > 2) {
                           phase -= 3;
                           curptr += 4;
                        }
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow

                  pen = delx;

                  while (delx-- > 0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else               setFGY2();

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        curptr -= dpitch;
                        phase -= dphase;
                        if (phase < 0) {
                           phase += 3;
                           curptr -= 4;
                        }
                     }
                     phase--;
                     if (phase < 0) {
                        phase += 3;
                        curptr -= 4;
                     }
                  }
               }
               else { // up-lft steep

                  pen = dely;

                  while (dely--!=0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else               setFGY2();

                     curptr -= dpitch;
                     phase -= dphase;
                     if (phase < 0) {
                        phase += 3;
                        curptr -= 4;
                     }
                     if ((pen-=brx) < 0) {
                        pen += bry;
                        phase--;
                        if (phase < 0) {
                           phase += 3;
                           curptr -= 4;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   curx = x;
   cury = y;
   int pel = (y * dwide + x);
   curptr = frameBuffer[0] + ((pel/3)<<2);
   phase = pel%3;
}

void CLineEngMON10::
drawDot()
{
   if      (phase==0) setFGY0();
   else if (phase==1) setFGY1();
   else               setFGY2();
}

void CLineEngMON10::
drawSpot(int x, int y)
{
   movTo(x,y);
   drawDot();
}

void CLineEngMON10::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int row,pel;
   unsigned char *endptr;
   int endphase;

#if ENDIAN == MTI_BIG_ENDIAN
   int color =  (fgColor[0]<<24) + 
               ((fgColor[1] + fgColor[2])<<16) +
               ((fgColor[3] + fgColor[4])<< 8) +
                (fgColor[5]);
#endif

#if ENDIAN == MTI_LITTLE_ENDIAN 
   int color =  (fgColor[5]<<24) +
               ((fgColor[3] + fgColor[4])<<16) +
               ((fgColor[1] + fgColor[2])<< 8) +
                (fgColor[0]);
#endif 

   for (int i=top;i<=bot;i++) {

      row = dwide * i; 

      pel = row + lft;
      curptr = frameBuffer[0] + ((pel/3)<<2);
      phase = pel%3;

      pel = row + rgt;
      endptr = frameBuffer[0] + ((pel/3)<<2);
      endphase = pel%3;

      if (curptr==endptr) {

         while (phase <= endphase) {

            if      (phase==0) setFGY0();
            else if (phase==1) setFGY1();
            else               setFGY2();

            phase++;
         }  
      }
      else  {

         if (phase==1) goto rlft1;

         if (phase==2) goto rlft2;

      rlft0:;

         setFGY0();

      rlft1:;

         setFGY1();

      rlft2:;

         setFGY2();

         curptr += 4;

         if (curptr < endptr) {

            unsigned int * curint = (unsigned int *)curptr;
            for (int i=0; i < (endptr - curptr)/4;i++)
               *curint++ = color;

         }

         curptr = endptr;

         setFGY0();

         if (endphase==0) goto endrow;

         setFGY1();

         if (endphase==1) goto endrow;

         setFGY2();

      endrow:;

      }
   }
}

void CLineEngMON10::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';
   
   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         int pel = (dwide * cury + x);
         curptr = frameBuffer[0] + ((pel/3)<<2);
         phase = pel%3; 

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  switch(phase) {

                     case 0: setFGY0(); break;

                     case 1: setFGY1(); break;

                     case 2: setFGY2(); break;
                  }

                  phase++;
                  if (phase > 2) {
                     phase -= 3;
                     curptr += 4;
                  }
               }
            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     switch(phase) {

                        case 0: setBGY0(); break;

                        case 1: setBGY1(); break;

                        case 2: setBGY2(); break;
                     }
                  }

                  phase++;
                  if (phase > 2) {
                     phase -= 3;
                     curptr += 4;
                  }
               }
            }

            fontscan = fontscan >> 1;
         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//              DPX MONO 10-bit packed
//
// This is like the SMPTE 10-bit packed, except that
// the byte order within each DWORD is swapped to be
// LITTLE-ENDIAN instead of BIG_ENDIAN
//
//////////////////////////////////////////////////////////

CLineEngMON10P::
CLineEngMON10P()
{
   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngMON10P::
~CLineEngMON10P()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
}

void CLineEngMON10P::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                        )
{
   dwide   = w;
   dhigh   = h;

   // this format is row-packed, so we'll want the
   // byte-length and phase-penalty for each row in
   // order to do the line algorithm incrementally
   dpitch  = (w/4)*5;
   dphase  = w%4;

   // we only support a non-interlaced frame buffer
   intrlc  = false;

   // calculate the frame buffer size
   fldSize = ((w*h+3)/4)*5;

   // basic font dimensions
   setFontSize(1);
}

int CLineEngMON10P::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];

   return(0);
}

void CLineEngMON10P::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
}

void CLineEngMON10P::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x00;
         fgColor[1] = 0x00;
         fgColor[2] = 0x00;
         fgColor[3] = 0x00;
         fgColor[4] = 0x00;
         fgColor[5] = 0x00;
         fgColor[6] = 0x00;
         fgColor[7] = 0x00;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0xff;
         fgColor[1] = 0x03;
         fgColor[2] = 0xfc;
         fgColor[3] = 0x0f;
         fgColor[4] = 0xf0;
         fgColor[5] = 0x3f;
         fgColor[6] = 0xc0;
         fgColor[7] = 0xff;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngMON10P::
setFGColor(MTI_UINT16  r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
   fgColor[0] = (y>>6)&0xff;
   fgColor[1] = (y>>14)&0x03;
   fgColor[2] = (y>>4)&0xfc;
   fgColor[3] = (y>>12)&0x0f;
   fgColor[4] = (y>>2)&0xf0;
   fgColor[5] = (y>>10)&0x3f;
   fgColor[6] = (y)&0xc0;
   fgColor[7] = (y>>8);
}

void CLineEngMON10P::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x00;
         bgColor[1] = 0x00;
         bgColor[2] = 0x00;
         bgColor[3] = 0x00;
         bgColor[4] = 0x00;
         bgColor[5] = 0x00;
         bgColor[6] = 0x00;
         bgColor[7] = 0x00;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0xff;
         bgColor[1] = 0x03;
         bgColor[2] = 0xfc;
         bgColor[3] = 0x0f;
         bgColor[4] = 0xf0;
         bgColor[5] = 0x3f;
         bgColor[6] = 0xc0;
         bgColor[7] = 0xff;
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngMON10P::
setBGColor(MTI_UINT16  r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
   bgColor[0] = (y>>6)&0xff;
   bgColor[1] = (y>>14)&0x03;
   bgColor[2] = (y>>4)&0xfc;
   bgColor[3] = (y>>12)&0x0f;
   bgColor[4] = (y>>2)&0xf0;
   bgColor[5] = (y>>10)&0x3f;
   bgColor[6] = (y)&0xc0;
   bgColor[7] = (y>>8);
}

void CLineEngMON10P::
setFGY0()
{
   curptr[0] = fgColor[0];
   curptr[1] = fgColor[1]+(curptr[1]&0xfc);
}

void CLineEngMON10P::
setFGY1()
{
   curptr[1] = (curptr[1]&0x03)+fgColor[2];
   curptr[2] = fgColor[3]+(curptr[2]&0xf0);
}

void CLineEngMON10P::
setFGY2()
{
   curptr[2] = (curptr[2]&0x0f)+fgColor[4];
   curptr[3] = fgColor[5]+(curptr[3]&0xc0);
}

void CLineEngMON10P::
setFGY3()
{
   curptr[3] = (curptr[3]&0x3f)+fgColor[6];
   curptr[4] = fgColor[7];
}

void CLineEngMON10P::
setBGY0()
{
   curptr[0] = bgColor[0];
   curptr[1] = bgColor[1]+(curptr[1]&0xfc);
}

void CLineEngMON10P::
setBGY1()
{
   curptr[1] = (curptr[1]&0x03)+bgColor[2];
   curptr[2] = bgColor[3]+(curptr[2]&0xf0);
}

void CLineEngMON10P::
setBGY2()
{
   curptr[2] = (curptr[2]&0x0f)+bgColor[4];
   curptr[3] = bgColor[5]+(curptr[3]&0xc0);
}

void CLineEngMON10P::
setBGY3()
{
   curptr[3] = (curptr[3]&0x3f)+bgColor[6];
   curptr[4] = bgColor[7];
}

void CLineEngMON10P::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   int pel = (y * dwide + x);
   curptr = frameBuffer[0] + ((pel/4)*5);
   phase = pel%4;
}

void CLineEngMON10P::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt

         if (phase==0) goto hr0;
         if (phase==1) goto hr1;
         if (phase==2) goto hr2;
         goto hr3;

         while (true) {

	    hr0:;

               if (delx--==0) break;
               setFGY0();

	    hr1:;

               if (delx--==0) break;
               setFGY1();

	    hr2:;

               if (delx--==0) break;
               setFGY2();

            hr3:;

               if (delx--==0) break;
               setFGY3();
               curptr += 5;
         }
      }
      else { // lft

         if (phase==3) goto hl3;
         if (phase==2) goto hl2;
         if (phase==1) goto hl1;
         goto hl0;

         while (true) {

            hl3:;

               if (delx++==0) break;
               setFGY3();

	    hl2:;

               if (delx++==0) break;
               setFGY2();

	    hl1:

               if (delx++==0) break;
               setFGY1();

	    hl0:

               if (delx++==0) break;
               setFGY0();
               curptr -= 5;
         }
      }
   }
   else {

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            while (dely-- > 0) {

               if      (phase==0) setFGY0();
               else if (phase==1) setFGY1();
               else if (phase==2) setFGY2();
               else               setFGY3();

               curptr += dpitch;
               phase += dphase;
               if (phase > 3) {
                  phase -= 4;
                  curptr += 5;
               }
            }
         }
         else { // up

            while (dely++ < 0) {

               if      (phase==0) setFGY0();
               else if (phase==1) setFGY1();
               else if (phase==2) setFGY2();
               else               setFGY3();

               curptr -= dpitch;
               phase -= dphase;
               if (phase < 0) {
                  phase += 4;
                  curptr -= 5;
               }
            }
         }
      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow

                  pen = delx;

                  while (delx-- > 0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else if (phase==2) setFGY2();
                     else               setFGY3();

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        curptr += dpitch;
                        phase += dphase;
                        if (phase > 3) {
                           phase -= 4;
                           curptr += 5;
                        }
                     }
                     phase++;
                     if (phase > 3) {
                        phase -= 4;
                        curptr += 5;
                     }

                  }
               }
               else { // dn-rgt steep

                  pen = dely;

                  while (dely--!=0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else if (phase==2) setFGY2();
                     else               setFGY3();

                     curptr += dpitch;
                     phase += dphase;
                     if (phase > 3) {
                        phase -= 4;
                        curptr += 5;
                     }
                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        phase++;
                        if (phase > 3) {
                           phase -= 4;
                           curptr += 5;
                        }
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow

                  pen = delx;

                  while (delx-- > 0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else if (phase==2) setFGY2();
                     else               setFGY3();

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        curptr += dpitch;
                        phase += dphase;
                        if (phase > 3) {
                           phase -= 4;
                           curptr += 5;
                        }
                     }
                     phase--;
                     if (phase < 0) {
                        phase += 4;
                        curptr -= 5;
                     }

                  }

               }
               else { // dn-lft steep

                  pen = dely;

                  while (dely--!=0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else if (phase==2) setFGY2();
                     else               setFGY3();

                     curptr += dpitch;
                     phase += dphase;
                     if (phase > 3) {
                        phase -= 4;
                        curptr += 5;
                     }
                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        phase--;
                        if (phase < 0) {
                           phase += 4;
                           curptr -= 5;
                        }
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow

                  pen = delx;

                  while (delx-- > 0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else if (phase==2) setFGY2();
                     else               setFGY3();

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        curptr -= dpitch;
                        phase -= dphase;
                        if (phase <0) {
                           phase += 4;
                           curptr -= 5;
                        }
                     }
                     phase++;
                     if (phase > 3) {
                        phase -= 4;
                        curptr += 5;
                     }

                  }

               }
               else { // up-rgt steep

                  pen = dely;

                  while (dely--!=0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else if (phase==2) setFGY2();
                     else               setFGY3();

                     curptr -= dpitch;
                     phase -= dphase;
                     if (phase < 0) {
                        phase += 4;
                        curptr -= 5;
                     }
                     if ((pen-=brx) < 0) {
                        pen += bry;
                        phase++;
                        if (phase > 3) {
                           phase -= 4;
                           curptr += 5;
                        }
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow

                  pen = delx;

                  while (delx-- > 0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else if (phase==2) setFGY2();
                     else               setFGY3();

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        curptr -= dpitch;
                        phase -= dphase;
                        if (phase < 0) {
                           phase += 4;
                           curptr -= 5;
                        }
                     }
                     phase--;
                     if (phase < 0) {
                        phase += 4;
                        curptr -= 5;
                     }
                  }
               }
               else { // up-lft steep

                  pen = dely;

                  while (dely--!=0) {

                     if      (phase==0) setFGY0();
                     else if (phase==1) setFGY1();
                     else if (phase==2) setFGY2();
                     else               setFGY3();

                     curptr -= dpitch;
                     phase -= dphase;
                     if (phase < 0) {
                        phase += 4;
                        curptr -= 5;
                     }
                     if ((pen-=brx) < 0) {
                        pen += bry;
                        phase--;
                        if (phase < 0) {
                           phase += 4;
                           curptr -= 5;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   curx = x;
   cury = y;
   int pel = (y * dwide + x);
   curptr = frameBuffer[0] + ((pel/4)*5);
   phase = pel%4;
}

void CLineEngMON10P::
drawDot()
{
   if      (phase==0) setFGY0();
   else if (phase==1) setFGY1();
   else if (phase==2) setFGY2();
   else               setFGY3();
}

void CLineEngMON10P::
drawSpot(int x, int y)
{
   movTo(x,y);
   drawDot();
}

void CLineEngMON10P::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int row,pel;
   unsigned char *endptr;
   int endphase;

   for (int i=top;i<=bot;i++) {

      row = dwide * i;
      pel = row + lft;
      curptr = frameBuffer[0] + ((pel/4)*5);
      phase = pel%4;

      pel = row + rgt;
      endptr = frameBuffer[0] + ((pel/4)*5);
      endphase = pel%4;

      if (curptr==endptr) {

         while (phase <= endphase) {

            if      (phase==0) setFGY0();
            else if (phase==1) setFGY1();
            else if (phase==2) setFGY2();
            else               setFGY3();

            phase++;
         }
      }
      else  {

         if (phase==1) goto rlft1;
         if (phase==2) goto rlft2;
         if (phase==3) goto rlft3;

      rlft0:;

         setFGY0();

      rlft1:;

         setFGY1();

      rlft2:;

         setFGY2();

      rlft3:;

         setFGY3();

         curptr += 5;

         if (curptr < endptr) {

            unsigned char *curbyt = curptr;
            for (int i=0; i < (endptr - curptr)/5;i++) {
               *curbyt++ = fgColor[0];
               *curbyt++ = fgColor[1]+fgColor[2];
               *curbyt++ = fgColor[3]+fgColor[4];
               *curbyt++ = fgColor[5]+fgColor[6];
               *curbyt++ = fgColor[7];
            }

         }

         curptr = endptr;

         setFGY0();

         if (endphase==0) goto endrow;

         setFGY1();

         if (endphase==1) goto endrow;

         setFGY2();

         if (endphase==2) goto endrow;

         setFGY3();

      endrow:;

      }
   }
}

void CLineEngMON10P::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         int pel = (dwide * cury + x);
         curptr = frameBuffer[0] + ((pel/4)*5);
         phase = pel%4;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  switch(phase) {

                     case 0: setFGY0(); break;

                     case 1: setFGY1(); break;

                     case 2: setFGY2(); break;

                     case 3: setFGY3(); break;
                  }

                  phase++;
                  if (phase > 3) {
                     phase -= 4;
                     curptr += 5;
                  }
               }
            }
            else {

               for (int k=0;k<pelMult;k++) {

                  if (bgndColor!=-1) {

                     switch(phase) {

                        case 0: setBGY0(); break;

                        case 1: setBGY1(); break;

                        case 2: setBGY2(); break;

                        case 3: setBGY3(); break;
                     }
                  }

                  phase++;
                  if (phase > 3) {
                     phase -= 4;
                     curptr += 5;
                  }
               }
            }

            fontscan = fontscan >> 1;
         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//              Y (mono) 16-bit
//
//////////////////////////////////////////////////////////

CLineEngMON16::CLineEngMON16()
{
}

CLineEngMON16::~CLineEngMON16()
{
   delete[] internalFrameBuffer;
}

void CLineEngMON16::setFrameBufferDimensions(
   int w,
   int h,
   int pitch,    // in bytes?
   bool il)      // ignored - can't be interlaced
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch >> 1; // WTF?? THIS MAKES IT THE SAME AS dwide ??? QQQ
   intrlc  = false;
   fldSize = pitch * dhigh;

   // basic font dimensions
   setFontSize(1);
}

int CLineEngMON16::allocateInternalFrameBuffer()
{
   internalFrameBuffer = (unsigned short *) new unsigned char[fldSize];
   targetFrameBuffer = internalFrameBuffer;

   return 0;
}

void CLineEngMON16::setExternalFrameBuffer(unsigned char **frmbuf)
{
   targetFrameBuffer = (unsigned short *) frmbuf[0];
}

void CLineEngMON16::setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor = 0x0000;
         fgndColor = col;
         break;

      case 1:
         fgColor = 0xffff;
         fgndColor = col;
         break;

      default:
         MTIassert(false);
         break;
   }
}

void CLineEngMON16::setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
   fgColor = ((y & 0xff) << 8) + ((y & 0xff00) >> 8); // WHY IS THIS BYTE-SWAPPED?? QQQ
}

void CLineEngMON16::setBGColor(int col)
{
   // WHAT THE FUCK IS THIS SHIT??
   switch (col)
   {
      case 0:
         bgColor = 0x0000;
         bgndColor = col;
         break;

      case 1:
         bgColor = 0xffff;
         bgndColor = col;
         break;

      case -1:
         bgndColor = col;
         break;

      default:
         MTIassert(false);
         break;
   }
}

void CLineEngMON16::setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
   bgColor = ((y & 0xff) << 8) + ((y & 0xff00) >> 8); // WHY IS THIS BYTE-SWAPPED?? QQQ
}

void CLineEngMON16::movTo(int x, int y)
{
   curx = x;
   cury = y;
   curptr = &targetFrameBuffer[(y * dwide) + x];
}

void CLineEngMON16::linTo(int x, int y)
{
#if 0
   int delx;
   int dely;
    int brx, bry, pen;
    OFENTRY *rowtbl, *rowy;

   if ((dely = (y - cury)) == 0)
   {
      // horz
      if ((delx = (x-curx)) >= 0)
      {
         // rgt
         while (delx-- != 0)
         {
            *(curptr)   = fgColor[0];

            curptr++;
            curx++;
         }
      }
      else
      {
         // lft
         while (delx++ != 0)
         {
            *curptr-- = fgColor[0];

            curx--;
         }
      }
   }
   else
   {
      rowtbl = rowTbl;

      if ((delx = (x - curx)) == 0)
      {
         // vert
         if ((dely = (y-cury)) > 0)
         {
            // dn
            while (dely-- != 0)
            {
               *curptr = fgColor[0];

               rowy = rowtbl + ++cury;
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;
            }

         }
         else
         {
            // up
            while (dely++ != 0)
            {
               *curptr = fgColor[0];

               rowy = rowtbl + (--cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

            }

         }

      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr += 3;
                     curx++;

                     if ((pen-=bry) <= 0) {
                        pen += brx;

                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr += 3;
                        curx++;
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr -= 3;
                     curx--;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr -= 3;
                        curx--;
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr += 3;
                     curx++;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr += 3;
                        curx++;
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr -= 3;
                     curx--;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr -= 3;
                        curx--;
                     }
                  }
               }
            }
         }
      }
   }
#endif
}

void CLineEngMON16::drawDot()
{
   *curptr = fgColor;
}

void CLineEngMON16::drawSpot(int x, int y)
{
   movTo(x,y);
   drawDot();
}

void CLineEngMON16::drawRectangle(int lft, int top, int rgt, int bot)
{
MTIassert(false);
#if 0
   // FILLED rectangle!
   int pxls = rgt - lft + 1;

   unsigned short *dst = linebuf;

   for (int i = 0; i < pxls; ++i)
   {
      *dst++ = fgColor;
   }

   pxls *= 2;

   for (int i = top; i <= bot; ++i)
   {
      unsigned char *rowbeg = (unsigned char *)(frameBuffer[rowTbl[i].rowfld] + rowTbl[i].rowoff + lft);
      MTImemcpy((unsigned char *) rowbeg, linebuf, pxls);
   }
#endif
}

void CLineEngMON16::drawNumeral(int x, int y, int num)
{
MTIassert(false);
#if 0
   if (num > 127) num = ' ';

   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + x*3;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  *(curptr)   = fgColor[0];
                  *(curptr+1) = fgColor[1];
                  *(curptr+2) = fgColor[2];

                  curptr += 3;

               }
            }
            else {

               if (bgndColor==-1) {

                  curptr += 3*pelMult;
               }
               else {

                  for (int k=0;k<pelMult;k++) {

                     *curptr     = bgColor[0];
                     *(curptr+1) = bgColor[1];
                     *(curptr+2) = bgColor[2];

                     curptr += 3;

                  }
               }
            }

            fontscan = fontscan >> 1;
         }
      }
   }
#endif
}

//////////////////////////////////////////////////////////
//
//              Y 16-bit Big-endian
//
//////////////////////////////////////////////////////////

void CLineEngMON16BE::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
#if ENDIAN == MTI_BIG_ENDIAN
   fgColor = y;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor = ((y & 0xff) << 8) + ((y & 0xff00) >> 8);
#endif
}

void CLineEngMON16BE::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
#if ENDIAN == MTI_BIG_ENDIAN
   bgColor = y;
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor = ((y & 0xff) << 8) + ((y & 0xff00) >> 8);
#endif
}

//////////////////////////////////////////////////////////
//
//              Y (Mono) 16-bit Little-endian
//
//////////////////////////////////////////////////////////

void CLineEngMON16LE::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
#if ENDIAN == MTI_BIG_ENDIAN
   fgColor = ((y & 0xff) << 8) + ((y & 0xff00) >> 8);
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   fgColor = y;
#endif
}

void CLineEngMON16LE::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   unsigned int y = (r + g + b) / 3;
#if ENDIAN == MTI_BIG_ENDIAN
   bgColor = ((y & 0xff) << 8) + ((y & 0xff00) >> 8);
#endif
#if ENDIAN == MTI_LITTLE_ENDIAN
   bgColor = y;
#endif
}

//////////////////////////////////////////////////////////
//
//              UYV 8-bit
//
//////////////////////////////////////////////////////////

CLineEngUYV8::
CLineEngUYV8()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngUYV8::
~CLineEngUYV8()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngUYV8::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                        )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch;
   intrlc  = il;
   fldSize = pitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned char[dpitch];

   // basic font dimensions

   setFontSize(1);
}

int CLineEngUYV8::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngUYV8::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = frmbuf[0];
   frameBuffer[1] = frmbuf[1];
}

void CLineEngUYV8::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor[0] = 0x80;
         fgColor[1] = 0x19;
         fgColor[2] = 0x80;
         fgndColor = col;
      break;

      case 1:
         fgColor[0] = 0x80;
         fgColor[1] = 0xc8;
         fgColor[2] = 0x80;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngUYV8::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   fgColor[0] = u>>8;
   fgColor[1] = y>>8;
   fgColor[2] = v>>8;
}

void CLineEngUYV8::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor[0] = 0x80;
         bgColor[1] = 0x19;
         bgColor[2] = 0x80;
         bgndColor = col;
      break;

      case 1:
         bgColor[0] = 0x80;
         bgColor[1] = 0xc8;
         bgColor[2] = 0x80;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngUYV8::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   bgColor[0] = u>>8;
   bgColor[1] = y>>8;
   bgColor[2] = v>>8;
}

void CLineEngUYV8::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + x*3;
}

void CLineEngUYV8::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {

            *(curptr)   = fgColor[0];
            *(curptr+1) = fgColor[1];
            *(curptr+2) = fgColor[2];

            curptr += 3;
            curx++;
         }
      }
      else { // lft
         while (delx++!=0) {

            *(curptr)   = fgColor[0];
            *(curptr+1) = fgColor[1];
            *(curptr+2) = fgColor[2];

            curptr -= 3;
            curx--;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            while (dely--!=0) {

               *(curptr)   = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];

               rowy = rowtbl + (++cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

            }

         }
         else { // up

            while (dely++!=0) {

               *(curptr)   = fgColor[0];
               *(curptr+1) = fgColor[1];
               *(curptr+2) = fgColor[2];

               rowy = rowtbl + (--cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

            }

         }

      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr += 3;
                     curx++;

                     if ((pen-=bry) <= 0) {
                        pen += brx;

                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr += 3;
                        curx++;
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr -= 3;
                     curx--;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr -= 3;
                        curx--;
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr += 3;
                     curx++;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr += 3;
                        curx++;
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     curptr -= 3;
                     curx--;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *(curptr)   = fgColor[0];
                     *(curptr+1) = fgColor[1];
                     *(curptr+2) = fgColor[2];

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx*3;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr -= 3;
                        curx--;
                     }
                  }
               }
            }
         }
      }
   }
}

void CLineEngUYV8::
drawDot()
{
   *(curptr)   = fgColor[0];
   *(curptr+1) = fgColor[1];
   *(curptr+2) = fgColor[2];
}

void CLineEngUYV8::
drawSpot(int x, int y)
{
   movTo(x,y);
   curptr[0] = fgColor[0];
   curptr[1] = fgColor[1];
   curptr[2] = fgColor[2];
}

void CLineEngUYV8::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int pxls = rgt - lft + 1;

   unsigned char *dst = linebuf;

   for (int i=0;i<pxls;i++) {
      *dst++ = fgColor[0];
      *dst++ = fgColor[1];
      *dst++ = fgColor[2];
   }

   pxls *= 3;
   for (int i=top;i<=bot;i++)
   {
      unsigned char *rowbeg = (unsigned char *)(frameBuffer[rowTbl[i].rowfld] + rowTbl[i].rowoff + lft*3);
      MTImemcpy((unsigned char *)rowbeg,linebuf,pxls);
   }
}

void CLineEngUYV8::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';
   
   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + x*3;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  *(curptr)   = fgColor[0];
                  *(curptr+1) = fgColor[1];
                  *(curptr+2) = fgColor[2];

                  curptr += 3;

               }
            }
            else {

               if (bgndColor==-1) {

                  curptr += 3*pelMult;
               }
               else {

                  for (int k=0;k<pelMult;k++) {

                     *curptr     = bgColor[0];
                     *(curptr+1) = bgColor[1];
                     *(curptr+2) = bgColor[2];

                     curptr += 3;

                  }
               }
            }

            fontscan = fontscan >> 1;

         }
      }
   }
}

//////////////////////////////////////////////////////////
//
//              UYV 10-bit
//
//////////////////////////////////////////////////////////

CLineEngUYV10::
CLineEngUYV10()
{
   rowTbl = NULL;

   linebuf = NULL;

   internalFrameBuffer[0] = NULL;
   internalFrameBuffer[1] = NULL;
}

CLineEngUYV10::
~CLineEngUYV10()
{
   if (internalFrameBuffer[0] != NULL)
      delete [] internalFrameBuffer[0];
   if (internalFrameBuffer[1] != NULL)
      delete [] internalFrameBuffer[1];

   delete [] linebuf;

   delete [] rowTbl;
}

void CLineEngUYV10::
setFrameBufferDimensions(
                         int w,
                         int h,
                         int pitch,
                         bool il
                        )
{
   dwide   = w;
   dhigh   = h;
   dpitch  = pitch>>2;
   intrlc  = il;
   fldSize = pitch*(dhigh>>(il?1:0));

   // table for accessing the rows of the framebuffer

   rowTbl = new OFENTRY[dhigh];
   for (int i=0;i<dhigh;i++) {
      rowTbl[i].rowfld = 0;
      rowTbl[i].rowoff = i*dpitch;
      if (il) {
         rowTbl[i].rowfld = i&1;
         rowTbl[i].rowoff = (i>>1)*dpitch;
      }
   }

   // table for use by rectangle draw

   linebuf = new unsigned int[dpitch];

   // basic font dimensions

   setFontSize(1);
}

int CLineEngUYV10::
allocateInternalFrameBuffer()
{
   internalFrameBuffer[0] = (unsigned int *)new unsigned char[fldSize];
   if (intrlc)
      internalFrameBuffer[1] = (unsigned int *)new unsigned char[fldSize];

   frameBuffer[0] = internalFrameBuffer[0];
   frameBuffer[1] = internalFrameBuffer[1];

   return(0);
}

void CLineEngUYV10::
setExternalFrameBuffer(unsigned char **frmbuf)
{
   frameBuffer[0] = (unsigned int *)frmbuf[0];
   frameBuffer[1] = (unsigned int *)frmbuf[1];
}

#if ENDIAN == MTI_BIG_ENDIAN
  #define UYVOFF 0x00920120
  #define UYVON  0x00820c20
#else
  #define UYVOFF 0x20019200
  #define UYVON  0x200c8200
#endif

void CLineEngUYV10::
setFGColor(int col)
{
   switch (col)
   {
      case 0:
         fgColor = UYVOFF;
         fgndColor = col;
      break;

      case 1:
         fgColor = UYVON;
         fgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngUYV10::
setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   unsigned int fgCol = (((unsigned int)v>>6)<<20)+
                        (((unsigned int)y>>6)<<10)+
                        (((unsigned int)u>>6)    );

#if ENDIAN == MTI_BIG_ENDIAN // xform LE to BE
   fgCol = ((fgCol&0xff)<<24)+
           ((fgCol&0xff00)<<8)+
           ((fgCol&0xff0000)>>8)+
           ((fgCol&0xff000000)>>24);
#endif

   fgColor = fgCol;
}

void CLineEngUYV10::
setBGColor(int col)
{
   switch (col)
   {
      case 0:
         bgColor = UYVOFF;
         bgndColor = col;
      break;

      case 1:
         bgColor = UYVON;
         bgndColor = col;
      break;

      case -1:
         bgndColor = col;
      break;

      default:
      break;
   }
}

void CLineEngUYV10::
setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b)
{
   MTI_UINT16 y, u, v;

   TransformRGBtoYUV(r,g,b,&y,&u,&v);

   unsigned int bgCol = (((unsigned int)v>>6)<<20)+
                        (((unsigned int)y>>6)<<10)+
                        (((unsigned int)u>>6)    );

#if ENDIAN == MTI_BIG_ENDIAN // xform LE to BE
   bgCol = ((bgCol&0xff)<<24)+
           ((bgCol&0xff00)<<8)+
           ((bgCol&0xff0000)>>8)+
           ((bgCol&0xff000000)>>24);
#endif

   bgColor = bgCol;
}

void CLineEngUYV10::
movTo(
        int x,
        int y
      )
{
   curx = x;
   cury = y;
   curptr = frameBuffer[rowTbl[y].rowfld] + rowTbl[y].rowoff + x;
}

void CLineEngUYV10::
linTo(
        int x,
        int y
      )
{
   int delx, dely;
    int brx,bry,pen;
    OFENTRY *rowtbl,*rowy;

   if ((dely=y-cury)==0) { // horz

      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {

            *curptr = fgColor;
            curptr++;
            curx++;
         }
      }
      else { // lft
         while (delx++!=0) {

            *curptr = fgColor;
            curptr--;
            curx--;
         }
      }
   }
   else {

      rowtbl = rowTbl;

      if ((delx=x-curx)==0) { // vert

         if ((dely=y-cury) > 0) { // dn

            while (dely--!=0) {

               *curptr = fgColor;
               rowy = rowtbl + (++cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

            }

         }
         else { // up

            while (dely++!=0) {

               *curptr = fgColor;
               rowy = rowtbl + (--cury);
               curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

            }

         }

      }
      else { // now both deltas are non-zero

         if (dely > 0) { // dn

            if (delx > 0) { // rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *curptr = fgColor;
                     curptr++;
                     curx++;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *curptr = fgColor;
                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr++;
                        curx++;
                     }
                  }
               }
            }
            else { // lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *curptr = fgColor;
                     curptr--;
                     curx--;

                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        rowy = rowtbl + (++cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *curptr = fgColor;

                     rowy = rowtbl + (++cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr--;
                        curx--;
                     }
                  }
               }
            }
         }
         else { // up

            dely = -dely;

            if (delx > 0) { // up-rgt

               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {

                     *curptr = fgColor;
                     curptr++;
                     curx++;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {

                     *curptr = fgColor;

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr++;
                        curx++;
                     }
                  }
               }
            }
            else { // up-lft

               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {

                     *curptr = fgColor;
                     curptr--;
                     curx--;

                     if ((pen-=bry) < 0) {
                        pen += brx;
                        rowy = rowtbl + (--cury);
                        curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {

                     *curptr = fgColor;

                     rowy = rowtbl + (--cury);
                     curptr = frameBuffer[rowy->rowfld] + rowy->rowoff + curx;

                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr--;
                        curx--;
                     }
                  }
               }
            }
         }
      }
   }
}

void CLineEngUYV10::
drawDot()
{
   *curptr = fgColor;
}

void CLineEngUYV10::
drawSpot(int x, int y)
{
   movTo(x,y);
   *curptr = fgColor;
}

void CLineEngUYV10::
drawRectangle(
               int lft,
               int top,
               int rgt,
               int bot
             )
{
   int pxls = rgt - lft + 1;

   unsigned int *dst = linebuf;

   for (int i=0;i<pxls;i++) {
      *dst++ = fgColor;
   }

   pxls *= 4;
   for (int i=top;i<=bot;i++)
   {
      unsigned char *rowbeg = (unsigned char *)(frameBuffer[rowTbl[i].rowfld] + rowTbl[i].rowoff + lft);
      MTImemcpy((unsigned char *)rowbeg,linebuf,pxls);
   }
}

void CLineEngUYV10::
drawNumeral(int x, int y, int num)
{
   if (num > 127) num = ' ';
   
   cury = y;

   for (int i=0;i<8;i++) {

      unsigned char fontbyte = numeral[num].row[i];

      for (int j=0;j<rowMult;j++,cury++) {

         curptr = frameBuffer[rowTbl[cury].rowfld] + rowTbl[cury].rowoff + x;

         unsigned char fontscan = 0x80;

         while (fontscan != 0) {

            if (fontscan&fontbyte) {

               for (int k=0;k<pelMult;k++) {

                  *curptr = fgColor;
                  curptr++;

               }
            }
            else {

               if (bgndColor==-1) {

                  curptr += pelMult;
               }
               else {

                  for (int k=0;k<pelMult;k++) {

                     *curptr     = bgColor;
                     curptr++;

                  }
               }
            }

            fontscan = fontscan >> 1;

         }
      }
   }
}
