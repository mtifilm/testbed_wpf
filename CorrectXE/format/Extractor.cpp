//
// #define AVG_UV // define this to average U and V on replace
//
// #define USE_MMX  // define this to use MMX for some codecs
//
// #define DPX12_PACKED // define this if 12-bit DPX is PACKED
// implementation file for class CExtractor
//
#include <stdio.h>
#include <stdlib.h>

#include "Extractor.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include "MTImalloc.h"

#ifdef __sgi
#include <sys/sysmp.h>
#include <unistd.h>
#endif

#ifdef __linux
#include "MTIio.h"
#endif

#define USE_MMX 1

#if !MTI_ASM_X86_INTEL
#undef USE_MMX
#endif

//////////////////////////////////////////////////////////////
//
// C E x t r a c t o r
//
// Author: Kurt Tolksdorf
// Begun:  7/26/01
//
// Added "WithinFrame"   methods 8/27/01
// Added "ActivePicture" methods 7/04/02
//
// Extract facility for conversion of image data to 16-bit 444
// progressive "intermediate" format. Three modes are available:
//
// i) "ActivePicture" - extract intermediate form for an
// entire frame of data, preserving any
// binary information in the vertical
// and horizontal intervals. The pitch
// of the data is determined by the
// width of the original image as
// specified in the source image format.
// In the case of monochrome formats, the
// luminance is not duplicated 3X
//
// ii) "Linear"        - extract 16-bit 444 data for a rectangular
// region into a linear array; the user
// must keep track of the pitch. In the case
// of monochrome formats, the luminance is
// duplicated 3X
//
// iii) "Within Frame"  - extract 16-bit 444 data for a rectangular
// region into a progressive frame with the
// same dimensions and pixel pitch as the
// source image. In the case of monochrome
// formats, the luminance is duplicated 3X
//
// Each mode has its corresponding inverse "replace" operation, which
// converts from 16-bit progressive to image format.
//
// Supported image formats are:
//
// YUV  8-bit interlaced and progressive
// YUV 10-bit interlaced and progressive
// DVS 10-bit interlaced and progressive
// UYV  8-bit interlaced and progressive
// UYV 10-bit interlaced and progressive
// RGB  8-bit progressive ("SGI")
// RGB 10-bit progressive ("DPX and CINEON")
// RGB 16-bit progressive "big endian" (sgi)
// RGB 16-bit progressive "little endian" (wintel)
// RGBA10-bit progressive ("DPX")
// LUM 10-bit progressive packed 3 pels in 4 bytes
// LUM 10-bit progressive packed 4 pels in 5 bytes
//
// Extract/replace "ActivePicture" is multithreaded for speed.

static void null(void *v, int iJob)
{
}

CExtractor::CExtractor()
{
#ifdef NO_MULTI
   nStripes = 1;
#else
   nStripes = 1;   // DO NOT SET THIS > 1!! IT IT DOESN'T WORK!!
#endif
}

CExtractor::~CExtractor()
{
}

static int getAlphaMatteSize(CExtractor *vp)
{
   int totPels = vp->frmWdth * vp->frmHght;
   int totByts;

   switch (vp->clipAlphaType)
   {
   case IF_ALPHA_MATTE_1_BIT:
      totByts = (totPels + 7) / 8;
      break;

   case IF_ALPHA_MATTE_8_BIT:
      totByts = totPels;
      break;

   case IF_ALPHA_MATTE_16_BIT:
      totByts = totPels * 2;
      break;

   default:
      totByts = 0;
      break;
   }

   return totByts;
}

////////////////////////////////////////////////////////////////////////////
//
// F O R M A T - S P E C I F I C   E X T R A C T   F U N C T I O N S

static void extractYUV_8(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left / 2) * 4;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // extract one line of image data as 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         dst[0] = src[1];
         dst[1] = src[0];
         dst[2] = src[2];

         dst[3] = src[3];
         dst[4] = src[0];
         dst[5] = src[2];

         src += 4;
         dst += 6;
      }
   }
}

static void extractYUV_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left / 2) * 5;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      unsigned int r0, r1;

      // extract one line of image data as 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((int *)src);
         r1 = (r0 << 8) + (src[4]);
#else
#if MTI_ASM_X86_INTEL
         _asm
         {
            mov ebx, src mov eax, [ebx]bswap eax mov r0, eax sal eax, 8 mov al, [ebx + 4]mov r1, eax
         }
#else
         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         r1 = (r0 << 8) + (src[4]);
#endif
#endif

         dst[0] = (r0 >> 12) & 0x3ff; // Y
         dst[1] = (r0 >> 22) & 0x3ff; // U
         dst[2] = (r0 >> 2) & 0x3ff; // V

         dst[3] = (r1) & 0x3ff; // Y
         dst[4] = dst[1]; // U
         dst[5] = dst[2]; // V

         src += 5;
         dst += 6;
      }
   }
}

static void extractDVS_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left / 6) * 18;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left / 6) * 16;
   int phase = vp->actRect.left % 6;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // extract one line of image data as 16-bit 444
      int prsCnt = actWdth / 2;

      unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
      r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
      r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
      r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
      r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
      r0 = *((int *)(src));
      r1 = *((int *)(src + 4));
      r2 = *((int *)(src + 8));
      r3 = *((int *)(src + 12));
#endif

      if (phase == 0)
      {
         goto p0;
      }
      if (phase == 2)
      {
         goto p2;
      }
      goto p4;

      while (true)
      {

p0: ;

         dst[0] = (r0 >> 10) & 0x3ff; // Y
         dst[1] = (r0) & 0x3ff; // U
         dst[2] = (r0 >> 20) & 0x3ff; // V

         dst[3] = (r1) & 0x3ff; // Y
         dst[4] = dst[1]; // U
         dst[5] = dst[2]; // V

         if (--prsCnt == 0)
         {
            break;
         }

p2: ;

         dst[6] = (r1 >> 20) & 0x3ff; // Y
         dst[7] = (r1 >> 10) & 0x3ff; // U
         dst[8] = (r2) & 0x3ff; // V

         dst[9] = (r2 >> 10) & 0x3ff; // Y
         dst[10] = dst[7]; // U
         dst[11] = dst[8]; // V

         if (--prsCnt == 0)
         {
            break;
         }

p4: ;

         dst[12] = (r3) & 0x3ff; // Y
         dst[13] = (r2 >> 20) & 0x3ff; // U
         dst[14] = (r3 >> 10) & 0x3ff; // V

         dst[15] = (r3 >> 20) & 0x3ff; // Y
         dst[16] = dst[13]; // U
         dst[17] = dst[14]; // V

         if (--prsCnt == 0)
         {
            break;
         }

         src += 16;
         dst += 18;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
         r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
         r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
         r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
         r0 = *((int *)(src));
         r1 = *((int *)(src + 4));
         r2 = *((int *)(src + 8));
         r3 = *((int *)(src + 12));
#endif

      }
   }
}

static void extractDVS_10a(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left / 6) * 18;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left / 6) * 16;
   int phase = vp->actRect.left % 6;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // extract one line of image data as 16-bit 444
      int prsCnt = actWdth / 2;

      unsigned int r0, r1, r2, r3;
#if ENDIAN == MTI_BIG_ENDIAN
      r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
      r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
      r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
      r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
      r0 = *((int *)(src));
      r1 = *((int *)(src + 4));
      r2 = *((int *)(src + 8));
      r3 = *((int *)(src + 12));
#endif

      if (phase == 0)
      {
         goto p0;
      }
      if (phase == 2)
      {
         goto p2;
      }
      goto p4;

      while (true)
      {

p0: ;

         dst[0] = ((r0 >> 6) & 0x3fc) + ((r0 >> 26) & 0x03); // Y
         dst[1] = ((r0 << 2) & 0x3fc) + ((r0 >> 28) & 0x03); // U
         dst[2] = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03); // V

         dst[3] = ((r1 << 2) & 0x3fc) + ((r1 >> 28) & 0x03); // Y
         dst[4] = dst[1]; // U
         dst[5] = dst[2]; // V

         if (--prsCnt == 0)
         {
            break;
         }

p2: ;

         dst[6] = ((r1 >> 14) & 0x3fc) + ((r1 >> 24) & 0x03); // Y
         dst[7] = ((r1 >> 6) & 0x3fc) + ((r1 >> 26) & 0x03); // U
         dst[8] = ((r2 << 2) & 0x3fc) + ((r2 >> 28) & 0x03); // V

         dst[9] = ((r2 >> 6) & 0x3fc) + ((r2 >> 26) & 0x03); // Y
         dst[10] = dst[7]; // U
         dst[11] = dst[8]; // V

         if (--prsCnt == 0)
         {
            break;
         }

p4: ;

         dst[12] = ((r3 << 2) & 0x3fc) + ((r3 >> 28) & 0x03); // Y
         dst[13] = ((r2 >> 14) & 0x3fc) + ((r2 >> 24) & 0x03); // U
         dst[14] = ((r3 >> 6) & 0x3fc) + ((r3 >> 26) & 0x03); // V

         dst[15] = ((r3 >> 14) & 0x3fc) + ((r3 >> 24) & 0x03); // Y
         dst[16] = dst[13]; // U
         dst[17] = dst[14]; // V

         if (--prsCnt == 0)
         {
            break;
         }

         src += 16;
         dst += 18;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
         r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
         r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
         r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
         r0 = *((int *)(src));
         r1 = *((int *)(src + 4));
         r2 = *((int *)(src + 8));
         r3 = *((int *)(src + 12));
#endif

      }
   }
}

static void extractDVS_10BE(void *v, int iJob) // CLIPSTER
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left / 6) * 18;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left / 6) * 16;
   int phase = vp->actRect.left % 6;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // extract one line of image data as 16-bit 444
      int prsCnt = actWdth / 2;

      unsigned int r0, r1, r2, r3;
#if ENDIAN == MTI_BIG_ENDIAN
      r0 = *((int *)(src));
      r1 = *((int *)(src + 4));
      r2 = *((int *)(src + 8));
      r3 = *((int *)(src + 12));
#else
#if MTI_ASM_X86_INTEL
      _asm
      { // swap endian
         mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
         eax mov eax, [ebx + 12]bswap eax mov r3, eax
      }
#else
      r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
      r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
      r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
      r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
#endif
#endif

      if (phase == 0)
      {
         goto p0;
      }
      if (phase == 2)
      {
         goto p2;
      }
      goto p4;

      while (true)
      {

p0: ;

         dst[0] = (r0 >> 12) & 0x3ff; // Y1
         dst[1] = (r0 >> 22) & 0x3ff; // U
         dst[2] = (r0 >> 2) & 0x3ff; // V

         dst[3] = (r1 >> 22) & 0x3ff; // Y2
         dst[4] = dst[1]; // U
         dst[5] = dst[2]; // V

         if (--prsCnt == 0)
         {
            break;
         }

p2: ;

         dst[6] = (r1 >> 2) & 0x3ff; // Y3
         dst[7] = (r1 >> 12) & 0x3ff; // U
         dst[8] = (r2 >> 22) & 0x3ff; // V

         dst[9] = (r2 >> 12) & 0x3ff; // Y4
         dst[10] = dst[7]; // U
         dst[11] = dst[8]; // V

         if (--prsCnt == 0)
         {
            break;
         }

p4: ;

         dst[12] = (r3 >> 22) & 0x3ff; // Y5
         dst[13] = (r2 >> 2) & 0x3ff; // U
         dst[14] = (r3 >> 12) & 0x3ff; // V

         dst[15] = (r3 >> 2) & 0x3ff; // Y6
         dst[16] = dst[13]; // U
         dst[17] = dst[14]; // V

         if (--prsCnt == 0)
         {
            break;
         }

         src += 16;
         dst += 18;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((int *)(src));
         r1 = *((int *)(src + 4));
         r2 = *((int *)(src + 8));
         r3 = *((int *)(src + 12));
#else
#if MTI_ASM_X86_INTEL
         _asm
         { // swap endian
            mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
            eax mov eax, [ebx + 12]bswap eax mov r3, eax
         }
#else
         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
         r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
         r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
#endif
#endif

      }
   }
}

static void extractUYV_8(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // extract one line of image data as 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         dst[0] = src[1]; // Y
         dst[1] = src[0]; // U
         dst[2] = src[2]; // V

         dst[3] = src[4]; // Y
         dst[4] = src[3]; // U
         dst[5] = src[5]; // V

         src += 6;
         dst += 6;
      }
   }
}

static void extractUYV_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left) * 4;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      unsigned int r0, r1;

      // extract one line of image data as 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
         r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
#else
         r0 = *((int *)(src));
         r1 = *((int *)(src + 4));
#endif

         dst[0] = (r0 >> 10) & 0x3ff; // Y
         dst[1] = (r0) & 0x3ff; // U
         dst[2] = (r0 >> 20) & 0x3ff; // V

         dst[3] = (r1 >> 10) & 0x3ff; // Y
         dst[4] = (r1) & 0x3ff; // U
         dst[5] = (r1 >> 20) & 0x3ff; // V

         src += 8;
         dst += 6;
      }
   }
}

static void extractRGB_8(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // extract one line of image data as 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         dst[0] = src[0]; // R
         dst[1] = src[1]; // G
         dst[2] = src[2]; // B

         dst[3] = src[3]; // R
         dst[4] = src[4]; // G
         dst[5] = src[5]; // B

         src += 6;
         dst += 6;
      }
   }
}

#ifdef _WIN64

static void extract_row_444_10_ASM(MTI_UINT8 *src, MTI_UINT16 *dst, int actWdth)
{
   __asm__(
         "		movl	%0, %%eax;			"
         "		sal	$2, %%eax;			"
         "		xorq	%%rbx, %%rbx;  	"
         "		movl  %%eax, %%ebx;		"
         "		addq	%%rsi, %%rbx;		"
         "		movl	$0x3ff03ff,%%ecx;	"
         "L0: 	movl	(%%rsi),%%eax; 	"
         "		bswap %%eax;				"
         "		movl	%%eax,%%edx;		"
         "		shrl	$6,%%edx;			"
         "		shrw	$6,%%dx;			   "
         "		roll	$16,%%edx;			"
         "		andl	%%ecx,%%edx;		"
         "		movl	%%edx,(%%rdi);		" // output two components RG
         "		movl	4(%%rsi),%%edx;	"
         "		bswap	%%edx;				"
         "		shl	$14,%%eax;			"
         "		rol	$10,%%edx;			"
         "		movw	%%dx,%%ax;			"
         "		rol	$16,%%eax;			"
         "		and	%%ecx,%%eax;	  	"
         "		movl	%%eax,4(%%rdi);	" // and BR
         "		shr	$6,%%edx;			"
         "		shr	$6,%%dx;			   "
         "		rol   $16,%%edx;			"
         "		and	%%ecx,%%edx;		"
         "		movl	%%edx,8(%%rdi);	" // and GB
         "		addq	$8,%%rsi;			"
         "		addq	$12,%%rdi;			"
         "		cmpq	%%rbx,%%rsi;		"
         "		jb		L0		  			   "
         :
         : "m"(actWdth), "S"(src), "D"(dst)
         : "eax", "ebx", "ecx", "edx");
}

#else

static void extract_row_444_10_ASM(MTI_UINT8 *src, MTI_UINT16 *dst, int actWdth)
{
   // this is about the same
   _asm
   {
      mov esi, src

            mov ebx, esi

            mov eax, actWdth

            sal eax, 2

            add ebx, eax

            mov ecx, 0x3ff03ff

            mov edi, dst

            /*
             Assume the dst rows were 32-bit aligned
             */

            $L0 : mov eax, [esi]

            bswap eax mov edx, eax

            shr edx, 6

            shr dx, 6

            rol edx, 16

            and edx, ecx mov[edi],

            edx // RG

            mov edx, [esi + 4]

            bswap edx shl eax, 14

            rol edx, 10

            mov ax, dx

            rol eax, 16

            and eax, ecx

            mov[edi + 4], eax // BR

            shr edx, 6

            shr dx, 6

            rol edx, 16

            and edx, ecx

            mov[edi + 8], edx // GB

            //// */

      add esi, 8

      add edi, 12

      cmp esi,

      ebx jb $L0
   }
}
#endif

#ifdef USE_MMX  // MMX

static void extract_row_444_10_MMX(MTI_UINT8 *src, MTI_UINT16 *dst, int actWdth)
{
   __int64 qMask_MM7; // 0x03FF000003FF0000LL;
   *(long*) &qMask_MM7 = 0x03FF0000UL;
   *(((long*)&qMask_MM7) + 1) = 0x03FF0000UL;

   // extract one line of image data as 16-bit 444

   MTI_UINT8 *srcStop = src + (actWdth * 4); // 4 bytes/pixel

   _asm
   {
      // Load up pointers and constants
      xor eax, eax mov esi, src mov edi, dst mov ebx, srcStop movq mm7, qword ptr[qMask_MM7]

            // while (src < srcStop) {
            @ exrowloop :

            // Take a pair of pixels packed 3_10bits_in_4_bytes_L format (big endian
            // order; the low-order 2 bits are 0) and unpack them into
            // 3_16bits_in_6_bytes format

            // Load the pixel pair (a doubleword each);
            // byte-swap the two doubles within the quad (not the whole quad!)
            movq mm0, qword ptr[esi] // mm0: 12345678
            movq mm1, mm0 // mm1: 12345678
            psrlw mm0, 8 // mm0: -1-3-5-7
            psllw mm1, 8 // mm1: 2-4-6-8-
            por mm0, mm1 // mm0: 21436587
            pshufw mm0, mm0, 0xB1 // mm0: 43218765

            movq mm1, mm0
            // mm1: 66666666 66555555 55554444 44444400 33333333 33222222 22221111 11111100
            movq mm2, mm0
            // mm2: 66666666 66555555 55554444 44444400 33333333 33222222 22221111 11111100
            psrld mm0, 22
            // mm0: 00000000 00000000 00000066 66666666 00000000 00000000 00000033 33333333
            pslld mm1, 4
            // mm1: 66666655 55555555 44444444 44000000 33333322 22222222 11111111 11000000
            pslld mm2, 14
            // mm2: 55555544 44444444 00000000 00000000 22222211 11111111 00000000 00000000
            pand mm1, mm7
            // mm1: 00000055 55555555 00000000 00000000 00000022 22222222 00000000 00000000
            pand mm2, mm7
            // mm2: 00000044 44444444 00000000 00000000 00000011 11111111 00000000 00000000

            por mm1, mm0
            // mm1: 00000055 55555555 00000066 66666666 00000022 22222222 00000033 33333333
            movd dword ptr[edi], mm1
            // *(long*)&dst[0] = 00000022 22222222 00000033 33333333

            por mm0, mm2
            // mm0: 00000044 44444444 00000066 66666666 00000011 11111111 00000033 33333333
            psrlq mm0, 16
            // mm0: 00000000 00000000 00000044 44444444 00000066 66666666 00000011 11111111
            movd dword ptr[edi + 0x4], mm0
            // *(long*)&dst[4] = 00000066 66666666 00000011 11111111

            psrlq mm1, 48
            // mm1: 00000000 00000000 00000000 00000000 00000000 00000000 00000055 55555555
            pshufw mm0, mm0, 0xFB
            // mm0: 00000000 00000000 00000000 00000000 00000044 44444444 00000000 00000000
            por mm0, mm1
            // mm0: 00000000 00000000 00000000 00000000 00000044 44444444 00000055 55555555
            movd dword ptr[edi + 0x8], mm0
            // *(long*)&dst[8] = 00000044 44444444 00000055 55555555

            add eax, 1 add esi, 8 // src
            add edi, 12 // dst
            cmp esi, ebx // srcStop
            jb @ exrowloop // (src < srcStop)
            // }  // while

            emms // Give the FPU back its registers
         }
}

#endif // asm

static void extractRGB_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = vp->actRect.left * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = vp->actRect.left * 4;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + (iJob * rowsPerStripe);
   int stripeRows = rowsPerStripe;
   bool copyAlpha = false;
   bool zeroAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
      copyAlpha = vp->copyAlpha;
      zeroAlpha = vp->zeroAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {
      MTI_UINT8 *src = srcImage[i % srcFieldCount];
      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;
      MTI_UINT16 *dst = dstPels + (i * dstPitch);

      // copy the horizontal interval data to the destination
      if (srcOffs > 0)
      {
         MTImemcpy((void *)dst, (void *)src, srcOffs);
      }

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // Bug in assembler code, overruns buffer if length is odd  QQQ
      if ((actWdth % 2) == 0)
      {
         extract_row_444_10_ASM(src, dst, actWdth);
      }
      else
      {
         for (int j = 0; j < actWdth; j++)
         {
            // extract one line of image data as 16-bit 444
             MTI_UINT32 r = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

            dst[0] = (r >> 22);
            dst[1] = (r >> 12) & 0x3ff;
            dst[2] = (r >> 2) & 0x3ff;

            src += 4;
            dst += 3;
         }
      }
   }

   // copy the alpha matte to the intermediate
   if (zeroAlpha)
   {
      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemset(alphadst, 0, (size_t)getAlphaMatteSize(vp));
   }
   else if (copyAlpha)
   {
      // -> Alpha map in native
      MTI_UINT8 *alphasrc = srcImage[0] + (actHght * srcPitch);

      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));

      MTImemcpy(alphadst, alphasrc, (size_t)getAlphaMatteSize(vp));
   }
}

static void extractRGB_10_A2(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;        // in COMPONENTS
   int dstOffs = vp->actRect.left * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;          // in BYTES
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = vp->actRect.left * 4;

   int actWdth = vp->actWdth;            // in PIXELS
   int actHght = vp->actHght;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + (iJob * rowsPerStripe);
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
   }

   bool copyAlpha = vp->copyAlpha;
   MTI_UINT8 *alphaDstStart = (MTI_UINT8*) (dstPels + (actHght * dstPitch));
   const int alphasPerByte = 8 / 2;
   int alphaBytesPerRow = int(std::ceil(double(actWdth) / alphasPerByte));
   MTI_UINT8 *alphaDst = alphaDstStart + (begRow * alphaBytesPerRow);
   MTI_UINT8 *alphaDstEnd = alphaDstStart + (stripeRows * alphaBytesPerRow);

   // We want to preincrement the shift instead of postincrement because
   // otherwise we need special code to not clear the next alpha byte after
   // the last real one, causing a crash if the buffer exactly fills a system
   // page. A value of 6 here will trigger the code to clear the shift and
   // zero out the first alpha byte.
   int alphaShift = 6;
   alphaDst -= 1;
   int alphaIndex = 0;

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {
      MTI_UINT8 *src = srcImage[i % srcFieldCount];
      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;
      MTI_UINT16 *dst = dstPels + (i * dstPitch);

      // copy the horizontal interval data to the destination
      if (srcOffs > 0)
      {
         MTImemcpy((void *)dst, (void *)src, srcOffs);
      }

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      for (int j = 0; j < actWdth; j++)
      {
         // extract one line of image data as 16-bit 444
          MTI_UINT32 r = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

         dst[0] = (r >> 22);
         dst[1] = (r >> 12) & 0x3ff;
         dst[2] = (r >> 2) & 0x3ff;

         if (copyAlpha)
         {
            // Note that this pre-increment works because we initialized
            // the alpha shift to -2. We want to do the *alphaDst = 0; before
            // setting the alpha bits to avoid doing it erroneously after the
            // last real alpha byte.
            if (alphaShift < 6)
            {
               alphaShift += 2;
            }
            else
            {
               ++alphaDst;
               //MTIassert(alphaDst < alphaDstEnd);
               if (alphaDst >= alphaDstEnd)
               {
                  TRACE_0(errout << "Alpha copy overwrite (" << (alphaDst - alphaDstStart) << " <= " << (alphaDstEnd - alphaDstStart) << ")");
                  alphaDst = alphaDstEnd - 1;
               }

               *alphaDst = 0;
               alphaShift = 0;
            }

            MTIassert((alphaDst - alphaDstStart) == (alphaIndex / alphasPerByte));
            ++alphaIndex;

            *alphaDst |= (r & 0x03) << alphaShift;
         }

         src += 4;
         dst += 3;
      }
   }
}

#ifndef DPX12_PACKED

static void extractRGB_12(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcWdth = vp->frmWdth;
   int srcPitch = ((srcWdth * 36 + 31) / 32) * 4;
   int srcLeft = vp->actRect.left;
   int srcOffs = (vp->actRect.left >> 3) * 36;

   int actHght = vp->actHght;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   // bool copyAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
      // copyAlpha = vp->copyAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[0];

      if (src == NULL)
      {
         continue;
      }

      src += i * srcPitch + (srcLeft >> 3) * 36;

      int srcPhase = (srcLeft % 8);

      MTI_UINT16 *dst = dstPels + i * dstPitch;

       MTI_UINT32 r0, r1, r2, r3, r4, r5, r6, r7, r8;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      dst += dstOffs;

      MTI_UINT16 *rowend = dst + dstPitch;

#if ENDIAN == MTI_BIG_ENDIAN
      r0 = *((unsigned int *)(src));
      r1 = *((unsigned int *)(src + 4));
      r2 = *((unsigned int *)(src + 8));
      r3 = *((unsigned int *)(src + 12));
      r4 = *((unsigned int *)(src + 16));
      r5 = *((unsigned int *)(src + 20));
      r6 = *((unsigned int *)(src + 24));
      r7 = *((unsigned int *)(src + 28));
      r8 = *((unsigned int *)(src + 32));
#else
#if MTI_ASM_X86_INTEL
      _asm
      {
         mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
         eax mov eax, [ebx + 12]bswap eax mov r3, eax mov eax, [ebx + 16]bswap eax mov r4, eax mov eax, [ebx + 20]bswap eax mov r5,
         eax mov eax, [ebx + 24]bswap eax mov r6, eax mov eax, [ebx + 28]bswap eax mov r7, eax mov eax, [ebx + 32]bswap eax mov r8, eax
      }
#else
      r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
      r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
      r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
      r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
      r4 = (src[16] << 24) + (src[17] << 16) + (src[18] << 8) + src[19];
      r5 = (src[20] << 24) + (src[21] << 16) + (src[22] << 8) + src[23];
      r6 = (src[24] << 24) + (src[25] << 16) + (src[26] << 8) + src[27];
      r7 = (src[28] << 24) + (src[29] << 16) + (src[30] << 8) + src[31];
      r8 = (src[32] << 24) + (src[33] << 16) + (src[34] << 8) + src[35];
#endif
#endif
      src += 36;

      if (srcPhase == 0)
      {
         goto p0;
      }
      if (srcPhase == 1)
      {
         goto p1;
      }
      if (srcPhase == 2)
      {
         goto p2;
      }
      if (srcPhase == 3)
      {
         goto p3;
      }
      if (srcPhase == 4)
      {
         goto p4;
      }
      if (srcPhase == 5)
      {
         goto p5;
      }
      if (srcPhase == 6)
      {
         goto p6;
      }
      goto p7;

      while (true)
      {

p0: ; // phase 0

         dst[0] = (r0) & 0xfff; // R
         dst[1] = (r0 >> 12) & 0xfff; // G
         dst[2] = ((r0 >> 24) + (r1 << 8)) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p1: ;

         dst[0] = (r1 >> 4) & 0xfff; // R
         dst[1] = (r1 >> 16) & 0xfff; // G
         dst[2] = ((r1 >> 28) + (r2 << 4)) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p2: ; // phase 2

         dst[0] = (r2 >> 8) & 0xfff; // R
         dst[1] = (r2 >> 20) & 0xfff; // G
         dst[2] = (r3) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p3: ;

         dst[0] = (r3 >> 12) & 0xfff; // R
         dst[1] = ((r3 >> 24) + (r4 << 8)) & 0xfff; // G
         dst[2] = (r4 >> 4) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p4: ; // phase 4

         dst[0] = (r4 >> 16) & 0xfff; // R
         dst[1] = ((r4 >> 28) + (r5 << 4)) & 0xfff; // G
         dst[2] = (r5 >> 8) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p5: ;

         dst[0] = (r5 >> 20) & 0xfff; // R
         dst[1] = (r6) & 0xfff; // G
         dst[2] = (r6 >> 12) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p6: ; // phase 6

         dst[0] = ((r6 >> 24) + (r7 << 8)) & 0xfff; // R
         dst[1] = (r7 >> 4) & 0xfff; // G
         dst[2] = (r7 >> 16) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p7: ;

         dst[0] = ((r7 >> 28) + (r8 << 4)) & 0xfff; // R
         dst[1] = (r8 >> 8) & 0xfff; // G
         dst[2] = (r8 >> 20) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src));
         r1 = *((unsigned int *)(src + 4));
         r2 = *((unsigned int *)(src + 8));
         r3 = *((unsigned int *)(src + 12));
         r4 = *((unsigned int *)(src + 16));
         r5 = *((unsigned int *)(src + 20));
         r6 = *((unsigned int *)(src + 24));
         r7 = *((unsigned int *)(src + 28));
         r8 = *((unsigned int *)(src + 32));
#else
#if MTI_ASM_X86_INTEL
         _asm
         {
            mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
            eax mov eax, [ebx + 12]bswap eax mov r3, eax mov eax, [ebx + 16]bswap eax mov r4, eax mov eax, [ebx + 20]bswap eax mov r5,
            eax mov eax, [ebx + 24]bswap eax mov r6, eax mov eax, [ebx + 28]bswap eax mov r7, eax mov eax, [ebx + 32]bswap eax mov r8, eax
         }
#else
         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
         r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
         r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
         r4 = (src[16] << 24) + (src[17] << 16) + (src[18] << 8) + src[19];
         r5 = (src[20] << 24) + (src[21] << 16) + (src[22] << 8) + src[23];
         r6 = (src[24] << 24) + (src[25] << 16) + (src[26] << 8) + src[27];
         r7 = (src[28] << 24) + (src[29] << 16) + (src[30] << 8) + src[31];
         r8 = (src[32] << 24) + (src[33] << 16) + (src[34] << 8) + src[35];
#endif
#endif
         src += 36;
      }
   }

#if 0
   if (zeroAlpha)
   {
      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemset(alphadst, 0, (size_t)getAlphaMatteSize(vp));
   }
   else if (copyAlpha)
   { // native to intermediate

      // -> Alpha map in native
      MTI_UINT8 *alphasrc = srcImage[0] + ((((actHght * srcWdth) * 36) + 31) / 32) * 4;

      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + actHght * dstPitch);

      // copy the alpha matte to the intermediate
      int alphasiz = getAlphaMatteSize(vp);
      for (int i = 0; i < alphasiz; i++)
      {
         alphadst[i] = alphasrc[i];
      }
   }
#endif
}

static void extractRGB_12_LR(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcWdth = vp->frmWdth;
   int srcPitch = ((srcWdth * 36 + 31) / 32) * 4;
   int srcLeft = vp->actRect.left;
   int srcOffs = (vp->actRect.left >> 3) * 36;

   int actHght = vp->actHght;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   // bool copyAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
      // copyAlpha = vp->copyAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[0];

      if (src == NULL)
      {
         continue;
      }

      src += i * srcPitch + (srcLeft >> 3) * 36;

      int srcPhase = (srcLeft % 8);

      MTI_UINT16 *dst = dstPels + i * dstPitch;

       MTI_UINT32 r0, r1, r2, r3, r4, r5, r6, r7, r8;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      dst += dstOffs;

      MTI_UINT16 *rowend = dst + dstPitch;

#if ENDIAN == MTI_BIG_ENDIAN
      r0 = *((unsigned int *)(src));
      r1 = *((unsigned int *)(src + 4));
      r2 = *((unsigned int *)(src + 8));
      r3 = *((unsigned int *)(src + 12));
      r4 = *((unsigned int *)(src + 16));
      r5 = *((unsigned int *)(src + 20));
      r6 = *((unsigned int *)(src + 24));
      r7 = *((unsigned int *)(src + 28));
      r8 = *((unsigned int *)(src + 32));
#else
#if MTI_ASM_X86_INTEL
      _asm
      {
         mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
         eax mov eax, [ebx + 12]bswap eax mov r3, eax mov eax, [ebx + 16]bswap eax mov r4, eax mov eax, [ebx + 20]bswap eax mov r5,
         eax mov eax, [ebx + 24]bswap eax mov r6, eax mov eax, [ebx + 28]bswap eax mov r7, eax mov eax, [ebx + 32]bswap eax mov r8, eax
      }
#else
      r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
      r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
      r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
      r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
      r4 = (src[16] << 24) + (src[17] << 16) + (src[18] << 8) + src[19];
      r5 = (src[20] << 24) + (src[21] << 16) + (src[22] << 8) + src[23];
      r6 = (src[24] << 24) + (src[25] << 16) + (src[26] << 8) + src[27];
      r7 = (src[28] << 24) + (src[29] << 16) + (src[30] << 8) + src[31];
      r8 = (src[32] << 24) + (src[33] << 16) + (src[34] << 8) + src[35];
#endif
#endif
      src += 36;

      if (srcPhase == 0)
      {
         goto p0;
      }
      if (srcPhase == 1)
      {
         goto p1;
      }
      if (srcPhase == 2)
      {
         goto p2;
      }
      if (srcPhase == 3)
      {
         goto p3;
      }
      if (srcPhase == 4)
      {
         goto p4;
      }
      if (srcPhase == 5)
      {
         goto p5;
      }
      if (srcPhase == 6)
      {
         goto p6;
      }
      goto p7;

      while (true)
      {

p0: ; // phase 0

         dst[0] = (r0 >> 20) & 0xfff; // R
         dst[1] = (r0 >> 8) & 0xfff; // G
         dst[2] = ((r0 << 4) + (r1 >> 28)) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p1: ;

         dst[0] = (r1 >> 16) & 0xfff; // R
         dst[1] = (r1 >> 4) & 0xfff; // G
         dst[2] = ((r1 << 8) + (r2 >> 24)) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p2: ; // phase 2

         dst[0] = (r2 >> 12) & 0xfff; // R
         dst[1] = (r2) & 0xfff; // G
         dst[2] = (r3 >> 20) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p3: ;

         dst[0] = (r3 >> 8) & 0xfff; // R
         dst[1] = ((r3 << 4) + (r4 >> 28)) & 0xfff; // G
         dst[2] = (r4 >> 16) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p4: ; // phase 4

         dst[0] = (r4 >> 4) & 0xfff; // R
         dst[1] = ((r4 << 8) + (r5 >> 24)) & 0xfff; // G
         dst[2] = (r5 >> 12) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p5: ;

         dst[0] = (r5) & 0xfff; // R
         dst[1] = (r6 >> 20) & 0xfff; // G
         dst[2] = (r6 >> 8) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p6: ; // phase 6

         dst[0] = ((r6 << 4) + (r7 >> 28)) & 0xfff; // R
         dst[1] = (r7 >> 16) & 0xfff; // G
         dst[2] = (r7 >> 4) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p7: ;

         dst[0] = ((r7 << 8) + (r8 >> 24)) & 0xfff; // R
         dst[1] = (r8 >> 12) & 0xfff; // G
         dst[2] = (r8) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src));
         r1 = *((unsigned int *)(src + 4));
         r2 = *((unsigned int *)(src + 8));
         r3 = *((unsigned int *)(src + 12));
         r4 = *((unsigned int *)(src + 16));
         r5 = *((unsigned int *)(src + 20));
         r6 = *((unsigned int *)(src + 24));
         r7 = *((unsigned int *)(src + 28));
         r8 = *((unsigned int *)(src + 32));
#else
#if MTI_ASM_X86_INTEL
         _asm
         {
            mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
            eax mov eax, [ebx + 12]bswap eax mov r3, eax mov eax, [ebx + 16]bswap eax mov r4, eax mov eax, [ebx + 20]bswap eax mov r5,
            eax mov eax, [ebx + 24]bswap eax mov r6, eax mov eax, [ebx + 28]bswap eax mov r7, eax mov eax, [ebx + 32]bswap eax mov r8, eax
         }
#else
         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
         r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
         r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
         r4 = (src[16] << 24) + (src[17] << 16) + (src[18] << 8) + src[19];
         r5 = (src[20] << 24) + (src[21] << 16) + (src[22] << 8) + src[23];
         r6 = (src[24] << 24) + (src[25] << 16) + (src[26] << 8) + src[27];
         r7 = (src[28] << 24) + (src[29] << 16) + (src[30] << 8) + src[31];
         r8 = (src[32] << 24) + (src[33] << 16) + (src[34] << 8) + src[35];
#endif
#endif
         src += 36;
      }
   }

#if 0
   if (zeroAlpha)
   {
      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemset(alphadst, 0, (size_t)getAlphaMatteSize(vp));
   }
   else if (copyAlpha)
   { // native to intermediate

      // -> Alpha map in native
      MTI_UINT8 *alphasrc = srcImage[0] + ((((actHght * srcWdth) * 36) + 31) / 32) * 4;

      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + actHght * dstPitch);

      // copy the alpha matte to the intermediate
      int alphasiz = getAlphaMatteSize(vp);
      for (int i = 0; i < alphasiz; i++)
      {
         alphadst[i] = alphasrc[i];
      }
   }
#endif
}

#else

static void extractRGB_12P(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcWdth = vp->frmWdth;
   int srcLeft = vp->actRect.left;
   int srcOffs = (vp->actRect.left >> 3) * 36;

   int actHght = vp->actHght;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   bool copyAlpha = false;
   bool zeroAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
      copyAlpha = vp->copyAlpha;
      zeroAlpha = vp->zeroAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[0];

      if (src == NULL)
      {
         continue;
      }

      int pel = i * srcWdth + srcLeft;

      src += (pel >> 3)*36;

      int srcPhase = (pel % 8);

      MTI_UINT16 *dst = dstPels + i * dstPitch;

       MTI_UINT32 r0, r1, r2, r3, r4, r5, r6, r7, r8;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      dst += dstOffs;

      MTI_UINT16 *rowend = dst + dstPitch;

#if ENDIAN == MTI_BIG_ENDIAN
      r0 = *((unsigned int *)(src));
      r1 = *((unsigned int *)(src + 4));
      r2 = *((unsigned int *)(src + 8));
      r3 = *((unsigned int *)(src + 12));
      r4 = *((unsigned int *)(src + 16));
      r5 = *((unsigned int *)(src + 20));
      r6 = *((unsigned int *)(src + 24));
      r7 = *((unsigned int *)(src + 28));
      r8 = *((unsigned int *)(src + 32));
#else
#if MTI_ASM_X86_INTEL
      _asm
      {
         mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
         eax mov eax, [ebx + 12]bswap eax mov r3, eax mov eax, [ebx + 16]bswap eax mov r4, eax mov eax, [ebx + 20]bswap eax mov r5,
         eax mov eax, [ebx + 24]bswap eax mov r6, eax mov eax, [ebx + 28]bswap eax mov r7, eax mov eax, [ebx + 32]bswap eax mov r8, eax
      }
#else
      r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
      r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
      r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
      r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
      r4 = (src[16] << 24) + (src[17] << 16) + (src[18] << 8) + src[19];
      r5 = (src[20] << 24) + (src[21] << 16) + (src[22] << 8) + src[23];
      r6 = (src[24] << 24) + (src[25] << 16) + (src[26] << 8) + src[27];
      r7 = (src[28] << 24) + (src[29] << 16) + (src[30] << 8) + src[31];
      r8 = (src[32] << 24) + (src[33] << 16) + (src[34] << 8) + src[35];
#endif
#endif
      src += 36;

      if (srcPhase == 0)
      {
         goto p0;
      }
      if (srcPhase == 1)
      {
         goto p1;
      }
      if (srcPhase == 2)
      {
         goto p2;
      }
      if (srcPhase == 3)
      {
         goto p3;
      }
      if (srcPhase == 4)
      {
         goto p4;
      }
      if (srcPhase == 5)
      {
         goto p5;
      }
      if (srcPhase == 6)
      {
         goto p6;
      }
      goto p7;

      while (true)
      {

p0: ; // phase 0

         dst[0] = (r0) & 0xfff; // R
         dst[1] = (r0 >> 12) & 0xfff; // G
         dst[2] = ((r0 >> 24) + (r1 << 8)) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p1: ;

         dst[0] = (r1 >> 4) & 0xfff; // R
         dst[1] = (r1 >> 16) & 0xfff; // G
         dst[2] = ((r1 >> 28) + (r2 << 4)) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p2: ; // phase 2

         dst[0] = (r2 >> 8) & 0xfff; // R
         dst[1] = (r2 >> 20) & 0xfff; // G
         dst[2] = (r3) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p3: ;

         dst[0] = (r3 >> 12) & 0xfff; // R
         dst[1] = ((r3 >> 24) + (r4 << 8)) & 0xfff; // G
         dst[2] = (r4 >> 4) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p4: ; // phase 4

         dst[0] = (r4 >> 16) & 0xfff; // R
         dst[1] = ((r4 >> 28) + (r5 << 4)) & 0xfff; // G
         dst[2] = (r5 >> 8) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p5: ;

         dst[0] = (r5 >> 20) & 0xfff; // R
         dst[1] = (r6) & 0xfff; // G
         dst[2] = (r6 >> 12) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p6: ; // phase 6

         dst[0] = ((r6 >> 24) + (r7 << 8)) & 0xfff; // R
         dst[1] = (r7 >> 4) & 0xfff; // G
         dst[2] = (r7 >> 16) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

p7: ;

         dst[0] = ((r7 >> 28) + (r8 << 4)) & 0xfff; // R
         dst[1] = (r8 >> 8) & 0xfff; // G
         dst[2] = (r8 >> 20) & 0xfff; // B
         dst += 3;

         if (dst == rowend)
         {
            break;
         }

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src));
         r1 = *((unsigned int *)(src + 4));
         r2 = *((unsigned int *)(src + 8));
         r3 = *((unsigned int *)(src + 12));
         r4 = *((unsigned int *)(src + 16));
         r5 = *((unsigned int *)(src + 20));
         r6 = *((unsigned int *)(src + 24));
         r7 = *((unsigned int *)(src + 28));
         r8 = *((unsigned int *)(src + 32));
#else
#if MTI_ASM_X86_INTEL
         _asm
         {
            mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
            eax mov eax, [ebx + 12]bswap eax mov r3, eax mov eax, [ebx + 16]bswap eax mov r4, eax mov eax, [ebx + 20]bswap eax mov r5,
            eax mov eax, [ebx + 24]bswap eax mov r6, eax mov eax, [ebx + 28]bswap eax mov r7, eax mov eax, [ebx + 32]bswap eax mov r8, eax
         }
#else
         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
         r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
         r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
         r4 = (src[16] << 24) + (src[17] << 16) + (src[18] << 8) + src[19];
         r5 = (src[20] << 24) + (src[21] << 16) + (src[22] << 8) + src[23];
         r6 = (src[24] << 24) + (src[25] << 16) + (src[26] << 8) + src[27];
         r7 = (src[28] << 24) + (src[29] << 16) + (src[30] << 8) + src[31];
         r8 = (src[32] << 24) + (src[33] << 16) + (src[34] << 8) + src[35];
#endif
#endif
         src += 36;
      }
   }

#if 0
   if (zeroAlpha)
   {
      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemset(alphadst, 0, (size_t)getAlphaMatteSize(vp));
   }
   else if (copyAlpha)
   { // native to intermediate

      // -> Alpha map in native
      MTI_UINT8 *alphasrc = srcImage[0] + ((((actHght * srcWdth) * 36) + 31) / 32) * 4;

      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + actHght * dstPitch);

      // copy the alpha matte to the intermediate
      int alphasiz = getAlphaMatteSize(vp);
      for (int i = 0; i < alphasiz; i++)
      {
         alphadst[i] = alphasrc[i];
      }
   }
#endif
}
#endif

static void extractRGB_12_L_LE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3; // in COMPONENTS

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch; // in BYTES
   if (srcImage[0] == NULL)
   {
      // Really an error!
      return;
   }

   int rowsPerStripe = vp->actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - (iJob * rowsPerStripe);
   }

   for (int i = begRow; i < (begRow + stripeRows); ++i)
   {
      MTI_UINT16 *src = (MTI_UINT16*)(srcImage[0] + (i * srcPitch));
      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // Convert to intermediate by moving the 12 bits to the low end of
      // the 16-bit container.
      int nComponents = vp->actWdth * 3;
      for (int j = 0; j < nComponents; ++j)
      {
         dst[j] = src[j] >> 4;
      }
   }
}

static void extractRGB_12_L_BE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3; // in COMPONENTS

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch; // in BYTES
   if (srcImage[0] == NULL)
   {
      // Really an error!
      return;
   }

   int rowsPerStripe = vp->actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - (iJob * rowsPerStripe);
   }

   for (int i = begRow; i < (begRow + stripeRows); ++i)
   {
      MTI_UINT16 *src = (MTI_UINT16*)(srcImage[0] + (i * srcPitch));
      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // Convert to intermediate by moving the 12 bits to the low end of
      // the 16-bit container and byte-swapping.
      int nComponents = vp->actWdth * 3;
      for (int j = 0; j < nComponents; ++j)
      {
         ////         dst[j] = (src[j] >> 12) + ((src[j] << 4) & 0xFF00);
         dst[j] = ((src[j] >> 12) + (src[j] << 4)) & 0x0FFF;
      }
   }
}

static void extractRGB_16_LE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3; // in COMPONENTS

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch; // in BYTES

   int actWdth = vp->actWdth; // in PIXELS
   int actHght = vp->actHght;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (actWdth - 1));
   MTIassert(vp->actRect.bottom == (actHght - 1));
   MTIassert(vp->frmFieldCount == 1);

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   bool copyAlpha = false;
   bool zeroAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
      copyAlpha = vp->copyAlpha;
      zeroAlpha = vp->zeroAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); ++i)
   {
      MTI_UINT8 *srcb = srcImage[0];
      if (srcb == NULL)
      {
         continue;
      }

      // Yay! It's just a memcpy!
      srcb += i * srcPitch;
      MTI_UINT16 *dst = dstPels + (i * dstPitch);
      MTImemcpy(dst, srcb, actWdth * 6);
   }

   // Copy non-interleaved alpha from native to intermediate buffer
   // if present (alpha immediately follows the image data in both cases).
   if (zeroAlpha)
   {
      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemset(alphadst, 0, (size_t)getAlphaMatteSize(vp));
   }
   else if (copyAlpha)
   {
      MTI_UINT8 *alphasrc = srcImage[0] + (actHght * srcPitch);
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemcpy(alphadst, alphasrc, getAlphaMatteSize(vp));
   }
}

static void extractRGBA_16_LE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3; // in COMPONENTS

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch; // in BYTES

   int actWdth = vp->actWdth; // in PIXELS
   int actHght = vp->actHght;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (actWdth - 1));
   MTIassert(vp->actRect.bottom == (actHght - 1));
   MTIassert(vp->frmFieldCount == 1);

   int rowsPerStripe = actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
   }

   bool copyAlpha = vp->copyAlpha;
   MTI_UINT16 *alphadst = dstPels + (actHght * dstPitch) + (begRow * actWdth);

   for (int i = begRow; i < (begRow + stripeRows); ++i)
   {
      MTI_UINT8 *srcb = srcImage[0];
      if (srcb == NULL)
      {
         continue;
      }

      // Careful - srcPitch is in bytes, but dstPitch is in components
      MTI_UINT16 *src = (MTI_UINT16*)(srcb + (i * srcPitch));
      MTI_UINT16 *dst = dstPels + (i * dstPitch);

      // Extract one line of RGBA image data to 16-bit RGB intermediate buffer
      // and maybe copy the alpha as well.
      for (int j = 0; j < actWdth; ++j)
      {
         dst[0] = src[0];
         dst[1] = src[1];
         dst[2] = src[2];
         if (copyAlpha)
         {
            *(alphadst++) = src[3];
         }

         src += 4;
         dst += 3;
      }
   }
}

static void extractBGRA_16_LE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3; // in COMPONENTS

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch; // in BYTES

   int actWdth = vp->actWdth; // in PIXELS
   int actHght = vp->actHght;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (actWdth - 1));
   MTIassert(vp->actRect.bottom == (actHght - 1));
   MTIassert(vp->frmFieldCount == 1);

   int rowsPerStripe = actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
   }

   bool copyAlpha = vp->copyAlpha;
   MTI_UINT16 *alphadst = dstPels + (actHght * dstPitch) + (begRow * actWdth);

   for (int i = begRow; i < (begRow + stripeRows); ++i)
   {
      MTI_UINT8 *srcb = srcImage[0];
      if (srcb == NULL)
      {
         continue;
      }

      // Careful - srcPitch is in bytes, but dstPitch is in components
      MTI_UINT16 *src = (MTI_UINT16*)(srcb + (i * srcPitch));
      MTI_UINT16 *dst = dstPels + (i * dstPitch);

      // Extract one line of RGBA image data to 16-bit RGB intermediate buffer
      // and maybe copy the alpha as well.
      for (int j = 0; j < actWdth; ++j)
      {
         dst[2] = src[0];
         dst[1] = src[1];
         dst[0] = src[2];
         if (copyAlpha)
         {
            *(alphadst++) = src[3];
         }

         src += 4;
         dst += 3;
      }
   }
}

static void extractRGB_Half(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;
   const int NumberOfComponentsPerPixel = 3;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (vp->actWdth - 1));
   MTIassert(vp->actRect.bottom == (vp->actHght - 1));
   MTIassert(vp->frmFieldCount == 1);
   MTIassert(vp->frame != nullptr && vp->frame[0] != nullptr);
   if (vp->frame == nullptr || vp->frame[0] == nullptr)
   {
      return;
   }

   MTI_UINT16 *srcImage = (MTI_UINT16*) vp->frame[0];
   MTI_UINT16 *dstImage = (MTI_UINT16*) vp->pels;

   int rowsPerStripe = vp->actHght / vp->nStripes;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - (iJob * rowsPerStripe);
   }

   int numberOfComponentsPerNormalStripe = rowsPerStripe * vp->actWdth * NumberOfComponentsPerPixel;
   int numberOfComponentsInThisStripe = stripeRows * vp->actWdth * NumberOfComponentsPerPixel;
   int startOfThisStripe = iJob * numberOfComponentsPerNormalStripe;
   int endOfThisStripe = startOfThisStripe + numberOfComponentsInThisStripe;

   for (int compIndex = startOfThisStripe; compIndex < endOfThisStripe; compIndex += 3)
   {
      dstImage[compIndex] = srcImage[compIndex + 2] /* ^ FlipSignBit */ ; // B
      dstImage[compIndex + 1] = srcImage[compIndex + 1] /* ^ FlipSignBit */ ; // G
      dstImage[compIndex + 2] = srcImage[compIndex] /* ^ FlipSignBit */ ; // R
   }
}

static void extractRGBA_Half(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3; // in COMPONENTS

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch; // in BYTES

   int actWdth = vp->actWdth; // in PIXELS
   int actHght = vp->actHght;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (actWdth - 1));
   MTIassert(vp->actRect.bottom == (actHght - 1));
   MTIassert(vp->frmFieldCount == 1);

   int rowsPerStripe = actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
   }

   bool copyAlpha = vp->copyAlpha;
   MTI_UINT16 *alphadst = dstPels + (actHght * dstPitch) + (begRow * actWdth);

   for (int i = begRow; i < (begRow + stripeRows); ++i)
   {
      MTI_UINT8 *srcb = srcImage[0];
      if (srcb == NULL)
      {
         continue;
      }

      // Careful - srcPitch is in bytes, but dstPitch is in components
      MTI_UINT16 *src = (MTI_UINT16*)(srcb + (i * srcPitch));
      MTI_UINT16 *dst = dstPels + (i * dstPitch);

      // Extract one line of RGBA image data to 16-bit RGB intermediate buffer
      // and maybe copy the alpha as well.
      const MTI_UINT64 FlipSignBits = 0x0000800080008000;
      int nDstComponentsPerRow = actWdth * 3;
      for (int j = 0; j < nDstComponentsPerRow; j += 3)
      {
         MTI_UINT64 temp = (*(MTI_UINT64*) src) /* ^ FlipSignBits */ ;
         dst[j] = temp >> 48; // R
         dst[j + 1] = temp >> 32; // G
         dst[j + 2] = temp >> 16; // B

         if (copyAlpha)
         {
            *(alphadst++) = temp;
         }

         src += 4;
      }
   }
}

static void extractBGR_8(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // extract one line of image data as 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         dst[0] = src[2]; // R
         dst[1] = src[1]; // G
         dst[2] = src[0]; // B

         dst[3] = src[5]; // R
         dst[4] = src[4]; // G
         dst[5] = src[3]; // B

         src += 6;
         dst += 6;
      }
   }
}

static void extractBGR_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;
   int srcFieldCount = vp->frmFieldCount;
   int srcOffs = (vp->actRect.left) * 4;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   bool copyAlpha = false;
   bool zeroAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
      copyAlpha = vp->copyAlpha;
      zeroAlpha = vp->zeroAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      src += (i / srcFieldCount) * srcPitch;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

       MTI_UINT32 r0, r1;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, srcOffs);

      // now set up to extract the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // these aren't so fast
      for (int j = 0; j < actWdth / 2; j++)
      {

         // extract one line of image data as 16-bit 444
         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];

         dst[2] = (r0 >> 22) & 0x3ff;
         dst[1] = (r0 >> 12) & 0x3ff;
         dst[0] = (r0 >> 2) & 0x3ff;

         dst[5] = (r1 >> 22) & 0x3ff;
         dst[4] = (r1 >> 12) & 0x3ff;
         dst[3] = (r1 >> 2) & 0x3ff;

         src += 8;
         dst += 6;
      }
   }

   // Copy non-interleaved alpha from native to intermediate buffer
   // if present (alpha immediately follows the image data in both cases).
   if (zeroAlpha)
   {
      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemset(alphadst, 0, (size_t)getAlphaMatteSize(vp));
   }
   if (copyAlpha)
   {
      MTI_UINT8 *alphasrc = srcImage[0] + (actHght * srcPitch);
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemcpy(alphadst, alphasrc, getAlphaMatteSize(vp));
   }
}

static void extractBGR_16_LE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3; // in COMPONENTS

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch; // in BYTES
   int srcFieldCount = vp->frmFieldCount;

   int actWdth = vp->actWdth; // in PIXELS
   int actHght = vp->actHght;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (actWdth - 1));
   MTIassert(vp->actRect.bottom == (actHght - 1));
   MTIassert(srcFieldCount == 1);

   int rowsPerStripe = actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   bool copyAlpha = false;
   bool zeroAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
      copyAlpha = vp->copyAlpha;
      zeroAlpha = vp->zeroAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); ++i)
   {
      MTI_UINT8 *srcb = srcImage[0];
      if (srcb == NULL)
      {
         continue;
      }

      MTI_UINT16 *src = (MTI_UINT16*)(srcb + (i * srcPitch));
      MTI_UINT16 *dst = dstPels + (i * dstPitch);

      // Extract one line of BGR image data, converting to RGB.
      int nComponents = actWdth * 3;
      for (int j = 0; j < nComponents; j += 3)
      {
         dst[j + 2] = src[j];
         dst[j + 1] = src[j + 1];
         dst[j] = src[j + 2];
      }
   }

   // Copy non-interleaved alpha from native to intermediate buffer
   // if present (alpha immediately follows the image data in both cases).
   if (zeroAlpha)
   {
      // -> Alpha map in intermediate
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemset(alphadst, 0, (size_t)getAlphaMatteSize(vp));
   }
   if (copyAlpha)
   {
      MTI_UINT8 *alphasrc = srcImage[0] + (actHght * srcPitch);
      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
      MTImemcpy(alphadst, alphasrc, getAlphaMatteSize(vp));
   }
}

static void extractRGBA_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;

   MTI_UINT16 *alphPels = vp->pels + dstPitch * vp->frmHght;
   int alphPitch = vp->frmWdth;

   MTI_UINT8 **srcImage = vp->frame;
   int srcWdth = vp->frmWdth;
   int srcFieldCount = vp->frmFieldCount;
   int srcLeft = vp->actRect.left;
   int srcPhase;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      int rowcnt = actWdth;

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      srcPhase = i * srcWdth + srcLeft;
      src += (srcPhase / 3) << 4;
      srcPhase %= 3;

      MTI_UINT16 *dst = dstPels + i * dstPitch;
      MTI_UINT16 *alphdst = alphPels + i * alphPitch;

      MTI_UINT32 r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
      r0 = *((int *)(src));
      r1 = *((int *)(src + 4));
      r2 = *((int *)(src + 8));
      r3 = *((int *)(src + 12));
#else
#if MTI_ASM_X86_INTEL
      _asm
      { // swap endian
         mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
         eax mov eax, [ebx + 12]bswap eax mov r3, eax
      }
#else
      r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
      r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
      r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
      r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
#endif
#endif

      if (srcPhase == 0)
      {
         goto p0;
      }
      if (srcPhase == 1)
      {
         dst -= 3;
         alphdst -= 1;
         goto p1;
      }
      else
      {
         dst -= 6;
         alphdst -= 2;
         goto p2;
      }

      while (true)
      {

p0: ;

//         if (dst >= (dstPels + ((i + 1) * dstPitch)))
//         {
//            DBTRACE(dst - (dstPels + (i * dstPitch)))
//         }
//         if (alphdst >= (alphPels + ((i + 1) * alphPitch)))
//         {
//            DBTRACE(alphdst - (alphPels + (i * alphPitch)))
//         }

         dst[0] = (r0 >> 22) & 0x3ff; // R
         dst[1] = (r0 >> 12) & 0x3ff; // G
         dst[2] = (r0 >> 2) & 0x3ff; // B

         alphdst[0] = (r1 >> 22) & 0x3ff; // A

         if (--rowcnt == 0)
         {
            break;
         }

p1: ;

//         if ((dst + 3) >= (dstPels + ((i + 1) * dstPitch)))
//         {
//            DBTRACE(dst - (dstPels + (i * dstPitch)))
//         }
//         if ((alphdst + 1) >= (alphPels + ((i + 1) * alphPitch)))
//         {
//            DBTRACE(alphdst - (alphPels + (i * alphPitch)))
//         }

         dst[3] = (r1 >> 12) & 0x3ff; // R
         dst[4] = (r1 >> 2) & 0x3ff; // G
         dst[5] = (r2 >> 22) & 0x3ff; // B

         alphdst[1] = (r2 >> 12) & 0x3ff; // A

         if (--rowcnt == 0)
         {
            break;
         }

p2: ;

//         if ((dst + 6) >= (dstPels + ((i + 1) * dstPitch)))
//         {
//            DBTRACE(dst - (dstPels + (i * dstPitch)))
//         }
//         if ((alphdst + 2) >= (alphPels + ((i + 1) * alphPitch)))
//         {
//            DBTRACE(alphdst - (alphPels + (i * alphPitch)))
//         }

         dst[6] = (r2 >> 2) & 0x3ff; // R
         dst[7] = (r3 >> 22) & 0x3ff; // G
         dst[8] = (r3 >> 12) & 0x3ff; // B

         alphdst[2] = (r3 >> 2) & 0x3ff; // A

         if (--rowcnt == 0)
         {
            break;
         }

         src += 16;
         dst += 9;
         alphdst += 3;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((int *)(src));
         r1 = *((int *)(src + 4));
         r2 = *((int *)(src + 8));
         r3 = *((int *)(src + 12));
#else
#if MTI_ASM_X86_INTEL
         _asm
         { // swap endian
            mov ebx, src
            mov eax, [ebx]
            bswap eax
            mov r0, eax
            mov eax, [ebx + 4]
            bswap eax
            mov r1, eax
            mov eax, [ebx + 8]
            bswap eax
            mov r2, eax
            mov eax, [ebx + 12]
            bswap eax
            mov r3, eax
         }
#else
         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
         r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
         r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
#endif
#endif
      }
   }
}

static void extractBGRA_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;

   MTI_UINT16 *alphPels = vp->pels + dstPitch * vp->frmHght;
   int alphPitch = vp->frmWdth;

   MTI_UINT8 **srcImage = vp->frame;
   int srcWdth = vp->frmWdth;
   int srcFieldCount = vp->frmFieldCount;
   int srcLeft = vp->actRect.left;
   int srcPhase;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      int rowcnt = actWdth;

      MTI_UINT8 *src = srcImage[i % srcFieldCount];

      if (src == NULL)
      {
         continue;
      }

      srcPhase = i * srcWdth + srcLeft;
      src += (srcPhase / 3) << 4;
      srcPhase %= 3;

      MTI_UINT16 *dst = dstPels + i * dstPitch;
      MTI_UINT16 *alphdst = alphPels + i * alphPitch;

      MTI_UINT32 r0, r1, r2, r3;

      r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
      r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
      r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
      r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];

      if (srcPhase == 0)
      {
         goto p0;
      }
      if (srcPhase == 1)
      {
         goto p1;
      }
      goto p2;

      while (true)
      {

p0: ;

         dst[2] = (r0 >> 22) & 0x3ff; // R
         dst[1] = (r0 >> 12) & 0x3ff; // G
         dst[0] = (r0 >> 2) & 0x3ff; // B

         alphdst[0] = (r1 >> 22) & 0x3ff; // A

         if (--rowcnt == 0)
         {
            break;
         }

p1: ;

         dst[5] = (r1 >> 12) & 0x3ff; // R
         dst[4] = (r1 >> 2) & 0x3ff; // G
         dst[3] = (r2 >> 22) & 0x3ff; // B

         alphdst[1] = (r2 >> 12) & 0x3ff; // A

         if (--rowcnt == 0)
         {
            break;
         }

p2: ;

         dst[8] = (r2 >> 2) & 0x3ff; // R
         dst[7] = (r3 >> 22) & 0x3ff; // G
         dst[6] = (r3 >> 12) & 0x3ff; // B

         alphdst[2] = (r3 >> 2) & 0x3ff; // A

         if (--rowcnt == 0)
         {
            break;
         }

         src += 16;
         dst += 9;
         alphdst += 3;

         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
         r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
         r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
      }
   }
}

static void extractY_10L(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;   // components
//   int dstOffs = (vp->actRect.left / 3) * 3;
   MTIassert(vp->actRect.left == 0);

   MTI_UINT8 **srcImage = vp->frame;
   int srcWdth = vp->frmWdth;       // bytes
   int srcLeft = vp->actRect.left;
   int srcPhase;

   int actHght = vp->actHght;
   int actWdth = vp->actWdth;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[0];

      if (src == NULL)
      {
         continue;
      }

      srcPhase = i * srcWdth + srcLeft;
      src += (srcPhase / 3) << 2;
      srcPhase %= 3;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // zero out the auxiliary data portion
//      for (int j = 0; j < dstOffs; j++)
//      {
//         dst[j] = 0;
//      }
//
//      dst += dstOffs;

       MTI_UINT32 r0;

      // now set up to extract the line's active image data
      int j = actWdth;   // pixels

#if ENDIAN == MTI_BIG_ENDIAN
      r0 = *((int *)src);
#else
#if MTI_ASM_X86_INTEL
      _asm
      {
         mov ebx, src mov eax, [ebx]bswap eax mov r0, eax
      }
#else
      r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
#endif
#endif

      if (srcPhase == 1)
      {
         goto e1;
      }
      if (srcPhase == 2)
      {
         goto e2;
      }

      goto e0;

      MTI_UINT16 v;
      while (true)
      {
e0:
         v = (r0) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

e1:
         v = (r0 >> 10) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

e2:
         v = (r0 >> 20) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

         src += 4;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((int *)src);
#else
#if MTI_ASM_X86_INTEL
         _asm
         {
            mov ebx, src mov eax, [ebx]bswap eax mov r0, eax
         }
#else
         r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
#endif
#endif

      }
   }
}

static void extractY_10R(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;   // components
//   int dstOffs = (vp->actRect.left / 3) * 3;
   MTIassert(vp->actRect.left == 0);

   MTI_UINT8 **srcImage = vp->frame;
   int srcWdth = vp->frmWdth;       // bytes
   int srcLeft = vp->actRect.left;
   int srcPhase;

   int actHght = vp->actHght;
   int actWdth = vp->actWdth;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[0];

      if (src == NULL)
      {
         continue;
      }

      srcPhase = i * srcWdth + srcLeft;
      src += (srcPhase / 3) << 2;
      srcPhase %= 3;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // zero out the auxiliary data portion
//      for (int j = 0; j < dstOffs; j++)
//      {
//         dst[j] = 0;
//      }
//
//      dst += dstOffs;

       MTI_UINT32 r0;

      // now set up to extract the line's active image data
      int j = actWdth;   // pixels

#if ENDIAN == MTI_BIG_ENDIAN
      r0 = *((int *)src);
#else
#if MTI_ASM_X86_INTEL
      _asm
      {
         mov ebx, src mov eax, [ebx]bswap eax mov r0, eax
      }
#else
      r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
#endif
#endif

      if (srcPhase == 1)
      {
         goto e1;
      }
      if (srcPhase == 2)
      {
         goto e2;
      }

      goto e0;

      MTI_UINT16 v;
      while (true)
      {
e0:
         v = (r0) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

e1:
         v = (r0 >> 10) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

e2:
         v = (r0 >> 20) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

         src += 4;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((int *)src);
#else
#if MTI_ASM_X86_INTEL
         _asm
         {
            mov ebx, src mov eax, [ebx]bswap eax mov r0, eax
         }
#else
         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
#endif
#endif

      }
   }
}

static void extractY_10P(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT16 *dstPels = vp->pels;
   int dstPitch = vp->frmWdth * 3;   // components
//   int dstOffs = (vp->actRect.left / 4) * 4;
   MTIassert(vp->actRect.left == 0);

   MTI_UINT8 **srcImage = vp->frame;
   int srcPitch = vp->frmPitch;     // bytes
   int srcLeft = vp->actRect.left;
   int srcPhase;

   int actHght = vp->actHght;
   int actWdth = vp->actWdth;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT8 *src = srcImage[0];

      if (src == NULL)
      {
         continue;
      }

      src += i * srcPitch + (srcLeft / 4) * 5;
      srcPhase = srcLeft % 4;

      MTI_UINT16 *dst = dstPels + i * dstPitch;

      // zero out the auxiliary data portion
//      for (int j = 0; j < vp->actRect.left; j++)
//      {
//         dst[j] = 0;
//      }
//      dst += dstOffs;

       MTI_UINT32 r0, r1;

      // now set up to extract the line's active image data
      int j = actWdth;

#if ENDIAN == MTI_BIG_ENDIAN
      r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + src[0];
      r1 = src[4];
#else
      r0 = *((int *)src);
      r1 = src[4];
#endif

      if (srcPhase == 0)
      {
         goto e0;
      }
      if (srcPhase == 1)
      {
         goto e1;
      }
      if (srcPhase == 2)
      {
         goto e2;
      }

      goto e3;

      MTI_UINT16 v;
      while (true)
      {
e0:
         v = (r0) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

e1:
         v = (r0 >> 10) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

e2:
         v = (r0 >> 20) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

e3:
         v = ((r0 >> 30) + (r1 << 2)) & 0x3ff;
         *(dst++) = v;
         *(dst++) = v;
         *(dst++) = v;
         if (--j == 0)
         {
            break;
         }

         src += 5;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + src[0];
         r1 = src[4];
#else
         r0 = *((int *)src);
         r1 = src[4];
#endif

      }
   }
}

//static void extractY_16_LE(void *v, int iJob)
//{
//   CExtractor *vp = (CExtractor*) v;
//
//   MTI_UINT16 *dstPels = vp->pels;
//   int dstPitch = vp->frmWdth * 3;
////   int dstOffs = vp->actRect.left;
//   MTIassert(vp->actRect.left == 0);
//
//   MTI_UINT8 **srcImage = vp->frame;
//   int srcPitch = vp->frmPitch;
//   int srcFieldCount = vp->frmFieldCount;
////   int srcOffs = vp->actRect.left;
//
//   int actWdth = vp->actWdth;
//   int actHght = vp->actHght;
//
//   int rowsPerStripe = actHght / vp->nStripes;
//
//   int begRow = vp->actRect.top + iJob * rowsPerStripe;
//   int stripeRows = rowsPerStripe;
//   bool copyAlpha = false;
//   bool zeroAlpha = false;
//   if (iJob == (vp->nStripes - 1))
//   {
//      stripeRows = actHght - (iJob * rowsPerStripe);
//      copyAlpha = vp->copyAlpha;
//      zeroAlpha = vp->zeroAlpha;
//   }
//
//   for (int i = begRow; i < (begRow + stripeRows); ++i)
//   {
//      MTI_UINT8 *src = srcImage[i % srcFieldCount];
//      if (src == NULL)
//      {
//         continue;
//      }
//
//      src += (i / srcFieldCount) * srcPitch;
//      MTI_UINT16 *dst = dstPels + (i * dstPitch);
//
//      // copy the horizontal interval data to the destination
////      if (srcOffs > 0)
////      {
////         MTImemcpy((void *)dst, (void *)src, srcOffs);
////      }
////
////      // now set up to extract the line's active image data
////      src += srcOffs;
////      dst += dstOffs;
//
//      // extract one line of image data as 16-bit 444
//      for (int j = 0; j < actWdth; ++j)
//      {
//         MTI_UINT16 v = (src[(2 * j) + 1] << 8) + (src[2 * j]);
//         *(dst++) = v;
//         *(dst++) = v;
//         *(dst++) = v;
//      }
//   }
//
//   if (zeroAlpha)
//   {
//      // -> Alpha map in intermediate
//      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
//      MTImemset(alphadst, 0, (size_t)getAlphaMatteSize(vp));
//   }
//   if (copyAlpha)
//   { // native to intermediate
//
//      // -> Alpha map in native
//      MTI_UINT8 *alphasrc = srcImage[0] + actHght * srcPitch;
//
//      // -> Alpha map in intermediate
//      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + actHght * dstPitch);
//
//      // copy the alpha matte to the intermediate
//      int alphasiz = getAlphaMatteSize(vp);
//      for (int i = 0; i < alphasiz; i++)
//      {
//         alphadst[i] = alphasrc[i];
//      }
//   }
//}

// SOURCE is Y - one component pixels
// DESTINATION is internal format YYY - 3-component pixels
static void extractY_16_LE(void *v, int stripeNumber)
{
   CExtractor *vp = (CExtractor*) v;

   // Old video-related crap, no longer supported!
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.right == vp->frmWdth - 1);
   MTIassert(vp->actRect.bottom == vp->frmHght - 1);
   MTIassert(vp->frmFieldCount == 1);

   MTI_UINT16 *src = (MTI_UINT16 *) vp->frame[0];
   MTI_UINT16 *dst = (MTI_UINT16 *) vp->pels;
   if (src == nullptr ||dst == nullptr)
   {
      return;
   }

   int frameWidthInPixels = vp->frmWdth;
   int frameHeight = vp->frmHght;
   int totalNumberOfStripes = vp->nStripes;
   int nominalRowsPerStripe = frameHeight / totalNumberOfStripes;
   int extraRows = frameHeight % totalNumberOfStripes;

   // The first "extraRows" number of stripes get one extra row each!
   int firstRowInThisStripe = (stripeNumber * nominalRowsPerStripe) + std::min<int>(extraRows, stripeNumber);
   int numberOfRowsInThisStripe = nominalRowsPerStripe + ((stripeNumber < extraRows) ? 1 : 0);

   auto stripeSrc = src + (firstRowInThisStripe * frameWidthInPixels);
   auto stripeDst = dst + (firstRowInThisStripe * frameWidthInPixels * 3);
   auto numberOfPixelsToReplace = numberOfRowsInThisStripe * frameWidthInPixels;

   for (auto pixel = 0; pixel < numberOfPixelsToReplace; ++pixel)
   {
      stripeDst[(pixel * 3) + 0] = stripeSrc[pixel];
      stripeDst[(pixel * 3) + 1] = stripeSrc[pixel];
      stripeDst[(pixel * 3) + 2] = stripeSrc[pixel];
   }

   if (vp->zeroAlpha || vp->copyAlpha)
   {
      // NOTE!! THE SET OF PIXELS HERE MAY BE DIFFERENT FROM THE Y->YYY SET.
      // But that's OK - we're just trying to spread the load!
      int totalNumberOfAlphaBytes = getAlphaMatteSize(vp);
      int nominalNumberOfAlphaBytesPerStripe = totalNumberOfAlphaBytes / totalNumberOfStripes;
      int extraAlphaBytes = totalNumberOfAlphaBytes % totalNumberOfStripes;

      int firstAlphaByteInThisStripe = (stripeNumber * nominalRowsPerStripe * frameWidthInPixels)
                                          + std::min<int>(extraAlphaBytes, stripeNumber);
      int numberOfAlphaBytesInThisStripe = nominalNumberOfAlphaBytesPerStripe + ((stripeNumber < extraAlphaBytes) ? 1 : 0);

      // UGLY - alpha starts right after the last destination data pixel.
      MTI_UINT8 *alphaDst = (MTI_UINT8*) &dst[frameWidthInPixels * frameHeight * 3];
      MTI_UINT8 *stripeAlphaDst = &alphaDst[firstAlphaByteInThisStripe];
      if (vp->zeroAlpha)
      {
         MTImemset(stripeAlphaDst, 0, numberOfAlphaBytesInThisStripe);
      }
      else if (vp->copyAlpha)
      {
         // UGLY - alpha starts right after the last source data pixel.
         MTI_UINT8 *alphaSrc = (MTI_UINT8*) &src[frameWidthInPixels * frameHeight];
         MTI_UINT8 *stripeAlphaSrc = &alphaSrc[firstAlphaByteInThisStripe];
         MTImemcpy(stripeAlphaDst, stripeAlphaSrc, numberOfAlphaBytesInThisStripe);
      }
   }
}

//static void extractMON_10(void *v, int iJob)
//{
//   CExtractor *vp = (CExtractor*) v;
//
//   MTI_UINT16 *dstPels = vp->pels;
//   int dstPitch = vp->frmWdth;
//   int dstOffs = (vp->actRect.left / 3) * 3;
//
//   MTI_UINT8 **srcImage = vp->frame;
//   int srcWdth = vp->frmWdth;
//   int srcLeft = vp->actRect.left;
//   int srcPhase;
//
//   int actHght = vp->actHght;
//   int actWdth = vp->actWdth;
//
//   int rowsPerStripe = actHght / vp->nStripes;
//
//   int begRow = vp->actRect.top + iJob * rowsPerStripe;
//   int stripeRows = rowsPerStripe;
//   if (iJob == (vp->nStripes - 1))
//   {
//      stripeRows = vp->actHght - iJob * rowsPerStripe;
//   }
//
//   for (int i = begRow; i < (begRow + stripeRows); i++)
//   {
//
//      MTI_UINT8 *src = srcImage[0];
//
//      if (src == NULL)
//      {
//         continue;
//      }
//
//      srcPhase = i * srcWdth + srcLeft;
//      src += (srcPhase / 3) << 2;
//      srcPhase %= 3;
//
//      // zero out the auxiliary data portion
//      MTI_UINT16 *dst = dstPels + i * dstPitch;
//      for (int j = 0; j < dstOffs; j++)
//      {
//         dst[j] = 0;
//      }
//
//      dst += dstOffs;
//
//       MTI_UINT32 r0;
//
//      // now set up to extract the line's active image data
//      int j = actWdth;
//
//#if ENDIAN == MTI_BIG_ENDIAN
//      r0 = *((int *)src);
//#else
//#if MTI_ASM_X86_INTEL
//      _asm
//      {
//         mov ebx, src mov eax, [ebx]bswap eax mov r0, eax
//      }
//#else
//      r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
//#endif
//#endif
//
//      if (srcPhase == 1)
//      {
//         goto e1;
//      }
//      if (srcPhase == 2)
//      {
//         goto e2;
//      }
//
//      goto e0;
//
//      while (true)
//      {
//e0:
//         *(dst++) = (r0) & 0x3ff;
//
//         if (--j == 0)
//         {
//            break;
//         }
//
//e1:
//         *(dst++) = (r0 >> 10) & 0x3ff;
//         if (--j == 0)
//         {
//            break;
//         }
//
//e2:
//         *(dst++) = (r0 >> 20) & 0x3ff;
//         if (--j == 0)
//         {
//            break;
//         }
//
//         src += 4;
//
//#if ENDIAN == MTI_BIG_ENDIAN
//         r0 = *((int *)src);
//#else
//#if MTI_ASM_X86_INTEL
//         _asm
//         {
//            mov ebx, src mov eax, [ebx]bswap eax mov r0, eax
//         }
//#else
//         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
//#endif
//#endif
//
//      }
//   }
//}
//
//static void extractMON_10P(void *v, int iJob)
//{
//   CExtractor *vp = (CExtractor*) v;
//
//   MTI_UINT16 *dstPels = vp->pels;
//   int dstPitch = vp->frmWdth;
//   int dstOffs = (vp->actRect.left / 4) * 4;
//
//   MTI_UINT8 **srcImage = vp->frame;
//   int srcPitch = vp->frmPitch;
//   int srcLeft = vp->actRect.left;
//   int srcPhase;
//
//   int actHght = vp->actHght;
//   int actWdth = vp->actWdth;
//
//   int rowsPerStripe = actHght / vp->nStripes;
//
//   int begRow = vp->actRect.top + iJob * rowsPerStripe;
//   int stripeRows = rowsPerStripe;
//   if (iJob == (vp->nStripes - 1))
//   {
//      stripeRows = actHght - iJob * rowsPerStripe;
//   }
//
//   for (int i = begRow; i < (begRow + stripeRows); i++)
//   {
//
//      MTI_UINT8 *src = srcImage[0];
//
//      if (src == NULL)
//      {
//         continue;
//      }
//
//      src += i * srcPitch + (srcLeft / 4) * 5;
//      srcPhase = srcLeft % 4;
//
//      // zero out the auxiliary data portion
//      MTI_UINT16 *dst = dstPels + i * dstPitch;
//      for (int j = 0; j < vp->actRect.left; j++)
//      {
//         dst[j] = 0;
//      }
//      dst += dstOffs;
//
//       MTI_UINT32 r0, r1;
//
//      // now set up to extract the line's active image data
//      int j = actWdth;
//
//#if ENDIAN == MTI_BIG_ENDIAN
//      r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + src[0];
//      r1 = src[4];
//#else
//      r0 = *((int *)src);
//      r1 = src[4];
//#endif
//
//      if (srcPhase == 0)
//      {
//         goto e0;
//      }
//      if (srcPhase == 1)
//      {
//         goto e1;
//      }
//      if (srcPhase == 2)
//      {
//         goto e2;
//      }
//
//      goto e3;
//
//      while (true)
//      {
//e0:
//         *(dst++) = (r0) & 0x3ff;
//         if (--j == 0)
//         {
//            break;
//         }
//
//e1:
//         *(dst++) = (r0 >> 10) & 0x3ff;
//         if (--j == 0)
//         {
//            break;
//         }
//
//e2:
//         *(dst++) = (r0 >> 20) & 0x3ff;
//         if (--j == 0)
//         {
//            break;
//         }
//
//e3:
//         *(dst++) = ((r0 >> 30) + (r1 << 2)) & 0x3ff;
//         if (--j == 0)
//         {
//            break;
//         }
//
//         src += 5;
//
//#if ENDIAN == MTI_BIG_ENDIAN
//         r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + src[0];
//         r1 = src[4];
//#else
//         r0 = *((int *)src);
//         r1 = src[4];
//#endif
//
//      }
//   }
//}
//
//static void extractMON_16_LE(void *v, int iJob)
//{
//   CExtractor *vp = (CExtractor*) v;
//
//   MTI_UINT16 *dstPels = vp->pels;
//   int dstPitch = vp->frmWdth;
//   int dstOffs = vp->actRect.left;
//
//   MTI_UINT8 **srcImage = vp->frame;
//   int srcPitch = vp->frmPitch;
//   int srcFieldCount = vp->frmFieldCount;
//   int srcOffs = vp->actRect.left;
//
//   int actWdth = vp->actWdth;
//   int actHght = vp->actHght;
//
//   int rowsPerStripe = actHght / vp->nStripes;
//
//   int begRow = vp->actRect.top + iJob * rowsPerStripe;
//   int stripeRows = rowsPerStripe;
//   bool copyAlpha = false;
//   bool zeroAlpha = false;
//   if (iJob == (vp->nStripes - 1))
//   {
//      stripeRows = actHght - (iJob * rowsPerStripe);
//      copyAlpha = vp->copyAlpha;
//      zeroAlpha = vp->zeroAlpha;
//   }
//
//   for (int i = begRow; i < (begRow + stripeRows); ++i)
//   {
//      MTI_UINT8 *src = srcImage[i % srcFieldCount];
//      if (src == NULL)
//      {
//         continue;
//      }
//
//      src += (i / srcFieldCount) * srcPitch;
//      MTI_UINT16 *dst = dstPels + (i * dstPitch);
//
//      // copy the horizontal interval data to the destination
//      if (srcOffs > 0)
//      {
//         MTImemcpy((void *)dst, (void *)src, srcOffs);
//      }
//
//      // now set up to extract the line's active image data
//      src += srcOffs;
//      dst += dstOffs;
//
//      // extract one line of image data as 16-bit 444
//      for (int j = 0; j < actWdth; ++j)
//      {
//         dst[j] = (src[(2 * j) + 1] << 8) + (src[2 * j]);
//      }
//   }
//
//   if (zeroAlpha)
//   {
//      // -> Alpha map in intermediate
//      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + (actHght * dstPitch));
//      MTImemset(alphadst, 0, (size_t)getAlphaMatteSize(vp));
//   }
//   if (copyAlpha)
//   { // native to intermediate
//
//      // -> Alpha map in native
//      MTI_UINT8 *alphasrc = srcImage[0] + actHght * srcPitch;
//
//      // -> Alpha map in intermediate
//      MTI_UINT8 *alphadst = (MTI_UINT8*)(dstPels + actHght * dstPitch);
//
//      // copy the alpha matte to the intermediate
//      int alphasiz = getAlphaMatteSize(vp);
//      for (int i = 0; i < alphasiz; i++)
//      {
//         alphadst[i] = alphasrc[i];
//      }
//   }
//}

// extract the active picture (specified in image format) from a frame
// using as much multithreading as possible. The left coordinate of
// the active picture must always begin on an even value
void CExtractor::extractActivePicture(MTI_UINT16 *dstpels, // array of 16-bit "444 data" (YUV or RGB)
      MTI_UINT8 **srcimg, // -> source field(s) (YUV422 or RGB variant)
      const CImageFormat *srcClipImageFormat, // image format of source clip)
      const CImageFormat *srcFrameImageFormat // image format of source frame)
      )
{
   pels = dstpels;
   frame = srcimg;
   frmFieldCount = srcClipImageFormat->getFieldCount();
   frmWdth = srcClipImageFormat->getPixelsPerLine();
   frmHght = srcClipImageFormat->getLinesPerFrame();
   frmPitch = srcClipImageFormat->getFramePitch();
   actRect = srcClipImageFormat->getActivePictureRect();
   actWdth = actRect.right - actRect.left + 1;
   actHght = actRect.bottom - actRect.top + 1;
   clipAlphaType = srcClipImageFormat->getAlphaMatteType();
   frameAlphaType = srcFrameImageFormat->getAlphaMatteType();
   copyAlpha = false;
   zeroAlpha = false;

   int frmPelComponents = srcClipImageFormat->getPixelComponents();
   int frmPelPacking = srcClipImageFormat->getPixelPacking();

   // set up the mthread structure
   MTHREAD_STRUCT tsThread;
   tsThread.PrimaryFunction   = (void (*)(void *,int))&null;
   tsThread.SecondaryFunction = NULL;
   tsThread.CallBackFunction  = NULL;
   tsThread.vpApplicationData = this;
   tsThread.iNThread          = nStripes;
   tsThread.iNJob             = nStripes;

   switch (frmPelComponents)
   {

   case IF_PIXEL_COMPONENTS_YUV422:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractYUV_8;
         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractYUV_10;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractDVS_10;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractDVS_10a;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractDVS_10BE;
         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_YUV444:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractUYV_8;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractUYV_10;
         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_RGB:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGB_8;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (clipAlphaType == IF_ALPHA_MATTE_2_BIT_EMBEDDED)
                                  ? (void(*)(void *, int)) & extractRGB_10_A2
                                  : (void(*)(void *, int)) & extractRGB_10;
         copyAlpha = ((clipAlphaType == IF_ALPHA_MATTE_1_BIT)
                     || (clipAlphaType == IF_ALPHA_MATTE_2_BIT_EMBEDDED)
                     || (clipAlphaType == IF_ALPHA_MATTE_8_BIT)
                     || (clipAlphaType == IF_ALPHA_MATTE_16_BIT));
         if (copyAlpha && clipAlphaType != frameAlphaType)
         {
            copyAlpha = false;
            zeroAlpha = true;
         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
#ifndef DPX12_PACKED
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGB_12;
#else
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGB_12P;
#endif
         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
#ifndef DPX12_PACKED
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGB_12_LR;
#else
         // tsThread.PrimaryFunction   = (void (*)(void *,int))&extractRGB_12P_B;
#endif
         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGB_12_L_BE;
         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGB_12_L_LE;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGB_16_LE;
         copyAlpha = (clipAlphaType == IF_ALPHA_MATTE_1_BIT);
         if (copyAlpha && clipAlphaType != frameAlphaType)
         {
            copyAlpha = false;
            zeroAlpha = true;
         }

         break;

      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGB_Half;
         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_BGR:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractBGR_8;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractBGR_10;
         copyAlpha = ((clipAlphaType == IF_ALPHA_MATTE_1_BIT) || (clipAlphaType == IF_ALPHA_MATTE_8_BIT) ||
               (clipAlphaType == IF_ALPHA_MATTE_16_BIT));
         if (copyAlpha && clipAlphaType != frameAlphaType)
         {
            copyAlpha = false;
            zeroAlpha = true;
         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractBGR_16_LE;
         copyAlpha = (clipAlphaType == IF_ALPHA_MATTE_1_BIT);
         if (copyAlpha && clipAlphaType != frameAlphaType)
         {
            copyAlpha = false;
            zeroAlpha = true;
         }

         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_RGBA:

      copyAlpha = true;

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGBA_10;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGBA_16_LE;
         break;

      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractRGBA_Half;
         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_BGRA:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractBGRA_10;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractBGRA_16_LE;
         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_Y:
   case IF_PIXEL_COMPONENTS_YYY:    // xyzzy1

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractY_10L;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractY_10R;
         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractY_10P;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
         tsThread.PrimaryFunction = (void(*)(void *, int)) & extractY_16_LE;
         break;
      }

      break;

   default:
      break;

   }

//   if (frmPelComponents != IF_PIXEL_COMPONENTS_LUMINANCE)
//   {
//
      // copy vertical interval data to 16-bit progressive field
      for (int i = 0; i < actRect.top; i++)
      {
         MTI_UINT8 *src = srcimg[i % frmFieldCount];
         if (src == NULL)
         {
            continue;
         }
         src += frmPitch * (i / frmFieldCount);

         MTI_UINT16 *dst = pels + frmWdth * 3 * i;
         MTImemcpy((MTI_UINT8 *)dst, src, frmPitch);
      }
//   }

   // now extract the active picture stripe by stripe
   CheckFencepostOverrun(dstpels);

#ifdef NO_MULTI
   for (int  i = 0; i < nStripes; i++)
   {
      tsThread.PrimaryFunction(this, i);
   }
#else
   // now allocate, run, and free the mthreads
   if (nStripes > 1)
   {
      int iRet = MThreadAlloc(&tsThread);
      if (iRet)
      {
            TRACE_0(errout << "Convert: MThreadAlloc failed, error=" << iRet);
      }
      else
      {
         iRet = MThreadStart(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadStart failed, error=" << iRet);
         }

         iRet = MThreadFree(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadFree failed, error=" << iRet);
         }
      }
   }
   else // nStripes <= 0
   {
      // If we're only doing one stripe, call the function directly.
      tsThread.PrimaryFunction(this, 0);
   }
#endif

   CheckFencepostOverrun(dstpels);
}

////////////////////////////////////////////////////////////////////////////
//
// F O R M A T - S P E C I F I C   R E P L A C E   F U N C T I O N S

static void replaceYUV_8(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left / 2) * 4;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // replace one line of image data from 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         dst[0] = (MTI_UINT8)src[1];
         dst[1] = (MTI_UINT8)src[0];
         dst[2] = (MTI_UINT8)src[5];
         dst[3] = (MTI_UINT8)src[3];
         dst += 4;

         src += 6;

      }
   }
}

static void replaceYUV_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left / 2) * 5;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      unsigned int r0, r1;

      // replace one line of image data from 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         r0 = (src[1] << 22) + (src[0] << 12) + (src[5] << 2);
         r1 = (r0 << 8) + (src[3]);

         dst[0] = (r0 >> 24);
         dst[1] = (r0 >> 16);
         dst[2] = (r0 >> 8);
         dst[3] = (r1 >> 8);
         dst[4] = (r1);
         dst += 5;

         src += 6;

      }
   }
}

static void replaceDVS_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left / 6) * 16;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left / 6) * 18;
   int phase = vp->actRect.left % 6;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // replace one line of image data from 16-bit 444
      int prsCnt = actWdth / 2;

      if (phase == 0)
      {
         goto p0;
      }
      if (phase == 2)
      {
         goto p2;
      }
      goto p4;

      while (true)
      {

p0: ;

         dst[0] = (MTI_UINT8)src[1]; // U
         dst[1] = (MTI_UINT8)((src[1] >> 8) + (src[0] << 2)); // UY
         dst[2] = (MTI_UINT8)((src[0] >> 6) + (src[5] << 4)); // YV
         dst[3] = (MTI_UINT8)(src[5] >> 4); // V
         dst[4] = (MTI_UINT8)src[3]; // Y
         dst[5] = (MTI_UINT8)((src[3] >> 8) + (dst[5] & 0xfc)); // YU

         if (--prsCnt == 0)
         {
            break;
         }

p2: ;

         dst[5] = (MTI_UINT8)((dst[5] & 0x03) + (src[7] << 2)); // YU
         dst[6] = (MTI_UINT8)((src[7] >> 6) + (src[6] << 4)); // UY
         dst[7] = (MTI_UINT8)(src[6] >> 4); // Y
         dst[8] = (MTI_UINT8)src[11]; // V
         dst[9] = (MTI_UINT8)((src[11] >> 8) + (src[9] << 2)); // VY
         dst[10] = (MTI_UINT8)((src[9] >> 6) + (dst[10] & 0xf0)); // YU

         if (--prsCnt == 0)
         {
            break;
         }

p4: ;

         dst[10] = (MTI_UINT8)((dst[10] & 0x0f) + (src[13] << 4)); // YU
         dst[11] = (MTI_UINT8)(src[13] >> 4); // U
         dst[12] = (MTI_UINT8)src[12]; // Y
         dst[13] = (MTI_UINT8)((src[12] >> 8) + (src[17] << 2)); // YV
         dst[14] = (MTI_UINT8)((src[17] >> 6) + (src[15] << 4)); // VY
         dst[15] = (MTI_UINT8)(src[15] >> 4); // Y

         if (--prsCnt == 0)
         {
            break;
         }

         src += 18;
         dst += 16;
      }
   }
}

static void replaceDVS_10a(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left / 6) * 16;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left / 6) * 18;
   int phase = vp->actRect.left % 6;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // replace one line of image data from 16-bit 444
      int prsCnt = actWdth / 2;

      if (phase == 0)
      {
         goto p0;
      }
      if (phase == 2)
      {
         goto p2;
      }
      goto p4;

      while (true)
      {

p0: ;

         dst[0] = (src[1] >> 2); // U
         dst[1] = (src[0] >> 2); // Y
         dst[2] = (src[5] >> 2); // V
         dst[3] = ((src[1] & 0x03) << 4) + ((src[0] & 0x03) << 2) + (src[5] & 0x03); // UYV
         dst[4] = (src[3] >> 2); // Y
         dst[7] = ((src[3] & 0x03) << 4) + (dst[7] & 0x0f); // YUY

         if (--prsCnt == 0)
         {
            break;
         }

p2: ;

         dst[5] = (src[7] >> 2); // U
         dst[6] = (src[6] >> 2); // Y
         dst[7] = (dst[7] & 0x30) + ((src[7] & 0x03) << 2) + (src[6] & 0x03); // VYU
         dst[8] = (src[11] >> 2); // V
         dst[9] = (src[9] >> 2); // Y
         dst[11] = ((src[11] & 0x03) << 4) + ((src[9] & 0x03) << 2) + (dst[11] & 0x03); // YVY

         if (--prsCnt == 0)
         {
            break;
         }

p4: ;

         dst[10] = (src[13] >> 2); // U
         dst[11] = (dst[11] & 0x3c) + (src[13] & 0x03); // VYU
         dst[12] = (src[12] >> 2); // Y
         dst[13] = (src[17] >> 2); // V
         dst[14] = (src[15] >> 2); // Y
         dst[15] = ((src[12] & 0x03) << 4) + ((src[17] & 0x03) << 2) + (src[15] & 0x03); // YVY

         if (--prsCnt == 0)
         {
            break;
         }

         src += 18;
         dst += 16;
      }
   }
}

static void replaceDVS_10BE(void *v, int iJob) // CLIPSTER
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left / 6) * 16;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left / 6) * 18;
   int phase = vp->actRect.left % 6;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // replace one line of image data from 16-bit 444
      int prsCnt = actWdth / 2;

      if (phase == 0)
      {
         goto p0;
      }
      if (phase == 2)
      {
         goto p2;
      }
      goto p4;

      while (true)
      {

p0: ;

         dst[0] = (src[1] >> 2); // U
         dst[1] = (src[1] << 6) + (src[0] >> 4); // UY
         dst[2] = (src[0] << 4) + (src[5] >> 6); // YV
         dst[3] = (src[5] << 2); // V
         dst[4] = (src[3] >> 2); // Y
         dst[5] = (src[3] << 6) + (dst[5] & 0x3f); // YU

         if (--prsCnt == 0)
         {
            break;
         }

p2: ;

         dst[5] = (dst[5] & 0xc0) + (src[7] >> 4); // YU
         dst[6] = (src[7] << 4) + (src[6] >> 6); // UY
         dst[7] = (src[6] << 2); // Y
         dst[8] = (src[11] >> 2); // V
         dst[9] = (src[11] << 6) + (src[9] >> 4); // VY
         dst[10] = (src[9] << 4) + (dst[10] & 0x0f); // YU

         if (--prsCnt == 0)
         {
            break;
         }

p4: ;

         dst[10] = (dst[10] & 0xf0) + (src[13] >> 6); // YU
         dst[11] = (src[13] << 2); // U
         dst[12] = (src[12] >> 2); // Y
         dst[13] = (src[12] << 6) + (src[17] >> 4); // YV
         dst[14] = (src[17] << 4) + (src[15] >> 6); // VY
         dst[15] = (src[15] << 2); // Y

         if (--prsCnt == 0)
         {
            break;
         }

         src += 18;
         dst += 16;
      }
   }
}

static void replaceUYV_8(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // replace one line of image data from 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         dst[0] = (MTI_UINT8)src[1]; // U
         dst[1] = (MTI_UINT8)src[0]; // Y
         dst[2] = (MTI_UINT8)src[2]; // V

         dst[3] = (MTI_UINT8)src[4]; // U
         dst[4] = (MTI_UINT8)src[3]; // Y
         dst[5] = (MTI_UINT8)src[5]; // V
         dst += 6;

         src += 6;
      }
   }
}

static void replaceUYV_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left) * 4;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

       MTI_UINT32 r0, r1;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // replace one line of image data from 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         r0 = (src[0] << 10) + (src[1]) + (src[2] << 20);
         r1 = (src[3] << 10) + (src[4]) + (src[5] << 20);

         dst[0] = (r0);
         dst[1] = (r0 >> 8);
         dst[2] = (r0 >> 16);
         dst[3] = (r0 >> 24);

         dst[4] = (r1);
         dst[5] = (r1 >> 8);
         dst[6] = (r1 >> 16);
         dst[7] = (r1 >> 24);
         dst += 8;

         src += 6;
      }
   }
}

static void replaceRGB_8(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // replace one line of image data from 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         dst[0] = (MTI_UINT8)src[0];
         dst[1] = (MTI_UINT8)src[1];
         dst[2] = (MTI_UINT8)src[2];

         dst[3] = (MTI_UINT8)src[3];
         dst[4] = (MTI_UINT8)src[4];
         dst[5] = (MTI_UINT8)src[5];
         dst += 6;

         src += 6;
      }
   }
}

#ifdef _WINDOWS
#ifdef USE_MMX

// A helper function for replaceRGB_10() - provides faster replacement
// for a row of 444 pixels by using the MMX instruction set
// RETURNS FALSE if it can't do MMX because of alignment problems
static bool replace_row_444_10_MMX(MTI_UINT16 *src, MTI_UINT8 *dst, int actWdth)
{
   // check alignments
   if (((reinterpret_cast<long>(src) % 8) != 0) || ((reinterpret_cast<long>(dst) % 8) != 0) || ((actWdth % 8) != 0))
   {
      return false;
   }

   MTI_UINT8 *dstStop = dst + (actWdth * 4); // 4 bytes/pixel

   // The following constant is used to emulate via multiplication the
   // independent logical shifting of the individual words of the
   __int64 qShifter_MM7; // 0x0000000410000040LL;
   *(unsigned long*)&qShifter_MM7 = 0x10000040UL;
   *(((unsigned long*)&qShifter_MM7) + 1) = 0x00000004UL;

   _asm
   {
      // Load pointers and constants
      mov esi, src mov edi, dst mov ebx, dstStop movq mm7, qword ptr[qShifter_MM7]

            // while (dst < dstStop) {
            @ rprowloop :

            // Take 12 consecutive 16-bit components and turn them into 4 pixels
            // packed 3_10_bits_in_4_bytes with the two low order bits 0 and
            // big endian byte ordering

            // The registers readouts below oare shown with HIGH ORDER BIT on left
            // mmN: BYTE_8 BYTE_7 BYTE_6 BYTE_5 BYTE_4 BYTE_3 BYTE_2 BYTE_1 BYTE_0

            movq mm0, qword ptr[esi]
            // mm0: 00000044 44444444 00000033 33333333 00000022 22222222 00000011 11111111
            movq mm4, mm0
            // mm4: 00000044 44444444 00000033 33333333 00000022 22222222 00000011 11111111
            movq mm1, mm0
            // mm1: 00000044 44444444 00000033 33333333 00000022 22222222 00000011 11111111
            pmullw mm0, mm7 // shift components by multiplying: 0 4 4096 64
            // mm0: 00000000 00000000 00003333 33333300 22220000 00000000 11111111 11000000
            pmulhuw mm1, mm7 // This gives us the high order words from the shift
            // mm1: 00000000 00000000 00000000 00000000 00000000 00222222 00000000 00000000
            pshufw mm2, mm0, 0xF1
            // mm2: 00000000 00000000 00000000 00000000 11111111 11000000 22220000 00000000
            pshufw mm3, mm0, 0xFE
            // mm3: 00000000 00000000 00000000 00000000 00000000 00000000 00003333 33333300
            por mm3, mm1 por mm3, mm2
            // mm3: 00000000 00000000 00000000 00000000 11111111 11222222 22223333 33333300

            movq mm0, qword ptr[esi + 8]
            // mm0: 00000088 88888888 00000077 77777777 00000066 66666666 00000055 55555555
            movq mm5, mm0
            // mm5: 00000088 88888888 00000077 77777777 00000066 66666666 00000055 55555555
            psllq mm0, 16
            // mm0: 00000077 77777777 00000066 66666666 00000055 55555555 00000000 00000000
            psrlq mm4, 48
            // mm4: 00000000 00000000 00000000 00000000 00000000 00000000 00000044 44444444
            por mm0, mm4
            // mm0: 00000077 77777777 00000066 66666666 00000055 55555555 00000044 44444444
            movq mm1, mm0
            // mm1: 00000077 77777777 00000066 66666666 00000055 55555555 00000044 44444444
            pmullw mm0, mm7
            // mm0: 00000000 00000000 00006666 66666600 55550000 00000000 44444444 44000000
            pmulhuw mm1, mm7
            // mm1: 00000000 00000000 00000000 00000000 00000000 00555555 00000000 00000000
            pshufw mm2, mm0, 0xF1
            // mm2: 00000000 00000000 00000000 00000000 44444444 44000000 55550000 00000000
            pshufw mm4, mm0, 0xFE
            // mm4: 00000000 00000000 00000000 00000000 00000000 00000000 00006666 66666600
            por mm4, mm1 por mm4, mm2
            // mm4: 00000000 00000000 00000000 00000000 44444444 44555555 55556666 66666600
            punpckldq mm3, mm4
            // mm3: 44444444 44555555 55556666 66666600 11111111 11222222 22223333 33333300

            // Byte swap the two doublewords (not the whole quad!)
            movq mm4, mm3 // mm3 & mm4: 12345678
            psrlw mm3, 8 // mm3: -1-3-5-7
            psllw mm4, 8 // mm4: 2-4-6-8-
            por mm3, mm4 // mm3: 21436587
            pshufw mm3, mm3, 0xB1 // mm3: 43218765

            // Save first two pixels
            movq qword ptr[edi], mm3

            movq mm0, qword ptr[esi + 16]
            // mm0: 000000CC CCCCCCCC 000000BB BBBBBBBB 000000AA AAAAAAAA 00000099 99999999
            movq mm4, mm0
            // mm4: 000000CC CCCCCCCC 000000BB BBBBBBBB 000000AA AAAAAAAA 00000099 99999999
            psrlq mm5, 32
            // mm5: 00000000 00000000 00000000 00000000 00000088 88888888 00000077 77777777
            psllq mm0, 32
            // mm0: 000000AA AAAAAAAA 00000099 99999999 00000000 00000000 00000000 00000000
            por mm0, mm5
            // mm0: 000000AA AAAAAAAA 00000099 99999999 00000088 88888888 00000077 77777777
            movq mm1, mm0
            // mm1: 000000AA AAAAAAAA 00000099 99999999 00000088 88888888 00000077 77777777
            pmullw mm0, mm7
            // mm0: 00000000 00000000 00009999 99999900 88880000 00000000 77777777 77000000
            pmulhuw mm1, mm7
            // mm1: 00000000 00000000 00000000 00000000 00000000 00888888 00000000 00000000
            pshufw mm2, mm0, 0xF1
            // mm2: 00000000 00000000 00000000 00000000 77777777 77000000 88880000 00000000
            pshufw mm3, mm0, 0xFE
            // mm3: 00000000 00000000 00000000 00000000 00000000 00000000 00009999 99999900
            por mm3, mm1 por mm3, mm2
            // mm3: 00000000 00000000 00000000 00000000 77777777 77888888 88889999 99999900

            psrlq mm4, 16
            // mm4: 00000000 00000000 000000CC CCCCCCCC 000000BB BBBBBBBB 000000AA AAAAAAAA
            movq mm1, mm4
            // mm1: 00000000 00000000 000000CC CCCCCCCC 000000BB BBBBBBBB 000000AA AAAAAAAA
            pmullw mm4, mm7
            // mm4: 00000000 00000000 0000CCCC CCCCCC00 BBBB0000 00000000 AAAAAAAA AA000000
            pmulhuw mm1, mm7
            // mm1: 00000000 00000000 00000000 00000000 00000000 00BBBBBB 00000000 00000000
            pshufw mm2, mm4, 0xF1
            // mm2: 00000000 00000000 00000000 00000000 AAAAAAAA AA000000 BBBB0000 00000000
            pshufw mm0, mm4, 0xFE
            // mm0: 00000000 00000000 00000000 00000000 00000000 00000000 0000CCCC CCCCCC00
            por mm0, mm1 por mm0, mm2
            // mm0: 00000000 00000000 00000000 00000000 AAAAAAAA AABBBBBB BBBBCCCC CCCCCC00

            punpckldq mm3, mm0
            // mm3: AAAAAAAA AABBBBBB BBBBCCCC CCCCCC00 77777777 77888888 88889999 99999900

            // Byte swap the two doublewords (not the whole quad!)
            movq mm4, mm3 // mm3 & mm4: 12345678
            psrlw mm3, 8 // mm3: -1-3-5-7
            psllw mm4, 8 // mm4: 2-4-6-8-
            por mm3, mm4 // mm3: 21436587
            pshufw mm3, mm3, 0xB1 // mm3: 43218765

            // Save second two pixels
            movq qword ptr[edi + 8], mm3

            add esi, 24 // src
            add edi, 16 // dst
            cmp edi, ebx // (dst < dstStop)?
            jb @ rprowloop
            // }  // while

            emms // Give the FPU back its registers
         } return true;
}

#endif // MMX
#endif // WINDOWS

extern void replace_row_444_10_ASM(MTI_UINT16 *src, MTI_UINT8 *dst, int actWdth);

static void replaceRGB_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;             // in BYTES
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left) * 4;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;         // in COMPONENTS
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   bool copyAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
      copyAlpha = vp->copyAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

       MTI_UINT32 r0, r1;

      // copy the horizontal interval data to the destination
      if (dstOffs > 0)
      {
         MTImemcpy((void *)dst, (void *)src, dstOffs);

         // now set up to replace the line's active image data
         src += srcOffs;
         dst += dstOffs;
      }

      // replace one line of image data from 16-bit 444
      replace_row_444_10_ASM(src, dst, actWdth);
   }

   if (copyAlpha)
   {
      // intermediate to native
      // -> Alpha map in intermediate
      MTI_UINT8 *alphasrc = (MTI_UINT8*)(srcPels + actHght * srcPitch);

      // -> Alpha map in native
      MTI_UINT8 *alphadst = dstImage[0] + actHght * dstPitch;

      // copy the alpha matte to the native frame
      int alphasiz = getAlphaMatteSize(vp);
      for (int i = 0; i < alphasiz; i++)
      {
         alphadst[i] = alphasrc[i];
      }
   }
}

static void replaceRGB_10_A2(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;
   if (!vp->copyAlpha)
   {
      replaceRGB_10(v, iJob);
      return;
   }

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;                 // in BYTES
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left) * 4;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;             // in COMPONENTS
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;                  // in PIXELS
   int actHght = vp->actHght;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
   }

   MTI_UINT8 *alphaStart = (MTI_UINT8*) (srcPels + (actHght * srcPitch));
   const int alphasPerByte = 8 / 2;
   MTI_UINT8 *alphaSrc = alphaStart + (begRow * (actWdth / alphasPerByte));
   MTI_UINT8 alphaMask = 3;
   int alphaShift = 0;

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {
      MTI_UINT16 *src = srcPels + i * srcPitch;
      MTI_UINT8 *dst = dstImage[i % dstFieldCount];
      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

       MTI_UINT32 r0, r1;

      // copy the horizontal interval data to the destination
      if (dstOffs > 0)
      {
         MTImemcpy((void *)dst, (void *)src, dstOffs);

         // now set up to replace the line's active image data
         src += srcOffs;
         dst += dstOffs;
      }

      // replace one line of image data from 16-bit 444
      for (int j = 0; j < actWdth; j++)
      {
         MTI_UINT32 r = ((src[0] & 0x3FF) << 22)
                      + ((src[1] & 0x3FF) << 12)
                      + ((src[2] & 0x3FF) << 2)
                      + ((*alphaSrc & alphaMask) >> alphaShift);

         dst[0] = r >> 24;
         dst[1] = r >> 16;
         dst[2] = r >> 8;
         dst[3] = r;

         src += 3;
         dst += 4;

         alphaShift += 2;
         alphaMask <<= 2;
         if (alphaShift == 8)
         {
            ++alphaSrc;
            alphaShift = 0;
            alphaMask = 3;
         }
      }
   }
}

#ifndef DPX12_PACKED

static void replaceRGB_12(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstWdth = vp->frmWdth;
   int dstPitch = ((dstWdth * 36 + 31) / 32) * 4;
   int dstLeft = vp->actRect.left;
   int dstOffs = (vp->actRect.left >> 3) * 36;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   // bool copyAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
      // copyAlpha = vp->copyAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[0];

      if (dst == NULL)
      {
         continue;
      }

      dst += i * dstPitch + (dstLeft >> 3) * 36;

      int dstPhase = (dstLeft % 8);

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      MTI_UINT16 *rowend = src + 3 * actWdth;

      // replace one line of image data from 16-bit 444
      if (dstPhase == 0)
      {
         goto p0;
      }
      if (dstPhase == 1)
      {
         goto p1;
      }
      if (dstPhase == 2)
      {
         goto p2;
      }
      if (dstPhase == 3)
      {
         goto p3;
      }
      if (dstPhase == 4)
      {
         goto p4;
      }
      if (dstPhase == 5)
      {
         goto p5;
      }
      if (dstPhase == 6)
      {
         goto p6;
      }
      goto p7;

      while (true)
      {

   p0: ; // phase 0

         dst[0] = (MTI_UINT8)src[2];
         dst[1] = (MTI_UINT8)(src[1] >> 4);
         dst[2] = (MTI_UINT8)((src[1] << 4) + (src[0] >> 8));
         dst[3] = (MTI_UINT8)src[0];
         ////   dst[7] = (MTI_UINT8)((dst[7]&0xf0)+(src[2]>>8));
         dst[7] = (MTI_UINT8)(src[2] >> 8);

         src += 3;
         if (src == rowend)
         {
            break;
         }

   p1: ;

         dst[4] = (MTI_UINT8)((src[2] << 4) + (src[1] >> 8));
         dst[5] = (MTI_UINT8)src[1];
         dst[6] = (MTI_UINT8)(src[0] >> 4);
         dst[7] = (MTI_UINT8)((src[0] << 4) + (dst[7] & 0x0f));
         dst[11] = (MTI_UINT8)(src[2] >> 4);

         src += 3;
         if (src == rowend)
         {
            break;
         }

   p2: ; // phase 2

         dst[8] = (MTI_UINT8)(src[1] >> 4);
         dst[9] = (MTI_UINT8)((src[1] << 4) + (src[0] >> 8));
         dst[10] = (MTI_UINT8)src[0];
         ////            dst[14] = (MTI_UINT8)((dst[14]&0xf0)+(src[2]>>8));
         dst[14] = (MTI_UINT8)(src[2] >> 8);
         dst[15] = (MTI_UINT8)src[2];

         src += 3;
         if (src == rowend)
         {
            break;
         }

   p3: ;

         dst[12] = (MTI_UINT8)src[1];
         dst[13] = (MTI_UINT8)(src[0] >> 4);
         dst[14] = (MTI_UINT8)((src[0] << 4) + (dst[14] & 0x0f));
         dst[18] = (MTI_UINT8)(src[2] >> 4);
         dst[19] = (MTI_UINT8)((src[2] << 4) + (src[1] >> 8));

         src += 3;
         if (src == rowend)
         {
            break;
         }

   p4: ; // phase 4

         dst[16] = (MTI_UINT8)((src[1] << 4) + (src[0] >> 8));
         dst[17] = (MTI_UINT8)src[0];
         ////            dst[21]= (MTI_UINT8)((dst[21]&0xf0)+(src[2]>>8));
         dst[21] = (MTI_UINT8)(src[2] >> 8);
         dst[22] = (MTI_UINT8)src[2];
         dst[23] = (MTI_UINT8)(src[1] >> 4);

         src += 3;
         if (src == rowend)
         {
            break;
         }

   p5: ;

         dst[20] = (MTI_UINT8)(src[0] >> 4);
         dst[21] = (MTI_UINT8)((src[0] << 4) + (dst[21] & 0x0f));
         dst[25] = (MTI_UINT8)(src[2] >> 4);
         dst[26] = (MTI_UINT8)((src[2] << 4) + (src[1] >> 8));
         dst[27] = (MTI_UINT8)src[1];

         src += 3;
         if (src == rowend)
         {
            break;
         }

   p6: ; // phase 6

         dst[24] = (MTI_UINT8)src[0];
         ////            dst[28] = (MTI_UINT8)((dst[28]&0xf0)+(src[2]>>8));
         dst[28] = (MTI_UINT8)(src[2] >> 8);
         dst[29] = (MTI_UINT8)src[2];
         dst[30] = (MTI_UINT8)(src[1] >> 4);
         dst[31] = (MTI_UINT8)((src[1] << 4) + (src[0] >> 8));

         src += 3;
         if (src == rowend)
         {
            break;
         }

   p7: ;

         dst[28] = (MTI_UINT8)((src[0] << 4) + (dst[28] & 0x0f));
         dst[32] = (MTI_UINT8)(src[2] >> 4);
         dst[33] = (MTI_UINT8)((src[2] << 4) + (src[1] >> 8));
         dst[34] = (MTI_UINT8)src[1];
         dst[35] = (MTI_UINT8)(src[0] >> 4);

         src += 3;
         if (src == rowend)
         {
            break;
         }

         dst += 36;

      }
   }

#if 0
   if (copyAlpha)
   { // intermediate to native

      // -> Alpha map in intermediate
      MTI_UINT8 *alphasrc = (MTI_UINT8*)(srcPels + actHght * srcPitch);

      // -> Alpha map in native
      MTI_UINT8 *alphadst = dstImage[0] + actHght * dstPitch;

      // copy the alpha matte to the native frame
      int alphasiz = getAlphaMatteSize(vp);
      for (int i = 0; i < alphasiz; i++)
      {
         alphadst[i] = alphasrc[i];
      }
   }
#endif
}

static void replaceRGB_12_LR(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstWdth = vp->frmWdth;
   int dstPitch = ((dstWdth * 36 + 31) / 32) * 4;
   int dstLeft = vp->actRect.left;
   int dstOffs = (vp->actRect.left >> 3) * 36;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   // bool copyAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
      // copyAlpha = vp->copyAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[0];

      if (dst == NULL)
      {
         continue;
      }

      dst += i * dstPitch + (dstLeft >> 3) * 36;

      int dstPhase = (dstLeft % 8);

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      MTI_UINT16 *rowend = src + 3 * actWdth;

      // replace one line of image data from 16-bit 444
      if (dstPhase == 0)
      {
         goto p0;
      }
      if (dstPhase == 1)
      {
         goto p1;
      }
      if (dstPhase == 2)
      {
         goto p2;
      }
      if (dstPhase == 3)
      {
         goto p3;
      }
      if (dstPhase == 4)
      {
         goto p4;
      }
      if (dstPhase == 5)
      {
         goto p5;
      }
      if (dstPhase == 6)
      {
         goto p6;
      }
      goto p7;

      while (true)
      {

p0: ; // phase 0

         dst[0] = (MTI_UINT8)(src[0] >> 4);
         dst[1] = (MTI_UINT8)((src[0] << 4) + (src[1] >> 8));
         dst[2] = (MTI_UINT8)src[1];
         dst[3] = (MTI_UINT8)(src[2] >> 4);
         dst[4] = (MTI_UINT8)((src[2] << 4) + (dst[4] & 0x0f));

         src += 3;
         if (src == rowend)
         {
            break;
         }

p1: ;

         dst[4] = (MTI_UINT8)((dst[4] & 0xf0) + (src[0] >> 8));
         dst[5] = (MTI_UINT8)src[0];
         dst[6] = (MTI_UINT8)(src[1] >> 4);
         dst[7] = (MTI_UINT8)((src[1] << 4) + (src[2] >> 8));
         dst[8] = (MTI_UINT8)src[2];

         src += 3;
         if (src == rowend)
         {
            break;
         }

p2: ; // phase 2

         dst[9] = (MTI_UINT8)(src[0] >> 4);
         dst[10] = (MTI_UINT8)((src[0] << 4) + (src[1] >> 8));
         dst[11] = (MTI_UINT8)src[1];
         dst[12] = (MTI_UINT8)(src[2] >> 4);
         dst[13] = (MTI_UINT8)((src[2] << 4) + (dst[13] & 0x0f));

         src += 3;
         if (src == rowend)
         {
            break;
         }

p3: ;

         dst[13] = (MTI_UINT8)((dst[13] & 0xf0) + (src[0] >> 8));
         dst[14] = (MTI_UINT8)src[0];
         dst[15] = (MTI_UINT8)(src[1] >> 4);
         dst[16] = (MTI_UINT8)((src[1] << 4) + (src[2] >> 8));
         dst[17] = (MTI_UINT8)src[2];

         src += 3;
         if (src == rowend)
         {
            break;
         }

p4: ; // phase 4

         dst[18] = (MTI_UINT8)(src[0] >> 4);
         dst[19] = (MTI_UINT8)((src[0] << 4) + (src[1] >> 8));
         dst[20] = (MTI_UINT8)src[1];
         dst[21] = (MTI_UINT8)(src[2] >> 4);
         dst[22] = (MTI_UINT8)((src[2] << 4) + (dst[22] & 0x0f));

         src += 3;
         if (src == rowend)
         {
            break;
         }

p5: ;

         dst[22] = (MTI_UINT8)((dst[22] & 0xf0) + (src[0] >> 8));
         dst[23] = (MTI_UINT8)src[0];
         dst[24] = (MTI_UINT8)(src[1] >> 4);
         dst[25] = (MTI_UINT8)((src[1] << 4) + (src[2] >> 8));
         dst[26] = (MTI_UINT8)src[2];

         src += 3;
         if (src == rowend)
         {
            break;
         }

p6: ; // phase 6

         dst[27] = (MTI_UINT8)(src[0] >> 4);
         dst[28] = (MTI_UINT8)((src[0] << 4) + (src[1] >> 8));
         dst[29] = (MTI_UINT8)src[1];
         dst[30] = (MTI_UINT8)(src[2] >> 4);
         dst[31] = (MTI_UINT8)((src[2] << 4) + (dst[31] & 0x0f));

         src += 3;
         if (src == rowend)
         {
            break;
         }

p7: ;

         dst[31] = (MTI_UINT8)((dst[31] & 0xf0) + (src[0] >> 8));
         dst[32] = (MTI_UINT8)src[0];
         dst[33] = (MTI_UINT8)(src[1] >> 4);
         dst[34] = (MTI_UINT8)((src[1] << 4) + (src[2] >> 8));
         dst[35] = (MTI_UINT8)src[2];

         src += 3;
         if (src == rowend)
         {
            break;
         }

         dst += 36;
      }
   }

#if 0
   if (copyAlpha)
   { // intermediate to native

      // -> Alpha map in intermediate
      MTI_UINT8 *alphasrc = (MTI_UINT8*)(srcPels + actHght * srcPitch);

      // -> Alpha map in native
      MTI_UINT8 *alphadst = dstImage[0] + actHght * dstPitch;

      // copy the alpha matte to the native frame
      int alphasiz = getAlphaMatteSize(vp);
      for (int i = 0; i < alphasiz; i++)
      {
         alphadst[i] = alphasrc[i];
      }
   }
#endif
}

#else

static void replaceRGB_12P(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstWdth = vp->frmWdth;
   int dstLeft = vp->actRect.left;
   int dstOffs = (vp->actRect.left >> 3) * 36;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   bool copyAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
      copyAlpha = vp->copyAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[0];

      if (dst == NULL)
      {
         continue;
      }

      int pel = i * dstWdth + dstLeft;

      dst += (pel >> 3)*36;

      int dstPhase = (pel % 8);

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      MTI_UINT16 *rowend = src + 3 * actWdth;

      // replace one line of image data from 16-bit 444
      if (dstPhase == 0)
      {
         goto p0;
      }
      if (dstPhase == 1)
      {
         goto p1;
      }
      if (dstPhase == 2)
      {
         goto p2;
      }
      if (dstPhase == 3)
      {
         goto p3;
      }
      if (dstPhase == 4)
      {
         goto p4;
      }
      if (dstPhase == 5)
      {
         goto p5;
      }
      if (dstPhase == 6)
      {
         goto p6;
      }
      goto p7;

      while (true)
      {

p0: ; // phase 0

         dst[0] = src[2];
         dst[1] = (src[1] >> 4);
         dst[2] = (src[1] << 4) + (src[0] >> 8);
         dst[3] = src[0];
         dst[7] = (dst[7] & 0xf0) + (src[2] >> 8);

         src += 3;
         if (src == rowend)
         {
            break;
         }

p1: ;

         dst[4] = (src[2] << 4) + (src[1] >> 8);
         dst[5] = src[1];
         dst[6] = (src[0] >> 4);
         dst[7] = (src[0] << 4) + (dst[7] & 0x0f);
         dst[11] = (src[2] >> 4);

         src += 3;
         if (src == rowend)
         {
            break;
         }

p2: ; // phase 2

         dst[8] = (src[1] >> 4);
         dst[9] = (src[1] << 4) + (src[0] >> 8);
         dst[14] = (dst[14] & 0xf0) + (src[2] >> 8);
         dst[15] = src[2];

         src += 3;
         if (src == rowend)
         {
            break;
         }

p3: ;

         dst[12] = src[1];
         dst[13] = (src[0] >> 4);
         dst[14] = (src[0] << 4) + (dst[14] & 0x0f);
         dst[18] = (src[2] >> 4);
         dst[19] = (src[2] << 4) + (src[1] >> 8);

         src += 3;
         if (src == rowend)
         {
            break;
         }

p4: ; // phase 4

         dst[16] = (src[1] << 4) + (src[0] >> 8);
         dst[17] = src[0];
         dst[21] = (dst[21] & 0xf0) + (src[2] >> 8);
         dst[22] = src[2];
         dst[23] = (src[1] >> 4);

         src += 3;
         if (src == rowend)
         {
            break;
         }

p5: ;

         dst[20] = (src[0] >> 4);
         dst[21] = (src[0] << 4) + (dst[21] & 0x0f);
         dst[25] = (src[2] >> 4);
         dst[26] = (src[2] << 4) + (src[1] >> 8);
         dst[27] = src[1];

         src += 3;
         if (src == rowend)
         {
            break;
         }

p6: ; // phase 6

         dst[24] = src[0];
         dst[28] = (dst[28] & 0xf0) + (src[2] >> 8);
         dst[29] = src[2];
         dst[30] = (src[1] >> 4);
         dst[31] = (src[1] << 4) + (src[0] >> 8);

         src += 3;
         if (src == rowend)
         {
            break;
         }

p7: ;

         dst[28] = (src[0] << 4) + (dst[28] & 0x0f);
         dst[32] = (src[2] >> 4);
         dst[33] = (src[2] << 4) + (src[1] >> 8);
         dst[34] = src[1];
         dst[35] = (src[0] >> 4);

         src += 3;
         if (src == rowend)
         {
            break;
         }

         dst += 36;
      }
   }

#if 0
   if (copyAlpha)
   { // intermediate to native

      // -> Alpha map in intermediate
      MTI_UINT8 *alphasrc = (MTI_UINT8*)(srcPels + actHght * srcPitch);

      // -> Alpha map in native
      MTI_UINT8 *alphadst = dstImage[0] + actHght * dstPitch;

      // copy the alpha matte to the native frame
      int alphasiz = getAlphaMatteSize(vp);
      for (int i = 0; i < alphasiz; i++)
      {
         alphadst[i] = alphasrc[i];
      }
   }
#endif
}
#endif

static void replaceRGB_12_L_LE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch; // in BYTES
   if (dstImage[0] == NULL)
   {
      // Really an error!
      return;
   }

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3; // in COMPONENTS

   int rowsPerStripe = vp->actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - (iJob * rowsPerStripe);
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {
      // Careful - dstPitch is in BYTES and srcPitch is in COMPONENTS!
      MTI_UINT16 *dst = (MTI_UINT16*)(dstImage[0] + (i * dstPitch));
      MTI_UINT16 *src = srcPels + (i * srcPitch);

      // Shift the data left 4 bits.
      // We also fill in the low order bits with a copy of the high order bits
      // so we don't just fill thos bits with 0 (I saw this in UCLA's frames)
      int nComponents = vp->actWdth * 3;
      for (int j = 0; j < nComponents; ++j)
      {
         dst[j] = (src[j] << 4) + ((src[j] >> 8) & 0x000F);
      }
   }

   // WTF! Where's the alpha stuff?
}

static void replaceRGB_12_L_BE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch; // in BYTES
   if (dstImage[0] == NULL)
   {
      // Really an error!
      return;
   }

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3; // in COMPONENTS

   int rowsPerStripe = vp->actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - (iJob * rowsPerStripe);
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {
      // Careful - dstPitch is in BYTES and srcPitch is in COMPONENTS!
      MTI_UINT16 *dst = (MTI_UINT16*)(dstImage[0] + (i * dstPitch));
      MTI_UINT16 *src = srcPels + (i * srcPitch);

      // Shift the data left 4 bits and byte swap.
      // We also fill in the low order bits with a copy of the high order bits
      // so we don't just fill thos bits with 0 (I saw this in UCLA's frames)
      int nComponents = vp->actWdth * 3;
      for (int j = 0; j < nComponents; ++j)
      {
         dst[j] = (src[j] << 12) + (src[j] >> 4) + (src[j] & 0x0F00);
      }
   }

   // WTF! Where's the alpha stuff?
}

static void replaceRGB_16_LE(void *v, int iJob)
{
CExtractor *vp = (CExtractor*) v;

MTI_UINT8 **dstImage = vp->frame;
int dstPitch = vp->frmPitch;
int dstFieldCount = vp->frmFieldCount;
int dstOffs = vp->actRect.left * 3; // QQQ this looks wrong - how can it be the same as srcOffs?

MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
int srcPitch = vp->frmWdth * 3;
int srcOffs = vp->actRect.left * 3;

int actWdth = vp->actWdth;
int actHght = vp->actHght;

int rowsPerStripe = actHght / vp->nStripes;

int begRow = vp->actRect.top + (iJob * rowsPerStripe);
int stripeRows = rowsPerStripe;
bool copyAlpha = false;
if (iJob == (vp->nStripes - 1))
{
   // Last stripe might be shorter.
   stripeRows = actHght - (iJob * rowsPerStripe);

   // The thread doing the LAST STRIPE also does the ALPHA
   copyAlpha = vp->copyAlpha;
}

for (int i = begRow; i < (begRow + stripeRows); ++i)
{
   MTI_UINT16 *src = srcPels + (i * srcPitch);
   MTI_UINT8 *dst = dstImage[i % dstFieldCount];

   if (dst == NULL)
   {
      continue;
   }

   dst += (i / dstFieldCount) * dstPitch;

   // copy the horizontal interval data to the destination
   MTImemcpy((void *)dst, (void *)src, dstOffs);

   // now set up to replace the line's active image data
   src += srcOffs;
   dst += dstOffs;

   MTI_UINT8 *srcb = (MTI_UINT8*)src;

   // replace one line of image data from 16-bit 444
   // WTF! This looks a lot like a memcpy to me!!! QQQ
   for (int j = 0; j < actWdth / 2; j++)
   {
      dst[0] = srcb[0];
      dst[1] = srcb[1];

      dst[2] = srcb[2];
      dst[3] = srcb[3];

      dst[4] = srcb[4];
      dst[5] = srcb[5];

      dst[6] = srcb[6];
      dst[7] = srcb[7];

      dst[8] = srcb[8];
      dst[9] = srcb[9];

      dst[10] = srcb[10];
      dst[11] = srcb[11];
      dst += 12;

      srcb += 12;
   }
}

// If there is an alpha matte, it wasn't interleaved, so should remain as
// a separate image that follows the RGB data. Of course we don't know the
// FUCKING BIT DEPTH of the alpha, which need not be 16 bits, although
// that's what we assume here! ANYHOW my feeling is that we should NOT be
// REPLACING the alpha at all because we are never supposed to modify it,
// so ALL of this shitty code should be nuked! QQQ
if (copyAlpha)
{
   // intermediate to native
   // -> Alpha map in intermediate
   MTI_UINT8 *alphasrc = (MTI_UINT8*)(srcPels + (actHght * srcPitch));

   // -> Alpha map in native
   MTI_UINT8 *alphadst = dstImage[0] + (actHght * dstPitch);

   // copy the alpha matte to the native frame
   int alphasiz = getAlphaMatteSize(vp);
   // WTF - just memcpy!!
   // for (int i = 0; i < alphasiz; ++i)
   // {
   // alphadst[i] = alphasrc[i];
   // }
   MTImemcpy(alphadst, alphasrc, alphasiz);
}
}

static void replaceRGBA_16_LE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch; // in BYTES

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3; // in COMPONENTS

   int actWdth = vp->actWdth; // in PIXELS
   int actHght = vp->actHght;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (actWdth - 1));
   MTIassert(vp->actRect.bottom == (actHght - 1));
   MTIassert(vp->frmFieldCount == 1);

   int rowsPerStripe = actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - begRow;
   }

   bool copyAlpha = vp->copyAlpha;
   MTIassert(copyAlpha);
   MTI_UINT16 *alphasrc = srcPels + (actHght * srcPitch) + (begRow * actWdth);

   for (int i = begRow; i < (begRow + stripeRows); ++i)
   {
      MTI_UINT8 *dstb = dstImage[0];
      if (dstb == NULL)
      {
         continue;
      }

      MTI_UINT16 *dst = (MTI_UINT16*)(dstb + (i * dstPitch));
      MTI_UINT16 *src = srcPels + (i * srcPitch);

      // replace one line of image data from 16-bit 444
      int nSrcComponents = actWdth * 3;
      for (int j = 0; j < nSrcComponents; j += 3)
      {
         dst[0] = src[j];
         dst[1] = src[j + 1];
         dst[2] = src[j + 2];
         dst[3] = copyAlpha ? *(alphasrc++) : 0;

         dst += 4;
      }
   }
}

static void replaceBGRA_16_LE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch; // in BYTES

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3; // in COMPONENTS

   int actWdth = vp->actWdth; // in PIXELS
   int actHght = vp->actHght;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (actWdth - 1));
   MTIassert(vp->actRect.bottom == (actHght - 1));
   MTIassert(vp->frmFieldCount == 1);

   int rowsPerStripe = actHght / vp->nStripes;
   int begRow = vp->actRect.top + (iJob * rowsPerStripe);
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
   }

   bool copyAlpha = vp->copyAlpha;
   MTIassert(copyAlpha);
   MTI_UINT16 *alphasrc = srcPels + (actHght * srcPitch) + (begRow * actWdth);

   for (int i = begRow; i < (begRow + stripeRows); ++i)
   {
      MTI_UINT8 *dstb = dstImage[0];
      if (dstb == NULL)
      {
         continue;
      }

      MTI_UINT16 *dst = (MTI_UINT16*)(dstb + (i * dstPitch));
      MTI_UINT16 *src = srcPels + (i * srcPitch);

      // replace one line of image data from 16-bit 444
      int nSrcComponents = actWdth * 3;
      for (int j = 0; j < nSrcComponents; j += 3)
      {
         dst[0] = src[j + 2];
         dst[1] = src[j + 1];
         dst[2] = src[j];
         dst[3] = copyAlpha ? *(alphasrc++) : 0;

         dst += 4;
      }
   }
}

static void replaceRGBA_Half(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch; // in BYTES

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3; // in COMPONENTS

   int actWdth = vp->actWdth; // in PIXELS
   int actHght = vp->actHght;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (actWdth - 1));
   MTIassert(vp->actRect.bottom == (actHght - 1));
   MTIassert(vp->frmFieldCount == 1);

   int rowsPerStripe = actHght / vp->nStripes;
   int begRow = iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - (iJob * rowsPerStripe);
   }

   bool copyAlpha = vp->copyAlpha;
   MTIassert(copyAlpha);
   MTI_UINT16 *alphasrc = srcPels + (actHght * srcPitch) + (begRow * actWdth);

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {
      MTI_UINT8 *dstb = dstImage[0];
      if (dstb == NULL)
      {
         continue;
      }

      dstb += i * dstPitch;
      MTI_UINT16 *src = srcPels + (i * srcPitch);

      // Replace one line of image data from 16-bit 444
      const MTI_UINT64 OnePointOhShiftedLeft48Bits = 0x3c00000000000000LL;
      for (int j = 0; j < actWdth; j++)
      {
         // *(MTI_UINT64 *) dstb = ((MTI_UINT64) *src
         // | (((MTI_UINT64) *(src + 1)) << 16)
         // | (((MTI_UINT64) *(src + 2)) << 32)
         // | (copyAlpha
         // ? (((MTI_UINT64) *(alphasrc++)) << 48)
         // : OnePointOhShiftedLeft48Bits))
         *(MTI_UINT64*) dstb = (((MTI_UINT64) * (src + 0)) << 48) // R
               | (((MTI_UINT64) * (src + 1)) << 32) // G
               | (((MTI_UINT64) * (src + 2)) << 16) // B
               | (copyAlpha ? (((MTI_UINT64) * (alphasrc++)) << 0) : OnePointOhShiftedLeft48Bits);

         dstb += 8;
         src += 3;
      }
   }
}

static void replaceBGR_8(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // replace one line of image data from 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         dst[0] = (MTI_UINT8)src[2];
         dst[1] = (MTI_UINT8)src[1];
         dst[2] = (MTI_UINT8)src[0];

         dst[3] = (MTI_UINT8)src[5];
         dst[4] = (MTI_UINT8)src[4];
         dst[5] = (MTI_UINT8)src[3];
         dst += 6;

         src += 6;
      }
   }
}

static void replaceBGR_10(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left) * 4;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   bool copyAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
      copyAlpha = vp->copyAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

       MTI_UINT32 r0, r1;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      // replace one line of image data from 16-bit 444
      // this is not very fast
      for (int j = 0; j < actWdth / 2; j++)
      {

         r0 = (src[2] << 22) + (src[1] << 12) + (src[0] << 2);
         r1 = (src[5] << 22) + (src[4] << 12) + (src[3] << 2);

         dst[0] = (r0 >> 24);
         dst[1] = (r0 >> 16);
         dst[2] = (r0 >> 8);
         dst[3] = (r0);

         dst[4] = (r1 >> 24);
         dst[5] = (r1 >> 16);
         dst[6] = (r1 >> 8);
         dst[7] = (r1);

         src += 6;
         dst += 8;
      }
   }

   if (copyAlpha)
   { // intermediate to native

      // -> Alpha map in intermediate
      MTI_UINT8 *alphasrc = (MTI_UINT8*)(srcPels + actHght * srcPitch);

      // -> Alpha map in native
      MTI_UINT8 *alphadst = dstImage[0] + actHght * dstPitch;

      // copy the alpha matte to the native frame
      int alphasiz = getAlphaMatteSize(vp);
      for (int i = 0; i < alphasiz; i++)
      {
         alphadst[i] = alphasrc[i];
      }
   }
}

static void replaceBGR_16_LE(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstFieldCount = vp->frmFieldCount;
   int dstOffs = (vp->actRect.left) * 3;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth * 3;
   int srcOffs = (vp->actRect.left) * 3;

   int actWdth = vp->actWdth;
   int actHght = vp->actHght;

   int rowsPerStripe = actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   bool copyAlpha = false;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = actHght - iJob * rowsPerStripe;
      copyAlpha = vp->copyAlpha;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch;

      MTI_UINT8 *dst = dstImage[i % dstFieldCount];

      if (dst == NULL)
      {
         continue;
      }

      dst += (i / dstFieldCount) * dstPitch;

      // copy the horizontal interval data to the destination
      MTImemcpy((void *)dst, (void *)src, dstOffs);

      // now set up to replace the line's active image data
      src += srcOffs;
      dst += dstOffs;

      MTI_UINT8 *srcb = (MTI_UINT8*)src;

      // replace one line of image data from 16-bit 444
      for (int j = 0; j < actWdth / 2; j++)
      {

         dst[0] = srcb[4];
         dst[1] = srcb[5];

         dst[2] = srcb[2];
         dst[3] = srcb[3];

         dst[4] = srcb[0];
         dst[5] = srcb[1];

         dst[6] = srcb[10];
         dst[7] = srcb[11];

         dst[8] = srcb[8];
         dst[9] = srcb[9];

         dst[10] = srcb[6];
         dst[11] = srcb[7];
         dst += 12;

         srcb += 12;
      }
   }

   if (copyAlpha)
   { // intermediate to native

      // -> Alpha map in intermediate
      MTI_UINT8 *alphasrc = (MTI_UINT8*)(srcPels + actHght * srcPitch);

      // -> Alpha map in native
      MTI_UINT8 *alphadst = dstImage[0] + actHght * dstPitch;

      // copy the alpha matte to the native frame
      int alphasiz = getAlphaMatteSize(vp);
      for (int i = 0; i < alphasiz; i++)
      {
         alphadst[i] = alphasrc[i];
      }
   }
}

static void replaceRGB_Half(void *v, int iJob)
{
   CExtractor *vp = (CExtractor*) v;
   const int NumberOfComponentsPerPixel = 3;

   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.right == (vp->actWdth - 1));
   MTIassert(vp->actRect.bottom == (vp->actHght - 1));
   MTIassert(vp->frmFieldCount == 1);
   MTIassert(vp->frame != nullptr && vp->frame[0] != nullptr);
   if (vp->frame == nullptr || vp->frame[0] == nullptr)
   {
      return;
   }

   MTI_UINT16 *srcImage = (MTI_UINT16*) vp->pels;
   MTI_UINT16 *dstImage = (MTI_UINT16*) vp->frame[0];

   int rowsPerStripe = vp->actHght / vp->nStripes;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - (iJob * rowsPerStripe);
   }

   int numberOfComponentsPerNormalStripe = rowsPerStripe * vp->actWdth * NumberOfComponentsPerPixel;
   int numberOfComponentsInThisStripe = stripeRows * vp->actWdth * NumberOfComponentsPerPixel;
   int startOfThisStripe = iJob * numberOfComponentsPerNormalStripe;
   int endOfThisStripe = startOfThisStripe + numberOfComponentsInThisStripe;

   for (int compIndex = startOfThisStripe; compIndex < endOfThisStripe; compIndex += 3)
   {
      dstImage[compIndex] = srcImage[compIndex + 2] /* ^ FlipSignBit */ ; // B
      dstImage[compIndex + 1] = srcImage[compIndex + 1] /* ^ FlipSignBit */ ; // G
      dstImage[compIndex + 2] = srcImage[compIndex] /* ^ FlipSignBit */ ; // R
   }
}

static void replaceRGBA_10(void *v, int iJob)
{
CExtractor *vp = (CExtractor*) v;

MTI_UINT8 **dstImage = vp->frame;
int dstWdth = vp->frmWdth;
int dstFieldCount = vp->frmFieldCount;
int dstLeft = vp->actRect.left;
int dstPhase;

MTI_UINT16 *srcPels = vp->pels;
int srcPitch = vp->frmWdth * 3;

MTI_UINT16 *alphPels = vp->pels + srcPitch * vp->frmHght;
int alphPitch = vp->frmWdth;

int actWdth = vp->actWdth;
int actHght = vp->actHght;

int rowsPerStripe = vp->actHght / vp->nStripes;

int begRow = vp->actRect.top + iJob * rowsPerStripe;
int stripeRows = rowsPerStripe;
if (iJob == (vp->nStripes - 1))
{
   stripeRows = actHght - iJob * rowsPerStripe;
}

for (int i = begRow; i < (begRow + stripeRows); i++)
{

   int rowcnt = actWdth;

   MTI_UINT16 *src = srcPels + i * srcPitch;
   MTI_UINT16 *alphsrc = alphPels + i * alphPitch;

   MTI_UINT8 *dst = dstImage[i % dstFieldCount];

   if (dst == NULL)
   {
      continue;
   }

   dstPhase = i * dstWdth + dstLeft;
   dst += (dstPhase / 3) << 4;
   dstPhase %= 3;

   // copy the horizontal interval data to the destination
   // TTT MTImemcpy((void *)dst, (void *)src, dstOffs);

   if (dstPhase == 0)
   {
      goto p0;
   }
   if (dstPhase == 1)
   {
      goto p1;
   }
   goto p2;

   while (true)
   {

p0: ;

#if MTI_ASM_X86_INTEL
      _asm
      {

         mov esi, src mov edi, dst movzx eax, WORD PTR[esi] // R
               sal eax, 22 movzx edx, WORD PTR[esi + 2] // G
               sal edx, 12 add eax, edx movzx edx, WORD PTR[esi + 4] // B
               sal edx, 2 add eax, edx bswap eax mov[edi], eax // RGB

               mov eax, [edi + 4]bswap eax and eax, 0x3ffffc // _RG
               mov esi, alphsrc movzx edx, WORD PTR[esi] // A
               sal edx, 22 add eax, edx bswap eax mov[edi + 4], eax // ARG
            }
#else
      dst[0] = (src[0] >> 2);
      dst[1] = (src[0] << 6) + (src[1] >> 4);
      dst[2] = (src[1] << 4) + (src[2] >> 6);
      dst[3] = (src[2] << 2);
      dst[4] = (alphsrc[0] >> 2);
      dst[5] = (alphsrc[0] << 6) + dst[5] & 0x3f;
#endif
      if (--rowcnt == 0)
      {
         break;
      }

p1: ;

#if MTI_ASM_X86_INTEL
      _asm
      {
         mov esi, src mov edi, dst mov eax, [edi + 4]bswap eax and eax, 0xffc00000 // A__
               movzx edx, WORD PTR[esi + 6] // R
               sal edx, 12 add eax, edx movzx edx, WORD PTR[esi + 8] // G
               sal edx, 2 add eax, edx bswap eax mov[edi + 4], eax // ARG

               mov eax, [edi + 8]and eax, 0xffc // __R
               movzx edx, WORD PTR[esi + 10] // B
               sal edx, 22 add eax, edx mov esi, alphsrc movzx edx, WORD PTR[esi + 2] // A
               sal edx, 12 add eax, edx bswap eax mov[edi + 8], eax // BAR
            }
#else
      dst[5] = (dst[5] & 0xc0) + (src[3] >> 4);
      dst[6] = (src[3] << 4) + (src[4] >> 6);
      dst[7] = (src[4] << 2);
      dst[8] = (src[5] >> 2);
      dst[9] = (src[5] << 6) + (alphsrc[1] >> 4);
      dst[10] = (alphsrc[1] << 4) + (dst[10] & 0x0f);
#endif
      if (--rowcnt == 0)
      {
         break;
      }

p2: ;

#if MTI_ASM_X86_INTEL
      _asm
      {
         mov esi, src mov edi, dst mov eax, [edi + 8]bswap eax and eax, 0xfffff000 // BA_
               movzx edx, WORD PTR[esi + 12] // R
               sal edx, 2 add eax, edx bswap eax mov[edi + 8], eax // BAR

               movzx eax, WORD PTR[esi + 14] // G
               sal eax, 22 movzx edx, WORD PTR[esi + 16] // B
               sal edx, 12 add eax, edx mov esi, alphsrc movzx edx, WORD PTR[esi + 4] // A
               sal edx, 2 add eax, edx bswap eax mov[edi + 12], eax // GBA
            }
#else
      dst[10] = (dst[10] & 0xf0) + (src[6] >> 6);
      dst[11] = (src[6] << 2);
      dst[12] = (src[7] >> 2);
      dst[13] = (src[7] << 6) + (src[8] >> 4);
      dst[14] = (src[8] << 4) + (alphsrc[2] >> 6);
      dst[15] = (alphsrc[2] << 2);
#endif
      if (--rowcnt == 0)
      {
         break;
      }

      src += 9;
      alphsrc += 3;
      dst += 16;
   }
}
}

static void replaceBGRA_10(void *v, int iJob)
{
CExtractor *vp = (CExtractor*) v;

MTI_UINT8 **dstImage = vp->frame;
int dstWdth = vp->frmWdth;
int dstFieldCount = vp->frmFieldCount;
int dstLeft = vp->actRect.left;
int dstPhase;

MTI_UINT16 *srcPels = vp->pels;
int srcPitch = vp->frmWdth * 3;

MTI_UINT16 *alphPels = vp->pels + srcPitch * vp->frmHght;
int alphPitch = vp->frmWdth;

int actWdth = vp->actWdth;
int actHght = vp->actHght;

int rowsPerStripe = vp->actHght / vp->nStripes;

int begRow = vp->actRect.top + iJob * rowsPerStripe;
int stripeRows = rowsPerStripe;
if (iJob == (vp->nStripes - 1))
{
   stripeRows = actHght - iJob * rowsPerStripe;
}

for (int i = begRow; i < (begRow + stripeRows); i++)
{

   int rowcnt = actWdth;

   MTI_UINT16 *src = srcPels + i * srcPitch;
   MTI_UINT16 *alphsrc = alphPels + i * alphPitch;

   MTI_UINT8 *dst = dstImage[i % dstFieldCount];

   if (dst == NULL)
   {
      continue;
   }

   dstPhase = i * dstWdth + dstLeft;
   dst += (dstPhase / 3) << 4;
   dstPhase %= 3;

   // copy the horizontal interval data to the destination
   // TTT MTImemcpy((void *)dst, (void *)src, dstOffs);

   if (dstPhase == 0)
   {
      goto p0;
   }
   if (dstPhase == 1)
   {
      goto p1;
   }
   goto p2;

   while (true)
   {

p0: ;

      dst[0] = (src[2] >> 2);
      dst[1] = (src[2] << 6) + (src[1] >> 4);
      dst[2] = (src[1] << 4) + (src[0] >> 6);
      dst[3] = (src[0] << 2);
      dst[4] = (alphsrc[0] >> 2);
      dst[5] = (alphsrc[0] << 6) + dst[5] & 0x3f;

      if (--rowcnt == 0)
      {
         break;
      }

p1: ;

      dst[5] = (dst[5] & 0xc0) + (src[5] >> 4);
      dst[6] = (src[5] << 4) + (src[4] >> 6);
      dst[7] = (src[4] << 2);
      dst[8] = (src[3] >> 2);
      dst[9] = (src[3] << 6) + (alphsrc[1] >> 4);
      dst[10] = (alphsrc[1] << 4) + (dst[10] & 0x0f);

      if (--rowcnt == 0)
      {
         break;
      }

p2: ;

      dst[10] = (dst[10] & 0xf0) + (src[6] >> 6);
      dst[11] = (src[8] << 2);
      dst[12] = (src[7] >> 2);
      dst[13] = (src[7] << 6) + (src[6] >> 4);
      dst[14] = (src[6] << 4) + (alphsrc[2] >> 6);
      dst[15] = (alphsrc[2] << 2);

      if (--rowcnt == 0)
      {
         break;
      }

      src += 9;
      alphsrc += 3;
      dst += 16;
   }
}
}

//static void replaceMON_10(void *v, int iJob)
//{
//   CExtractor *vp = (CExtractor*) v;
//
//   MTI_UINT8 **dstImage = vp->frame;
//   int dstWdth = vp->frmWdth;
//   int dstLeft = vp->actRect.left;
//   int dstPhase;
//
//   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
//   int srcPitch = vp->frmWdth;
//   int srcOffs = (vp->actRect.left / 3) * 3;
//
//   int actWdth = vp->actWdth;
//
//   int rowsPerStripe = vp->actHght / vp->nStripes;
//
//   int begRow = vp->actRect.top + iJob * rowsPerStripe;
//   int stripeRows = rowsPerStripe;
//   if (iJob == (vp->nStripes - 1))
//   {
//      stripeRows = vp->actHght - iJob * rowsPerStripe;
//   }
//
//   for (int i = begRow; i < (begRow + stripeRows); i++)
//   {
//
//      MTI_UINT16 *src = srcPels + i * srcPitch + srcOffs;
//
//      MTI_UINT8 *dst = dstImage[0];
//
//      if (dst == NULL)
//      {
//         continue;
//      }
//
//      dstPhase = i * dstWdth + dstLeft;
//      dst += ((dstPhase / 3) << 2);
//      dstPhase %= 3;
//
//       MTI_UINT32 r0;
//
//      // now set up to replace the line's active image data
//      int j = actWdth;
//
//      if (dstPhase == 1)
//      {
//         goto r1;
//      }
//      if (dstPhase == 2)
//      {
//         goto r2;
//      }
//      goto r0;
//
//      while (true)
//      {
//
//r0: ;
//
//         r0 = src[0];
//         dst[2] = (dst[2] & 0xfc) + (r0 >> 8);
//         dst[3] = (r0 & 0xff);
//
//         if (--j == 0)
//         {
//            break;
//         }
//
//r1: ;
//
//         r0 = src[1];
//         dst[1] = (dst[1] & 0xf0) + (r0 >> 6);
//         dst[2] = ((r0 << 2) & 0xfc) + (dst[2] & 0x03);
//
//         if (--j == 0)
//         {
//            break;
//         }
//
//r2: ;
//
//         r0 = src[2];
//         dst[0] = (r0 >> 4);
//         dst[1] = ((r0 << 4) & 0xf0) + (dst[1] & 0x0f);
//
//         if (--j == 0)
//         {
//            break;
//         }
//
//         src += 3;
//         dst += 4;
//      }
//   }
//}
//
//static void replaceMON_10P(void *v, int iJob)
//{
//   CExtractor *vp = (CExtractor*) v;
//
//   MTI_UINT8 **dstImage = vp->frame;
//   int dstPitch = vp->frmPitch;
//   int dstLeft = vp->actRect.left;
//   int dstPhase;
//
//   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
//   int srcPitch = vp->frmWdth;
//   int srcOffs = (vp->actRect.left / 4) * 4;
//
//   int actWdth = vp->actWdth;
//
//   int rowsPerStripe = vp->actHght / vp->nStripes;
//
//   int begRow = vp->actRect.top + iJob * rowsPerStripe;
//   int stripeRows = rowsPerStripe;
//   if (iJob == (vp->nStripes - 1))
//   {
//      stripeRows = vp->actHght - iJob * rowsPerStripe;
//   }
//
//   for (int i = begRow; i < (begRow + stripeRows); i++)
//   {
//
//      MTI_UINT16 *src = srcPels + i * srcPitch + srcOffs;
//
//      MTI_UINT8 *dst = dstImage[0];
//
//      if (dst == NULL)
//      {
//         continue;
//      }
//
//      dst += i * dstPitch + (dstLeft / 4) * 5;
//      dstPhase = dstLeft % 4;
//
//       MTI_UINT32 r0;
//
//      // now set up to replace the line's active image data
//      int j = actWdth;
//
//      if (dstPhase == 0)
//      {
//         goto r0;
//      }
//      if (dstPhase == 1)
//      {
//         goto r1;
//      }
//      if (dstPhase == 2)
//      {
//         goto r2;
//      }
//      goto r3;
//
//      while (true)
//      {
//
//r0: ;
//
//         r0 = src[0];
//         dst[0] = (r0);
//         dst[1] = ((r0 >> 8) & 0x03) + (dst[1] & 0xfc);
//
//         if (--j == 0)
//         {
//            break;
//         }
//
//r1: ;
//
//         r0 = src[1];
//         dst[1] = (dst[1] & 0x03) + (r0 << 2);
//         dst[2] = ((r0 >> 6) & 0x0f) + (dst[2] & 0xf0);
//
//         if (--j == 0)
//         {
//            break;
//         }
//
//r2: ;
//
//         r0 = src[2];
//         dst[2] = (dst[2] & 0x0f) + (r0 << 4);
//         dst[3] = ((r0 >> 4) & 0x3f) + (dst[3] & 0xc0);
//
//         if (--j == 0)
//         {
//            break;
//         }
//
//r3: ;
//
//         r0 = src[3];
//         dst[3] = (dst[3] & 0x3f) + (r0 << 6);
//         dst[4] = (r0 >> 2);
//
//         if (--j == 0)
//         {
//            break;
//         }
//
//         src += 4;
//         dst += 5;
//      }
//   }
//}
//
//static void replaceMON_16_LE(void *v, int iJob)
//{
//   CExtractor *vp = (CExtractor*) v;
//
//   MTI_UINT8 **dstImage = vp->frame;
//   int dstPitch = vp->frmPitch;
//   int dstFieldCount = vp->frmFieldCount;
//   int dstOffs = vp->actRect.left;
//
//   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
//   int srcPitch = vp->frmWdth;
//   int srcOffs = vp->actRect.left;
//
//   int actWdth = vp->actWdth;
//   int actHght = vp->actHght;
//
//   int rowsPerStripe = actHght / vp->nStripes;
//
//   int begRow = vp->actRect.top + (iJob * rowsPerStripe);
//   int stripeRows = rowsPerStripe;
//   bool copyAlpha = false;
//   if (iJob == (vp->nStripes - 1))
//   {
//      stripeRows = actHght - (iJob * rowsPerStripe);
//      copyAlpha = vp->copyAlpha;
//   }
//
//   for (int i = begRow; i < (begRow + stripeRows); ++i)
//   {
//
//      MTI_UINT16 *src = srcPels + (i * srcPitch);
//      MTI_UINT8 *dst = dstImage[i % dstFieldCount];
//
//      if (dst == NULL)
//      {
//         continue;
//      }
//
//      dst += (i / dstFieldCount) * dstPitch;
//
//      // copy the horizontal interval data to the destination
//      if (dstOffs > 0)
//      {
//         MTImemcpy((void *)dst, (void *)src, dstOffs);
//      }
//
//      // now set up to replace the line's active image data
//      src += srcOffs;
//      dst += dstOffs;
//
//      MTI_UINT8 *srcb = (MTI_UINT8*)src;
//
//      // replace one line of image data from 16-bit 444
//      for (int j = 0; j < actWdth / 2; ++j)
//      {
//         dst[0] = srcb[0];
//         dst[1] = srcb[1];
//
//         dst[2] = srcb[2];
//         dst[3] = srcb[3];
//
//         dst += 4;
//         srcb += 4;
//      }
//   }
//
//   if (copyAlpha)
//   {
//      // intermediate to native
//      // -> Alpha map in intermediate
//      MTI_UINT8 *alphasrc = (MTI_UINT8*)(srcPels + (actHght * srcPitch));
//
//      // -> Alpha map in native
//      MTI_UINT8 *alphadst = dstImage[0] + (actHght * dstPitch);
//
//      // copy the alpha matte to the native frame
//      int alphasiz = getAlphaMatteSize(vp);
//      for (int i = 0; i < alphasiz; ++i)
//      {
//         alphadst[i] = alphasrc[i];
//      }
//   }
//}

static void replaceY_10L(void *v, int stripeNumber)
{
   CExtractor *vp = (CExtractor*) v;

   // Old video-related crap, no longer supported!
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.right == vp->frmWdth - 1);
   MTIassert(vp->actRect.bottom == vp->frmHght - 1);
   MTIassert(vp->frmFieldCount == 1);

   MTI_UINT16 *fullImageSrc16 = (MTI_UINT16 *) vp->pels;
   MTI_UINT8 *fullImageDst8 = vp->frame[0];
   if (fullImageSrc16 == nullptr ||fullImageDst8 == nullptr)
   {
      return;
   }

   // CAREFUL: For 10-bit monochrome, THERE IS NO PADDING at the end of rows,
   // so a row may start in the middle of a 32-bit triad! That means we don't
   // need to slice the image by rows, just by a number of pixels that is
   // divisible by 3;
   int frameWidthInPixels = vp->frmWdth;
   int frameHeight = vp->frmHght;
   int toatalNumberOfPixels = frameWidthInPixels * frameHeight;
   int totalNumberOfPixelTriads = toatalNumberOfPixels / 3;
   int totalNumberOfStripes = vp->nStripes;
   int nominalPixelTriadsPerStripe = totalNumberOfPixelTriads / totalNumberOfStripes;
   int extraPixelTriads = totalNumberOfPixelTriads % totalNumberOfStripes;

   // We may have to deal with one or two leftover pixels right at the vary end,
   // just past the last stripe
   int thisIsTheLastStripe = stripeNumber == (vp->nStripes - 1);
   int extraPixelsAtEnd = toatalNumberOfPixels - (totalNumberOfPixelTriads * 3);

   // The first "extraPixelTriads" number of stripes get one extra triad each!
   int firstPixelInThisStripe = 3 * ((stripeNumber * nominalPixelTriadsPerStripe) + std::min<int>(extraPixelTriads, stripeNumber));
   int numberOfPixelTriadsInThisStripe = nominalPixelTriadsPerStripe + ((stripeNumber < extraPixelTriads) ? 1 : 0);
   int firstPixelOutOfThisStripe = firstPixelInThisStripe + (3 * numberOfPixelTriadsInThisStripe);

   auto src16 = fullImageSrc16 + firstPixelInThisStripe;
   auto dst8 = fullImageDst8 + ((firstPixelInThisStripe / 3) * 4);

   for (int pixel = firstPixelInThisStripe; pixel < firstPixelOutOfThisStripe; pixel += 3)
   {
      // CAREFUL! Always meke BIG-ENDIAN 10-bit image! If necessary, will be swapped during I/O!
      // Also note that the padding bits are the MSBs, not the LSBs like with RGB data!
      dst8[0] = src16[6] >> 2;                       // 98765432
      dst8[1] = (src16[6] << 6) | (src16[3] >> 4);   // 10 | 987654
      dst8[2] = (src16[3] << 4) | (src16[0] >> 6);   // 3210 | 9876
      dst8[3] = (src16[0] << 2);                     // 543210 | XX

      dst8 += 4;
      src16 += 9;
   }

   if (thisIsTheLastStripe)
   {
      if (extraPixelsAtEnd == 2)
      {
         dst8[0] = src16[0] >> 2;                      // 98765432
         dst8[1] = (src16[0] << 6) | (src16[3] >> 4);  // 10 | 987654
         dst8[2] = (src16[3] << 4) | (dst8[2] & 0x0F); // 3210 | ----
      }
      else if (extraPixelsAtEnd == 1)
      {
         dst8[0] = src16[0] >> 2;                      // 98765432
         dst8[1] = (src16[0] << 6) | (dst8[1] & 0x3F); // 10 | ------
      }
   }
}

static void replaceY_10R(void *v, int stripeNumber)
{
   CExtractor *vp = (CExtractor*) v;

   // Old video-related crap, no longer supported!
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.right == vp->frmWdth - 1);
   MTIassert(vp->actRect.bottom == vp->frmHght - 1);
   MTIassert(vp->frmFieldCount == 1);

   MTI_UINT16 *fullImageSrc16 = (MTI_UINT16 *) vp->pels;
   MTI_UINT8 *fullImageDst8 = vp->frame[0];
   if (fullImageSrc16 == nullptr ||fullImageDst8 == nullptr)
   {
      return;
   }

   // CAREFUL: For 10-bit monochrome, THERE IS NO PADDING at the end of rows,
   // so a row may start in the middle of a 32-bit triad! That means we don't
   // need to slice the image by rows, just by a number of pixels that is
   // divisible by 3;
   int frameWidthInPixels = vp->frmWdth;
   int frameHeight = vp->frmHght;
   int toatalNumberOfPixels = frameWidthInPixels * frameHeight;
   int totalNumberOfPixelTriads = toatalNumberOfPixels / 3;
   int totalNumberOfStripes = vp->nStripes;
   int nominalPixelTriadsPerStripe = totalNumberOfPixelTriads / totalNumberOfStripes;
   int extraPixelTriads = totalNumberOfPixelTriads % totalNumberOfStripes;

   // We may have to deal with one or two leftover pixels right at the vary end,
   // just past the last stripe
   int thisIsTheLastStripe = stripeNumber == (vp->nStripes - 1);
   int extraPixelsAtEnd = toatalNumberOfPixels - (totalNumberOfPixelTriads * 3);

   // The first "extraPixelTriads" number of stripes get one extra triad each!
   int firstPixelInThisStripe = 3 * ((stripeNumber * nominalPixelTriadsPerStripe) + std::min<int>(extraPixelTriads, stripeNumber));
   int numberOfPixelTriadsInThisStripe = nominalPixelTriadsPerStripe + ((stripeNumber < extraPixelTriads) ? 1 : 0);
   int firstPixelOutOfThisStripe = firstPixelInThisStripe + (3 * numberOfPixelTriadsInThisStripe);

   auto src16 = fullImageSrc16 + firstPixelInThisStripe;
   auto dst8 = fullImageDst8 + ((firstPixelInThisStripe / 3) * 4);

   for (int pixel = firstPixelInThisStripe; pixel < firstPixelOutOfThisStripe; pixel += 3)
   {
      // CAREFUL! Always meke BIG-ENDIAN 10-bit image! If necessary, will be swapped during I/O!
      // Also note that the padding bits are the MSBs, not the LSBs like with RGB data!
      dst8[0] = src16[6] >> 4;                       // xx987654
      dst8[1] = (src16[6] << 4) | (src16[3] >> 6);   // 3210 | 9876
      dst8[2] = (src16[3] << 2) | (src16[0] >> 8);   // 543210 | 98
      dst8[3] = src16[0];                            // 76543210

      dst8 += 4;
      src16 += 9;
   }

   if (thisIsTheLastStripe)
   {
      if (extraPixelsAtEnd == 2)
      {
         dst8[0] = src16[0] >> 4;                      // xx987654
         dst8[1] = (src16[0] << 4) | (src16[3] >> 6);  // 3210 | 9876
         dst8[2] = (src16[3] << 2) | (dst8[2] & 0x03); // 543210 | --
      }
      else if (extraPixelsAtEnd == 1)
      {
         dst8[0] = src16[0] >> 4;                      // xx987654
         dst8[1] = (src16[0] << 4) | (dst8[1] & 0x0F); // 3210 | ----
      }
   }
}

static void replaceY_10P(void *v, int iJob)
{
   MTIassert(false);  // needs to be fixed QQQ
   CExtractor *vp = (CExtractor*) v;

   MTI_UINT8 **dstImage = vp->frame;
   int dstPitch = vp->frmPitch;
   int dstLeft = vp->actRect.left;
   int dstPhase;

   MTI_UINT16 *srcPels = (MTI_UINT16*)vp->pels;
   int srcPitch = vp->frmWdth;
   int srcOffs = (vp->actRect.left / 4) * 4;

   int actWdth = vp->actWdth;

   int rowsPerStripe = vp->actHght / vp->nStripes;

   int begRow = vp->actRect.top + iJob * rowsPerStripe;
   int stripeRows = rowsPerStripe;
   if (iJob == (vp->nStripes - 1))
   {
      stripeRows = vp->actHght - iJob * rowsPerStripe;
   }

   for (int i = begRow; i < (begRow + stripeRows); i++)
   {

      MTI_UINT16 *src = srcPels + i * srcPitch + srcOffs;

      MTI_UINT8 *dst = dstImage[0];

      if (dst == NULL)
      {
         continue;
      }

      dst += i * dstPitch + (dstLeft / 4) * 5;
      dstPhase = dstLeft % 4;

       MTI_UINT32 r0;

      // now set up to replace the line's active image data
      int j = actWdth;

      if (dstPhase == 0)
      {
         goto r0;
      }
      if (dstPhase == 1)
      {
         goto r1;
      }
      if (dstPhase == 2)
      {
         goto r2;
      }
      goto r3;

      while (true)
      {

r0: ;

         r0 = src[0];
         dst[0] = (r0);
         dst[1] = ((r0 >> 8) & 0x03) + (dst[1] & 0xfc);

         if (--j == 0)
         {
            break;
         }

r1: ;

         r0 = src[1];
         dst[1] = (dst[1] & 0x03) + (r0 << 2);
         dst[2] = ((r0 >> 6) & 0x0f) + (dst[2] & 0xf0);

         if (--j == 0)
         {
            break;
         }

r2: ;

         r0 = src[2];
         dst[2] = (dst[2] & 0x0f) + (r0 << 4);
         dst[3] = ((r0 >> 4) & 0x3f) + (dst[3] & 0xc0);

         if (--j == 0)
         {
            break;
         }

r3: ;

         r0 = src[3];
         dst[3] = (dst[3] & 0x3f) + (r0 << 6);
         dst[4] = (r0 >> 2);

         if (--j == 0)
         {
            break;
         }

         src += 4;
         dst += 5;
      }
   }
}

// SOURCE is internal format YYY - 3-component pixels
// DESTINATION is Y - one component pixels
static void replaceY_16_LE(void *v, int stripeNumber)
{
   CExtractor *vp = (CExtractor*) v;

   // Old video-related crap, no longer supported!
   MTIassert(vp->actRect.top == 0);
   MTIassert(vp->actRect.left == 0);
   MTIassert(vp->actRect.right == vp->frmWdth - 1);
   MTIassert(vp->actRect.bottom == vp->frmHght - 1);
   MTIassert(vp->frmFieldCount == 1);

   MTI_UINT16 *src = (MTI_UINT16 *) vp->pels;
   MTI_UINT16 *dst = (MTI_UINT16 *) vp->frame[0];
   if (src == nullptr ||dst == nullptr)
   {
      return;
   }

   int frameWidthInPixels = vp->frmWdth;
   int frameHeight = vp->frmHght;
   int totalNumberOfStripes = vp->nStripes;
   int nominalRowsPerStripe = frameHeight / totalNumberOfStripes;
   int extraRows = frameHeight % totalNumberOfStripes;

   // The first "extraRows" number of stripes get one extra row each!
   int firstRowInThisStripe = (stripeNumber * nominalRowsPerStripe) + std::min<int>(extraRows, stripeNumber);
   int numberOfRowsInThisStripe = nominalRowsPerStripe + ((stripeNumber < extraRows) ? 1 : 0);

   auto stripeSrc = src + (firstRowInThisStripe * frameWidthInPixels * 3);
   auto stripeDst = dst + (firstRowInThisStripe * frameWidthInPixels);
   auto numberOfPixelsToReplace = numberOfRowsInThisStripe * frameWidthInPixels;

   for (auto pixel = 0; pixel < numberOfPixelsToReplace; ++pixel)
   {
      stripeDst[pixel] = stripeSrc[pixel * 3];  // Arbitrarily use RED channel.
   }
}

// replace the active picture (specified in image format) in a frame
// using as much multithreading as possible
void CExtractor::replaceActivePicture(MTI_UINT8 **dstimg, // -> dest field(s) (YUV422 or RGB variant)
      const CImageFormat *srcfmt, // image format of dest frame)
      MTI_UINT16 *srcpels // array of 16-bit "444 data" (YUV or RGB)
      )
{
   frame = dstimg;
   pels = srcpels;
   frmFieldCount = srcfmt->getFieldCount();
   frmWdth = srcfmt->getPixelsPerLine();
   frmHght = srcfmt->getLinesPerFrame();
   frmPitch = srcfmt->getFramePitch();
   actRect = srcfmt->getActivePictureRect();
   actWdth = actRect.right - actRect.left + 1;
   actHght = actRect.bottom - actRect.top + 1;
   clipAlphaType = srcfmt->getAlphaMatteType();
   copyAlpha = false;

   int frmPelComponents = srcfmt->getPixelComponents();
   int frmPelPacking = srcfmt->getPixelPacking();

   // set up the mthread structure
   MTHREAD_STRUCT tsThread;
   tsThread.PrimaryFunction   = (void (*)(void *,int))&null;
   tsThread.SecondaryFunction = NULL;
   tsThread.CallBackFunction  = NULL;
   tsThread.vpApplicationData = this;
   tsThread.iNThread          = nStripes;
   tsThread.iNJob             = nStripes;

   switch (frmPelComponents)
   {

   case IF_PIXEL_COMPONENTS_YUV422:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceYUV_8;
         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceYUV_10;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceDVS_10;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceDVS_10a;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceDVS_10BE;
         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_YUV444:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceUYV_8;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceUYV_10;
         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_RGB:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGB_8;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (clipAlphaType == IF_ALPHA_MATTE_2_BIT_EMBEDDED) ? (void(*)(void *, int)) & replaceRGB_10_A2 :
               (void(*)(void *, int)) & replaceRGB_10;
         copyAlpha = ((clipAlphaType == IF_ALPHA_MATTE_1_BIT) || (clipAlphaType == IF_ALPHA_MATTE_2_BIT_EMBEDDED) ||
               (clipAlphaType == IF_ALPHA_MATTE_8_BIT) || (clipAlphaType == IF_ALPHA_MATTE_16_BIT));
         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
#ifndef DPX12_PACKED
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGB_12;
#else
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGB_12P;
#endif
         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
#ifndef DPX12_PACKED
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGB_12_LR;
#else
         // tsThread.PrimaryFunction   = (void (*)(void *,int))&replaceRGB_12P;
#endif
         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGB_12_L_BE;
         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGB_12_L_LE;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGB_16_LE;
         copyAlpha = (clipAlphaType == IF_ALPHA_MATTE_1_BIT);
         break;

      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGB_Half;
         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_BGR:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceBGR_8;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceBGR_10;
         copyAlpha = ((clipAlphaType == IF_ALPHA_MATTE_1_BIT) || (clipAlphaType == IF_ALPHA_MATTE_8_BIT) ||
               (clipAlphaType == IF_ALPHA_MATTE_16_BIT));
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceBGR_16_LE;
         copyAlpha = (clipAlphaType == IF_ALPHA_MATTE_1_BIT);
         break;

      default:
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_RGBA:

      copyAlpha = true;

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGBA_10;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGBA_16_LE;
         break;

      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceRGBA_Half;
         break;

      default:
         break;

      }
      break;

   case IF_PIXEL_COMPONENTS_BGRA:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceBGRA_10;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceBGRA_16_LE;
         break;

      default:
         break;

      }
      break;

   case IF_PIXEL_COMPONENTS_Y:
   case IF_PIXEL_COMPONENTS_YYY: // xyzzy1

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceY_10L;
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceY_10R;
         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceY_10P;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
         tsThread.PrimaryFunction = (void(*)(void *, int)) & replaceY_16_LE;
         break;
      }

      break;

   default:
      break;

   }

//   if (frmPelComponents != IF_PIXEL_COMPONENTS_LUMINANCE)
//   {
//
      // copy vertical interval data frm 16-bit progressive field
      for (int i = 0; i < actRect.top; i++)
      {
         MTI_UINT16 *src = pels + frmWdth * 3 * i;
         MTI_UINT8 *dst = dstimg[i % frmFieldCount];
         if (dst == NULL)
         {
            continue;
         }
         dst += frmPitch * (i / frmFieldCount);

         MTImemcpy((void *)dst, (void *)src, frmPitch);
      }
//   }

   // now replace the active picture stripe by stripe
#ifdef NO_MULTI
   for (int  i = 0; i < nStripes; i++)
   {
      tsThread.PrimaryFunction(this, i);
   }
#else
   // now allocate, run, and free the mthreads
   if (nStripes > 1)
   {
      int iRet = MThreadAlloc(&tsThread);
      if (iRet)
      {
            TRACE_0(errout << "Convert: MThreadAlloc failed, error=" << iRet);
      }
      else
      {
         iRet = MThreadStart(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadStart failed, error=" << iRet);
         }

         iRet = MThreadFree(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadFree failed, error=" << iRet);
         }
      }
   }
   else // nStripes <= 0
   {
      // If we're only doing one stripe, call the function directly.
      tsThread.PrimaryFunction(this, 0);
   }
#endif
}

// extract a subregion from a clip frame, placing the result in an array of 16-bit values
// comprising either explicit YUV or RGB -- 48 bits per pel.
int CExtractor::extractSubregion(MTI_UINT16 *dstpels, // array of 16-bit "444 data" (YUV or RGB)
      MTI_UINT8 **srcimg, // -> source field(s) (YUV422 or RGB variant)
      const CImageFormat *srcfmt, // image format of source frame
      RECT *srcrgn // region to be extracted
      )
{
   return extractSubregion(dstpels, srcimg, srcfmt, *srcrgn);
}

int CExtractor::extractSubregion(MTI_UINT16 *dstpels, // array of 16-bit "444 data" (YUV or RGB)
      MTI_UINT8 **srcimg, // -> source field(s) (YUV422 or RGB variant)
      const CImageFormat *srcfmt, // image format of source frame
      const RECT &srcrgn // region to be extracted
      )
{
   int retval = 0; // assume supported format

   int frmFieldCount = srcfmt->getFieldCount();
   int frmWdth = srcfmt->getTotalFrameWidth();
   int frmPitch = srcfmt->getFramePitch();
   int frmPelComponents = srcfmt->getPixelComponents();
   int frmPelPacking = srcfmt->getPixelPacking();
   int rowPels = srcrgn.right - srcrgn.left + 1;
   int left = srcrgn.left;
   bool lftOdd = ((left & 1) == 1);
   int lftOffs;
   int phase;

   unsigned int r0, r1, r2, r3;

   switch (frmPelComponents)
   {

   case IF_PIXEL_COMPONENTS_YUV422:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left >> 1) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            int pelcnt = rowPels;

            if (lftOdd)
            { // beg on 2nd half of pixel pair
               dstpels[0] = ((MTI_UINT16)src[3]); // Y
               dstpels[1] = ((MTI_UINT16)src[0]); // U
               dstpels[2] = ((MTI_UINT16)src[2]); // V

               dstpels += 3;

               src += 4;
               --pelcnt;
            }

            if (pelcnt != 0)
            {

               for (; ;)
               { // on 1st half of pixel pair

                  dstpels[0] = ((MTI_UINT16)src[1]); // Y
                  dstpels[1] = ((MTI_UINT16)src[0]); // U
                  dstpels[2] = ((MTI_UINT16)src[2]); // V

                  dstpels += 3;

                  if ((--pelcnt) == 0)
                  {
                     break;
                  }

                  dstpels[0] = ((MTI_UINT16)src[3]); // Y
                  dstpels[1] = ((MTI_UINT16)src[0]); // U
                  dstpels[2] = ((MTI_UINT16)src[2]); // V

                  dstpels += 3;

                  if ((--pelcnt) == 0)
                  {
                     break;
                  }

                  src += 4;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         lftOffs = (left >> 1) * 5;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            int pelcnt = rowPels;

            if (lftOdd)
            { // beg on 2nd half of pixel pair
               dstpels[0] = ((((MTI_UINT16)src[3]) & 0x03) << 8) + ((MTI_UINT16)src[4]); // Y
               dstpels[1] = (((MTI_UINT16)src[0]) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // U
               dstpels[2] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + (((MTI_UINT16)src[3]) >> 2); // V

               dstpels += 3;
               src += 5;
               --pelcnt;
            }

            if (pelcnt != 0)
            {

               for (; ;)
               { // on 1st half of pixel pair

                  dstpels[0] = ((((MTI_UINT16)src[1]) & 0x3f) << 4) + ((((MTI_UINT16)src[2]) & 0xf0) >> 4); // Y
                  dstpels[1] = (((MTI_UINT16)src[0]) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // U
                  dstpels[2] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + (((MTI_UINT16)src[3]) >> 2); // V

                  dstpels += 3;
                  if ((--pelcnt) == 0)
                  {
                     break;
                  }

                  dstpels[0] = ((((MTI_UINT16)src[3]) & 0x03) << 8) + ((MTI_UINT16)src[4]); // Y
                  dstpels[1] = (((MTI_UINT16)src[0]) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // U
                  dstpels[2] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + (((MTI_UINT16)src[3]) >> 2); // V

                  dstpels += 3;
                  if ((--pelcnt) == 0)
                  {
                     break;
                  }

                  src += 5;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            int pelcnt = rowPels;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
            r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
            r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
            r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
            r0 = *((int *)(src));
            r1 = *((int *)(src + 4));
            r2 = *((int *)(src + 8));
            r3 = *((int *)(src + 12));
#endif

            if (phase == 0)
            {
               goto p0;
            }
            if (phase == 1)
            {
               goto p1;
            }
            if (phase == 2)
            {
               goto p2;
            }
            if (phase == 3)
            {
               goto p3;
            }
            if (phase == 4)
            {
               goto p4;
            }
            goto p5;

            while (true)
            {

p0: ;

               dstpels[0] = (r0 >> 10) & 0x3ff; // Y
               dstpels[1] = (r0) & 0x3ff; // U
               dstpels[2] = (r0 >> 20) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p1: ;

               dstpels[0] = (r1) & 0x3ff; // Y
               dstpels[1] = (r0) & 0x3ff; // U
               dstpels[2] = (r0 >> 20) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p2: ;

               dstpels[0] = (r1 >> 20) & 0x3ff; // Y
               dstpels[1] = (r1 >> 10) & 0x3ff; // U
               dstpels[2] = (r2) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p3: ;

               dstpels[0] = (r2 >> 10) & 0x3ff; // Y
               dstpels[1] = (r1 >> 10) & 0x3ff; // U
               dstpels[2] = (r2) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p4: ;

               dstpels[0] = (r3) & 0x3ff; // Y
               dstpels[1] = (r2 >> 20) & 0x3ff; // U
               dstpels[2] = (r3 >> 10) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p5: ;

               dstpels[0] = (r3 >> 20) & 0x3ff; // Y
               dstpels[1] = (r2 >> 20) & 0x3ff; // U
               dstpels[2] = (r3 >> 10) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 16;

#if ENDIAN == MTI_BIG_ENDIAN
               r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
               r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
               r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
               r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
               r0 = *((int *)(src));
               r1 = *((int *)(src + 4));
               r2 = *((int *)(src + 8));
               r3 = *((int *)(src + 12));
#endif

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            int pelcnt = rowPels;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
            r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
            r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
            r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
            r0 = *((int *)(src));
            r1 = *((int *)(src + 4));
            r2 = *((int *)(src + 8));
            r3 = *((int *)(src + 12));
#endif

            if (phase == 0)
            {
               goto q0;
            }
            if (phase == 1)
            {
               goto q1;
            }
            if (phase == 2)
            {
               goto q2;
            }
            if (phase == 3)
            {
               goto q3;
            }
            if (phase == 4)
            {
               goto q4;
            }
            goto q5;

            while (true)
            {

q0: ;

               dstpels[0] = ((r0 >> 6) & 0x3fc) + ((r0 >> 26) & 0x03); // Y
               dstpels[1] = ((r0 << 2) & 0x3fc) + ((r0 >> 28) & 0x03); // U
               dstpels[2] = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03); // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q1: ;

               dstpels[0] = ((r1 << 2) & 0x3fc) + ((r1 >> 28) & 0x03); // Y
               dstpels[1] = ((r0 << 2) & 0x3fc) + ((r0 >> 28) & 0x03); // U
               dstpels[2] = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03); // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q2: ;

               dstpels[0] = ((r1 >> 14) & 0x3fc) + ((r1 >> 24) & 0x03); // Y
               dstpels[1] = ((r1 >> 6) & 0x3fc) + ((r1 >> 26) & 0x03); // U
               dstpels[2] = ((r2 << 2) & 0x3fc) + ((r2 >> 28) & 0x03); // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q3: ;

               dstpels[0] = ((r2 >> 6) & 0x3fc) + ((r2 >> 26) & 0x03); // Y
               dstpels[1] = ((r1 >> 6) & 0x3fc) + ((r1 >> 26) & 0x03); // U
               dstpels[2] = ((r2 << 2) & 0x3fc) + ((r2 >> 28) & 0x03); // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q4: ;

               dstpels[0] = ((r3 << 2) & 0x3fc) + ((r3 >> 28) & 0x03); // Y
               dstpels[1] = ((r2 >> 14) & 0x3fc) + ((r2 >> 24) & 0x03); // U
               dstpels[2] = ((r3 >> 6) & 0x3fc) + ((r3 >> 26) & 0x03); // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q5: ;

               dstpels[0] = ((r3 >> 14) & 0x3fc) + ((r3 >> 24) & 0x03); // Y
               dstpels[1] = ((r2 >> 14) & 0x3fc) + ((r2 >> 24) & 0x03); // U
               dstpels[2] = ((r3 >> 6) & 0x3fc) + ((r3 >> 26) & 0x03); // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 16;

#if ENDIAN == MTI_BIG_ENDIAN
               r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
               r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
               r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
               r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
               r0 = *((int *)(src));
               r1 = *((int *)(src + 4));
               r2 = *((int *)(src + 8));
               r3 = *((int *)(src + 12));
#endif

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L: // CLIPSTER

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            int pelcnt = rowPels;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = *((int *)(src));
            r1 = *((int *)(src + 4));
            r2 = *((int *)(src + 8));
            r3 = *((int *)(src + 12));
#else
#if MTI_ASM_X86_INTEL
            _asm
            { // swap endian
               mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
               eax mov eax, [ebx + 12]bswap eax mov r3, eax
            }
#else
            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + (src[3]);
            r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + (src[7]);
            r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + (src[11]);
            r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + (src[15]);
#endif
#endif
            if (phase == 0)
            {
               goto r0;
            }
            if (phase == 1)
            {
               goto r1;
            }
            if (phase == 2)
            {
               goto r2;
            }
            if (phase == 3)
            {
               goto r3;
            }
            if (phase == 4)
            {
               goto r4;
            }
            goto r5;

            while (true)
            {

r0: ;

               dstpels[0] = (r0 >> 12) & 0x3ff; // Y
               dstpels[1] = (r0 >> 22) & 0x3ff; // U
               dstpels[2] = (r0 >> 2) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r1: ;

               dstpels[0] = (r1 >> 22) & 0x3ff; // Y
               dstpels[1] = (r0 >> 22) & 0x3ff; // U
               dstpels[2] = (r0 >> 2) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r2: ;

               dstpels[0] = (r1 >> 2) & 0x3ff; // Y
               dstpels[1] = (r1 >> 12) & 0x3ff; // U
               dstpels[2] = (r2 >> 22) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r3: ;

               dstpels[0] = (r2 >> 12) & 0x3ff; // Y
               dstpels[1] = (r1 >> 12) & 0x3ff; // U
               dstpels[2] = (r2 >> 22) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r4: ;

               dstpels[0] = (r3 >> 22) & 0x3ff; // Y
               dstpels[1] = (r2 >> 2) & 0x3ff; // U
               dstpels[2] = (r3 >> 12) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r5: ;

               dstpels[0] = (r3 >> 2) & 0x3ff; // Y
               dstpels[1] = (r2 >> 2) & 0x3ff; // U
               dstpels[2] = (r3 >> 12) & 0x3ff; // V

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 16;

#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((int *)(src));
               r1 = *((int *)(src + 4));
               r2 = *((int *)(src + 8));
               r3 = *((int *)(src + 12));
#else
#if MTI_ASM_X86_INTEL
               _asm
               { // swap endian
                  mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax,
                  [ebx + 8]bswap eax mov r2, eax mov eax, [ebx + 12]bswap eax mov r3, eax
               }
#else
               r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + (src[3]);
               r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + (src[7]);
               r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + (src[11]);
               r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + (src[15]);
#endif
#endif

            }

         }

         break;

      default:
         retval = -1;
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_YUV444:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            for (int i = 0; i < rowPels; i++)
            {

               dstpels[0] = (MTI_UINT16)src[1]; // Y
               dstpels[1] = (MTI_UINT16)src[0]; // U
               dstpels[2] = (MTI_UINT16)src[2]; // V

               dstpels += 3;
               src += 3;

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         lftOffs = (left) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            for (int i = 0; i < rowPels; i++)
            {

               dstpels[0] = ((((MTI_UINT16)src[1]) & 0xfc) >> 2) + ((((MTI_UINT16)src[2]) & 0x0f) << 6); // Y
               dstpels[1] = ((((MTI_UINT16)src[0]))) + ((((MTI_UINT16)src[1]) & 0x3) << 8); // U
               dstpels[2] = ((((MTI_UINT16)src[2]) & 0xf0) >> 4) + ((((MTI_UINT16)src[3]) & 0x3f) << 4); // V

               dstpels += 3;
               src += 4;

            }

         }

         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_RGB:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            for (int i = 0; i < rowPels; i++)
            {

               dstpels[0] = (MTI_UINT16)src[0]; // R
               dstpels[1] = (MTI_UINT16)src[1]; // G
               dstpels[2] = (MTI_UINT16)src[2]; // B

               dstpels += 3;
               src += 3;

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         lftOffs = (left) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            for (int i = 0; i < rowPels; i++)
            {

               dstpels[0] = ((((MTI_UINT16)src[0])) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // R
               dstpels[1] = ((((MTI_UINT16)src[1]) & 0x3f) << 4) + ((((MTI_UINT16)src[2]) & 0xf0) >> 4); // G
               dstpels[2] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + ((((MTI_UINT16)src[3]) & 0xfc) >> 2); // B

               dstpels += 3;
               src += 4;

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         lftOffs = (left) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            for (int i = 0; i < rowPels; i++)
            {

               dstpels[0] = ((((MTI_UINT16)src[0]) & 0x3f) << 4) + ((((MTI_UINT16)src[1]) & 0xf0) >> 4); // R
               dstpels[1] = ((((MTI_UINT16)src[1]) & 0x0f) << 6) + ((((MTI_UINT16)src[2]) & 0xfc) >> 2); // G
               dstpels[2] = ((((MTI_UINT16)src[2]) & 0x03) << 8) + ((((MTI_UINT16)src[3]))); // B

               dstpels += 3;
               src += 4;

            }

         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:

         lftOffs = (left >> 3) * 36;
         phase = left % 8;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            int pelcnt = rowPels;
#ifndef DPX12_PACKED
            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
#else
            int pel = irow * frmWdth + left;
            MTI_UINT8 *src = srcimg[0] + (pel >> 3) * 36;
            phase = pel % 8;
#endif
            if (phase == 0)
            {
               goto s0;
            }
            if (phase == 1)
            {
               goto s1;
            }
            if (phase == 2)
            {
               goto s2;
            }
            if (phase == 3)
            {
               goto s3;
            }
            if (phase == 4)
            {
               goto s4;
            }
            if (phase == 5)
            {
               goto s5;
            }
            if (phase == 6)
            {
               goto s6;
            }
            goto s7;

            while (true)
            {

s0:

               dstpels[0] = ((src[2] & 0x0f) << 8) + src[3];
               dstpels[1] = (src[1] << 4) + ((src[2] & 0xf0) >> 4);
               dstpels[2] = ((src[7] & 0x0f) << 8) + src[0];
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s1:

               dstpels[0] = (src[6] << 4) + ((src[7] & 0xf0) >> 4);
               dstpels[1] = ((src[4] & 0x0f) << 8) + src[5];
               dstpels[2] = (src[11] << 4) + ((src[4] & 0xf0) >> 4);
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s2:
               dstpels[0] = ((src[9] & 0x0f) << 8) + src[10];
               dstpels[1] = (src[8] << 4) + ((src[9] & 0xf0) >> 4);
               dstpels[2] = ((src[14] & 0x0f) << 8) + src[15];
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s3:

               dstpels[0] = (src[13] << 4) + ((src[14] & 0xf0) >> 4);
               dstpels[1] = ((src[19] & 0x0f) << 8) + src[12];
               dstpels[2] = (src[18] << 4) + ((src[19] & 0xf0) >> 4);
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s4:

               dstpels[0] = ((src[16] & 0x0f) << 8) + src[17];
               dstpels[1] = (src[23] << 4) + ((src[16] & 0xf0) >> 4);
               dstpels[2] = ((src[21] & 0x0f) << 8) + src[22];
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s5:

               dstpels[0] = (src[20] << 4) + ((src[21] & 0xf0) >> 4);
               dstpels[1] = ((src[26] & 0x0f) << 8) + src[27];
               dstpels[2] = (src[25] << 4) + ((src[26] & 0xf0) >> 4);
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s6:

               dstpels[0] = ((src[31] & 0x0f) << 8) + src[24];
               dstpels[1] = (src[30] << 4) + ((src[31] & 0xf0) >> 4);
               dstpels[2] = ((src[28] & 0x0f) << 8) + src[29];
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s7:

               dstpels[0] = (src[35] << 4) + ((src[28] & 0xf0) >> 4);
               dstpels[1] = ((src[33] & 0x0f) << 8) + src[34];
               dstpels[2] = (src[32] << 4) + ((src[33] & 0xf0) >> 4);
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 36;
            }
         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:

         lftOffs = (left >> 3) * 36;
         phase = left % 8;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            int pelcnt = rowPels;
#ifndef DPX12_PACKED
            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
#else
            int pel = irow * frmWdth + left;
            MTI_UINT8 *src = srcimg[0] + (pel >> 3) * 36;
            phase = pel % 8;
#endif
            if (phase == 0)
            {
               goto t0;
            }
            if (phase == 1)
            {
               goto t1;
            }
            if (phase == 2)
            {
               goto t2;
            }
            if (phase == 3)
            {
               goto t3;
            }
            if (phase == 4)
            {
               goto t4;
            }
            if (phase == 5)
            {
               goto t5;
            }
            if (phase == 6)
            {
               goto t6;
            }
            goto t7;

            while (true)
            {

t0:

               dstpels[0] = (src[0] << 4) + (src[1] >> 4);
               dstpels[1] = ((src[1] & 0x0f) << 8) + src[2];
               dstpels[2] = (src[3] << 4) + (src[4] >> 4);
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t1:

               dstpels[0] = ((src[4] & 0x0f) << 8) + src[5];
               dstpels[1] = (src[6] << 4) + (src[7] >> 4);
               dstpels[2] = ((src[7] & 0x0f) << 8) + src[8];
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t2:
               dstpels[0] = (src[9] << 4) + (src[10] >> 4);
               dstpels[1] = ((src[10] & 0x0f) << 8) + src[11];
               dstpels[2] = (src[12] << 4) + (src[13] >> 4);
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t3:

               dstpels[0] = ((src[13] & 0x0f) << 8) + src[14];
               dstpels[1] = (src[15] << 4) + (src[16] >> 4);
               dstpels[2] = ((src[16] & 0x0f) << 8) + src[17];
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t4:

               dstpels[0] = (src[18] << 4) + (src[19] >> 4);
               dstpels[1] = ((src[19] & 0x0f) << 8) + src[20];
               dstpels[2] = (src[21] << 4) + (src[22] >> 4);
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t5:

               dstpels[0] = ((src[22] & 0x0f) << 8) + src[23];
               dstpels[1] = (src[24] << 4) + (src[25] >> 4);
               dstpels[2] = ((src[25] & 0x0f) << 8) + src[26];
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t6:

               dstpels[0] = (src[27] << 4) + (src[28] >> 4);
               dstpels[1] = ((src[28] & 0x0f) << 8) + src[29];
               dstpels[2] = (src[30] << 4) + (src[31] >> 4);
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t7:

               dstpels[0] = ((src[31] & 0x0f) << 8) + src[32];
               dstpels[1] = (src[33] << 4) + (src[34] >> 4);
               dstpels[2] = ((src[34] & 0x0f) << 8) + src[35];
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 36;
            }
         }

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:

         lftOffs = (left) * 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            for (int i = 0; i < rowPels; i++)
            {

               dstpels[0] = ((((MTI_UINT16)src[1]) << 8) + (MTI_UINT16)src[0]) >> 4; // R
               dstpels[1] = ((((MTI_UINT16)src[3]) << 8) + (MTI_UINT16)src[2]) >> 4; // G
               dstpels[2] = ((((MTI_UINT16)src[5]) << 8) + (MTI_UINT16)src[4]) >> 4; // B

               dstpels += 3;
               src += 6;

            }

         }

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:

         lftOffs = (left) * 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            for (int i = 0; i < rowPels; i++)
            {

               dstpels[0] = ((((MTI_UINT16)src[0]) << 8) + (MTI_UINT16)src[1]) >> 4; // R
               dstpels[1] = ((((MTI_UINT16)src[2]) << 8) + (MTI_UINT16)src[3]) >> 4; // G
               dstpels[2] = ((((MTI_UINT16)src[4]) << 8) + (MTI_UINT16)src[5]) >> 4; // B

               dstpels += 3;
               src += 6;

            }

         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         if (left == 0 && rowPels == frmWdth && frmFieldCount == 1)
         {
            int nBytes = ((srcrgn.bottom - srcrgn.top) + 1) * frmPitch;
            MTImemcpy(dstpels, srcimg, nBytes);
         }
         else
         {
            lftOffs = (left) * 6;

            for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
            {

               MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dstpels[0] = (((MTI_UINT16)src[1]) << 8) + ((MTI_UINT16)src[0]); // R
                  dstpels[1] = (((MTI_UINT16)src[3]) << 8) + ((MTI_UINT16)src[2]); // G
                  dstpels[2] = (((MTI_UINT16)src[5]) << 8) + ((MTI_UINT16)src[4]); // B

                  dstpels += 3;
                  src += 6;
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:

         if (left == 0 && rowPels == frmWdth && frmFieldCount == 1)
         {
            int nBytes = ((srcrgn.bottom - srcrgn.top) + 1) * frmPitch;
            MTImemcpy(dstpels, srcimg, nBytes);
         }
         else
         {
            lftOffs = (left) * 6;

            for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
            {

               MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {
                  dstpels[2] = (((MTI_UINT16)src[1]) << 8) + ((MTI_UINT16)src[0]); // R
                  dstpels[1] = (((MTI_UINT16)src[3]) << 8) + ((MTI_UINT16)src[2]); // G
                  dstpels[0] = (((MTI_UINT16)src[5]) << 8) + ((MTI_UINT16)src[4]); // B

                  dstpels += 3;
                  src += 6;
               }
            }
         }

         break;

      default:
         retval = -1; // unsupported format
         break;
      }

      break;

   case IF_PIXEL_COMPONENTS_RGBA:
      {
         const MTI_UINT64 FlipSignBits = 0x0000800080008000;

         switch (frmPelPacking)
         {

         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

            lftOffs = left * 8;

            for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
            {
               MTI_UINT8 *src = srcimg[0] + (irow * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {
                  dstpels[0] = (((MTI_UINT16)src[1]) << 8) + ((MTI_UINT16)src[0]); // R
                  dstpels[1] = (((MTI_UINT16)src[3]) << 8) + ((MTI_UINT16)src[2]); // G
                  dstpels[2] = (((MTI_UINT16)src[5]) << 8) + ((MTI_UINT16)src[4]); // B

                  dstpels += 3;
                  src += 8;
               }
            }

            break;

         case IF_PIXEL_PACKING_1_Half_IN_2Bytes:

            lftOffs = left * 8;

            for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
            {
               MTI_UINT8 *src = srcimg[0] + (irow * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {
                  MTI_UINT64 temp = (*(MTI_UINT64*) src) /* ^ FlipSignBits */ ;
                  dstpels[3] = temp;
                  dstpels[2] = temp >> 16;
                  dstpels[1] = temp >> 32;

                  dstpels += 3;
                  src += 8;
               }
            }

            break;

         default:
            retval = -1; // unsupported format
            break;
         }
      } break;

   case IF_PIXEL_COMPONENTS_BGR:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            for (int i = 0; i < rowPels; i++)
            {

               dstpels[0] = (MTI_UINT16)src[2]; // R
               dstpels[1] = (MTI_UINT16)src[1]; // G
               dstpels[2] = (MTI_UINT16)src[0]; // B

               dstpels += 3;
               src += 3;
            }
         }
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         lftOffs = (left) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

            for (int i = 0; i < rowPels; i++)
            {

               dstpels[2] = ((((MTI_UINT16)src[0])) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // B
               dstpels[1] = ((((MTI_UINT16)src[1]) & 0x3f) << 4) + ((((MTI_UINT16)src[2]) & 0xf0) >> 4); // G
               dstpels[0] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + ((((MTI_UINT16)src[3]) & 0xfc) >> 2); // R

               dstpels += 3;
               src += 4;
            }
         }
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:

         if (left == 0 && frmFieldCount == 1)
         {
            int nBytes = ((srcrgn.bottom - srcrgn.top) + 1) * frmPitch;
            MTImemcpy(dstpels, srcimg, nBytes);
         }
         else
         {
            lftOffs = (left) * 6;

            for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
            {

               MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dstpels[2] = (((MTI_UINT16)src[1]) << 8) + ((MTI_UINT16)src[0]); // B
                  dstpels[1] = (((MTI_UINT16)src[3]) << 8) + ((MTI_UINT16)src[2]); // G
                  dstpels[0] = (((MTI_UINT16)src[5]) << 8) + ((MTI_UINT16)src[4]); // R

                  dstpels += 3;
                  src += 6;
               }
            }
         }
      }

      break;

   case IF_PIXEL_COMPONENTS_Y:
   case IF_PIXEL_COMPONENTS_YYY:        // xyzzy1

      switch (frmPelPacking)
      {
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {
            phase = irow * frmWdth + srcrgn.left;
            MTI_UINT8 *src = srcimg[0] + ((phase / 3) * 4);
            phase %= 3;

            int pelcnt = rowPels;
            r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);

            if (phase == 0)
            {
               goto u0;
            }

            if (phase == 1)
            {
               goto u1;
            }

            goto u2;

            while (true)
            {
u0: ;
               dstpels[0] = dstpels[1] = dstpels[2] = r0 & 0x3ff;
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }
u1: ;
               dstpels[0] = dstpels[1] = dstpels[2] = (r0 >> 10) & 0x3ff;
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }
u2: ;
               dstpels[0] = dstpels[1] = dstpels[2] = (r0 >> 20) & 0x3ff;
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 4;
               r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
            }
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {
            phase = irow * frmWdth + srcrgn.left;
            MTI_UINT8 *src = srcimg[0] + ((phase / 3) * 4);
            phase %= 3;

            int pelcnt = rowPels;
            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

            if (phase == 0)
            {
               goto w0;
            }

            if (phase == 1)
            {
               goto w1;
            }

            goto w2;

            while (true)
            {
w0: ;
               dstpels[0] = dstpels[1] = dstpels[2] = r0 & 0x3ff;
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }
w1: ;
               dstpels[0] = dstpels[1] = dstpels[2] = (r0 >> 10) & 0x3ff;
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }
w2: ;
               dstpels[0] = dstpels[1] = dstpels[2] = (r0 >> 20) & 0x3ff;
               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 4;
               r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
            }
         }

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         lftOffs = (srcrgn.left / 4) * 5;
         phase = srcrgn.left % 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[0] + irow * frmPitch + lftOffs;

            int pelcnt = rowPels;

            r0 = *((int *)src);
            r1 = src[4];

            if (phase == 0)
            {
               goto v0;
            }

            if (phase == 1)
            {
               goto v1;
            }

            if (phase == 2)
            {
               goto v2;
            }

            goto v3;

            while (true)
            {
v0: ;
               dstpels[0] = (r0) & 0x3ff;
               dstpels[1] = dstpels[0];
               dstpels[2] = dstpels[0];

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }
v1: ;
               dstpels[0] = (r0 >> 10) & 0x3ff;
               dstpels[1] = dstpels[0];
               dstpels[2] = dstpels[0];

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }
v2: ;
               dstpels[0] = (r0 >> 20) & 0x3ff;
               dstpels[1] = dstpels[0];
               dstpels[2] = dstpels[0];

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }
v3: ;
               dstpels[0] = ((r0 >> 30) + (r1 << 2)) & 0x3ff;
               dstpels[1] = dstpels[0];
               dstpels[2] = dstpels[0];

               dstpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 5;

               r0 = *((int *)src);
               r1 = src[4];
            }
         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:

         lftOffs = (left) * 2;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; ++irow)
         {
            MTI_UINT16 *src = (MTI_UINT16 *) (srcimg[0] + (irow * frmPitch) + lftOffs);
            for (int i = 0; i < rowPels; ++i)
            {
               dstpels[0] = dstpels[1] = dstpels[2] = src[i];
               dstpels += 3;
            }
         }

         break;

      default:
         retval = -1;
         break;
      }

      break;

   default:
      retval = -1; // unsupported format
      break;

   }

   return (retval);

}

// replace subregion from a 16-bit 444 linear array beginning at srcpels.
// Modified 12/12/01 to replace only those lines for which we have fields
int CExtractor::replaceSubregion(
      MTI_UINT8 **dstimg, // -> destination field(s) (YUV422 or RGB variant)
      const CImageFormat *dstfmt, // image format of destination frame
      RECT *dstrgn, // region to be replaced
      MTI_UINT16 *srcpels // array of 16-bit "444 data" (YUV or RGB)
      )
{
   return replaceSubregion(dstimg, dstfmt, *dstrgn, srcpels);
}

int CExtractor::replaceSubregion(
      MTI_UINT8 **dstimg, // -> destination field(s) (YUV422 or RGB variant)
      const CImageFormat *dstfmt, // image format of destination frame
      const RECT &dstrgn, // region to be replaced
      MTI_UINT16 *srcpels // array of 16-bit "444 data" (YUV or RGB)
      )
{
   int retval = 0; // assume supported format

   if (dstimg[0] == nullptr || srcpels == nullptr)
   {
      return 0;
   }

   int frmFieldCount = dstfmt->getFieldCount();
   int frmWdth = dstfmt->getTotalFrameWidth();
   int frmPitch = dstfmt->getFramePitch();
   int frmPelComponents = dstfmt->getPixelComponents();
   int frmPelPacking = dstfmt->getPixelPacking();
   int rowPels = dstrgn.right - dstrgn.left + 1;
   int left = dstrgn.left;
   bool lftOdd = ((left & 1) == 1);
   int lftOffs;
   int phase;

   switch (frmPelComponents)
   {

   case IF_PIXEL_COMPONENTS_YUV422:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left >> 1) * 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount] != NULL)
            {

               int pelcnt = rowPels;

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               if (lftOdd)
               { // beg on 2nd half of pixel pair
                  dst[2] = (MTI_UINT8)srcpels[2]; // V
                  dst[3] = (MTI_UINT8)srcpels[0]; // Y
                  srcpels += 3;
                  dst += 4;
                  --pelcnt;
               }

               if (pelcnt != 0)
               {

                  for (; ;)
                  { // on 1st half of pixel-pair

                     MTI_UINT32 uAvg = (MTI_UINT32)srcpels[1];
                     MTI_UINT32 vAvg = (MTI_UINT32)srcpels[2];

                     dst[0] = (MTI_UINT8) uAvg; // U

                     dst[1] = (MTI_UINT8)srcpels[0]; // Y
                     srcpels += 3;
                     if ((--pelcnt) == 0)
                     {
                        break;
                     }

                     // the 2nd half of the pixel pair is available
                     // calculate U and V as averages over the halves

                     uAvg += (MTI_UINT32)srcpels[1];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = uAvg >> 1;
                     dst[0] = (MTI_UINT8) uAvg; // U

                     vAvg += (MTI_UINT32)srcpels[2];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = vAvg >> 1;
                     dst[2] = (MTI_UINT8) vAvg; // V

                     dst[3] = (MTI_UINT8)srcpels[0]; // Y
                     srcpels += 3;
                     if ((--pelcnt) == 0)
                     {
                        break;
                     }

                     dst += 4;

                  }

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         lftOffs = (left >> 1) * 5;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount] != NULL)
            {

               int pelcnt = rowPels;

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               if (lftOdd)
               { // beg on 2nd half of pixel pair

                  dst[2] = (dst[2] & 0xf0) + (MTI_UINT8)((srcpels[2] & 0x3c0) >> 6);
                  dst[3] = (MTI_UINT8)(((srcpels[2] & 0x3f) << 2) + ((srcpels[0] & 0x300) >> 8));
                  dst[4] = (MTI_UINT8)(srcpels[0] & 0xff);

                  srcpels += 3;
                  dst += 5;
                  --pelcnt;
               }

               if (pelcnt != 0)
               {

                  for (; ;)
                  { // on 1st half of pixel-pair

                     MTI_UINT32 uAvg = (MTI_UINT32)srcpels[1];
                     MTI_UINT32 vAvg = (MTI_UINT32)srcpels[2];

                     // drop U and Y into place

                     dst[0] = (MTI_UINT8)(uAvg >> 2);
                     dst[1] = (MTI_UINT8)(((uAvg & 0x03) << 6) + ((srcpels[0] & 0x3f0) >> 4));
                     dst[2] = (dst[2] & 0x0f) + (MTI_UINT8)((srcpels[0] & 0x0f) << 4);

                     srcpels += 3;
                     if ((--pelcnt) == 0)
                     {
                        break;
                     }

                     // the 2nd half of the pixel pair is available
                     // calculate U and V as averages over the halves

                     uAvg += (MTI_UINT32)srcpels[1];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = uAvg >> 1;

                     // revise the previous U

                     dst[0] = (MTI_UINT8)(uAvg >> 2);
                     dst[1] = (MTI_UINT8)((uAvg & 3) << 6) + (dst[1] & 0x3f);

                     vAvg += (MTI_UINT32)srcpels[2];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = vAvg >> 1;

                     // drop V and Y into place

                     dst[2] = (dst[2] & 0xf0) + (MTI_UINT8)((vAvg & 0x3c0) >> 6);
                     dst[3] = (MTI_UINT8)(((vAvg & 0x3f) << 2) + ((srcpels[0] & 0x300) >> 8));
                     dst[4] = (MTI_UINT8)(srcpels[0] & 0xff);

                     srcpels += 3;
                     if ((--pelcnt) == 0)
                     {
                        break;
                     }

                     dst += 5;

                  }

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               int pelcnt = rowPels;

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                MTI_UINT32 y, uAvg, vAvg;

               if (phase == 0)
               {
                  goto p0;
               }
               if (phase == 1)
               {
                  goto p1;
               }
               if (phase == 2)
               {
                  goto p2;
               }
               if (phase == 3)
               {
                  goto p3;
               }
               if (phase == 4)
               {
                  goto p4;
               }
               goto p5;

               while (true)
               {

p0: ;

                  uAvg = srcpels[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += srcpels[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[0] = uAvg;
                  dst[1] = (y << 2) + (uAvg >> 8);
                  dst[2] = (dst[2] & 0xf0) + (y >> 6);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

p1: ;

                  vAvg = srcpels[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += srcpels[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[2] = (vAvg << 4) + (dst[2] & 0x0f);
                  dst[3] = (vAvg >> 4);
                  dst[4] = y;
                  dst[5] = (dst[5] & 0xfc) + (y >> 8);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

p2: ;

                  uAvg = srcpels[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += srcpels[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[5] = (uAvg << 2) + (dst[5] & 0x3);
                  dst[6] = (y << 4) + (uAvg >> 6);
                  dst[7] = (y >> 4);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

p3: ;

                  vAvg = srcpels[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += srcpels[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[8] = vAvg;
                  dst[9] = (vAvg >> 8) + (y << 2);
                  dst[10] = (dst[10] & 0xf0) + (y >> 6);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

p4: ;

                  uAvg = srcpels[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += srcpels[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[10] = (uAvg << 4) + (dst[10] & 0x0f);
                  dst[11] = (uAvg >> 4);
                  dst[12] = y;
                  dst[13] = (dst[13] & 0xfc) + (y >> 8);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

p5: ;

                  vAvg = srcpels[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += srcpels[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[13] = (vAvg << 2) + (dst[13] & 0x3);
                  dst[14] = (y << 4) + (vAvg >> 6);
                  dst[15] = (y >> 4);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  dst += 16;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               int pelcnt = rowPels;

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                MTI_UINT32 y, uAvg, vAvg;

               if (phase == 0)
               {
                  goto q0;
               }
               if (phase == 1)
               {
                  goto q1;
               }
               if (phase == 2)
               {
                  goto q2;
               }
               if (phase == 3)
               {
                  goto q3;
               }
               if (phase == 4)
               {
                  goto q4;
               }
               goto q5;

               while (true)
               {

q0: ;

                  uAvg = srcpels[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += srcpels[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[0] = uAvg >> 2;
                  dst[1] = y >> 2;
                  dst[3] = ((uAvg & 0x03) << 4) + ((y & 0x03) << 2) + (dst[3] & 0x03);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q1: ;

                  vAvg = srcpels[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += srcpels[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[2] = vAvg >> 2;
                  dst[3] = (dst[3] & 0x3c) + (vAvg & 3);
                  dst[4] = y >> 2;
                  dst[7] = ((y & 0x03) << 4) + (dst[7] & 0xfc);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q2: ;

                  uAvg = srcpels[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += srcpels[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[5] = uAvg >> 2;
                  dst[6] = y >> 2;
                  dst[7] = (dst[7] & 0x30) + ((uAvg & 0x03) << 2) + (y & 0x03);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q3: ;

                  vAvg = srcpels[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += srcpels[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[8] = vAvg >> 2;
                  dst[9] = y >> 2;
                  dst[11] = ((vAvg & 0x03) << 4) + ((y & 0x03) << 2);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q4: ;

                  uAvg = srcpels[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += srcpels[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[10] = uAvg >> 2;
                  dst[11] = (dst[11] & 0x3c) + (uAvg & 0x03);
                  dst[12] = y >> 2; ;
                  dst[15] = (dst[15] & 0x3c) + (uAvg & 0x03);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q5: ;

                  vAvg = srcpels[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += srcpels[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = srcpels[0];

                  dst[13] = vAvg >> 2;
                  dst[14] = y >> 2;
                  dst[15] = (dst[15] & 0x30) + ((vAvg & 0x03) << 2) + (y & 0x03);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  dst += 16;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L: // CLIPSTER

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               int pelcnt = rowPels;

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                MTI_UINT32 y, uAvg, vAvg;

               if (phase == 0)
               {
                  goto r0;
               }
               if (phase == 1)
               {
                  goto r1;
               }
               if (phase == 2)
               {
                  goto r2;
               }
               if (phase == 3)
               {
                  goto r3;
               }
               if (phase == 4)
               {
                  goto r4;
               }
               goto r5;

               while (true)
               {

r0: ;

                  uAvg = srcpels[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg = (uAvg + srcpels[4]) >> 1;
                  }
#endif
                  y = srcpels[0];

                  dst[0] = uAvg >> 2;
                  dst[1] = (uAvg << 6) + (y >> 4);
                  dst[2] = (y << 4) + (dst[2] & 0x0f);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r1: ;

                  vAvg = srcpels[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg = (vAvg + srcpels[-1]) >> 1;
                  }
#endif
                  y = srcpels[0];

                  dst[2] = (dst[2] & 0xf0) + (vAvg >> 6);
                  dst[3] = (vAvg << 2);
                  dst[4] = (y >> 2);
                  dst[5] = (y << 6) + (dst[5] & 0x3f);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r2: ;

                  uAvg = srcpels[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg = (uAvg + srcpels[4]) >> 1;
                  }
#endif
                  y = srcpels[0];

                  dst[5] = (dst[5] & 0xc0) + (uAvg >> 4);
                  dst[6] = (uAvg << 4) + (y >> 6);
                  dst[7] = (y << 2);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r3: ;

                  vAvg = srcpels[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg = (vAvg + srcpels[-1]) >> 1;
                  }
#endif
                  y = srcpels[0];

                  dst[8] = (vAvg >> 2);
                  dst[9] = (vAvg << 6) + (y >> 4);
                  dst[10] = (y << 4) + (dst[10] & 0x0f);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r4: ;

                  uAvg = srcpels[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg = (uAvg + srcpels[4]) >> 1;
                  }
#endif
                  y = srcpels[0];

                  dst[10] = (dst[10] & 0xf0) + (uAvg >> 6);
                  dst[11] = (uAvg << 2);
                  dst[12] = (y >> 2);
                  dst[13] = (y << 6) + (dst[13] & 0x3f);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r5: ;

                  vAvg = srcpels[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg = (vAvg + srcpels[-1]) >> 1;
                  }
#endif
                  y = srcpels[0];

                  dst[13] = (dst[13] & 0xc0) + (vAvg >> 4);
                  dst[14] = (vAvg << 4) + (y >> 6);
                  dst[15] = (y << 2);

                  srcpels += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  dst += 16;
               }

            }

         }

         break;

      default:
         retval = -1;
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_YUV444:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)srcpels[1];
                  dst[1] = (MTI_UINT8)srcpels[0];
                  dst[2] = (MTI_UINT8)srcpels[2];

                  dst += 3;
                  srcpels += 3;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         lftOffs = (left) * 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount] != NULL)
            {

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)(srcpels[1] & 0xff);
                  dst[1] = (MTI_UINT8)((srcpels[1] & 0x300) >> 8) + (MTI_UINT8)((srcpels[0] & 0x3f) << 2);
                  dst[2] = (MTI_UINT8)((srcpels[0] & 0x3c0) >> 6) + (MTI_UINT8)((srcpels[2] & 0x0f) << 4);
                  dst[3] = (MTI_UINT8)((srcpels[2] & 0x3f0) >> 4);

                  dst += 4;
                  srcpels += 3;

               }

            }

         }

         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_RGB:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)srcpels[0];
                  dst[1] = (MTI_UINT8)srcpels[1];
                  dst[2] = (MTI_UINT8)srcpels[2];

                  dst += 3;
                  srcpels += 3;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         lftOffs = (left) * 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount] != NULL)
            {

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)((srcpels[0] & 0x3fc) >> 2);
                  dst[1] = (MTI_UINT8)((srcpels[0] & 0x03) << 6) + (MTI_UINT8)((srcpels[1] & 0x3f0) >> 4);
                  dst[2] = (MTI_UINT8)((srcpels[1] & 0x0f) << 4) + (MTI_UINT8)((srcpels[2] & 0x3c0) >> 6);
                  dst[3] = (MTI_UINT8)((srcpels[2] & 0x3f) << 2);

                  dst += 4;
                  srcpels += 3;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         lftOffs = (left) * 4;
         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {
            if (dstimg[irow % frmFieldCount] != NULL)
            {
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
               for (int i = 0; i < rowPels; i++)
               {
                  dst[0] = (MTI_UINT8)((srcpels[0] & 0x3f0) >> 4);
                  dst[1] = (MTI_UINT8)((srcpels[0] & 0x0f) << 4) + (MTI_UINT8)((srcpels[1] & 0x3c0) >> 6);
                  dst[2] = (MTI_UINT8)((srcpels[1] & 0x3f) << 2) + (MTI_UINT8)((srcpels[2] & 0x300) >> 8);
                  dst[3] = (MTI_UINT8)((srcpels[2] & 0xff));

                  dst += 4;
                  srcpels += 3;
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:

         lftOffs = (left >> 3) * 36;
         phase = left % 8;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            int pelcnt = rowPels;

#ifndef DPX12_PACKED
            MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
#else
            int pel = irow * frmWdth + left;
            MTI_UINT8 *dst = dstimg[0] + (pel >> 3) * 36;
            phase = pel % 8;
#endif
            if (phase == 0)
            {
               goto s0;
            }
            if (phase == 1)
            {
               goto s1;
            }
            if (phase == 2)
            {
               goto s2;
            }
            if (phase == 3)
            {
               goto s3;
            }
            if (phase == 4)
            {
               goto s4;
            }
            if (phase == 5)
            {
               goto s5;
            }
            if (phase == 6)
            {
               goto s6;
            }
            goto s7;

            while (true)
            {

s0:

               dst[0] = (MTI_UINT8)srcpels[2];
               dst[1] = (MTI_UINT8)(srcpels[1] >> 4);
               dst[2] = (MTI_UINT8)((srcpels[1] << 4) + (srcpels[0] >> 8));
               dst[3] = (MTI_UINT8)srcpels[0];
               dst[7] = (MTI_UINT8)((dst[7] & 0xf0) + (srcpels[2] >> 8));
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s1:

               dst[4] = (MTI_UINT8)((srcpels[2] << 4) + (srcpels[1] >> 8));
               dst[5] = (MTI_UINT8)srcpels[1];
               dst[6] = (MTI_UINT8)(srcpels[0] >> 4);
               dst[7] = (MTI_UINT8)((srcpels[0] << 4) + (dst[7] & 0x0f));
               dst[11] = (MTI_UINT8)(srcpels[2] >> 4);
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s2:

               dst[8] = (MTI_UINT8)(srcpels[1] >> 4);
               dst[9] = (MTI_UINT8)((srcpels[1] << 4) + (srcpels[0] >> 8));
               dst[10] = (MTI_UINT8)srcpels[0];
               dst[14] = (MTI_UINT8)((dst[14] & 0xf0) + (srcpels[2] >> 8));
               dst[15] = (MTI_UINT8)srcpels[2];
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s3:

               dst[12] = (MTI_UINT8)srcpels[1];
               dst[13] = (MTI_UINT8)(srcpels[0] >> 4);
               dst[14] = (MTI_UINT8)((srcpels[0] << 4) + (dst[14] & 0x0f));
               dst[18] = (MTI_UINT8)(srcpels[2] >> 4);
               dst[19] = (MTI_UINT8)((srcpels[2] << 4) + (srcpels[1] >> 8));
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s4:

               dst[16] = (MTI_UINT8)((srcpels[1] << 4) + (srcpels[0] >> 8));
               dst[17] = (MTI_UINT8)srcpels[0];
               dst[21] = (MTI_UINT8)((dst[21] & 0xf0) + (srcpels[2] >> 8));
               dst[22] = (MTI_UINT8)srcpels[2];
               dst[23] = (MTI_UINT8)(srcpels[1] >> 4);
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s5:

               dst[20] = (MTI_UINT8)(srcpels[0] >> 4);
               dst[21] = (MTI_UINT8)((srcpels[0] << 4) + (dst[21] & 0x0f));
               dst[25] = (MTI_UINT8)(srcpels[2] >> 4);
               dst[26] = (MTI_UINT8)((srcpels[2] << 4) + (srcpels[1] >> 8));
               dst[27] = (MTI_UINT8)srcpels[1];
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s6:

               dst[24] = (MTI_UINT8)srcpels[0];
               dst[28] = (MTI_UINT8)((dst[28] & 0xf0) + (srcpels[2] >> 8));
               dst[29] = (MTI_UINT8)srcpels[2];
               dst[30] = (MTI_UINT8)(srcpels[1] >> 4);
               dst[31] = (MTI_UINT8)((srcpels[1] << 4) + (srcpels[0] >> 8));
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s7:

               dst[28] = (MTI_UINT8)((srcpels[0] << 4) + (dst[28] & 0x0f));
               dst[32] = (MTI_UINT8)(srcpels[2] >> 4);
               dst[33] = (MTI_UINT8)((srcpels[2] << 4) + (srcpels[1] >> 8));
               dst[34] = (MTI_UINT8)srcpels[1];
               dst[35] = (MTI_UINT8)(srcpels[0] >> 4);
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               dst += 36;
            }
         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:

         lftOffs = (left >> 3) * 36;
         phase = left % 8;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            int pelcnt = rowPels;

#ifndef DPX12_PACKED
            MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
#else
            int pel = irow * frmWdth + left;
            MTI_UINT8 *dst = dstimg[0] + (pel >> 3) * 36;
            phase = pel % 8;
#endif
            if (phase == 0)
            {
               goto t0;
            }
            if (phase == 1)
            {
               goto t1;
            }
            if (phase == 2)
            {
               goto t2;
            }
            if (phase == 3)
            {
               goto t3;
            }
            if (phase == 4)
            {
               goto t4;
            }
            if (phase == 5)
            {
               goto t5;
            }
            if (phase == 6)
            {
               goto t6;
            }
            goto t7;

            while (true)
            {

t0:

               dst[0] = (MTI_UINT8)(srcpels[0] >> 4);
               dst[1] = (MTI_UINT8)((srcpels[0] << 4) + (srcpels[1] >> 8));
               dst[2] = (MTI_UINT8)srcpels[1];
               dst[3] = (MTI_UINT8)(srcpels[2] >> 4);
               dst[4] = (MTI_UINT8)((srcpels[2] << 4) + (dst[4] & 0x0f));
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t1:

               dst[4] = (MTI_UINT8)((dst[4] & 0xf0) + (srcpels[0] >> 8));
               dst[5] = (MTI_UINT8)srcpels[0];
               dst[6] = (MTI_UINT8)(srcpels[1] >> 4);
               dst[7] = (MTI_UINT8)((srcpels[1] << 4) + (srcpels[2] >> 8));
               dst[8] = (MTI_UINT8)srcpels[2];
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t2:

               dst[9] = (MTI_UINT8)(srcpels[0] >> 4);
               dst[10] = (MTI_UINT8)((srcpels[0] << 4) + (srcpels[1] >> 8));
               dst[11] = (MTI_UINT8)srcpels[1];
               dst[12] = (MTI_UINT8)(srcpels[2] >> 4);
               dst[13] = (MTI_UINT8)((srcpels[2] << 4) + (dst[13] & 0x0f));
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t3:

               dst[13] = (MTI_UINT8)((dst[13] & 0xf0) + (srcpels[0] >> 8));
               dst[14] = (MTI_UINT8)srcpels[0];
               dst[15] = (MTI_UINT8)(srcpels[1] >> 4);
               dst[16] = (MTI_UINT8)((srcpels[1] << 4) + (srcpels[2] >> 8));
               dst[17] = (MTI_UINT8)srcpels[2];
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t4:

               dst[18] = (MTI_UINT8)(srcpels[0] >> 4);
               dst[19] = (MTI_UINT8)((srcpels[0] << 4) + (srcpels[1] >> 8));
               dst[20] = (MTI_UINT8)srcpels[1];
               dst[21] = (MTI_UINT8)(srcpels[2] >> 4);
               dst[22] = (MTI_UINT8)((srcpels[2] << 4) + (dst[22] & 0x0f));
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t5:

               dst[22] = (MTI_UINT8)((dst[22] & 0xf0) + (srcpels[0] >> 8));
               dst[23] = (MTI_UINT8)srcpels[0];
               dst[24] = (MTI_UINT8)(srcpels[1] >> 4);
               dst[25] = (MTI_UINT8)((srcpels[1] << 4) + (srcpels[2] >> 8));
               dst[26] = (MTI_UINT8)srcpels[2];
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t6:

               dst[27] = (MTI_UINT8)(srcpels[0] >> 4);
               dst[28] = (MTI_UINT8)((srcpels[0] << 4) + (srcpels[1] >> 8));
               dst[29] = (MTI_UINT8)srcpels[1];
               dst[30] = (MTI_UINT8)(srcpels[2] >> 4);
               dst[31] = (MTI_UINT8)((srcpels[2] << 4) + (dst[31] & 0x0f));
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t7:

               dst[31] = (MTI_UINT8)((dst[31] & 0xf0) + (srcpels[0] >> 8));
               dst[32] = (MTI_UINT8)srcpels[0];
               dst[33] = (MTI_UINT8)(srcpels[1] >> 4);
               dst[34] = (MTI_UINT8)((srcpels[1] << 4) + (srcpels[2] >> 8));
               dst[35] = (MTI_UINT8)srcpels[2];
               srcpels += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               dst += 36;
            }
         }

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:

         lftOffs = (left) * 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)(srcpels[0] << 4);
                  dst[1] = (MTI_UINT8)(srcpels[0] >> 4);
                  dst[2] = (MTI_UINT8)(srcpels[1] << 4); ;
                  dst[3] = (MTI_UINT8)(srcpels[1] >> 4);
                  dst[4] = (MTI_UINT8)(srcpels[2] << 4);
                  dst[5] = (MTI_UINT8)(srcpels[2] >> 4);

                  dst += 6;
                  srcpels += 3;

               }
            }
         }

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:

         lftOffs = (left) * 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)(srcpels[0] >> 4);
                  dst[1] = (MTI_UINT8)(srcpels[0] << 4);
                  dst[2] = (MTI_UINT8)(srcpels[1] >> 4);
                  dst[3] = (MTI_UINT8)(srcpels[1] << 4);
                  dst[4] = (MTI_UINT8)(srcpels[2] >> 4);
                  dst[5] = (MTI_UINT8)(srcpels[2] << 4);

                  dst += 6;
                  srcpels += 3;

               }
            }
         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         if (frmFieldCount == 1 && left == 0)
         {
            MTI_UINT8 *dst = dstimg[0];
            MTI_UINT8 *src = (MTI_UINT8*)srcpels;
            size_t nBytes = ((dstrgn.bottom - dstrgn.top) + 1) * frmPitch;
            MTImemcpy(dst, src, nBytes);
         }
         else
         {
            lftOffs = (left) * 6;

            for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
            {

               if (dstimg[irow % frmFieldCount])
               {

                  MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                  for (int i = 0; i < rowPels; i++)
                  {

                     dst[0] = (MTI_UINT8)(srcpels[0]);
                     dst[1] = (MTI_UINT8)(srcpels[0] >> 8);
                     dst[2] = (MTI_UINT8)(srcpels[1]);
                     dst[3] = (MTI_UINT8)(srcpels[1] >> 8);
                     dst[4] = (MTI_UINT8)(srcpels[2]);
                     dst[5] = (MTI_UINT8)(srcpels[2] >> 8);

                     dst += 6;
                     srcpels += 3;

                  }
               }
            }
         }

         break;

      default:
         retval = -1; // unsupported format
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_BGR:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)srcpels[2]; // B
                  dst[1] = (MTI_UINT8)srcpels[1]; // G
                  dst[2] = (MTI_UINT8)srcpels[0]; // R

                  dst += 3;
                  srcpels += 3;

               }
            }
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         lftOffs = (left) * 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount] != NULL)
            {

               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)((srcpels[2] & 0x3fc) >> 2);
                  dst[1] = (MTI_UINT8)((srcpels[2] & 0x03) << 6) + (MTI_UINT8)((srcpels[1] & 0x3f0) >> 4);
                  dst[2] = (MTI_UINT8)((srcpels[1] & 0x0f) << 4) + (MTI_UINT8)((srcpels[0] & 0x3c0) >> 6);
                  dst[3] = (MTI_UINT8)((srcpels[0] & 0x3f) << 2);

                  dst += 4;
                  srcpels += 3;
               }
            }
         }
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:

         if (frmFieldCount == 1 && left == 0)
         {
            MTI_UINT8 *dst = dstimg[0];
            MTI_UINT8 *src = (MTI_UINT8*)srcpels;
            size_t nBytes = ((dstrgn.bottom - dstrgn.top) + 1) * frmPitch;
            MTImemcpy(dst, src, nBytes);
         }
         else
         {
            lftOffs = (left) * 6;

            for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
            {

               if (dstimg[irow % frmFieldCount])
               {

                  MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                  for (int i = 0; i < rowPels; i++)
                  {

                     dst[0] = (MTI_UINT8)(srcpels[2]);
                     dst[1] = (MTI_UINT8)(srcpels[2] >> 8);
                     dst[2] = (MTI_UINT8)(srcpels[1]);
                     dst[3] = (MTI_UINT8)(srcpels[1] >> 8);
                     dst[4] = (MTI_UINT8)(srcpels[0]);
                     dst[5] = (MTI_UINT8)(srcpels[0] >> 8);

                     dst += 6;
                     srcpels += 3;

                  }
               }
            }
         }

         break;
      }

      break;

   case IF_PIXEL_COMPONENTS_Y: // xyzzy1
   case IF_PIXEL_COMPONENTS_YYY: // xyzzy1

      // SOURCE is YYY
      // DETINATION is Y
      unsigned int r0;
      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         // DESTINATION is big-endian
         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {
            phase = irow * frmWdth + dstrgn.left;
            MTI_UINT8 *dst = dstimg[0] + ((phase / 3) * 4);
            phase %= 3;

            int pelcnt = rowPels;

            if (phase == 0)
            {
               goto u0;
            }

            if (phase == 1)
            {
               goto u1;
            }

            goto u2;

            while (true)
            {
u0: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[2] = (dst[2] & 0xfc) + (r0 >> 6); // PPPP | 9876
               dst[3] = (r0 << 2);                   // 543210 | XX

               if (--pelcnt == 0)
               {
                  break;
               }
u1: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[1] = (dst[1] & 0xC0) + (r0 >> 4); // PP | 987654
               dst[2] = (r0 << 4) + (dst[2] & 0x0F); // 3210 | PPPP

               if (--pelcnt == 0)
               {
                  break;
               }
u2: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[0] = (r0 >> 2);                   // 98765432
               dst[1] = (r0 << 6) + (dst[1] & 0x3F); // 10 | PPPPPP

               if (--pelcnt == 0)
               {
                  break;
               }

               dst += 4;
            }
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         // DESTINATION is big-endian
         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {
            phase = irow * frmWdth + dstrgn.left;
            MTI_UINT8 *dst = dstimg[0] + ((phase / 3) * 4);
            phase %= 3;

            int pelcnt = rowPels;

            if (phase == 0)
            {
               goto w0;
            }

            if (phase == 1)
            {
               goto w1;
            }

            goto w2;

            while (true)
            {
w0: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[2] = (dst[2] & 0xfc) + (r0 >> 8);
               dst[3] = (r0 & 0xff);

               if (--pelcnt == 0)
               {
                  break;
               }
w1: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[1] = (dst[1] & 0xf0) + (r0 >> 6);
               dst[2] = ((r0 << 2) & 0xfc) + (dst[2] & 0x03);

               if (--pelcnt == 0)
               {
                  break;
               }
w2: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[0] = (r0 >> 4);
               dst[1] = ((r0 << 4) & 0xf0) + (dst[1] & 0x0f);

               if (--pelcnt == 0)
               {
                  break;
               }

               dst += 4;
            }
         }

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         lftOffs = (dstrgn.left / 4) * 5;
         phase = dstrgn.left % 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {
            MTI_UINT8 *dst = dstimg[0] + irow * frmPitch + lftOffs;
            int pelcnt = rowPels;

            if (phase == 0)
            {
               goto v0;
            }

            if (phase == 1)
            {
               goto v1;
            }

            if (phase == 2)
            {
               goto v2;
            }

            goto v3;

            while (true)
            {
v0: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[0] = (r0);
               dst[1] = ((r0 >> 8) & 0x03) + (dst[1] & 0xfc);

               if (--pelcnt == 0)
               {
                  break;
               }
v1: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[1] = (dst[1] & 0x03) + (r0 << 2);
               dst[2] = ((r0 >> 6) & 0x0f) + (dst[2] & 0xf0);

               if (--pelcnt == 0)
               {
                  break;
               }
v2: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[2] = (dst[2] & 0x0f) + (r0 << 4);
               dst[3] = ((r0 >> 4) & 0x3f) + (dst[3] & 0xc0);

               if (--pelcnt == 0)
               {
                  break;
               }
v3: ;
               r0 = *srcpels;
               srcpels += 3;
               dst[3] = (dst[3] & 0x3f) + (r0 << 6);
               dst[4] = (r0 >> 2);

               if (--pelcnt == 0)
               {
                  break;
               }

               dst += 5;
            }
         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:

         lftOffs = left * 2;
         for (int irow = dstrgn.top; irow <= dstrgn.bottom; ++irow)
         {
            if (dstimg[irow % frmFieldCount])
            {
               MTI_UINT16 *dstpels = (MTI_UINT16 *) (dstimg[0] + (irow * frmPitch) + lftOffs);
               for (int i = 0; i < rowPels; i += 3)
               {
                  *dstpels++ = srcpels[i];
               }
            }
         }

         break;

      default:
         retval = -1; // unsupported format
         break;
      }

      break;

   default:
      retval = -1; // unsupported format
      break;
   }

   return (retval);
}

// extract a subregion from the source as a BitBlt to a 16-bit 444 progressive
// frame having the same dimensions as the source
int CExtractor::extractSubregionWithinFrame(MTI_UINT16 *dstpels, // array of 16-bit "444 data" (YUV or RGB)
      MTI_UINT8 **srcimg, // -> source field(s) (YUV422 or RGB variant)
      const CImageFormat *srcfmt, // image format of source frame
      RECT *srcrgn // region to be extracted
      )
{
   return extractSubregionWithinFrame(dstpels, srcimg, srcfmt, *srcrgn);
}

int CExtractor::extractSubregionWithinFrame(MTI_UINT16 *dstpels, // array of 16-bit "444 data" (YUV or RGB)
      MTI_UINT8 **srcimg, // -> source field(s) (YUV422 or RGB variant)
      const CImageFormat *srcfmt, // image format of source frame
      const RECT &srcrgn // region to be extracted
      )
{
   int retval = 0; // assume supported format

   int frmFieldCount = srcfmt->getFieldCount();
   int frmWdth = srcfmt->getPixelsPerLine();
   int frmPitch = srcfmt->getFramePitch();
   int frmPelComponents = srcfmt->getPixelComponents();
   int frmPelPacking = srcfmt->getPixelPacking();
   int rowPels = srcrgn.right - srcrgn.left + 1;
   int left = srcrgn.left;
   bool lftOdd = ((left & 1) == 1);
   int lftOffs;
   int phase;

   unsigned int r0, r1, r2, r3;

   switch (frmPelComponents)
   {

   case IF_PIXEL_COMPONENTS_YUV422:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left >> 1) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            int pelcnt = rowPels;

            if (lftOdd)
            { // beg on 2nd half of pixel pair
               dst[0] = ((MTI_UINT16)src[3]); // Y
               dst[1] = ((MTI_UINT16)src[0]); // U
               dst[2] = ((MTI_UINT16)src[2]); // V
               dst += 3;
               src += 4;
               --pelcnt;
            }

            if (pelcnt != 0)
            {

               for (; ;)
               { // on 1st half of pixel pair

                  dst[0] = ((MTI_UINT16)src[1]); // Y
                  dst[1] = ((MTI_UINT16)src[0]); // U
                  dst[2] = ((MTI_UINT16)src[2]); // V

                  dst += 3;
                  if ((--pelcnt) == 0)
                  {
                     break;
                  }

                  dst[0] = ((MTI_UINT16)src[3]); // Y
                  dst[1] = ((MTI_UINT16)src[0]); // U
                  dst[2] = ((MTI_UINT16)src[2]); // V

                  dst += 3;
                  if ((--pelcnt) == 0)
                  {
                     break;
                  }

                  src += 4;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         lftOffs = (left >> 1) * 5;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            int pelcnt = rowPels;

            if (lftOdd)
            { // beg on 2nd half of pixel pair
               dst[0] = ((((MTI_UINT16)src[3]) & 0x03) << 8) + ((MTI_UINT16)src[4]); // Y
               dst[1] = (((MTI_UINT16)src[0]) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // U
               dst[2] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + (((MTI_UINT16)src[3]) >> 2); // V

               dst += 3;
               src += 5;
               --pelcnt;
            }

            if (pelcnt != 0)
            {

               for (; ;)
               { // on 1st half of pixel pair

                  dst[0] = ((((MTI_UINT16)src[1]) & 0x3f) << 4) + ((((MTI_UINT16)src[2]) & 0xf0) >> 4); // Y
                  dst[1] = (((MTI_UINT16)src[0]) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // U
                  dst[2] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + (((MTI_UINT16)src[3]) >> 2); // V

                  dst += 3;
                  if ((--pelcnt) == 0)
                  {
                     break;
                  }

                  dst[0] = ((((MTI_UINT16)src[3]) & 0x03) << 8) + ((MTI_UINT16)src[4]); // Y
                  dst[1] = (((MTI_UINT16)src[0]) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // U
                  dst[2] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + (((MTI_UINT16)src[3]) >> 2); // V

                  dst += 3;
                  if ((--pelcnt) == 0)
                  {
                     break;
                  }

                  src += 5;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            int pelcnt = rowPels;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
            r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
            r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
            r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
            r0 = *((int *)(src));
            r1 = *((int *)(src + 4));
            r2 = *((int *)(src + 8));
            r3 = *((int *)(src + 12));
#endif
            if (phase == 0)
            {
               goto p0;
            }
            if (phase == 1)
            {
               goto p1;
            }
            if (phase == 2)
            {
               goto p2;
            }
            if (phase == 3)
            {
               goto p3;
            }
            if (phase == 4)
            {
               goto p4;
            }
            goto p5;

            while (true)
            {

p0: ;

               dst[0] = (r0 >> 10) & 0x3ff; // Y
               dst[1] = (r0) & 0x3ff; // U
               dst[2] = (r0 >> 20) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p1: ;

               dst[0] = (r1) & 0x3ff; // Y
               dst[1] = (r0) & 0x3ff; // U
               dst[2] = (r0 >> 20) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p2: ;

               dst[0] = (r1 >> 20) & 0x3ff; // Y
               dst[1] = (r1 >> 10) & 0x3ff; // U
               dst[2] = (r2) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p3: ;

               dst[0] = (r2 >> 10) & 0x3ff; // Y
               dst[1] = (r1 >> 10) & 0x3ff; // U
               dst[2] = (r2) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p4: ;

               dst[0] = (r3) & 0x3ff; // Y
               dst[1] = (r2 >> 20) & 0x3ff; // U
               dst[2] = (r3 >> 10) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

p5: ;

               dst[0] = (r3 >> 20) & 0x3ff; // Y
               dst[1] = (r2 >> 20) & 0x3ff; // U
               dst[2] = (r3 >> 10) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 16;

#if ENDIAN == MTI_BIG_ENDIAN
               r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
               r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
               r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
               r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
               r0 = *((int *)(src));
               r1 = *((int *)(src + 4));
               r2 = *((int *)(src + 8));
               r3 = *((int *)(src + 12));
#endif
            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            int pelcnt = rowPels;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
            r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
            r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
            r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
            r0 = *((int *)(src));
            r1 = *((int *)(src + 4));
            r2 = *((int *)(src + 8));
            r3 = *((int *)(src + 12));
#endif
            if (phase == 0)
            {
               goto q0;
            }
            if (phase == 1)
            {
               goto q1;
            }
            if (phase == 2)
            {
               goto q2;
            }
            if (phase == 3)
            {
               goto q3;
            }
            if (phase == 4)
            {
               goto q4;
            }
            goto q5;

            while (true)
            {

q0: ;

               dst[0] = ((r0 >> 6) & 0x3fc) + ((r0 >> 26) & 0x03); // Y
               dst[1] = ((r0 << 2) & 0x3fc) + ((r0 >> 28) & 0x03); // U
               dst[2] = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03); // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q1: ;

               dst[0] = ((r1 << 2) & 0x3fc) + ((r1 >> 28) & 0x03); // Y
               dst[1] = ((r0 << 2) & 0x3fc) + ((r0 >> 28) & 0x03); // U
               dst[2] = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03); // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q2: ;

               dst[0] = ((r1 >> 14) & 0x3fc) + ((r1 >> 24) & 0x03); // Y
               dst[1] = ((r1 >> 6) & 0x3fc) + ((r1 >> 26) & 0x03); // U
               dst[2] = ((r2 << 2) & 0x3fc) + ((r2 >> 28) & 0x03); // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q3: ;

               dst[0] = ((r2 >> 6) & 0x3fc) + ((r2 >> 26) & 0x03); // Y
               dst[1] = ((r1 >> 6) & 0x3fc) + ((r1 >> 26) & 0x03); // U
               dst[2] = ((r2 << 2) & 0x3fc) + ((r2 >> 28) & 0x03); // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q4: ;

               dst[0] = ((r3 << 2) & 0x3fc) + ((r3 >> 28) & 0x03); // Y
               dst[1] = ((r2 >> 14) & 0x3fc) + ((r2 >> 24) & 0x03); // U
               dst[2] = ((r3 >> 6) & 0x3fc) + ((r3 >> 26) & 0x03); // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

q5: ;

               dst[0] = ((r3 >> 14) & 0x3fc) + ((r3 >> 24) & 0x03); // Y
               dst[1] = ((r2 >> 14) & 0x3fc) + ((r2 >> 24) & 0x03); // U
               dst[2] = ((r3 >> 6) & 0x3fc) + ((r3 >> 26) & 0x03); // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 16;

#if ENDIAN == MTI_BIG_ENDIAN
               r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + (src[0]);
               r1 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8) + (src[4]);
               r2 = (src[11] << 24) + (src[10] << 16) + (src[9] << 8) + (src[8]);
               r3 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8) + (src[12]);
#else
               r0 = *((int *)(src));
               r1 = *((int *)(src + 4));
               r2 = *((int *)(src + 8));
               r3 = *((int *)(src + 12));
#endif
            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L: // CLIPSTER

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            int pelcnt = rowPels;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = *((int *)(src));
            r1 = *((int *)(src + 4));
            r2 = *((int *)(src + 8));
            r3 = *((int *)(src + 12));
#else
#if MTI_ASM_X86_INTEL
            _asm
            { // swap endian
               mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax, [ebx + 8]bswap eax mov r2,
               eax mov eax, [ebx + 12]bswap eax mov r3, eax
            }
#else
            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
            r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
            r2 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
            r3 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
#endif
#endif
            if (phase == 0)
            {
               goto r0;
            }
            if (phase == 1)
            {
               goto r1;
            }
            if (phase == 2)
            {
               goto r2;
            }
            if (phase == 3)
            {
               goto r3;
            }
            if (phase == 4)
            {
               goto r4;
            }
            goto r5;

            while (true)
            {

r0: ;

               dst[0] = (r0 >> 12) & 0x3ff; // Y
               dst[1] = (r0 >> 22) & 0x3ff; // U
               dst[2] = (r0 >> 2) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r1: ;

               dst[0] = (r1 >> 22) & 0x3ff; // Y
               dst[1] = (r0 >> 22) & 0x3ff; // U
               dst[2] = (r0 >> 2) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r2: ;

               dst[0] = (r1 >> 2) & 0x3ff; // Y
               dst[1] = (r1 >> 12) & 0x3ff; // U
               dst[2] = (r2 >> 22) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r3: ;

               dst[0] = (r2 >> 12) & 0x3ff; // Y
               dst[1] = (r1 >> 12) & 0x3ff; // U
               dst[2] = (r2 >> 22) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r4: ;

               dst[0] = (r3 >> 22) & 0x3ff; // Y
               dst[1] = (r2 >> 2) & 0x3ff; // U
               dst[2] = (r3 >> 12) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

r5: ;

               dst[0] = (r3 >> 2) & 0x3ff; // Y
               dst[1] = (r2 >> 2) & 0x3ff; // U
               dst[2] = (r3 >> 12) & 0x3ff; // V

               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 16;

#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((int *)(src));
               r1 = *((int *)(src + 4));
               r2 = *((int *)(src + 8));
               r3 = *((int *)(src + 12));
#else
#if MTI_ASM_X86_INTEL
               _asm
               { // swap endian
                  mov ebx, src mov eax, [ebx]bswap eax mov r0, eax mov eax, [ebx + 4]bswap eax mov r1, eax mov eax,
                  [ebx + 8]bswap eax mov r2, eax mov eax, [ebx + 12]bswap eax mov r3, eax
               }
#else
               r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
               r1 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
               r0 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
               r0 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];
#endif
#endif
            }

         }

         break;

      default:
         retval = -1;
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_YUV444:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            for (int i = 0; i < rowPels; i++)
            {

               dst[0] = (MTI_UINT16)src[1]; // Y
               dst[1] = (MTI_UINT16)src[0]; // U
               dst[2] = (MTI_UINT16)src[2]; // V

               dst += 3;

               src += 3;

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         lftOffs = (left) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            for (int i = 0; i < rowPels; i++)
            {

               dst[0] = ((((MTI_UINT16)src[1]) & 0xfc) >> 2) + ((((MTI_UINT16)src[2]) & 0x0f) << 6); // Y
               dst[1] = ((((MTI_UINT16)src[0]))) + ((((MTI_UINT16)src[1]) & 0x3) << 8); // U
               dst[2] = ((((MTI_UINT16)src[2]) & 0xf0) >> 4) + ((((MTI_UINT16)src[3]) & 0x3f) << 4); // V

               dst += 3;

               src += 4;

            }

         }

         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_RGB:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            for (int i = 0; i < rowPels; i++)
            {

               dst[0] = (MTI_UINT16)src[0]; // R
               dst[1] = (MTI_UINT16)src[1]; // G
               dst[2] = (MTI_UINT16)src[2]; // B

               dst += 3;

               src += 3;

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         lftOffs = (left) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            for (int i = 0; i < rowPels; i++)
            {

               dst[0] = ((((MTI_UINT16)src[0])) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // R
               dst[1] = ((((MTI_UINT16)src[1]) & 0x3f) << 4) + ((((MTI_UINT16)src[2]) & 0xf0) >> 4); // G
               dst[2] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + ((((MTI_UINT16)src[3]) & 0xfc) >> 2); // B

               dst += 3;
               src += 4;

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         lftOffs = (left) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            for (int i = 0; i < rowPels; i++)
            {

               dst[0] = ((((MTI_UINT16)src[0]) & 0x3f) << 4) + ((((MTI_UINT16)src[1]) & 0xf0) >> 4); // R
               dst[1] = ((((MTI_UINT16)src[1]) & 0x0f) << 6) + ((((MTI_UINT16)src[2]) & 0xfc) >> 2); // G
               dst[2] = ((((MTI_UINT16)src[2]) & 0x03) << 8) + ((((MTI_UINT16)src[3]))); // B

               dst += 3;
               src += 4;

            }

         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:

         lftOffs = (left >> 3) * 36;
         phase = left % 8;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            int pelcnt = rowPels;

#ifndef DPX12_PACKED
            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
#else
            int pel = irow * frmWdth + left;
            MTI_UINT8 *src = srcimg[0] + (pel >> 3) * 36;
            phase = pel % 8;
#endif
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            if (phase == 0)
            {
               goto s0;
            }
            if (phase == 1)
            {
               goto s1;
            }
            if (phase == 2)
            {
               goto s2;
            }
            if (phase == 3)
            {
               goto s3;
            }
            if (phase == 4)
            {
               goto s4;
            }
            if (phase == 5)
            {
               goto s5;
            }
            if (phase == 6)
            {
               goto s6;
            }
            goto s7;

            while (true)
            {

s0:

               dst[0] = ((src[2] & 0x0f) << 8) + src[3];
               dst[1] = (src[1] << 4) + ((src[2] & 0xf0) >> 4);
               dst[2] = ((src[7] & 0x0f) << 8) + src[0];
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s1:

               dst[0] = (src[6] << 4) + ((src[7] & 0xf0) >> 4);
               dst[1] = ((src[4] & 0x0f) << 8) + src[5];
               dst[2] = (src[11] << 4) + ((src[4] & 0xf0) >> 4);
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s2:
               dst[0] = ((src[9] & 0x0f) << 8) + src[10];
               dst[1] = (src[8] << 4) + ((src[9] & 0xf0) >> 4);
               dst[2] = ((src[14] & 0x0f) << 8) + src[15];
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s3:

               dst[0] = (src[13] << 4) + ((src[14] & 0xf0) >> 4);
               dst[1] = ((src[19] & 0x0f) << 8) + src[12];
               dst[2] = (src[18] << 4) + ((src[19] & 0xf0) >> 4);
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s4:

               dst[0] = ((src[16] & 0x0f) << 8) + src[17];
               dst[1] = (src[23] << 4) + ((src[16] & 0xf0) >> 4);
               dst[2] = ((src[21] & 0x0f) << 8) + src[22];
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s5:

               dst[0] = (src[20] << 4) + ((src[21] & 0xf0) >> 4);
               dst[1] = ((src[26] & 0x0f) << 8) + src[27];
               dst[2] = (src[25] << 4) + ((src[26] & 0xf0) >> 4);
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s6:

               dst[0] = ((src[31] & 0x0f) << 8) + src[24];
               dst[1] = (src[30] << 4) + ((src[31] & 0xf0) >> 4);
               dst[2] = ((src[28] & 0x0f) << 8) + src[29];
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s7:

               dst[0] = (src[35] << 4) + ((src[28] & 0xf0) >> 4);
               dst[1] = ((src[33] & 0x0f) << 8) + src[34];
               dst[2] = (src[32] << 4) + ((src[33] & 0xf0) >> 4);
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 36;
            }
         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:

         lftOffs = (left >> 3) * 36;
         phase = left % 8;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            int pelcnt = rowPels;

#ifndef DPX12_PACKED
            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
#else
            int pel = irow * frmWdth + left;
            MTI_UINT8 *src = srcimg[0] + (pel >> 3) * 36;
            phase = pel % 8;
#endif
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            if (phase == 0)
            {
               goto t0;
            }
            if (phase == 1)
            {
               goto t1;
            }
            if (phase == 2)
            {
               goto t2;
            }
            if (phase == 3)
            {
               goto t3;
            }
            if (phase == 4)
            {
               goto t4;
            }
            if (phase == 5)
            {
               goto t5;
            }
            if (phase == 6)
            {
               goto t6;
            }
            goto t7;

            while (true)
            {

t0:

               dst[0] = (src[0] << 4) + (src[1] >> 4);
               dst[1] = ((src[1] & 0x0f) << 8) + src[2];
               dst[2] = (src[3] << 4) + (src[4] >> 4);
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t1:

               dst[0] = ((src[4] & 0x0f) << 8) + src[5];
               dst[1] = (src[6] << 4) + (src[7] >> 4);
               dst[2] = ((src[7] & 0x0f) << 8) + src[8];
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t2:
               dst[0] = (src[9] << 4) + (src[10] >> 4);
               dst[1] = ((src[10] & 0x0f) << 8) + src[11];
               dst[2] = (src[12] << 4) + (src[13] >> 4);
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t3:

               dst[0] = ((src[13] & 0x0f) << 8) + src[14];
               dst[1] = (src[15] << 4) + (src[16] >> 4);
               dst[2] = ((src[16] & 0x0f) << 8) + src[17];
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t4:

               dst[0] = (src[18] << 4) + (src[19] >> 4);
               dst[1] = ((src[19] & 0x0f) << 8) + src[20];
               dst[2] = (src[21] << 4) + (src[22] >> 4);
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t5:

               dst[0] = ((src[22] & 0x0f) << 8) + src[23];
               dst[1] = (src[24] << 4) + (src[25] >> 4);
               dst[2] = ((src[25] & 0x0f) << 8) + src[26];
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t6:

               dst[0] = (src[27] << 4) + (src[28] >> 4);
               dst[1] = ((src[28] & 0x0f) << 8) + src[29];
               dst[2] = (src[30] << 4) + (src[31] >> 4);
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t7:

               dst[0] = ((src[31] & 0x0f) << 8) + src[32];
               dst[1] = (src[33] << 4) + (src[34] >> 4);
               dst[2] = ((src[34] & 0x0f) << 8) + src[35];
               dst += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               src += 36;
            }
         }

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:

         lftOffs = (left) * 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            for (int i = 0; i < rowPels; i++)
            {

               dst[0] = (((MTI_UINT16)src[1]) << 4) + (((MTI_UINT16)src[0]) >> 4); // R
               dst[1] = (((MTI_UINT16)src[3]) << 4) + (((MTI_UINT16)src[2]) >> 4); // G
               dst[2] = (((MTI_UINT16)src[5]) << 4) + (((MTI_UINT16)src[4]) >> 4); // B

               dst += 3;
               src += 6;

            }

         }

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:

         lftOffs = (left) * 6;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            for (int i = 0; i < rowPels; i++)
            {

               dst[0] = (((MTI_UINT16)src[0]) << 4) + (((MTI_UINT16)src[1]) >> 4); // R
               dst[1] = (((MTI_UINT16)src[2]) << 4) + (((MTI_UINT16)src[3]) >> 4); // G
               dst[2] = (((MTI_UINT16)src[4]) << 4) + (((MTI_UINT16)src[5]) >> 4); // B

               dst += 3;
               src += 6;

            }

         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         if (left == 0 && frmFieldCount == 1)
         {
            int nBytes = ((srcrgn.bottom - srcrgn.top) + 1) * frmPitch;
            MTImemcpy(dstpels, srcimg, nBytes);
         }
         else
         {
            lftOffs = (left) * 6;

            for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
            {

               MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
               MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (((MTI_UINT16)src[1]) << 8) + ((MTI_UINT16)src[0]); // R
                  dst[1] = (((MTI_UINT16)src[3]) << 8) + ((MTI_UINT16)src[2]); // G
                  dst[2] = (((MTI_UINT16)src[5]) << 8) + ((MTI_UINT16)src[4]); // B

                  dst += 3;
                  src += 6;
               }
            }
         }

         break;

      default:
         retval = -1; // unsupported format
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_BGR:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            for (int i = 0; i < rowPels; i++)
            {

               dst[0] = (MTI_UINT16)src[2]; // R
               dst[1] = (MTI_UINT16)src[1]; // G
               dst[2] = (MTI_UINT16)src[0]; // B

               dst += 3;
               src += 3;
            }
         }
         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         lftOffs = (left) * 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
            MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

            for (int i = 0; i < rowPels; i++)
            {

               dst[2] = ((((MTI_UINT16)src[0])) << 2) + ((((MTI_UINT16)src[1]) & 0xc0) >> 6); // B
               dst[1] = ((((MTI_UINT16)src[1]) & 0x3f) << 4) + ((((MTI_UINT16)src[2]) & 0xf0) >> 4); // G
               dst[0] = ((((MTI_UINT16)src[2]) & 0x0f) << 6) + ((((MTI_UINT16)src[3]) & 0xfc) >> 2); // R

               dst += 3;
               src += 4;
            }
         }
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         if (left == 0 && frmFieldCount == 1)
         {
            int nBytes = ((srcrgn.bottom - srcrgn.top) + 1) * frmPitch;
            MTImemcpy(dstpels, srcimg, nBytes);
         }
         else
         {
            lftOffs = (left) * 6;

            for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
            {

               MTI_UINT8 *src = srcimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
               MTI_UINT16 *dst = dstpels + 3 * (irow * frmWdth + left);

               for (int i = 0; i < rowPels; i++)
               {

                  dst[2] = (((MTI_UINT16)src[1]) << 8) + ((MTI_UINT16)src[0]); // B
                  dst[1] = (((MTI_UINT16)src[3]) << 8) + ((MTI_UINT16)src[2]); // G
                  dst[0] = (((MTI_UINT16)src[5]) << 8) + ((MTI_UINT16)src[4]); // R

                  dst += 3;
                  src += 6;
               }
            }
         }

         break;

      default:
         retval = -1; // unsupported format
         break;

      }
      break;

   case IF_PIXEL_COMPONENTS_Y: // xyzzy1
   case IF_PIXEL_COMPONENTS_YYY: // xyzzy1

      // SOURCE is Y
      // DESTINATION is YYY
      switch (frmPelPacking)
      {
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {
            phase = irow * frmWdth + srcrgn.left;
            MTI_UINT8 *src = srcimg[0] + ((phase / 3) * 4);
            phase %= 3;

            MTI_UINT16 *dst = dstpels + irow * frmWdth + left;
            int pelcnt = rowPels;

            // SOURCE is BIG-ENDIAN
            r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);

            if (phase == 0)
            {
               goto u0;
            }

            if (phase == 1)
            {
               goto u1;
            }

            goto u2;

            while (true)
            {
u0: ;
               dst[0] = dst[1] = dst[2] = r0 & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }
u1: ;
               dst[0] = dst[1] = dst[2] = (r0 >> 10) & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }
u2: ;
               dst[0] = dst[1] = dst[2] = (r0 >> 20) & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }

               src += 4;
               r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
            }
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {
            phase = irow * frmWdth + srcrgn.left;
            MTI_UINT8 *src = srcimg[0] + ((phase / 3) * 4);
            phase %= 3;

            MTI_UINT16 *dst = dstpels + irow * frmWdth + left;
            int pelcnt = rowPels;

            // SOURCE is BIG-ENDIAN
            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

            if (phase == 0)
            {
               goto w0;
            }

            if (phase == 1)
            {
               goto w1;
            }

            goto w2;

            while (true)
            {
w0: ;
               dst[0] = dst[1] = dst[2] = (r0) & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }
w1: ;
               dst[0] = dst[1] = dst[2] = (r0 >> 10) & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }
w2: ;
               dst[0] = dst[1] = dst[2] = (r0 >> 20) & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }

               src += 4;

               r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
            }
         }

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         lftOffs = (left / 4) * 5;
         phase = left % 4;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; irow++)
         {

            MTI_UINT8 *src = srcimg[0] + irow * frmPitch + lftOffs;

            MTI_UINT16 *dst = dstpels + irow * frmWdth + left;

            int pelcnt = rowPels;

            r0 = *((MTI_UINT32 *) src);
            r1 = src[4];

            if (phase == 0)
            {
               goto v0;
            }

            if (phase == 1)
            {
               goto v1;
            }

            if (phase == 2)
            {
               goto v2;
            }

            goto v3;

            while (true)
            {
v0: ;
               dst[0] = dst[1] = dst[2] = (r0) & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }
v1: ;
               dst[0] = dst[1] = dst[2] = (r0 >> 10) & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }
v2: ;
               dst[0] = dst[1] = dst[2] = (r0 >> 20) & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }
v3: ;
               dst[0] = dst[1] = dst[2] = ((r0 >> 30) + (r1 << 2)) & 0x3ff;
               dst += 3;

               if (--pelcnt == 0)
               {
                  break;
               }

               src += 5;

               r0 = *((MTI_UINT32 *) src);
               r1 = src[4];
            }
         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         lftOffs = left * 2;

         for (int irow = srcrgn.top; irow <= srcrgn.bottom; ++irow)
         {
            MTI_UINT16 *src = (MTI_UINT16 *) (srcimg[0] + (irow * frmPitch) + lftOffs);
            MTI_UINT16 *dst = dstpels + (irow * frmWdth) + left;

            for (int i = 0; i < rowPels; i += 3)
            {
               dst[i] = dst[i + 1] = dst[i + 2] = *src++;
            }
         }

         break;

      default:
         retval = -1;
         break;

      }

      break;

   default:
      retval = -1; // unsupported format
      break;

   }

   return (retval);

}

// replace subregion to the destination as a BitBlt from a 16-bit 444
// progressive frame having the same dimensions as the destination
int CExtractor::replaceSubregionWithinFrame(MTI_UINT8 **dstimg, // -> destination field(s) (YUV422 or RGB variant)
      const CImageFormat *dstfmt, // image format of destination frame
      RECT *dstrgn, // region to be replaced
      MTI_UINT16 *srcpels) // array of 16-bit "444 data" (YUV or RGB)
{
return replaceSubregionWithinFrame(dstimg, dstfmt, *dstrgn, srcpels);
}

int CExtractor::replaceSubregionWithinFrame(

      MTI_UINT8 **dstimg, // -> destination field(s) (YUV422 or RGB variant)
      const CImageFormat *dstfmt, // image format of destination frame
      const RECT &dstrgn, // region to be replaced
      MTI_UINT16 *srcpels // array of 16-bit "444 data" (YUV or RGB)
      )
{
   int retval = 0; // assume supported format

   if (dstimg[0] == nullptr || srcpels == nullptr)
   {
      return 0;
   }

   int frmFieldCount = dstfmt->getFieldCount();
   int frmWdth = dstfmt->getPixelsPerLine();
   int frmPitch = dstfmt->getFramePitch();
   int frmPelComponents = dstfmt->getPixelComponents();
   int frmPelPacking = dstfmt->getPixelPacking();
   int rowPels = dstrgn.right - dstrgn.left + 1;
   int left = dstrgn.left;
   bool lftOdd = ((left & 1) == 1);
   int lftOffs;
   int phase;

   switch (frmPelComponents)
   {

   case IF_PIXEL_COMPONENTS_YUV422:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left >> 1) * 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               int pelcnt = rowPels;

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               if (lftOdd)
               { // beg on 2nd half of pixel pair
                  dst[2] = (MTI_UINT8)src[2]; // V
                  dst[3] = (MTI_UINT8)src[0]; // Y
                  src += 3;
                  dst += 4;
                  --pelcnt;
               }

               if (pelcnt != 0)
               {

                  for (; ;)
                  { // on 1st half of pixel-pair

                     MTI_UINT32 uAvg = (MTI_UINT32)src[1];
                     MTI_UINT32 vAvg = (MTI_UINT32)src[2];

                     dst[0] = (MTI_UINT8) uAvg; // U

                     dst[1] = (MTI_UINT8)src[0]; // Y
                     src += 3;
                     if ((--pelcnt) == 0)
                     {
                        break;
                     }

                     // the 2nd half of the pixel pair is available
                     // calculate U and V as averages over the halves

                     uAvg += (MTI_UINT32)src[1];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = uAvg >> 1;
                     dst[0] = (MTI_UINT8) uAvg; // U

                     vAvg += (MTI_UINT32)src[2];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = vAvg >> 1;
                     dst[2] = (MTI_UINT8) vAvg; // V

                     dst[3] = (MTI_UINT8)src[0]; // Y
                     src += 3;
                     if ((--pelcnt) == 0)
                     {
                        break;
                     }

                     dst += 4;

                  }

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         lftOffs = (left >> 1) * 5;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               int pelcnt = rowPels;

               // typo here created bug crashing HTV release - orig
               // said "MTI_UINT16 *src = src + ..."
               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               if (lftOdd)
               { // beg on 2nd half of pixel pair

                  dst[2] = (dst[2] & 0xf0) + (MTI_UINT8)((src[2] & 0x3c0) >> 6);
                  dst[3] = (MTI_UINT8)(((src[2] & 0x3f) << 2) + ((src[0] & 0x300) >> 8));
                  dst[4] = (MTI_UINT8)(src[0] & 0xff);

                  src += 3;
                  dst += 5;
                  --pelcnt;
               }

               if (pelcnt != 0)
               {

                  for (; ;)
                  { // on 1st half of pixel-pair

                     MTI_UINT32 uAvg = (MTI_UINT32)src[1];
                     MTI_UINT32 vAvg = (MTI_UINT32)src[2];

                     // drop U and Y into place

                     dst[0] = (MTI_UINT8)(uAvg >> 2);
                     dst[1] = (MTI_UINT8)(((uAvg & 0x03) << 6) + ((src[0] & 0x3f0) >> 4));
                     dst[2] = (dst[2] & 0x0f) + (MTI_UINT8)((src[0] & 0x0f) << 4);

                     src += 3;
                     if ((--pelcnt) == 0)
                     {
                        break;
                     }

                     // the 2nd half of the pixel pair is available
                     // calculate U and V as averages over the halves

                     uAvg += (MTI_UINT32)src[1];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = uAvg >> 1;

                     // revise the previous U

                     dst[0] = (MTI_UINT8)(uAvg >> 2);
                     dst[1] = (MTI_UINT8)((uAvg & 3) << 6) + (dst[1] & 0x3f);

                     vAvg += (MTI_UINT32)src[2];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = vAvg >> 1;

                     // drop V and Y into place

                     dst[2] = (dst[2] & 0xf0) + (MTI_UINT8)((vAvg & 0x3c0) >> 6);
                     dst[3] = (MTI_UINT8)(((vAvg & 0x3f) << 2) + ((src[0] & 0x300) >> 8));
                     dst[4] = (MTI_UINT8)(src[0] & 0xff);

                     src += 3;
                     if ((--pelcnt) == 0)
                     {
                        break;
                     }

                     dst += 5;

                  }

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               int pelcnt = rowPels;

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                MTI_UINT32 y, uAvg, vAvg;

               if (phase == 0)
               {
                  goto p0;
               }
               if (phase == 1)
               {
                  goto p1;
               }
               if (phase == 2)
               {
                  goto p2;
               }
               if (phase == 3)
               {
                  goto p3;
               }
               if (phase == 4)
               {
                  goto p4;
               }
               goto p5;

               while (true)
               {

p0: ;

                  uAvg = src[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += src[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[0] = uAvg;
                  dst[1] = (y << 2) + (uAvg >> 8);
                  dst[2] = (dst[2] & 0xf0) + (y >> 6);

                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  src += 3;

p1: ;

                  vAvg = src[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += src[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[2] = (vAvg << 4) + (dst[2] & 0x0f);
                  dst[3] = (vAvg >> 4);
                  dst[4] = y;
                  dst[5] = (dst[5] & 0xfc) + (y >> 8);

                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  src += 3;

p2: ;

                  uAvg = src[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += src[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[5] = (uAvg << 2) + (dst[5] & 0x3);
                  dst[6] = (y << 4) + (uAvg >> 6);
                  dst[7] = (y >> 4);

                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  src += 3;

p3: ;

                  vAvg = src[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += src[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[8] = vAvg;
                  dst[9] = (vAvg >> 8) + (y << 2);
                  dst[10] = (dst[10] & 0xf0) + (y >> 6);

                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  src += 3;

p4: ;

                  uAvg = src[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += src[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[10] = (uAvg << 4) + (dst[10] & 0x0f);
                  dst[11] = (uAvg >> 4);
                  dst[12] = y;
                  dst[13] = (dst[13] & 0xfc) + (y >> 8);

                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  src += 3;

p5: ;

                  vAvg = src[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += src[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[13] = (vAvg << 2) + (dst[13] & 0x3);
                  dst[14] = (y << 4) + (vAvg >> 6);
                  dst[15] = (y >> 4);

                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  src += 3;

                  dst += 16;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               int pelcnt = rowPels;

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                MTI_UINT32 y, uAvg, vAvg;

               if (phase == 0)
               {
                  goto q0;
               }
               if (phase == 1)
               {
                  goto q1;
               }
               if (phase == 2)
               {
                  goto q2;
               }
               if (phase == 3)
               {
                  goto q3;
               }
               if (phase == 4)
               {
                  goto q4;
               }
               goto q5;

               while (true)
               {

q0: ;

                  uAvg = src[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += src[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[0] = uAvg >> 2;
                  dst[1] = y >> 2;
                  dst[3] = ((uAvg & 0x03) << 4) + ((y & 0x03) << 2) + (dst[3] & 0x03);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q1: ;

                  vAvg = src[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += src[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[2] = vAvg >> 2;
                  dst[3] = (dst[3] & 0x3c) + (vAvg & 3);
                  dst[4] = y >> 2;
                  dst[7] = ((y & 0x03) << 4) + (dst[7] & 0xfc);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q2: ;

                  uAvg = src[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += src[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[5] = uAvg >> 2;
                  dst[6] = y >> 2;
                  dst[7] = (dst[7] & 0x30) + ((uAvg & 0x03) << 2) + (y & 0x03);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q3: ;

                  vAvg = src[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += src[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[8] = vAvg >> 2;
                  dst[9] = y >> 2;
                  dst[11] = ((vAvg & 0x03) << 4) + ((y & 0x03) << 2);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q4: ;

                  uAvg = src[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg += src[4];
                     if (uAvg & 1)
                     {
                        uAvg++;
                     }
                     uAvg = (uAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[10] = uAvg >> 2;
                  dst[11] = (dst[11] & 0x3c) + (uAvg & 0x03);
                  dst[12] = y >> 2; ;
                  dst[15] = (dst[15] & 0x3c) + (uAvg & 0x03);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

q5: ;

                  vAvg = src[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg += src[-1];
                     if (vAvg & 1)
                     {
                        vAvg++;
                     }
                     vAvg = (vAvg >> 1);
                  }
#endif
                  y = src[0];

                  dst[13] = vAvg >> 2;
                  dst[14] = y >> 2;
                  dst[15] = (dst[15] & 0x30) + ((vAvg & 0x03) << 2) + (y & 0x03);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  dst += 16;
               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L: // CLIPSTER

         lftOffs = (left / 6) * 16;
         phase = left % 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               int pelcnt = rowPels;

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                MTI_UINT32 y, uAvg, vAvg;

               if (phase == 0)
               {
                  goto r0;
               }
               if (phase == 1)
               {
                  goto r1;
               }
               if (phase == 2)
               {
                  goto r2;
               }
               if (phase == 3)
               {
                  goto r3;
               }
               if (phase == 4)
               {
                  goto r4;
               }
               goto r5;

               while (true)
               {

r0: ;

                  uAvg = src[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg = (uAvg + srcpels[4]) >> 1;
                  }
#endif
                  y = src[0];

                  dst[0] = uAvg >> 2;
                  dst[1] = (uAvg << 6) + (y >> 4);
                  dst[2] = (y << 4) + (dst[2] & 0x0f);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r1: ;

                  vAvg = src[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg = (vAvg + src[-1]) >> 1;
                  }
#endif
                  y = src[0];

                  dst[2] = (dst[2] & 0xf0) + (vAvg >> 6);
                  dst[3] = (vAvg << 2);
                  dst[4] = (y >> 2);
                  dst[5] = (y << 6) + (dst[5] & 0x3f);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r2: ;

                  uAvg = src[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg = (uAvg + src[4]) >> 1;
                  }
#endif
                  y = src[0];

                  dst[5] = (dst[5] & 0xc0) + (uAvg >> 4);
                  dst[6] = (uAvg << 4) + (y >> 6);
                  dst[7] = (y << 2);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r3: ;

                  vAvg = src[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg = (vAvg + src[-1]) >> 1;
                  }
#endif
                  y = src[0];

                  dst[8] = (vAvg >> 2);
                  dst[9] = (vAvg << 6) + (y >> 4);
                  dst[10] = (y << 4) + (dst[10] & 0x0f);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r4: ;

                  uAvg = src[1];
#ifdef AVG_UV
                  if (pelcnt > 1)
                  { // avg w next U
                     uAvg = (uAvg + src[4]) >> 1;
                  }
#endif
                  y = src[0];

                  dst[10] = (dst[10] & 0xf0) + (uAvg >> 6);
                  dst[11] = (uAvg << 2);
                  dst[12] = (y >> 2);
                  dst[13] = (y << 6) + (dst[13] & 0x3f);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

r5: ;

                  vAvg = src[2];
#ifdef AVG_UV
                  if (pelcnt != rowPels)
                  { // avg w prev V
                     vAvg = (vAvg + src[-1]) >> 1;
                  }
#endif
                  y = src[0];

                  dst[13] = (dst[13] & 0xc0) + (vAvg >> 4);
                  dst[14] = (vAvg << 4) + (y >> 6);
                  dst[15] = (y << 2);

                  src += 3;
                  if (--pelcnt == 0)
                  {
                     break;
                  }

                  dst += 16;
               }

            }

         }

         break;

      default:
         retval = -1;
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_YUV444:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)src[1]; // U
                  dst[1] = (MTI_UINT8)src[0]; // Y
                  dst[2] = (MTI_UINT8)src[2]; // V

                  dst += 3;
                  src += 3;

               }

            }

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         lftOffs = (left) * 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount] != NULL)
            {

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)(src[1] & 0xff);
                  dst[1] = (MTI_UINT8)((src[1] & 0x300) >> 8) + (MTI_UINT8)((src[0] & 0x3f) << 2);
                  dst[2] = (MTI_UINT8)((src[0] & 0x3c0) >> 6) + (MTI_UINT8)((src[2] & 0x0f) << 4);
                  dst[3] = (MTI_UINT8)((src[2] & 0x3f0) >> 4);

                  dst += 4;
                  src += 3;

               }

            }

         }

         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_RGB:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)src[0];
                  dst[1] = (MTI_UINT8)src[1];
                  dst[2] = (MTI_UINT8)src[2];

                  dst += 3;
                  src += 3;
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         lftOffs = (left) * 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)((src[0] & 0x3fc) >> 2);
                  dst[1] = (MTI_UINT8)((src[0] & 0x03) << 6) + (MTI_UINT8)((src[1] & 0x3f0) >> 4);
                  dst[2] = (MTI_UINT8)((src[1] & 0x0f) << 4) + (MTI_UINT8)((src[2] & 0x3c0) >> 6);
                  dst[3] = (MTI_UINT8)((src[2] & 0x3f) << 2);

                  dst += 4;
                  src += 3;
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         lftOffs = (left) * 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)((src[0] & 0x3f0) >> 4);
                  dst[1] = (MTI_UINT8)((src[0] & 0x0f) << 4) + (MTI_UINT8)((src[1] & 0x3c0) >> 6);
                  dst[2] = (MTI_UINT8)((src[1] & 0x3f) << 2) + (MTI_UINT8)((src[2] & 0x300) >> 8);
                  dst[3] = (MTI_UINT8)((src[2] & 0xff));

                  dst += 4;
                  src += 3;
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:

         lftOffs = (left >> 3) * 36;
         phase = left % 8;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            int pelcnt = rowPels;

            MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
#ifndef DPX12_PACKED
            MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
#else
            int pel = irow * frmWdth + left;
            MTI_UINT8 *dst = dstimg[0] + (pel >> 3) * 36;
            phase = pel % 8;
#endif
            if (phase == 0)
            {
               goto s0;
            }
            if (phase == 1)
            {
               goto s1;
            }
            if (phase == 2)
            {
               goto s2;
            }
            if (phase == 3)
            {
               goto s3;
            }
            if (phase == 4)
            {
               goto s4;
            }
            if (phase == 5)
            {
               goto s5;
            }
            if (phase == 6)
            {
               goto s6;
            }
            goto s7;

            while (true)
            {

s0:

               dst[0] = (MTI_UINT8)src[2];
               dst[1] = (MTI_UINT8)(src[1] >> 4);
               dst[2] = (MTI_UINT8)((src[1] << 4) + (src[0] >> 8));
               dst[3] = (MTI_UINT8)src[0];
               dst[7] = (MTI_UINT8)((dst[7] & 0xf0) + (src[2] >> 8));
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s1:

               dst[4] = (MTI_UINT8)((src[2] << 4) + (src[1] >> 8));
               dst[5] = (MTI_UINT8)src[1];
               dst[6] = (MTI_UINT8)(src[0] >> 4);
               dst[7] = (MTI_UINT8)((src[0] << 4) + (dst[7] & 0x0f));
               dst[11] = (MTI_UINT8)(src[2] >> 4);
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s2:

               dst[8] = (MTI_UINT8)(src[1] >> 4);
               dst[9] = (MTI_UINT8)((src[1] << 4) + (src[0] >> 8));
               dst[10] = (MTI_UINT8)src[0];
               dst[14] = (MTI_UINT8)((dst[14] & 0xf0) + (src[2] >> 8));
               dst[15] = (MTI_UINT8)src[2];
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s3:

               dst[12] = (MTI_UINT8)src[1];
               dst[13] = (MTI_UINT8)(src[0] >> 4);
               dst[14] = (MTI_UINT8)((src[0] << 4) + (dst[14] & 0x0f));
               dst[18] = (MTI_UINT8)(src[2] >> 4);
               dst[19] = (MTI_UINT8)((src[2] << 4) + (src[1] >> 8));
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s4:

               dst[16] = (MTI_UINT8)((src[1] << 4) + (src[0] >> 8));
               dst[17] = (MTI_UINT8)src[0];
               dst[21] = (MTI_UINT8)((dst[21] & 0xf0) + (src[2] >> 8));
               dst[22] = (MTI_UINT8)src[2];
               dst[23] = (MTI_UINT8)(src[1] >> 4);
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s5:

               dst[20] = (MTI_UINT8)(src[0] >> 4);
               dst[21] = (MTI_UINT8)((src[0] << 4) + (dst[21] & 0x0f));
               dst[25] = (MTI_UINT8)(src[2] >> 4);
               dst[26] = (MTI_UINT8)((src[2] << 4) + (src[1] >> 8));
               dst[27] = (MTI_UINT8)src[1];
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s6:

               dst[24] = (MTI_UINT8)src[0];
               dst[28] = (MTI_UINT8)((dst[28] & 0xf0) + (src[2] >> 8));
               dst[29] = (MTI_UINT8)src[2];
               dst[30] = (MTI_UINT8)(src[1] >> 4);
               dst[31] = (MTI_UINT8)((src[1] << 4) + (src[0] >> 8));
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

s7:

               dst[28] = (MTI_UINT8)((src[0] << 4) + (dst[28] & 0x0f));
               dst[32] = (MTI_UINT8)(src[2] >> 4);
               dst[33] = (MTI_UINT8)((src[2] << 4) + (src[1] >> 8));
               dst[34] = (MTI_UINT8)src[1];
               dst[35] = (MTI_UINT8)(src[0] >> 4);
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               dst += 36;
            }
         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:

         lftOffs = (left >> 3) * 36;
         phase = left % 8;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            int pelcnt = rowPels;

            MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
#ifndef DPX12_PACKED
            MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;
#else
            int pel = irow * frmWdth + left;
            MTI_UINT8 *dst = dstimg[0] + (pel >> 3) * 36;
            phase = pel % 8;
#endif
            if (phase == 0)
            {
               goto t0;
            }
            if (phase == 1)
            {
               goto t1;
            }
            if (phase == 2)
            {
               goto t2;
            }
            if (phase == 3)
            {
               goto t3;
            }
            if (phase == 4)
            {
               goto t4;
            }
            if (phase == 5)
            {
               goto t5;
            }
            if (phase == 6)
            {
               goto t6;
            }
            goto t7;

            while (true)
            {

t0:

               dst[0] = (MTI_UINT8)(src[0] >> 4);
               dst[1] = (MTI_UINT8)((src[0] << 4) + (src[1] >> 8));
               dst[2] = (MTI_UINT8)src[1];
               dst[3] = (MTI_UINT8)(src[2] >> 4);
               dst[4] = (MTI_UINT8)((src[2] << 4) + (dst[4] & 0x0f));
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t1:

               dst[4] = (MTI_UINT8)((dst[4] & 0xf0) + (src[0] >> 8));
               dst[5] = (MTI_UINT8)src[0];
               dst[6] = (MTI_UINT8)(src[1] >> 4);
               dst[7] = (MTI_UINT8)((src[1] << 4) + (src[2] >> 8));
               dst[8] = (MTI_UINT8)src[2];
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t2:

               dst[9] = (MTI_UINT8)(src[0] >> 4);
               dst[10] = (MTI_UINT8)((src[0] << 4) + (src[1] >> 8));
               dst[11] = (MTI_UINT8)src[1];
               dst[12] = (MTI_UINT8)(src[2] >> 4);
               dst[13] = (MTI_UINT8)((src[2] << 4) + (dst[13] & 0x0f));
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t3:

               dst[13] = (MTI_UINT8)((dst[13] & 0xf0) + (src[0] >> 8));
               dst[14] = (MTI_UINT8)src[0];
               dst[15] = (MTI_UINT8)(src[1] >> 4);
               dst[16] = (MTI_UINT8)((src[1] << 4) + (src[2] >> 8));
               dst[17] = (MTI_UINT8)src[2];
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t4:

               dst[18] = (MTI_UINT8)(src[0] >> 4);
               dst[19] = (MTI_UINT8)((src[0] << 4) + (src[1] >> 8));
               dst[20] = (MTI_UINT8)src[1];
               dst[21] = (MTI_UINT8)(src[2] >> 4);
               dst[22] = (MTI_UINT8)((src[2] << 4) + (dst[22] & 0x0f));
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t5:

               dst[22] = (MTI_UINT8)((dst[22] & 0xf0) + (src[0] >> 8));
               dst[23] = (MTI_UINT8)src[0];
               dst[24] = (MTI_UINT8)(src[1] >> 4);
               dst[25] = (MTI_UINT8)((src[1] << 4) + (src[2] >> 8));
               dst[26] = (MTI_UINT8)src[2];
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t6:

               dst[27] = (MTI_UINT8)(src[0] >> 4);
               dst[28] = (MTI_UINT8)((src[0] << 4) + (src[1] >> 8));
               dst[29] = (MTI_UINT8)src[1];
               dst[30] = (MTI_UINT8)(src[2] >> 4);
               dst[31] = (MTI_UINT8)((src[2] << 4) + (dst[31] & 0x0f));
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

t7:

               dst[31] = (MTI_UINT8)((dst[31] & 0xf0) + (src[0] >> 8));
               dst[32] = (MTI_UINT8)src[0];
               dst[33] = (MTI_UINT8)(src[1] >> 4);
               dst[34] = (MTI_UINT8)((src[1] << 4) + (src[2] >> 8));
               dst[35] = (MTI_UINT8)src[2];
               src += 3;
               if (--pelcnt == 0)
               {
                  break;
               }

               dst += 36;
            }
         }

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:

         lftOffs = (left) * 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)(src[0] << 4);
                  dst[1] = (MTI_UINT8)(src[0] >> 4);
                  dst[2] = (MTI_UINT8)(src[1] << 4);
                  dst[3] = (MTI_UINT8)(src[1] >> 4);
                  dst[4] = (MTI_UINT8)(src[2] << 4);
                  dst[5] = (MTI_UINT8)(src[2] >> 4);

                  dst += 6;
                  src += 3;
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:

         lftOffs = (left) * 6;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)(src[0] >> 4);
                  dst[1] = (MTI_UINT8)(src[0] << 4);
                  dst[2] = (MTI_UINT8)(src[1] >> 4);
                  dst[3] = (MTI_UINT8)(src[1] << 4);
                  dst[4] = (MTI_UINT8)(src[2] >> 4);
                  dst[5] = (MTI_UINT8)(src[2] << 4);

                  dst += 6;
                  src += 3;
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         if (left == 0 && frmFieldCount == 1)
         {
            int nBytes = ((dstrgn.bottom - dstrgn.top) + 1) * frmPitch;
            MTImemcpy(dstimg[0], srcpels, nBytes);
         }
         else
         {
            lftOffs = (left) * 6;

            for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
            {

               if (dstimg[irow % frmFieldCount])
               {

                  MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
                  MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                  for (int i = 0; i < rowPels; i++)
                  {

                     dst[0] = (MTI_UINT8)(src[0]);
                     dst[1] = (MTI_UINT8)(src[0] >> 8);
                     dst[2] = (MTI_UINT8)(src[1]); ;
                     dst[3] = (MTI_UINT8)(src[1] >> 8);
                     dst[4] = (MTI_UINT8)(src[2]);
                     dst[5] = (MTI_UINT8)(src[2] >> 8);

                     dst += 6;
                     src += 3;
                  }
               }
            }
         }

         break;

      default:
         retval = -1; // unsupported format
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_BGR:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         lftOffs = (left) * 3;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)src[2]; // B
                  dst[1] = (MTI_UINT8)src[1]; // G
                  dst[2] = (MTI_UINT8)src[0]; // R

                  dst += 3;
                  src += 3;
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         lftOffs = (left) * 4;

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {

            if (dstimg[irow % frmFieldCount])
            {

               MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
               MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

               for (int i = 0; i < rowPels; i++)
               {

                  dst[0] = (MTI_UINT8)((src[2] & 0x3fc) >> 2);
                  dst[1] = (MTI_UINT8)((src[2] & 0x03) << 6) + (MTI_UINT8)((src[1] & 0x3f0) >> 4);
                  dst[2] = (MTI_UINT8)((src[1] & 0x0f) << 4) + (MTI_UINT8)((src[0] & 0x3c0) >> 6);
                  dst[3] = (MTI_UINT8)((src[0] & 0x3f) << 2);

                  dst += 4;
                  src += 3;
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         if (left == 0 && frmFieldCount == 1)
         {
            int nBytes = ((dstrgn.bottom - dstrgn.top) + 1) * frmPitch;
            MTImemcpy(dstimg[0], srcpels, nBytes);
         }
         else
         {
            lftOffs = (left) * 6;

            for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
            {

               if (dstimg[irow % frmFieldCount])
               {

                  MTI_UINT16 *src = srcpels + 3 * (irow * frmWdth + left);
                  MTI_UINT8 *dst = dstimg[irow % frmFieldCount] + ((irow / frmFieldCount) * frmPitch) + lftOffs;

                  for (int i = 0; i < rowPels; i++)
                  {

                     dst[0] = (MTI_UINT8)(src[2]);
                     dst[1] = (MTI_UINT8)(src[2] >> 8);
                     dst[2] = (MTI_UINT8)(src[1]); ;
                     dst[3] = (MTI_UINT8)(src[1] >> 8);
                     dst[4] = (MTI_UINT8)(src[0]);
                     dst[5] = (MTI_UINT8)(src[0] >> 8);

                     dst += 6;
                     src += 3;
                  }
               }
            }
         }

         break;

      default:
         retval = -1; // unsupported format
         break;
      }

      break;

   case IF_PIXEL_COMPONENTS_Y:
   case IF_PIXEL_COMPONENTS_YYY:

      // SOURCE is YYY
      // DESTINATION is Y
      switch (frmPelPacking)
      {
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         // DESTINATION is BIG-ENDIAN
         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {
            int pelcnt = rowPels;

            int pixelOffset = (irow * frmWdth) + left;
            MTI_UINT16 *src = srcpels + (pixelOffset * 3);
            MTI_UINT8 *dst = dstimg[0] + ((pixelOffset / 3) * 4);
            phase = pixelOffset % 3;

            if (phase == 0)
            {
               while (pelcnt >= 3)
               {
                  dst[0] = (src[0] >> 2);                    // 98765432
                  dst[1] = (src[0] << 6) | (src[3] >> 4);    // 10 | 987654
                  dst[2] = (src[3] << 4) | (src[6] >> 6);    // 3210 | 9876
                  dst[3] = (src[6] << 2);                    // 543210 | XX

                  dst += 4;
                  src += 9;
                  pelcnt -= 3;
               }

               if (pelcnt == 2)
               {
                  dst[0] = (src[0] >> 2);                     // 98765432
                  dst[1] = (src[0] << 6) | (src[3] >> 4);     // 10 | 987654
                  dst[2] = (src[3] << 4) | (dst[2] & 0x0F);   // 3210 | PPPP
               }
               else if (pelcnt == 1)
               {
                  dst[0] = src[0] >> 4;                       // 98765432
                  dst[1] = (src[0] << 4) | (dst[1] & 0x3F);   // 10 | PPPPPP
               }
            }
            else if (phase == 1)
            {
               while (pelcnt >= 3)
               {
                  dst[1] = (dst[1] & 0xC0) | (src[0] >> 4);   // PP | 987654
                  dst[2] = (src[0] << 4)   | (src[3] >> 6);   // 3210 | 9876
                  dst[3] = (src[3] << 2);                     // 543210 | XX
                  dst[4] = (src[6] >> 2);                     // 98765432
                  dst[5] = (src[6] << 6)   | (dst[5] & 0x3F); // 10 | PPPPPP

                  dst += 4;
                  src += 9;
                  pelcnt -= 3;
               }

               if (pelcnt == 2)
               {
                  dst[1] = (dst[1] & 0xC0) | (src[0] >> 4);   // PP | 987654
                  dst[2] = (src[0] << 4)   | (src[3] >> 6);   // 3210 | 9876
                  dst[3] = (src[3] << 2)   | (dst[3] & 0x03); // 543210 | PP
               }
               else if (pelcnt == 1)
               {
                  dst[1] = (dst[1] & 0xC0) | (src[0] >> 4);   // PP | 987654
                  dst[2] = (src[0] << 2)   | (dst[2] & 0x0F); // 3210 | PPPP
               }
            }
            else
            {
               while (pelcnt >= 3)
               {
                  dst[2] = (dst[2] & 0xF0) | (src[0] >> 6);   // PPPP | 9876
                  dst[3] = (src[0] << 2);                     // 543210 | XX
                  dst[4] = (src[3] >> 2);                     // 98765432
                  dst[5] = (src[3] << 6) | (src[6] >> 4);     // 10 | 987654
                  dst[6] = (src[6] << 4) | (dst[6] & 0x0F);   // 3210 | PPPP

                  dst += 4;
                  src += 9;
                  pelcnt -= 3;
               }

               if (pelcnt == 2)
               {
                  dst[2] = (dst[2] & 0xF0) | (src[0] >> 6);   // PPPP | 9876
                  dst[3] = (src[0] << 2);                     // 543210 | XX
                  dst[4] = (src[3] >> 2);                     // 98765432
                  dst[5] = (src[3] << 6)   | (dst[5] & 0x3F); // 10 | PPPPPP
               }
               else if (pelcnt == 1)
               {
                  dst[2] = (dst[2] & 0xF0) | (src[0] >> 6);   // PPPP | 9876
                  dst[3] = (src[0] << 2);                     // 543210 | XX
               }
            }
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         // DESTINATION is BIG-ENDIAN
         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
         {
            int pelcnt = rowPels;

            int pixelOffset = (irow * frmWdth) + left;
            MTI_UINT16 *src = srcpels + (pixelOffset * 3);
            MTI_UINT8 *dst = dstimg[0] + ((pixelOffset / 3) * 4);
            phase = pixelOffset % 3;

            if (phase == 0)
            {
               while (pelcnt >= 3)
               {
                  dst[0] = src[0] >> 4;                      // xx987654
                  dst[1] = (src[0] << 4) | (src[3] >> 6);    // 3210 | 9876
                  dst[2] = (src[3] << 2) | (src[6] >> 8);    // 543210 | 98
                  dst[3] = src[6] & 0xff;                    // 76543210

                  dst += 4;
                  src += 9;
                  pelcnt -= 3;
               }

               if (pelcnt == 2)
               {
                  dst[0] = src[0] >> 4;                      // xx987654
                  dst[1] = (src[0] << 4) | (src[3] >> 6);    // 3210 | 9876
                  dst[2] = (src[3] << 2) | (dst[2] & 0x03);  // 543210 | pp
               }
               else if (pelcnt == 1)
               {
                  dst[0] = src[0] >> 4;                      // xx987654
                  dst[1] = (src[0] << 4) | (dst[1] & 0x0F);  // 3210 | pppp
               }
            }
            else if (phase == 1)
            {
               while (pelcnt >= 3)
               {
                  dst[1] = (dst[1] & 0xF0) | (src[0] >> 6);  // pppp | 9876
                  dst[2] = (src[0] << 2) | (src[3] >> 8);    // 543210 | 98
                  dst[3] = src[3] & 0xff;                    // 76543210
                  dst[4] = src[6] >> 4;                      // xx987654
                  dst[5] = (src[6] << 4) | (dst[5] & 0x0F);  // 3210 | pppp

                  dst += 4;
                  src += 9;
                  pelcnt -= 3;
               }

               if (pelcnt == 2)
               {
                  dst[1] = (dst[1] & 0xF0) | (src[0] >> 6);  // pppp | 9876
                  dst[2] = (src[0] << 2) | (src[3] >> 8);    // 543210 | 98
                  dst[3] = src[3] & 0xff;                    // 76543210
               }
               else if (pelcnt == 1)
               {
                  dst[1] = (dst[1] & 0xF0) | (src[0] >> 6);  // pppp | 9876
                  dst[2] = (src[0] << 2) | (dst[2] & 0x03);  // 543210 | PP
               }
            }
            else
            {
               while (pelcnt >= 3)
               {
                  dst[2] = (dst[2] & 0xFC) | (src[0] >> 8);  // pppppp | 98
                  dst[3] = src[0] & 0xff;                    // 76543210
                  dst[4] = src[3] >> 4;                      // xx987654
                  dst[5] = (src[3] << 4) | (src[6] >> 6);    // 3210 | 9876
                  dst[6] = (src[6] << 2) | (dst[6] & 0x03);  // 543210 | pp

                  dst += 4;
                  src += 9;
                  pelcnt -= 3;
               }

               if (pelcnt == 2)
               {
                  dst[2] = (dst[2] & 0xFC) | (src[0] >> 8);  // pppppp | 98
                  dst[3] = src[0] & 0xff;                    // 76543210
                  dst[4] = src[3] >> 4;                      // xx987654
                  dst[5] = (src[3] << 4) | (dst[5] & 0x0F);// 3210 | pppp
               }
               else if (pelcnt == 1)
               {
                  dst[2] = (dst[2] & 0xFC) | (src[0] >> 8);  // pppppp | 98
                  dst[3] = src[0] & 0xff;                    // 76543210
               }
            }
         }

         break;

      // QQQ I'm pretty sure this one is wrong.... QQQ
      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

            MTIassert(frmPelPacking != IF_PIXEL_PACKING_4_10Bits_IN_5Bytes);

//         lftOffs = (left / 4) * 5;
//         phase = left % 4;
//
//         for (int irow = dstrgn.top; irow <= dstrgn.bottom; irow++)
//         {
//
//            if (dstimg[0])
//            {
//
//               int pelcnt = rowPels;
//
//               MTI_UINT16 *src = srcpels + irow * frmWdth + left;
//               MTI_UINT8 *dst = dstimg[0] + irow * frmPitch + lftOffs;
//
//               if (phase == 0)
//               {
//                  goto v0;
//               }
//               if (phase == 1)
//               {
//                  goto v1;
//               }
//               if (phase == 2)
//               {
//                  goto v2;
//               }
//               goto v3;
//
//               while (true)
//               {
//
//v0: ;
//
//                  r0 = src[0];
//                  dst[0] = (r0);
//                  dst[1] = ((r0 >> 8) & 0x03) + (dst[1] & 0xfc);
//
//                  if (--pelcnt == 0)
//                  {
//                     break;
//                  }
//
//v1: ;
//
//                  r0 = src[1];
//                  dst[1] = (dst[1] & 0x03) + (r0 << 2);
//                  dst[2] = ((r0 >> 6) & 0x0f) + (dst[2] & 0xf0);
//
//                  if (--pelcnt == 0)
//                  {
//                     break;
//                  }
//
//v2: ;
//
//                  r0 = src[2];
//                  dst[2] = (dst[2] & 0x0f) + (r0 << 4);
//                  dst[3] = ((r0 >> 4) & 0x3f) + (dst[3] & 0xc0);
//
//                  if (--pelcnt == 0)
//                  {
//                     break;
//                  }
//
//v3: ;
//
//                  r0 = src[3];
//                  dst[3] = (dst[3] & 0x3f) + (r0 << 6);
//                  dst[4] = (r0 >> 2);
//
//                  if (--pelcnt == 0)
//                  {
//                     break;
//                  }
//
//                  src += 4;
//                  dst += 5;
//               }
//            }
//         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE in I/O

         for (int irow = dstrgn.top; irow <= dstrgn.bottom; ++irow)
         {
            int pixelOffset = (irow * frmWdth) + left;
            MTI_UINT16 *src = srcpels + (pixelOffset * 3);
            MTI_UINT16 *dst = ((MTI_UINT16 *) dstimg[0]) + pixelOffset;

            for (int i = 0; i < rowPels; ++i)
            {
               dst[i] = src[i * 3];  // Arbitrarily grab from RED channel.
            }
         }

         break;

      default:
         retval = -1; // unsupported format
         break;
      }

      break;

   default:
      retval = -1; // unsupported format
      break;

   }

   return (retval);
}

// this call returns the RGB (YUV) component values for a given (x, y) point within a native frame
//
int CExtractor::extractPoint(MTI_UINT16 * RY, MTI_UINT16 * GU, MTI_UINT16 * BV, MTI_UINT8 **srcimg, const CImageFormat *srcfmt,
      int x, int y)
{
   int retval = 0; // assume supported format

   int frmFieldCount = srcfmt->getFieldCount();
   int frmWdth = srcfmt->getTotalFrameWidth();
   int frmHght = srcfmt->getTotalFrameHeight();
   int frmPitch = srcfmt->getFramePitch();
   int frmPelComponents = srcfmt->getPixelComponents();
   int frmPelPacking = srcfmt->getPixelPacking();

   if ((x < 0) || (x > (frmWdth - 1)) || (y < 0) || (y > (frmHght - 1)))
   {
      return (-1);
   }

   int pel;
   MTI_UINT8 *src;
   MTI_UINT32 r0, r1;

   switch (frmPelComponents)
   {

   case IF_PIXEL_COMPONENTS_YUV422:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + ((x / 2) << 2);

         switch ((x & 1))
         { // phase

         case 0:

            *RY = src[1];
            *GU = src[0];
            *BV = src[2];

            break;

         case 1:

            *RY = src[3];
            *GU = src[0];
            *BV = src[2];

            break;

         }

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + ((x / 2) * 5);

         switch ((x & 1))
         { // phase

         case 0:

            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

            *RY = (r0 >> 12) & 0x3ff;
            *GU = (r0 >> 22) & 0x3ff;
            *BV = (r0 >> 2) & 0x3ff;

            break;

         case 1:

            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
            r1 = (src[3] << 8) + src[4];

            *RY = (r1) & 0x3ff;
            *GU = (r0 >> 22) & 0x3ff;
            *BV = (r0 >> 2) & 0x3ff;

            break;

         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + ((x / 6) << 4);

         switch ((x % 6))
         { // phase

         case 0:

            r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + src[0];

            *RY = (r0 >> 10) & 0x3ff;
            *GU = (r0) & 0x3ff;
            *BV = (r0 >> 20) & 0x3ff;

            break;

         case 1:

            r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + src[0];
            r1 = (src[5] << 8) + src[4];

            *RY = (r1) & 0x3ff;
            *GU = (r0) & 0x3ff;
            *BV = (r0 >> 20) & 0x3ff;

            break;

         case 2:

            r0 = (src[7] << 24) + (src[6] << 16) + (src[5] << 8);
            r1 = (src[9] << 8) + src[8];

            *RY = (r0 >> 20) & 0x3ff;
            *GU = (r0 >> 10) & 0x3ff;
            *BV = (r1) & 0x3ff;

            break;

         case 3:

            r0 = (src[6] << 16) + (src[5] << 8);
            r1 = (src[10] << 16) + (src[9] << 8) + src[8];

            *RY = (r1 >> 10) & 0x3ff;
            *GU = (r0 >> 10) & 0x3ff;
            *BV = (r1) & 0x3ff;

            break;

         case 4:

            r0 = (src[11] << 24) + (src[10] << 16);
            r1 = (src[14] << 16) + (src[13] << 8) + src[12];

            *RY = (r1) & 0x3ff;
            *GU = (r0 >> 20) & 0x3ff;
            *BV = (r1 >> 10) & 0x3ff;

            break;

         case 5:

            r0 = (src[11] << 24) + (src[10] << 16);
            r1 = (src[15] << 24) + (src[14] << 16) + (src[13] << 8);

            *RY = (r1 >> 20) & 0x3ff;
            *GU = (r0 >> 20) & 0x3ff;
            *BV = (r1 >> 10) & 0x3ff;

            break;
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + ((x / 6) << 4);

         switch ((x % 6))
         { // phase

         case 0:

            r0 = *((int *)src);

            *RY = ((r0 >> 6) & 0x3fc) + ((r0 >> 26) & 0x03);
            *GU = ((r0 << 2) & 0x3fc) + ((r0 >> 28) & 0x03);
            *BV = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03);

            break;

         case 1:

            r0 = *((int *)src);
            r1 = *((int *)src + 4);

            *RY = ((r1 << 2) & 0x3fc) + ((r1 >> 28) & 0x03);
            *GU = ((r0 << 2) & 0x3fc) + ((r0 >> 28) & 0x03);
            *BV = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03);

            break;

         case 2:

            r0 = *((int *)src + 4);
            r1 = *((int *)src + 8);

            *RY = ((r0 >> 6) & 0x3fc) + ((r0 >> 26) & 0x03);
            *GU = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03);
            *BV = ((r1 << 2) & 0x3fc) + ((r1 >> 28) & 0x03);

            break;

         case 3:

            r0 = *((int *)src + 4);
            r1 = *((int *)src + 8);

            *RY = ((r1 >> 6) & 0x3fc);
            *GU = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03);
            *BV = ((r1 << 2) & 0x3fc) + ((r1 >> 28) & 0x03);

            break;

         case 4:

            r0 = *((int *)src + 8);
            r1 = *((int *)src + 12);

            *RY = ((r1 << 2) & 0x3fc) + ((r1 >> 28) & 0x03);
            *GU = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03);
            *BV = ((r1 >> 6) & 0x3fc) + ((r1 >> 26) & 0x03);

            break;

         case 5:

            r0 = *((int *)src + 8);
            r1 = *((int *)src + 12);

            *RY = ((r1 >> 14) & 0x3fc) + ((r1 >> 24) & 0x03);
            *GU = ((r0 >> 14) & 0x3fc) + ((r0 >> 24) & 0x03);
            *BV = ((r1 >> 6) & 0x3fc) + ((r1 >> 26) & 0x03);

            break;
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + ((x / 6) << 4);

         switch ((x % 6))
         { // phase

         case 0:

            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

            *RY = (r0 >> 12) & 0x3ff;
            *GU = (r0 >> 22) & 0x3ff;
            *BV = (r0 >> 2) & 0x3ff;

            break;

         case 1:

            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
            r1 = (src[4] << 24) + (src[5] << 16);

            *RY = (r1 >> 22) & 0x3ff;
            *GU = (r0 >> 22) & 0x3ff;
            *BV = (r0 >> 2) & 0x3ff;

            break;

         case 2:

            r0 = (src[5] << 16) + (src[6] << 8) + (src[7]);
            r1 = (src[8] << 24) + (src[9] << 16);

            *RY = (r0 >> 2) & 0x3ff;
            *GU = (r0 >> 12) & 0x3ff;
            *BV = (r1 >> 22) & 0x3ff;

            break;

         case 3:

            r0 = (src[5] << 16) + (src[6] << 8);
            r1 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8);

            *RY = (r1 >> 12) & 0x3ff;
            *GU = (r0 >> 12) & 0x3ff;
            *BV = (r1 >> 22) & 0x3ff;

            break;

         case 4:

            r0 = (src[10] << 8) + (src[11]);
            r1 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8);

            *RY = (r1 >> 22) & 0x3ff;
            *GU = (r0 >> 2) & 0x3ff;
            *BV = (r1 >> 12) & 0x3ff;

            break;

         case 5:

            r0 = (src[10] << 8) + (src[11]);
            r1 = (src[13] << 16) + (src[14] << 8) + (src[15]);

            *RY = (r1 >> 2) & 0x3ff;
            *GU = (r0 >> 2) & 0x3ff;
            *BV = (r1 >> 12) & 0x3ff;

            break;
         }

         break;

      default:
         retval = -1;
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_YUV444:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 3);

         *RY = src[1];
         *GU = src[0];
         *BV = src[2];

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x << 2);

         r0 = (src[3] << 24) + (src[2] << 16) + (src[1] << 8) + src[0];

         *RY = (r0 >> 10) & 0x3ff;
         *GU = (r0) & 0x3ff;
         *BV = (r0 >> 20) & 0x3ff;

         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_RGB:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 3);

         *RY = src[0];
         *GU = src[1];
         *BV = src[2];

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x << 2);

         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

         *RY = (r0 >> 22) & 0x3ff;
         *GU = (r0 >> 12) & 0x3ff;
         *BV = (r0 >> 2) & 0x3ff;

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x << 2);

         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

         *RY = (r0 >> 20) & 0x3ff;
         *GU = (r0 >> 10) & 0x3ff;
         *BV = (r0) & 0x3ff;

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 6);

         *RY = ((MTI_UINT16)(src[1] << 8) + src[0]) >> 4;
         *GU = ((MTI_UINT16)(src[3] << 8) + src[2]) >> 4;
         *BV = ((MTI_UINT16)(src[5] << 8) + src[4]) >> 4;

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x >> 3) * 36;

         switch (x % 8)
         {

         case 0:

            *RY = ((src[2] & 0x0f) << 8) + src[3];
            *GU = (src[1] << 4) + (src[5] >> 4);
            *BV = ((src[7] & 0x0f) << 8) + src[0];

            break;

         case 1:

            *RY = (src[6] << 4) + (src[7] >> 4);
            *GU = ((src[4] & 0x0f) << 8) + src[5];
            *BV = (src[11] << 4) + (src[4] >> 4);

            break;

         case 2:

            *RY = ((src[9] & 0x0f) << 8) + src[10];
            *GU = (src[8] << 4) + (src[9] >> 4);
            *BV = ((src[14] & 0x0f) << 8) + src[15];

            break;

         case 3:

            *RY = (src[13] << 4) + (src[14] >> 4);
            *GU = ((src[19] & 0x0f) << 8) + src[12];
            *BV = (src[18] << 4) + (src[19] >> 4);

            break;

         case 4:

            *RY = ((src[16] & 0x0f) << 8) + src[17];
            *GU = (src[23] << 4) + (src[16] >> 4);
            *BV = ((src[21] & 0x0f) << 8) + src[22];

            break;

         case 5:

            *RY = (src[20] << 4) + (src[21] >> 4);
            *GU = ((src[26] & 0x0f) << 8) + src[27];
            *BV = (src[25] << 4) + (src[26] >> 4);

            break;

         case 6:

            *RY = ((src[31] & 0x0f) << 8) + src[24];
            *GU = (src[30] << 4) + (src[31] >> 4);
            *BV = ((src[28] & 0x0f) << 8) + src[29];

            break;

         case 7:

            *RY = (src[35] << 4) + (src[28] >> 4);
            *GU = ((src[33] & 0x0f) << 8) + src[34];
            *BV = (src[32] << 4) + (src[33] >> 4);

            break;
         }

         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x >> 3) * 36;

         switch (x % 8)
         {

         case 0:

            *RY = (src[0] << 4) + (src[1] >> 4);
            *GU = ((src[1] & 0x0f) << 8) + src[2];
            *BV = (src[3] << 4) + (src[4] >> 4);

            break;

         case 1:

            *RY = ((src[4] & 0x0f) << 8) + src[5];
            *GU = (src[6] << 4) + (src[7] >> 4);
            *BV = ((src[7] & 0x0f) << 8) + src[8];

            break;

         case 2:

            *RY = (src[9] << 4) + (src[10] >> 4);
            *GU = ((src[10] & 0x0f) << 8) + src[11];
            *BV = (src[12] << 4) + (src[13] >> 4);

            break;

         case 3:

            *RY = ((src[13] & 0x0f) << 8) + src[14];
            *GU = (src[15] << 4) + (src[16] >> 4);
            *BV = ((src[16] & 0x0f) << 8) + src[17];

            break;

         case 4:

            *RY = (src[18] << 4) + (src[19] >> 4);
            *GU = ((src[19] & 0x0f) << 8) + src[20];
            *BV = (src[21] << 4) + (src[22] >> 4);

            break;

         case 5:

            *RY = ((src[22] & 0x0f) << 8) + src[23];
            *GU = (src[24] << 4) + (src[25] >> 4);
            *BV = ((src[25] & 0x0f) << 8) + src[26];

            break;

         case 6:

            *RY = (src[27] << 4) + (src[28] >> 4);
            *GU = ((src[28] & 0x0f) << 8) + src[29];
            *BV = (src[30] << 4) + (src[31] >> 4);

            break;

         case 7:

            *RY = ((src[31] & 0x0f) << 8) + src[32];
            *GU = (src[33] << 4) + (src[34] >> 4);
            *BV = ((src[34] & 0x0f) << 8) + src[35];

            break;
         }

         break;

      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 6);

         *RY = ((MTI_UINT16)(src[0] << 8) + src[1]) >> 4;
         *GU = ((MTI_UINT16)(src[2] << 8) + src[3]) >> 4;
         *BV = ((MTI_UINT16)(src[4] << 8) + src[5]) >> 4;

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 6);

         *RY = (src[1] << 8) + src[0];
         *GU = (src[3] << 8) + src[2];
         *BV = (src[5] << 8) + src[4];

         break;

         // Ugh fix me - apparently HALF format should be labeled BGR not RGB
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 6);

         *BV = (src[1] << 8) + src[0];
         *GU = (src[3] << 8) + src[2];
         *RY = (src[5] << 8) + src[4];

         break;

      default:
         retval = -1; // unsupported format
         break;

      }

      break;

   case IF_PIXEL_COMPONENTS_BGR:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_8Bits_IN_1Byte:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 3);

         *RY = src[2];
         *GU = src[1];
         *BV = src[0];

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x << 2);

         r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

         *BV = (r0 >> 22) & 0x3ff;
         *GU = (r0 >> 12) & 0x3ff;
         *RY = (r0 >> 2) & 0x3ff;

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 6);

         *BV = (src[1] << 8) + src[0];
         *GU = (src[3] << 8) + src[2];
         *RY = (src[5] << 8) + src[4];

         break;

      default:
         retval = -1; // unsupported format
         break;
      }

      break;

   case IF_PIXEL_COMPONENTS_RGBA:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + ((x / 3) << 4);

         switch (x % 3)
         {

         case 0:

            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];

            *RY = (r0 >> 22) & 0x3ff;
            *GU = (r0 >> 12) & 0x3ff;
            *BV = (r0 >> 2) & 0x3ff;

            break;

         case 1:

            r0 = (src[4] << 24) + (src[5] << 16) + (src[6] << 8) + src[7];
            r1 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];

            *RY = (r0 >> 12) & 0x3ff;
            *GU = (r0 >> 2) & 0x3ff;
            *BV = (r1 >> 22) & 0x3ff;

            break;

         case 2:

            r0 = (src[8] << 24) + (src[9] << 16) + (src[10] << 8) + src[11];
            r1 = (src[12] << 24) + (src[13] << 16) + (src[14] << 8) + src[15];

            *RY = (r0 >> 2) & 0x3ff;
            *GU = (r1 >> 22) & 0x3ff;
            *BV = (r1 >> 12) & 0x3ff;

            break;
         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 8);

         *RY = (src[1] << 8) + src[0];
         *GU = (src[3] << 8) + src[2];
         *BV = (src[5] << 8) + src[4];

         break;

      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:

         src = srcimg[y % frmFieldCount] + ((y / frmFieldCount) * frmPitch) + (x * 8);

         *RY = (src[7] << 8) + src[6];
         *GU = (src[5] << 8) + src[4];
         *BV = (src[3] << 8) + src[2];

         break;
      }
      break;

//   case IF_PIXEL_COMPONENTS_LUMINANCE:
   case IF_PIXEL_COMPONENTS_Y:
   case IF_PIXEL_COMPONENTS_YYY:

      switch (frmPelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:

         pel = y * frmWdth + x;
         src = srcimg[0] + ((pel / 3) * 4);

         switch ((pel % 3))
         {

         case 0:
            r0 = (src[2] << 8) + src[3];
            *RY = *GU = *BV = (r0 >> 2) & 0x3ff;
            break;

         case 1:
            r0 = (src[1] << 16) + (src[2] << 8);
            *RY = *GU = *BV = (r0 >> 12) & 0x3ff;
            break;

         case 2:
            r0 = (src[0] << 24) + (src[1] << 16);
            *RY = *GU = *BV = (r0 >> 22) & 0x3ff;
            break;
         }

         break;

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         pel = y * frmWdth + x;
         src = srcimg[0] + ((pel / 3) * 4);

         switch ((pel % 3))
         {

         case 0:
            r0 = (src[2] << 8) + src[3];
            *RY = *GU = *BV = r0 & 0x3ff;
            break;

         case 1:
            r0 = (src[1] << 16) + (src[2] << 8);
            *RY = *GU = *BV = (r0 >> 10) & 0x3ff;
            break;

         case 2:
            r0 = (src[0] << 24) + (src[1] << 16);
            *RY = *GU = *BV = (r0 >> 20) & 0x3ff;
            break;
         }

         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         src = srcimg[0] + y * frmPitch + (x / 4) * 5;

         switch ((x % 4))
         {

         case 0:
            r0 = (src[1] << 8) + (src[0]);
            *RY = *GU = *BV = r0 & 0x3ff;
            break;

         case 1:
            r0 = (src[2] << 6) + (src[1] >> 2);
            *RY = *GU = *BV = r0 & 0x3ff;
            break;

         case 2:

            r0 = (src[3] << 4) + (src[2] >> 4);
            *RY = *GU = *BV = r0 & 0x3ff;
            break;

         case 3:

            r0 = (src[4] << 2) + (src[3] >> 6);
            *RY = *GU = *BV = r0 & 0x3ff;
            break;
         }

         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // Byte-swapped to LE during I/O

         src = srcimg[0] + (y * frmPitch) + (x * 2);
         *RY = *GU = *BV = (src[1] << 8) + src[0];
         break;

      default:
         retval = -1;
         break;

      }

      break;

   default:
      retval = -1; // unsupported format
      break;

   }

   return (retval);
}
