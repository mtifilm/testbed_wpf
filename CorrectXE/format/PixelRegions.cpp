//
// PixelRegions.cpp: implementation of CPixelRegionList and CPixelRegion classes
//                   These classes hold and manage collections of pixels
//
/*
$Header: /usr/local/filmroot/format/source/PixelRegions.cpp,v 1.2.2.21 2009/10/26 19:58:54 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "PixelRegions.h"
#include "MTImalloc.h"
#include "Mask.h"
#include "LineEngine.h"
#include "IniFile.h"     // for TRACE_N()... gaaack
#include "ThreadLocker.h"
#include <stdlib.h>
#include <stdio.h>
#if defined(__sgi) || defined(__linux)
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#elif _WIN32
#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys\stat.h>
#include <windows.h>
#endif
#include "MTIio.h"
#include "MTIstringstream.h"

// turn on mbraca hacks
#define CONSOLIDATE_POINT_REGION_MALLOCS 1
#define THROW_BAD_ALLOC_EXCEPTION_ON_OUT_OF_MEMORY  1
//--------------------------------------------------------------------

namespace {

bool isRunBogus(int frameWidth, int frameHeight,
                       int runX, int runY, int runLen)
{
   if (frameWidth == -1 && frameHeight == -1)
      return false;  // nothing to check against

   // Simple check of pixel run sanity with respect to the frame dimensions!
   MTIassert(frameWidth < 10000);
   MTIassert(frameHeight < 7500);
   MTIassert(runX < frameWidth);
   MTIassert(runY < frameHeight);
   MTIassert(runLen <= (frameWidth - runX));

   if (frameWidth < 10000 && frameHeight < 7500 &&
       runX < frameWidth && runY < frameHeight &&
       runLen <= (frameWidth - runX)) {

      // MTIassert will already have output a TRACE
      return false;
   }

   return true;
};

} // end of anonymous namespace

int CPixelRegion::debugHackFrameWidth = -1;
int CPixelRegion::debugHackFrameHeight = -1;

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                          #####################################
// ###     CPixelChannel Class    ####################################
// ####                          #####################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPixelChannel::CPixelChannel()
{
   // no RES buffer yet
   resBufByts = 0;
   resBuf = 0;
   resBufStop = 0;

   // no buffer yet
   pxlBufByts = 0;
   pxlBuf = 0;
   pxlBufStop = 0;

   // channel file not open yet
   pxlFileHandle = -1;

   // file ptr is 0
   pxlFilePtr = 0;

   // no bytes to read
   pxlBytes = 0;

   // allocate buffer for edge records
   edgcnt = VEDGES_INIT;
   edgbuf = (VEDGE *)malloc(VEDGES_INIT*sizeof(VEDGE)); // Was MTImalloc
   if (edgbuf==NULL) edgcnt = 0;
}

CPixelChannel::~CPixelChannel()
{
   // QQQ should we automatically perform a close here?
   closePixelChannel();

   MTIassert(pxlFileHandle == -1);

   MTIfree(pxlBuf);

   MTIfree(resBuf);
}

int CPixelChannel::expandIndexSection()
{
   int bytcnt, result;

   // pxlSectionBeg => beg of RLE pixel data
   // pxlFilePtr    => end of RLE pixel data
   //
   MTI_INT64 blkBeg;
   MTI_INT64 blkEnd = pxlFilePtr;

   // move the pixel data one block at a time
   // using the PXL buffer - back to front
   //
   while (blkEnd > pxlSectionBeg) {

      blkBeg = blkEnd - pxlBufByts;

      if (blkBeg < pxlSectionBeg) {

         blkBeg = pxlSectionBeg;
      }

      bytcnt = blkEnd - blkBeg;

      if (lseek(pxlFileHandle, blkBeg, SEEK_SET) == -1)
         return(-1);

      result = read(pxlFileHandle, pxlBuf, bytcnt);
      if (result != bytcnt)
         return(-1);

      if (lseek(pxlFileHandle, (blkBeg + resBlkFctr * sizeof(FPF_RESTORE_RECORD)), SEEK_SET) == -1)
         return(-1);

      result = write(pxlFileHandle, pxlBuf, bytcnt);
      if (result != bytcnt)
         return(-1);

      blkEnd = blkBeg;
   }

   // both these advance to make room for new index block
   pxlSectionBeg += resBlkFctr * sizeof(FPF_RESTORE_RECORD);
   pxlFilePtr    += resBlkFctr * sizeof(FPF_RESTORE_RECORD);

   return 0;
}

// changed to return
//    -1               on failure
//    .PXL file handle on success
int CPixelChannel::openPixelChannel(int bufbyts,
                                    int pxlComponentCount,
                                    string& pxlFileName,
                                    bool useIndex)
{
   int bytcnt, result;

   // set the blocking factor for the optional index
   resBlkFctr = useIndex ? RES_BLK_FCTR : 0;
   resBufSize = resBlkFctr * sizeof(FPF_RESTORE_RECORD);

   // if size of buffer changes, re-allocate it
   int resbufbyts = resBufSize;
   if ((resbufbyts > 0)&&(resBufByts != resbufbyts)) {

      MTIfree(resBuf);
      resBuf = (FPF_RESTORE_RECORD *)MTImalloc(resbufbyts);
      resBufStop = resBuf + resBlkFctr;
      if (resBuf == NULL)
         return(-1);
      resBufByts = resbufbyts;
   }

   // save the name of the channel
   pixelFileName = pxlFileName;

   // the component count is now passed in
   pixelComponents = pxlComponentCount;

   // if size of buffer changes, re-allocate it
   bufbyts = (bufbyts+1)&0xfffffffe;
   if (bufbyts != pxlBufByts) {

      MTIfree(pxlBuf);
      pxlBuf = (MTI_UINT16 *)MTImalloc(bufbyts);
      if (pxlBuf == NULL)
         return(-1);
      pxlBufByts = bufbyts;
      pxlBufStop = pxlBuf + pxlBufByts/2;
   }

   // open the file
	if ((pxlFileHandle = open(pixelFileName.c_str(),
									  O_BINARY|O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1)
	{
		_errno = errno;
		if (pixelFileName != _oldHistoryName)
		{
			CAutoErrorReporter autoErr("CSaveRestore::openForRestore", AUTO_ERROR_NOTIFY_WAIT, AUTO_ERROR_TRACE_0);
			autoErr.errorCode = 100000 + errno;

         // Stupid strerror returns string with trailing newline...
         string errorMessage = strerror(errno);
         errorMessage = errorMessage.substr(0, errorMessage.length() - 1);
			autoErr.msg << "HISTORY CANNOT BE SAVED! " << endl  << endl
					<< "Error opening file " << GetFileNameWithExt(pixelFileName) << endl
               << "in folder: " << GetFilePath(pixelFileName) << endl
					<< "Reason: " << errorMessage << " (" << (100000 + errno) << ")" << endl << endl
               << "The file may be temporarily inaccessible or the" << endl
               << "historyDirectory setting in the media folder's" << endl
               << "MTImetadata.ini file may be incorrect.";
		}

		_oldHistoryName = pixelFileName;
		return -1;
	}

	_oldHistoryName = pixelFileName;

   // important to zero this for MONOLITHIC history type
   pxlSectionBeg = 0;

   // advance to set up for appending
   pxlFilePtr = lseek64(pxlFileHandle, 0, SEEK_END);
   if (pxlFilePtr == -1)
	{
		_errno = errno;
		return -1;
	}

   if (resBlkFctr > 0) { // we have an index

      if (pxlFilePtr == 0) { // brand new file

         resRecCount = 0;

         // file addr current block of RES records
         resFilePtr = sizeof(int);

         // file addr of beg of pixel section
         pxlSectionBeg = resFilePtr + resBufSize;

         // based on resRecCount set ptr to first new RES record
         resRecPtr = resBuf;

         // zero out the block of RES records
         MTImemset(resBuf, 0, resBufSize);

         // set up for appending pixel data
         pxlFilePtr = lseek64(pxlFileHandle, pxlSectionBeg, SEEK_SET);
			if (pxlFilePtr == -1)
			{
				_errno = errno;
				return -1;
			}

      }
      else if (pxlFilePtr >= (resBlkFctr*sizeof(FPF_RESTORE_RECORD) + sizeof(int))) {

         // read resRecCount from first 4 bytes
			if (lseek64(pxlFileHandle, 0, SEEK_SET) == -1)
			{
				_errno = errno;
				return -1;
			}

         result = read(pxlFileHandle, &resRecCount, sizeof(int));
         if (result != (int)sizeof(int))
			{
				_errno = errno;
				return -1;
			}

         // file addr current block of RES records
         resFilePtr = (resRecCount / resBlkFctr) * resBufSize + sizeof(int);

         // file addr of beg of pixel section
         pxlSectionBeg = resFilePtr + resBufSize;

         // based on resRecCount set ptr to first new RES record
         resRecPtr = resBuf + (resRecCount % resBlkFctr);
         if (resRecPtr == resBuf) {

            // zero out the block of RES records
            MTImemset(resBuf, 0, resBufSize);
         }
         else {

            // read block containing first new RES record
				if (lseek64(pxlFileHandle, resFilePtr, SEEK_SET) == -1)
				{
					_errno = errno;
					return -1;
				}

				bytcnt = resBufSize;
				result = read(pxlFileHandle, resBuf, bytcnt);
				if (result != bytcnt)
				{
					_errno = errno;
					return -1;
				}
			}

//   TRACE_2(errout << "=========openPixelChannel FPF RES REC=========" << endl);
//   TRACE_2(dumpFPFRestoreRecord(resBuf));
//   TRACE_2(errout << "==============================================" << endl);

         // no pixel data is saved for this RES record yet
         resRecPtr->resRec.pxlByteCount = 0;

         // advance to set up for appending pixel data
         pxlFilePtr = lseek64(pxlFileHandle, 0, SEEK_END);
			if (pxlFilePtr == -1)
			{
				_errno = errno;
				return -1;
			}
      }

      // gets set if block of RES record is modified
      resWriteBackFlag = false;
   }

   // get ready to receive pixel data
   pxlFillPtr = pxlBuf;

	// success
   _errno = 0;
   return pxlFileHandle;
}

FPF_RESTORE_RECORD * CPixelChannel::getRestoreRecord()
{
   if ((pxlFileHandle > 0)&&(resBlkFctr > 0))
      return resRecPtr;

   return(NULL);
}

int CPixelChannel::nextRestoreRecord()
{
   int result, bytcnt;

   while(true) {

      resRecPtr--;
      if (resRecPtr < resBuf) {

         if (resWriteBackFlag) {

            if (lseek(pxlFileHandle, resFilePtr, SEEK_SET) == -1)
               return(-1);

            bytcnt = resBufSize;
            result = write(pxlFileHandle, resBuf, bytcnt);
            if (result != bytcnt)
               return(-1);

            resWriteBackFlag = false;
         }

         if (resFilePtr == sizeof(int)) // addr of first block of RES records
            return(-1);

         resFilePtr -= resBufSize;

         if (lseek(pxlFileHandle, resFilePtr, SEEK_SET) == -1)
            return(-1);

         if (read(pxlFileHandle, resBuf, resBufSize) != resBufSize)
            return(-1);

         resRecPtr = resBuf + resBlkFctr - 1;
      }

      if (((resRecPtr->resRec.restoreFlag&IS_INVALID) == 0)&&
           (resRecPtr->resRec.pxlByteCount > 0)) // => good RES record
         return(0);
   }
}

void CPixelChannel::setRestoreWritebackFlag(bool val)
{
   resWriteBackFlag = val;
}

int CPixelChannel::flushSaves()
{
   return 0;
}

int CPixelChannel::setPixelChannelFilePtr(MTI_INT64 newfileptr)
{
   if (pxlFileHandle < 0)
      return -1;

   pxlFilePtr = lseek64(pxlFileHandle, newfileptr, SEEK_SET);

   if (pxlFilePtr != newfileptr) {
      TRACE_0(errout << "ERROR: CPixelChannel::setPixelChannelFilePtr: "
                     << "Unable to set file ptr (" << pxlFilePtr << " != "
                     << newfileptr << ")");
      MTIassert(pxlFilePtr == newfileptr);
      return -1;
   }
   return 0;
}

MTI_INT64 CPixelChannel::getPixelChannelFilePtr()
{
   return(pxlFilePtr);
}

int CPixelChannel::openPixelChannelForRestore(MTI_INT64 chaddr, int chbytes)
{
   int bytcnt, result;

   MTIassert(pxlFileHandle >= 0);

   // the current file ptr for this .RES record
   pxlBegFilePtr = chaddr + pxlSectionBeg;

   // the total bytes to process for this .RES record
   pxlBytes = chbytes;

   // end file ptr for this .RES record
   pxlEndFilePtr = pxlBegFilePtr + pxlBytes;

   // read data from .PXL file
   bytcnt = pxlBytes;
   if (bytcnt > pxlBufByts)
      bytcnt = pxlBufByts;

   if (lseek64(pxlFileHandle, pxlBegFilePtr, SEEK_SET) == -1) {

      TRACE_0(errout << "ERROR: History PXL file seek failed because "
                     << strerror(errno) << " (" << (100000 + errno) << ")");
      return(-1);
   }

   result = read(pxlFileHandle, pxlBuf, bytcnt);
   if (result != bytcnt) {

      TRACE_0(errout << "ERROR: History PXL file read failed because "
                     << strerror(errno) << " (" << (100000 + errno) << ")");
      return(-1);
   }
   if (result != bytcnt) {

      // Technically, this is legal, in practice on Windows it seems to not happen
      TRACE_0(errout << "ERROR: History PXL file short read (expected  "
                     << bytcnt << ", got " << result << ")");
      return(-1);
   }

   // init the  ptrs
   rgnBegFilePtr = rgnEndFilePtr = pxlBegFilePtr;

   return(0);
}

MTI_UINT16* CPixelChannel::getNextPixelRegion()
{
   int bytcnt, result;

   MTIassert(pxlFileHandle >= 0);

   rgnBegFilePtr = rgnEndFilePtr;

   if (rgnBegFilePtr >= pxlEndFilePtr) // no more pixel regions
      return(NULL);

   MTI_UINT16 *rgnPtr = pxlBuf + (int)(rgnBegFilePtr - pxlBegFilePtr)/2;

   if ((rgnPtr + (11 + MAX_LASSO_PTS*2)) >= pxlBufStop) { // realign

      // read data from .PXL file
      MTIassert(rgnBegFilePtr < pxlEndFilePtr);
      int bytes = pxlEndFilePtr - rgnBegFilePtr;
      if (bytes > pxlBufByts)
         bytes = pxlBufByts;

      // put the pixel region at the beg
      pxlBegFilePtr = rgnBegFilePtr;
      if (lseek64(pxlFileHandle, pxlBegFilePtr, SEEK_SET) == -1) {

         TRACE_0(errout << "ERROR: History PXL file seek failed because "
                        << strerror(errno) << " (" << (100000 + errno) << ")");
         MTIassert(result != -1);
         return(NULL);
      }
      result = read(pxlFileHandle, pxlBuf, bytes);
      if (result != bytes) {

         TRACE_0(errout << "ERROR: History PXL file read failed because "
                        << strerror(errno) << " (" << (100000 + errno) << ")");
         MTIassert(result != -1);
         return(NULL);
      }
      if (result != bytes) {

      // Technically, this is legal, in practice on Windows it seems to not happen
         TRACE_0(errout << "ERROR: History PXL file short read (expected  "
                        << bytes << ", got " << result << ")");
         MTIassert(result != bytes);
         return(NULL);
      }

      rgnPtr = pxlBuf;
   }

   int offset; // offset in shorts past header and vertex array

   // calculate the new value of rgnEndFilePtr and runEndFilePtr
   switch(*rgnPtr) {

      case PIXEL_REGION_TYPE_POINT: // TTT check this again!

         rgnEndFilePtr = rgnBegFilePtr +
                         (4 + pixelComponents)*sizeof(MTI_UINT16);

         runEndFilePtr = rgnBegFilePtr +
                         1*sizeof(MTI_UINT16);
         break;

      case PIXEL_REGION_TYPE_RECT:

         rgnEndFilePtr = rgnBegFilePtr +
                         (7 + (rgnPtr[6]<<16)+rgnPtr[5])*sizeof(MTI_UINT16);

         runEndFilePtr = rgnBegFilePtr +
                         7*sizeof(MTI_UINT16);
         break;

      case PIXEL_REGION_TYPE_MASK:

         rgnEndFilePtr = rgnBegFilePtr +
                         (7 + (rgnPtr[6]<<16)+rgnPtr[5])*sizeof(MTI_UINT16);

         runEndFilePtr = rgnBegFilePtr +
                         7*sizeof(MTI_UINT16);
         break;

      case PIXEL_REGION_TYPE_LASSO:

         offset = 6 + rgnPtr[5]*2; // 2 16-bit coords per vertex

         rgnEndFilePtr = rgnBegFilePtr +
                         (offset + 2 + (rgnPtr[offset+1]<<16)+rgnPtr[offset])
                         *sizeof(MTI_UINT16);

         runEndFilePtr = rgnBegFilePtr +
                         (offset + 2)*sizeof(MTI_UINT16);

         break;

      case PIXEL_REGION_TYPE_BEZIER:

         offset = 6 + rgnPtr[5]*6; // 6 16-bit coords per vertex

         rgnEndFilePtr = rgnBegFilePtr +
                         (offset + 2 + (rgnPtr[offset+1]<<16)+rgnPtr[offset])
                         *sizeof(MTI_UINT16);

         runEndFilePtr = rgnBegFilePtr +
                         (offset + 2)*sizeof(MTI_UINT16);

         break;

      default:

         TRACE_0(errout << "ERROR: Clip history is corrupt: "
                        << "unknown pixel region type " << (*rgnPtr));
         rgnPtr = NULL; // Prevent infinite loop on corrupt history file

         break;
   }

   return(rgnPtr);
}

MTI_UINT16* CPixelChannel::getNextRun()
{
   int bytcnt, result;

   MTIassert(pxlFileHandle >= 0);

   // Well wtf. This happens a lot when I GOV paint stuff,
   // so I'm nuking the assert
   //MTIassert(runEndFilePtr <= rgnEndFilePtr);
   runBegFilePtr = runEndFilePtr;

   if (runBegFilePtr == rgnEndFilePtr) // no more runs
      return(NULL);

   // I added this when I nuked the assert... it doesn't seem to be a
   // problem to bail out here if things are wonky, but I left a TRACE
   // in to remind myself to look at it someday
   if (runBegFilePtr > rgnEndFilePtr)
   {
      // hmmm this can't be good
      TRACE_1(errout << "INTERNAL ERROR in CPixelChannel::getNextRun() IGNORED");
      return NULL;
   }

   MTI_UINT16 *runPtr = pxlBuf + (int)(runBegFilePtr - pxlBegFilePtr)/2;

   // Haha WTF is this??? Can runPtr[2] be NEGATIVE??? QQQ
   // It's KTF - if the first three wds of the run aren't
   // within the buffer, then runPtr[2] is meaningless and
   // mustn't be accessed!!
   if (((runPtr + 3) >= pxlBufStop)||
       ((runPtr + 3 + runPtr[2]*pixelComponents) >= pxlBufStop)) {

      // read data from .PXL file
      int bytes = pxlEndFilePtr - runBegFilePtr;
      if (bytes > pxlBufByts)
         bytes = pxlBufByts;

      // put the pixel region at the beg
      pxlBegFilePtr = runBegFilePtr;
      if (lseek64(pxlFileHandle, pxlBegFilePtr, SEEK_SET) == -1) {

         TRACE_0(errout << "ERROR: History PXL file seek failed because "
                        << strerror(errno) << " (" << (100000 + errno) << ")");
         return(NULL);
      }
      result = read(pxlFileHandle, pxlBuf, bytes);
      if (result != bytes) {

         TRACE_0(errout << "ERROR: History PXL file read failed because "
                        << strerror(errno) << " (" << (100000 + errno) << ")");
         return(NULL);
      }
      if (result != bytes) {

         // Technically, this is legal, in practice on Windows it seems to not happen
         TRACE_0(errout << "ERROR: History PXL file short read (expected  "
                        << bytes << ", got " << result << ")");
         return(NULL);
      }

      runPtr = pxlBuf;
   }

   runEndFilePtr = runBegFilePtr + (3 + runPtr[2]*pixelComponents)*sizeof(MTI_UINT16);
   // Well wtf. This happens a lot when I GOV paint stuff,
   // so I'm nuking the assert... doesn't seem to be a problem to ignore it
   //MTIassert(runEndFilePtr <= rgnEndFilePtr);
   return(runPtr);
}

int CPixelChannel::putDifferenceRunsToChannel(MTI_UINT16 *iniFrame,
                                              MTI_UINT16 *finFrame,
                                              MTI_UINT16 *srcFrame,
                                              int frmWdth, int frmHght,
                                              bool evn, bool odd,
                                              RECT *ext)
{
   int result, bytcnt;

   // buffer for collecting one run at a time
   MTI_UINT16 runBuf[32768];

   // open for putting runs to channel
   pxlFillPtr = pxlBuf;

   // first pass derives the extent

   const MTI_UINT16 *iniptr = iniFrame;
   const MTI_UINT16 *finptr = finFrame;
   const MTI_UINT16 *srcptr;

   // capture extent of runs here
   RECT rect;

   // init to capture exent
   rect.left   = frmWdth;
   rect.top    = frmHght;
   rect.right  = 0;
   rect.bottom = 0;

   int runwds = 0;

   for (int i=0;i<frmHght;i++) {

      bool oddLine = i&1;
      if ((!oddLine||!odd)&&(oddLine||!evn)) {
         iniptr += frmWdth*pixelComponents;
         finptr += frmWdth*pixelComponents;
         continue;
      }

      bool phase = false;

      for (int j=0;j<frmWdth;j++) {

         bool diff = false;
         for (int k=0;k<pixelComponents;k++) {
            diff = (finptr[k] != iniptr[k]);
            if (diff)
               break;
         }

         if (diff) {

            // found a differing pixel;

            if (!phase) {

               // init new run

               runBuf[0] = j;
               runBuf[1] = i;
               runBuf[2] = 0;

               phase = true;

            }
            if (phase) {

               // continue run

               runBuf[2]++;
            }
         }
         else {

            // found no difference

            if (phase) { // doing a run

               // completed run

               if (runBuf[0] < rect.left)
                  rect.left = runBuf[0];
               if (i < rect.top)
                  rect.top = i;
               if ((j-1) > rect.right)
                  rect.right = (j-1);
               if (i > rect.bottom)
                  rect.bottom = i;

               runwds += 3 + runBuf[2]*pixelComponents;

               // init for next run
               phase = false;
            }
         }

         finptr += pixelComponents;
         iniptr += pixelComponents;
      }

      if (phase) { // end of row

         // completed run

         if (runBuf[0] < rect.left)
            rect.left = runBuf[0];
         if (i < rect.top)
            rect.top = i;
         rect.right = frmWdth - 1;
         if (i > rect.bottom)
            rect.bottom = i;
         runwds += 3 + runBuf[2]*pixelComponents;
      }
   }

   if (ext != 0) { // return the extent to caller

      *ext = rect;
   }

   if (resBlkFctr > 0) {  // we have an index

      // put the extent in the RES record
      resRecPtr->resRec.extent = rect;

      // save the (relative) addr of the pixel data
      resRecPtr->resRec.pxlFileByteOffset = pxlFilePtr - pxlSectionBeg;

      // save the byte count of the data to be saved
      resRecPtr->resRec.pxlByteCount = (7 + runwds) * 2;

   }

   // write out 7-word region header
   // enter the type header
   *(pxlFillPtr)++ = PIXEL_REGION_TYPE_MASK;

   // serialize the rect
   *(MTI_INT16 *)(pxlFillPtr)++ = rect.left;
   *(MTI_INT16 *)(pxlFillPtr)++ = rect.top;
   *(MTI_INT16 *)(pxlFillPtr)++ = rect.right;
   *(MTI_INT16 *)(pxlFillPtr)++ = rect.bottom;

   // record total length of runs
   *(pxlFillPtr)++ = runwds&0xffff; // lo
   *(pxlFillPtr)++ = runwds>>16;    // hi

   runwds += 7;

   MTI_UINT16 *dstPtr;

   int offset = (frmWdth*rect.top + rect.left)*pixelComponents;
   iniptr = iniFrame + offset;
   finptr = finFrame + offset;
   srcptr = srcFrame + offset;

   int rowadv = (frmWdth - (rect.right - rect.left + 1))*pixelComponents;

   for (int i=rect.top;i<=rect.bottom;i++) {

      bool oddLine = i&1;
      if ((!oddLine||!odd)&&(oddLine||!evn)) {
         iniptr += frmWdth*pixelComponents;
         finptr += frmWdth*pixelComponents;
         srcptr += frmWdth*pixelComponents;
         continue;
      }

      bool phase = false;

      for (int j=rect.left;j<=rect.right;j++) {

         bool diff = false;
         for (int k=0;k<pixelComponents;k++) {
            diff = (finptr[k] != iniptr[k]);
            if (diff)
               break;
         }

         if (diff) {

            // found a differing pixel;

            if (!phase) {

               // init new run

               runBuf[0] = j;
               runBuf[1] = i;
               runBuf[2] = 0;

               dstPtr = runBuf + 3;

               phase = true;

            }
            if (phase) {

               // add pixel to current run

               for (int k=0;k<pixelComponents;k++)
                  *dstPtr++ = srcptr[k];

               runBuf[2]++;
            }
         }
         else {

            if (phase) {

               // completed run - add it to the channel

               int wds = 3 + runBuf[2] * pixelComponents;

               if ((pxlFillPtr + wds) >= pxlBufStop) { // no room in buffer

                  // write out current runs to channel file
                  bytcnt = (pxlFillPtr - pxlBuf)*2;
                  result = write(pxlFileHandle,(MTI_UINT8 *)pxlBuf, bytcnt);
                  if (result != bytcnt)
                     return(-1);
                  pxlFilePtr += bytcnt;
                  pxlFillPtr = pxlBuf;
               }

               MTImemcpy((void *)pxlFillPtr, (void *)runBuf, sizeof(MTI_UINT16)*wds);
               pxlFillPtr += wds;

               // init for next run
               phase = false;
            }
         }

         iniptr += pixelComponents;
         finptr += pixelComponents;
         srcptr += pixelComponents;
      }

      if (phase) {

         // completed run at end of row

         int wds = 3 + runBuf[2] * pixelComponents;

         if ((pxlFillPtr + wds) >= pxlBufStop) { // no room in buffer

            // write out current runs to channel file
            bytcnt = (pxlFillPtr - pxlBuf)*2;
            result = write(pxlFileHandle,(MTI_UINT8 *)pxlBuf, bytcnt);
            if (result != bytcnt)
               return(-1);
            pxlFilePtr += bytcnt;
            pxlFillPtr = pxlBuf;
         }

         MTImemcpy((void *)pxlFillPtr, (void *)runBuf, sizeof(MTI_UINT16)*wds);
         pxlFillPtr += wds;
      }

      iniptr += rowadv;
      finptr += rowadv;
      srcptr += rowadv;
   }

   // write out last buffer of runs to channel file
   bytcnt = (pxlFillPtr - pxlBuf)*2;
   result = write(pxlFileHandle,(MTI_UINT8 *)pxlBuf, bytcnt);
   if (result != bytcnt)
      return(-1);
   pxlFilePtr += bytcnt;
   pxlFillPtr = pxlBuf;

   if (resBlkFctr > 0) { // done with RES record

      // in file-per-frame history global save numbers are no longer
      // global but are simply the index of the fix in the .hst file
      resRecPtr->resRec.globalSaveNumber = resRecCount;

      resRecCount++;

      resRecPtr++;

      resWriteBackFlag = true;

      if (resRecPtr == resBufStop) {

         // write out cur block of RES records
         if (lseek64(pxlFileHandle, resFilePtr, SEEK_SET) == -1)
            return(-1);

         bytcnt = resBufSize;
         result = write(pxlFileHandle, resBuf, bytcnt);
         if (result != bytcnt)
            return(-1);

         // move pixel data down
         expandIndexSection();

         // base addr of new block
         resFilePtr += resBufSize;

         // seek to end of pixel data
         if (lseek64(pxlFileHandle, pxlFilePtr, SEEK_SET) == -1)
            return(-1);

         resRecPtr = resBuf;
      }
   }

   return(runwds * 2) ;
}

// now we save only difference runs within a closed "blob outline"
int CPixelChannel::putDifferenceRunsToChannel(MTI_UINT16 *iniFrame,
                                              MTI_UINT16 *finFrame,
                                              MTI_UINT16 *srcFrame,
                                              vector <POINT> *blobOutl,
                                              int frmWdth, int frmHght,
                                              bool evn, bool odd,
                                              RECT *ext)
{
   int result, bytcnt;

   // buffer for collecting one run at a time
   MTI_UINT16 runBuf[32768];

   // open for putting runs to channel
   pxlFillPtr = pxlBuf;

   // capture extent of runs here
   RECT rect;

   // init to capture exent
   rect.left   = frmWdth;
   rect.top    = frmHght;
   rect.right  = 0;
   rect.bottom = 0;

   // capture wds in runs
   int runwds = 0;

   if (resBlkFctr > 0) { // we have an index

      // save the (relative) addr of the pixel data
      resRecPtr->resRec.pxlFileByteOffset = pxlFilePtr - pxlSectionBeg;
   }

   // this is the address in the body of the channel
   // where region hdr, 4 wds of extent and 2 of total
   // run wds will be written - AFTER they're derived
   MTI_INT64 pxlExtentFilePtr = pxlFilePtr;

   // write out 7-word region header, extent, and runwds
   // as DUMMIES until the data is derived from the scan
   *pxlFillPtr++ = PIXEL_REGION_TYPE_MASK;

   // serialize the rect - this addr in the
   // body of the file is pxlExtentFilePtr
   *pxlFillPtr++ = rect.left;
   *pxlFillPtr++ = rect.top;
   *pxlFillPtr++ = rect.right;
   *pxlFillPtr++ = rect.bottom;

   // record total length of runs
   *pxlFillPtr++ = runwds&0xffff; // lo
   *pxlFillPtr++ = runwds>>16;    // hi

   // if no blob outline is supplied, do ENTIRE FRAME
   if (blobOutl == NULL) {

      initRunGeneration(4);

      moveTo(0, 0);
      lineTo(frmWdth, 0);
      lineTo(frmWdth, frmHght);
      lineTo(0, frmHght);
      lineTo(0, 0);
   }
   else { // blob supplied - get the VERTICAL edges

      int npts = blobOutl->size();

      initRunGeneration(npts);

      moveTo((*blobOutl)[0].x, (*blobOutl)[0].y);
      for (int i=1; i < npts; i++) {
         lineTo((*blobOutl)[i].x, (*blobOutl)[i].y);
      }
   }

   // scan the blob - derive 1) extent and 2) runwds

   /////////////////////////////////////////////////////////////////////////////

   VEDGINDEX icuredg, ipreedg, inxtedg;
   int rowcov, lft, rgt;

   if (iedgptr > 0) { // we have edges in the buffer

      //     S O R T   E D G E S

      // the edges in scan order (increasing y = down)
      ilexlst = iedgeSort(0,iedgptr-1);

      // the edges crossing a scan-line
      iedglst = NULLVEDG;

      do {  //     M A I N   L O O P  for scan-lines

         // edge list is empty, advance the scan to next edge
         if (iedglst==NULLVEDG) {

            // set CP to left of frame's row
            curx = 0;
            cury = edgbuf[ilexlst].ybot;
         }

         //     I N S E R T   P H A S E

         // pull in all the edges which have lower y at this scan-line and
         // insert them into the edge-list in their proper order
         while ((ilexlst!=NULLVEDG)&&(edgbuf[ilexlst].ybot==cury)) {

            // the edge is vertical. Insert into edge-list
            ipreedg = NULLVEDG;
            icuredg = iedglst;
            while ((icuredg!=NULLVEDG)&&(edgbuf[ilexlst].xbot>edgbuf[icuredg].xbot)) {
               ipreedg = icuredg;
               icuredg = edgbuf[icuredg].ifwd;
            }
            insertEdge(ilexlst,ipreedg,icuredg);

            // advance to the next edge in y-order
            ilexlst = edgbuf[ilexlst].inxtlex;
         }

         //     D R A W   P H A S E

         // beginning with left edge of the blob, trace the edge-list
         // in x-order drawing the hatch lines when the covering is 1
         //
         bool oddLine = cury&1;
         if ((oddLine&odd)||(!oddLine&evn)) {

            rowcov = 0;
            lft = 0;
            icuredg = iedglst;
            while (icuredg!=NULLVEDG) {

               rgt = edgbuf[icuredg].xbot;

               if (rowcov) {

                  // lft incl, rgt excl

                  int offset = (cury*frmWdth + lft)*pixelComponents;
                  MTI_UINT16 *iniptr = iniFrame + offset;
                  MTI_UINT16 *finptr = finFrame + offset;
                  MTI_UINT16 *srcptr = srcFrame + offset;

                  MTI_UINT16 *dstPtr;

                  bool phase = false;

                  for (int j=lft; j<rgt; j++) {

                     bool diff = false;
                     for (int k=0;k<pixelComponents;k++) {
                        diff = (finptr[k] != iniptr[k]);
                        if (diff)
                           break;
                     }

                     if (diff) {

                        // found a differing pixel;

                        if (!phase) {

                           // init new run

                           runBuf[0] = j;
                           runBuf[1] = cury;
                           runBuf[2] = 0;

                           dstPtr = runBuf + 3;

                           phase = true;

                        }
                        if (phase) {

                           // add pixel to current run

                           for (int k=0;k<pixelComponents;k++)
                              *dstPtr++ = srcptr[k];

                           runBuf[2]++;
                        }
                     }
                     else {

                        if (phase) {

                           // completed run - include in runs extent
                           if (runBuf[0] < rect.left)
                              rect.left = runBuf[0];
                           if (runBuf[1] < rect.top)
                              rect.top = runBuf[1];
                           if ((runBuf[0]+runBuf[2]-1) > rect.right)
                              rect.right = (runBuf[0]+runBuf[2]-1);
                           if (runBuf[1] > rect.bottom)
                              rect.bottom = runBuf[1];

                           int wds = 3 + runBuf[2] * pixelComponents;

                           if ((pxlFillPtr + wds) >= pxlBufStop) { // no room in buffer

                              // write out current runs to channel file
                              bytcnt = (pxlFillPtr - pxlBuf)*2;
                              result = write(pxlFileHandle,(MTI_UINT8 *)pxlBuf, bytcnt);
                              if (result != bytcnt)
                                 return(-1);
                              pxlFilePtr += bytcnt;
                              pxlFillPtr = pxlBuf;
                           }

                           MTImemcpy((void *)pxlFillPtr, (void *)runBuf, sizeof(MTI_UINT16)*wds);
                           pxlFillPtr += wds;

                           // count run wds
                           runwds += wds;

                           // init for next run
                           phase = false;
                        }
                     }

                     iniptr += pixelComponents;
                     finptr += pixelComponents;
                     srcptr += pixelComponents;
                  }

                  if (phase) {

                     // completed run - include in runs extent
                     if (runBuf[0] < rect.left)
                        rect.left = runBuf[0];
                     if (runBuf[1] < rect.top)
                        rect.top = runBuf[1];
                     if ((runBuf[0]+runBuf[2]-1) > rect.right)
                        rect.right = (runBuf[0]+runBuf[2]-1);
                     if (runBuf[1] > rect.bottom)
                        rect.bottom = runBuf[1];

                     int wds = 3 + runBuf[2] * pixelComponents;

                     if ((pxlFillPtr + wds) >= pxlBufStop) { // no room in buffer

                        // write out current runs to channel file
                        bytcnt = (pxlFillPtr - pxlBuf)*2;
                        result = write(pxlFileHandle,(MTI_UINT8 *)pxlBuf, bytcnt);
                        if (result != bytcnt)
                           return(-1);
                        pxlFilePtr += bytcnt;
                        pxlFillPtr = pxlBuf;
                     }

                     MTImemcpy((void *)pxlFillPtr, (void *)runBuf, sizeof(MTI_UINT16)*wds);
                     pxlFillPtr += wds;

                     // count run wds
                     runwds += wds;
                  }
               }

               lft = rgt;

               rowcov = 1 - rowcov;

               // next edge of scan-lst
               icuredg = edgbuf[icuredg].ifwd;
            }

            // should always end with rowcov = 0;
            //if (rowcov) hatchLine(lft,region.right);
         }

         // if we pass bottom of frame, we're all done
         if (++cury > (frmHght-1)) iedglst = ilexlst = NULLVEDG;

         //     U P D A T E   P H A S E
         //
         // Go through the edge list and remove all
         // edges whose ytop equals cury of the scan

         icuredg = iedglst;
         while (icuredg!=NULLVEDG) {

            inxtedg = edgbuf[icuredg].ifwd;

            if (edgbuf[icuredg].ytop==cury) {
               deleteEdge(icuredg);
            }

            icuredg = inxtedg;
         }

      } while ((ilexlst!=NULLVEDG)||(iedglst!=NULLVEDG));
   }

   // write out last buffer of runs to channel file
   bytcnt = (pxlFillPtr - pxlBuf)*2;
   result = write(pxlFileHandle,(MTI_UINT8 *)pxlBuf, bytcnt);
   if (result != bytcnt)
      return(-1);
   pxlFilePtr += bytcnt;
   pxlFillPtr = pxlBuf;

   /////////////////////////////////////////////////////////////////////////////

   if (ext != 0) { // return the extent to caller

      *ext = rect;
   }

   if (resBlkFctr > 0) {  // we have an index

      // put the extent in the RES record
      resRecPtr->resRec.extent = rect;

      // save the (relative) addr of the pixel data
      //resRecPtr->pxlFileByteOffset = pxlFilePtr - pxlSectionBeg;

      // save the byte count of the data to be saved
      resRecPtr->resRec.pxlByteCount = (7 + runwds) * 2;

   }

   // THIS READ CHECK IS EXPENDABLE

   // set up for inserting header, extent, runwds
   pxlFilePtr = lseek64(pxlFileHandle, pxlExtentFilePtr, SEEK_SET);
   if (pxlFilePtr == -1)
      return(-1);

   result = read(pxlFileHandle, pxlBuf, 14);
   if (result != 14)
      return(-1);

   pxlFillPtr = pxlBuf;
   if (*pxlFillPtr != PIXEL_REGION_TYPE_MASK) {
      bytcnt = 100;
   }

   // THE WRITING OF THESE 7 WORDS IS OBLIGATORY

   // write out 7-word region header
   // enter the type header
   *pxlFillPtr++ = PIXEL_REGION_TYPE_MASK;

   // serialize the rect - this addr in the
   // body of the file is pxlExtentFilePtr
   *pxlFillPtr++ = rect.left;
   *pxlFillPtr++ = rect.top;
   *pxlFillPtr++ = rect.right;
   *pxlFillPtr++ = rect.bottom;

   // record total length of runs
   *pxlFillPtr++ = runwds&0xffff; // lo
   *pxlFillPtr++ = runwds>>16;    // hi

   pxlFilePtr = lseek64(pxlFileHandle, pxlExtentFilePtr, SEEK_SET);
   if (pxlFilePtr == -1)
      return(-1);

   // put out header, extent, and runwds data
   result = write(pxlFileHandle, pxlBuf, 14);
   if (result != 14)
      return(-1);

   runwds += 7;

   // get ready for next fix by seeking to end
   pxlFilePtr = lseek64(pxlFileHandle, 0, SEEK_END);
   if (pxlFilePtr == -1)
      return(-1);
   pxlFillPtr = pxlBuf;

   if (resBlkFctr > 0) { // done with RES record

      // put the extent in the RES record
      resRecPtr->resRec.extent = rect;

      // save the byte count of the data to be saved
      resRecPtr->resRec.pxlByteCount = runwds * 2;

      // in file-per-frame history global save numbers are no longer
      // global but are simply the index of the fix in the .hst file
      resRecPtr->resRec.globalSaveNumber = resRecCount;

      resRecCount++;

      resRecPtr++;

      resWriteBackFlag = true;

      if (resRecPtr == resBufStop) {

         // write out cur block of RES records
         if (lseek64(pxlFileHandle, resFilePtr, SEEK_SET) == -1)
            return(-1);

         bytcnt = resBufSize;
         result = write(pxlFileHandle, resBuf, bytcnt);
         if (result != bytcnt)
            return(-1);

         // move pixel data down
         expandIndexSection();

         // base addr of new block
         resFilePtr += resBufSize;

         // seek to end of pixel data
         if (lseek64(pxlFileHandle, pxlFilePtr, SEEK_SET) == -1)
            return(-1);

         resRecPtr = resBuf;
      }
   }

   return(runwds * 2) ;
}

int CPixelChannel::getDifferenceRunsFmChannel(int chaddr, int chbytes,
                                              MTI_UINT16 *trg,
                                              int frmWdth, int frmHght)
{
   if (openPixelChannelForRestore(chaddr, chbytes) == -1)
      return -1;

   if (getNextPixelRegion() == NULL)
      return -1;

   // the runs have no header (i.e., there is no "pixel region");
   //runEndFilePtr = chaddr;
   //rgnEndFilePtr = chaddr + chbytes;

   int totbyts = 0;

   MTI_UINT16 *runptr;
   while ((runptr = getNextRun()) != NULL) {

      int runwds = runptr[2]*pixelComponents;

      MTI_UINT16 *srcptr = runptr + 3;

      MTI_UINT16 *dstptr = trg + (runptr[1]*frmWdth + runptr[0])*
                           pixelComponents;

      MTImemcpy((void *)dstptr, (void *)srcptr, runwds*2);

      totbyts += 6 + runwds*2;
   }

   // totbyts must be = chbytes here

   return totbyts;
}

int CPixelChannel::discardNFixes(int numdscrd)
{
   if (resBlkFctr == 0)
      return -1;

   if (numdscrd < 0)
      return -1;

   if (numdscrd > resRecCount)
      return -1;

   MTI_INT64 newFileLength = 0;

   if (numdscrd < resRecCount) {

      while (numdscrd > 0) {

         if (nextRestoreRecord() == 0) {

            newFileLength = resRecPtr->resRec.pxlFileByteOffset;

            resRecPtr->resRec.pxlFileByteOffset = 0;

            resRecPtr->resRec.restoreFlag = IS_INVALID;

            setRestoreWritebackFlag(true);

            numdscrd--;
         }
         else { // error

            return -1;
         }
      }

      // detect dead history file, close, and delete if necessary
      if (closePixelChannel() < 0)
         return -1;
   }
   else {

      if (close(pxlFileHandle) < 0)
         return -1;

      pxlFileHandle = -1;  // Added JAM because a close should say the file handle is -1
      DeleteFile(pixelFileName.c_str());

      return 0;
   }

   return 0;
}

int CPixelChannel::closePixelChannel()
{
   int bytcnt, result;

   if (pxlFileHandle >= 0) {

      // check that pixel data is always flushed
      MTIassert(pxlFillPtr == pxlBuf);

      if (resBlkFctr > 0) { // using index

         if (resWriteBackFlag) {

            // go to channel addr of block of RES records
            if (lseek(pxlFileHandle, resFilePtr, SEEK_SET) == -1)
               return(-1);

            // write the entire block out
            if (write(pxlFileHandle, resBuf, resBufSize) != resBufSize)
               return(-1);

            // write 4 byte RES record count to first 4 bytes of file
            if (lseek(pxlFileHandle, 0, SEEK_SET) == -1)
               return(-1);

            if (write(pxlFileHandle, &resRecCount, sizeof(int)) != (int)sizeof(int))
               return(-1);

            resWriteBackFlag = false;
         }

         ///////////////////////////////////////////////////////////////////////

         // file addr of block containing last RES record
         MTI_UINT64 theFilePtr = (resRecCount / resBlkFctr) * resBufSize + sizeof(int);

         if (resFilePtr != theFilePtr) {

            // read block containing last RES record
            if (lseek64(pxlFileHandle, resFilePtr, SEEK_SET) == -1)
               return(-1);

            if (read(pxlFileHandle, resBuf, resBufSize) != resBufSize)
               return(-1);

            resFilePtr = theFilePtr;
         }

         // based on resRecCount set ptr to last RES record
         resRecPtr = resBuf + (resRecCount % resBlkFctr);

         MTI_UINT64 totPixelLength = 0;
         while (nextRestoreRecord() == 0) {

            totPixelLength = resRecPtr->resRec.pxlFileByteOffset +
                             resRecPtr->resRec.pxlByteCount;

            // we exit at the first RES record which is not INVALID
            break;
         }

         // close the current file
         if (close(pxlFileHandle) < 0)
            return -1;

         pxlFileHandle = -1;

         if (totPixelLength != 0) {  // file is still relevant

            totPixelLength += pxlSectionBeg;

            truncate64(pixelFileName.c_str(), totPixelLength);
         }
         else {                      // file can be deleted

            DeleteFile(pixelFileName.c_str());
         }

         return 0;

         ///////////////////////////////////////////////////////////////////////

      }

      if (close(pxlFileHandle) < 0)
         return -1;

      pxlFileHandle = -1;

      return 0;
   }

   return 0;
}

int CPixelChannel::closeAndTruncatePixelChannel()
{
   int retVal = closePixelChannel();
   if (retVal != 0) return retVal;
   return(truncate64(pixelFileName.c_str(), pxlFilePtr));
}

///////////////////  For Scanning Individual Blobs /////////////////////////////

int CPixelChannel::initRunGeneration(int edges)
{
   if (edges > edgcnt) { // edge-buffer not big enough, need to reallocate

      if (edgbuf!=NULL) free(edgbuf);          // Was MTIfree
      edgcnt = 0;
      edgbuf = (VEDGE *)malloc(edges*sizeof(VEDGE));  // was MTImalloc
      if (edgbuf == NULL) return ERR_INSUFF_MEMORY;
      edgcnt = edges;
   }

   iedgptr = 0;
   return 0;
}

void CPixelChannel::moveTo(int x, int y)
{
   curx = x;
   cury = y;
}

void CPixelChannel::lineTo(int x, int y)
{
   if (x == curx) {

      edgbuf[iedgptr].inxtlex = NULLVEDG;
      edgbuf[iedgptr].ifwd    = NULLVEDG;
      edgbuf[iedgptr].ibwd    = NULLVEDG;
      edgbuf[iedgptr].xbot = x;

      if (y < cury) { // going up

         edgbuf[iedgptr].ybot = y;
         edgbuf[iedgptr].ytop = cury;

         iedgptr++;
      }
      else if (y > cury) { // going dn

         edgbuf[iedgptr].ybot = cury;
         edgbuf[iedgptr].ytop = y;

         iedgptr++;
      }
   }

   curx = x;
   cury = y;
}

// edge-node sort using recursive algorithm
VEDGINDEX CPixelChannel::iedgeSort(VEDGINDEX iloptr, VEDGINDEX ihiptr)
{
  VEDGINDEX imidptr, ichn1, ichn2, itemp, ihead, itail;

  if (iloptr==ihiptr) { // only one node
    edgbuf[iloptr].inxtlex = NULLVEDG;
    return iloptr;
  }

  if ((ihiptr - iloptr)==1) { // only two nodes

    if ((edgbuf[iloptr].ybot<edgbuf[ihiptr].ybot)||
       ((edgbuf[iloptr].ybot==edgbuf[ihiptr].ybot)&&
       (edgbuf[iloptr].xbot<=edgbuf[ihiptr].xbot))) {

        edgbuf[iloptr].inxtlex = ihiptr;
        edgbuf[ihiptr].inxtlex = NULLVEDG;
        return iloptr;
    }
    else {
        edgbuf[ihiptr].inxtlex = iloptr;
        edgbuf[iloptr].inxtlex = NULLVEDG;
        return ihiptr;
    }
  }

  // more than two nodes: split into two halves and sort each separately
  imidptr = (iloptr + (ihiptr - iloptr)/2);
  ichn1 = iedgeSort(iloptr,imidptr);
  ichn2 = iedgeSort(imidptr+1,ihiptr);

  // define the head of the merged chain
  if ((edgbuf[ichn2].ybot<edgbuf[ichn1].ybot)||
     ((edgbuf[ichn2].ybot==edgbuf[ichn1].ybot)&&
     (edgbuf[ichn2].xbot<edgbuf[ichn1].xbot))) {
    itemp = ichn1;
    ichn1 = ichn2;
    ichn2 = itemp;
  }
  ihead = itail = ichn1;
  ichn1 = edgbuf[ichn1].inxtlex;

  // as above, CHN1 is always the chain that advances
  while (ichn1 != NULLVEDG) {
    if ((edgbuf[ichn2].ybot<edgbuf[ichn1].ybot)||
       ((edgbuf[ichn2].ybot==edgbuf[ichn1].ybot)&&
       (edgbuf[ichn2].xbot<edgbuf[ichn1].xbot))) {

      itemp = ichn1;
      ichn1 = ichn2;
      ichn2 = itemp;
    }
    edgbuf[itail].inxtlex = ichn1;
    itail = ichn1;
    ichn1 = edgbuf[ichn1].inxtlex;
  }
  edgbuf[itail].inxtlex = ichn2;

  return ihead;
}

// insert an edge into the SCAN LIST
void CPixelChannel::insertEdge(VEDGINDEX iedg, VEDGINDEX ipre, VEDGINDEX inxt)
{
  edgbuf[iedg].ibwd = ipre;
  if (ipre!=NULLVEDG) {
    edgbuf[ipre].ifwd = iedg;
  }
  else {
    iedglst = iedg;
  }
  edgbuf[iedg].ifwd = inxt;
  if (inxt!=NULLVEDG) {
    edgbuf[inxt].ibwd = iedg;
  }
}

// delete an edge from the SCAN LIST
void CPixelChannel::deleteEdge(VEDGINDEX iedg)
{
  VEDGINDEX ipre,inxt;

  ipre = edgbuf[iedg].ibwd;
  inxt = edgbuf[iedg].ifwd;
  if (ipre!=NULLVEDG) {
    edgbuf[ipre].ifwd = inxt;
  }
  else {
    iedglst = inxt;
  }
  if (inxt!=NULLVEDG) {
    edgbuf[inxt].ibwd = ipre;
  }
}

//-------------------------------------------------------------------------
//
// dumpRestoreRecord - use to dump out elements of a passed RESTORE_RECORD.
//
//  Creation Date: 8/13/2003
//  Author:        Michael Russell
//
//-------------------------------------------------------------------------
void CPixelChannel::dumpFPFRestoreRecord (FPF_RESTORE_RECORD *fpfResRecPtr)
{
  if (fpfResRecPtr == 0)
  {
      TRACE_0(errout << "NO RESTORE RECORD TO DUMP!");
      return;
  }

  string stResFlag = (fpfResRecPtr->resRec.restoreFlag & IS_INVALID)
                       ? "Invalid"
                       : (fpfResRecPtr->resRec.restoreFlag & IS_RESTORED)
                          ? "Restored"
                          : (fpfResRecPtr->resRec.restoreFlag == 0)
                             ? "Valid"
                             : "***Unknown***";

  TRACE_0(errout << "res->frameNum [" << offsetof(RESTORE_RECORD, frameNum) << "]        : " << fpfResRecPtr->resRec.frameNum
            << endl);
  TRACE_0(errout << "   ->fieldIndex [" << offsetof(RESTORE_RECORD, fieldIndex) << "]      : " << fpfResRecPtr->resRec.fieldIndex
            << endl);
  TRACE_0(errout << "   ->fieldNumber [" << offsetof(RESTORE_RECORD, fieldNumber) << "]     : " << fpfResRecPtr->resRec.fieldNumber
            << endl);
  TRACE_0(errout << "   ->globalSaveNumber [" << offsetof(RESTORE_RECORD, globalSaveNumber) << "]: " << fpfResRecPtr->resRec.globalSaveNumber
            << endl);
  TRACE_0(errout << "   ->saveTime [" << offsetof(RESTORE_RECORD, saveTime) << "]        : " << fpfResRecPtr->resRec.saveTime
            << endl);
  TRACE_0(errout << "   ->toolCode [" << offsetof(RESTORE_RECORD, toolCode) << "]        : " << fpfResRecPtr->resRec.toolCode
            << endl);
  TRACE_0(errout << "   ->restoreFlag [" << offsetof(RESTORE_RECORD, restoreFlag) << "]     : " << stResFlag << endl);
  TRACE_0(errout << "   ->extent.left [" << offsetof(RESTORE_RECORD, extent) << "]     : " << fpfResRecPtr->resRec.extent.left
            << endl);
  TRACE_0(errout << "           .top       : " << fpfResRecPtr->resRec.extent.top
            << endl);
  TRACE_0(errout << "           .right     : " << fpfResRecPtr->resRec.extent.right
            << endl);
  TRACE_0(errout << "           .bottom    : " << fpfResRecPtr->resRec.extent.bottom
            << endl);
  TRACE_0(errout << "   ->pxlFileByteOffset: [" << offsetof(RESTORE_RECORD, pxlFileByteOffset) << "]" << fpfResRecPtr->resRec.pxlFileByteOffset
            << endl);
  TRACE_0(errout << "   ->pxlByteCount [" << offsetof(RESTORE_RECORD, pxlByteCount) << "]    : " << fpfResRecPtr->resRec.pxlByteCount
            << endl);
  TRACE_0(errout << "SIZE: " << sizeof(*fpfResRecPtr));
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                          #####################################
// ###     CPixelRegion Class     ####################################
// ####                          #####################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

// Stubs - nothing to do in pure abstract base class
CPixelRegion::CPixelRegion()
{
}

CPixelRegion::CPixelRegion(const CPixelRegion &src)
{
  // was made private
  MTIassert(false);
}

CPixelRegion::~CPixelRegion()
{
}

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                          #####################################
// ###  CSinglePixelRegion Class  ####################################
// ####                          #####################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSinglePixelRegion::CSinglePixelRegion()
 : pixelComponents(0),
   // pixelArrayLength(0),        ZZ2. computable from components & populated
   // pixelArrayIsPopulated(false),
   // pixelArray(NULL)            ZZ3 eliminated
   currentRunIndex(-1)
{
}

CSinglePixelRegion::CSinglePixelRegion(const CSinglePixelRegion& srcPixelRegion)
 : pixelComponents(srcPixelRegion.pixelComponents),
   // type(srcPixelRegion.type),  ZZ1. Eliminate 'type' variable
   // pixelArrayLength(0),        ZZ2. computable from components & populated
   // pixelArrayIsPopulated(false),
   // pixelArray(NULL)            ZZ3 eliminated
   currentRunIndex(-1)
{
   // Reallocate and copy source's pixel array
   //CopyPixelArray(srcPixelRegion);                    // ZZ3
   inlinePixelArray = srcPixelRegion.inlinePixelArray;  // ZZ3
}

CSinglePixelRegion::CSinglePixelRegion(/* EPixelRegionType newType, */ int pxlComponentCount)
 : pixelComponents(pxlComponentCount),
   // type(newType),              ZZ1. Eliminate 'type' variable
   // pixelArrayLength(0),        ZZ2. computable from components & populated
   // pixelArrayIsPopulated(false),
   // pixelArray(NULL)            ZZ3 eliminated
   currentRunIndex(-1)
{
}

CSinglePixelRegion::~CSinglePixelRegion()
{
   // delete [] pixelArray;    // ZZ3 eliminated
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

#if 0 // ZZ3 eliminated entirely
void CSinglePixelRegion::CopyPixelArray(const CSinglePixelRegion& srcPixelRegion)
{
   // Delete existing pixel array
   // delete [] pixelArray;
   // pixelArray = NULL;       // ZZ3 eliminated
   // pixelArrayLength = 0;       ZZ2. computable from components & populated
   //inlinePixelArray.population = 0;   // ZZ2

   // pixelArrayLength = srcPixelRegion.pixelArrayLength;   ZZ2
   //int pixelArrayLength = 3 + pixelComponents;

   //inlinePixelArray.population = (srcPixelRegion.IsPixelArrayPopulated()? 1 : 0);

   // Copy caller's pixel data to new pixel array
   //if (srcPixelRegion.GetPixelArray() != NULL) {

      // Allocate a new array of pixels
      //pixelArray = new MTI_UINT16[pixelArrayLength];  ZZ3 eliminated

      // Assume C RTL does optimal byte copy
      //MTImemcpy(&inlinePixelArray, srcPixelRegion.GetPixelArray(),
                  //sizeof(MTI_UINT16) * pixelArrayLength);
   //}

   // It's real easy now! Almost too esy... hmmm...
   inlinePixelArray = srcPixelRegion.inlinePixelArray;
}
#endif

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

void CSinglePixelRegion::operator=(const CSinglePixelRegion& srcPixelRegion)
{
   if (&srcPixelRegion == this)
      return;  // Avoid copy to self

   // type = srcPixelRegion.type;          ZZ1. Eliminate 'type' variable

   // Reallocate and copy source's pixel array
   //CopyPixelArray(srcPixelRegion);                        ZZ3
   inlinePixelArray = srcPixelRegion.inlinePixelArray;   // ZZ3

   // copy other stuff
   pixelComponents = srcPixelRegion.GetPixelComponentCount();
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

int CSinglePixelRegion::GetPixelComponentCount() const
{
   return pixelComponents;
}

// Length in UINT16's!
int CSinglePixelRegion::GetPixelArrayLength() const
{
   // We hold max one pixel
   int pixelArrayLength;

   pixelArrayLength = 3 + inlinePixelArray.population * pixelComponents;

   return pixelArrayLength;
}

MTI_UINT16* CSinglePixelRegion::GetPixelArray() const
{
   return (MTI_UINT16 *) &inlinePixelArray.x;
}

bool CSinglePixelRegion::IsPixelArrayPopulated() const
{
   return (inlinePixelArray.population > 0);
}

void CSinglePixelRegion::OpenRuns()
{
   //currentRun = GetPixelArray();                     ZZ3
   //endOfRuns = currentRun + GetPixelArrayLength();   ZZ3
   //currentRunLength = 0;                             ZZ3
   currentRunIndex = -1;   // ZZ3. Before start of one and only run
}

MTI_UINT16 *CSinglePixelRegion::GetNextRun()
{
   // advance to the new current run
   // HAHa WTF??                                 ZZ3
   //currentRun += currentRunLength;
   //if ((currentRun==NULL)||(currentRun==endOfRuns))
      //return NULL;

   // set the length of the current run
   //currentRunLength = 3;
   //if (pixelArrayPopulation > 0)
      //currentRunLength += currentRun[2] * pixelComponents;

   // ZZ3 there can be only one run max!
   MTI_UINT16 *currentRun = NULL;

   if (currentRunIndex == -1)
   {
      currentRunIndex = 0;
      currentRun = reinterpret_cast<MTI_UINT16 *>( &inlinePixelArray );
   }

   return currentRun;
}

int CSinglePixelRegion::GetLengthOfRuns(bool copyEven, bool copyOdd)
{
   MTI_UINT16 *run;

   int totwds = 0;
   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(debugHackFrameWidth, debugHackFrameHeight, runX, runY, runLen);

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         totwds += 3 + runLen * pixelComponents;

      }
   }

   return totwds;
}

int CSinglePixelRegion::PutRunsToChannel(CPixelChannel *pxlchn,
                                   bool copyEven, bool copyOdd)
{
   MTIassert(pxlchn->pxlFileHandle >= 0);

   MTI_UINT16 *run;

   int totwds = 0;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(debugHackFrameWidth, debugHackFrameHeight, runX, runY, runLen);

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         int wds = 3 + runLen * pixelComponents;

         if ((pxlchn->pxlFillPtr + wds) >= pxlchn->pxlBufStop) { // no room in buffer

            // write out current runs to channel file
            int icnt = (pxlchn->pxlFillPtr - pxlchn->pxlBuf)*2;
            if (write(pxlchn->pxlFileHandle,(MTI_UINT8 *)pxlchn->pxlBuf,icnt)!=icnt)
               return -1;
            pxlchn->pxlFilePtr += icnt;
            pxlchn->pxlFillPtr = pxlchn->pxlBuf;

         }

         MTImemcpy((void *)pxlchn->pxlFillPtr, (void *)run, sizeof(MTI_UINT16)*wds);
         pxlchn->pxlFillPtr += wds;
         totwds += wds;
      }
   }

   return totwds;
}

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

MTI_UINT16 *CSinglePixelRegion::PutRunsToBuffer(MTI_UINT16 *dstPtr,
                                        bool copyEven, bool copyOdd)
{
   MTI_UINT16 *run;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         int wds = 3 + runLen * pixelComponents;

         MTImemcpy((void *)dstPtr, (void *)run, sizeof(MTI_UINT16) * wds);

         dstPtr += wds;

      }
   }

   return dstPtr;
}

void CSinglePixelRegion:: GetRunsFromBuffer(const MTI_UINT16 *srcPtr, int arrayLength)
{
   if (arrayLength > 6)
      arrayLength = 6;
   MTImemcpy((void *)&inlinePixelArray, (void *)srcPtr, sizeof(MTI_UINT16)*arrayLength);
   if (inlinePixelArray.population > 1)
      inlinePixelArray.population = 1;
}

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

// replace the pixel data in the pixel region's runs with data from
// the source 16-bit 444 intermediate frame
void CSinglePixelRegion::ReplacePixelsFromFrame(const MTI_UINT16 *srcFrame,
                                          int frameWidth,
                                          int frameHeight,
                                          bool copyEven, bool copyOdd)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   // if the pixel array is not populated, abort this call
   if (!IsPixelArrayPopulated())
      return;

   MTI_UINT16 *run;
   const MTI_UINT16 *srcPtr;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

      bool oddLine = (runY & 1);
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         srcPtr = ConvertFrameXYToPtr(srcFrame,
                                      (MTI_INT16)runX,
                                      (MTI_INT16)runY,
                                      frameWidth, pixelComponents);

         MTIassert(srcPtr >= srcFrame);
         MTIassert((srcPtr + runLen * pixelComponents) <= (srcFrame + (frameWidth * frameHeight * pixelComponents)));
         if (srcPtr < srcFrame ||
            (srcPtr + runLen * pixelComponents) > (srcFrame + (frameWidth * frameHeight * pixelComponents))) {
            TRACE_0(errout << "Bogus run in CSinglePixelRegion::ReplacePixelsFromFrame!!!     " << endl
                << "x=" << runX << "  y=" << runY << "  n=" << runLen
                << " w=" << frameWidth << " h=" << frameHeight
                << " c=" << pixelComponents);
         }
         else

         MTImemcpy((void *)(run+3), (void *)srcPtr,
               runLen*sizeof(MTI_UINT16)*pixelComponents);

      }
   }
}

// replace the pixel data in the source 16-bit 444 intermediate
// frame with data from the pixel region's runs
void CSinglePixelRegion::ReplacePixelsInFrame(const MTI_UINT16 *srcFrame,
                                        int frameWidth,
                                        int frameHeight,
                                        bool copyEven, bool copyOdd)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   // if the pixel array is not populated, abort this call
   if (!IsPixelArrayPopulated())
      return;

   MTI_UINT16 *run;
   MTI_UINT16 *srcPtr;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

      bool oddLine = (runY & 1);
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         srcPtr = (MTI_UINT16 *)(ConvertFrameXYToPtr(srcFrame,
                                                     (MTI_INT16)runX,
                                                     (MTI_INT16)runY,
                                                     frameWidth, pixelComponents));

         MTIassert(srcPtr >= srcFrame);
         MTIassert((srcPtr + runLen * pixelComponents) <= (srcFrame + (frameWidth * frameHeight * pixelComponents)));
         if (srcPtr < srcFrame ||
            (srcPtr + runLen * pixelComponents) > (srcFrame + (frameWidth * frameHeight * pixelComponents))) {
            TRACE_0(errout << "Bogus run in CSinglePixelRegion::ReplacePixelsInFrame!!!     " << endl
                << "x=" << runX << "  y=" << runY << "  n=" << runLen
                << " w=" << frameWidth << " h=" << frameHeight
                << " c=" << pixelComponents);
         }
         else

         MTImemcpy((void *)srcPtr, (void *)(run+3),
                runLen*sizeof(MTI_UINT16)*pixelComponents);

      }
   }
}

// use the runs in the pixel region as a template to copy pixels
// from a source 16-bit 444 frame to a destination 16-bit 444 frame
void CSinglePixelRegion::CopyPixelsFrameToFrame(const MTI_UINT16 *srcFrame,
                                          int frameWidth,
                                          int frameHeight,
                                          MTI_UINT16 *dstFrame,
                                          bool copyEven, bool copyOdd)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   MTI_UINT16 *run;
   const MTI_UINT16 *srcPtr;
   const MTI_UINT16 *dstPtr;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

      bool oddLine = (runY & 1);
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         srcPtr = ConvertFrameXYToPtr(srcFrame,
                                      (MTI_INT16)runX,
                                      (MTI_INT16)runY,
                                      frameWidth, pixelComponents);
         dstPtr = ConvertFrameXYToPtr(dstFrame,
                                      (MTI_INT16)runX,
                                      (MTI_INT16)runY,
                                      frameWidth, pixelComponents);

         MTIassert(srcPtr >= srcFrame);
         MTIassert((srcPtr + runLen * pixelComponents) <= (srcFrame + (frameWidth * frameHeight * pixelComponents)));
         MTIassert(dstPtr >= dstFrame);
         MTIassert((dstPtr + runLen * pixelComponents) <= (dstFrame + (frameWidth * frameHeight * pixelComponents)));
         if (srcPtr < srcFrame ||
            (srcPtr + runLen * pixelComponents) > (srcFrame + (frameWidth * frameHeight * pixelComponents)) ||
             dstPtr < dstFrame ||
            (dstPtr + runLen * pixelComponents) > (dstFrame + (frameWidth * frameHeight * pixelComponents))) {
            TRACE_0(errout << "Bogus run in CSinglePixelRegion::CopyPixelsFrameToFrame!!!     " << endl
                << "x=" << runX << "  y=" << runY << "  n=" << runLen
                << " w=" << frameWidth << " h=" << frameHeight
                << " c=" << pixelComponents);
         }
         else

         MTImemcpy((void *)dstPtr,(void *)srcPtr,
               runLen*sizeof(MTI_UINT16)*pixelComponents);
      }
   }
}

void CSinglePixelRegion::CopyPixelsToMask(const MTI_UINT16 *dstMask,
                                    int frameWidth, int frameHeight,
                                    bool copyEven, bool copyOdd,
                                    MTI_UINT16 mask)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   MTI_UINT16 *run;
   MTI_UINT16 *dstPtr;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

      bool oddLine = (runY & 1);
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         dstPtr = (MTI_UINT16 *)(ConvertFrameXYToPtr(dstMask,
                                                     (MTI_INT16)runX,
                                                     (MTI_INT16)runY,
                                                     frameWidth, 1));

         MTIassert(dstPtr >= dstMask);
         MTIassert((dstPtr + runLen) <= (dstMask + (frameWidth * frameHeight)));
         if (dstPtr < dstMask ||
            (dstPtr + runLen) > (dstMask + (frameWidth * frameHeight))) {
            TRACE_0(errout << "Bogus run in CMultiPixelRegion::CopyPixelsToMask!!!     " << endl
                << "x=" << runX << "  y=" << runY << "  n=" << runLen
                << " w=" << frameWidth << " h=" << frameHeight
                << " c=" << pixelComponents);
         }
         else

         // set the run's pels in mask
         for (int i=0;i<runLen;i++)
            dstPtr[i] = mask;
      }
   }
}

// highlight the runs of a pixel region in a frame buffer. The frame
// buffer affected is the one attached to the line engine supplied
// as argument. The highlight color is the modal value of FOREGROUND
// color previously set by a call to 'lineEngine->setFGColor(r,g,b)'
void CSinglePixelRegion::HighlightRunsInFrameBuffer(CLineEngine *lineEngine)
{
   MTI_UINT16 *run;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) { // for each run

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(lineEngine->getFrameWidth(), lineEngine->getFrameHeight(),
                 runX, runY, runLen);

      int xlft = runX&0xfffffffe;
      int xrgt = ((runX+runLen-1)&0xfffffffe) + 2;

      lineEngine->moveTo(xlft,runY);
      lineEngine->lineTo(xrgt,runY);

      run += 3 + runLen*pixelComponents;

   } // for each run
}


//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                          #####################################
// ###   CMultiPixelRegion Class  ####################################
// ####                          #####################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMultiPixelRegion::CMultiPixelRegion()
:pixelComponents(0),
 pixelArrayLength(0),
 pixelArrayIsPopulated(false),
 pixelArray(NULL)
{
}

CMultiPixelRegion::CMultiPixelRegion(const CMultiPixelRegion& srcPixelRegion)
 : pixelComponents(srcPixelRegion.pixelComponents),
   // type(srcPixelRegion.type),        ZZ1. Eliminate 'type' variable
   pixelArrayLength(0),
   pixelArrayIsPopulated(false),
   pixelArray(NULL)
{
   // Reallocate and copy source's pixel array
   CopyPixelArray(srcPixelRegion);
}

CMultiPixelRegion::CMultiPixelRegion(/* EPixelRegionType newType, */ int pxlComponentCount)
 : pixelComponents(pxlComponentCount),
   // type(newType),            ZZ1. Eliminate 'type' variable
   pixelArrayLength(0),
   pixelArrayIsPopulated(false),
   pixelArray(NULL)
{
}

CMultiPixelRegion::~CMultiPixelRegion()
{
   delete [] pixelArray;
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

void CMultiPixelRegion::CopyPixelArray(const CMultiPixelRegion& srcPixelRegion)
{
   // mbraca changed this to NOT BLINDLY DELETE AND REALLOCATE the frickin
   // pixel array if it's the right size or larger! Duh!
   if (pixelArrayLength < srcPixelRegion.pixelArrayLength)
   {
      // Delete existing pixel array
      delete [] pixelArray;
      pixelArray = NULL;
      pixelArrayLength = 0;
   }

   if (pixelArray == NULL || pixelArrayLength == 0)
   {
      try {
         // Allocate a new array of pixels
         pixelArray = new MTI_UINT16[srcPixelRegion.pixelArrayLength];
      }
      catch (std::bad_alloc)
      {
         TRACE_0(errout << "ERROR: Out of memory! History cannot be saved!  ");
         pixelArray = NULL;
         pixelArrayLength = 0;
         throw; // crash so we don't screw up the history
      }
   }

   pixelArrayLength = srcPixelRegion.pixelArrayLength;
   pixelArrayIsPopulated = srcPixelRegion.pixelArrayIsPopulated;

   // Copy caller's pixel data to new pixel array
   if (srcPixelRegion.pixelArray != 0) {

      // Assume C RTL does optimal byte copy
      MTImemcpy((void *)pixelArray, (void *)srcPixelRegion.pixelArray,
                  sizeof(MTI_UINT16) * pixelArrayLength);
   }
}

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

void CMultiPixelRegion::operator=(const CMultiPixelRegion& srcPixelRegion)
{
   if (&srcPixelRegion == this)
      return;  // Avoid copy to self

   //type = srcPixelRegion.type;             ZZ1. Eliminate 'type' variable

   // Reallocate and copy source's pixel array
   CopyPixelArray(srcPixelRegion);
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

int CMultiPixelRegion::GetPixelComponentCount() const
{
   return pixelComponents;
}

int CMultiPixelRegion::GetPixelArrayLength() const
{
   return pixelArrayLength;
}

MTI_UINT16* CMultiPixelRegion::GetPixelArray() const
{
   return pixelArray;
}

bool CMultiPixelRegion::IsPixelArrayPopulated() const
{
   return pixelArrayIsPopulated;
}

void CMultiPixelRegion::OpenRuns()
{
   currentRun = GetPixelArray();
   endOfRuns = currentRun + GetPixelArrayLength();
   currentRunLength = 0;
}

MTI_UINT16 *CMultiPixelRegion::GetNextRun()
{
   // advance to the new current run
   currentRun += currentRunLength;
   MTIassert(currentRun <= endOfRuns);
   if ((currentRun==NULL)||(currentRun==endOfRuns))
      return NULL;

   // set the length of the current run
   currentRunLength = 3;
   if (pixelArrayIsPopulated)
      currentRunLength += currentRun[2] * pixelComponents;
   MTIassert(currentRun <= endOfRuns);

   return currentRun;
}

int CMultiPixelRegion::GetLengthOfRuns(bool copyEven, bool copyOdd)
{
   MTI_UINT16 *run;

   int totwds = 0;
   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(debugHackFrameWidth, debugHackFrameHeight, runX, runY, runLen);

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&copyOdd)) {

         totwds += 3 + runLen * pixelComponents;

      }
   }

   return totwds;
}

int CMultiPixelRegion::PutRunsToChannel(CPixelChannel *pxlchn,
                                   bool copyEven, bool copyOdd)
{
   MTIassert(pxlchn->pxlFileHandle >= 0);

   MTI_UINT16 *run;
   int totwds = 0;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(debugHackFrameWidth, debugHackFrameHeight, runX, runY, runLen);

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         int wds = 3 + runLen * pixelComponents;

         if ((pxlchn->pxlFillPtr + wds) >= pxlchn->pxlBufStop) { // no room in buffer

            // write out current runs to channel file
            int icnt = (pxlchn->pxlFillPtr - pxlchn->pxlBuf)*2;
            if (write(pxlchn->pxlFileHandle,(MTI_UINT8 *)pxlchn->pxlBuf,icnt)!=icnt)
               return -1;
            pxlchn->pxlFilePtr += icnt;
            pxlchn->pxlFillPtr = pxlchn->pxlBuf;

         }

         MTImemcpy((void *)pxlchn->pxlFillPtr, (void *)run, sizeof(MTI_UINT16)*wds);
         pxlchn->pxlFillPtr += wds;

         totwds += wds;

      }
   }

   return totwds;
}

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

MTI_UINT16 *CMultiPixelRegion::PutRunsToBuffer(MTI_UINT16 *dstPtr,
                                        bool copyEven, bool copyOdd)
{
   MTI_UINT16 *run;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         int wds = 3 + runLen * pixelComponents;

         MTImemcpy((void *)dstPtr, (void *)run, sizeof(MTI_UINT16) * wds);

         dstPtr += wds;

      }
   }

   return dstPtr;
}

void CMultiPixelRegion:: GetRunsFromBuffer(const MTI_UINT16 *srcPtr, int arrayLength)
{
   MTImemcpy((void *)pixelArray, (void *)srcPtr, sizeof(MTI_UINT16)*arrayLength);
}

#endif //  __STUFF_NOT_CALLED_BY_ANYBODY__

// replace the pixel data in the pixel region's runs with data from
// the source 16-bit 444 intermediate frame
void CMultiPixelRegion::ReplacePixelsFromFrame(const MTI_UINT16 *srcFrame,
                                          int frameWidth,
                                          int frameHeight,
                                          bool copyEven, bool copyOdd)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   // if the pixel array is not populated, abort this call
   if (!pixelArrayIsPopulated) return;

   MTI_UINT16 *run;
   const MTI_UINT16 *srcPtr;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

      bool oddLine = (runY & 1);
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         srcPtr = ConvertFrameXYToPtr(srcFrame,
                                      (MTI_INT16)runX,
                                      (MTI_INT16)runY,
                                      frameWidth, pixelComponents);

         MTIassert(srcPtr >= srcFrame);
         MTIassert((srcPtr + runLen * pixelComponents) <= (srcFrame + (frameWidth * frameHeight * pixelComponents)));
         if (srcPtr < srcFrame ||
            (srcPtr + runLen * pixelComponents) > (srcFrame + (frameWidth * frameHeight * pixelComponents))) {
            TRACE_0(errout << "Bogus run in CMultiPixelRegion::ReplacePixelsFromFrame!!!     " << endl
                << "x=" << runX << "  y=" << runY << "  n=" << runLen
                << " w=" << frameWidth << " h=" << frameHeight
                << " c=" << pixelComponents);
         }
         else

         MTImemcpy((void *)(run+3), (void *)srcPtr,
               runLen*sizeof(MTI_UINT16)*pixelComponents);

      }
   }
}

// replace the pixel data in the source 16-bit 444 intermediate
// frame with data from the pixel region's runs
void CMultiPixelRegion::ReplacePixelsInFrame(const MTI_UINT16 *srcFrame,
                                        int frameWidth,
                                        int frameHeight,
                                        bool copyEven, bool copyOdd)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   // if the pixel array is not populated, abort this call
   if (!pixelArrayIsPopulated) return;

   MTI_UINT16 *run;
   MTI_UINT16 *srcPtr;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

      bool oddLine = (runY & 1);
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         srcPtr = (MTI_UINT16 *)(ConvertFrameXYToPtr(srcFrame,
                                                     (MTI_INT16)runX,
                                                     (MTI_INT16)runY,
                                                     frameWidth, pixelComponents));

         MTIassert(srcPtr >= srcFrame);
         MTIassert((srcPtr + runLen * pixelComponents) <= (srcFrame + (frameWidth * frameHeight * pixelComponents)));
         if (srcPtr < srcFrame ||
            (srcPtr + runLen * pixelComponents) > (srcFrame + (frameWidth * frameHeight * pixelComponents))) {
            TRACE_0(errout << "Bogus run in CMultiPixelRegion::ReplacePixelsInFrame!!!     " << endl
                << "x=" << runX << "  y=" << runY << "  n=" << runLen
                << " w=" << frameWidth << " h=" << frameHeight
                << " c=" << pixelComponents);
         }
         else

         MTImemcpy((void *)srcPtr, (void *)(run+3),
                runLen*sizeof(MTI_UINT16)*pixelComponents);

      }
   }
}

// use the runs in the pixel region as a template to copy pixels
// from a source 16-bit 444 frame to a destination 16-bit 444 frame
void CMultiPixelRegion::CopyPixelsFrameToFrame(const MTI_UINT16 *srcFrame,
                                          int frameWidth,
                                          int frameHeight,
                                          MTI_UINT16 *dstFrame,
                                          bool copyEven, bool copyOdd)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

  MTI_UINT16 *run;
   const MTI_UINT16 *srcPtr;
   const MTI_UINT16 *dstPtr;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

      bool oddLine = (runY & 1);
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         srcPtr = ConvertFrameXYToPtr(srcFrame,
                                      (MTI_INT16)runX,
                                      (MTI_INT16)runY,
                                      frameWidth, pixelComponents);
         dstPtr = ConvertFrameXYToPtr(dstFrame,
                                      (MTI_INT16)runX,
                                      (MTI_INT16)runY,
                                      frameWidth, pixelComponents);

         MTIassert(srcPtr >= srcFrame);
         MTIassert((srcPtr + runLen * pixelComponents) <= (srcFrame + (frameWidth * frameHeight * pixelComponents)));
         MTIassert(dstPtr >= dstFrame);
         MTIassert((dstPtr + runLen * pixelComponents) <= (dstFrame + (frameWidth * frameHeight * pixelComponents)));
         if (srcPtr < srcFrame ||
            (srcPtr + runLen * pixelComponents) > (srcFrame + (frameWidth * frameHeight * pixelComponents)) ||
             dstPtr < dstFrame ||
            (dstPtr + runLen * pixelComponents) > (dstFrame + (frameWidth * frameHeight * pixelComponents))) {
            TRACE_0(errout << "Bogus run in CSinglePixelRegion::CopyPixelsFrameToFrame!!!     " << endl
                << "x=" << runX << "  y=" << runY << "  n=" << runLen
                << " w=" << frameWidth << " h=" << frameHeight
                << " c=" << pixelComponents);
         }
         else

         MTImemcpy((void *)dstPtr,(void *)srcPtr,
               runLen*sizeof(MTI_UINT16)*pixelComponents);
      }
   }
}

void CMultiPixelRegion::CopyPixelsToMask(const MTI_UINT16 *dstMask,
                                    int frameWidth, int frameHeight,
                                    bool copyEven, bool copyOdd,
                                    MTI_UINT16 mask)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   MTI_UINT16 *run;
   MTI_UINT16 *dstPtr;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      // just assert - i wouldn't know what to do if it fails
      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

      bool oddLine = (runY & 1);
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         dstPtr = (MTI_UINT16 *)(ConvertFrameXYToPtr(dstMask,
                                                     (MTI_INT16)runX,
                                                     (MTI_INT16)runY,
                                                     frameWidth, 1));

         MTIassert(dstPtr >= dstMask);
         MTIassert((dstPtr + runLen) <= (dstMask + (frameWidth * frameHeight)));
         if (dstPtr < dstMask ||
            (dstPtr + runLen) > (dstMask + (frameWidth * frameHeight))) {
            TRACE_0(errout << "Bogus run in CMultiPixelRegion::CopyPixelsToMask!!!     " << endl
                << "x=" << runX << "  y=" << runY << "  n=" << runLen
                << " w=" << frameWidth << " h=" << frameHeight
                << " c=" << pixelComponents);
         }
         else

         // set the run's pels in mask
         for (int i=0;i<runLen;i++)
            dstPtr[i] = mask;
      }
   }
}

// highlight the runs of a pixel region in a frame buffer. The frame
// buffer affected is the one attached to the line engine supplied
// as argument. The highlight color is the modal value of FOREGROUND
// color previously set by a call to 'lineEngine->setFGColor(r,g,b)'
void CMultiPixelRegion::HighlightRunsInFrameBuffer(CLineEngine *lineEngine)
{
   MTI_UINT16 *run;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) { // for each run

      MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
      isRunBogus(lineEngine->getFrameWidth(), lineEngine->getFrameHeight(),
                     runX, runY, runLen);

      int xlft = runX&0xfffffffe;
      int xrgt = ((runX+runLen-1)&0xfffffffe) + 2;

      lineEngine->moveTo(xlft,runY);
      lineEngine->lineTo(xrgt,runY);

      run += 3 + runLen*pixelComponents;

   } // for each run
}


//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                               ################################
// ###     CPointPixelRegion Class     ###############################
// ####                               ################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPointPixelRegion::CPointPixelRegion(const POINT& newPoint)
 : CSinglePixelRegion(/* PIXEL_REGION_TYPE_POINT, */ 0)
{
   // point = newPoint;     // ZZ3
   inlinePixelArray.x = newPoint.x;
   inlinePixelArray.y = newPoint.y;
}

CPointPixelRegion::CPointPixelRegion(const CPointPixelRegion& srcPixelRegion)
 : CSinglePixelRegion(srcPixelRegion)
{
   // point = srcPixelRegion.GetPoint();  ZZ3
   inlinePixelArray.x = srcPixelRegion.GetPoint().x;
   inlinePixelArray.y = srcPixelRegion.GetPoint().y;
}

CPointPixelRegion::CPointPixelRegion(const POINT& newPoint,
                                     const MTI_UINT16 *srcFrame,
                                     int frameWidth,
                                     int frameHeight,
                                     int pxlComponentCount)
 : CSinglePixelRegion(/* PIXEL_REGION_TYPE_POINT, */ pxlComponentCount)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   //int pixelArrayLength = 0;   // ZZ2. computable from components and populated
   inlinePixelArray.population = ((srcFrame != NULL)? 1 : 0);

   // defining point
   //point = newPoint;
   inlinePixelArray.x = newPoint.x;
   inlinePixelArray.y = newPoint.y;

   // if the point is off the frame we're done
   if ((inlinePixelArray.x < 0)||
       (inlinePixelArray.x >= frameWidth)||
       (inlinePixelArray.y < 0)||
       (inlinePixelArray.y >= frameHeight)) return;

   if (inlinePixelArray.population > 0) { // populated runs

      //pixelArrayLength = 3 + pixelComponents;
      //pixelArray = new MTI_UINT16[pixelArrayLength];

      //pixelArray[0] = newPoint.x;       ZZ3
      //pixelArray[1] = newPoint.y;       ZZ3
      //pixelArray[2] = 1;                ZZ3 now population

      // populate the run
      MTImemcpy(/*pixelArray+3*/ (void *)&inlinePixelArray.r_or_y,
             (void *)ConvertFrameXYToPtr(srcFrame,
                                 inlinePixelArray.x, inlinePixelArray.y,
                                 frameWidth, pixelComponents),
             sizeof(MTI_UINT16)*pixelComponents);
   }
   else {                  // unpopulated runs

   // UH-OH - I guess I changed the semantics! Unpopulated run might still
   // have length of the run is 1 ????

      //pixelArrayLength = 3;
      //pixelArray = new MTI_UINT16[pixelArrayLength];

      //pixelArray[0] = newPoint.x;
      //pixelArray[1] = newPoint.y;
      //pixelArray[2] = 1;           // THIS SEEMS WRONG - SHOULDN'T IT BE 0?
   }
}

CPointPixelRegion::~CPointPixelRegion()
{
}

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

// Custom allocator stuff

#ifdef _WIN64

// This is 24 because that's what I observed the 'new' operator mallocing
// when creating a CPointPixelRegion - 16 bytes of data and 8 more bytes for
// ... vtable pointer maybe?
#define PPR_BYTES_PERSTORAGE_UNIT 24

#else

// This is 20 because that's what I observed the 'new' operator mallocing
// when creating a CPointPixelRegion - 16 bytes of data and 4 more bytes for
// ... vtable pointer maybe?
#define PPR_BYTES_PERSTORAGE_UNIT 20

#endif

#if CONSOLIDATE_POINT_REGION_MALLOCS
namespace {

static CSpinLock PPRLock;

union PPRStorageUnit
{
   void *next;     // Free list pointer overlay
   unsigned char foo[PPR_BYTES_PERSTORAGE_UNIT];
};
PPRStorageUnit *PPRFreeStorageList = NULL;

// Arbitrary number, I thought maybe something around 1 MPixel would be a good size
static const int PPRStorageMaxAllocCount = (256 * 4096);

// Arbitrary number, I thought maybe something around 100 KPixel would be a good size
static const int PPRStorageFirstAllocCount = (25 * 4096);

static size_t PPRStorageInUseCount = 0;
static PPRStorageUnit *PPRFirstStorageArea = NULL;
static size_t PPRFirstStorageAreaSize = 0;
static vector<PPRStorageUnit *> PPRAuxiliaryStorageAreas;

#if 0 // obsolete code

// an auto-initializing Critical Section
class autoInitCSection
{
   CRITICAL_SECTION myCriticalSection;
public:
   autoInitCSection()  { InitializeCriticalSection(&myCriticalSection); };
   ~autoInitCSection() { DeleteCriticalSection(&myCriticalSection); };
   void enter()        { EnterCriticalSection(&myCriticalSection); };
   void exit()         { LeaveCriticalSection(&myCriticalSection); };
};

autoInitCSection PPRCriticalSection;

#endif


}; // end anonymous namespace
#endif

static size_t debugLastTraceAmount = (size_t) -1;

void* CPointPixelRegion::operator new (size_t size)
{
  void * retVal = NULL;

#if CONSOLIDATE_POINT_REGION_MALLOCS

  MTIassert(size <= PPR_BYTES_PERSTORAGE_UNIT);
  if (size <= PPR_BYTES_PERSTORAGE_UNIT)
  {
     CAutoSpinLocker lock(PPRLock);

     try {

        if (PPRFreeStorageList == NULL)
        {
           size_t storageAllocCount = PPRStorageMaxAllocCount;
           if (PPRFirstStorageArea == NULL)
           {
              storageAllocCount = PPRStorageFirstAllocCount;
           }

           while (PPRFreeStorageList == NULL && storageAllocCount >= 1)
           {
              PPRFreeStorageList = (PPRStorageUnit *)
                 MTImalloc(storageAllocCount * PPR_BYTES_PERSTORAGE_UNIT);
              if (PPRFreeStorageList == NULL)
              {
                 storageAllocCount /= 2;
                 TRACE_3(errout << "///// PPR NEW: Can't fit "
                                << (storageAllocCount * 2) << ", trying "
                                << storageAllocCount);
              }
           }

           if (PPRFreeStorageList != NULL)
           {
              if (PPRFirstStorageArea == NULL)
              {
                 PPRFirstStorageArea = PPRFreeStorageList;
                 PPRFirstStorageAreaSize = storageAllocCount;
              }
              else
              {
                 PPRAuxiliaryStorageAreas.push_back(PPRFreeStorageList);
                 // don't need to save the size, this is to just delete them
              }

              size_t i;
              for (i = 0; i < (storageAllocCount - 1); ++i)
              {
                 PPRFreeStorageList[i].next = &PPRFreeStorageList[i + 1];
              }
              PPRFreeStorageList[i].next = NULL;
           }
        }
        if (PPRFreeStorageList != NULL)
        {
           retVal = PPRFreeStorageList;
           PPRFreeStorageList = (PPRStorageUnit *) PPRFreeStorageList->next;
        }
     }
     catch(...)
     {
        MTIassert(false);
     }

#if THROW_BAD_ALLOC_EXCEPTION_ON_OUT_OF_MEMORY
     if (retVal == NULL)
        throw std::bad_alloc();
#endif

     ++PPRStorageInUseCount;

     if ((PPRStorageInUseCount % 1000000) == 0)
     {
        if (debugLastTraceAmount != PPRStorageInUseCount)
        {
           TRACE_3(errout << "^^^^^ " << PPRStorageInUseCount
                          << " PPRs in use in "
                          << (PPRAuxiliaryStorageAreas.size() + 1)
                          << " storage areas!");

           debugLastTraceAmount = PPRStorageInUseCount;
        }
     }
  }
  else
  {
     // This shouldn't happen
     retVal = MTInew(size);
  }
#else // !CONSOLIDATE_POINT_REGION_MALLOCS

  retVal = MTInew(size);

#endif

  return retVal;
}

void CPointPixelRegion::operator delete (void *p)
{
#if CONSOLIDATE_POINT_REGION_MALLOCS

   CAutoSpinLocker lock(PPRLock);

   PPRStorageUnit *sup = (PPRStorageUnit *)p;
   sup->next = PPRFreeStorageList;
   PPRFreeStorageList = sup;

   MTIassert(PPRStorageInUseCount != 0);
   if (PPRStorageInUseCount == 0)
   {
      PPRStorageInUseCount = 1;
   }

   --PPRStorageInUseCount;
   if ((PPRStorageInUseCount % 1000000) == 0)
   {
      if (debugLastTraceAmount != PPRStorageInUseCount)
      {
         TRACE_3(errout << "vvvvv " << PPRStorageInUseCount
                        << " PPRs in use!");
         debugLastTraceAmount = PPRStorageInUseCount;
      }
   }
   if (PPRStorageInUseCount == 0)
   {
      if (!PPRAuxiliaryStorageAreas.empty())
      {
         // Free all auxiliary areas
         for (size_t i = 0; i < PPRAuxiliaryStorageAreas.size(); ++i)
         {
            MTIfree(PPRAuxiliaryStorageAreas[i]);
            PPRAuxiliaryStorageAreas[i] = NULL;
         }
         PPRAuxiliaryStorageAreas.clear();

         // Back to just one area
         PPRFreeStorageList = PPRFirstStorageArea;

         // Thread the free list through the remaining undeleted block
         size_t i;
         for (i = 0; i < (PPRFirstStorageAreaSize - 1); ++i)
         {
            PPRFreeStorageList[i].next = &PPRFreeStorageList[i + 1];
         }
         PPRFreeStorageList[i].next = NULL;
      }
   }

#else // !CONSOLIDATE_POINT_REGION_MALLOCS

   MTIdelete(p);

#endif
}


CPointPixelRegion&
CPointPixelRegion::operator=(const CPointPixelRegion& srcPixelRegion)
{
   if (&srcPixelRegion != this)  // Avoid copy to self
      {
      // Copy supertype, including reallocating pixel array
      CSinglePixelRegion::operator=(srcPixelRegion);

      // Copy contents of POINT
      //point = srcPixelRegion.point;     ZZ# all done in supertype
      }

   return *this;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

// ZZ1. Eliminate 'type' variable
EPixelRegionType CPointPixelRegion::GetType() const
{
   return PIXEL_REGION_TYPE_POINT;
}

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

int CPointPixelRegion::GetLengthInBuffer(bool copyEven, bool copyOdd)
{
   bool oddLine = inlinePixelArray.y & 1;

   if ((!oddLine&&copyEven)||(oddLine&&copyOdd))
      return 4 + pixelComponents; // TTT - Check this!

   return 0;
}

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

int CPointPixelRegion::PutRegionToChannel(CPixelChannel *pxlchn,
                                         bool copyEven, bool copyOdd)
{
   MTIassert(pxlchn->pxlFileHandle >= 0);

   bool oddLine = inlinePixelArray.y &1;

   int totwds = 0;

   if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

      if ((pxlchn->pxlFillPtr + 1) >= pxlchn->pxlBufStop) { // no room in buffer

         // write out current runs to channel file
         int icnt = (pxlchn->pxlFillPtr - pxlchn->pxlBuf)*2;
         if (write(pxlchn->pxlFileHandle,(MTI_UINT8 *)pxlchn->pxlBuf,icnt)!=icnt)
            return -1;
         pxlchn->pxlFilePtr += icnt;
         pxlchn->pxlFillPtr = pxlchn->pxlBuf;

      }

      *pxlchn->pxlFillPtr++ = PIXEL_REGION_TYPE_POINT;

      // put runs out to channel
      if (PutRunsToChannel(pxlchn, copyEven, copyOdd) == -1)
         return(-1);

      totwds = 4 + pixelComponents;
   }

   return(totwds);
}

POINT CPointPixelRegion::GetPoint() const
{
   // return point;     ZZ3
   POINT retVal;

   retVal.x = inlinePixelArray.x;
   retVal.y = inlinePixelArray.y;

   return retVal;
}

// override this method for POINTs in order to get more efficiency
void CPointPixelRegion::HighlightRunsInFrameBuffer(CLineEngine *lineEngine)
{
   //lineEngine->drawSpot(point.x,point.y);                      // ZZ3
   lineEngine->drawSpot(inlinePixelArray.x,inlinePixelArray.y);  // ZZ3
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###     CRectPixelRegion Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRectPixelRegion::CRectPixelRegion(const RECT& newRect)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_RECT, */ 0)
{
   rect = newRect;
}

// Copy Constructor, allocates new pixel array
CRectPixelRegion::CRectPixelRegion(const CRectPixelRegion& srcPixelRegion)
 : CMultiPixelRegion(srcPixelRegion)
{
   rect = srcPixelRegion.rect;
}

// Constructor that copies the pixels from within the caller's frame buffer
CRectPixelRegion::CRectPixelRegion(const RECT& newRect,
                                   const MTI_UINT16 *srcFrame,
                                   int frameWidth,
                                   int frameHeight,
                                   int pxlComponentCount)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_RECT, */ pxlComponentCount)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   pixelArrayIsPopulated = (srcFrame != NULL);

   // defining rectangle
   rect = newRect;

   // clip the rectangle to the frame
   if (rect.left < 0) rect.left = 0;
   if (rect.top  < 0) rect.top  = 0;
   if (rect.right  >= frameWidth) rect.right  = frameWidth-1;
   if (rect.bottom >= frameHeight) rect.bottom = frameHeight-1;

   // now go on to the runs
   int rowcount  = rect.bottom - rect.top + 1;
   int rowpixels = rect.right - rect.left + 1;
   int rowlength = rowpixels * pixelComponents;

   // if the clipped rect is NULL you're done
   if ((rowcount <= 0)||(rowpixels <=0)) return;

   if (pixelArrayIsPopulated) { // populated runs

      pixelArrayLength = rowcount * (3 + rowlength);
      pixelArray = new MTI_UINT16[pixelArrayLength];

      const MTI_UINT16 *srcPtr;
      MTI_UINT16 *dstPtr = pixelArray;

      for (int i=rect.top;i<=rect.bottom;i++) {

         // each run has X, Y and pixel count
         *dstPtr++ = rect.left;
         *dstPtr++ = i;
         *dstPtr++ = rowpixels;

         // populate the run
         srcPtr = ::ConvertFrameXYToPtr(srcFrame,rect.left,i,
                                        frameWidth,
                                        pixelComponents);

         for (int j=0;j<rowlength;j++)
            *dstPtr++ = *srcPtr++;
      }
   }
   else {                  // unpopulated runs

      pixelArrayLength = rowcount * (3);
      pixelArray = new MTI_UINT16[pixelArrayLength];

      MTI_UINT16 *dstPtr = pixelArray;

      for (int i=rect.top;i<=rect.bottom;i++) {

         // each run has X, Y and pixel count
         *dstPtr++ = rect.left;
         *dstPtr++ = i;
         *dstPtr++ = rowpixels;
      }
   }
}

CRectPixelRegion::~CRectPixelRegion()
{
}

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

CRectPixelRegion&
CRectPixelRegion::operator=(const CRectPixelRegion& srcPixelRegion)
{
   if (&srcPixelRegion != this) // Avoid copy to self
      {
      // Copy supertype, including reallocating pixel array
      CMultiPixelRegion::operator=(srcPixelRegion);

      // Copy contents of RECT
      rect = srcPixelRegion.rect;
      }

   return *this;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

// ZZ1. Eliminate 'type' variable
EPixelRegionType CRectPixelRegion::GetType() const
{
   return PIXEL_REGION_TYPE_RECT;
}

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

int CRectPixelRegion::GetLengthInBuffer(bool copyEven, bool copyOdd)
{
   MTI_UINT16 *run;
   int totwds = 0;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         totwds += 3 + runLen * pixelComponents;

      }
   }

   if (totwds > 0)
      return 7 + totwds;

   return 0;
}

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

int CRectPixelRegion::PutRegionToChannel(CPixelChannel *pxlchn,
                                         bool copyEven, bool copyOdd)
{
   MTIassert(pxlchn->pxlFileHandle >= 0);

   int totwds = 0;

   int runwds = GetLengthOfRuns(copyEven, copyOdd);
   if (runwds > 0) {

      if ((pxlchn->pxlFillPtr + 7) >= pxlchn->pxlBufStop) {

         // write out current runs to channel file
         int icnt = (pxlchn->pxlFillPtr - pxlchn->pxlBuf)*2;
         if (write(pxlchn->pxlFileHandle,(MTI_UINT8 *)pxlchn->pxlBuf,icnt)!=icnt)
            return -1;
         pxlchn->pxlFilePtr += icnt;
         pxlchn->pxlFillPtr = pxlchn->pxlBuf;

      }

      // enter the type header
      *(pxlchn->pxlFillPtr)++ = PIXEL_REGION_TYPE_RECT;

      // serialize the rect
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.left;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.top;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.right;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.bottom;

      // record total length of runs
      *(pxlchn->pxlFillPtr)++ = runwds&0xffff; // lo
      *(pxlchn->pxlFillPtr)++ = runwds>>16;    // hi

      // put runs out to channel
      if (PutRunsToChannel(pxlchn, copyEven, copyOdd) == -1)
         return(-1);

      totwds = 7 + runwds;
   }

   return(totwds);
}

RECT CRectPixelRegion::GetRect() const
{
   return rect;
}

// override this method for RECTs so as to draw just the outline
void CRectPixelRegion::HighlightRunsInFrameBuffer(CLineEngine *lineEngine)
{
   // force lft to be even, rgt to be odd
   // ditto for top and bottom, resp.
   int lft = rect.left&0xfffffffe;
   int top = rect.top&0xfffffffe;
   int rgt = (rect.right&0xfffffffe)+1;
   int bot = (rect.bottom&0xfffffffe)+1;

   // once around the outside of the rect
   lineEngine->moveTo(lft,top);
   lineEngine->lineTo(rgt,top);
   lineEngine->lineTo(rgt,bot);
   lineEngine->lineTo(lft,bot);
   lineEngine->lineTo(lft,top);

   // in one pixel and around again
   // note that the adjusted coords
   // will all lie within the frame
   lineEngine->moveTo(lft+1,top+1);
   lineEngine->lineTo(rgt-1,top+1);
   lineEngine->lineTo(rgt-1,bot-1);
   lineEngine->lineTo(lft+1,bot-1);
   lineEngine->lineTo(lft+1,top+1);
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###     CMaskPixelRegion Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// WTF good is something with 0 components?
CMaskPixelRegion::CMaskPixelRegion(const RECT& newRect)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_MASK, */ 0)
{
   rect = newRect;
}

// Copy Constructor, allocates new pixel array
CMaskPixelRegion::CMaskPixelRegion(const CMaskPixelRegion& srcPixelRegion)
 : CMultiPixelRegion(srcPixelRegion)
{
   rect = srcPixelRegion.rect;
}

// Constructor that copies the pixels from within the caller's frame buffer
CMaskPixelRegion::CMaskPixelRegion(const RECT& newRect,
                                   const MTI_UINT16 *newMask,
                                   const MTI_UINT16 *srcFrame,
                                   int frameWidth,
                                   int frameHeight,
                                   int pxlComponentCount,
                                   int iBitMask)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_MASK, */ pxlComponentCount)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   pixelArrayIsPopulated = (srcFrame != NULL);

   // defining rectangle of mask
   rect = newRect;

   // clip the rectangle to the frame
   if (rect.left < 0) rect.left = 0;
   if (rect.top  < 0) rect.top  = 0;
   if (rect.right  >= frameWidth) rect.right  = frameWidth-1;
   if (rect.bottom >= frameHeight) rect.bottom = frameHeight-1;

   // now go on to the runs
   int rowcount  = rect.bottom - rect.top + 1;
   int rowpixels = rect.right - rect.left + 1;
   int rowlength = rowpixels * pixelComponents;

   // if the clipped rect is NULL you're done
   if ((rowcount <= 0)||(rowpixels <=0)) return;

   MTI_UINT16 *dstPtr;

   if (pixelArrayIsPopulated) { // populated runs

      try {
         pixelArrayLength = rowcount * (3*64 + rowlength); // max 64 runs across
         pixelArray = new MTI_UINT16[pixelArrayLength];
      }
      catch (std::bad_alloc)
      {
         TRACE_0(errout << "ERROR: Out of memory trying to create mask pixel region!");
         pixelArrayLength = 0;
         pixelArray = NULL;
         throw;  // We're hosed anyhow - don't fuck up the history
      }

      dstPtr = pixelArray;

      const MTI_UINT16 *srcPtr =
      ConvertFrameXYToPtr(srcFrame,rect.left,rect.top,
                          frameWidth,pixelComponents);

      const MTI_UINT16 *mskptr =
      ConvertMaskXYToPtr(newMask,rect.left,rect.top,
                         frameWidth);
      MTI_UINT16 *run;

      for (int i=rect.top;i<=rect.bottom;i++) {

         bool phase = false;

         for (int j=rect.left;j<=rect.right;j++) {

            if (*mskptr&iBitMask) {

               if (!phase) { // init new run

                  run = dstPtr;
                  MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
                  dstPtr += 3;
                  runX = j;
                  runY = i;
                  runLen = 0;

                  // Theoretically the following can't fail because we clipped
                  // the bounding rect to the frame boundaries, but we call it
                  // anyhow to run the ASSERTs!
                  isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

                  phase = true;

               }
               if (phase) { // continue run

                  MTI_UINT16 &runLen = run[2];

	          for (int k=0;k<pixelComponents;k++)
                     *dstPtr++ = srcPtr[k];

                  runLen++;
               }
            }
            else
               phase = false;

            srcPtr += pixelComponents;
            mskptr++;
         }

         srcPtr += (frameWidth - rowpixels)*pixelComponents;
         mskptr +=  frameWidth - rowpixels;
      }
   }
   else {                  // unpopulated runs

      pixelArrayLength = rowcount * (3*64); // max 64 runs across
      pixelArray = new MTI_UINT16[pixelArrayLength];

      dstPtr = pixelArray;

      const MTI_UINT16 *mskptr =
      ConvertMaskXYToPtr(newMask,rect.left,rect.top,
                         frameWidth);
      MTI_UINT16 *run;

      for (int i=rect.top;i<=rect.bottom;i++) {

         bool phase = false;

         for (int j=rect.left;j<=rect.right;j++) {

            if (*mskptr&iBitMask) {

               if (!phase) { // init new run

                  run = dstPtr;
                  MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
                  dstPtr += 3;
                  runX = j;
                  runY = i;
                  runLen = 0;

                  // Theoretically the following can't fail because we clipped
                  // the bounding rect to the frame boundaries, but we call it
                  // anyhow to run the ASSERTs!
                  isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

                  phase = true;

               }
               if (phase) { // continue run

                  MTI_UINT16 &runLen = run[2];

                  runLen++;
               }
            }
            else
               phase = false;

            mskptr++;
         }

         mskptr +=  frameWidth - rowpixels;
      }
   }

   pixelArrayLength = dstPtr - pixelArray;
}

CMaskPixelRegion::CMaskPixelRegion(const RECT& newRect,
                                   const MTI_UINT8 *newMask,
                                   const MTI_UINT16 *srcFrame,
                                   int frameWidth,
                                   int frameHeight,
                                   int pxlComponentCount,
                                   int iBitMask)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_MASK, */ pxlComponentCount)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   pixelArrayIsPopulated = (srcFrame != NULL);

   // defining rectangle of mask
   rect = newRect;

   // clip the rectangle to the frame
   if (rect.left < 0) rect.left = 0;
   if (rect.top  < 0) rect.top  = 0;
   if (rect.right  >= frameWidth) rect.right  = frameWidth-1;
   if (rect.bottom >= frameHeight) rect.bottom = frameHeight-1;

   // now go on to the runs
   int rowcount  = rect.bottom - rect.top + 1;
   int rowpixels = rect.right - rect.left + 1;
   int rowlength = rowpixels * pixelComponents;

   // if the clipped rect is NULL you're done
   if ((rowcount <= 0)||(rowpixels <=0)) return;

   MTI_UINT16 *dstPtr;

   if (pixelArrayIsPopulated) { // populated runs

      pixelArrayLength = rowcount * (3*64 + rowlength); // max 64 runs across
      pixelArrayIsPopulated = true;
      pixelArray = new MTI_UINT16[pixelArrayLength];

      dstPtr = pixelArray;

      const MTI_UINT16 *srcPtr =
      ConvertFrameXYToPtr(srcFrame,rect.left,rect.top,
                          frameWidth,pixelComponents);

      const MTI_UINT8 *mskptr =
      ConvertMaskXYToPtr(newMask,rect.left,rect.top,
                         frameWidth);
      MTI_UINT16 *run;

      for (int i=rect.top;i<=rect.bottom;i++) {

         bool phase = false;

         for (int j=rect.left;j<=rect.right;j++) {

            if (*mskptr&iBitMask) {

               if (!phase) { // init new run

                  run = dstPtr;
                  MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
                  dstPtr += 3;
                  runX = j;
                  runY = i;
                  runLen = 0;

                  // Theoretically the following can't fail because we clipped
                  // the bounding rect to the frame boundaries, but we call it
                  // anyhow to run the ASSERTs!
                  isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

                  phase = true;

               }
               if (phase) { // continue run

                  MTI_UINT16 &runLen = run[2];

	          for (int k=0;k<pixelComponents;k++)
                     *dstPtr++ = srcPtr[k];

                  runLen++;
               }
            }
            else
               phase = false;

            srcPtr += pixelComponents;
            mskptr++;
         }

         srcPtr += (frameWidth - rowpixels)*pixelComponents;
         mskptr +=  frameWidth - rowpixels;
      }
   }
   else {                  // unpopulated runs

      pixelArrayLength = rowcount * (3*64); // max 64 runs across
      pixelArrayIsPopulated = false;
      pixelArray = new MTI_UINT16[pixelArrayLength];

      dstPtr = pixelArray;

      const MTI_UINT8 *mskptr =
      ConvertMaskXYToPtr(newMask,rect.left,rect.top,
                         frameWidth);
      MTI_UINT16 *run;

      for (int i=rect.top;i<=rect.bottom;i++) {

         bool phase = false;

         for (int j=rect.left;j<=rect.right;j++) {

            if (*mskptr&iBitMask) {

               if (!phase) { // init new run

                  run = dstPtr;
                  MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
                  dstPtr += 3;
                  runX = j;
                  runY = i;
                  runLen = 0;

                  // Theoretically the following can't fail because we clipped
                  // the bounding rect to the frame boundaries, but we call it
                  // anyhow to run the ASSERTs!
                  isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

                  phase = true;

               }
               if (phase) { // continue run

                  MTI_UINT16 &runLen = run[2];

                  runLen++;
               }
            }
            else
               phase = false;

            mskptr++;
         }

         mskptr +=  frameWidth - rowpixels;
      }
   }

   pixelArrayLength = dstPtr - pixelArray;
}

// this constructor derives a Mask pixel region by comparing an AFTER
// frame with a BEFORE frame, populating runs from the BEFORE frame.
CMaskPixelRegion::CMaskPixelRegion(const MTI_UINT16 *finFrame,
                                   const MTI_UINT16 *iniFrame,
                                   int frameWidth,
                                   int frameHeight,
                                   int pxlComponentCount)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_MASK, */ pxlComponentCount)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   // extent rectangle of mask
   rect.left   = 0x7fffffff;
   rect.top    = 0x7fffffff;
   rect.right  = 0;
   rect.bottom = 0;

   pixelArrayIsPopulated = ((finFrame!=NULL)&&(iniFrame!=NULL));

   if (pixelArrayIsPopulated) { // populated runs

      // first pass derives the extent

      const MTI_UINT16 *finptr = finFrame;
      const MTI_UINT16 *iniptr = iniFrame;

      for (int i=0; i<frameHeight; i++) {

         for (int j=0; j<frameWidth; j++) {

            for (int k=0; k<pixelComponents; k++) {

               if (finptr[k] != iniptr[k]) {
                  if (i < rect.top   ) rect.top    = i;
                  if (i > rect.bottom) rect.bottom = i;
                  if (j < rect.left  ) rect.left   = j;
                  if (j > rect.right ) rect.right  = j;
                  break;
               }
            }

            finptr+= pixelComponents;
            iniptr+= pixelComponents;
         }
      }

      // HUH? WTF happens if there is no difference???
      // This happens to me a lot in Paint
      //MTIassert(rect.top != 0x7fffffff);
      // just bail out here - I hope the CMultipixel constructor did a good
      // job of initializing stuff...
      if (rect.top == 0x7fffffff)
      {
         rect.top = 0; // does this matter?
         rect.left = 0;
         return;
      }

      // we have the extent - go on to the runs
      int rowcount  = rect.bottom - rect.top + 1;
      int rowpixels = rect.right - rect.left + 1;
      int rowlength = rowpixels * pixelComponents;

      pixelArrayLength = rowcount * (3*64 + rowlength); // max 64 runs across
      pixelArrayIsPopulated = true;
      pixelArray = new MTI_UINT16[pixelArrayLength];

      finptr = ConvertFrameXYToPtr(finFrame,rect.left,rect.top,
                                   frameWidth,pixelComponents);

      iniptr = ConvertFrameXYToPtr(iniFrame,rect.left,rect.top,
                                   frameWidth,pixelComponents);

      MTI_UINT16 *dstPtr = pixelArray;

      MTI_UINT16 *run;

      for (int i=rect.top;i<=rect.bottom;i++) {

         bool phase = false;

         for (int j=rect.left;j<=rect.right;j++) {

            bool diff = false;
            for (int k=0;k<pixelComponents;k++) {
               diff = (finptr[k] != iniptr[k]);
               if (diff)
                  break;
            }

            if (diff) {

               if (!phase) { // init new run

                  run = dstPtr;
                  MTI_UINT16 &runX = run[0], &runY = run[1], &runLen = run[2];
                  dstPtr += 3;
                  runX = j;
                  runY = i;
                  runLen = 0;

                  // Theoretically the following can't fail because we clipped
                  // the bounding rect to the frame boundaries, but we call it
                  // anyhow to run the ASSERTs!
                  isRunBogus(frameWidth, frameHeight, runX, runY, runLen);

                  phase = true;

               }
               if (phase) { // continue run

                  MTI_UINT16 &runLen = run[2];

	          for (int k=0;k<pixelComponents;k++)
                     *dstPtr++ = iniptr[k];

                  runLen++;
               }
            }
            else
               phase = false;

            finptr += pixelComponents;
            iniptr += pixelComponents;
         }

         finptr += (frameWidth - rowpixels)*pixelComponents;
         iniptr += (frameWidth - rowpixels)*pixelComponents;
      }

      pixelArrayLength = dstPtr - pixelArray;
   }
}

CMaskPixelRegion::CMaskPixelRegion(CPixelChannel *pxlchn,
                                   MTI_UINT64 chaddr, MTI_UINT32 chbytes,
                                   int pxlComponentCount)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_MASK, */ pxlComponentCount)
{
   // extent rectangle of mask
   rect.left   = 0x7fffffff;
   rect.top    = 0x7fffffff;
   rect.right  = 0;
   rect.bottom = 0;

   if (pxlchn->openPixelChannelForRestore(chaddr, chbytes) == -1) return;

   MTI_UINT16 *rgnptr = pxlchn->getNextPixelRegion();

   if ((rgnptr == NULL)||(*rgnptr != PIXEL_REGION_TYPE_MASK)) return;

   rect.left   = rgnptr[1];
   rect.top    = rgnptr[2];
   rect.right  = rgnptr[3];
   rect.bottom = rgnptr[4];

   // allocate storage for runs
   unsigned int runwds = ((unsigned int)rgnptr[6]<<16) + (unsigned int)rgnptr[5];

   // now populate the runs from the contents of the channel
   pixelArrayIsPopulated = true;
   pixelArray = new MTI_UINT16[runwds];

   MTI_UINT16 *runptr;
   MTI_UINT16 *dstPtr = pixelArray;
   while ((runptr = pxlchn->getNextRun()) != NULL) {
      runwds = 3 + runptr[2]*pixelComponents;
      MTImemcpy((void *)dstPtr, (void *)runptr, runwds*2);
      dstPtr += runwds;
   }

   pixelArrayLength = dstPtr - pixelArray;
}

CMaskPixelRegion::~CMaskPixelRegion()
{
}

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

CMaskPixelRegion&
CMaskPixelRegion::operator=(const CMaskPixelRegion& srcPixelRegion)
{
   if (&srcPixelRegion != this) // Avoid copy to self
      {
      // Copy supertype, including reallocating pixel array
      CMultiPixelRegion::operator=(srcPixelRegion);

      // Copy contents of RECT
      rect = srcPixelRegion.rect;
      }

   return *this;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

// ZZ1. Eliminate 'type' variable
EPixelRegionType CMaskPixelRegion::GetType() const
{
   return PIXEL_REGION_TYPE_MASK;
}

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

int CMaskPixelRegion::GetLengthInBuffer(bool copyEven, bool copyOdd)
{
   MTI_UINT16 *run;
   int totwds = 0;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         totwds += 3 + runLen * pixelComponents;

      }
   }

   if (totwds > 0)
      return 7 + totwds;

   return 0;
}

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

int CMaskPixelRegion::PutRegionToChannel(CPixelChannel *pxlchn,
                                         bool copyEven, bool copyOdd)
{
   MTIassert(pxlchn->pxlFileHandle >= 0);

   int totwds = 0;

   int runwds = GetLengthOfRuns(copyEven, copyOdd);
   if (runwds > 0) {

      if ((pxlchn->pxlFillPtr + 7) >= pxlchn->pxlBufStop) {

         // write out current runs to channel file
         int icnt = (pxlchn->pxlFillPtr - pxlchn->pxlBuf)*2;
         if (write(pxlchn->pxlFileHandle,(MTI_UINT8 *)pxlchn->pxlBuf,icnt)!=icnt)
            return -1;
         pxlchn->pxlFilePtr += icnt;
         pxlchn->pxlFillPtr = pxlchn->pxlBuf;

      }

      // enter the type header
      *(pxlchn->pxlFillPtr)++ = PIXEL_REGION_TYPE_MASK;

      // serialize the rect
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.left;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.top;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.right;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.bottom;

      // record total length of runs
      *(pxlchn->pxlFillPtr)++ = runwds&0xffff; // lo
      *(pxlchn->pxlFillPtr)++ = runwds>>16;    // hi

      // put runs out to channel
      if (PutRunsToChannel(pxlchn, copyEven, copyOdd) == -1)
         return(-1);

      totwds = 7 + runwds;
   }

   return(totwds);
}

RECT CMaskPixelRegion::GetRect() const
{
   return rect;
}

// override this method for MASKs so as to draw just the outline
void CMaskPixelRegion::HighlightRunsInFrameBuffer(CLineEngine *lineEngine)
{
   // force lft to be even, rgt to be odd
   // ditto for top and bottom, resp.
   int lft = rect.left&0xfffffffe;
   int top = rect.top&0xfffffffe;
   int rgt = (rect.right&0xfffffffe)+1;
   int bot = (rect.bottom&0xfffffffe)+1;

   // once around the outside of the rect
   lineEngine->moveTo(lft,top);
   lineEngine->lineTo(rgt,top);
   lineEngine->lineTo(rgt,bot);
   lineEngine->lineTo(lft,bot);
   lineEngine->lineTo(lft,top);

   // in one pixel and around again
   // note that the adjusted coords
   // will all lie within the frame
   lineEngine->moveTo(lft+1,top+1);
   lineEngine->lineTo(rgt-1,top+1);
   lineEngine->lineTo(rgt-1,bot-1);
   lineEngine->lineTo(lft+1,bot-1);
   lineEngine->lineTo(lft+1,top+1);
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###    CLassoPixelRegion Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLassoPixelRegion::CLassoPixelRegion(const POINT *newLasso)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_LASSO, */ 0)
{
   // defining rectangle of lasso
   rect.left   = MAXFRAMEX;
   rect.top    = MAXFRAMEY;
   rect.right  = MINFRAMEX;
   rect.bottom = MINFRAMEY;

   int i;
   for (i=0;;i++) {

      if (newLasso[i].x < rect.left)
         rect.left = newLasso[i].x;
      if (newLasso[i].y < rect.top)
         rect.top = newLasso[i].y;
      if (newLasso[i].x > rect.right)
         rect.right = newLasso[i].x;
      if (newLasso[i].y > rect.bottom)
         rect.bottom = newLasso[i].y;

      if ((i > 1)&&
          (newLasso[i].x == newLasso[0].x)&&
          (newLasso[i].y == newLasso[0].y))
         break;
   }

   vertexCount = i + 1;

   lasso = new POINT[vertexCount];

   // copy the vertices of the lasso from the argument
   MTImemcpy((void *)lasso, (void *)&newLasso[0], sizeof(POINT)*vertexCount);
}

// Copy Constructor, allocates new pixel array
CLassoPixelRegion::CLassoPixelRegion(const CLassoPixelRegion& srcPixelRegion)
 : CMultiPixelRegion(srcPixelRegion)
{
   // the bounding rectangle is copied over
   rect = srcPixelRegion.rect;

   lasso = 0;
   vertexCount = 0;

   if (srcPixelRegion.vertexCount < 4)
      return;

   vertexCount = srcPixelRegion.vertexCount;

   // Allocate a new array of lasso vertices
   lasso = new POINT[vertexCount];

   // Copy caller's pixel data to new lasso
   if (srcPixelRegion.lasso != 0) {

      MTImemcpy((void *)lasso, (void *)srcPixelRegion.lasso,
                  sizeof(POINT) * vertexCount);
   }
}

// Constructor that copies the pixels from the caller's frame buffer
CLassoPixelRegion::CLassoPixelRegion(const POINT *newLasso,
                                   const MTI_UINT16 *srcFrame,
                                   int frameWidth,
                                   int frameHeight,
                                   int pxlComponentCount)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_LASSO, */ pxlComponentCount)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   pixelArrayIsPopulated = (srcFrame != NULL);

   // defining rectangle of lasso
   rect.left   = MAXFRAMEX;
   rect.top    = MAXFRAMEY;
   rect.right  = MINFRAMEY;
   rect.bottom = MINFRAMEY;

   int i;
   for (i=0;;i++) {

      if (newLasso[i].x < rect.left)
         rect.left = newLasso[i].x;
      if (newLasso[i].y < rect.top)
         rect.top = newLasso[i].y;
      if (newLasso[i].x > rect.right)
         rect.right = newLasso[i].x;
      if (newLasso[i].y > rect.bottom)
         rect.bottom = newLasso[i].y;

      if ((i > 1)&&
          (newLasso[i].x == newLasso[0].x)&&
          (newLasso[i].y == newLasso[0].y))
         break;
   }

   vertexCount = i + 1;

   lasso = new POINT[vertexCount];

   // copy the vertices of the lasso from the argument
   MTImemcpy((void *)lasso, (void *)&newLasso[0], sizeof(POINT)*vertexCount);

   // clip the rectangle to the frame
   if (rect.left < 0) rect.left = 0;
   if (rect.top  < 0) rect.top  = 0;
   if (rect.right  >= frameWidth) rect.right  = frameWidth-1;
   if (rect.bottom >= frameHeight) rect.bottom = frameHeight-1;

   // now go on to the runs
   int rowcount  = rect.bottom - rect.top + 1;
   int rowpixels = rect.right - rect.left + 1;
   //int rowlength = rowpixels * pixelComponents;

   // if the clipped rect is NULL you're done
   if ((rowcount <= 0)||(rowpixels <=0)) return;

   if (pixelArrayIsPopulated) { // populated runs

      // for safety assume each pixel is its own run
      pixelArrayLength = 2*(rowcount+1)*rowpixels*(3 + pixelComponents);
      pixelArrayIsPopulated = true;
   }
   else {                  // unpopulatd runs

      // for safety assume each pixel is its own run
      pixelArrayLength = 2*(rowcount+1)*rowpixels*3;
      pixelArrayIsPopulated = false;
   }

   pixelArray = new MTI_UINT16[pixelArrayLength];

   // now use the MASK class to generate the runs
   CMask runEngine;

   // right and bottom are EXclusive
   runEngine.setActiveRegion(0, 0,
                             rect.right+1, rect.bottom+1);

   // when srcFrame is NULL we get unpopulated runs
   runEngine.initRun(pixelArray,
                     srcFrame,
                     frameWidth,
                     pixelComponents,
                     vertexCount);
   runEngine.moveToFill(lasso[0].x,lasso[0].y);
   for (int i=1;i<vertexCount;i++) {
      runEngine.lineToFill(lasso[i].x,lasso[i].y);
   }
   runEngine.fillPolygon();

   // set to exact value
   pixelArrayLength = runEngine.getEndOfRuns() - pixelArray;
}

CLassoPixelRegion::~CLassoPixelRegion()
{
   delete [] lasso;
}

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

CLassoPixelRegion&
CLassoPixelRegion::operator=(const CLassoPixelRegion& srcPixelRegion)
{
   if (&srcPixelRegion != this) // Avoid copy to self
      {
      // Copy supertype, including reallocating pixel array
      CMultiPixelRegion::operator=(srcPixelRegion);

      // Copy contents of RECT
      rect = srcPixelRegion.rect;

      // Delete existing lasso
      delete [] lasso;
      lasso = 0;
      vertexCount = 0;

      if (srcPixelRegion.vertexCount < 3)
         return *this;

      vertexCount = srcPixelRegion.vertexCount;

      // Allocate a new array of vertices
      lasso = new POINT[vertexCount];

      // Copy caller's vertex data to new lasso array
      if (srcPixelRegion.lasso != 0)
         MTImemcpy((void *)lasso, (void *)srcPixelRegion.lasso,
                  sizeof(POINT) * vertexCount);

      }

   return *this;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

// ZZ1. Eliminate 'type' variable
EPixelRegionType CLassoPixelRegion::GetType() const
{
   return PIXEL_REGION_TYPE_LASSO;
}

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

int CLassoPixelRegion::GetLengthInBuffer(bool copyEven, bool copyOdd)
{
   MTI_UINT16 *run;
   int totwds = 0;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         totwds += 3 + runLen * pixelComponents;

      }
   }

   if (totwds > 0)
      // see the .h file for details of serialization
      // note that each Lasso vertex is 2 16-bit wds
      return 8 + vertexCount*2 + totwds;

   return 0;
}

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

int CLassoPixelRegion::PutRegionToChannel(CPixelChannel *pxlchn,
                                         bool copyEven, bool copyOdd)
{
   MTIassert(pxlchn->pxlFileHandle >= 0);

   int totwds = 0;

   int runwds = GetLengthOfRuns(copyEven, copyOdd);
   if (runwds > 0) {

      if ((pxlchn->pxlFillPtr + 8 + vertexCount*2) >= pxlchn->pxlBufStop) {

         // write out current runs to channel file
         int icnt = (pxlchn->pxlFillPtr - pxlchn->pxlBuf)*2;
         if (write(pxlchn->pxlFileHandle,(MTI_UINT8 *)pxlchn->pxlBuf,icnt)!=icnt)
            return -1;
         pxlchn->pxlFilePtr += icnt;
         pxlchn->pxlFillPtr = pxlchn->pxlBuf;

      }

      // enter the type header
      *(pxlchn->pxlFillPtr)++ = PIXEL_REGION_TYPE_LASSO;

      // serialize the rect
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.left;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.top;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.right;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.bottom;

      // serialize the lasso polygon
      *(pxlchn->pxlFillPtr)++ = vertexCount;
      MTI_INT32 *vtxptr = (MTI_INT32 *)lasso;
      for (int i=0;i<vertexCount;i++) {
         *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = *vtxptr++;
         *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = *vtxptr++;
      }

      // record total length of runs
      *(pxlchn->pxlFillPtr)++ = runwds&0xffff; // lo
      *(pxlchn->pxlFillPtr)++ = runwds>>16;    // hi

      // put runs out to channel
      if (PutRunsToChannel(pxlchn, copyEven, copyOdd) == -1)
         return(-1);

      totwds = 8 + vertexCount*2 + runwds;
   }

   return(totwds);
}

RECT CLassoPixelRegion::GetRect() const
{
   return rect;
}

POINT *CLassoPixelRegion::GetLasso() const
{
   return lasso;
}

// override this method for LASSOs so as to draw just the outline
void CLassoPixelRegion::HighlightRunsInFrameBuffer(CLineEngine *lineEngine)
{
   lineEngine->moveTo(lasso[0].x,lasso[0].y);
   for (int i=1;i<vertexCount;i++)
      lineEngine->lineTo(lasso[i].x,lasso[i].y);
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###    CBezierPixelRegion Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBezierPixelRegion::CBezierPixelRegion(const BEZIER_POINT *newBezier)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_BEZIER, */ 0)
{
   // one pass through determines lasso extent and no of vertices
   rect.left   = MAXFRAMEX;
   rect.top    = MAXFRAMEY;
   rect.right  = MINFRAMEX;
   rect.bottom = MINFRAMEY;

   int i;
   for (i=0;;i++) {

      if (newBezier[i].xctrlpre < rect.left)
         rect.left = newBezier[i].xctrlpre;
      if (newBezier[i].yctrlpre < rect.top)
         rect.top = newBezier[i].yctrlpre;
      if (newBezier[i].xctrlpre > rect.right)
         rect.right = newBezier[i].xctrlpre;
      if (newBezier[i].yctrlpre > rect.bottom)
         rect.bottom = newBezier[i].yctrlpre;

      if (newBezier[i].x < rect.left)
         rect.left = newBezier[i].x;
      if (newBezier[i].y < rect.top)
         rect.top = newBezier[i].y;
      if (newBezier[i].x > rect.right)
         rect.right = newBezier[i].x;
      if (newBezier[i].y > rect.bottom)
         rect.bottom = newBezier[i].y;

      if (newBezier[i].xctrlnxt < rect.left)
         rect.left = newBezier[i].xctrlnxt;
      if (newBezier[i].yctrlnxt < rect.top)
         rect.top = newBezier[i].yctrlnxt;
      if (newBezier[i].xctrlnxt > rect.right)
         rect.right = newBezier[i].xctrlnxt;
      if (newBezier[i].yctrlnxt > rect.bottom)
         rect.bottom = newBezier[i].yctrlnxt;

      if ((i > 1)&&
          (newBezier[i].xctrlpre == newBezier[0].xctrlpre)&&
          (newBezier[i].yctrlpre == newBezier[0].yctrlpre)&&
          (newBezier[i].x == newBezier[0].x)&&
          (newBezier[i].y == newBezier[0].y)&&
          (newBezier[i].xctrlnxt == newBezier[0].xctrlnxt)&&
          (newBezier[i].yctrlnxt == newBezier[0].yctrlnxt))
         break;
   }

   vertexCount = i + 1;

   bezier = new BEZIER_POINT[vertexCount];

   // copy the vertices of the bezier from the argument
   MTImemcpy((void *)bezier,(void *)&newBezier[0],sizeof(BEZIER_POINT)*vertexCount);
}

// Copy Constructor, allocates new pixel array
CBezierPixelRegion::CBezierPixelRegion(const CBezierPixelRegion& srcPixelRegion)
 : CMultiPixelRegion(srcPixelRegion)
{
   // the bounding rectangle is copied over
   rect = srcPixelRegion.rect;

   bezier = 0;
   vertexCount = 0;

   if (srcPixelRegion.vertexCount < 3)
      return;

   vertexCount = srcPixelRegion.vertexCount;

   // Allocate a new array of bezier vertices
   bezier = new BEZIER_POINT[vertexCount];

   // Copy caller's pixel data to new bezier
   if (srcPixelRegion.bezier != 0) {

      MTImemcpy((void *)bezier, (void *)srcPixelRegion.bezier,
                  sizeof(BEZIER_POINT) * vertexCount);
   }
}

// Constructor that copies the pixels from the caller's frame buffer
CBezierPixelRegion::CBezierPixelRegion(const BEZIER_POINT *newBezier,
                                       const MTI_UINT16 *srcFrame,
                                       int frameWidth,
                                       int frameHeight,
                                       int pxlComponentCount)
 : CMultiPixelRegion(/* PIXEL_REGION_TYPE_BEZIER, */ pxlComponentCount)
{
#ifndef NDEBUG
   debugHackFrameWidth = frameWidth;
   debugHackFrameHeight = frameHeight;
#endif

   pixelArrayIsPopulated = (srcFrame != NULL);

   // one pass through determines lasso extent and no of vertices
   rect.left   = MAXFRAMEX;
   rect.top    = MAXFRAMEY;
   rect.right  = MINFRAMEX;
   rect.bottom = MINFRAMEY;

   int i;
   for (i=0;;i++) {

      if (newBezier[i].xctrlpre < rect.left)
         rect.left = newBezier[i].xctrlpre;
      if (newBezier[i].yctrlpre < rect.top)
         rect.top = newBezier[i].yctrlpre;
      if (newBezier[i].xctrlpre > rect.right)
         rect.right = newBezier[i].xctrlpre;
      if (newBezier[i].yctrlpre > rect.bottom)
         rect.bottom = newBezier[i].yctrlpre;

      if (newBezier[i].x < rect.left)
         rect.left = newBezier[i].x;
      if (newBezier[i].y < rect.top)
         rect.top = newBezier[i].y;
      if (newBezier[i].x > rect.right)
         rect.right = newBezier[i].x;
      if (newBezier[i].y > rect.bottom)
         rect.bottom = newBezier[i].y;

      if (newBezier[i].xctrlnxt < rect.left)
         rect.left = newBezier[i].xctrlnxt;
      if (newBezier[i].yctrlnxt < rect.top)
         rect.top = newBezier[i].yctrlnxt;
      if (newBezier[i].xctrlnxt > rect.right)
         rect.right = newBezier[i].xctrlnxt;
      if (newBezier[i].yctrlnxt > rect.bottom)
         rect.bottom = newBezier[i].yctrlnxt;

      if ((i > 1)&&
          (newBezier[i].xctrlpre == newBezier[0].xctrlpre)&&
          (newBezier[i].yctrlpre == newBezier[0].yctrlpre)&&
          (newBezier[i].x == newBezier[0].x)&&
          (newBezier[i].y == newBezier[0].y)&&
          (newBezier[i].xctrlnxt == newBezier[0].xctrlnxt)&&
          (newBezier[i].yctrlnxt == newBezier[0].yctrlnxt))
         break;
   }

   vertexCount = i + 1;

   bezier = new BEZIER_POINT[vertexCount];

   // copy the vertices of the bezier from the argument
   MTImemcpy((void *)bezier,(void *)&newBezier[0],sizeof(BEZIER_POINT)*vertexCount);

   // clip the rectangle to the frame
   if (rect.left < 0) rect.left = 0;
   if (rect.top  < 0) rect.top  = 0;
   if (rect.right  >= frameWidth) rect.right  = frameWidth-1;
   if (rect.bottom >= frameHeight) rect.bottom = frameHeight-1;

   // now go on to the runs
   int rowcount  = rect.bottom - rect.top + 1;
   int rowpixels = rect.right - rect.left + 1;
   //int rowlength = rowpixels * pixelComponents;

   // if the clipped rect is NULL you're done
   if ((rowcount <= 0)||(rowpixels <=0)) return;

   if (pixelArrayIsPopulated) { // populated runs

      // for safety assume each pixel is its own run
      pixelArrayLength = 2*(rowcount+1)*rowpixels*(3 + pixelComponents);
      pixelArrayIsPopulated = true;
   }
   else {                  // unpopulatd runs

      // for safety assume each pixel is its own run
      pixelArrayLength = 2*(rowcount+1)*rowpixels*3;
      pixelArrayIsPopulated = false;
   }

   pixelArray = new MTI_UINT16[pixelArrayLength];

   // now use the MASK class to generate the runs
   CMask runEngine;

   runEngine.setActiveRegion(0, 0,
                             rect.right+1, rect.bottom+1);

   // when srcFrame is NULL we get unpopulated runs
   int chordleng = 4;
   int totchords = runEngine.getTotalChords(bezier, chordleng);
   if (runEngine.initRun(pixelArray,srcFrame,frameWidth,pixelComponents, totchords)!=-1) {
      runEngine.moveToFill(bezier[0].x,bezier[0].y);
      for (int i=1;i<vertexCount;i++) {
         runEngine.splineToFill(bezier[i-1].xctrlnxt, bezier[i-1].yctrlnxt,
                                bezier[i  ].xctrlpre, bezier[i  ].yctrlpre,
                                bezier[i  ].x       , bezier[i  ].y       ,
                                chordleng );
      }
      runEngine.fillPolygon();
   }

   // set to exact value
   pixelArrayLength = runEngine.getEndOfRuns() - pixelArray;
}

CBezierPixelRegion::~CBezierPixelRegion()
{
   delete [] bezier;
}

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

CBezierPixelRegion&
CBezierPixelRegion::operator=(const CBezierPixelRegion& srcPixelRegion)
{
   if (&srcPixelRegion != this) // Avoid copy to self
      {
      // Copy supertype, including reallocating pixel array
      CMultiPixelRegion::operator=(srcPixelRegion);

      // Copy contents of RECT
      rect = srcPixelRegion.rect;

      // Delete existing bezier
      delete [] bezier;
      bezier = 0;
      vertexCount = 0;

      if (srcPixelRegion.vertexCount < 3)
         return *this;

      vertexCount = srcPixelRegion.vertexCount;

      // Allocate a new array of vertices
      bezier = new BEZIER_POINT[vertexCount];

      // Copy caller's vertex data to new bezier array
      if (srcPixelRegion.bezier != 0)
         MTImemcpy((void *)bezier, (void *)srcPixelRegion.bezier,
                  sizeof(BEZIER_POINT) * vertexCount);

      }

   return *this;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

// ZZ1. Eliminate 'type' variable
EPixelRegionType CBezierPixelRegion::GetType() const
{
   return PIXEL_REGION_TYPE_BEZIER;
}

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

int CBezierPixelRegion::GetLengthInBuffer(bool copyEven, bool copyOdd)
{
   MTI_UINT16 *run;
   int totwds = 0;

   OpenRuns();
   while ((run=GetNextRun())!=NULL) {

      bool oddLine = runY & 1;
      if ((!oddLine&&copyEven)||(oddLine&&copyOdd)) {

         totwds += 3 + runLen * pixelComponents;

      }
   }

   if (totwds > 0)
      // see the .h file for details of serialization
      // note that each Bezier vertex is 6 16-bit wds
      return 8 + vertexCount*6 + totwds;

   return 0;
}

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

int CBezierPixelRegion::PutRegionToChannel(CPixelChannel *pxlchn,
                                         bool copyEven, bool copyOdd)
{
   MTIassert(pxlchn->pxlFileHandle >= 0);

   int totwds = 0;

   int runwds = GetLengthOfRuns(copyEven, copyOdd);
   if (runwds > 0) {

      if ((pxlchn->pxlFillPtr + 8 + vertexCount*6) >= pxlchn->pxlBufStop) {

         // write out current runs to channel file
         int icnt = (pxlchn->pxlFillPtr - pxlchn->pxlBuf)*2;
         if (write(pxlchn->pxlFileHandle,(MTI_UINT8 *)pxlchn->pxlBuf,icnt)!=icnt)
            return -1;
         pxlchn->pxlFilePtr += icnt;
         pxlchn->pxlFillPtr = pxlchn->pxlBuf;

      }

      // enter the type header
      *(pxlchn->pxlFillPtr)++ = PIXEL_REGION_TYPE_BEZIER;

      // serialize the rect
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.left;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.top;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.right;
      *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = rect.bottom;

      // serialize the bezier polygon
      *(pxlchn->pxlFillPtr)++ = vertexCount;
      MTI_INT32 *vtxptr = (MTI_INT32 *)bezier;
      for (int i=0;i<vertexCount;i++) {
         *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = *vtxptr++; // xctrlpre
         *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = *vtxptr++; // yctrlpre
         *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = *vtxptr++; // x
         *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = *vtxptr++; // y
         *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = *vtxptr++; // xctrlnxt
         *(MTI_INT16 *)(pxlchn->pxlFillPtr)++ = *vtxptr++; // yctrlnxt
      }

      // record total length of runs
      *(pxlchn->pxlFillPtr)++ = runwds&0xffff; // lo
      *(pxlchn->pxlFillPtr)++ = runwds>>16;    // hi

      // put runs out to channel
      if (PutRunsToChannel(pxlchn, copyEven, copyOdd) == -1)
         return(-1);

      totwds = 8 + vertexCount*6 + runwds;
   }

   return(totwds);
}

RECT CBezierPixelRegion::GetRect() const
{
   return rect;
}

BEZIER_POINT *CBezierPixelRegion::GetBezier() const
{
   return bezier;
}

// override this method for BEZIERs so as to draw just the outline
void CBezierPixelRegion::HighlightRunsInFrameBuffer(CLineEngine *lineEngine)
{
   lineEngine->moveTo(bezier[0].x,bezier[0].y);
   for (int i=1;i<vertexCount;i++)
      lineEngine->splineTo(bezier[i-1].xctrlnxt, bezier[i-1].yctrlnxt,
                           bezier[i  ].xctrlpre, bezier[i  ].yctrlpre,
                           bezier[i  ].x       , bezier[i  ].y        );
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// ###################################################################
// ####                              #################################
// ###     CPixelRegionList Class     ################################
// ####                              #################################
// ###################################################################
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPixelRegionList::CPixelRegionList()
{
   Clear();
}

CPixelRegionList::CPixelRegionList(const CPixelRegionList& srcPixelRegionList)
{
   Clear();

   Add(srcPixelRegionList);
}

CPixelRegionList::~CPixelRegionList()
{
   DeletePixelRegionList();
}

CPixelRegionList&
CPixelRegionList::operator=(const CPixelRegionList& srcPixelRegionList)
{
   if (this != &srcPixelRegionList)
      {
      //DeletePixelRegionList();   Don't we need to init the bounding box?
      Clear();

      Add(srcPixelRegionList);
      }

   return *this;
}

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

void CPixelRegionList::Clear()
{
   DeletePixelRegionList();

   InitBoundingBox();
}

void CPixelRegionList::ClearWithoutDeletingRegions()
{
   listOfPixelRegions.clear();

   InitBoundingBox();
}

void CPixelRegionList::DeletePixelRegionList()
{
   int count = GetEntryCount();

   // Delete CMultiPixelRegion instance for each entry in list
   for (int i = 0; i < count; ++i)
      {
      delete listOfPixelRegions.at(i);
      }

   // Clear the list
   listOfPixelRegions.clear();
}

void CPixelRegionList::InitBoundingBox()
{
   boundingBox.left = 0;
   boundingBox.top = 0;
   boundingBox.right = 0;
   boundingBox.bottom = 0;

   boundingBoxValid = false;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

RECT CPixelRegionList::GetBoundingBox() const
{
   return boundingBox;
}

CPixelRegion* CPixelRegionList::GetEntry(int index) const
{
   if (index < 0 || index >= GetEntryCount())
      return 0;

   return listOfPixelRegions.at(index);
}

int CPixelRegionList::GetEntryCount() const
{
   return (int)listOfPixelRegions.size();
}

bool CPixelRegionList::IsBoundingBoxValid() const
{
   return boundingBoxValid;
}

//////////////////////////////////////////////////////////////////////
// Add Functions
//////////////////////////////////////////////////////////////////////

// ---------------------------------------------------------------------------
// Add POINT
void CPixelRegionList::AddPoint(const POINT& newPoint)
{
   CPointPixelRegion* newPixelRegion = new CPointPixelRegion(newPoint);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPoint);
}

void CPixelRegionList::Add(const POINT& newPoint, const MTI_UINT16 *srcFrame,
                           int frameWidth, int frameHeight, int pxlComponentCount)
{
   CPointPixelRegion* newPixelRegion = new CPointPixelRegion(newPoint, srcFrame,
                                                             frameWidth,
                                                             frameHeight,
                                                             pxlComponentCount);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPoint);
}

void CPixelRegionList::Add(const CPointPixelRegion& pixelRegion)
{
   // Create a new instance of Point Pixel Region using copy constructor
   CPointPixelRegion* newPixelRegion = new CPointPixelRegion(pixelRegion);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPixelRegion->GetPoint());
}

// careful - never delete pixel region independent of the list!
void CPixelRegionList::Add(CPointPixelRegion* pixelRegion)
{
   listOfPixelRegions.push_back(pixelRegion);

   ExpandBoundingBox(pixelRegion->GetPoint());
}

// ----------------------------------------------------------------------------
// Add RECT
void CPixelRegionList::AddRect(const RECT& newRect)
{
   CRectPixelRegion* newPixelRegion = new CRectPixelRegion(newRect);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newRect);
}

void CPixelRegionList::Add(const RECT& newRect, const MTI_UINT16 *srcFrame,
                           int frameWidth, int frameHeight, int pxlComponentCount)
{
   CRectPixelRegion* newPixelRegion = new CRectPixelRegion(newRect, srcFrame,
                                                           frameWidth,
                                                           frameHeight,
                                                           pxlComponentCount);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newRect);
}

void CPixelRegionList::Add(const CRectPixelRegion& pixelRegion)
{
   CRectPixelRegion* newPixelRegion = new CRectPixelRegion(pixelRegion);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPixelRegion->GetRect());
}

// careful - never delete pixel region independent of the list!
void CPixelRegionList::Add(CRectPixelRegion* pixelRegion)
{
   listOfPixelRegions.push_back(pixelRegion);

   ExpandBoundingBox(pixelRegion->GetRect());
}

// ----------------------------------------------------------------------------
// Add MASK
void CPixelRegionList::AddMask(const RECT& newRect)
{
   CMaskPixelRegion* newPixelRegion = new CMaskPixelRegion(newRect);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newRect);
}

void CPixelRegionList::Add(const RECT& newRect, const MTI_UINT16 *newMask,
                           const MTI_UINT16 *srcFrame,
                           int frameWidth,
                           int frameHeight,
                           int pxlComponentCount,
                           int iBitMask)
{
   CMaskPixelRegion* newPixelRegion = new CMaskPixelRegion(newRect, newMask,
                                                           srcFrame,
                                                           frameWidth,
                                                           frameHeight,
                                                           pxlComponentCount,
                                                           iBitMask);
   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newRect);
}

void CPixelRegionList::Add(const RECT& newRect, const MTI_UINT8 *newMask,
                           const MTI_UINT16 *srcFrame,
                           int frameWidth,
                           int frameHeight,
                           int pxlComponentCount,
                           int iBitMask)
{
   CMaskPixelRegion* newPixelRegion = new CMaskPixelRegion(newRect, newMask,
                                                           srcFrame,
                                                           frameWidth,
                                                           frameHeight,
                                                           pxlComponentCount,
                                                           iBitMask);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newRect);
}

void CPixelRegionList::Add(const CMaskPixelRegion& pixelRegion)
{
   CMaskPixelRegion* newPixelRegion = new CMaskPixelRegion(pixelRegion);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPixelRegion->GetRect());
}

// careful - never delete pixel region independent of the list!
void CPixelRegionList::Add(CMaskPixelRegion* pixelRegion)
{
   listOfPixelRegions.push_back(pixelRegion);

   ExpandBoundingBox(pixelRegion->GetRect());
}

// ----------------------------------------------------------------------------
// Add LASSO

void CPixelRegionList::AddLasso(const POINT * newLasso)
{
   CLassoPixelRegion* newPixelRegion = new CLassoPixelRegion(newLasso);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPixelRegion->GetRect());
}

void CPixelRegionList::Add(const POINT * newLasso, const MTI_UINT16 *srcFrame,
                           int frameWidth,
                           int frameHeight,
                           int pxlComponentCount)
{
   CLassoPixelRegion* newPixelRegion = new CLassoPixelRegion(newLasso, srcFrame,
                                                             frameWidth,
                                                             frameHeight,
                                                             pxlComponentCount);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPixelRegion->GetRect());
}

void CPixelRegionList::Add(const CLassoPixelRegion& pixelRegion)
{
   CLassoPixelRegion* newPixelRegion = new CLassoPixelRegion(pixelRegion);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPixelRegion->GetRect());
}

// careful - never delete pixel region independent of the list!
void CPixelRegionList::Add(CLassoPixelRegion* pixelRegion)
{
   listOfPixelRegions.push_back(pixelRegion);

   ExpandBoundingBox(pixelRegion->GetRect());
}

// ----------------------------------------------------------------------------
// Add BEZIER

void CPixelRegionList::AddBezier(const BEZIER_POINT * newBezier)
{
   CBezierPixelRegion* newPixelRegion = new CBezierPixelRegion(newBezier);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPixelRegion->GetRect());
}

void CPixelRegionList::Add(const BEZIER_POINT * newBezier, const MTI_UINT16 *srcFrame,
                           int frameWidth,
                           int frameHeight,
                           int pxlComponentCount)
{
   CBezierPixelRegion* newPixelRegion = new CBezierPixelRegion(newBezier, srcFrame,
                                                             frameWidth,
                                                             frameHeight,
                                                             pxlComponentCount);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPixelRegion->GetRect());
}

void CPixelRegionList::Add(const CBezierPixelRegion& pixelRegion)
{
   CBezierPixelRegion* newPixelRegion = new CBezierPixelRegion(pixelRegion);

   listOfPixelRegions.push_back(newPixelRegion);

   ExpandBoundingBox(newPixelRegion->GetRect());
}

// careful - never delete pixel region independent of the list!
void CPixelRegionList::Add(CBezierPixelRegion* pixelRegion)
{
   listOfPixelRegions.push_back(pixelRegion);

   ExpandBoundingBox(pixelRegion->GetRect());
}

// ----------------------------------------------------------------------------
// Copy and Append contents of a Pixel Region List on the end of this list
// Caller must handle std::bad_alloc exception thrown from CopyPixelRegion
// on out of memory error.

void CPixelRegionList::Add(const CPixelRegionList& srcPixelRegionList)
{
   int srcCount = srcPixelRegionList.GetEntryCount();
   CPixelRegion* newPixelRegion;

   if (srcPixelRegionList.IsBoundingBoxValid())
      ExpandBoundingBox(srcPixelRegionList.GetBoundingBox());

   for (int i = 0; i < srcCount; ++i)
      {
      newPixelRegion = CopyPixelRegion(*(srcPixelRegionList.GetEntry(i)));
      listOfPixelRegions.push_back(newPixelRegion);
      }
}

// ----------------------------------------------------------------------------
// Move and Append contents of a Pixel Region List on the end of this list -
// after the operation the source list will be cleared

void CPixelRegionList::Move(CPixelRegionList& srcPixelRegionList)
{
   int srcCount = srcPixelRegionList.GetEntryCount();
   CPixelRegion* newPixelRegion;

   if (srcPixelRegionList.IsBoundingBoxValid())
      ExpandBoundingBox(srcPixelRegionList.GetBoundingBox());

   for (int i = 0; i < srcCount; ++i)
      {
      listOfPixelRegions.push_back(srcPixelRegionList.GetEntry(i));
      }

   srcPixelRegionList.ClearWithoutDeletingRegions();
}

// ----------------------------------------------------------------------------
// Replace all of the pixel values in the pixel region list with
// new values extracted from the caller's frame buffer
void CPixelRegionList::ReplacePixelsFromFrame(const MTI_UINT16 *srcFrame,
                                              int frameWidth,
                                              int frameHeight,
                                              bool copyEven, bool copyOdd)
{
   for (int i = 0; i < GetEntryCount(); ++i)
      {
      listOfPixelRegions.at(i)->ReplacePixelsFromFrame(srcFrame,
                                                    frameWidth,
                                                    frameHeight,
                                                    copyEven, copyOdd);
      }
}

// Copy the pixels identified in the Pixel Region List from the source frame
// to the destination frame.  Assume source and destination frames have
// the same pitch
void CPixelRegionList::CopyPixelsFrameToFrame(const MTI_UINT16 *srcFrame,
                                              int frameWidth,
                                              int frameHeight,
                                              MTI_UINT16 *dstFrame,
                                              bool copyEven, bool copyOdd)
{
   for (int i = 0; i < GetEntryCount(); ++i)
      {
      listOfPixelRegions.at(i)->CopyPixelsFrameToFrame(srcFrame,
                                                    frameWidth, frameHeight,
                                                    dstFrame,
                                                    copyEven, copyOdd);
      }
}

// Given a pixel region list, make a copy whose constituent pixel regions
// contain no runs, but is drawable by HighlightRunsInFrameBuffer and
// drawPixelRegionList
CPixelRegionList* CPixelRegionList::MakeCopyWithNoRuns()
{
   CPixelRegionList *retval = new CPixelRegionList();

   for (int i = 0; i < GetEntryCount(); ++i) {

      switch(listOfPixelRegions.at(i)->GetType()) {

         case PIXEL_REGION_TYPE_POINT:

            retval->AddPoint((static_cast<CPointPixelRegion *>
                             (listOfPixelRegions.at(i)))->GetPoint());
            break;

         case PIXEL_REGION_TYPE_RECT:

            retval->AddRect((static_cast<CRectPixelRegion *>
                            (listOfPixelRegions.at(i)))->GetRect());
            break;

         case PIXEL_REGION_TYPE_MASK:

            retval->AddMask((static_cast<CMaskPixelRegion *>
                            (listOfPixelRegions.at(i)))->GetRect());
            break;

         case PIXEL_REGION_TYPE_LASSO:

            retval->AddLasso((static_cast<CLassoPixelRegion *>
                             (listOfPixelRegions.at(i)))->GetLasso());
            break;

         case PIXEL_REGION_TYPE_BEZIER:

            retval->AddBezier((static_cast<CBezierPixelRegion *>
                             (listOfPixelRegions.at(i)))->GetBezier());
            break;
      }
   }

   return retval;
}
// ----------------------------------------------------------------------------
// Factory Method

CPixelRegion* CPixelRegionList::CopyPixelRegion(const CPixelRegion& pixelRegion)
{
   CPixelRegion* newPixelRegion = 0;

   switch(pixelRegion.GetType())
      {

      case PIXEL_REGION_TYPE_POINT :   // Point

         newPixelRegion
          = new CPointPixelRegion(static_cast<const CPointPixelRegion&>(pixelRegion));
         break;

      case PIXEL_REGION_TYPE_RECT :    // Rectangle
         newPixelRegion
            = new CRectPixelRegion(static_cast<const CRectPixelRegion&>(pixelRegion));
         break;

      case PIXEL_REGION_TYPE_MASK :    // Mask
         newPixelRegion
            = new CMaskPixelRegion(static_cast<const CMaskPixelRegion&>(pixelRegion));
         break;

      case PIXEL_REGION_TYPE_LASSO :   // Lasso
         newPixelRegion
           = new CLassoPixelRegion(static_cast<const CLassoPixelRegion&>(pixelRegion));
         break;

      case PIXEL_REGION_TYPE_BEZIER :   // Bezier
         newPixelRegion
           = new CBezierPixelRegion(static_cast<const CBezierPixelRegion&>(pixelRegion));
         break;

      default :
         newPixelRegion = 0;
         break;
      }

   return newPixelRegion;
}

//////////////////////////////////////////////////////////////////////
// Bounding Box
//////////////////////////////////////////////////////////////////////

void CPixelRegionList::ExpandBoundingBox(const POINT& point)
{
   if (boundingBoxValid)
      {
      if (boundingBox.left > point.x)
         boundingBox.left = point.x;
      if (boundingBox.right < point.x)
         boundingBox.right = point.x;
      if (boundingBox.top > point.y)
         boundingBox.top = point.y;
      if (boundingBox.bottom < point.y)
         boundingBox.bottom = point.y;
      }
   else
      {
      boundingBoxValid = true;
      boundingBox.left = boundingBox.right = point.x;
      boundingBox.top = boundingBox.bottom = point.y;
      }
}

void CPixelRegionList::ExpandBoundingBox(const RECT& rect)
{
   if (boundingBoxValid)
      {
      if (boundingBox.left > rect.left)
         boundingBox.left = rect.left;
      if (boundingBox.right < rect.right)
         boundingBox.right = rect.right;
      if (boundingBox.top > rect.top)
         boundingBox.top = rect.top;
      if (boundingBox.bottom < rect.bottom)
         boundingBox.bottom = rect.bottom;
      }
   else
      {
      boundingBoxValid = true;
      boundingBox = rect;
      }
}

void CPixelRegionList::ExpandBoundingBox(const POINT& point, int runLength)
{
   int right = point.x + runLength - 1;

   if (boundingBoxValid)
      {
      if (boundingBox.left > point.x)
         boundingBox.left = point.x;
      if (boundingBox.right < right)
         boundingBox.right = right;
      if (boundingBox.top > point.y)
         boundingBox.top = point.y;
      if (boundingBox.bottom < point.y)
         boundingBox.bottom = point.y;
      }
   else
      {
      boundingBoxValid = true;
      boundingBox.left = point.x;
      boundingBox.right = right;
      boundingBox.top = boundingBox.bottom = point.y;
      }
}

// highlight all runs of a pixel region list in a frame buffer. The
// buffer affected is the one attached to the line engine supplied
// as argument. The highlight color is the modal value of FOREGROUND
// color previously set by a call to 'lineEngine->setFGColor(r,g,b)'
void CPixelRegionList::HighlightRunsInFrameBuffer(CLineEngine *lineEngine)
{
   for (int i=0; i<GetEntryCount(); i++) {

      GetEntry(i)->HighlightRunsInFrameBuffer(lineEngine);

   }
}

// output a pixel region list to a pixel channel, returning the
// total number of bytes copied
int CPixelRegionList::PutRegionListToChannel(CPixelChannel *pxlchn,
                                             bool evn, bool odd)
{
   int bytcnt, result;

   MTIassert(pxlchn->pxlFileHandle >= 0);

   MTI_INT64 tmpPxlFileByteOffset = pxlchn->pxlFilePtr - pxlchn->pxlSectionBeg;

   int totwds = 0;

   // for each region in the argument region list do...
   for (int i=0;i<GetEntryCount();i++) {

      CPixelRegion* srcrgn = GetEntry(i);

      totwds += srcrgn->PutRegionToChannel(pxlchn,evn,odd);
   }

   // include the last runs in the channel file
   bytcnt = (pxlchn->pxlFillPtr - pxlchn->pxlBuf)*2;
   result = write(pxlchn->pxlFileHandle,(MTI_UINT8 *)pxlchn->pxlBuf, bytcnt);
   if (result != bytcnt)
      return -1;

   pxlchn->pxlFilePtr += bytcnt;
   pxlchn->pxlFillPtr = pxlchn->pxlBuf;

   if (pxlchn->resBlkFctr > 0) { // done with RES record

      pxlchn->resRecPtr->resRec.pxlFileByteOffset = tmpPxlFileByteOffset;

      pxlchn->resRecPtr->resRec.pxlByteCount = (totwds * 2);

      // in file-per-frame history global save numbers are no longer
      // global but are simply the index of the fix in the .hst file
      pxlchn->resRecPtr->resRec.globalSaveNumber = pxlchn->resRecCount;

      pxlchn->resRecCount++;

      pxlchn->resRecPtr++;

      pxlchn->resWriteBackFlag = true;

      if (pxlchn->resRecPtr == pxlchn->resBufStop) {

         // write out cur block of RES records
         if (lseek64(pxlchn->pxlFileHandle, pxlchn->resFilePtr, SEEK_SET) == -1)
            return(-1);

         bytcnt = pxlchn->resBufSize;
         result = write(pxlchn->pxlFileHandle, pxlchn->resBuf, bytcnt);
         if (result != bytcnt)
            return(-1);

         // move pixel data down
         pxlchn->expandIndexSection();

         // base addr of new block
         pxlchn->resFilePtr += pxlchn->resBufSize;

         // seek to end of pixel data
         if (lseek64(pxlchn->pxlFileHandle, pxlchn->pxlFilePtr, SEEK_SET) == -1)
            return(-1);

         pxlchn->resRecPtr = pxlchn->resBuf;
      }
   }

   return(totwds * 2);
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

#define MAX_PIXEL_REGION_LIST_ALLOC_COUNT (256 * 1024)
//static void *debugExcluder = NULL;

KindaLikeAVectorOfPixelRegionPtrs::KindaLikeAVectorOfPixelRegionPtrs()
{
   count = 0;
}

KindaLikeAVectorOfPixelRegionPtrs::~KindaLikeAVectorOfPixelRegionPtrs()
{
   clear();
}

void KindaLikeAVectorOfPixelRegionPtrs::clear()
{
   for (size_t i = 0; i < listPieces.size(); ++i)
   {
      MTIfree(listPieces[i]);
   }
   listPieces.clear();
   count = 0;
}

CPixelRegion *KindaLikeAVectorOfPixelRegionPtrs::at(size_t atIndex) const
{
   CPixelRegion *retVal = NULL;
   size_t pieceSize = listPieces.size();

   MTIassert(pieceSize > 0);
   MTIassert(atIndex < count);
   if (pieceSize > 0 && atIndex < count)
   {
      PieceOfTheList const *listPiece;
      size_t index;

      // here "-1" because we don't bother chacking the capacity of the
      // last piece, we assume if we got that far that it's the one (we
      // already checked against the overall count, above)
      for (index = (pieceSize - 1), listPiece = listPieces[index];
           listPiece->firstIndex > atIndex;
           listPiece = listPieces[--index])
      {
         /* just looking! */;
      }
      retVal = listPiece->ptrs[atIndex - listPiece->firstIndex];
   }

   return retVal;
}

size_t KindaLikeAVectorOfPixelRegionPtrs::size() const
{
   return count;
}

void KindaLikeAVectorOfPixelRegionPtrs::push_back(CPixelRegion *pixelRegionPtr)
{
   PieceOfTheList *lastListPiece;

   if (count == 0)
   {
      PieceOfTheList *piece = allocPiece(0);
      MTIassert(piece != NULL);
      if (piece != NULL)
      {
         listPieces.push_back(piece);
      }
      else
      {
         // What do I do now? Silently fail?
         return;
      }
      lastListPiece = listPieces[0];
   }
   else
   {
      lastListPiece = listPieces[listPieces.size() - 1];

      if (lastListPiece->remaining == 0)
      {
         lastListPiece = allocPiece(lastListPiece->firstIndex +
                                    lastListPiece->capacity);
         MTIassert(lastListPiece != NULL);
         if (lastListPiece != NULL)
         {
            try {

            listPieces.push_back(lastListPiece);

            } catch (...) {
               MTIassert(false);
               //should rethrow? QQQ
            }
         }
         else
         {
            // What do I do now? Silently fail?
            return;
         }
      }
   }
   size_t nextIndex = lastListPiece->capacity - lastListPiece->remaining;

   lastListPiece->ptrs[nextIndex] = pixelRegionPtr;

   --(lastListPiece->remaining);
   ++count;
}

KindaLikeAVectorOfPixelRegionPtrs::PieceOfTheList *
KindaLikeAVectorOfPixelRegionPtrs::allocPiece(size_t firstIndex)
{
   PieceOfTheList *piece = NULL;
   size_t allocCount = MAX_PIXEL_REGION_LIST_ALLOC_COUNT;

   while (piece == NULL && allocCount > 0)
   {
      // in the following, "- 1" because PieceOfTheList includes one entry
      size_t allocSize = (sizeof(CPixelRegion *) * (allocCount - 1)) +
                           sizeof(PieceOfTheList);
      piece = (PieceOfTheList *) MTImalloc(allocSize);
      if (piece == NULL)
      {
         allocCount /= 2;
         TRACE_3(errout << "///// PRL AllocPiece: Can't fit "
                        << (allocCount * 2) << ", trying "
                        << allocCount);
      }
   }
   if (piece != NULL)
   {
      piece->capacity = piece->remaining = allocCount;
      piece->firstIndex = firstIndex;
   }

   return piece;
}
