// AudioFormat3.h: interface for the CAudioFormat class.
//
// Created by: John Starr, December 17, 2001
//
/* CVS Info:
$Header: /usr/local/filmroot/format/include/AudioFormat3.h,v 1.6 2005/10/06 18:12:18 starr Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef AUDIO_FORMAT3_H
#define AUDIO_FORMAT3_H

#include "MediaFormat.h"
#include "AudioInfo.h"
#include "ImageInfo.h"
#include "MTIstringstream.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CTimecode;

//////////////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CAudioFormat : public CMediaFormat
{
public:
   CAudioFormat();
   CAudioFormat(const CAudioFormat &src);
   virtual ~CAudioFormat();

   // Virtual "default constructor" returns new instance with same type
   // as *this.  Note: this requires that compiler support "covariant return
   // types."  If compiler does not support it, then conditionally compile
   // to return CMediaFormat* and see what else breaks.
//   CAudioFormat* construct() const {return new CAudioFormat;};
   CMediaFormat* construct() const {return new CAudioFormat;};
   // Virtual "copy constructor"
//   CAudioFormat* clone() const {return new CAudioFormat(*this);};
   CMediaFormat* clone() const {return new CAudioFormat(*this);};

   CMediaFormat& assign(const CMediaFormat &src);

   CAudioFormat& operator=(const CAudioFormat& rhs);

   // Accessor Functions
   int getBytesPerSample() const;
   int getChannelCount() const;
   int getSampleCountAtTimecode(const CTimecode& timecode,
                                int fieldNumber) const;
   EAudioSampleEndian getSampleDataEndian() const;
   EAudioSamplePacking getSampleDataPacking() const;
   int getSampleDataSize() const;
   EAudioSampleType getSampleDataType() const;
   double getSamplesPerField() const;
   double getSampleRate() const;
   EAudioSampleRate getSampleRateEnum() const;

   // Mutator Functions
   void setChannelCount(int newChannelCount);
   void setSampleDataEndian(EAudioSampleEndian newSampleDataEndian);
   void setSampleDataSize(int newSampleDataSize);
   void setSampleDataPacking(EAudioSamplePacking newSampleDataPacking);
   void setSampleDataType(EAudioSampleType newSampleDataType);
   void setSamplesPerField(double newSamplesPerField);
   void setSampleRate(EAudioSampleRate newSampleRateEnum,
                      double newSampleRate);

   void setToDefaults(EImageFormatType newImageFormatType,
                      int newChannelCount,
                      EAudioSampleRate newSampleRateEnum,
                      double newSampleRate,
                      EAudioSampleType newSampleDataType,
                      int newSampleDataSize,
                      EAudioSamplePacking newSampleDataPacking);

   void setToDefaults();

   // Validation functions
   int validate() const;

   int readSection(CIniFile *iniFile, const string& sectionPrefix);
   int readClipInitSection(CIniFile *iniFile, const string& sectionPrefix);
   int writeSection(CIniFile *iniFile, const string& sectionPrefix);
   int writeClipInitSection(CIniFile *iniFile, const string& sectionPrefix);

   void dump(MTIostringstream &str) const;      // For testing;

private:

   int channelCount;

   EAudioSampleRate sampleRateEnum;
   double sampleRate;        // Audio sample rate

   double samplesPerField;  // Number of samples per field

   EAudioSampleType sampleDataType; // int8, int16, int24, int32, uint, real
   int sampleDataSize;              // 8, 16, 20, 24, 32, etc.
   EAudioSamplePacking sampleDataPacking; // left or right justified, 3 bytes
   EAudioSampleEndian sampleDataEndian;   // Big or little endian data

private:
   static string audioFormatSectionName;
   static string channelCountKey;
   static string sampleDataPackingKey;
   static string sampleDataSizeKey;
   static string sampleDataTypeKey;
   static string sampleRateKey;
   static string samplesPerFieldKey;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef AUDIO_FORMAT3_H
