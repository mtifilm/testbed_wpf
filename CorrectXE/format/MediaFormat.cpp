// MediaFormat.cpp: implementation of the CMediaFormat class.
//
//////////////////////////////////////////////////////////////////////

#include "err_format.h"
#include "MediaFormat.h"
#include "IniFile.h"

//////////////////////////////////////////////////////////////////////
// Static Data Members Initialization
//////////////////////////////////////////////////////////////////////

string CMediaFormat::interlacedKey = "Interlaced";

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMediaFormat::CMediaFormat(EMediaFormatType newMediaFormatType)
: mediaFormatType(newMediaFormatType), interlaced(false)
{

}

// Copy Constructor
CMediaFormat::CMediaFormat(const CMediaFormat &src)
: mediaFormatType(src.getMediaFormatType())
{
   *this = src;	// Use assignment operator for deep copy
}

CMediaFormat::~CMediaFormat()
{

}

void CMediaFormat::operator=(const CMediaFormat& src)
{
   // Note: This function is protected since CMediaFormat is an
   //       abstract base class. 
 
   if (this == &src)       // Check for self-assignment
      return;
   
   mediaFormatType = src.mediaFormatType;
   interlaced = src.interlaced;
}

bool CMediaFormat::compare(const CMediaFormat& rhs) const
{
   // Note: This function is protected since CMediaFormat is an
   //       abstract base class.

   if (this == &rhs)       // Check for self-compare
      return true;

   if(mediaFormatType != rhs.mediaFormatType)
      return false;
   if(interlaced != rhs.interlaced)
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////
// File Interface
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CMediaFormat::readSection(CIniFile *iniFile, const string& sectionName)
{
   // Verify that the appropriate keys exist within the section
   // These keys are not optional and do not have default values
   if (!iniFile->KeyExists(sectionName, interlacedKey))
      {
      // ERROR: Media Format section key does not exist
      return -350;
      }

   // Read Interlaced
   bool newInterlaced = iniFile->ReadBool(sectionName, interlacedKey,
                                          getInterlaced());

   CMediaFormat::setInterlaced(newInterlaced);

   // Success
   return 0;

} // readSection

//------------------------------------------------------------------------
//
// Function:    readSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CMediaFormat::readClipInitSection(CIniFile *iniFile,
                                       const string& sectionName)
{
   // Read Interlaced, if it is present
   if (iniFile->KeyExists(sectionName, interlacedKey))
      {
      bool newInterlaced = iniFile->ReadBool(sectionName, interlacedKey,
                                             getInterlaced());

      CMediaFormat::setInterlaced(newInterlaced);
      }

   // Success
   return 0;

} // readClipInitSection

//------------------------------------------------------------------------
//
// Function:    writeSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CMediaFormat::writeSection(CIniFile *iniFile, const string& sectionName)
{
   // Write Interlaced
   iniFile->WriteBool(sectionName, interlacedKey, getInterlaced());

   return 0;

} // writeSection

//////////////////////////////////////////////////////////////////////
// Accessor Functions
//////////////////////////////////////////////////////////////////////

int CMediaFormat::getFieldCount() const
{
   return ((interlaced) ? 2 : 1);
}

bool  CMediaFormat::getInterlaced() const
{
   return interlaced;
}

EMediaFormatType CMediaFormat::getMediaFormatType() const
{
   return mediaFormatType;
}

//////////////////////////////////////////////////////////////////////
// Mutator Functions
//////////////////////////////////////////////////////////////////////

void CMediaFormat::setInterlaced(bool newInterlaced)
{
   interlaced = newInterlaced;
}

//////////////////////////////////////////////////////////////////////
// Static Query Methods
//////////////////////////////////////////////////////////////////////

const char* CMediaFormat::
queryMediaFormatTypeName(EMediaFormatType mediaFormatType)
{
   const char* mediaFormatTypeName = 0;

   switch(mediaFormatType)
      {
      case MEDIA_FORMAT_TYPE_INVALID :
         mediaFormatTypeName = "Invalid";
         break;
      case MEDIA_FORMAT_TYPE_VIDEO2 :
      case MEDIA_FORMAT_TYPE_VIDEO3 :
         mediaFormatTypeName = "Video";
         break;
      case MEDIA_FORMAT_TYPE_AUDIO2 :
      case MEDIA_FORMAT_TYPE_AUDIO3 :
         mediaFormatTypeName = "Audio";
         break;
      default :
         mediaFormatTypeName = "Unknown";
         break;
      }

   return mediaFormatTypeName;
}

//////////////////////////////////////////////////////////////////////
// Serialization functions
//////////////////////////////////////////////////////////////////////

bool CMediaFormat::serialize(ostream &osStream) const
{
    osStream.write((char *)&mediaFormatType, sizeof(EMediaFormatType));
    if (osStream.rdstate() != ios::goodbit) return false;

    osStream.write((char *)&interlaced, sizeof(interlaced));
    if (osStream.rdstate() != ios::goodbit) return false;

    return true;
}

bool CMediaFormat::readFromStream(istream &isStream)
{
    isStream.read((char *)&mediaFormatType, sizeof(EMediaFormatType));
    if (isStream.rdstate() != ios::goodbit) return false;

    isStream.read((char *)&interlaced, sizeof(interlaced));
    if (isStream.rdstate() != ios::goodbit) return false;

    return true;
}


//////////////////////////////////////////////////////////////////////
// Validation functions
//////////////////////////////////////////////////////////////////////

int CMediaFormat::validate() const
{
   // Check the media format type
   switch(mediaFormatType)
      {
      case MEDIA_FORMAT_TYPE_VIDEO2 :
      case MEDIA_FORMAT_TYPE_VIDEO3 :
      case MEDIA_FORMAT_TYPE_AUDIO2 :
      case MEDIA_FORMAT_TYPE_AUDIO3 :
         // Okay
         break;
      case MEDIA_FORMAT_TYPE_INVALID :
      default :
         return FORMAT_ERROR_INVALID_MEDIA_FORMAT_TYPE;
      }

   return 0;   // Success
}

//////////////////////////////////////////////////////////////////////
// Testing Methods
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    dump
//
// Description: 
//
// Arguments    
//
// Returns:     none
//
//------------------------------------------------------------------------
void CMediaFormat::dump(MTIostringstream &str) const
{
   str << "  mediaFormatType: " << queryMediaFormatTypeName(getMediaFormatType())
       << " (" << getMediaFormatType() << ")"
       << "  interlaced: " << getInterlaced() 
       << " fieldCount: " << getFieldCount()
       << std::endl;
}

