/*
	Name:	ColorSpaceConvert.cpp
	By:	Kevin Manbeck
	Date:	March 7, 2001

	The ColorSpaceConvert code is used to convert from one image to
	another.
*/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>

#include "ImageFormat3.h"
#include "ColorSpaceConvert.h"
#include "Extractor.h"
#include "mt_math.h"
#include "err_format.h"
#include "MTImalloc.h"

//#define pow powf

CColorSpaceConvert::CColorSpaceConvert()
{
  ccflpLUT = NULL;
  uspTmp = NULL;

  lProcessFlow = PF_NONE;
  bInitialized = false;
  bEstablishLUT = false;
  bWeOwnTheFancyLUT = false;

  ifpSrc = 0;
  ifpDst = 0;

  fpLUT = NULL;
  fpWeiLB = NULL;
  fpWeiUB = NULL;

  bUseApproxConvert = false;

  lRandIndex = 0;

}  /* CColorSpaceConvert */

CColorSpaceConvert::~CColorSpaceConvert()
{
  Free ();
}  /* ~CColorSpaceConvert */

void CColorSpaceConvert::Free ()
{
  if (bWeOwnTheFancyLUT)
    delete ccflpLUT;
  ccflpLUT = NULL;
  bWeOwnTheFancyLUT = false;

  MTIfree(uspTmp);
  uspTmp = NULL;

  lProcessFlow = PF_NONE;
  bInitialized = false;
  bEstablishLUT = false;

  delete ifpSrc;
  ifpSrc = 0;
  delete ifpDst;
  ifpDst = 0;

  MTIfree(fpLUT);
  fpLUT = NULL;
  MTIfree(fpWeiLB);
  fpWeiLB = NULL;
  MTIfree(fpWeiUB);
  fpWeiUB = NULL;
}  /* Free */

int CColorSpaceConvert::Init (const CImageFormat *ifpSrcArg,
		const CImageFormat *ifpDstArg,
        CColorConvertFancyLUT *ccflpLUTArg,
        int iDitherSeed, int iSourceLogSpace)
{
  int iRet;

  Free ();

  ifpSrc = new CImageFormat();
  ifpDst = new CImageFormat();

  *ifpSrc = *ifpSrcArg;
  *ifpDst = *ifpDstArg;

  if (ccflpLUTArg != NULL)
   {
    ccflpLUT = ccflpLUTArg;
    bWeOwnTheFancyLUT = false;
   }

  if (
	(ifpSrc->getLinesPerFrame() + ifpSrc->getVerticalIntervalRows()) !=
	(ifpDst->getLinesPerFrame() + ifpDst->getVerticalIntervalRows())
     )
   {
    return FORMAT_ERROR_VERTICAL_DIM;
   }
  else
   {
    lNRow = ifpSrc->getLinesPerFrame() + ifpSrc->getVerticalIntervalRows();
   }

  if (ifpSrc->getPixelsPerLine() != ifpDst->getPixelsPerLine())
   {
    return FORMAT_ERROR_HORIZONTAL_DIM;
   }
  else
   {
    lNCol = ifpSrc->getPixelsPerLine();
   }

  iRet = EstablishParam (&cssSrc, ifpSrc, iSourceLogSpace);
  if (iRet)
   {
    return iRet;
   }

  iRet = EstablishParam (&cssDst, ifpDst, -1);
  if (iRet)
   {
    return iRet;
   }

  lRandIndex = iDitherSeed % NUM_RAND;

  bInitialized = true;

  return 0;
}  /* Init */

int CColorSpaceConvert::EstablishParam (COLOR_SPACE_STRUCT *cssp,
		CImageFormat *ifp, int iLogSpace)
{
  float *fpMat[MAX_COMPONENT], *fpInv[MAX_COMPONENT];
  int iRet;
  EColorSpace csTransferCharacteristic = IF_COLOR_SPACE_INVALID;

  cssp->lNComponents = 3;

/*
	Establish the Color Representation Matrix
*/

  if (
	(ifp->getPixelComponents() == IF_PIXEL_COMPONENTS_YUV422) ||
	(ifp->getPixelComponents() == IF_PIXEL_COMPONENTS_YUV444)
     )
   {
    switch (ifp->getColorSpace())
     {
      case IF_COLOR_SPACE_SMPTE_240:
			/* The RGB to YCbCr matrix */
        cssp->faColorRepresentation[0][0] = 0.212;	/* R to Y */
        cssp->faColorRepresentation[0][1] = 0.701;	/* G to Y */
        cssp->faColorRepresentation[0][2] = 0.087;	/* B to Y */
        cssp->faColorRepresentation[1][0] = -0.1161;	/* R to Cb (U) */
        cssp->faColorRepresentation[1][1] = -0.3839;	/* G to Cb (U) */
        cssp->faColorRepresentation[1][2] = 0.5;	/* B to Cb (U) */
        cssp->faColorRepresentation[2][0] = 0.5;	/* R to Cr (V) */
        cssp->faColorRepresentation[2][1] = -0.4448;	/* G to Cr (V) */
        cssp->faColorRepresentation[2][2] = -0.0552;	/* B to Cr (V) */
      break;
      case IF_COLOR_SPACE_CCIR_601_M:
      case IF_COLOR_SPACE_CCIR_601_BG:
			/* The RGB to YCbCr matrix */
        cssp->faColorRepresentation[0][0] = 0.299;	/* R to Y */
        cssp->faColorRepresentation[0][1] = 0.587;	/* G to Y */
        cssp->faColorRepresentation[0][2] = 0.114;	/* B to Y */
        cssp->faColorRepresentation[1][0] = -0.1687;	/* R to Cb (U) */
        cssp->faColorRepresentation[1][1] = -0.3313;	/* G to Cb (U) */
        cssp->faColorRepresentation[1][2] = 0.5;	/* B to Cb (U) */
        cssp->faColorRepresentation[2][0] = 0.5;	/* R to Cr (V) */
        cssp->faColorRepresentation[2][1] = -0.4187;	/* G to Cr (V) */
        cssp->faColorRepresentation[2][2] = -0.0813;	/* B to Cr (V) */
      break;
      case IF_COLOR_SPACE_CCIR_709:
			/* The RGB to YCbCr matrix */
        cssp->faColorRepresentation[0][0] = 0.2125;	/* R to Y */
        cssp->faColorRepresentation[0][1] = 0.7154;	/* G to Y */
        cssp->faColorRepresentation[0][2] = 0.0721;	/* B to Y */
        cssp->faColorRepresentation[1][0] = -0.1145;	/* R to Cb (U) */
        cssp->faColorRepresentation[1][1] = -0.3855;	/* G to Cb (U) */
        cssp->faColorRepresentation[1][2] = 0.5;	/* B to Cb (U) */
        cssp->faColorRepresentation[2][0] = 0.5;	/* R to Cr (V) */
        cssp->faColorRepresentation[2][1] = -0.4542;	/* G to Cr (V) */
        cssp->faColorRepresentation[2][2] = -0.0458;	/* B to Cr (V) */
      break;
      default:
        return FORMAT_ERROR_BAD_COLORSPACE;
     }
   }
  else if (ifp->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB
	    || ifp->getPixelComponents() == IF_PIXEL_COMPONENTS_BGR
       || ifp->getPixelComponents() == IF_PIXEL_COMPONENTS_RGBA
       || ifp->getPixelComponents() == IF_PIXEL_COMPONENTS_BGRA)
   {
    switch (ifp->getColorSpace())
     {
      default:
			/* The RGB to RGB matrix */
        cssp->faColorRepresentation[0][0] = 1.0;	/* R to R */
        cssp->faColorRepresentation[0][1] = 0.0;	/* G to R */
        cssp->faColorRepresentation[0][2] = 0.0;	/* B to R */
        cssp->faColorRepresentation[1][0] = 0.0;	/* R to G */
        cssp->faColorRepresentation[1][1] = 1.0;	/* G to G */
        cssp->faColorRepresentation[1][2] = 0.0;	/* B to G */
        cssp->faColorRepresentation[2][0] = 0.0;	/* R to B */
        cssp->faColorRepresentation[2][1] = 0.0;	/* G to B */
        cssp->faColorRepresentation[2][2] = 1.0;	/* B to B */
      }
   }
//  else if (ifp->getPixelComponents() == IF_PIXEL_COMPONENTS_LUMINANCE)
//   {
//    cssp->lNComponents = 1;
//
//    switch (ifp->getColorSpace())
//     {
//      default:
//			/* The RGB to LUMINANCE matrix (Y from the CCIR-601 matrix) */
//        cssp->faColorRepresentation[0][0] = 0.299;	/* R to Y */
//        cssp->faColorRepresentation[0][1] = 0.587;	/* G to Y */
//        cssp->faColorRepresentation[0][2] = 0.114;	/* B to Y */
//        cssp->faColorRepresentation[1][0] = -0.1687;	/* R to Cb (U) */
//        cssp->faColorRepresentation[1][1] = -0.3313;	/* G to Cb (U) */
//        cssp->faColorRepresentation[1][2] = 0.5;	/* B to Cb (U) */
//        cssp->faColorRepresentation[2][0] = 0.5;	/* R to Cr (V) */
//        cssp->faColorRepresentation[2][1] = -0.4187;	/* G to Cr (V) */
//        cssp->faColorRepresentation[2][2] = -0.0813;	/* B to Cr (V) */
//      }
//   }
  else if (ifp->getPixelComponents() == IF_PIXEL_COMPONENTS_Y
       || ifp->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY)
   {
    cssp->lNComponents = 3;

    // Pass thru as if RGB - all channels should be the same!
    // maybe should set them all to one channel, e.g. RED? QQQ
    switch (ifp->getColorSpace())
     {
      default:
        cssp->faColorRepresentation[0][0] = 1.0;	/* R to R */
        cssp->faColorRepresentation[0][1] = 0.0;	/* G to R */
        cssp->faColorRepresentation[0][2] = 0.0;	/* B to R */
        cssp->faColorRepresentation[1][0] = 0.0;	/* R to G */
        cssp->faColorRepresentation[1][1] = 1.0;	/* G to G */
        cssp->faColorRepresentation[1][2] = 0.0;	/* B to G */
        cssp->faColorRepresentation[2][0] = 0.0;	/* R to B */
        cssp->faColorRepresentation[2][1] = 0.0;	/* G to B */
        cssp->faColorRepresentation[2][2] = 1.0;	/* B to B */
      }
   }
  else
   {
    return FORMAT_ERROR_PIXEL_COMPONENT;
   }

  fpMat[0] = cssp->faColorRepresentation[0];
  fpMat[1] = cssp->faColorRepresentation[1];
  fpMat[2] = cssp->faColorRepresentation[2];
  fpInv[0] = cssp->faColorRepresentationInv[0];
  fpInv[1] = cssp->faColorRepresentationInv[1];
  fpInv[2] = cssp->faColorRepresentationInv[2];

  iRet = InvertMatrix (fpMat, fpInv, MAX_COMPONENT);
  if (iRet)
   {
    return iRet;
   }

/*
	Establish the values in the TransferFunction LUT

	We map the floating point value 0.0 to index=0.  We map the
	floating point value 1.0 to index = lNEntryLUT/4.
*/
  csTransferCharacteristic = ifp->getTransferCharacteristic();
  if (csTransferCharacteristic == IF_COLOR_SPACE_INVALID)
   {
    csTransferCharacteristic = ifp->getColorSpace();     // default
   }

  switch (csTransferCharacteristic)
   {
    case IF_COLOR_SPACE_INVALID:
      return FORMAT_ERROR_BAD_COLORSPACE;

    case IF_COLOR_SPACE_SMPTE_240:
			/* The RGB to YCbCr matrix */
      cssp->fTransferFunctionGain = 4.0;
      cssp->fTransferFunctionBreakPoint = 0.0228;
      cssp->fTransferFunctionGamma = 2.2222;
    break;
    case IF_COLOR_SPACE_CCIR_601_M:
    case IF_COLOR_SPACE_CCIR_601_BG:
    case IF_COLOR_SPACE_CCIR_709:
      cssp->fTransferFunctionGain = 4.5;
      cssp->fTransferFunctionBreakPoint = 0.018;
      cssp->fTransferFunctionGamma = 2.2222;
    break;
    case IF_COLOR_SPACE_LOG:
    case IF_COLOR_SPACE_LINEAR:
    case IF_COLOR_SPACE_EXR:
    default:
      // Theoretically the gain doesn't matter because the breakpoint is 0,
      // but I had an issue with the LUMINANCE case hack where it tried to
      // divide by it, so I just set it to 1.0 for now - mbraca
      cssp->fTransferFunctionGain = 1.0;
      cssp->fTransferFunctionBreakPoint = 0.0;
      if (CImageInfo::isGammaValid(ifp->getGamma())
      == FORMAT_ERROR_INVALID_GAMMA)
       {
        cssp->fTransferFunctionGamma = 1.7;
       }
      else
       {
        cssp->fTransferFunctionGamma = ifp->getGamma();
       }
    break;
   }

/*
	Establish the LUT for DataEncode
*/


  if (ifp->getColorSpace() == IF_COLOR_SPACE_LOG || ifp->getColorSpace() == IF_COLOR_SPACE_EXR)
   {
    cssp->lDataEncodeFunctionType = DATA_ENCODE_FUNCTION_TYPE_PRINT_DENSITY;
   }
  else
   {
    cssp->lDataEncodeFunctionType = DATA_ENCODE_FUNCTION_TYPE_LINEAR;
   }

  if (iLogSpace != -1)
   {
    if (iLogSpace == true)
      cssp->lDataEncodeFunctionType = DATA_ENCODE_FUNCTION_TYPE_PRINT_DENSITY;
    else
      cssp->lDataEncodeFunctionType = DATA_ENCODE_FUNCTION_TYPE_LINEAR;
   }

  MTI_UINT16 componentValues[MAX_COMPONENT_COUNT];

  // set the lower bound
  ifp->getComponentValueMin (componentValues);
  cssp->laDataEncodeMinimum[0] = componentValues[0];
  cssp->laDataEncodeMinimum[1] = componentValues[1];
  cssp->laDataEncodeMinimum[2] = componentValues[2];

  // set the upper bound
  ifp->getComponentValueMax (componentValues);
  cssp->laDataEncodeMaximum[0] = componentValues[0];
  cssp->laDataEncodeMaximum[1] = componentValues[1];
  cssp->laDataEncodeMaximum[2] = componentValues[2];

  // set the lower clip point
  ifp->getComponentValueMin (componentValues);
  cssp->laDataEncodeClipLower[0] = componentValues[0];
  cssp->laDataEncodeClipLower[1] = componentValues[1];
  cssp->laDataEncodeClipLower[2] = componentValues[2];

  // set the upper clip point
  ifp->getComponentValueMax (componentValues);
  cssp->laDataEncodeClipUpper[0] = componentValues[0];
  cssp->laDataEncodeClipUpper[1] = componentValues[1];
  cssp->laDataEncodeClipUpper[2] = componentValues[2];

/*
	Now establish some derived parameters
*/

  FindDataEncodeFactors (cssp);
  FindTransferFunctionNormalize (cssp);

  return 0;
}  /* EstablishParam */

int CColorSpaceConvert::SetColorRepresentation (bool bSrc,
	float faRGBtoXYZ[MAX_COMPONENT][MAX_COMPONENT])
/*
	faRGBtoXYZ is the 2D matrix that converts R,G,B space into the
	desired color space
*/
{
  float *fpMat[MAX_COMPONENT], *fpInv[MAX_COMPONENT];
  int iRet;
  COLOR_SPACE_STRUCT *cssp;

  if (bSrc)
    cssp = &cssSrc;
  else
    cssp = &cssDst;

  for (int iRow = 0; iRow < MAX_COMPONENT; iRow++)
  for (int iCol = 0; iCol < MAX_COMPONENT; iCol++)
   {
    cssp->faColorRepresentation[iRow][iCol] = faRGBtoXYZ[iRow][iCol];
   }

  fpMat[0] = cssp->faColorRepresentation[0];
  fpMat[1] = cssp->faColorRepresentation[1];
  fpMat[2] = cssp->faColorRepresentation[2];
  fpInv[0] = cssp->faColorRepresentationInv[0];
  fpInv[1] = cssp->faColorRepresentationInv[1];
  fpInv[2] = cssp->faColorRepresentationInv[2];

  iRet = InvertMatrix (fpMat, fpInv, MAX_COMPONENT);
  if (iRet)
   {
    return iRet;
   }

  return 0;
}  /* SetColorRepresentation */

void CColorSpaceConvert::SetTransferFunction (bool bSrc,
      float fGain, float fBreakPoint, float fGamma)
{
  COLOR_SPACE_STRUCT *cssp;

  if (bSrc)
    cssp = &cssSrc;
  else
    cssp = &cssDst;

  cssp->fTransferFunctionGain = fGain;
  cssp->fTransferFunctionBreakPoint = fBreakPoint;
  cssp->fTransferFunctionGamma = fGamma;

  FindDataEncodeFactors (cssp);

}  /* SetTransferFunction */

void CColorSpaceConvert::SetEncodeParameters (bool bSrc,
	long laMin[MAX_COMPONENT], long laMax[MAX_COMPONENT],
	long laLower[MAX_COMPONENT], long laUpper[MAX_COMPONENT])
{
  COLOR_SPACE_STRUCT *cssp;

  if (bSrc)
    cssp = &cssSrc;
  else
    cssp = &cssDst;

  for (int iCnt = 0; iCnt < MAX_COMPONENT; iCnt++)
   {
    cssp->laDataEncodeMinimum[iCnt] = laMin[iCnt];
    cssp->laDataEncodeMaximum[iCnt] = laMax[iCnt];
    cssp->laDataEncodeClipLower[iCnt] = laLower[iCnt];
    cssp->laDataEncodeClipUpper[iCnt] = laUpper[iCnt];
   }

  FindTransferFunctionNormalize (cssp);
}  /* SetEncodeParameters */

void CColorSpaceConvert::GetColorRepresentation (bool bSrc,
	float faRGBtoXYZ[MAX_COMPONENT][MAX_COMPONENT])
/*
	faRGBtoXYZ is the 2D matrix that converts R,G,B space into the
	desired color space
*/
{
  COLOR_SPACE_STRUCT *cssp;

  if (bSrc)
    cssp = &cssSrc;
  else
    cssp = &cssDst;

  for (int iRow = 0; iRow < MAX_COMPONENT; iRow++)
  for (int iCol = 0; iCol < MAX_COMPONENT; iCol++)
   {
    faRGBtoXYZ[iRow][iCol] = cssp->faColorRepresentation[iRow][iCol];
   }
}  /* GetColorRepresentation */

void CColorSpaceConvert::GetTransferFunction (bool bSrc,
	float &fGain, float &fBreakPoint, float &fGamma)
{
  COLOR_SPACE_STRUCT *cssp;

  if (bSrc)
    cssp = &cssSrc;
  else
    cssp = &cssDst;


  fGain = cssp->fTransferFunctionGain;
  fBreakPoint = cssp->fTransferFunctionBreakPoint;
  fGamma = cssp->fTransferFunctionGamma;
}  /* GetTransferFunction */

void CColorSpaceConvert::GetEncodeParameters (bool bSrc,
	long laMin[MAX_COMPONENT], long laMax[MAX_COMPONENT],
	long laLower[MAX_COMPONENT], long laUpper[MAX_COMPONENT])
{
  COLOR_SPACE_STRUCT *cssp;

  if (bSrc)
    cssp = &cssSrc;
  else
    cssp = &cssDst;

  for (int iCnt = 0; iCnt < MAX_COMPONENT; iCnt++)
   {
    laMin[iCnt] = cssp->laDataEncodeMinimum[iCnt];
    laMax[iCnt] = cssp->laDataEncodeMaximum[iCnt];
    laLower[iCnt] = cssp->laDataEncodeClipLower[iCnt];
    laUpper[iCnt] = cssp->laDataEncodeClipUpper[iCnt];
   }
}  /* GetEncodeParameters */

void CColorSpaceConvert::MatrixCombine (long lX0, long lX1, long lX2,
	long *lpY0, long *lpY1, long *lpY2,
	long *lpLoc[MAX_COMPONENT][MAX_COMPONENT])
{
/*
	If lX? is negative, subtract the value instead of add
*/
  *lpY0 = 0;
  *lpY1 = 0;
  *lpY2 = 0;

  if (lX0 < 0)
   {
    lX0 = -lX0;
    if (lX0 >= ccflpLUT->lNEntryLUT)
      lX0 = ccflpLUT->lNEntryLUT - 1;
    *lpY0 -= lpLoc[0][0][lX0];
    *lpY1 -= lpLoc[1][0][lX0];
    *lpY2 -= lpLoc[2][0][lX0];
   }
  else
   {
    if (lX0 >= ccflpLUT->lNEntryLUT)
      lX0 = ccflpLUT->lNEntryLUT - 1;
    *lpY0 += lpLoc[0][0][lX0];
    *lpY1 += lpLoc[1][0][lX0];
    *lpY2 += lpLoc[2][0][lX0];
   }

  if (lX1 < 0)
   {
    lX1 = -lX1;
    if (lX1 >= ccflpLUT->lNEntryLUT)
      lX1 = ccflpLUT->lNEntryLUT - 1;
    *lpY0 -= lpLoc[0][1][lX1];
    *lpY1 -= lpLoc[1][1][lX1];
    *lpY2 -= lpLoc[2][1][lX1];
   }
  else
   {
    if (lX1 >= ccflpLUT->lNEntryLUT)
      lX1 = ccflpLUT->lNEntryLUT - 1;
    *lpY0 += lpLoc[0][1][lX1];
    *lpY1 += lpLoc[1][1][lX1];
    *lpY2 += lpLoc[2][1][lX1];
   }

  if (lX2 < 0)
   {
    lX2 = -lX2;
    if (lX2 >= ccflpLUT->lNEntryLUT)
      lX2 = ccflpLUT->lNEntryLUT - 1;
    *lpY0 -= lpLoc[0][2][lX2];
    *lpY1 -= lpLoc[1][2][lX2];
    *lpY2 -= lpLoc[2][2][lX2];
   }
  else
   {
    if (lX2 >= ccflpLUT->lNEntryLUT)
      lX2 = ccflpLUT->lNEntryLUT - 1;
    *lpY0 += lpLoc[0][2][lX2];
    *lpY1 += lpLoc[1][2][lX2];
    *lpY2 += lpLoc[2][2][lX2];
   }

  return;
}  /* MatrixCombine */

int CColorSpaceConvert::EstablishLUTApprox ()
{
  long lMaxVal, lMaxValBit;
  MTI_UINT16 usSrc0, usSrc1, usSrc2;
  long lIndex0, lIndex1, lIndex2, lVal, lLB, lUB, lDiffLB, lDiffUB;
  float fDst0, fDst1, fDst2, *fp, fNormalization;

printf ("TTT Doing LUT\n");

/*
	Allocate Storage
*/

  lNIndex = (1 << INDEX_SHIFT);

  // find max value as a power of two
  lMaxVal = cssSrc.laDataEncodeClipUpper[0];
  if (cssSrc.laDataEncodeClipUpper[1] > lMaxVal)
    lMaxVal = cssSrc.laDataEncodeClipUpper[1];
  if (cssSrc.laDataEncodeClipUpper[2] > lMaxVal)
    lMaxVal = cssSrc.laDataEncodeClipUpper[2];

  lMaxValBit = 0;
  while (lMaxVal > (1<<lMaxValBit))
   {
    lMaxValBit++;
   }

  // the amount to shift the source value to get the lower bound
  lLBShift = lMaxValBit - INDEX_SHIFT;
  if (lLBShift < 0)
    lLBShift = 0;

  // allocate the lookup table
  fpLUT = (float *) MTImalloc (lNIndex * lNIndex * lNIndex * MAX_COMPONENT *
		sizeof (float));
  if (fpLUT == NULL)
   {
    return FORMAT_ERROR_MALLOC;
   }

  // run through all the indices and compute the look up values
  fp = fpLUT;
  for (lIndex0 = 0; lIndex0 < lNIndex; lIndex0++)
  for (lIndex1 = 0; lIndex1 < lNIndex; lIndex1++)
  for (lIndex2 = 0; lIndex2 < lNIndex; lIndex2++)
   {
    usSrc0 = (lIndex0<<lLBShift);
    usSrc1 = (lIndex1<<lLBShift);
    usSrc2 = (lIndex2<<lLBShift);

    ConvertOneValue (usSrc0, usSrc1, usSrc2, &fDst0, &fDst1, &fDst2);

    *fp++ = fDst0;
    *fp++ = fDst1;
    *fp++ = fDst2;
   }

  fpWeiLB = (float *) MTImalloc ((1<<lMaxValBit) * sizeof(float));
  if (fpWeiLB == NULL)
   {
    return FORMAT_ERROR_MALLOC;
   }

  fpWeiUB = (float *) MTImalloc ((1<<lMaxValBit) * sizeof(float));
  if (fpWeiUB == NULL)
   {
    return FORMAT_ERROR_MALLOC;
   }

  fNormalization = 1. / (3. * (float)(1<<lLBShift));

  for (lVal = 0; lVal < (1<<lMaxValBit); lVal++)
   {
    lLB = (lVal >> lLBShift);
    lUB = (lLB+1) & (lNIndex - 1);
    lDiffLB = lVal - (lLB<<lLBShift);
    lDiffUB = (lUB<<lLBShift) - lVal;

    fpWeiLB[lVal] = fNormalization * (float)lDiffUB;
    fpWeiUB[lVal] = fNormalization * (float)lDiffLB;
   }

printf ("TTT Finished with LUT\n");

  return 0;
}  /* EstablishLUTApprox */

int CColorSpaceConvert::EstablishFlow ()
{
/*
	Set up the process flow for this conversion.  There are three
	steps:  unpack the data, convert the data, repack the data.

	Each step has several variants which depend on the input and
	output formats.
*/

	// establish the unpacking of the source

  lProcessFlow = PF_NONE;
  EPixelPacking packing = ifpSrc->getPixelPacking();
  if ((packing == IF_PIXEL_PACKING_1_8Bits_IN_2Bytes
       || packing == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes
       || packing == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L
       || packing == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R
       || packing == IF_PIXEL_PACKING_1_12Bits_IN_2Bytes
       || packing == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT)
  && ifpSrc->getFieldCount() == 1
  && (ifpSrc->getPixelComponents() == IF_PIXEL_COMPONENTS_YUV444
       || ifpSrc->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB
       || ifpSrc->getPixelComponents() == IF_PIXEL_COMPONENTS_BGR
//       || ifpSrc->getPixelComponents() == IF_PIXEL_COMPONENTS_LUMINANCE
       || ifpSrc->getPixelComponents() == IF_PIXEL_COMPONENTS_Y
       || ifpSrc->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY))
   {
    lProcessFlow |= PF_UNPACK_NO;
   }
  else
   {
    lProcessFlow |= PF_UNPACK_YES;
   }

	// establish the conversion from the source to the destination

  if (DifferentParameters())
   {
    lProcessFlow |= PF_CONVERT_YES;
   }
  else
   {
    lProcessFlow |= PF_CONVERT_NO;
   }

	// establish the repacking to the destination
  packing = ifpDst->getPixelPacking();
  if (//ifpDst->getBitsPerComponent() == 16 &&
     (packing == IF_PIXEL_PACKING_1_8Bits_IN_2Bytes ||
        packing == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes ||
        packing == IF_PIXEL_PACKING_1_12Bits_IN_2Bytes ||
        packing == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT) &&
		ifpDst->getFieldCount() == 1 &&
		(ifpDst->getPixelComponents() == IF_PIXEL_COMPONENTS_YUV444 ||
		ifpDst->getPixelComponents() == IF_PIXEL_COMPONENTS_RGB ||
		ifpDst->getPixelComponents() == IF_PIXEL_COMPONENTS_BGR
//       || ifpDst->getPixelComponents() == IF_PIXEL_COMPONENTS_LUMINANCE
       || ifpDst->getPixelComponents() == IF_PIXEL_COMPONENTS_Y
       || ifpDst->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY))
   {
    lProcessFlow |= PF_REPACK_NO;
   }
  else
   {
    lProcessFlow |= PF_REPACK_YES;
   }

/*
	If we will be using the temporary buffer, allocate it here

	Look at the chart in ConvertOneFrame() to see when we need
	a temporary buffer.
*/

  if (
	((lProcessFlow & (PF_UNPACK_YES|PF_CONVERT_YES|PF_REPACK_YES)) ==
		(PF_UNPACK_YES|PF_CONVERT_YES|PF_REPACK_YES)) ||
	((lProcessFlow & (PF_UNPACK_YES|PF_CONVERT_NO|PF_REPACK_YES)) ==
		(PF_UNPACK_YES|PF_CONVERT_NO|PF_REPACK_YES)) ||
	((lProcessFlow & (PF_UNPACK_NO|PF_CONVERT_YES|PF_REPACK_YES)) ==
		(PF_UNPACK_NO|PF_CONVERT_YES|PF_REPACK_YES))
     )
   {
    uspTmp = (MTI_UINT16 *) MTImalloc (lNRow * lNCol * MAX_COMPONENT * sizeof(MTI_UINT16));
    if (uspTmp == NULL)
      return FORMAT_ERROR_MALLOC;
   }

  return 0;
}  /* EstablishFlow */

void CColorSpaceConvert::FindDataEncodeFactors (COLOR_SPACE_STRUCT *cssp)
{
  long lComponent;
  float fMinValue, fMaxValue;

  for (lComponent = 0; lComponent < MAX_COMPONENT; lComponent++)
   {
    FindDataEncodeMinMax (cssp, lComponent, &fMinValue, &fMaxValue);

/*
	In the case of PRINT DENSITY encoding, we will be taking the
	log.  To do this we multiply value by 2^PRINT_DENSITY_BIT_DEPTH
*/

    if (cssp->lDataEncodeFunctionType ==
		DATA_ENCODE_FUNCTION_TYPE_PRINT_DENSITY)
     {
/*
	To simply, assume L is in range 0 to 1.  Convert to D in range
	D0 to D1:

                T0 = 10 ^ ((95-685)*.002/.6)
		T = L * (1. - T0) + T0;
		D = D1 - log (T) * (.6 / .002) * ((D1-D0)/(95-685));
*/

		cssp->faDataEncodeSlope[lComponent] =
		-(PRINT_DENSITY_GAMMA / PRINT_DENSITY_INCREMENT) *
	(float)(cssp->laDataEncodeMaximum[lComponent] -
		cssp->laDataEncodeMinimum[lComponent]) /
		(PRINT_DENSITY_BLACK-PRINT_DENSITY_WHITE);

      cssp->faDataEncodeIntercept[lComponent] =
	(float)cssp->laDataEncodeMaximum[lComponent];
     }
    else
     {
      if (fMaxValue > fMinValue)
       {
        cssp->faDataEncodeSlope[lComponent] =
      (float)(cssp->laDataEncodeMaximum[lComponent] -
          cssp->laDataEncodeMinimum[lComponent]) /
          (fMaxValue - fMinValue);
       }
      else
       {
        cssp->faDataEncodeSlope[lComponent] = 0;
       }

      cssp->faDataEncodeIntercept[lComponent] =
	(float)cssp->laDataEncodeMinimum[lComponent] -
		cssp->faDataEncodeSlope[lComponent] * fMinValue;
     }
   }

  return;
}  /* FindDataEncodeFactors */

float CColorSpaceConvert::CalcDataEncodeInv (COLOR_SPACE_STRUCT *cssp,
		long lComponent, long lEncode)
{
  float fEncodeInv, fValue;

  if (lEncode < cssp->laDataEncodeClipLower[lComponent])
    lEncode = cssp->laDataEncodeClipLower[lComponent];
  if (lEncode > cssp->laDataEncodeClipUpper[lComponent])
    lEncode = cssp->laDataEncodeClipUpper[lComponent];

  fValue = ((float)lEncode  - cssp->faDataEncodeIntercept[lComponent]) /
		cssp->faDataEncodeSlope[lComponent];

  if (cssp->lDataEncodeFunctionType ==
		DATA_ENCODE_FUNCTION_TYPE_PRINT_DENSITY)
   {
/*

	Header file describes these equations:

                T0 = 10 ^ ((95-685)*.002/.6)
		T = L * (1. - T0) + T0;
		D = D1 - log (T) * (.6 / .002) * ((D1-D0)/(95-685));

	Here, fValue is log(T).  Need to convert to L in 0. to 1.

*/

    float fTmp0 = pow (10., (float)(PRINT_DENSITY_BLACK - PRINT_DENSITY_WHITE)
		* .002 / .6);

    float fTmp = pow (10., (double)fValue);

    fEncodeInv = (fTmp - fTmp0) / (1. - fTmp0);

    if (fEncodeInv < 0.)
      fEncodeInv = 0.;

    if (fEncodeInv > 1.)
      fEncodeInv = 1.;
   }
  else
   {
    fEncodeInv = fValue;
   }

  return fEncodeInv;
}  /* CalcDataEncodeInv */

float CColorSpaceConvert::CalcDataEncode (COLOR_SPACE_STRUCT *cssp,
		long lComponent, float fValue)
{
  float fEncode, fLogVal;

  if (cssp->lDataEncodeFunctionType ==
		DATA_ENCODE_FUNCTION_TYPE_PRINT_DENSITY)
   {
/*

	Header file describes these equations:

                T0 = 10 ^ ((95-685)*.002/.6)
		T = L * (1. - T0) + T0;
		D = D1 - log (T) * (.6 / .002) * ((D1-D0)/(95-685));

	Here, fValue is L.  Need to convert to log (T).

*/

    float fTmp0 = pow (10., (float)(PRINT_DENSITY_BLACK - PRINT_DENSITY_WHITE)
		* .002 / .6);

    float fTmp = fValue * (1. - fTmp0) + fTmp0;
    if (fTmp <= 0.)
      fTmp = .00001;

    fValue = log10 (fTmp);
   }

  fEncode = fValue * cssp->faDataEncodeSlope[lComponent] +
		cssp->faDataEncodeIntercept[lComponent];

/*
	Clip fEncode into the proper range
*/

  if (fEncode < (float)cssp->laDataEncodeClipLower[lComponent])
    fEncode = (float)cssp->laDataEncodeClipLower[lComponent];
  if (fEncode > (float)cssp->laDataEncodeClipUpper[lComponent])
    fEncode = (float)cssp->laDataEncodeClipUpper[lComponent];

  return fEncode;
}  /* CalcDataEncode */

void CColorSpaceConvert::FindDataEncodeMinMax (COLOR_SPACE_STRUCT *cssp,
		long lComponent, float *fpMin, float *fpMax)
{
  long lC;
  float fMinValue, fMaxValue;

  fMinValue = 0.;
  fMaxValue = 0.;
  for (lC = 0; lC < MAX_COMPONENT; lC++)
   {
    if (cssp->faColorRepresentation[lComponent][lC] < 0.)
     {
      fMinValue += cssp->faColorRepresentation[lComponent][lC];
     }
    else
     {
      fMaxValue += cssp->faColorRepresentation[lComponent][lC];
     }
   }

  *fpMin = fMinValue;
  *fpMax = fMaxValue;

  return;
}  /* FindDataEncodeMinMax */

float CColorSpaceConvert::CalcTransferFunctionInv (COLOR_SPACE_STRUCT *cssp,
		float fTransfer)
{
  float fTransferInv, fValue;

  if (fTransfer < cssp->fTransferFunctionThreshold)
   {
    fTransferInv = fTransfer / cssp->fTransferFunctionGain;
   }
  else
   {
    fValue = (fTransfer - (1. - cssp->fTransferFunctionNormalize)) /
		cssp->fTransferFunctionNormalize;
    fTransferInv = pow((double)fValue, (double)cssp->fTransferFunctionGamma);
   }

  return fTransferInv;
}  /* CalcTransferFunctionInv */

float CColorSpaceConvert::CalcTransferFunction (COLOR_SPACE_STRUCT *cssp,
		float fTransferInv)
{
  float fTransfer;

  if (fTransferInv < cssp->fTransferFunctionBreakPoint)
   {
    fTransfer = cssp->fTransferFunctionGain * fTransferInv;
   }
  else
   {
    fTransfer = cssp->fTransferFunctionNormalize * pow((double)fTransferInv, 1. / cssp->fTransferFunctionGamma)
                + 1. - cssp->fTransferFunctionNormalize;
   }

  return fTransfer;
}  /* CalcTransferFunction */

void CColorSpaceConvert::FindTransferFunctionNormalize
		(COLOR_SPACE_STRUCT *cssp)
{
  assert(cssp->fTransferFunctionGamma > 0);
  cssp->fTransferFunctionThreshold = cssp->fTransferFunctionGain *
	cssp->fTransferFunctionBreakPoint;
  cssp->fTransferFunctionNormalize = (1. - cssp->fTransferFunctionThreshold) /
		(1. - pow ((double)cssp->fTransferFunctionBreakPoint,
			(double)(1./cssp->fTransferFunctionGamma)));

  return;
}  /* FindTransferFunctionNormalize */

int CColorSpaceConvert::ConvertOneFrame (MTI_UINT8 **ucpSrc,
		MTI_UINT8 **ucpDst)
{
  MTI_UINT16 *uspSrcInter;
  MTI_UINT16 *uspDstInter;
  int iRet;

  if (bInitialized == false)
   {
    return FORMAT_ERROR_NOT_INITIALIZED;
   }

  // Finish initialization if not already done.
  iRet = EstablishLUT();
  if (iRet)
    return iRet;

/*
	The process flow is the following:

		1.  Unpack the source image (if necessary)
		2.  Convert from the source color space to the destination
			color space (if necessary)
		3.  Repack the destination image (if necessary)

	We use lProcessFlow to guide us.

	We use intermediate images to hold the unpacked source and
	converted source.  We repack the destination from the intermediate
	image.

	The intermediate images do not have their own storage.  Rather they
	pointer to one of three choices:
		1.  the source image
		2.  the temporary image
		3.  the destination image

	The following chart describes what storage is used for the
	intermediate images:

	Unpack Convert	Repack    SRC Intermediate        DST Intermediate
	Y      Y        Y         Temporary image         Temporary image
	Y      Y        N         Destination image       Destination image
	Y      N        Y         Temporary image         Temporary image
	Y      N        N         Destination image       Destination image
	N      Y        Y         Source image            Temporary image
	N      Y        N         Source image            Destination image
	N      N        Y         Source image            Source image
	N      N        N         Source image            Destination image

	Note that the case of (N, N, Y) uses the source image as the
	DST intermediate image.  This is fine because no conversion takes
	place.  We cannot use the source image for the intermediate
	if there will be a conversion, because this would destroy the
	source data.
*/

		// Assign the SRC intermediate
  if (lProcessFlow & PF_UNPACK_NO)
   {
		// use the source image as the intermediate
    uspSrcInter = (MTI_UINT16 *)ucpSrc[0];
   }
  else
   {
    if (lProcessFlow & PF_REPACK_NO)
     {
		// use the destination image as the intermediate
      uspSrcInter = (MTI_UINT16 *)ucpDst[0];
     }
    else
     {
		// use the temporary image as the intermediate
      uspSrcInter = uspTmp;
     }
   }

		// Assign the DST intermediate
  if (lProcessFlow & PF_REPACK_NO)
   {
		// use the destination image as the intermediate
    uspDstInter = (MTI_UINT16 *)ucpDst[0];
   }
  else
   {
    if ((lProcessFlow & PF_UNPACK_NO) && (lProcessFlow & PF_CONVERT_NO))
     {
		// use the source image as the intermediate
      uspDstInter = (MTI_UINT16 *)ucpSrc[0];
     }
    else
     {
		// use the temporary image as the intermediate
      uspDstInter = uspTmp;
     }
   }

/*
	If it is necessary to unpack the data, do it here
*/

  if (lProcessFlow & PF_UNPACK_YES)
   {
    iRet = UnpackSourceData (ucpSrc, uspSrcInter);
    if (iRet)
      return iRet;
   }

/*
	If it is necessary to convert the data, do it here
*/

  if (lProcessFlow & PF_CONVERT_YES)
   {
    if (bUseApproxConvert)
	  ConvertSourceDataApprox (uspSrcInter, uspDstInter);
    else
	  ConvertSourceData (uspSrcInter, uspDstInter);
   }
  else if (uspSrcInter != uspDstInter)
   {
    // this is the case of PF_UNPACK_NO|PF_CONVERT_NO|PF_REPACK_NO
    // we must simply copy uspSrcInter to uspDstInter

    MTImemcpy ((void *)uspDstInter, (void *)uspSrcInter,
            lNRow * lNCol * cssSrc.lNComponents * sizeof(MTI_UINT16));
   }

/*
	If it is necessary to repack the destination data, do it here
*/

  if (lProcessFlow & PF_REPACK_YES)
   {
    iRet = RepackDestinationData (ucpDst, uspDstInter);
    if (iRet)
      return iRet;
   }

  return 0;
}  /* ConvertOneFrame */

void CColorSpaceConvert::ConvertOneValue (
	MTI_UINT16 usSrc0, MTI_UINT16 usSrc1, MTI_UINT16 usSrc2,
	MTI_UINT16 *uspDst0, MTI_UINT16 *uspDst1, MTI_UINT16 *uspDst2)
{
  long lC0, lC1;
  float faEncodeInv[MAX_COMPONENT], faColorRepresentationInv[MAX_COMPONENT],
	faTransferFunctionInv[MAX_COMPONENT], faTransferFunction[MAX_COMPONENT],
	faColorRepresentation[MAX_COMPONENT], faEncode[MAX_COMPONENT];


		// step 1:  invert the data encoding
  faEncodeInv[0] = CalcDataEncodeInv (&cssSrc, 0, (long)usSrc0);
  faEncodeInv[1] = CalcDataEncodeInv (&cssSrc, 1, (long)usSrc1);
  faEncodeInv[2] = CalcDataEncodeInv (&cssSrc, 2, (long)usSrc2);

		// step 2:  invert the color representation
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faColorRepresentationInv[lC0] = 0;
    for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
     {
      faColorRepresentationInv[lC0] +=
		cssSrc.faColorRepresentationInv[lC0][lC1] * faEncodeInv[lC1];
     }
   }

		// step 3:  invert the transfer function
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faTransferFunctionInv[lC0] = CalcTransferFunctionInv (&cssSrc,
		faColorRepresentationInv[lC0]);
   }

		// step 4:  apply transfer function
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faTransferFunction[lC0] = CalcTransferFunction (&cssDst,
		faTransferFunctionInv[lC0]);
   }

		// step 5:  apply color representation
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faColorRepresentation[lC0] = 0.;
    for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
     {
      faColorRepresentation[lC0] += cssDst.faColorRepresentation[lC0][lC1] *
		faTransferFunction[lC1];
     }
   }

		// step 6: apply data encoding
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faEncode[lC0] = CalcDataEncode (&cssDst, lC0, faColorRepresentation[lC0]);
   }

  *uspDst0 = faEncode[0] + 0.5;
  *uspDst1 = faEncode[1] + 0.5;
  *uspDst2 = faEncode[2] + 0.5;

#ifdef CSC_DEBUG
  // Do the calculation with the LUTs
  if (bEstablishLUT == false)
   {
    int iRet = EstablishFlow ();
    if (iRet)
     {
      printf ("Unable to establish flow\n");
      return;
     }

    if (lProcessFlow & PF_CONVERT_YES)
     {
      iRet = EstablishLUT ();
      if (iRet)
       {
        printf ("Unable to establish LUT");
        return;
       }
     }

    bEstablishLUT = true;
   }

  if (lProcessFlow & PF_CONVERT_YES)
   {
    long lW0, lW1, lW2, lX0, lX1, lX2, lY0, lY1, lY2, lZ0, lZ1, lZ2;

    lW0 = usSrc0;
    lW1 = usSrc1;
    lW2 = usSrc2;

    MatrixCombine (lW0, lW1, lW2, &lX0, &lX1, &lX2, lpLUTA);
    MatrixCombine (lX0, lX1, lX2, &lY0, &lY1, &lY2, lpLUTB);
    ccflpLUT->ApplyLUTC (lY0, lY1, lY2, &lZ0, &lZ1, &lZ2, &lRandIndex);

printf ("Source: %d %d %d\n", lW0, lW1, lW2);
printf ("After  1 & 2:  %f %f %f\n", faColorRepresentationInv[0],
	faColorRepresentationInv[1], faColorRepresentationInv[2]);
printf ("After   LUTA:  %f %f %f\n", (float)lX0*4./(float)ccflpLUT->lNEntryLUT,
	(float)lX1*4./(float)lNEntryLUT, (float)lX2*4./(float)ccflpLUT->lNEntryLUT);
printf ("After  3,4,5:  %f %f %f\n", faColorRepresentation[0],
	faColorRepresentation[1], faColorRepresentation[2]);
printf ("After   LUTB:  %f %f %f\n", (float)lY0/faSlopeC[0],
	(float)lY1/faSlopeC[1], (float)lY2/faSlopeC[2]);
printf ("After      6:  %f %f %f\n", faEncode[0], faEncode[1], faEncode[2]);
printf ("After   LUTC:  %f %f %f\n", (float)lZ0, (float)lZ1, (float)lZ2);

printf ("lY0: %d lY1: %d  lY2: %d\n", lY0, lY1, lY2);
   }
#endif

  return;
}  /* ConvertOneValue */

void CColorSpaceConvert::ConvertOneValue (
	MTI_UINT16 usSrc0, MTI_UINT16 usSrc1, MTI_UINT16 usSrc2,
	float *fpDst0, float *fpDst1, float *fpDst2)
{
  long lC0, lC1;
  float faEncodeInv[MAX_COMPONENT], faColorRepresentationInv[MAX_COMPONENT],
	faTransferFunctionInv[MAX_COMPONENT], faTransferFunction[MAX_COMPONENT],
	faColorRepresentation[MAX_COMPONENT], faEncode[MAX_COMPONENT];


		// step 1:  invert the data encoding
  faEncodeInv[0] = CalcDataEncodeInv (&cssSrc, 0, (long)usSrc0);
  faEncodeInv[1] = CalcDataEncodeInv (&cssSrc, 1, (long)usSrc1);
  faEncodeInv[2] = CalcDataEncodeInv (&cssSrc, 2, (long)usSrc2);

		// step 2:  invert the color representation
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faColorRepresentationInv[lC0] = 0;
    for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
     {
      faColorRepresentationInv[lC0] +=
		cssSrc.faColorRepresentationInv[lC0][lC1] * faEncodeInv[lC1];
     }
   }

		// step 3:  invert the transfer function
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faTransferFunctionInv[lC0] = CalcTransferFunctionInv (&cssSrc,
		faColorRepresentationInv[lC0]);
   }

		// step 4:  apply transfer function
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faTransferFunction[lC0] = CalcTransferFunction (&cssDst,
		faTransferFunctionInv[lC0]);
   }

		// step 5:  apply color representation
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faColorRepresentation[lC0] = 0.;
    for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
     {
      faColorRepresentation[lC0] += cssDst.faColorRepresentation[lC0][lC1] *
		faTransferFunction[lC1];
     }
   }

		// step 6: apply data encoding
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    faEncode[lC0] = CalcDataEncode (&cssDst, lC0, faColorRepresentation[lC0]);
   }

  *fpDst0 = faEncode[0];
  *fpDst1 = faEncode[1];
  *fpDst2 = faEncode[2];

  return;
}  /* ConvertOneValue */

void CColorSpaceConvert::ConvertOneValue (
	float fSrc0, float fSrc1, float fSrc2,
	float *fpDst0, float *fpDst1, float *fpDst2)
{
  MTI_UINT16 usSrcLB0, usSrcLB1, usSrcLB2;
  MTI_UINT16 usSrcUB0, usSrcUB1, usSrcUB2;
  float fLBOut0, fLBOut1, fLBOut2;
  float fUB0Out0, fUB0Out1, fUB0Out2, fUB1Out0, fUB1Out1, fUB1Out2,
		fUB2Out0, fUB2Out1, fUB2Out2;
  float fDistLB0, fDistLB1, fDistLB2;
  float fWeiLB0, fWeiLB1, fWeiLB2, fWeiUB0, fWeiUB1, fWeiUB2;

/*
        To convert floating point values follow these steps:

                1.  round (fSrc0, fSrc1, fSrc2) down
                2.  ConvertOneValue()
                3.  round (fSrc0, fSrc1, fSrc2) up
                4.  ConvertOneValue()
                5.  Take the weighted average.
*/

  usSrcLB0 = floor(fSrc0);
  usSrcLB1 = floor(fSrc1);
  usSrcLB2 = floor(fSrc2);
  usSrcUB0 = ceil(fSrc0);
  usSrcUB1 = ceil(fSrc1);
  usSrcUB2 = ceil(fSrc2);

  ConvertOneValue (usSrcLB0, usSrcLB1, usSrcLB2, &fLBOut0,
                &fLBOut1, &fLBOut2);
  ConvertOneValue (usSrcUB0, usSrcLB1, usSrcLB2, &fUB0Out0, &fUB0Out1,
                &fUB0Out2);
  ConvertOneValue (usSrcLB0, usSrcUB1, usSrcLB2, &fUB1Out0, &fUB1Out1,
                &fUB1Out2);
  ConvertOneValue (usSrcLB0, usSrcLB1, usSrcUB2, &fUB2Out0, &fUB2Out1,
                &fUB2Out2);

  fDistLB0 = fSrc0 - (float)usSrcLB0;	// between 0 and 1
  fDistLB1 = fSrc1 - (float)usSrcLB1;
  fDistLB2 = fSrc2 - (float)usSrcLB2;

  fWeiLB0 = (1. - fDistLB0) / 3.;
  fWeiLB1 = (1. - fDistLB1) / 3.;
  fWeiLB2 = (1. - fDistLB2) / 3.;
  fWeiUB0 = fDistLB0 / 3.;
  fWeiUB1 = fDistLB1 / 3.;
  fWeiUB2 = fDistLB2 / 3.;

  // calculate sum for each component
  *fpDst0 = fLBOut0 * (fWeiLB0 + fWeiLB1 + fWeiLB2) +
	fUB0Out0 * fWeiUB0 + fUB1Out0 * fWeiUB1 + fUB2Out0 * fWeiUB2;
  *fpDst1 = fLBOut1 * (fWeiLB0 + fWeiLB1 + fWeiLB2) +
	fUB0Out1 * fWeiUB0 + fUB1Out1 * fWeiUB1 + fUB2Out1 * fWeiUB2;
  *fpDst2 = fLBOut2 * (fWeiLB0 + fWeiLB1 + fWeiLB2) +
	fUB0Out2 * fWeiUB0 + fUB1Out2 * fWeiUB1 + fUB2Out2 * fWeiUB2;

  return;
}  /* ConvertOneValue */

int CColorSpaceConvert::UnpackSourceData (MTI_UINT8 **ucpSrc,
		MTI_UINT16 *uspIntermediate)
/*
	This function unpacks the source data into 16 bit values.
*/
{
  CExtractor extractor;
  RECT rect;
  int iRet;

  rect.left = 0;
  rect.top = 0;
  rect.right = lNCol - 1;
  rect.bottom = lNRow - 1;

  iRet = extractor.extractSubregionWithinFrame (uspIntermediate, ucpSrc,
	ifpSrc, &rect);
  return iRet;
}  /* UnpackSourceData */

void CColorSpaceConvert::ConvertSourceData(MTI_UINT16 *uspSrcInter,
   MTI_UINT16 *uspDstInter)
{
   long lRow, lCol, lW0, lW1, lW2, lX0, lX1, lX2, lY0, lY1, lY2, lZ0, lZ1, lZ2;
   MTI_UINT16 *uspS, *uspD;

   /*
    The steps involved in doing a color space conversion:

    1.  Inverse Data Encode the source
    2.  Inverse Color Representation the source
    3.  Inverse Transfer Function the source
    4.  Transfer Function using destination parameters
    5.  Color Representation using destination parameters
    6.  Data Encode using the destination parameters

    steps 1 and 2 are accomplished with LUTA.
    steps 3,4, and 5 are accomplished with LUTB.
    step 6 is accomplished with LUTC.
    */

   for (lRow = 0; lRow < lNRow; lRow++)
   {
      uspS = uspSrcInter + (lRow * lNCol * cssSrc.lNComponents);
      uspD = uspDstInter + (lRow * lNCol * cssDst.lNComponents);

      for (lCol = 0; lCol < lNCol; lCol++)
      {
         long lTmp = 0;

         lW0 = *uspS++;
         if (cssSrc.lNComponents > 1)
         {
            lW1 = *uspS++;
            lW2 = *uspS++;
         }
         else
         {
            EPixelPacking pp = ifpSrc->getPixelPacking();

            switch (pp)
            {
               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
               case IF_PIXEL_PACKING_1_8Bits_IN_2Bytes:
                  lTmp = 128;
                  break;
               case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
               case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
               case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
               case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes:
                  lTmp = 512;
                  break;
               case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
               case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
               case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
               case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
               case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes:
                  lTmp = 2048;
                  break;
               case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
               case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
               case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT:
                  lTmp = 32768;
                  break;

               default:
                  break;
            }

            lW1 = lTmp;
            lW2 = lTmp;
         }

         MatrixCombine(lW0, lW1, lW2, &lX0, &lX1, &lX2, ccflpLUT->lpLUTA);
         MatrixCombine(lX0, lX1, lX2, &lY0, &lY1, &lY2, ccflpLUT->lpLUTB);
         ccflpLUT->ApplyLUTC(lY0, lY1, lY2, &lZ0, &lZ1, &lZ2, &lRandIndex);

         if (cssSrc.lNComponents == 1)
         {
            switch (ifpDst->getPixelComponents())
            {
               case IF_PIXEL_COMPONENTS_RGB:
               case IF_PIXEL_COMPONENTS_BGR:
               case IF_PIXEL_COMPONENTS_Y:
               case IF_PIXEL_COMPONENTS_YYY:
                  // Make all three components the same
                  lTmp = (long)(((lZ0 + lZ1 + lZ2) / 3.0) + 0.5);
                  lZ0 = lZ1 = lZ2 = lTmp;
                  break;

               case IF_PIXEL_COMPONENTS_YUV422:
               case IF_PIXEL_COMPONENTS_YUV444:
                  {
                     // clamp U & V
                     EPixelPacking pp = ifpSrc->getPixelPacking();

                     switch (pp)
                     {
                        case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                        case IF_PIXEL_PACKING_1_8Bits_IN_2Bytes:
                           lTmp = 128;
                           break;
                        case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
                        case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                        case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
                        case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
                        case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
                        case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes:
                        case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
                           lTmp = 512;
                           break;
                        case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
                        case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
                        case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:
                        case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
                        case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes:
                           lTmp = 2048;
                           break;
                        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
                        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
                        case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT:
                           lTmp = 32768;
                           break;

                        default:
                           break;
                     }
                     lZ1 = lZ2 = lTmp;
                  } break;

               default:
                  break;
            } // switch(ifpDst->getColorSpace())
         }
         else
         {
            if (ifpDst->getPixelComponents() == IF_PIXEL_COMPONENTS_Y
            || ifpDst->getPixelComponents() == IF_PIXEL_COMPONENTS_YYY)
            {
                  // Make all three components the same
                  lTmp = (long)(((lZ0 + lZ1 + lZ2) / 3.0) + 0.5);
                  lZ0 = lZ1 = lZ2 = lTmp;
            }
         }

         *uspD++ = lZ0;
         if (cssDst.lNComponents > 1)
         {
            *uspD++ = lZ1;
            *uspD++ = lZ2;
         }
      }
   }

   return;
} /* ConvertSourceData */

void CColorSpaceConvert::ConvertSourceDataApprox (MTI_UINT16 *uspSrcInter,
		MTI_UINT16 *uspDstInter)
{
  long lRow, lCol, lIn0, lIn1, lIn2, lLBIn0, lLBIn1, lLBIn2, lUBIn0,
	lUBIn1, lUBIn2, lIndex;
  MTI_UINT16 *uspS, *uspD;
  float *fp;
  float fSum0, fSum1, fSum2, fUB0Out0, fUB0Out1, fUB0Out2, fUB1Out0,
	fUB1Out1, fUB1Out2, fUB2Out0, fUB2Out1, fUB2Out2,
	fLBOut0, fLBOut1, fLBOut2;
  float fWeiLB0, fWeiLB1, fWeiLB2, fWeiUB0, fWeiUB1, fWeiUB2;

/*
	The steps involved in doing an approximate color space conversion:

		1.  Remove LSBs from source value to create lower bound
		2.  create upper bound
		3.  Use LUT to determine converted lower bound value
		4.  Use LUT to determine converted upper bound value
		5.  Result is a weighted average of 3 and 4.
*/

  for (lRow = 0; lRow < lNRow; lRow++)
   {
    uspS = uspSrcInter + (lRow * lNCol * MAX_COMPONENT);
    uspD = uspDstInter + (lRow * lNCol * MAX_COMPONENT);

    for (lCol = 0; lCol < lNCol; lCol++)
     {
      // source pixel value
      lIn0 = *uspS++;
      lIn1 = *uspS++;
      lIn2 = *uspS++;

      // lower bound
      lLBIn0 = (lIn0 >> lLBShift);
      lLBIn1 = (lIn1 >> lLBShift);
      lLBIn2 = (lIn2 >> lLBShift);

      // upper bound.  Don't allow to get too large
      lUBIn0 = (lLBIn0 + 1) & (lNIndex-1);
      lUBIn1 = (lLBIn1 + 1) & (lNIndex-1);
      lUBIn2 = (lLBIn2 + 1) & (lNIndex-1);

      // converted lower bound
      lIndex = (((lLBIn0 << INDEX_SHIFT) + lLBIn1) << INDEX_SHIFT) + lLBIn2;
      fp = fpLUT + lIndex*MAX_COMPONENT;
      fLBOut0 = *fp++;
      fLBOut1 = *fp++;
      fLBOut2 = *fp++;

      // converted upper bound for component 0
      lIndex = (((lUBIn0 << INDEX_SHIFT) + lLBIn1) << INDEX_SHIFT) + lLBIn2;
      fp = fpLUT + lIndex*MAX_COMPONENT;
      fUB0Out0 = *fp++;
      fUB0Out1 = *fp++;
      fUB0Out2 = *fp++;

      // converted upper bound for component 1
      lIndex = (((lLBIn0 << INDEX_SHIFT) + lUBIn1) << INDEX_SHIFT) + lLBIn2;
      fp = fpLUT + lIndex*MAX_COMPONENT;
      fUB1Out0 = *fp++;
      fUB1Out1 = *fp++;
      fUB1Out2 = *fp++;

      // converted upper bound for component 2
      lIndex = (((lLBIn0 << INDEX_SHIFT) + lLBIn1) << INDEX_SHIFT) + lUBIn2;
      fp = fpLUT + lIndex*MAX_COMPONENT;
      fUB2Out0 = *fp++;
      fUB2Out1 = *fp++;
      fUB2Out2 = *fp++;

      // find weights for each component
      fWeiLB0 = fpWeiLB[lIn0];
      fWeiLB1 = fpWeiLB[lIn1];
      fWeiLB2 = fpWeiLB[lIn2];
      fWeiUB0 = fpWeiUB[lIn0];
      fWeiUB1 = fpWeiUB[lIn1];
      fWeiUB2 = fpWeiUB[lIn2];

      // calculate sum for each component
      fSum0 = fLBOut0 * (fWeiLB0 + fWeiLB1 + fWeiLB2) +
	fUB0Out0 * fWeiUB0 + fUB1Out0 * fWeiUB1 + fUB2Out0 * fWeiUB2;
      fSum1 = fLBOut1 * (fWeiLB0 + fWeiLB1 + fWeiLB2) +
	fUB0Out1 * fWeiUB0 + fUB1Out1 * fWeiUB1 + fUB2Out1 * fWeiUB2;
      fSum2 = fLBOut2 * (fWeiLB0 + fWeiLB1 + fWeiLB2) +
	fUB0Out2 * fWeiUB0 + fUB1Out2 * fWeiUB1 + fUB2Out2 * fWeiUB2;

      // normalize the sum to create the output values
      *uspD++ = fSum0 + 0.5;	// TTT should have dither
      *uspD++ = fSum1 + 0.5;
      *uspD++ = fSum2 + 0.5;
     }
   }

  return;
}  /* ConvertSourceDataApprox */

int CColorSpaceConvert::RepackDestinationData (MTI_UINT8 **ucpDst,
		MTI_UINT16 *uspIntermediate)
/*
	This function unpacks the source data into 16 bit values.
*/
{
  CExtractor extractor;
  RECT rect;
  int iRet;

  rect.left = 0;
  rect.top = 0;
  rect.right = lNCol - 1;
  rect.bottom = lNRow - 1;

  iRet = extractor.replaceSubregionWithinFrame (ucpDst, ifpDst, &rect,
	uspIntermediate);
  return iRet;
}  /* RepackDestinationData */

bool CColorSpaceConvert::DifferentParameters()
/*
	This function determines is the source parameters are different
	from the destination parameters.  If they are not different,
	there is not reason to perform the conversion.
*/
{
  bool bDifferent = false;
  long lC0, lC1;

  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
  for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
   {
    if (cssSrc.faColorRepresentation[lC0][lC1] !=
		cssDst.faColorRepresentation[lC0][lC1])
      bDifferent = true;
   }

  if (cssSrc.fTransferFunctionGain != cssDst.fTransferFunctionGain)
    bDifferent = true;
  if (cssSrc.fTransferFunctionBreakPoint != cssDst.fTransferFunctionBreakPoint)
    bDifferent = true;
  if (cssSrc.fTransferFunctionGamma != cssDst.fTransferFunctionGamma)
    bDifferent = true;

  if (cssSrc.lDataEncodeFunctionType != cssDst.lDataEncodeFunctionType)
    bDifferent = true;

/* TTT   this needs some more work.  For example when converting Y values
	in the range 16 to 235, to Y values in the range 16*256 to 235*256,
	we do not need to do a color space convert.

	If, however, we are converting 16 to 235 to 0 to 65535, we do need
	a color space convert.
*/
  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    if (cssSrc.laDataEncodeMinimum[lC0] != cssDst.laDataEncodeMinimum[lC0])
      bDifferent = true;
    if (cssSrc.laDataEncodeMaximum[lC0] != cssDst.laDataEncodeMaximum[lC0])
      bDifferent = true;
    if (cssSrc.laDataEncodeClipLower[lC0] != cssDst.laDataEncodeClipLower[lC0])
      bDifferent = true;
    if (cssSrc.laDataEncodeClipUpper[lC0] != cssDst.laDataEncodeClipUpper[lC0])
      bDifferent = true;
   }

  return bDifferent;
}  /* DifferentParameters */


CColorConvertFancyLUT::CColorConvertFancyLUT()
{
  long lDither, lC0, lC1;
/*
	Initialize all pointers to NULL
*/

  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
     {
      lpLUTA[lC0][lC1] = NULL;
      lpLUTB[lC0][lC1] = NULL;
     }
    for (lDither = 0; lDither < NUM_DITHER; lDither++)
     {
      lpLUTC[lC0][lDither] = NULL;
     }
   }

  lpRandLUT = NULL;
}

CColorConvertFancyLUT::~CColorConvertFancyLUT()
{
  long lC0, lC1, lDither;
/*
	Free up the storage
*/

  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
     {
      MTIfree(lpLUTA[lC0][lC1]);
      lpLUTA[lC0][lC1] = NULL;
      MTIfree(lpLUTB[lC0][lC1]);
      lpLUTB[lC0][lC1] = NULL;
     }
    for (lDither = 0; lDither < NUM_DITHER; lDither++)
     {
      if (lpLUTC[lC0][lDither])
       {
        lpLUTC[lC0][lDither] += laMinIndexLUTC[lC0];
        MTIfree(lpLUTC[lC0][lDither]);
        lpLUTC[lC0][lDither] = NULL;
       }
     }
   }

  MTIfree(lpRandLUT);
  lpRandLUT = NULL;
}

void CColorConvertFancyLUT::ApplyLUTC (long lX0, long lX1, long lX2, long *lpY0,
	long *lpY1, long *lpY2, long *lpRandIndex)
{
  if (*lpRandIndex < 0 || *lpRandIndex >= NUM_RAND)
    *lpRandIndex = 0;
  long lDither = lpRandLUT[*lpRandIndex];
  (*lpRandIndex)++;  // increment for the next call

  if (lX0 < laMinIndexLUTC[0])
    lX0 = laMinIndexLUTC[0];
  else if (lX0 >= laMaxIndexLUTC[0])
    lX0 = laMaxIndexLUTC[0] - 1;
  if (lX1 < laMinIndexLUTC[1])
    lX1 = laMinIndexLUTC[1];
  else if (lX1 >= laMaxIndexLUTC[1])
    lX1 = laMaxIndexLUTC[1] - 1;
  if (lX2 < laMinIndexLUTC[2])
    lX2 = laMinIndexLUTC[2];
  else if (lX2 >= laMaxIndexLUTC[2])
    lX2 = laMaxIndexLUTC[2] - 1;

  *lpY0 = lpLUTC[0][lDither][lX0];
  *lpY1 = lpLUTC[1][lDither][lX1];
  *lpY2 = lpLUTC[2][lDither][lX2];

  return;
}  /* ApplyLUTC */


int CColorSpaceConvert::EstablishLUT (CColorConvertFancyLUT **ccflppLUTArg)
{
  int iRet = 0;

  if (ccflppLUTArg)
    *ccflppLUTArg = NULL;

  if (bEstablishLUT == false)
   {
    iRet = EstablishFlow ();
    if ((iRet == 0) && (lProcessFlow & PF_CONVERT_YES))
     {
      if (bUseApproxConvert)
       {
        iRet = EstablishLUTApprox ();
       }
      else if (ccflpLUT == NULL)
       {
        ccflpLUT = new CColorConvertFancyLUT;
        if (ccflpLUT == NULL)
         {
          iRet = FORMAT_ERROR_MALLOC;
         }
        else
         {
          bWeOwnTheFancyLUT = true;
          iRet = EstablishFancyLUT ();
         }
       }
     }
    if (iRet == 0)
     {
      bEstablishLUT = true;
     }
   }
  if (ccflppLUTArg)
    *ccflppLUTArg = ccflpLUT;

  return iRet;
}  /* Establish LUT */

int CColorSpaceConvert::EstablishFancyLUT ()
{
  float fTransfer, fValue, fEncode;
  float fEncodeInv, fTransferInv;
  long lLUT, lComponent, lC0, lDither, lC1, lResult, lD2, lTmp, lIntercept;
  long lSeed = 1353857;
  float *fpValueLUT;
  long lRand;
  float fMin, fMax;
  long lNEntryLUT;

/*
	Allocate Storage
*/

  ccflpLUT->lNEntryLUT = lNEntryLUT = (1<<BIT_PER_LUT);

  for (lC0 = 0; lC0 < MAX_COMPONENT; lC0++)
   {
    for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
     {
      ccflpLUT->lpLUTA[lC0][lC1] = (long *) MTImalloc (lNEntryLUT * sizeof(long));
      if (ccflpLUT->lpLUTA[lC0][lC1] == NULL)
        return FORMAT_ERROR_MALLOC;

      ccflpLUT->lpLUTB[lC0][lC1] = (long *) MTImalloc (lNEntryLUT * sizeof(long));
      if (ccflpLUT->lpLUTB[lC0][lC1] == NULL)
        return FORMAT_ERROR_MALLOC;
     }

/*
	Colors can be negative before they are encoded.  We determine where
	the zero index should fall.

	Allow lNEntryLUT/4 entries for the range of 1.
	Allow (fMin + fMax) / 2 to land at lNEntryLUT/2.

	Make sure the value f=0. maps to LUT 0
*/

    FindDataEncodeMinMax (&cssDst, lC0, &fMin, &fMax);

/*
	Define the line such that

	lIndex = slope * fValue + intercept
*/

    ccflpLUT->faSlopeC[lC0] = ((float)lNEntryLUT/4.);
	// since (fMin+fMax)/2 maps to lNEntryLUT/2
    ccflpLUT->faInterceptC[lC0] = ((float)lNEntryLUT/2.) - (ccflpLUT->faSlopeC[lC0] *
		(fMin+fMax)/2.);

    lIntercept = ccflpLUT->faInterceptC[lC0] + 0.5;
    ccflpLUT->laMinIndexLUTC[lC0] = -lIntercept;
    ccflpLUT->laMaxIndexLUTC[lC0] = lNEntryLUT - lIntercept;

    for (lDither = 0; lDither < NUM_DITHER; lDither++)
     {
      ccflpLUT->lpLUTC[lC0][lDither] = (long *) MTImalloc (lNEntryLUT * sizeof(long));
      if (ccflpLUT->lpLUTC[lC0][lDither] == NULL)
        return FORMAT_ERROR_MALLOC;

      ccflpLUT->lpLUTC[lC0][lDither] -= ccflpLUT->laMinIndexLUTC[lC0];
     }
   }

/*
	Allocate local storage
*/

  fpValueLUT = (float *)MTImalloc (lNEntryLUT * sizeof(float));
  if (fpValueLUT == NULL)
    return FORMAT_ERROR_MALLOC;

  ccflpLUT->lpRandLUT = (long *)MTImalloc (NUM_RAND * sizeof (long));
  if (ccflpLUT->lpRandLUT == NULL)
    return FORMAT_ERROR_MALLOC;

/*
	Create local LUT values
*/

  for (lLUT = 0; lLUT < lNEntryLUT; lLUT++)
   {
    fpValueLUT[lLUT] = (float)lLUT / (float)(lNEntryLUT/4);
   }

  for (lRand = 0; lRand < NUM_RAND; lRand++)
   {
    ccflpLUT->lpRandLUT[lRand] = ran266 (&lSeed) * NUM_DITHER;
   }

/*
	Create LUTA.  LUTA is the combination of steps 1 and 2.
	That is, LUTA is the inverse of the source Data Encode followed
	by the inverse of the source color representation.
*/

printf ("TTT Doing LUTA\n");
  for (lComponent = 0; lComponent < MAX_COMPONENT; lComponent++)
   {
    for (lLUT = 0; lLUT < lNEntryLUT; lLUT++)
     {
/*
	Establish the inverse of the source's DataEncode LUT
*/

      fEncodeInv = CalcDataEncodeInv (&cssSrc, lComponent, lLUT);

      for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
       {
/*
	fEncodeInv is the inverse encoded value of the lComponent entry.
	fEncodeInv contributes to three LUT tables:
		lpLUTA[0][lComponent],
		lpLUTA[1][lComponent],
		lpLUTA[2][lComponent]
*/
        fValue = fEncodeInv * cssSrc.faColorRepresentationInv[lC1][lComponent];
        lResult = (long)(fValue * (float)(lNEntryLUT/4) + 0.5);
        ccflpLUT->lpLUTA[lC1][lComponent][lLUT] = lResult;
       }
     }
   }

/*
	Create LUTB.  LUTB is the combination of steps 3,4, and 5.
	That is, LUTB is the inverse of the source Transfer Function
	followed by the destination Transfer Function followed by the
	destination Color Representation.
*/

printf ("TTT Doing LUTB\n");
  for (lLUT = 0; lLUT < lNEntryLUT; lLUT++)
   {
/*
	Find the inverse of the source transfer function
*/

    fTransferInv = CalcTransferFunctionInv (&cssSrc, fpValueLUT[lLUT]);

/*
	Find the destination transfer function
*/

    fTransfer = CalcTransferFunction (&cssDst, fTransferInv);

/*
	Find the destination Color Representation
*/

    for (lComponent = 0; lComponent < MAX_COMPONENT; lComponent++)
    for (lC1 = 0; lC1 < MAX_COMPONENT; lC1++)
     {
      fValue = fTransfer * cssDst.faColorRepresentation[lComponent][lC1];
      lResult = (long)(fValue * ccflpLUT->faSlopeC[lC1] + 0.5);
      ccflpLUT->lpLUTB[lComponent][lC1][lLUT] = lResult;
     }
   }

/*
	Create LUTC.  LUTC captures step 6.  That is the Data Encode of
	the destination.
*/

printf ("TTT Doing LUTC\n");
  for (lComponent = 0; lComponent < MAX_COMPONENT; lComponent++)
   {
    for (lLUT = ccflpLUT->laMinIndexLUTC[lComponent];
		lLUT < ccflpLUT->laMaxIndexLUTC[lComponent]; lLUT++)
     {
      // lLUT will be the output of LUTB.  Convert this back to a float.
      fValue = (float)lLUT / ccflpLUT->faSlopeC[lComponent];

      fEncode = CalcDataEncode (&cssDst, lComponent, fValue);

/*
	Now, save this value into the DataEncode LUT.  We play a clever trick
	to dither round off errors.  We keep NUM_DITHER versions of
	the LUT.  The entries in the different versions are selected to
	compensate for rounding.  In other words, by averaging the values
	of lpLUTC[x][lDither][y] over lDither, we will arrive
	at the value very close to fEncode.
*/

      for (lDither = 0; lDither < NUM_DITHER; lDither++)
       {
	// ran266 has a mean of .5, so on average we end up rounding fEncode
        ccflpLUT->lpLUTC[lComponent][lDither][lLUT] = (long)(fEncode + ran266 (&lSeed));
       }
     }
   }

/*
	Randomize the entries over Dither to avoid bias.
*/

  lRand = 0;
  for (lComponent = 0; lComponent < MAX_COMPONENT; lComponent++)
  for (lLUT = ccflpLUT->laMinIndexLUTC[lComponent]; lLUT < ccflpLUT->laMaxIndexLUTC[lComponent];
		lLUT++)
  for (lDither = 0; lDither < NUM_DITHER; lDither++)
   {
    lD2 = ccflpLUT->lpRandLUT[(lRand++)%NUM_RAND];
    lTmp = ccflpLUT->lpLUTC[lComponent][lDither][lLUT];
    ccflpLUT->lpLUTC[lComponent][lDither][lLUT] = ccflpLUT->lpLUTC[lComponent][lD2][lLUT];
    ccflpLUT->lpLUTC[lComponent][lD2][lLUT] = lTmp;
   }

printf ("TTT Finished with LUT's\n");

#ifdef CSC_DEBUG
{
  long lC0, lC1, lC2, lV0, lV1, lV2, lX0, lX1, lX2, lY0, lY1, lY2, lZ0,
		lZ1, lZ2;
	// convert a few values:

  for (lC0 = 0; lC0 < 5; lC0++)
  for (lC1 = 0; lC1 < 5; lC1++)
  for (lC2 = 0; lC2 < 5; lC2++)
   {
    lV0 = (lC0 * (cssSrc.laDataEncodeClipUpper[0] -
		cssSrc.laDataEncodeClipLower[0]) / 4) +
		cssSrc.laDataEncodeClipLower[0];
    lV1 = (lC1 * (cssSrc.laDataEncodeClipUpper[1] -
		cssSrc.laDataEncodeClipLower[1]) / 4) +
		cssSrc.laDataEncodeClipLower[1];
    lV2 = (lC2 * (cssSrc.laDataEncodeClipUpper[2] -
		cssSrc.laDataEncodeClipLower[2]) / 4) +
		cssSrc.laDataEncodeClipLower[2];

    MatrixCombine (lV0, lV1, lV2, &lX0, &lX1, &lX2, lpLUTA);
    MatrixCombine (lX0, lX1, lX2, &lY0, &lY1, &lY2, lpLUTB);
    ccflpLUT->ApplyLUTC (lY0, lY1, lY2, &lZ0, &lZ1, &lZ2, &lRandIndex);

    printf ("(%d, %d, %d)  maps to  (%d, %d, %d)  i.e. (%d, %d, %d)\n",
		lV0, lV1, lV2, lZ0, lZ1, lZ2, lV0-lZ0/4, lV1-lZ1/4, lV2-lZ2/4);
   }
}
#endif

  // clean up local storage
  MTIfree(fpValueLUT);

  return 0;
}  /* EstablishLUT */
