//
// include file for class CMask
//
#ifndef MASK_H
#define MASK_H

#include "formatDLL.h"
//#include "PixelRegions.h"
#define MAX_LASSO_PTS 3*2048

// WAS #define SMALL
#ifdef SMALL
typedef MTI_UINT16 EDGINDEX;
#define NULLEDG (EDGINDEX) 0xffff
#else
typedef MTI_UINT32 EDGINDEX;
#define NULLEDG (EDGINDEX) 0xffffffff
#endif

// using 16-bit edge indices, size is 26 bytes
// using 32-bit edge indices, size is 32 bytes

struct EDGE {

   EDGINDEX inxtlex;
   EDGINDEX ifwd;
   EDGINDEX ibwd;

   MTI_INT16 xbot;
   MTI_INT16 ybot;
   MTI_INT16 xtop;
   MTI_INT16 ytop;
   MTI_INT16 xmin;
   MTI_INT16 xmax;

   MTI_INT16 pen;
   MTI_INT16 brx;
   MTI_INT16 bry;

   MTI_UINT8 edgCod;
};


class MTI_FORMATDLL_API CMask
{

public:

   CMask();
   ~CMask();

   void initMask(int wdth, int hght,  MTI_UINT16 *frm);

   void setActiveRegion(int lft, int top,
                        int rgt, int bot);

   void setDrawColor(MTI_UINT16);

   void fillActiveRegion();

   void drawRectangle(int lft, int top,
                      int rgt, int bot);

   void moveTo(int, int);

   void drawDot();

   void drawDot(int, int);

   void lineTo(int, int);

   int getTotalEdges(POINT *);

   int getTotalChords(BEZIER_POINT *, int chordleng);

#define MASK_ERR_INSUFF_EDGES -1
   int initFill(int edges);

   // here the pitch is in pixels
   int initRun(MTI_UINT16 *dstrun, const MTI_UINT16 *frame,
               int pitch, int pxlComponentCount,
               int edges);

   void moveToFill(int, int);

   void lineToFill(int, int);

   void splineToFill(int, int, int, int, int, int, int chordleng);

   void fillPolygon();

   void drawLasso(POINT *);

   void drawBezier(BEZIER_POINT *);

   MTI_UINT16 *getEndOfRuns();

   // XOR the portion of the mask in the
   // src rectangle to the destination buffer
   // using the same mapping as CONVERT. Used
   // for display of the mask contents
   void xorMask( RECT *srcrect,
                 unsigned int *dstbuf,
                 int  dstwdth,
                 int  dsthght  );

   void setPadRadius(int);

   void setEnableColor(MTI_UINT16);

   void setReplaceColor(MTI_UINT16);

   void padPixel();

   void fattenTo(int, int);

private:

   // sort the edges in lexicographic order
   EDGINDEX iedgeSort(EDGINDEX, EDGINDEX);

   // insert an edge-record into the scan
   // line list between two other edges
   void insertEdge(EDGINDEX, EDGINDEX, EDGINDEX);

   // delete an edge-record from the
   // scan line list
   void deleteEdge(EDGINDEX);

   // draw a horizontal line from minx
   // to maxx at the current cury level
   void hatchLine(int, int);

   // edge stretch methods
   static void StretchXEdge0(EDGE *);
   static void StretchXEdge1(EDGE *);
   static void StretchXEdge2(EDGE *);
   static void StretchXEdge3(EDGE *);
   static void StretchXEdge4(EDGE *);
   static void StretchXEdge5(EDGE *);
   static void StretchXEdge6(EDGE *);
   static void StretchXEdge7(EDGE *);

   // dispatch table for same
   void (*strTbl[8])(EDGE *);

   // edge step methods
   static void StepYEdge0(EDGE *);
   static void StepYEdge1(EDGE *);
   static void StepYEdge2(EDGE *);
   static void StepYEdge3(EDGE *);
   static void StepYEdge4(EDGE *);
   static void StepYEdge5(EDGE *);
   static void StepYEdge6(EDGE *);
   static void StepYEdge7(EDGE *);

   // dispatch table for same
   void (*stpTbl[8])(EDGE *);

   private:

   int wdth,
       hght;

   // active region of frame buffer
   // (all coords INCLUSIVE)
   RECT region;

   int dpitch;

   int pxlComponentCount;

   MTI_UINT16 *frmbuf;

   MTI_UINT8 *scnbuf;

   int curx,
       cury;

   MTI_UINT16 *curptr;

   MTI_UINT16 fcolr;

#define EDGES_INIT 2048
   int  edgcnt;
   EDGE *edgbuf;

   EDGINDEX iedgptr;
   EDGINDEX ilexlst;
   EDGINDEX iedglst;

   // padding radius
   int padRadius,
       padRadiusSq;

   // enable color
   MTI_UINT16 enableColor;

   // replace color
   MTI_UINT16 replaceColor;

#define DRAW_RUN   0
#define GRAB_RUN   1
   MTI_UINT8 hatchMode;

   MTI_UINT16 *endOfRun;

   // -> 16-bit 444 intermediate
   const MTI_UINT16 *srcOfRuns;

};

#endif
