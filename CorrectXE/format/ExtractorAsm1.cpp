// This is separate because you can only have ONE asm procedure per cpp file!

#include "machine.h"

void replace_row_444_10_ASM(MTI_UINT16 *src, MTI_UINT8  *dst, int actWdth)
{
#ifdef _WIN64
	__asm__
	(
	"	mov     %%rdi,%%rbx;     "
	"	xorq    %%rax,%%rax;     "
	"	movl    %0,%%eax;        "
	"	sal     $2,%%rax;        "
	"	addq    %%rax,%%rbx;     "
	"L0:                        "
	"	movzwl  (%%rsi),%%edx;   "
	"	shll    $10,%%edx;       "
	"	movzwl  2(%%rsi),%%eax;  "
	"	addl    %%eax,%%edx;     "
	"	shll    $10,%%edx;       "
	"	movzwl  4(%%rsi),%%eax;  "
	"	addl    %%eax,%%edx;     "
	"	shll    $2,%%edx;        "
	"	bswap   %%edx;	          "
	"	mov     %%edx,(%%rdi);   "
	"	movzwl  6(%%rsi),%%edx;  "
	"	shll    $10,%%edx;       "
	"	movzwl  8(%%rsi),%%eax;  "
	"	addl    %%eax,%%edx;     "
	"	shll    $10,%%edx;       "
	"	movzwl  10(%%rsi),%%eax; "
	"	addl    %%eax,%%edx;	    "
	"	shll    $2,%%edx;        "
	"	bswap   %%edx;           "
	"	mov     %%edx,4(%%rdi);  "
	"	addq    $12,%%rsi;       "
	"	addq    $8,%%rdi;        "
	"	cmpq    %%rbx,%%rdi;     "
	"	jb      L0;	             "
	:
	: "r"(actWdth), "S"(src), "D"(dst)
	: "eax", "ebx", "ecx", "edx");
#endif
}
