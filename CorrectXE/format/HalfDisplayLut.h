#ifndef HalfDisplayLutH
#define HalfDisplayLutH

#include "machine.h"
#include "formatDLL.h"

class MTI_FORMATDLL_API HalfDisplayLut
{
	unsigned char displayLut[65536];
	unsigned short blackPointIn;
	unsigned short whitePointIn;
	unsigned char blackPointOut;
	unsigned char whitePointOut;
	double gamma;

public:

	static unsigned char *GetDefaultLutPtr();

	HalfDisplayLut(
		unsigned short blackPointInArg,
		unsigned short whitePointInArg,
		unsigned char blackPointOutArg,
		unsigned char whitePointOutArg,
		double gammaArg)
	: blackPointIn(blackPointInArg)
	, whitePointIn(whitePointInArg)
	, blackPointOut(blackPointOutArg)
	, whitePointOut(whitePointOutArg)
   , gamma(gammaArg)
	{
	};

	int FillLut(unsigned char *lut);
};


#endif