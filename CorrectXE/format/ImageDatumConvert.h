#ifndef ImageDatumConvertH
#define ImageDatumConvertH
#include "IniFile.h"    // For the TRACE in MTIAssert! WTF!

template<class _Ty> inline
	const _Ty& (clamp)(const _Ty& _Val, const _Ty& _Min, const _Ty& _Max)
	{	// Return _Val clamped to range _Min to _Max
   MTIassert(_Max >= _Min);
	return (_Val < _Min)? _Min : ((_Val > _Max) ? _Max : _Val);
	}

namespace ImageDatum
{

const int FULL_SHIFT = 16;
const int FLOAT_EXP_BITS = 8;
const int HALF_EXP_BITS = 5;
const int EXP_AND_MANTISSA_SHIFT = FULL_SHIFT - (FLOAT_EXP_BITS - HALF_EXP_BITS);
const unsigned int EXP_CONVERT_VALUE = (127 - 15) << 10;
const unsigned short HALF_SIGN_MASK = 0x8000;
const unsigned short HALF_EXP_MASK = 0x7C00;
const unsigned short HALF_MANTISSA_MASK = 0x03FF;

union unsignedIntOrFloat
{
	unsigned int asUnsignedInt;
	float asFloat;
};

inline float halfToFloat(unsigned short half)
{
	unsignedIntOrFloat retVal;
	retVal.asUnsignedInt = 0;

	unsigned int sign = (half & HALF_SIGN_MASK) << FULL_SHIFT;
	retVal.asUnsignedInt = sign | (((unsigned int)(half & ~HALF_SIGN_MASK) + EXP_CONVERT_VALUE) << EXP_AND_MANTISSA_SHIFT);

	return retVal.asFloat;
}

inline unsigned short floatToHalf(float f)
{
	unsignedIntOrFloat retVal;
	retVal.asUnsignedInt = 0;

	retVal.asFloat = f;
	unsigned int sign = (retVal.asUnsignedInt >> FULL_SHIFT) & 0x8000;

	const unsigned int ROUNDING_BIT = 0x00001000U;
	retVal.asUnsignedInt += ROUNDING_BIT;

	// Compute half exponent by removing excess 127 and adding excess 15.
	int exponent = ((retVal.asUnsignedInt & 0x7F800000) >> 13) - EXP_CONVERT_VALUE;
	if (exponent < 0)
	{
		retVal.asUnsignedInt = sign | 0x0000;
	}
	else if (exponent > 0x7C00)
	{
		retVal.asUnsignedInt = sign | 0x7C00;
	}
	else
	{
		unsigned int mantissa = (retVal.asUnsignedInt >> EXP_AND_MANTISSA_SHIFT) & HALF_MANTISSA_MASK;
		retVal.asUnsignedInt = sign | (unsigned int)exponent | mantissa;
	}

	return (unsigned short) retVal.asUnsignedInt;
}

// Convert to real Half data type from our screwball Frankenstein format.
inline unsigned short toHalf(unsigned short datum)
{
//	unsigned short testHalf = datum ^ (HALF_SIGN_MASK | ((datum & HALF_SIGN_MASK) ? 0 : HALF_MANTISSA_MASK));

	unsigned short temp = datum ^ HALF_SIGN_MASK;
	unsigned short newHalf = temp ^ (((unsigned short)(((short) temp) >> (FULL_SHIFT - 1))) & ~(HALF_SIGN_MASK | HALF_EXP_MASK));

//	MTIassert(testHalf == newHalf);

	return newHalf;
}

// Convert from real Half data type to our screwball Frankenstein format.
inline unsigned short fromHalf(unsigned short datum)
{
//	unsigned short testDatum = datum ^ (HALF_SIGN_MASK | ((datum & HALF_SIGN_MASK) ? HALF_MANTISSA_MASK : 0));

	unsigned short newDatum = datum ^ (HALF_SIGN_MASK | (((unsigned short)(((short) datum) >> (FULL_SHIFT - 1))) & ~(HALF_SIGN_MASK | HALF_EXP_MASK)));

//	MTIassert(testDatum == newDatum);

	return newDatum;

}

inline float toFloat(unsigned short datum, bool dataTypeIsReallyHalfFloat)
{
	return dataTypeIsReallyHalfFloat
				? (halfToFloat(toHalf(datum)))
				: (float) datum;
}

inline unsigned short fromFloat(float f, bool dataTypeIsReallyHalfFloat)
{
	if (dataTypeIsReallyHalfFloat)
	{
		return fromHalf(floatToHalf(f));
	}

	int i = clamp<int>((int)std::floor(f + 0.5F), 0, 0xFFFF);

	return (unsigned short) i;
}

};

inline void ConvertImageDataToHalfs(unsigned short *buffer, size_t len)
{
	for (int i = 0; i < (int)len; ++i)
	{
		buffer[i] = ImageDatum::toHalf(buffer[i]);
	}
}

inline void ConvertImageDataToHalfs(unsigned short *bufferIn, unsigned short *bufferOut, size_t len)
{
	for (int i = 0; i < (int)len; ++i)
	{
		bufferOut[i] = ImageDatum::toHalf(bufferIn[i]);
	}
}

inline void ConvertHalfsToImageData(unsigned short *buffer, size_t len)
{
	for (int i = 0; i < (int)len; ++i)
	{
// Too slow for debug config, which doesn't do inlining.
#ifndef _DEBUG
		buffer[i] = ImageDatum::fromHalf(buffer[i]);
#else
		unsigned short &datum = buffer[i];
		datum = datum ^ (ImageDatum::HALF_SIGN_MASK | (((unsigned short)(((short) datum) >> (ImageDatum::FULL_SHIFT - 1)))
								& ~(ImageDatum::HALF_SIGN_MASK | ImageDatum::HALF_EXP_MASK)));
#endif
	}
}

inline void ConvertHalfsToImageData(unsigned short *bufferIn, unsigned short *bufferOut, size_t len)
{
	for (int i = 0; i < (int)len; ++i)
	{
		bufferOut[i] = ImageDatum::fromHalf(bufferIn[i]);
	}
}

#endif
