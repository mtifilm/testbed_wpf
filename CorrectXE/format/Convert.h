#ifndef ConvertH
#define ConvertH

//
// include file for class CConvert
//
#include "machine.h"
#include "formatDLL.h"
#include "ImageInfo.h"
#include "mthread.h"
// #include "ConvertCommon.h"   inlined below (COLEBTRY, ROWENTRY)

class CImageFormat;

struct COLENTRY
{
   int offs;
	int phase;
};

struct ROWENTRY
{
   int fld;
	int offs;
	int phase;
};

class MTI_FORMATDLL_API CConvert
{

public:

   CConvert(int numStripes = 0);

   ~CConvert();

   void setAlphaColor(MTI_UINT32 alphacol); // use RGBA

   unsigned int getAlphaColor();

	void convertWtf(  unsigned char **srcbuf,
						const CImageFormat *srcfmt,
						RECT *srcrect,
						unsigned int *dstbuf,
						int dstwdth,
						int dsthght,
						int dstpitch,
						unsigned char *lutptr = 0);

	void displayConvert(
						unsigned char **srcbuf,
						const CImageFormat *srcfmt,
						RECT *srcrect,
						unsigned int *dstbuf,
						int dstwdth,
						int dsthght,
						int dstpitch,
						unsigned char *lutptr = 0,
						unsigned int rgbmask = 0x00ffffff,
						unsigned int posneg = 0,
						bool isWiperComparand = false,
						int *comparandDividerPositionPerRow = nullptr);

	void displayConvertIntermediate(
						MTI_UINT16 *srcbuf,
						const CImageFormat *srcfmt,
						RECT *srcrect,
						unsigned int *dstbuf,
						int dstwdth,
						int dsthght,
						int dstpitch,
						unsigned char *lutptr = 0,
						unsigned int rgbmask = 0x00ffffff,
						unsigned int posneg = 0,
						unsigned char alpha = 0x7f);

///////////////////////////////////////////////////////////////////

   // this vble is the color to use for the alpha channel (when it
   // is present). When this is 0, alpha display is inhibited

   MTI_UINT32 alphaColor; // TTT demo

///////////////////////////////////////////////////////////////////

// the INDEX into the RGB table is assembled from:
//
// the 7 most-significant bits of U in bits 13-18
//#define msku 0x3f8000
// the 7 most-significant bits of V in bits 7-12
//#define mskv 0x7f00
// the 8 most-significant bits of Y in bits 0-6
//#define msky 0xff

// the output of the conversion is an RGBA integer
// in the case of the (big-endian) SGI and an ARGB
// integer in the case of the (little-endian) PC

// the COMPONENTS of the SGI RGBA pixel are
//
// bits 24-31 are RED
// bits 16-23 are GREEN
// bits 8-15  are BLUE
// bits 0-7   are 0
//
// the COMPONENTS of the PC ARGB pixel are
//
// bits 24-31 are 0
// bits 16-23 are RED
// bits  8-15 are GREEN
// bits  0-7  are BLUE

   static int instanceCount;

   static unsigned char *RGBTbl; // 4M values of UVY xform 

   unsigned char *rgbtbl;

///////////////////////////////////////////////////////////////////

	// if _isWiperComparand is true, _comparandDividerPositionPerRow
   // must point to an array with one short per row of the frame.
	bool _isWiperComparand = false;
	int *_comparandDividerPositionPerRow = nullptr;

///////////////////////////////////////////////////////////////////

   static unsigned char *Linear8; // 256 values of linear xform

   unsigned char *linear8;

   static unsigned char *Linear16;  // 65536 values of linear xform

   unsigned char *linear16;

   static unsigned char *AntiLog8 ; // 256 values of  8-bit antilog

   unsigned char *antilog8;

   static unsigned char *AntiLog16; // 65536 values of 16-bit antilog

   unsigned char *antilog16;

   static unsigned char *AntiHalf; // 65536 values of 16-bit HALF antilog

   unsigned char *antihalf;

///////////////////////////////////////////////////////////////////

   static unsigned int *GreyTbl;

///////////////////////////////////////////////////////////////////

   static unsigned char *PosTbl;

   static unsigned char *NegTbl;

///////////////////////////////////////////////////////////////////

   unsigned int rgbMask;

///////////////////////////////////////////////////////////////////

#define PNMASK_POS 2
#define PNMASK_NEG 4
#define PNMASK_ANY 6

   unsigned int pnMask;

///////////////////////////////////////////////////////////////////

   unsigned char alphaVal;

///////////////////////////////////////////////////////////////////

// THIS IS THE MAX WIDTH OF THE DISPLAY MONITOR
#define MAXCOL 4096

    int *xstrTbl;

///////////////////////////////////////////////////////////////////

// THIS IS THE MAX HEIGHT OF THE DISPLAY MONITOR
#define MAXROW 4096

   ROWENTRY *ystrTbl;

///////////////////////////////////////////////////////////////////
//
// room for a single converted row (RGBA) per stripe

#define MAX_FRAME_WIDTH (10 * 1024)
#define MAX_CONVERSION_STRIPES 16

   unsigned int *RGBrow[MAX_CONVERSION_STRIPES];

   unsigned int *ALPHArow[MAX_CONVERSION_STRIPES];

   ////////////////////////////////////////////////////////////////
   //
   unsigned char **curSrcBuf;

   MTI_UINT16 *curSrcInt;

   const CImageFormat *curFmt;

   int curSrcWdth;
   int curSrcHght;
   int curSrcPitch;
   int curSrcColorSpace;
   int curSrcPelComponents;
   int curSrcPelPacking;
   bool curInterlaced;
   EAlphaMatteType curAlphaMatteType;
   RECT curSrcRect;
   unsigned int *curDstBuf;
   int curDstWdth;
   int curDstHght;
   int curDstPitch;
   int curWinWdth;
   int curWinHght;

   enum ConvertType
   {
      Unknown,
      PlainConvert,
      NormalDisplayConvert,
      IntermediateDisplayConvert
   } curConvertType;

   int lfts, // for normal YUV422, see inner loop for converting rows
       mids,
       rgts;

   int phase; // for 10-bit DVS and MON, range 0-2

   ///////////////////////////////////////////////////////////////

   int nStripes; // number of stripes in frame MUST NOT BE GREATER THAN MAX_CONVERSION_STRIPES

   MTHREAD_STRUCT tsThread;

};

#endif
