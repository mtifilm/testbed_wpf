// ImageInfo.cpp: implementation of the CImageInfo class.
//
/*
$Header: /usr/local/filmroot/format/source/ImageInfo.cpp,v 1.34.4.2 2009/01/24 04:35:12 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "err_format.h"
#include "ImageInfo.h"
#include "IniFile.h"
#include "MTIio.h"
#include "MTIstringstream.h"

static bool DpxIsDefaultLogCache = false; // Default is LINEAR
static bool DpxIsDefaultLogCacheIsValid = false;

//////////////////////////////////////////////////////////////////////
// Static Data
//////////////////////////////////////////////////////////////////////

// Image Format Type/Name Map Table

struct SImageFormatTypeMap
{
   const char* name;
   EImageFormatType type;
};

static const SImageFormatTypeMap imageFormatTypeMap[] =
{
   {"525_5994",      IF_TYPE_525_5994  }, // Digital NTSC, "Standard Definition"
   {"525_60",        IF_TYPE_525_60    }, // Digital NTSC, "Standard Definition"
   {"625_4795",      IF_TYPE_625_4795    }, // Digital PAL, "Standard Def"
   {"625_48",        IF_TYPE_625_48    }, // Digital PAL, "Standard Def"
   {"625_50",        IF_TYPE_625_50    }, // Digital PAL, "Standard Def"
   {"720P_50",       IF_TYPE_720P_50   }, // 720 progressive-scan, 50 frames/second
   {"720P_5994",     IF_TYPE_720P_5994 }, // 720 progressive-scan, 60 frames/second
   {"720P_60",       IF_TYPE_720P_60   }, // 720 progressive-scan, 60 frames/second
   {"1080I_50",      IF_TYPE_1080I_50  }, // 1080 interlaced, 50 frames/second
   {"1080I_5994",    IF_TYPE_1080I_5994}, // 1080 interlaced, 60 frames/second
   {"1080I_60",      IF_TYPE_1080I_60  }, // 1080 interlaced, 60 frames/second
   {"1080P_2398",    IF_TYPE_1080P_2398}, // 1080 progressive-scan, 24 frames/second
   {"1080PSF_2398",  IF_TYPE_1080PSF_2398},  // 1080 progressive-scan, 24 frames/second
   {"1080P_24",      IF_TYPE_1080P_24  }, // 1080 progressive-scan, 24 frames/second
   {"1080PSF_24",    IF_TYPE_1080PSF_24}, // 1080 progressive-scan, 24 frames/second
   {"1080P_25",      IF_TYPE_1080P_25  }, // 1080 progressive-scan, 25 frames/second
   {"1080PSF_25",    IF_TYPE_1080PSF_25}, // 1080 progressive-scan, 25 frames/second

   {"525_5994_DVS",      IF_TYPE_525_5994_DVS  }, // Digital NTSC, "Standard Definition"
   {"525_60_DVS",        IF_TYPE_525_60_DVS    }, // Digital NTSC, "Standard Definition"
   {"625_4795_DVS",      IF_TYPE_625_4795_DVS    }, // Digital PAL, "Standard Def"
   {"625_48_DVS",        IF_TYPE_625_48_DVS    }, // Digital PAL, "Standard Def"
   {"625_50_DVS",        IF_TYPE_625_50_DVS    }, // Digital PAL, "Standard Def"
   {"720P_50_DVS",       IF_TYPE_720P_50_DVS   }, // 720 progressive-scan, 50 frames/second
   {"720P_5994_DVS",     IF_TYPE_720P_5994_DVS }, // 720 progressive-scan, 60 frames/second
   {"720P_60_DVS",       IF_TYPE_720P_60_DVS   }, // 720 progressive-scan, 60 frames/second
   {"1080I_50_DVS",      IF_TYPE_1080I_50_DVS  }, // 1080 interlaced, 50 frames/second
   {"1080I_5994_DVS",    IF_TYPE_1080I_5994_DVS}, // 1080 interlaced, 60 frames/second
   {"1080I_60_DVS",      IF_TYPE_1080I_60_DVS  }, // 1080 interlaced, 60 frames/second
   {"1080P_2398_DVS",    IF_TYPE_1080P_2398_DVS}, // 1080 progressive-scan, 24 frames/second
   {"1080PSF_2398_DVS",  IF_TYPE_1080PSF_2398_DVS},  // 1080 progressive-scan, 24 frames/second
   {"1080P_24_DVS",      IF_TYPE_1080P_24_DVS  }, // 1080 progressive-scan, 24 frames/second
   {"1080PSF_24_DVS",    IF_TYPE_1080PSF_24_DVS}, // 1080 progressive-scan, 24 frames/second
   {"1080P_25_DVS",      IF_TYPE_1080P_25_DVS  }, // 1080 progressive-scan, 25 frames/second
   {"1080PSF_25_DVS",    IF_TYPE_1080PSF_25_DVS}, // 1080 progressive-scan, 25 frames/second

   {"DPX",           IF_TYPE_FILE_DPX  }, // SMPTE DPX film file format
   {"CINEON",        IF_TYPE_FILE_CINEON},// Kodak CINEON file format (similar to DPX)
   {"TIFF",          IF_TYPE_FILE_TIFF }, // TIFF graphics file format
   {"SGI",           IF_TYPE_FILE_SGI  }, // Silicon Graphics graphics file format

   {"INTERNAL",      IF_TYPE_INTERNAL  }, // Internal format, typically 16-bit
                                          // progressive, YUV444 or RGB
   {"TGA",          IF_TYPE_FILE_TGA }, // TGA graphics file format
   {"TGA2",          IF_TYPE_FILE_TGA2 }, // TGA graphics file format
   {"BMP",           IF_TYPE_FILE_BMP }, // BMP graphics file format
   {"EXR",           IF_TYPE_FILE_EXR }, // EXR graphics file format

   {"2048_1536PSF_24_DVS",   IF_TYPE_2048_1536PSF_24_DVS   },
   {"2048_1536P_24_DVS",     IF_TYPE_2048_1536P_24_DVS     },
   {"2048_1556PSF_1498_DVS", IF_TYPE_2048_1556PSF_1498_DVS },
   {"2048_1556PSF_15_DVS",   IF_TYPE_2048_1556PSF_15_DVS   },
   {"2048_1556PSF_24_DVS",   IF_TYPE_2048_1556PSF_24_DVS   },
   {"2048_1556P_24_DVS",     IF_TYPE_2048_1556P_24_DVS     },
   {"2048_1556PSF_1498_25_DVS", IF_TYPE_2048_1556PSF_1498_25_DVS},
   {"2048_1556PSF_15_25_DVS", IF_TYPE_2048_1556PSF_15_25_DVS  },

   // 525 Proxy Formats
   // Nominally 23.98/24 frames-per-second WITHOUT 3:2 pulldown
   {"525_4795",              IF_TYPE_525_4795     }, // 525/47.95   SGI 10 bit, SGI or DVS 8-bit
   {"525_48",                IF_TYPE_525_48       }, // 525/48      SGI 10 bit, SGI or DVS 8-bit
   {"525_4795_DVS",          IF_TYPE_525_4795_DVS }, // 525/47.95   DVS 10 bit
   {"525_48_DVS",            IF_TYPE_525_48_DVS   }, // 525/48      DVS 10 bit

   // 1080 Progressive
   // Media stored as two fields, odd & even lines, as though it was interlaced
   {"1080PI_2398",       IF_TYPE_1080PI_2398       },  // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
   {"1080PI_24",         IF_TYPE_1080PI_24         },  // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
   {"1080PI_25",         IF_TYPE_1080PI_25         },  // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
   {"1080PI_2398_DVS",   IF_TYPE_1080PI_2398_DVS   },  // 1080P/23.98   DVS 10 bit
   {"1080PI_24_DVS",     IF_TYPE_1080PI_24_DVS     },  // 1080P/24      DVS 10 bit
   {"1080PI_25_DVS",     IF_TYPE_1080PI_25_DVS     },  // 1080P/25      DVS 10 bit

   // 1080 Progressive, Segmented Field
   // Media stored as two fields, odd & even lines, as though it was interlaced
   {"1080PSFI_2398",     IF_TYPE_1080PSFI_2398     },  // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
   {"1080PSFI_24",       IF_TYPE_1080PSFI_24       },  // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
   {"1080PSFI_25",       IF_TYPE_1080PSFI_25       },  // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
   {"1080PSFI_2398_DVS", IF_TYPE_1080PSFI_2398_DVS },  // 1080PsF/23.98 DVS 10 bit
   {"1080PSFI_24_DVS",   IF_TYPE_1080PSFI_24_DVS   },  // 1080PsF/24    DVS 10 bit
   {"1080PSFI_25_DVS",   IF_TYPE_1080PSFI_25_DVS   },  // 1080PsF/25    DVS 10 bit

   // "Progressive" SD formats for when the data is line-interleaved in
   // one file per frame
   {"525IP_5994",          IF_TYPE_525IP_5994     },  // 525/P29.97     SD PROGRESSIVE "NTSC"
   {"525IP_60",            IF_TYPE_525IP_60       },  // 525/P30        SD PROGRESSIVE "NTSC"
   {"525P_2398",           IF_TYPE_525P_2398     },  // 525/P23.98     SD PROGRESSIVE "NTSC"
   {"525P_24",             IF_TYPE_525P_24       },  // 525/P24        SD FILM FRAMES "NTSC"
   {"625P_25",             IF_TYPE_625P_25       },  // 625/P25        SD PROGRESSIVE "PAL"
   {"525IP_5994_DVS",      IF_TYPE_525IP_5994_DVS },  // 525/P29.97     SD PROGRESSIVE "NTSC"
   {"525IP_60_DVS",        IF_TYPE_525IP_60_DVS   },  // 525/P30        SD PROGRESSIVE "NTSC"
   {"525P_2398_DVS",       IF_TYPE_525P_2398_DVS },  // 525/P23.98     SD PROGRESSIVE "NTSC"
   {"525P_24_DVS",         IF_TYPE_525P_24_DVS   },  // 525/P24        SD FILM FRAMES "NTSC"
   {"625P_25_DVS",         IF_TYPE_625P_25_DVS   },  // 625/P25        SD PROGRESSIVE "PAL"

   {"IF_TYPE_525_5994",      IF_TYPE_525_5994  }, // Digital NTSC, "Standard Definition"
   {"IF_TYPE_525_60",        IF_TYPE_525_60    }, // Digital NTSC, "Standard Definition"
   {"IF_TYPE_625_4795",      IF_TYPE_625_4795    }, // Digital PAL, "Standard Def"
   {"IF_TYPE_625_48",        IF_TYPE_625_48    }, // Digital PAL, "Standard Def"
   {"IF_TYPE_625_50",        IF_TYPE_625_50    }, // Digital PAL, "Standard Def"
   {"IF_TYPE_720P_50",       IF_TYPE_720P_50   }, // 720 progressive-scan, 50 frames/second
   {"IF_TYPE_720P_5994",     IF_TYPE_720P_5994 }, // 720 progressive-scan, 60 frames/second
   {"IF_TYPE_720P_60",       IF_TYPE_720P_60   }, // 720 progressive-scan, 60 frames/second
   {"IF_TYPE_1080I_50",      IF_TYPE_1080I_50  }, // 1080 interlaced, 50 frames/second
   {"IF_TYPE_1080I_5994",    IF_TYPE_1080I_5994}, // 1080 interlaced, 60 frames/second
   {"IF_TYPE_1080I_60",      IF_TYPE_1080I_60  }, // 1080 interlaced, 60 frames/second
   {"IF_TYPE_1080P_2398",    IF_TYPE_1080P_2398}, // 1080 progressive-scan, 24 frames/second
   {"IF_TYPE_1080PSF_2398",  IF_TYPE_1080PSF_2398},  // 1080 progressive-scan, 24 frames/second
   {"IF_TYPE_1080P_24",      IF_TYPE_1080P_24  }, // 1080 progressive-scan, 24 frames/second
   {"IF_TYPE_1080PSF_24",    IF_TYPE_1080PSF_24}, // 1080 progressive-scan, 24 frames/second
   {"IF_TYPE_1080P_25",      IF_TYPE_1080P_25  }, // 1080 progressive-scan, 25 frames/second
   {"IF_TYPE_1080PSF_25",    IF_TYPE_1080PSF_25}, // 1080 progressive-scan, 25 frames/second

   {"IF_TYPE_525_5994_DVS",      IF_TYPE_525_5994_DVS  }, // Digital NTSC, "Standard Definition"
   {"IF_TYPE_525_60_DVS",        IF_TYPE_525_60_DVS    }, // Digital NTSC, "Standard Definition"
   {"IF_TYPE_625_4795_DVS",      IF_TYPE_625_4795_DVS    }, // Digital PAL, "Standard Def"
   {"IF_TYPE_625_48_DVS",        IF_TYPE_625_48_DVS    }, // Digital PAL, "Standard Def"
   {"IF_TYPE_625_50_DVS",        IF_TYPE_625_50_DVS    }, // Digital PAL, "Standard Def"
   {"IF_TYPE_720P_50_DVS",       IF_TYPE_720P_50_DVS   }, // 720 progressive-scan, 50 frames/second
   {"IF_TYPE_720P_5994_DVS",     IF_TYPE_720P_5994_DVS }, // 720 progressive-scan, 60 frames/second
   {"IF_TYPE_720P_60_DVS",       IF_TYPE_720P_60_DVS   }, // 720 progressive-scan, 60 frames/second
   {"IF_TYPE_1080I_50_DVS",      IF_TYPE_1080I_50_DVS  }, // 1080 interlaced, 50 frames/second
   {"IF_TYPE_1080I_5994_DVS",    IF_TYPE_1080I_5994_DVS}, // 1080 interlaced, 60 frames/second
   {"IF_TYPE_1080I_60_DVS",      IF_TYPE_1080I_60_DVS  }, // 1080 interlaced, 60 frames/second
   {"IF_TYPE_1080P_2398_DVS",    IF_TYPE_1080P_2398_DVS}, // 1080 progressive-scan, 24 frames/second
   {"IF_TYPE_1080PSF_2398_DVS",  IF_TYPE_1080PSF_2398_DVS},  // 1080 progressive-scan, 24 frames/second
   {"IF_TYPE_1080P_24_DVS",      IF_TYPE_1080P_24_DVS  }, // 1080 progressive-scan, 24 frames/second
   {"IF_TYPE_1080PSF_24_DVS",    IF_TYPE_1080PSF_24_DVS}, // 1080 progressive-scan, 24 frames/second
   {"IF_TYPE_1080P_25_DVS",      IF_TYPE_1080P_25_DVS  }, // 1080 progressive-scan, 25 frames/second
   {"IF_TYPE_1080PSF_25_DVS",    IF_TYPE_1080PSF_25_DVS}, // 1080 progressive-scan, 25 frames/second

   {"IF_TYPE_DPX",           IF_TYPE_FILE_DPX  }, // SMPTE DPX film file format
   {"IF_TYPE_CINEON",        IF_TYPE_FILE_CINEON},// Kodak CINEON file format (similar to DPX)
   {"IF_TYPE_TIFF",          IF_TYPE_FILE_TIFF }, // TIFF graphics file format
   {"IF_TYPE_SGI",           IF_TYPE_FILE_SGI  }, // Silicon Graphics graphics file format

   {"IF_TYPE_INTERNAL",      IF_TYPE_INTERNAL  }, // Internal format, typically 16-bit
                                                  // progressive, YUV444 or RGB
   {"IF_TYPE_TGA",           IF_TYPE_FILE_TGA  }, // TGA graphics file format
   {"IF_TYPE_TGA2",          IF_TYPE_FILE_TGA2 }, // TGA graphics file format
   {"IF_TYPE_BMP",           IF_TYPE_FILE_BMP }, // BMP graphics file format
   {"IF_TYPE_EXR",           IF_TYPE_FILE_EXR }, // EXR graphics file format

   {"IF_TYPE_2048_1536PSF_24_DVS",   IF_TYPE_2048_1536PSF_24_DVS   },
   {"IF_TYPE_2048_1536P_24_DVS",     IF_TYPE_2048_1536P_24_DVS     },
   {"IF_TYPE_2048_1556PSF_1498_DVS", IF_TYPE_2048_1556PSF_1498_DVS },
   {"IF_TYPE_2048_1556PSF_15_DVS",   IF_TYPE_2048_1556PSF_15_DVS   },
   {"IF_TYPE_2048_1556PSF_24_DVS",   IF_TYPE_2048_1556PSF_24_DVS   },
   {"IF_TYPE_2048_1556P_24_DVS",     IF_TYPE_2048_1556P_24_DVS     },

   // 525 Proxy Formats
   // Nominally 23.98/24 frames-per-second WITHOUT 3:2 pulldown
   {"IF_TYPE_525_4795",              IF_TYPE_525_4795     }, // 525/47.95   SGI 10 bit, SGI or DVS 8-bit
   {"IF_TYPE_525_48",                IF_TYPE_525_48       }, // 525/48      SGI 10 bit, SGI or DVS 8-bit
   {"IF_TYPE_525_4795_DVS",          IF_TYPE_525_4795_DVS }, // 525/47.95   DVS 10 bit
   {"IF_TYPE_525_48_DVS",            IF_TYPE_525_48_DVS   }, // 525/48      DVS 10 bit

   // 1080 Progressive
   // Media stored as two fields, odd & even lines, as though it was interlaced
   {"IF_TYPE_1080PI_2398",       IF_TYPE_1080PI_2398       },  // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
   {"IF_TYPE_1080PI_24",         IF_TYPE_1080PI_24         },  // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
   {"IF_TYPE_1080PI_25",         IF_TYPE_1080PI_25         },  // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
   {"IF_TYPE_1080PI_2398_DVS",   IF_TYPE_1080PI_2398_DVS   },  // 1080P/23.98   DVS 10 bit
   {"IF_TYPE_1080PI_24_DVS",     IF_TYPE_1080PI_24_DVS     },  // 1080P/24      DVS 10 bit
   {"IF_TYPE_1080PI_25_DVS",     IF_TYPE_1080PI_25_DVS     },  // 1080P/25      DVS 10 bit

   // 1080 Progressive, Segmented Field
   // Media stored as two fields, odd & even lines, as though it was interlaced
   {"IF_TYPE_1080PSFI_2398",     IF_TYPE_1080PSFI_2398     },  // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
   {"IF_TYPE_1080PSFI_24",       IF_TYPE_1080PSFI_24       },  // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
   {"IF_TYPE_1080PSFI_25",       IF_TYPE_1080PSFI_25       },  // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
   {"IF_TYPE_1080PSFI_2398_DVS", IF_TYPE_1080PSFI_2398_DVS },  // 1080PsF/23.98 DVS 10 bit
   {"IF_TYPE_1080PSFI_24_DVS",   IF_TYPE_1080PSFI_24_DVS   },  // 1080PsF/24    DVS 10 bit
   {"IF_TYPE_1080PSFI_25_DVS",   IF_TYPE_1080PSFI_25_DVS   },  // 1080PsF/25    DVS 10 bit

   // "Progressive" SD formats for when the data is line-interleaved in
   // one file per frame
   {"IF_TYPE_525IP_5994",     IF_TYPE_525IP_5994     },  // 525/P29.97     SD PROGRESSIVE "NTSC"
   {"IF_TYPE_525IP_60",       IF_TYPE_525IP_60       },  // 525/P30        SD PROGRESSIVE "NTSC"
   {"IF_TYPE_525P_2398",      IF_TYPE_525P_2398      },  // 525/P23.98     SD PROGRESSIVE "NTSC"
   {"IF_TYPE_525P_24",        IF_TYPE_525P_24        },  // 525/P24        SD FILM FRAMES "NTSC"
   {"IF_TYPE_625P_25",        IF_TYPE_625P_25        },  // 625/P25        SD PROGRESSIVE "PAL"
   {"IF_TYPE_525IP_5994_DVS", IF_TYPE_525IP_5994_DVS },  // 525/P29.97     SD PROGRESSIVE "NTSC"
   {"IF_TYPE_525IP_60_DVS",   IF_TYPE_525IP_60_DVS   },  // 525/P30        SD PROGRESSIVE "NTSC"
   {"IF_TYPE_525P_2398_DVS",  IF_TYPE_525P_2398_DVS  },  // 525/P23.98     SD PROGRESSIVE "NTSC"
   {"IF_TYPE_525P_24_DVS",    IF_TYPE_525P_24_DVS    },  // 525/P24        SD FILM FRAMES "NTSC"
   {"IF_TYPE_625P_25_DVS",    IF_TYPE_625P_25_DVS    },  // 625/P25        SD PROGRESSIVE "PAL"
};

#define IMAGE_FORMAT_TYPE_COUNT (sizeof(imageFormatTypeMap)/sizeof(SImageFormatTypeMap))

//----------------------------------------------------------------------------
// Image Format Type Descriptions

struct SImageFormatTypeDescription
{
   EImageFormatType type;
   const char *descr1;
   const char *descr2;
};

static const SImageFormatTypeDescription imageFormatTypeDescriptions[] =
{
   // 525 Proxy
   {IF_TYPE_525_4795,          "525/47.95",     "SD Interlaced, Special Proxy, No Pulldown, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_525_4795_DVS,      "525/47.95",     "SD Interlaced, Special Proxy, No Pulldown, DVS 10 bit"},
   {IF_TYPE_525_48,            "525/48",        "SD Interlaced, Special Proxy, No Pulldown, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_525_48,            "525/48",        "SD Interlaced, Special Proxy, No Pulldown, DVS 10 bit"},

   // 525 SD 'NTSC'
   {IF_TYPE_525_5994,          "525/59.94",     "SD Interlaced 'NTSC', DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_525_5994_DVS,      "525/59.94",     "SD Interlaced 'NTSC', DVS 10 bit"},
   {IF_TYPE_525_60,            "525/60",        "SD Interlaced 'NTSC', DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_525_60_DVS,        "525/59.94",     "SD Interlaced 'NTSC', DVS 10 bit"},

   // 625 Proxy
   {IF_TYPE_625_4795,          "625/47.95",     "SD Interlaced, Special Proxy, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_625_4795_DVS,      "625/47.95",     "SD Interlaced, Special Proxy, DVS 10 bit"},
   {IF_TYPE_625_48,            "625/48",        "SD Interlaced, Special Proxy, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_625_48_DVS,        "625/48",        "SD Interlaced, Special Proxy, DVS 10 bit"},

   // 625 SD 'PAL'
   {IF_TYPE_625_50,            "625/50",        "SD Interlaced, 'PAL', DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_625_50_DVS,        "625/50",        "SD Interlaced, 'PAL', DVS 10 bit"},

   // 720P HD
   {IF_TYPE_720P_50,           "720P/50",       "HD Progressive, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_720P_50_DVS,       "720P/50",       "HD Progressive, DVS 10 bit"},
   {IF_TYPE_720P_5994,         "720P/59.94",    "HD Progressive, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_720P_5994_DVS,     "720P/59.94",    "HD Progressive, DVS 10 bit"},
   {IF_TYPE_720P_60,           "720P/60",       "HD Progressive, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_720P_60_DVS,       "720P/60",       "HD Progressive, DVS 10 bit"},

   // 1080I HD
   {IF_TYPE_1080I_50,          "1080I/59.94",   "HD Interlaced, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080I_50_DVS,      "1080I/59.94",   "HD Interlaced, DVS 10 bit"},
   {IF_TYPE_1080I_5994,        "1080I/60",      "HD Interlaced, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080I_5994_DVS,    "1080I/60",      "HD Interlaced, DVS 10 bit"},
   {IF_TYPE_1080I_60,          "1080I/50",      "HD Interlaced, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080I_60_DVS,      "1080I/50",      "HD Interlaced, DVS 10 bit"},

   // 1080P HD, Non-segmented
   // Media stored as single progressive frame
   {IF_TYPE_1080P_2398,        "1080P/23.98",   "HD Progressive, Non-segmented, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080P_2398_DVS,    "1080P/23.98",   "HD Progressive, Non-segmented, DVS 10 bit"},
   {IF_TYPE_1080P_24,          "1080P/24",      "HD Progressive, Non-segmented, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080P_24_DVS,      "1080P/24",      "HD Progressive, Non-segmented, DVS 10 bit"},
   {IF_TYPE_1080P_25,          "1080P/25",      "HD Progressive, Non-segmented, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080P_25_DVS,      "1080P/25",      "HD Progressive, Non-segmented, DVS 10 bit"},

   // 1080PsF HD, Segmented Field
   // Media stored as single progressive frame
   {IF_TYPE_1080PSF_2398,      "1080PsF/23.98", "HD Progressive, Segmented Frame, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080PSF_2398_DVS,  "1080PsF/23.98", "HD Progressive, Segmented Frame, DVS 10 bit"},
   {IF_TYPE_1080PSF_24,        "1080PsF/24",    "HD Progressive, Segmented Frame, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080PSF_24_DVS,    "1080PsF/24",    "HD Progressive, Segmented Frame, DVS 10 bit"},
   {IF_TYPE_1080PSF_25,        "1080PsF/25",    "HD Progressive, Segmented Frame, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080PSF_25_DVS,    "1080PsF/25",    "HD Progressive, Segmented Frame, DVS 10 bit"},

   // 1080 Progressive
   // Media stored as two fields, odd & even lines, as though it was interlaced
   {IF_TYPE_1080PI_2398,        "1080P/23.98",   "HD Progressive, Non-segmented, Odd/Even Fields, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080PI_2398_DVS,    "1080P/23.98",   "HD Progressive, Non-segmented, Odd/Even Fields, DVS 10 bit"},
   {IF_TYPE_1080PI_24,          "1080P/24",      "HD Progressive, Non-segmented, Odd/Even Fields, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080PI_24_DVS,      "1080P/24",      "HD Progressive, Non-segmented, Odd/Even Fields, DVS 10 bit"},
   {IF_TYPE_1080PI_25,          "1080P/25",      "HD Progressive, Non-segmented, Odd/Even Fields, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080PI_25_DVS,      "1080P/25",      "HD Progressive, Non-segmented, Odd/Even Fields, DVS 10 bit"},

   // 1080 Progressive, Segmented Field
   // Media stored as two fields, odd & even lines, as though it was interlaced
   {IF_TYPE_1080PSFI_2398,      "1080PsF/23.98", "HD Progressive, Segmented Frame, Odd/Even Fields, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080PSFI_2398_DVS,  "1080PsF/23.98", "HD Progressive, Segmented Frame, Odd/Even Fields, DVS 10 bit"},
   {IF_TYPE_1080PSFI_24,        "1080PsF/24",    "HD Progressive, Segmented Frame, Odd/Even Fields, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080PSFI_24_DVS,    "1080PsF/24",    "HD Progressive, Segmented Frame, Odd/Even Fields, DVS 10 bit"},
   {IF_TYPE_1080PSFI_25,        "1080PsF/25",    "HD Progressive, Segmented Frame, Odd/Even Fields, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_1080PSFI_25_DVS,    "1080PsF/25",    "HD Progressive, Segmented Frame, Odd/Even Fields, DVS 10 bit"},

   // Image File
   {IF_TYPE_FILE_DPX,           "DPX",           "SMPTE DPX Image File"},
   {IF_TYPE_FILE_CINEON,        "CINEON",        "Kodak Cineon Image File"},
   {IF_TYPE_FILE_TIFF,          "TIFF",          "Image File"},
   {IF_TYPE_FILE_SGI,           "SGI",           "Image File"},
   {IF_TYPE_FILE_TGA,           "Targa",         "Image File"},
   {IF_TYPE_FILE_TGA2,          "Targa 2",       "Image File"},
   {IF_TYPE_FILE_BMP,           "BMP",           "MS Bitmap Image File"},
   {IF_TYPE_FILE_EXR,           "EXR",           "EXR Bitmap Image File"},

   // Internal
   {IF_TYPE_INTERNAL,          "Internal",      "MTI Internal Format"},

   // 2K HSDL
   {IF_TYPE_2048_1536PSF_24_DVS,   "2048Psf/24",    "2048x1536, DVS HSDL, Segmented Field"},
   {IF_TYPE_2048_1536P_24_DVS,     "2048P/24",      "2048x1536, DVS HSDL, Non-segmented"},
   {IF_TYPE_2048_1556PSF_1498_DVS, "2048Psf/14.98", "2048x1556, DVS HSDL, Segmented Field"},
   {IF_TYPE_2048_1556PSF_15_DVS,   "2048Psf/15",    "2048x1556, DVS HSDL, Segmented Field"},
   {IF_TYPE_2048_1556PSF_24_DVS,   "2048Psf/24",    "2048x1556, DVS HSDL, Segmented Field"},
   {IF_TYPE_2048_1556P_24_DVS,     "2048P/24",      "2048x1556, DVS HSDL, Non-segmented"},

   // SD "Progrssive" fake formats for dealing with video stored in DPX files
   // NOTE: the "interlaced stored as progressive" formats are not yet
   //       implemented, pending evaluation of whether anyone actually would
   //       have a desire for such a beast. If you use the regular 525 and
   //       625 formats with DPX files, you get one field per file, which
   //       I imagine is not very interchangeable with other systems.
   {IF_TYPE_525IP_5994,     "525i/59.94",  "SD Interlaced stored as Progressive, 'NTSC', DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_525IP_60,       "525i/60",     "SD Interlaced stored as Progressive, 'NTSC', DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_525P_2398,      "525P/2398",   "SD Progressive, 'NTSC', No Pulldown, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_525P_24,        "525P/24",     "SD Progressive, 'NTSC', No Pulldown, DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_625P_25,        "625P/25",     "SD Progressive, 'PAL', DVS 8 bit, SGI 8 & 10 bit"},
   {IF_TYPE_525IP_5994_DVS, "525i/59.94",  "SD Interlaced stored as Progressive, 'NTSC', DVS 10 bit"},
   {IF_TYPE_525IP_60_DVS,   "525i/60",     "SD Interlaced stored as Progressive, 'NTSC', DVS 10 bit"},
   {IF_TYPE_525P_2398_DVS,  "525P/2398",   "SD Progressive, 'NTSC', No Pulldown, DVS 10 bit"},
   {IF_TYPE_525P_24_DVS,    "525P/24",     "SD Progressive, 'NTSC', No Pulldown, DVS 10 bit"},
   {IF_TYPE_625P_25_DVS,    "625P/25",     "SD Progressive, 'PAL', DVS 10 bit"}
};

#define IMAGE_FORMAT_TYPE_DESCR_COUNT (sizeof(imageFormatTypeDescriptions)/sizeof(SImageFormatTypeDescription))

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

static const SIniAssociation SynchAssociationsData[] =
{
   {"VI_SYNC_SOURCE_DEFAULT",              VI_SYNC_SOURCE_INVALID},
   {"VI_SYNC_SOURCE_INTERNAL",             VI_SYNC_SOURCE_INTERNAL},
   {"VI_SYNC_SOURCE_VIDEO_INPUT_A",        VI_SYNC_SOURCE_VIDEO_INPUT_A},
   {"VI_SYNC_SOURCE_VIDEO_INPUT_B",        VI_SYNC_SOURCE_VIDEO_INPUT_B},
   {"VI_SYNC_SOURCE_EXTERNAL_525_60",      VI_SYNC_SOURCE_EXTERNAL_525_60},
   {"VI_SYNC_SOURCE_EXTERNAL_625_50",      VI_SYNC_SOURCE_EXTERNAL_625_50},
   {"VI_SYNC_SOURCE_EXTERNAL_750_60",      VI_SYNC_SOURCE_EXTERNAL_750_60},
   {"VI_SYNC_SOURCE_EXTERNAL_750_5994",    VI_SYNC_SOURCE_EXTERNAL_750_5994},
   {"VI_SYNC_SOURCE_EXTERNAL_1125_60",     VI_SYNC_SOURCE_EXTERNAL_1125_60},
   {"VI_SYNC_SOURCE_EXTERNAL_1125_5994",   VI_SYNC_SOURCE_EXTERNAL_1125_5994},
   {"VI_SYNC_SOURCE_EXTERNAL_1125_50",     VI_SYNC_SOURCE_EXTERNAL_1125_50},
   {"VI_SYNC_SOURCE_EXTERNAL_1125_48",     VI_SYNC_SOURCE_EXTERNAL_1125_48},
   {"VI_SYNC_SOURCE_EXTERNAL_1125_4796",   VI_SYNC_SOURCE_EXTERNAL_1125_4796},
   {"VI_SYNC_SOURCE_EXTERNAL_1125_25",     VI_SYNC_SOURCE_EXTERNAL_1125_25},
   {"VI_SYNC_SOURCE_EXTERNAL_1125_24",     VI_SYNC_SOURCE_EXTERNAL_1125_24},
   {"VI_SYNC_SOURCE_EXTERNAL_1125_2398",   VI_SYNC_SOURCE_EXTERNAL_1125_2398},
   {"VI_SYNC_SOURCE_EXTERNAL_750_50",      VI_SYNC_SOURCE_EXTERNAL_750_50},
   {"VI_SYNC_SOURCE_NONE",                 VI_SYNC_SOURCE_NONE},
   {"", -1}
};

//////////////////////////////////////////////////////////////////////
// Video Field Rate
//
static const SIniAssociation VideoFieldRateAssociationData[] =
{
   {"VI_VIDEO_FIELD_RATE_DEFAULT", VI_VIDEO_FIELD_RATE_INVALID},
   {"VI_VIDEO_FIELD_RATE_60", VI_VIDEO_FIELD_RATE_60},
   {"VI_VIDEO_FIELD_RATE_5994", VI_VIDEO_FIELD_RATE_5994},
   {"VI_VIDEO_FIELD_RATE_50", VI_VIDEO_FIELD_RATE_50},
   {"VI_VIDEO_FIELD_RATE_48", VI_VIDEO_FIELD_RATE_48},
   {"VI_VIDEO_FIELD_RATE_4796", VI_VIDEO_FIELD_RATE_4796},
   {"VI_VIDEO_FIELD_RATE_30", VI_VIDEO_FIELD_RATE_30},
   {"VI_VIDEO_FIELD_RATE_2997", VI_VIDEO_FIELD_RATE_2997},
   {"VI_VIDEO_FIELD_RATE_25", VI_VIDEO_FIELD_RATE_25},
   {"VI_VIDEO_FIELD_RATE_24", VI_VIDEO_FIELD_RATE_24},
   {"VI_VIDEO_FIELD_RATE_2398", VI_VIDEO_FIELD_RATE_2398},
   {"VI_VIDEO_FIELD_RATE_1498", VI_VIDEO_FIELD_RATE_1498},
   {"VI_VIDEO_FIELD_RATE_15", VI_VIDEO_FIELD_RATE_15},
   {"VI_VIDEO_FIELD_RATE_2997_25", VI_VIDEO_FIELD_RATE_2997_25},
   {"VI_VIDEO_FIELD_RATE_30_25", VI_VIDEO_FIELD_RATE_30_25},
   {"VI_VIDEO_FIELD_RATE_1498_25", VI_VIDEO_FIELD_RATE_1498_25},
   {"VI_VIDEO_FIELD_RATE_15_25", VI_VIDEO_FIELD_RATE_15_25},
   {"", -1}
};

//////////////////////////////////////////////////////////////////////
// Video Progressive Type;
//
static const SIniAssociation VideoProgressiveAssociationData[] =
{
   {"VI_VIDEO_PROGRESSIVE_TYPE_DEFAULT", VI_VIDEO_PROGRESSIVE_TYPE_INVALID},
   {"VI_VIDEO_PROGRESSIVE_TYPE_P", VI_VIDEO_PROGRESSIVE_TYPE_P},
   {"VI_VIDEO_PROGRESSIVE_TYPE_SF", VI_VIDEO_PROGRESSIVE_TYPE_SF},
   {"", -1}
};

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Video Loopback
//
static const SIniAssociation VideoLoopbackAssociationsData[] =
{
   {"VI_VIDEO_LOOPBACK_DEFAULT", VI_VIDEO_LOOPBACK_INVALID},
   {"VI_VIDEO_LOOPBACK_OFF", VI_VIDEO_LOOPBACK_OFF},
   {"VI_VIDEO_LOOPBACK_ON", VI_VIDEO_LOOPBACK_ON},
   {"", -1}
};

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Video Precision
//
static const SIniAssociation VideoPrecisionAssociationsData[] =
{
   {"VI_EXTERNAL_PRECISION_DEFAULT", VI_EXTERNAL_PRECISION_INVALID},
   {"VI_EXTERNAL_PRECISION_8", VI_EXTERNAL_PRECISION_8},
   {"VI_EXTERNAL_PRECISION_10", VI_EXTERNAL_PRECISION_10},
   {"", -1}
};


CIniAssociations SynchAssociations(SynchAssociationsData, "VI_SYNC_SOURCE_");
CIniAssociations VideoLoopbackAssociations(VideoLoopbackAssociationsData, "VI_VIDEO_");
CIniAssociations VideoPrecisionAssociations(VideoPrecisionAssociationsData, "VI_EXTERNAL_");
CIniAssociations VideoProgressiveAssociations(VideoProgressiveAssociationData, "VI_VIDEO_PROGRESSIVE_TYPE_");
CIniAssociations VideoFieldRateAssociations(VideoFieldRateAssociationData, "VI_VIDEO_FIELD_RATE_");
CIniAssociations ImageFormatAssociations((SIniAssociation *)(imageFormatTypeMap + IMAGE_FORMAT_TYPE_COUNT/2), "IF_TYPE_", IMAGE_FORMAT_TYPE_COUNT/2);

// ----------------------------------------------------------------------------
// Pixel Components Type/Name Map Table

struct SPixelComponentsTypeMap
{
   const char* name;
   EPixelComponents pixelComponents;
};

static const SPixelComponentsTypeMap pixelComponentsTypeMap[] =
{
   {"RGB",       IF_PIXEL_COMPONENTS_RGB     },
   {"RGB444",    IF_PIXEL_COMPONENTS_RGB     },
   {"RGBA",      IF_PIXEL_COMPONENTS_RGBA    }, // A = alpha matte channel
   {"YUV422",    IF_PIXEL_COMPONENTS_YUV422  }, // CbYCrY
   {"YUV4224",   IF_PIXEL_COMPONENTS_YUV4224 }, // YUV + alpha, CbYACrYA
   {"YUV444",    IF_PIXEL_COMPONENTS_YUV444  }, // CbYCr
   {"BGR",       IF_PIXEL_COMPONENTS_BGR     }, // BGR format, e.g. Targa
   {"BGRA",      IF_PIXEL_COMPONENTS_BGRA    },
   {"Y",         IF_PIXEL_COMPONENTS_Y       }, // B&W - 1 chan source, 3 chan internal
   {"LUMINANCE", IF_PIXEL_COMPONENTS_Y       }, // **** Must be AFTER {"Y", IF_PIXEL_COMPONENTS_Y} line! *****
//   {"YYY",       IF_PIXEL_COMPONENTS_YYY     }, // B&W - 3 channels with same values
};
#define PIXEL_COMPONENTS_TYPE_COUNT (sizeof(pixelComponentsTypeMap)/sizeof(SPixelComponentsTypeMap))

// ----------------------------------------------------------------------------
// Pixel Packing Type/Name Mapping Table
struct SPixelPackingTypeMap
{
   const char* name;
   EPixelPacking pixelPacking;
};

static const SPixelPackingTypeMap pixelPackingTypeMap[] =
{
   {"8Bits_IN_1Byte",          IF_PIXEL_PACKING_8Bits_IN_1Byte      }, // 8 bits, any format
   {"4_10Bits_IN_5Bytes",      IF_PIXEL_PACKING_4_10Bits_IN_5Bytes  }, // Typical 10 bit YUV422
   {"3_10Bits_IN_4Bytes_L",    IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L}, // 10 bit RGB in 32-bit word
   {"3_10Bits_IN_4Bytes_R",    IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R}, // 10 bit RGB in 32-bit word
   {"1_10Bits_IN_2Bytes_L",    IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L}, // 10 bits in 16-bit word
   {"1_10Bits_IN_2Bytes_R",    IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R}, // 10 bits in 16-bit word
   {"1_16Bits_IN_2Bytes_BE",   IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE}, // 16 bits, Big-endian
   {"1_16Bits_IN_2Bytes_LE",   IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE}, // 16 bits, Little-endian
   {"1_8Bits_IN_2Bytes",       IF_PIXEL_PACKING_1_8Bits_IN_2Bytes   }, // 8-bits, native byte ordering
   {"1_10Bits_IN_2Bytes",      IF_PIXEL_PACKING_1_10Bits_IN_2Bytes  }, // 10-bits, native byte ordering
   {"1_16Bits_IN_2Bytes_INT",  IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT  }, // 16-bits, native byte ordering
   {"3_10Bits_IN_4Bytes_DVS",  IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS},   // std DVS 10 bit in 32-bit word
   {"3_10Bits_IN_4Bytes_DVSa", IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa}, // old DVS 10 bit in 32-bit word
   {"2_12Bits_IN_3Bytes",      IF_PIXEL_PACKING_2_12Bits_IN_3Bytes  }, // 12 bits, padded, follows spec
   {"2_12Bits_IN_3Bytes_LR",   IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR }, // 12 bits, padded, reverses spec
   {"1_12Bits_IN_2Bytes_L_LE", IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE}, // 12 bits, left justified, Little-endian
   {"1_12Bits_IN_2Bytes_L_BE", IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE}, // 12 bits, left justified, Big-endian
   {"1_12Bits_IN_2Bytes",      IF_PIXEL_PACKING_1_12Bits_IN_2Bytes  }, // 12 bits, native byte ordering
   {"1_Half_IN_2Bytes",        IF_PIXEL_PACKING_1_Half_IN_2Bytes }, // 16-bit float, Little-endian
};
#define PIXEL_PACKING_TYPE_COUNT (sizeof(pixelPackingTypeMap)/sizeof(SPixelPackingTypeMap))

// ----------------------------------------------------------------------------
// Color Space Type/Name Mapping Table

struct SColorSpaceTypeMap
{
   const char* name;
   EColorSpace colorSpace;
};

static const SColorSpaceTypeMap colorSpaceTypeMap[] =
{
   {"LINEAR",    IF_COLOR_SPACE_LINEAR     }, // Typically RGB
   {"LOG",       IF_COLOR_SPACE_LOG        }, // Typically RGB
   {"SMPTE_240", IF_COLOR_SPACE_SMPTE_240  }, // Typically video
   {"CCIR_709",  IF_COLOR_SPACE_CCIR_709   }, // Typically HD video, also SD video
   {"REC_601B",  IF_COLOR_SPACE_CCIR_601_BG}, // 601 System B or G, Typically 625
   {"REC_601M",  IF_COLOR_SPACE_CCIR_601_M }, // 601 System M, Typically 525
   {"CCIR_601B", IF_COLOR_SPACE_CCIR_601_BG}, // 601 System B or G, Typically 625
   {"CCIR_601M", IF_COLOR_SPACE_CCIR_601_M }, // 601 System M, Typically 525
   {"EXR",       IF_COLOR_SPACE_EXR        }, // EXR RGB
};
#define COLOR_SPACE_TYPE_COUNT (sizeof(colorSpaceTypeMap)/sizeof(SColorSpaceTypeMap))

// ----------------------------------------------------------------------------
// Alpha Matte Type/Name Mapping Table

struct SAlphaMatteTypeMap
{
   const char *name;
   EAlphaMatteType alphaMatteType;
};

static const SAlphaMatteTypeMap alphaMatteTypeMap[] =
{
   {"Default",            IF_ALPHA_MATTE_DEFAULT },     // type depends on image format
   {"None",               IF_ALPHA_MATTE_NONE  },       // Alpha Matte not present
   {"1_BIT",              IF_ALPHA_MATTE_1_BIT },       //  1-bit per pixel
   {"8_BIT",              IF_ALPHA_MATTE_8_BIT },       //  8-bit per pixel
   {"16_BIT",             IF_ALPHA_MATTE_16_BIT },      // 16-bit per pixel
   {"1_BIT_IN_PADDING",   IF_ALPHA_MATTE_2_BIT_EMBEDDED },  // Embedded 2-bit per pixel (old name)
   {"2_BIT_EMBEDDED",     IF_ALPHA_MATTE_2_BIT_EMBEDDED },  // Embedded 2-bit per pixel (new name)
   {"8_BIT_INTERLEAVED",  IF_ALPHA_MATTE_8_BIT_INTERLEAVED },  // 8-bit int'lvd
   {"10_BIT_INTERLEAVED", IF_ALPHA_MATTE_10_BIT_INTERLEAVED }, // 10-bit int'lvd
   {"16_BIT_INTERLEAVED", IF_ALPHA_MATTE_16_BIT_INTERLEAVED }  // 16-bit int'lvd
};

#define ALPHA_MATTE_TYPE_COUNT (sizeof(alphaMatteTypeMap)/sizeof(SAlphaMatteTypeMap))

// ----------------------------------------------------------------------------

static MTI_UINT16 componentValuesRGB1[][MAX_COMPONENT_COUNT] =
{
   {0, 0, 0},  // Min
   {1, 1, 1},  // Max
   {0, 0, 0},  // Black
   {1, 1, 1},  // White
   {1, 1, 1},  // Fill
};

static MTI_UINT16 componentValuesRGB8[][MAX_COMPONENT_COUNT] =
{
   {  0,   0,   0},  // Min
   {255, 255, 255},  // Max
   {  0,   0,   0},  // Black
   {255, 255, 255},  // White
   {128, 128, 128},  // Fill
};

static MTI_UINT16 componentValuesRGBVideo8[][MAX_COMPONENT_COUNT] =
{
   {  1,   1,   1},  // Min
   {254, 254, 254},  // Max
   {  1,   1,   1},  // Black
   {254, 254, 254},  // White
   {128, 128, 128},  // Fill
};

static MTI_UINT16 componentValuesRGB10[][MAX_COMPONENT_COUNT] =
{
   {   0,    0,    0},  // Min
   {1023, 1023, 1023},  // Max
   {   0,    0,    0},  // Black
   {1023, 1023, 1023},  // White
   { 512,  512,  512},  // Fill
};

static MTI_UINT16 componentValuesRGBVideo10[][MAX_COMPONENT_COUNT] =
{
   {   4,    4,    4},  // Min
   {1019, 1019, 1019},  // Max
   {   4,    4,    4},  // Black
   {1019, 1019, 1019},  // White
   { 512,  512,  512},  // Fill
};

static MTI_UINT16 componentValuesRGB12[][MAX_COMPONENT_COUNT] =
{
   {   0,    0,    0},  // Min
   {4095, 4095, 4095},  // Max
   {   0,    0,    0},  // Black
   {4095, 4095, 4095},  // White
   {2048, 2048, 2048},  // Fill
};

static MTI_UINT16 componentValuesRGB16[][MAX_COMPONENT_COUNT] =
{
   {    0,     0,     0},  // Min
   {65535, 65535, 65535},  // Max
   {    0,     0,     0},  // Black
   {65535, 65535, 65535},  // White
   {32768, 32768, 32768},  // Fill
};

static MTI_UINT16 componentValuesRGBHalf[][MAX_COMPONENT_COUNT] =
{
	{     0,      0,      0},  // Min
	{0xFFFF, 0xFFFF, 0xFFFF},  // Max
   {0x8000, 0x8000, 0x8000},  // Black
   {0xBC00, 0xBC00, 0xBC00},  // White
	{0xB800, 0xB800, 0xB800},  // Fill
};

static MTI_UINT16 componentValuesYUV8[][MAX_COMPONENT_COUNT] =
{
	{ 16,  16,  16},  // Min
   {235, 240, 240},  // Max
   { 16, 128, 128},  // Black
   {235, 128, 128},  // White
   {128, 128, 128},  // Fill
};

static MTI_UINT16 componentValuesYUV10[][MAX_COMPONENT_COUNT] =
{
   { 16*4,  16*4,  16*4},  // Min
   {235*4, 240*4, 240*4},  // Max
   { 16*4, 128*4, 128*4},  // Black
   {235*4, 128*4, 128*4},  // White
   {128*4, 128*4, 128*4},  // Fill
};

// ----------------------------------------------------------------------------
// Frame Rate Type/Name Mapping Table

struct SFrameRateTypeMap
{
   const char* name;
   EFrameRate frameRate;
};

static const SFrameRateTypeMap frameRateTypeMap[] =
{
   {"14.98",   IF_FRAME_RATE_1498 },  // 14.98 fps (15/1.001)
   {"15",      IF_FRAME_RATE_1500 },  // 15.00 fps
   {"14.98_25", IF_FRAME_RATE_1498_25 }, // 14.98 fps transfer / 25 fps TC
   {"15_25",   IF_FRAME_RATE_1500_25 }, // 15 fps transfer / 25 fps TC
   {"23.98",   IF_FRAME_RATE_2398 },  // 23.976 fps (24/1.001)
   {"24",      IF_FRAME_RATE_2400 },  // 24.00 fps (Standard film rate)
   {"25",      IF_FRAME_RATE_2500 },  // 25.00 fps (PAL)
   {"29.97",   IF_FRAME_RATE_2997 },  // 29.97 fps (NTSC, 30/1.001)
   {"30",      IF_FRAME_RATE_3000 },  // 30.00 fps
   {"50",      IF_FRAME_RATE_5000 },  // 50.00 fps
   {"59.94",   IF_FRAME_RATE_5994 },  // 59.94 fps (60/1.001)
   {"60",      IF_FRAME_RATE_6000 },  // 60.00 fps
};
#define FRAME_RATE_TYPE_COUNT (sizeof(frameRateTypeMap)/sizeof(SFrameRateTypeMap))

//////////////////////////////////////////////////////////////////////
// Frame Rate / Field Rate Conversion
//////////////////////////////////////////////////////////////////////

EVideoFieldRate CImageInfo::convertFrameRateToFieldRate(EFrameRate frameRate,
                                                        bool interlaced)
{
   EVideoFieldRate fieldRate;

   switch (frameRate)
      {
      case IF_FRAME_RATE_1498 :             // 14.98 fps
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_2997
                                  : VI_VIDEO_FIELD_RATE_1498;
         break;
      case IF_FRAME_RATE_1500 :             // 15.00 fps
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_30
                                  : VI_VIDEO_FIELD_RATE_15;
         break;
      case IF_FRAME_RATE_1498_25 :          // 14.98 fps
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_2997_25
                                  : VI_VIDEO_FIELD_RATE_1498_25;
         break;
      case IF_FRAME_RATE_1500_25 :          // 15.00 fps
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_30_25
                                  : VI_VIDEO_FIELD_RATE_15_25;
         break;
      case IF_FRAME_RATE_2398 :             // 23.976 fps (24/1.001)
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_4796
                                  : VI_VIDEO_FIELD_RATE_2398;
         break;
      case IF_FRAME_RATE_2400 :             // 24.00 fps (Standard film rate)
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_48
                                  : VI_VIDEO_FIELD_RATE_24;
         break;
      case IF_FRAME_RATE_2500 :             // 25.00 fps (PAL)
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_50
                                  : VI_VIDEO_FIELD_RATE_25;
         break;
      case IF_FRAME_RATE_2997 :             // 29.97 fps (NTSC, 30/1.001) 
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_5994
                                  : VI_VIDEO_FIELD_RATE_2997;
         break;
      case IF_FRAME_RATE_3000 :             // 30.00 fps
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_60
                                  : VI_VIDEO_FIELD_RATE_30;
         break;
      case IF_FRAME_RATE_5000 :             // 50.00 fps 
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_INVALID
                                  : VI_VIDEO_FIELD_RATE_50;
         break;
      case IF_FRAME_RATE_5994 :             // 59.94 fps (60/1.001)
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_INVALID
                                  : VI_VIDEO_FIELD_RATE_5994;
         break;
      case IF_FRAME_RATE_6000 :             // 60.00 fps
         fieldRate = (interlaced) ? VI_VIDEO_FIELD_RATE_INVALID
                                  : VI_VIDEO_FIELD_RATE_60;
         break;
      case IF_FRAME_RATE_INVALID :
      case IF_FRAME_RATE_NON_STANDARD :     // Non-standard frame rate 
      default :
         // frameRateEnum is a non-standard, invalid or illegal value
         fieldRate = VI_VIDEO_FIELD_RATE_INVALID;
         break;
      }

   return fieldRate;
}

EFrameRate CImageInfo::convertFieldRateToFrameRate(EVideoFieldRate fieldRate,
                                                   bool interlaced)
{
   EFrameRate frameRate;
   switch (fieldRate)
      {
      case VI_VIDEO_FIELD_RATE_60 :
         frameRate = (interlaced) ? IF_FRAME_RATE_3000
                                  : IF_FRAME_RATE_6000;
         break;
      case VI_VIDEO_FIELD_RATE_5994 :
         frameRate = (interlaced) ? IF_FRAME_RATE_2997
                                  : IF_FRAME_RATE_5994;
         break;
      case VI_VIDEO_FIELD_RATE_50 :
         frameRate = (interlaced) ? IF_FRAME_RATE_2500
                                  : IF_FRAME_RATE_5000;
         break;
      case VI_VIDEO_FIELD_RATE_48 :
         frameRate = (interlaced) ? IF_FRAME_RATE_2400
                                  : IF_FRAME_RATE_INVALID;  // Not supported
         break;
      case VI_VIDEO_FIELD_RATE_4796 :
         frameRate = (interlaced) ? IF_FRAME_RATE_2398
                                  : IF_FRAME_RATE_INVALID;  // Not supported
         break;
      case VI_VIDEO_FIELD_RATE_30 :
         frameRate = (interlaced) ? IF_FRAME_RATE_INVALID   // Not supported
                                  : IF_FRAME_RATE_3000;
         break;
      case VI_VIDEO_FIELD_RATE_2997 :
         frameRate = (interlaced) ? IF_FRAME_RATE_INVALID   // Not supported
                                  : IF_FRAME_RATE_2997;
         break;
      case VI_VIDEO_FIELD_RATE_25 :
         frameRate = (interlaced) ? IF_FRAME_RATE_INVALID   // Not supported
                                  : IF_FRAME_RATE_2500;
         break;
      case VI_VIDEO_FIELD_RATE_24 :
         frameRate = (interlaced) ? IF_FRAME_RATE_INVALID   // Not supported
                                  : IF_FRAME_RATE_2400;
         break;
      case VI_VIDEO_FIELD_RATE_2398 :
         frameRate = (interlaced) ? IF_FRAME_RATE_INVALID   // Not supported
                                  : IF_FRAME_RATE_2398;
         break;
      case VI_VIDEO_FIELD_RATE_1498 :
         frameRate = (interlaced) ? IF_FRAME_RATE_INVALID   // Not supported
                                  : IF_FRAME_RATE_1498;
         break;
      case VI_VIDEO_FIELD_RATE_15 :
         frameRate = (interlaced) ? IF_FRAME_RATE_INVALID   // not supported
                                  : IF_FRAME_RATE_1500;
      case VI_VIDEO_FIELD_RATE_1498_25 :
         frameRate = (interlaced) ? IF_FRAME_RATE_INVALID   // Not supported
                                  : IF_FRAME_RATE_1498_25;
         break;
      case VI_VIDEO_FIELD_RATE_15_25 :
         frameRate = (interlaced) ? IF_FRAME_RATE_INVALID   // not supported
                                  : IF_FRAME_RATE_1500_25;
         break;
      case VI_VIDEO_FIELD_RATE_30_25 :
         frameRate = (interlaced) ? IF_FRAME_RATE_1500_25
                                  : IF_FRAME_RATE_INVALID;  // Not supported
         break;
      case VI_VIDEO_FIELD_RATE_2997_25 :
         frameRate = (interlaced) ? IF_FRAME_RATE_1498_25
                                  : IF_FRAME_RATE_INVALID;  // Not supported
         break;
         
      case VI_VIDEO_FIELD_RATE_INVALID :
      default:
         frameRate = IF_FRAME_RATE_INVALID;
         break;
      }

   return frameRate;
}

//////////////////////////////////////////////////////////////////////
// Static Query Functions
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    queryAlphaMatteType
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
EAlphaMatteType CImageInfo::queryAlphaMatteType(const string& typeName)
{
	for (int i = 0; i < (int)ALPHA_MATTE_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(alphaMatteTypeMap[i].name, typeName))
         return alphaMatteTypeMap[i].alphaMatteType;
      }

   return IF_ALPHA_MATTE_INVALID;
}

//------------------------------------------------------------------------
//
// Function:    queryAlphaMatteTypeName
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
string CImageInfo::queryAlphaMatteTypeName(EAlphaMatteType alphaMatteType)
{
   for (int i = 0; i < (int)ALPHA_MATTE_TYPE_COUNT; ++i)
      {
      if (alphaMatteTypeMap[i].alphaMatteType == alphaMatteType)
         return string(alphaMatteTypeMap[i].name);
      }

   return string("INVALID");
}

//------------------------------------------------------------------------
//
// Function:    queryAlphaMatteTypeCanBeHidden
//
// Description: Returns true if the type of alpha matte can be hidden
//              (i,e. has a separate image in the file for alpha matte)
//
// Arguments    EAlphaMatteType alphaMatteType   Type to query
//
// Returns:     Flag indicating whether or not the alpha matte can be
//              hidden for the queried type
//
//------------------------------------------------------------------------
bool CImageInfo::queryAlphaMatteTypeCanBeHidden(
   EAlphaMatteType alphaMatteType)
{
   bool retVal = false;

   switch (alphaMatteType)
      {
      case IF_ALPHA_MATTE_1_BIT:
      case IF_ALPHA_MATTE_8_BIT:
      case IF_ALPHA_MATTE_16_BIT:
		 retVal = true;
		 break;

	  default:
		break;
      }

   return retVal;
}

//------------------------------------------------------------------------
//
// Function:    queryBitsPerComponent
//
// Description: Returns the bits per component for a given pixel packing.
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments    EPixelPacking pixelPacking   Type of pixel packing
//
// Returns:     Number of bits per component
//              Returns 0 if pixelPacking is invalid
//
//------------------------------------------------------------------------
MTI_UINT8 CImageInfo::queryBitsPerComponent(EPixelPacking pixelPacking)
{
   MTI_UINT8 newBitsPerComponent;
   
   switch(pixelPacking)
      {
      case IF_PIXEL_PACKING_8Bits_IN_1Byte :
      case IF_PIXEL_PACKING_1_8Bits_IN_2Bytes:      // 8-bits, native byte
                                                    // ordering
         newBitsPerComponent = 8;
         break;

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes :   // Typical 10bit YUV422
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L : // 10 bit RGB in 32-bit word
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R : // 10 bit RGB in 32-bit word
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L : // 10 bits in 16-bit word
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R : // 10 bits in 16-bit word
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes:    // 10-bits, native byte
                                                   // ordering
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS :  // std DVS 10-Bit YUV422
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:  // old DVS 10-bit YUV422
         newBitsPerComponent = 10;
         break;

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:     // 12 bits padded follows  spec
      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:  // 12 bits padded reverses spec
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:   // 12 bits, left justified, Little-Endian
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:   // 12 bits, left justified, Big-Endian
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes  :   // 12 bits, native byte
                                                    // ordering
         newBitsPerComponent = 12;
         break;

      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE : // 16-bits, Big-Endian byte ordering
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE : // 16-bits, Little-Endian byte ordering
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT: // 16-bits, native byte ordering
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:       // 16-bit float, Little-Endian byte ordering
         newBitsPerComponent = 16;
         break;

      case IF_PIXEL_PACKING_INVALID :
      default:
         newBitsPerComponent = 0;
         break;
      }
   return newBitsPerComponent;
}

//------------------------------------------------------------------------
//
// Function:    queryBytesPerPixel
//
// Description: Returns a fractional number of bytes required to store
//              a pixel given the pixel component type (YUV422, RGB, etc.)
//              and the pixel packing type.  
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments    EPixelComponents pixelComponents   Pixel component type
//              EPixelPacking pixelPacking         Pixel packing type
//
// Returns:     Fractional number of bytes per pixel
//              Returns 0 if either pixelComponents or pixelPacking is invalid
//
//------------------------------------------------------------------------
double CImageInfo::queryBytesPerPixel(EPixelComponents pixelComponents,
                                      EPixelPacking pixelPacking)
{
   double packingFactor, componentFactor;

   // Determine the factor contributed by the pixel packing type
   // Roughly equivalent to fractional number of bytes per component
   switch(pixelPacking)
      {
      case IF_PIXEL_PACKING_8Bits_IN_1Byte :       // 8-bit component
         packingFactor = 1.;
         break;
      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes :    // 12-bit component
      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:  // reverses spec
         packingFactor = 3. / 2.;
         break;
      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes :   // Typical 10-bit YUV422
         packingFactor = 5. / 4.;
         break;
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L : // Typical 10-bit RGB 
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R : // in 32-bit word
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS :  // std DVS 10-Bit YUV422
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:  // old DVS 10-bit YUV422
         packingFactor = 4. / 3.;
         break;
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L : // 10 data bits in 16-bit word
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R :
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE : // 16-bits, Big-Endian byte ordering
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE : // 16-bits, Little-Endian byte ordering

      case IF_PIXEL_PACKING_1_8Bits_IN_2Bytes :    // 8-bits, native byte ordering
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes :   // 10-bits, native byte ordering
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE : // 12-bits, left justified, Little-Endian
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE : // 12-bits, left justified, Big-Endian
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes :   // 12-bits, native byte ordering
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT : // 16-bits, native byte ordering
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:      // 16-bit float, Little-Endian byte ordering
         packingFactor = 2.;
         break;
      case IF_PIXEL_PACKING_INVALID :
      default:
         packingFactor = 0.;
         break;
      }

   // Determine the factor contributed by the pixel component type.
   // Roughly equivalent to number of components per pixel
   switch (pixelComponents)
      {
//      case IF_PIXEL_COMPONENTS_LUMINANCE :   // Luminance only, monochrome
//         componentFactor = 1.;
//         break;
      case IF_PIXEL_COMPONENTS_RGB :         // RGB
         componentFactor = 3.;
         break;
      case IF_PIXEL_COMPONENTS_RGBA :        // RGB + alpha matte channel
         componentFactor = 4.;
         break;
      case IF_PIXEL_COMPONENTS_YUV422 :      // CbYCrY
         componentFactor = 2.;
         break;
      case IF_PIXEL_COMPONENTS_YUV4224 :     // YUV + alpha, CbYACrYA
         componentFactor = 3.;
         break;
      case IF_PIXEL_COMPONENTS_YUV444 :      // CbYCr
         componentFactor = 3.;
         break;
      case IF_PIXEL_COMPONENTS_BGR :         // BGR, e.g. Targa
         componentFactor = 3.;
         break;
      case IF_PIXEL_COMPONENTS_BGRA :        // BGRA
         componentFactor = 4.;
         break;
      case IF_PIXEL_COMPONENTS_Y :           // B&W, 1 chan source
         componentFactor = 1.;
         break;
      case IF_PIXEL_COMPONENTS_YYY :         // B&W, 3 channels with same values
         componentFactor = 3.;
         break;
      case IF_PIXEL_COMPONENTS_INVALID :
      default :
         componentFactor = 0.;
         break;
      }

   return packingFactor * componentFactor;
}

//------------------------------------------------------------------------
//
// Function:    queryBytesForPixels
//
// Description: Determines the number of bytes of storage required for 
//              a number of pixels given the pixel component type (YUV422,
//              RGB, etc.) and the pixel packing type.
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments    EPixelComponents pixelComponents   Pixel component type
//              EPixelPacking pixelPacking         Pixel packing type
//              MTI_UINT32 pixelCount              Number of pixels to store
//
// Returns:     Number of whole bytes required to store pixels
//              Returns 0 if either pixelComponents or pixelPacking is invalid
//
//------------------------------------------------------------------------
MTI_UINT32 CImageInfo::queryBytesForPixels(EPixelComponents pixelComponents,
                                           EPixelPacking pixelPacking,
                                           MTI_UINT32 pixelCount)
{
   double dBytesRequired = queryBytesPerPixel(pixelComponents, pixelPacking)
                           * pixelCount;

   return (MTI_UINT32)(dBytesRequired + .5);
}

//------------------------------------------------------------------------
//
// Function:    queryColorSpace
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
EColorSpace CImageInfo::queryColorSpace(const string& colorSpaceName)
{
   for (int i = 0; i < (int)COLOR_SPACE_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(colorSpaceTypeMap[i].name, colorSpaceName))
         return colorSpaceTypeMap[i].colorSpace;
      }

   return IF_COLOR_SPACE_INVALID;
}

//------------------------------------------------------------------------
//
// Function:    queryColorSpaceName
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
string CImageInfo::queryColorSpaceName(EColorSpace colorSpace)
{
   for (int i = 0; i < (int)COLOR_SPACE_TYPE_COUNT; ++i)
      {
      if (colorSpaceTypeMap[i].colorSpace == colorSpace)
         return string(colorSpaceTypeMap[i].name);
      }

   return string("INVALID");
}

//------------------------------------------------------------------------
//
// Function:    queryFilmFramesFrameRate
//
// Description: Determines the frame rate after the pulldown is removed
//
// Arguments
//
// Returns:      None.  Modifies caller's arguments
//
//------------------------------------------------------------------------
void CImageInfo::queryFilmFramesFrameRate(EPulldown pulldown,
                                          EFrameRate *frameRateEnum,
                                          double *frameRate)
{
   EFrameRate filmFrameRateEnum;
   double filmFrameRate = 0;

   if (pulldown == IF_PULLDOWN_FIELD_3_2
       || pulldown == IF_PULLDOWN_SWAPPED_FIELD_3_2)
      {
      if (*frameRateEnum == IF_FRAME_RATE_2997)
         {
         filmFrameRateEnum = IF_FRAME_RATE_2398;
         filmFrameRate = queryStandardFrameRate(filmFrameRateEnum);
         }
      else if (*frameRateEnum == IF_FRAME_RATE_3000)
         {
         filmFrameRateEnum = IF_FRAME_RATE_2400;
         filmFrameRate = queryStandardFrameRate(filmFrameRateEnum);
         }
      else if (*frameRateEnum == IF_FRAME_RATE_NON_STANDARD)
         {
         // WTF?? 0 FPS should never be allowed, so change to INVALID!
         if (*frameRate < 0.001)
            {
            filmFrameRateEnum = IF_FRAME_RATE_INVALID;
            }
         else
            {
            filmFrameRateEnum = IF_FRAME_RATE_NON_STANDARD;
            // Calculate new frame rate as 80% of video frames per second
            // since removing 3:2 pulldown converts five video frames into four
            // film frames
            filmFrameRate = *frameRate * .8;
            }
         }
      else
         {
         // Cannot handle this frame rate, return without changing input
         return;
         }
      }
   else if (pulldown == IF_PULLDOWN_FRAME_3_2)
      {
      if (*frameRateEnum == IF_FRAME_RATE_5994)
         {
         filmFrameRateEnum = IF_FRAME_RATE_2398;
         filmFrameRate = queryStandardFrameRate(filmFrameRateEnum);
         }
      else if (*frameRateEnum == IF_FRAME_RATE_6000)
         {
         filmFrameRateEnum = IF_FRAME_RATE_2400;
         filmFrameRate = queryStandardFrameRate(filmFrameRateEnum);
         }
      else if (*frameRateEnum == IF_FRAME_RATE_NON_STANDARD)
         {
         // WTF?? 0 FPS should never be allowed, so change to INVALID!
         if (*frameRate < 0.001)
            {
            filmFrameRateEnum = IF_FRAME_RATE_INVALID;
            }
         else
            {
            filmFrameRateEnum = IF_FRAME_RATE_NON_STANDARD;
            // Calculate new frame rate as 40% of video frames per second
            // since removing 3:2 pulldown converts five video frames into two
            // film frames
           filmFrameRate = *frameRate * .4;
           }
         }
      else
         {
         // Cannot handle this frame rate, return without changing input
         return;
         }
      }
   else
      {
      // Nothing to do, return without changing input
      return;
      }

   // Set caller's values with newly calculated frame rate
   *frameRateEnum = filmFrameRateEnum;
   *frameRate = filmFrameRate;
}

//------------------------------------------------------------------------
//
// Function:    queryFrameRate
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
EFrameRate CImageInfo::queryFrameRate(const string& frameRateName,
                                      double &newFrameRate)
{
   // Search table of standard frame rates for match with frameRateName
   for (int i = 0; i < (int)FRAME_RATE_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(frameRateTypeMap[i].name, frameRateName))
         {
         EFrameRate frameRateEnum = frameRateTypeMap[i].frameRate;
         newFrameRate = queryStandardFrameRate(frameRateEnum);
         return frameRateEnum;
         }
      }

   // Did not find a match among standard frame rates, so attempt to
   // convert string to a double value
   MTIistringstream strStream;
   strStream.str(frameRateName);
   strStream >> newFrameRate;
   if (((bool) strStream) && (newFrameRate > 0.001))
      {
      return IF_FRAME_RATE_NON_STANDARD;
      }
   else
      {
      // Could not convert string or it turned up 0, which is not valid
      return IF_FRAME_RATE_INVALID;
      }
}

//------------------------------------------------------------------------
//
// Function:    queryFrameRateName
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
string CImageInfo::queryFrameRateName(EFrameRate frameRate)
{
   for (int i = 0; i < (int)FRAME_RATE_TYPE_COUNT; ++i)
      {
      if (frameRateTypeMap[i].frameRate == frameRate)
         return string(frameRateTypeMap[i].name);
      }

   return string("INVALID");
}

//------------------------------------------------------------------------
//
// Function:
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
EImageFormatType CImageInfo::
queryImageFormatType(const string& imageFormatTypeName)
{
   for (int i = 0; i < (int)IMAGE_FORMAT_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(imageFormatTypeMap[i].name, imageFormatTypeName))
         return imageFormatTypeMap[i].type;
      }

   return IF_TYPE_INVALID;
}

//------------------------------------------------------------------------
//
// Function:    queryImageFormatTypeDescription
//
// Description: Gets description strings for a image format type.
//              The caller provides two strings which are filled in
//              with descriptions of the image format.  Description 1
//              contains a basic name of the format, while Description 2
//              contains some details.
//
//              This is a static member function which means that it can be 
//              called without a CImageType object instance.
//
// Arguments    EImageFormatType imageFormat    Image type
//              string &descr1                  Caller's string for descr 1
//              string &descr2                  Caller's string for descr 2
//
// Returns:     0 if succeeded,
//              non-zero if an invalid image format type
//
//------------------------------------------------------------------------
int CImageInfo::queryImageFormatTypeDescription(EImageFormatType imageFormat,
                                                string &descr1,
                                                string &descr2)
{
   if (isImageFormatTypeValid(imageFormat))
      {
      descr1 = "Invalid";
      descr2 = "";
      return FORMAT_ERROR_INVALID_IMAGE_FORMAT_TYPE;
      }

   for (int i = 0; i < (int)IMAGE_FORMAT_TYPE_DESCR_COUNT; ++i)
      {
      if (imageFormatTypeDescriptions[i].type == imageFormat)
         {
         // Found the description for the caller's image format type
         descr1 = imageFormatTypeDescriptions[i].descr1;
         descr2 = imageFormatTypeDescriptions[i].descr2;
         return 0;
         }
      }

   // Image Format Type was not found in the table
   // Note: the table should be updated.
   descr1 = "Unknown";
   descr2 = "";

   return 0;
}

int CImageInfo::queryImageFormatTypeDescriptionCount()
{
   return IMAGE_FORMAT_TYPE_DESCR_COUNT;
}

EImageFormatType CImageInfo::queryImageFormatTypeDescriptionTable(int index)
{
   if (index < 0 || index >= (int)IMAGE_FORMAT_TYPE_DESCR_COUNT)
      return IF_TYPE_INVALID;

   return imageFormatTypeDescriptions[index].type;
}

//------------------------------------------------------------------------
//
// Function:    queryImageFormatTypeName
//
// Description: Gets name string of image format type.  This is a static
//              member function which means that it can be called without
//              a CImageType object instance.
//
// Arguments    EImageFormatType imageFormat    Image type
//
// Returns:     string instance containing image format type name
//
//------------------------------------------------------------------------
string CImageInfo::queryImageFormatTypeName(EImageFormatType imageFormat)
{
   for (int i = 0; i < (int)IMAGE_FORMAT_TYPE_COUNT; ++i)
      {
      if (imageFormatTypeMap[i].type == imageFormat)
         return string(imageFormatTypeMap[i].name);
      }

   return string("INVALID");
}

//------------------------------------------------------------------------
//
// Function:    queryImageFormatTypeSimpleName
//
// Description: Gets a"simple" name string for the  image format type.
//              This is a static member function which means that it can be 
//              called without a CImageType object instance.
//
// Arguments    EImageFormatType imageFormat    Image type
//
// Returns:     string instance containing simple image format type name
//
//------------------------------------------------------------------------
string CImageInfo::queryImageFormatTypeSimpleName(EImageFormatType imageFormat)
{
   string simpleName;

   switch (imageFormat)
      {
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS : // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :   // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
         simpleName = "525";
         break;

      case IF_TYPE_625_4795 :     // Special proxy format
      case IF_TYPE_625_4795_DVS : // Special proxy format
      case IF_TYPE_625_48 :       // Special proxy format
      case IF_TYPE_625_48_DVS :   // Special proxy format
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :   // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
         simpleName = "625";
         break;

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_50_DVS :  // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_5994_DVS :// 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_60_DVS :  // 720 progressive-scan, 60 frames/second
         simpleName = "720P";
         break;

      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
         simpleName = "1080I";
         break;

      case IF_TYPE_1080P_2398 :    // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_2398_DVS :// 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 :  // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS:// 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :      // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_24_DVS :  // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :    // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :// 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :      // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25_DVS :  // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :    // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :// 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit
         simpleName = "1080P";
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
         simpleName = "DPX";
         break;

      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
         simpleName = "CIN";
         break;

      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
         simpleName = "TIFF";
         break;

      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
         simpleName = "SGI";
         break;

      case IF_TYPE_FILE_TGA :    // TGA graphics file format
         simpleName = "TGA";
         break;

      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
         simpleName = "TGA2";
         break;

      case IF_TYPE_FILE_BMP :    // BMP graphics file format
         simpleName = "BMP";
         break;

      case IF_TYPE_FILE_EXR :    // EXR graphics file format
         simpleName = "EXR";
         break;

      case IF_TYPE_INTERNAL :     // Internal format, typically 16-bit
                                  // progressive, YUV444 or RGB
         simpleName = "INTERNAL";
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         simpleName = "2048P";
         break;

      default :
         // Unexpected image format type
         simpleName = "INVALID";
         break;
      }

   return simpleName;
}

//------------------------------------------------------------------------
//
// Function:    queryIsCadenceRepairSupported
//
// Description: Returns if format is 525 or 1080I.
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     true if 525, false otherwise
//
//------------------------------------------------------------------------
bool CImageInfo::queryIsCadenceRepairSupported(EImageFormatType imageFormatType)
{
   bool isSupported;

   switch (imageFormatType)
      {
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS : // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :   // Digital NTSC, "Standard Definition"
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
         isSupported = true;
         break;

      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
         isSupported = false;
         break;

      case IF_TYPE_625_4795 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_625_4795_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_BMP :     // BMP file format
      case IF_TYPE_FILE_EXR :     // EXR file format

      case IF_TYPE_INTERNAL :     // Internal Format 16-bit progressive,
                                  // 3-component YUV444 or RGB

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
      
      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

         isSupported = false;
         break;

      default :
         // Unexpected image format type
         isSupported = false;
         break;
      }

   return isSupported;
}

//------------------------------------------------------------------------
//
// Function:    queryIsFormat525
//
// Description: Returns if format is 525.
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     true if 525, false otherwise
//
//------------------------------------------------------------------------
bool CImageInfo::queryIsFormat525(EImageFormatType imageFormatType)
{
   bool is525;

   switch (imageFormatType)
      {
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS : // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :   // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
         // Is a 525 format
         is525 = true;
         break;

      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
         // Is a 525 format
         is525 = true;
         break;

      case IF_TYPE_625_4795 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_625_4795_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_BMP :     // BMP file format
      case IF_TYPE_FILE_EXR :     // EXR file format

      case IF_TYPE_INTERNAL :     // Internal Format 16-bit progressive,
                                  // 3-component YUV444 or RGB

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
      
      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

         // Not a 525 format
         is525 = false;
         break;

      default :
         // Unexpected image format type
         is525 = false;
         break;
      }

   return is525;
}

//------------------------------------------------------------------------
//
// Function:    queryIsFormat720P
//
// Description: Returns if format is 720P.  This information is used with
//              CTimecode instances
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     true if 720P, false otherwise
//
//------------------------------------------------------------------------
bool CImageInfo::queryIsFormat720P(EImageFormatType imageFormatType)
{
   bool is720P;

   switch (imageFormatType)
      {
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_625_4795 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_625_4795_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_BMP :     // BMP file format      
      case IF_TYPE_FILE_EXR :     // EXR file format

      case IF_TYPE_INTERNAL :     // Internal Format 16-bit progressive,
                                  // 3-component YUV444 or RGB

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
      
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive

         // Not a 720P format
         is720P = false;
         break;

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second
         // Is a 720P format
         is720P = true;
         break;

      default :
         // Unexpected image format type
         is720P = false;
         break;
      }

   return is720P;
}

//------------------------------------------------------------------------
//
// Function:    queryIsFormatFile
//
// Description: Returns true if the image format is image file based.
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     true if file-based format, false if video format, internal
//              format or invalid
//
//------------------------------------------------------------------------
bool CImageInfo::queryIsFormatFile(EImageFormatType imageFormatType)
{
   bool isFile;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)

      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive

      case IF_TYPE_625_4795 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_4795_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive

      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second

      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :

         // Not a file format
         isFile = false;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_TGA :     // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :     // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR graphics file format
         // Is a file format
         isFile = true;
         break;

      case IF_TYPE_INTERNAL :     // Internal Format 16-bit progressive,
                                  // could be derived from either video- or
                                  // file-based source format
      default :
         // Unexpected image format type
         isFile = false;
         break;
      }

   return isFile;
}

//------------------------------------------------------------------------
//
// Function:    queryIsFormatVideo
//
// Description: Returns true if the image format is video-based.
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     true if video-based format, false if file format, internal
//              format or invalid
//
//------------------------------------------------------------------------
bool CImageInfo::queryIsFormatVideo(EImageFormatType imageFormatType)
{
   bool isVideo;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)

      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive

      case IF_TYPE_625_4795 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_4795_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive

      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second

      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :

         // Is a Video format
         isVideo = true;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_TGA :     // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :     // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format
         // Is not a Video format
         isVideo = false;
         break;

      case IF_TYPE_INTERNAL :     // Internal Format 16-bit progressive,
                                  // could be derived from either video- or
                                  // file-based source format
      default :
         // Unexpected image format type
         isVideo = false;
         break;
      }

   return isVideo;
}

//------------------------------------------------------------------------
//
// Function:    queryIsFormatConventionalVTR
//
// Description: Returns true if the image format is video-based and
//              supported by conventional VTRs.
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     true if VTR format, false if file format, internal
//              format or invalid
//
//------------------------------------------------------------------------
bool CImageInfo::queryIsFormatConventionalVTR(EImageFormatType imageFormatType)
{
   bool isVideo;

   switch (imageFormatType)
      {
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive

      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive

      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second

      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second

         // Is a Video format
         isVideo = true;
         break;

      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)

      case IF_TYPE_625_4795 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_4795_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_TGA :     // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :     // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :

         // Is not a Video format
         isVideo = false;
         break;

      case IF_TYPE_INTERNAL :     // Internal Format 16-bit progressive,
                                  // could be derived from either video- or
                                  // file-based source format

      default :
         // Unexpected image format type
         isVideo = false;
         break;
      }

   return isVideo;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalAlphaMatteType
//
// Description: Returns nominal alpha matte type for image format type.
//              When this function was written, the only format type
//              that was relevant was the hacked IF_TYPE_FILE_DPXA1.
//
//              Now that we've eliminated IF_TYPE_FILE_DPXA1, the
//              nominal type is IF_ALPHA_MATTE_NONE for all types.
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     Alpha matte type enum
//
//------------------------------------------------------------------------
EAlphaMatteType CImageInfo::queryNominalAlphaMatteType(EImageFormatType imageFormatType)
{
/*
   EAlphaMatteType nominalAlphaMatteType;

   switch (imageFormatType)
      {
      default :
         nominalAlphaMatteType = IF_ALPHA_MATTE_NONE;
         break;
      }
*/

   return IF_ALPHA_MATTE_NONE;

} // queryNominalAlphaMatteType

//------------------------------------------------------------------------
//
// Function:    queryNominalColorSpace
//
// Description: Returns nominal Color Space type for image format type
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     Color Space type
//
//------------------------------------------------------------------------
EColorSpace CImageInfo::
queryNominalColorSpace(EImageFormatType imageFormatType)
{
   EColorSpace nominalColorSpace;

   switch (imageFormatType)
      {
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive

      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)

         nominalColorSpace = IF_COLOR_SPACE_CCIR_601_M;
         break;

      case IF_TYPE_625_50 :           // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
         nominalColorSpace = IF_COLOR_SPACE_CCIR_601_BG;
         break;

      case IF_TYPE_625_4795 :     // Special proxy format
      case IF_TYPE_625_48 :       // Special proxy format
      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_625_4795_DVS :     // Special proxy format
      case IF_TYPE_625_48_DVS :       // Special proxy format
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

         nominalColorSpace = IF_COLOR_SPACE_CCIR_709;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
         nominalColorSpace = CImageInfo::GetDpxIsDefaultLog()
                                  ? IF_COLOR_SPACE_LOG
                                  : IF_COLOR_SPACE_LINEAR;
         break;

      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
         nominalColorSpace = IF_COLOR_SPACE_LOG;
         break;

      case IF_TYPE_FILE_EXR :     // EXR file format
         nominalColorSpace = IF_COLOR_SPACE_EXR;
         break;

      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :     // BMP graphics file format      
         nominalColorSpace = IF_COLOR_SPACE_LINEAR;
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         nominalColorSpace = IF_COLOR_SPACE_LOG;
         break;

      case IF_TYPE_INTERNAL :     // Internal format, no nominal color space
      default :
         // Unexpected image format type
         nominalColorSpace = IF_COLOR_SPACE_INVALID;
         break;
      }

   return nominalColorSpace;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalComponentCount
//
// Description: Returns the nominal number of separate components that make 
//              up a pixel for a given EPixelComponents
//
//              NOTE: YUV422, has three separate components even though
//              it only has two components per pixel.
//
//              See function queryNominalComponentsPerPixel for the number
//              of components in a single pixel
//
// Arguments    EPixelComponents pixelComponents
//
// Returns:     The number of separate components
//              Returns -1 for an invalid argument
//
//------------------------------------------------------------------------
int CImageInfo::queryNominalComponentCount(EPixelComponents pixelComponents)
{
   int componentCount;

   switch (pixelComponents)
      {
//      case IF_PIXEL_COMPONENTS_LUMINANCE :   // Luminance only, monochrome
//         componentCount = 1;
//         break;
      case IF_PIXEL_COMPONENTS_RGB :         // RGB
      case IF_PIXEL_COMPONENTS_BGR :         // BGR, e.g. Targa
         componentCount = 3;
         break;
      case IF_PIXEL_COMPONENTS_YUV422 :      // CbYCrY
         componentCount = 3;
         break;
      case IF_PIXEL_COMPONENTS_RGBA :        // RGB + alpha matte channel
      case IF_PIXEL_COMPONENTS_BGRA :        // BGR + alpha matte channel
         componentCount = 4;
         break;
      case IF_PIXEL_COMPONENTS_YUV444 :      // CbYCr
         componentCount = 3;
         break;
      case IF_PIXEL_COMPONENTS_YUV4224 :     // YUV + alpha, CbYACrYA
         componentCount = 4;
         break;
      case IF_PIXEL_COMPONENTS_Y :           // Y
         componentCount = 3;                 // BECAUSE IT GETS EXPANDED TO YYY
         break;
      case IF_PIXEL_COMPONENTS_YYY :         // YYY
         componentCount = 3;
         break;

      case IF_PIXEL_COMPONENTS_INVALID :
      default :
         // Bad
         componentCount = -1;
         break;
      }

   return componentCount; // Okay
}

//------------------------------------------------------------------------
//
// Function:    queryNominalComponentsPerPixel
//
// Description: Returns the nominal number of components that make up a pixel
//              for a given EPixelComponents
//
//              NOTE: YUV422, although is has three separate components, 
//              only has two components per pixel.
//
//              See function queryNominalComponentCount for the number of
//              separate components
//
// Arguments    EPixelComponents pixelComponents
//
// Returns:     The number of components that make up a pixel
//              Returns -1 for an invalid argument
//
//------------------------------------------------------------------------
int CImageInfo::queryNominalComponentsPerPixel(EPixelComponents pixelComponents)
{
   int componentCount;

   switch (pixelComponents)
      {
//      case IF_PIXEL_COMPONENTS_LUMINANCE :   // Luminance only, monochrome
//         componentCount = 1;
//         break;
      case IF_PIXEL_COMPONENTS_RGB :         // RGB
      case IF_PIXEL_COMPONENTS_BGR :         // BGR, e.g. Targa
         componentCount = 3;
         break;
      case IF_PIXEL_COMPONENTS_YUV444 :      // CbYCr
         componentCount = 3;
         break;
      case IF_PIXEL_COMPONENTS_YUV422 :      // CbYCrY
         componentCount = 2;
         break;
      case IF_PIXEL_COMPONENTS_RGBA :        // RGB + alpha matte channel
      case IF_PIXEL_COMPONENTS_BGRA :        // BGR + alpha matte channel
         componentCount = 4;
         break;
      case IF_PIXEL_COMPONENTS_YUV4224 :     // YUV + alpha, CbYACrYA
         componentCount = 3;
         break;
      case IF_PIXEL_COMPONENTS_Y :           // Y
         componentCount = 1;                 // in RAW format!
         break;
      case IF_PIXEL_COMPONENTS_YYY :         // YYY
         componentCount = 3;
         break;

      case IF_PIXEL_COMPONENTS_INVALID :
      default :
         // Bad
         componentCount = -1;
         break;
      }

   return componentCount; // Okay
}

//------------------------------------------------------------------------
//
// Function:    queryNominalFrameAspectRatio
//
// Description: Returns nominal frame aspect ratio for the given
//              image format type.  Nominal frame aspect ratio is 4:3 for
//              standard definition video and 16:9 for high-definition video.
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     Number of whole bytes required to store pixels
//              Returns 0 if E
//
//------------------------------------------------------------------------
double CImageInfo::
queryNominalFrameAspectRatio(EImageFormatType imageFormatType)
{
   double frameAspectRatio;

   switch (imageFormatType)
      {
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_625_4795 :     // Proxy format
      case IF_TYPE_625_48 :       // Proxy format
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive

      case IF_TYPE_525_5994_DVS : // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :   // Digital NTSC, "Standard Definition"
      case IF_TYPE_625_4795_DVS : // Proxy format
      case IF_TYPE_625_48_DVS :   // Proxy format
      case IF_TYPE_625_50_DVS :   // Digital PAL, "Standard Def"
      
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)

         // Standard definition (SD) formats
         frameAspectRatio = IF_FRAME_ASPECT_RATIO_SD;
         break;

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

         // High-definition (HD) formats
         frameAspectRatio = IF_FRAME_ASPECT_RATIO_HD;
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
         frameAspectRatio = (4./3.);
         break;

      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         frameAspectRatio = 2048./1556.;
         break;

      case IF_TYPE_INTERNAL :         // Internal Format, no nominal value
      default :
         // Unexpected image format type
         frameAspectRatio = 0.;
         break;
      }

   return frameAspectRatio;
}
//------------------------------------------------------------------------
//
// Function:    queryNominalFrameRate
//
// Description:
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments    
//
// Returns:     
//
//------------------------------------------------------------------------
double CImageInfo::
queryNominalFrameRate(EImageFormatType imageFormatType)
{
   EFrameRate frameRateEnum = queryNominalFrameRateEnum(imageFormatType);

   return( queryStandardFrameRate(frameRateEnum));
}

//------------------------------------------------------------------------
//
// Function:    queryNominalFieldRateEnum
//
// Description:
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments
//
// Returns:     
//
//------------------------------------------------------------------------
EVideoFieldRate CImageInfo::
queryNominalFieldRateEnum(EImageFormatType imageFormatType)
{
   EVideoFieldRate fieldRateEnum =
        convertFrameRateToFieldRate(queryNominalFrameRateEnum(imageFormatType),
                                    queryNominalInterlaced(imageFormatType));

   return fieldRateEnum;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalFrameRateEnum
//
// Description:
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments
//
// Returns:     
//
//------------------------------------------------------------------------
EFrameRate CImageInfo::
queryNominalFrameRateEnum(EImageFormatType imageFormatType)
{
   EFrameRate frameRateEnum;

   switch (imageFormatType)
      {
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS : // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
         frameRateEnum = IF_FRAME_RATE_2997;
         break;

      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :   // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
         frameRateEnum = IF_FRAME_RATE_3000;
         break;

      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :   // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
         frameRateEnum = IF_FRAME_RATE_2500;
         break;

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_50_DVS :  // 720 progressive-scan, 50 frames/second
         frameRateEnum = IF_FRAME_RATE_5000;
         break;

      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_5994_DVS :// 720 progressive-scan, 59.94 frames/second
         frameRateEnum = IF_FRAME_RATE_5994;
         break;

      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_60_DVS :  // 720 progressive-scan, 60 frames/second
         frameRateEnum = IF_FRAME_RATE_6000;
         break;

      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_50_DVS : // 1080 interlaced, 50 fields/second
         frameRateEnum = IF_FRAME_RATE_2500;
         break;

      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_5994_DVS : // 1080 interlaced, 59.94 fields/second
         frameRateEnum = IF_FRAME_RATE_2997;
         break;

      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080I_60_DVS : // 1080 interlaced, 60 fields/second
         frameRateEnum = IF_FRAME_RATE_3000;
         break;
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625_4795 :     // Digital PAL, "Standard Def"
      case IF_TYPE_625_4795_DVS : // Digital PAL, "Standard Def"
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PSFI_2398 :    // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :// 1080PsF/23.98 DVS 10 bit

        frameRateEnum = IF_FRAME_RATE_2398;
        break;

      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :   // Digital PAL, "Standard Def"
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PSFI_24 :      // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24_DVS :  // 1080PsF/24    DVS 10 bit
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :

         frameRateEnum = IF_FRAME_RATE_2400;
         break;

      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit
      case IF_TYPE_1080PSFI_25 :      // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25_DVS :  // 1080PsF/25    DVS 10 bit

         frameRateEnum = IF_FRAME_RATE_2500;
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1556PSF_1498_DVS :
         frameRateEnum = IF_FRAME_RATE_1498;
         break;

      case IF_TYPE_2048_1556PSF_15_DVS :
         frameRateEnum = IF_FRAME_RATE_1500;
         break;

      case IF_TYPE_2048_1556PSF_1498_25_DVS :
         frameRateEnum = IF_FRAME_RATE_1498_25;
         break;

      case IF_TYPE_2048_1556PSF_15_25_DVS :
         frameRateEnum = IF_FRAME_RATE_1500_25;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :     // BMP graphics file format      
      case IF_TYPE_FILE_EXR :     // EXR file format
         // Assume image file formats are 24 fps film
         frameRateEnum = IF_FRAME_RATE_2400;
         break;

      case IF_TYPE_INTERNAL :         // Internal Format, no nominal value
      default :
         // Unexpected image format type
         frameRateEnum = IF_FRAME_RATE_INVALID;
         break;
      }

   return frameRateEnum;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalHandleCount
//
// Description: Returns nominal number of "handle" frames for image format type
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     Number of handle frames
//
//------------------------------------------------------------------------
int CImageInfo::queryNominalHandleCount(EImageFormatType imageFormatType)
{
   int handleCount;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625_4795 :     // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_625_4795_DVS :     // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

         // Video formats
         handleCount = 16;
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         handleCount = 16;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :     // BMP graphics file format      
      case IF_TYPE_FILE_EXR :     // EXR file format
         // Image file formats
         handleCount = 0;
         break;

     case IF_TYPE_INTERNAL :         // Internal Format, no nominal value
     default :
         // Unexpected image format type
         handleCount = 0;
         break;
      }

   return handleCount;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalInterlaced
//
// Description: Returns nominal interlaced flag for image format type
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     true if Interlaced, false if Progressing
//
//------------------------------------------------------------------------
bool CImageInfo::queryNominalInterlaced(EImageFormatType imageFormatType)
{
   bool nominalInterlaced;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_625_4795 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second

      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_625_4795_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second

      // Note: the 1080PI formats come in as progressive video, but are stored
      //       as two interlaced fields
      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      // Note: the 1080PSFI formats come in as progressive segmented field
      //       video, but are stored as two interlaced fields
      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

         // Interlaced formats
         nominalInterlaced = true;
         break;

      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second

      // Note: the 1080P formats come in as progressive video and are stored
      //       in a single progressive frame
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second

      // Note: the 1080PSF formats come in as segmented-field video, but are
      //       stored as a single progressive frame
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
         // Progressive-scan formats
         nominalInterlaced = false;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format      
      case IF_TYPE_FILE_EXR :     // EXR file format
         // Assume image file formats are progressive
         nominalInterlaced = false;
         break;

      case IF_TYPE_INTERNAL :         // Internal Format, typically 16-bit
         nominalInterlaced = true;    // progressive, 3 component YUV444 or RGB
         break;                            

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         nominalInterlaced = false;
         break;

      // SD Progressive formats
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
         nominalInterlaced = false;
         break;

      default :
         // Unexpected image format type
         nominalInterlaced = false;
         break;
      }

   return nominalInterlaced;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalLineAlignment
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
MTI_UINT32
CImageInfo::queryNominalLineAlignment(EImageFormatType imageFormatType)
{
   MTI_UINT32 nominalLineAlignment;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625_4795 :     // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
         // No line alignment required
         nominalLineAlignment = 1;
         break;

      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994_DVS :
      case IF_TYPE_525_60_DVS :
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625_4795_DVS :
      case IF_TYPE_625_48_DVS :
      case IF_TYPE_625_50_DVS :
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_1080I_50_DVS :
      case IF_TYPE_1080I_5994_DVS :
      case IF_TYPE_1080I_60_DVS :
      case IF_TYPE_1080P_2398_DVS :
      case IF_TYPE_1080PSF_2398_DVS :
      case IF_TYPE_1080P_24_DVS :
      case IF_TYPE_1080PSF_24_DVS :
      case IF_TYPE_1080P_25_DVS :
      case IF_TYPE_1080PSF_25_DVS :
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit
         // DVS video formats must have lines aligned on 128 byte boundaries
         nominalLineAlignment = 128;
         break;

      case IF_TYPE_720P_50_DVS :
      case IF_TYPE_720P_5994_DVS :
      case IF_TYPE_720P_60_DVS :
         // DVS 720P are now aligned on 32 byte boundaries??!?
         nominalLineAlignment = 32;
         break;


      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         // DVS video formats must have lines aligned on 128 byte boundaries
         nominalLineAlignment = 128;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format
         // No line alignment required
         nominalLineAlignment = 1;
         break;

      case IF_TYPE_INTERNAL :         // Internal Format
         // No line alignment required
         nominalLineAlignment = 1;
         break;

      default :
         // Unexpected image format type
         nominalLineAlignment = 1;
         break;
      }

   return nominalLineAlignment;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalLinesPerFrame
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CImageInfo::queryNominalLinesPerFrame(EImageFormatType imageFormatType)
{
   int nominalLinesPerFrame;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
         nominalLinesPerFrame = IF_ACTIVE_ROWS_PER_FRAME_525;
         break;

      case IF_TYPE_625_4795 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_4795_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
         nominalLinesPerFrame = IF_ACTIVE_ROWS_PER_FRAME_625;
         break;

      case IF_TYPE_720P_50 :  // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :  // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_50_DVS :  // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :  // 720 progressive-scan, 60 frames/second
         nominalLinesPerFrame = IF_ACTIVE_ROWS_PER_FRAME_720P;
         break;

      case IF_TYPE_1080I_50 : // 1080 interlaced, 50 frames/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 : // 1080 interlaced, 60 frames/second
      case IF_TYPE_1080I_50_DVS : // 1080 interlaced, 50 frames/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS : // 1080 interlaced, 60 frames/second
         nominalLinesPerFrame = IF_ACTIVE_ROWS_PER_FRAME_1080I;
         break;

      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_2398_DVS :// 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS : // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :// 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25_DVS : // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :// 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit
         nominalLinesPerFrame = IF_ACTIVE_ROWS_PER_FRAME_1080P;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format
         // There is no default value for film and graphics file formats
         nominalLinesPerFrame = 0;
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
         nominalLinesPerFrame = 1536;
         break;

      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         nominalLinesPerFrame = 1556;
         break;

      case IF_TYPE_INTERNAL :         // Internal Format, no nominal value
      default :
         // Unexpected image format type
         nominalLinesPerFrame = -1;
      }

   return nominalLinesPerFrame;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalPixelAspectRatio
//
// Description: Returns the nominal pixel aspect ratio for a give
//              image format type.  Nominal pixel aspect ratio is the
//              pixel aspect ratio that achieves the nominal frame aspect
//              ratio (4:3 for standard def, 16:9 for high def) for
//              the image format's standard horizontal and vertical resolution.
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     Floating point width / height aspect ratio of pixel
//              Returns 0 if imageFormatType is invalid
//
//------------------------------------------------------------------------
double CImageInfo::
queryNominalPixelAspectRatio(EImageFormatType imageFormatType)
{
   double pixelAspectRatio;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS : // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :   // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
          pixelAspectRatio = IF_PIXEL_ASPECT_RATIO_525;
         break;

      case IF_TYPE_625_4795 :     // Proxy format
      case IF_TYPE_625_48 :       // Proxy format
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_4795_DVS : // Proxy format
      case IF_TYPE_625_48_DVS :   // Proxy format
      case IF_TYPE_625_50_DVS :   // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
         pixelAspectRatio = IF_PIXEL_ASPECT_RATIO_625;
         break;

      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit
         pixelAspectRatio = IF_PIXEL_ASPECT_RATIO_HD;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format
         // Assume image file formats have square pixels
         pixelAspectRatio = IF_PIXEL_ASPECT_RATIO_HD;
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         // Assume "dual link" formats have square pixels
         pixelAspectRatio = IF_PIXEL_ASPECT_RATIO_HD;
         break;

      case IF_TYPE_INTERNAL :         // Internal Format, no nominal value
      default :
         // Unexpected image format type
         pixelAspectRatio = 0.;
         break;
      }

   return pixelAspectRatio;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalPixelComponents
//
// Description: Returns nominal Pixel Components type for image format type
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     Pixel Components type
//
//------------------------------------------------------------------------
EPixelComponents CImageInfo::
queryNominalPixelComponents(EImageFormatType imageFormatType)
{
   EPixelComponents nominalPixelComponents;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625_4795 :     // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_625_4795_DVS :     // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit
         nominalPixelComponents = IF_PIXEL_COMPONENTS_YUV422;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format
         nominalPixelComponents = IF_PIXEL_COMPONENTS_RGB;
         break;

      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format
         nominalPixelComponents = IF_PIXEL_COMPONENTS_BGR;
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         nominalPixelComponents = IF_PIXEL_COMPONENTS_RGB;
         break;

      case IF_TYPE_INTERNAL :         // Internal Format, no nominal value
      default :
         // Unexpected image format type
         nominalPixelComponents = IF_PIXEL_COMPONENTS_INVALID;
         break;
      }

   return nominalPixelComponents;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalPixelPerLine
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CImageInfo::queryNominalPixelsPerLine(EImageFormatType imageFormatType)
{
   int nominalPixelsPerLine;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :
      case IF_TYPE_525_5994_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
         nominalPixelsPerLine = IF_PIXELS_PER_ROW_525;
         break;

      case IF_TYPE_625_4795 :     // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :         // Digital PAL, "Standard Def"
      case IF_TYPE_625_4795_DVS :     // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :         // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
         nominalPixelsPerLine = IF_PIXELS_PER_ROW_625;
         break;

      case IF_TYPE_720P_50 :        // 720 progressive-scan
      case IF_TYPE_720P_5994 :
      case IF_TYPE_720P_60 :
      case IF_TYPE_720P_50_DVS :        // 720 progressive-scan
      case IF_TYPE_720P_5994_DVS :
      case IF_TYPE_720P_60_DVS :
         nominalPixelsPerLine = IF_PIXELS_PER_ROW_720P;
         break;

      case IF_TYPE_1080I_50 :       // 1080 interlaced
      case IF_TYPE_1080I_5994 :
      case IF_TYPE_1080I_60 :
      case IF_TYPE_1080I_50_DVS :       // 1080 interlaced
      case IF_TYPE_1080I_5994_DVS :
      case IF_TYPE_1080I_60_DVS :
         nominalPixelsPerLine = IF_PIXELS_PER_ROW_1080I;
         break;

      case IF_TYPE_1080P_2398 :     // 1080 progressive-scan
      case IF_TYPE_1080PSF_2398 :
      case IF_TYPE_1080P_24 :
      case IF_TYPE_1080PSF_24 :
      case IF_TYPE_1080P_25 :
      case IF_TYPE_1080PSF_25 :

      case IF_TYPE_1080P_2398_DVS :     // 1080 progressive-scan
      case IF_TYPE_1080PSF_2398_DVS :
      case IF_TYPE_1080P_24_DVS :
      case IF_TYPE_1080PSF_24_DVS :
      case IF_TYPE_1080P_25_DVS :
      case IF_TYPE_1080PSF_25_DVS :
      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit
         nominalPixelsPerLine = IF_PIXELS_PER_ROW_1080P;
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         nominalPixelsPerLine = 2048;
         break;

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format
         // There is no default value for film and graphics file formats
         nominalPixelsPerLine = 0;
         break;

      case IF_TYPE_INTERNAL :         // Internal Format, no nominal value
      default :
         // Unexpected image format type
         nominalPixelsPerLine = -1;
      }

   return nominalPixelsPerLine;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalPulldown
//
// Description: Returns typical pull-down for a given image format type.
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments    EImageFormatType imageFormatType
//
// Returns:     Typical pulldown type for given image format
//                IF_PULLDOWN_INVALID      Invalid pulldown
//                IF_PULLDOWN_NONE         No pulldown
//                IF_PULLDOWN_FRAME_3_2    Frame-based 3:2 pulldown
//                IF_PULLDOWN_FIELD_3_2    Field-based 3:2 pulldown 
//
//------------------------------------------------------------------------
EPulldown CImageInfo::queryNominalPulldown(EImageFormatType imageFormatType)
{
   EPulldown nominalPulldown;

   switch (imageFormatType)
      {
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_5994_DVS : // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
         // Formats that typically have a field-based 3:2 pulldown
         // and video source has odd/even field order that is swapped
         nominalPulldown = IF_PULLDOWN_SWAPPED_FIELD_3_2;
         break;

      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
         // Formats that typically have a field-based 3:2 pulldown
         nominalPulldown = IF_PULLDOWN_FIELD_3_2;
         break;

      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second
         // Formats that typically have a frame-based 3:2 pulldown
         nominalPulldown = IF_PULLDOWN_FRAME_3_2;
         break;

      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_625_4795 :     // Special 625 proxy format
      case IF_TYPE_625_48 :       // Special 625 proxy format
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_625_4795_DVS :     // Special 625 proxy format
      case IF_TYPE_625_48_DVS :       // Special 625 proxy format
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS:      // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :

      // SD Progressive formats   (QQQ FIELDS SWAPPED?)
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive

         // Formats that typically do not have any type of pulldown
         nominalPulldown = IF_PULLDOWN_NONE;
         break;
         
      case IF_TYPE_INTERNAL :         // Internal Format, no nominal value
      default :
         // Unexpected image format type
         nominalPulldown = IF_PULLDOWN_INVALID;
         break;
      }

   return nominalPulldown;

} // queryNominalPulldown( )

//------------------------------------------------------------------------
//
// Function:    queryNominalTimecodeFrameRate
//
// Description:
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CImageInfo::queryNominalTimecodeFrameRate(EImageFormatType imageFormatType)
{
   int timecodeFrameRate;

   switch (imageFormatType)
      {
      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format
         // Image File formats, presumably RGB Film formats
         // Timecode frame rate of zero changes a CTimecode into frame counter
         timecodeFrameRate = 0;
         break;

      default :
         // Video formats, timecode frame rate follows nominal frame rate
         // for the format
         double dFrameRate = queryNominalFrameRate(imageFormatType);
         timecodeFrameRate = (int)(dFrameRate + .5);
         break;
      }

   return timecodeFrameRate;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalGamma
//
// Description: Returns nominal gamma for image format type
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     nominal gamma
//
//------------------------------------------------------------------------
double CImageInfo::queryNominalGamma(EImageFormatType imageFormatType)
{
   double nominalGamma;

   switch (imageFormatType)
      {
      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format
			// Assume image files have a reasonable intrinsic display gamma
         nominalGamma = 1.7;
         break;

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
      case IF_TYPE_FILE_EXR :     // EXR file format
         // No clue for 2K format (LOG color space)  QQQ check this!
         nominalGamma = 1.0;
         break;

      default :
         // For video formats it's actually hardwired in the color convert
         // module to 2.222
         nominalGamma = 2.2222;
         break;
      }

   return nominalGamma;
}

//------------------------------------------------------------------------
//
// Function:    queryNominalTransferCharacteristic
//
// Description: Always returns IF_COLOR_SPACE_INVALID which means that
//              the transfer fucntion is whataver is specified for the
//              color space.
//
// Arguments:   EImageFormatType imageFormatType   Image format type
//
// Returns:     Color Space type
//
//------------------------------------------------------------------------
EColorSpace CImageInfo::
queryNominalTransferCharacteristic(EImageFormatType imageFormatType)
{
   EColorSpace nominalTransferCharacteristic;

   nominalTransferCharacteristic = IF_COLOR_SPACE_INVALID;

   return nominalTransferCharacteristic;
}

//------------------------------------------------------------------------
//
// Function:    queryPixelComponents
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
EPixelComponents CImageInfo::
queryPixelComponents(const string& pixelComponentsName)
{
   for (int i = 0; i < (int)PIXEL_COMPONENTS_TYPE_COUNT; ++i)
      {
      if (EqualIgnoreCase(pixelComponentsTypeMap[i].name, pixelComponentsName))
         return pixelComponentsTypeMap[i].pixelComponents;
      }

   return IF_PIXEL_COMPONENTS_INVALID;
}

//------------------------------------------------------------------------
//
// Function:    queryPixelComponentsName
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
string CImageInfo::queryPixelComponentsName(EPixelComponents pixelComponents)
{
   for (int i = 0; i < (int)PIXEL_COMPONENTS_TYPE_COUNT; ++i)
      {
      if (pixelComponentsTypeMap[i].pixelComponents == pixelComponents)
         return string(pixelComponentsTypeMap[i].name);
      }

   return string("INVALID");
}

//------------------------------------------------------------------------
//
// Function:    queryPixelPacking
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
EPixelPacking CImageInfo::queryPixelPacking(const string& pixelPackingName)
{
   for (int i = 0; i < (int)PIXEL_PACKING_TYPE_COUNT; ++i)
	  {
      if (EqualIgnoreCase(pixelPackingTypeMap[i].name, pixelPackingName))
         return pixelPackingTypeMap[i].pixelPacking;
      }

   return IF_PIXEL_PACKING_INVALID;
}

//------------------------------------------------------------------------
//
// Function:    queryPixelPackingName
//
// Description: Gets name string of a pixel packing type.  This is a static
//              member function which means that it can be called without
//              a CImageType object instance.
//
// Arguments    EPixelPacking pixelPacking   Pixel Packing type
//
// Returns:     string containing name of pixel packing type
//
//------------------------------------------------------------------------
string CImageInfo::queryPixelPackingName(EPixelPacking pixelPacking)
{
   for (int i = 0; i < (int)PIXEL_PACKING_TYPE_COUNT; ++i)
      {
      if (pixelPackingTypeMap[i].pixelPacking == pixelPacking)
         return string(pixelPackingTypeMap[i].name);
      }

   return string("INVALID");
}

//------------------------------------------------------------------------
//
// Function:    queryStandardFrameRate
//
// Description:
//              This is a static function and does not require or use
//              an instance of CImageInfo class.
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
double CImageInfo::queryStandardFrameRate(EFrameRate frameRateEnum)
{
   double frameRate;

   switch (frameRateEnum)
      {
      case IF_FRAME_RATE_1498 :
         frameRate = 24. * IF_DF_FRAME_RATE_RATIO;
         break;
      case IF_FRAME_RATE_1500 :
         frameRate = 24.;
         break;
      case IF_FRAME_RATE_1498_25 :
/* need to get Gianni to test.  He prefers using 25 frame, but does it
 * work? */
         frameRate = 25.;
         break;
      case IF_FRAME_RATE_1500_25 :
/* need to get Gianni to test.  He prefers using 25 frame, but does it
 * work? */
         frameRate = 25.;
         break;
      case IF_FRAME_RATE_2398 :             // 23.976 fps (24/1.001)
         frameRate = 24. * IF_DF_FRAME_RATE_RATIO; 
         break;
      case IF_FRAME_RATE_2400 :             // 24.00 fps (Standard film rate)
         frameRate = 24.; 
         break;
      case IF_FRAME_RATE_2500 :             // 25.00 fps (PAL)
         frameRate = 25.; 
         break;
      case IF_FRAME_RATE_2997 :             // 29.97 fps (NTSC, 30/1.001) 
         frameRate = 30. * IF_DF_FRAME_RATE_RATIO; 
         break;
      case IF_FRAME_RATE_3000 :             // 30.00 fps
         frameRate = 30.; 
         break;
      case IF_FRAME_RATE_5000 :             // 50.00 fps 
         frameRate = 50.;
         break;
      case IF_FRAME_RATE_5994 :             // 59.94 fps (60/1.001)
         frameRate = 60. * IF_DF_FRAME_RATE_RATIO; 
         break;
      case IF_FRAME_RATE_6000 :             // 60.00 fps
         frameRate = 60.; 
         break;
      case IF_FRAME_RATE_INVALID :
      case IF_FRAME_RATE_NON_STANDARD :     // Non-standard frame rate 
      default :
         // frameRateEnum is a non-standard, invalid or illegal value
         // Return 0 as frame rate, which should cause some trouble
         frameRate = 0.;
         break;
      }

   return frameRate;
}

//////////////////////////////////////////////////////////////////////
// Static Validation Functions
//////////////////////////////////////////////////////////////////////

int CImageInfo::isAlphaMatteTypeValid(EAlphaMatteType alphaMatteType)
{
   switch(alphaMatteType)
      {
      case IF_ALPHA_MATTE_DEFAULT:
      case IF_ALPHA_MATTE_NONE:
      case IF_ALPHA_MATTE_1_BIT:
      case IF_ALPHA_MATTE_8_BIT:
      case IF_ALPHA_MATTE_16_BIT:
      case IF_ALPHA_MATTE_2_BIT_EMBEDDED:
      case IF_ALPHA_MATTE_8_BIT_INTERLEAVED:
      case IF_ALPHA_MATTE_10_BIT_INTERLEAVED:
      case IF_ALPHA_MATTE_16_BIT_INTERLEAVED:
         return 0;   // Okay

      case IF_ALPHA_MATTE_INVALID:
      default:
         return FORMAT_ERROR_INVALID_ALPHA_MATTE_TYPE;
      }
}

//------------------------------------------------------------------------
//
// Function:    isColorSpaceValid
//
// Description: Validates an image format's color space
//
// Arguments    EColorSpace colorSpace
//
// Returns:     0 if okay
//              IF_COLOR_SPACE_INVALID if color space is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isColorSpaceValid(EColorSpace colorSpace)
{
   switch(colorSpace)
      {
      case IF_COLOR_SPACE_LINEAR:          // Typically RGB
      case IF_COLOR_SPACE_LOG:             // Typically RGB
      case IF_COLOR_SPACE_SMPTE_240:       // Typically video
      case IF_COLOR_SPACE_CCIR_709:        // Typically HD video, also SD video
      case IF_COLOR_SPACE_CCIR_601_BG:     // 601 System B or G, Typically 625
      case IF_COLOR_SPACE_CCIR_601_M:      // 601 System M, Typically 525
      case IF_COLOR_SPACE_EXR:             // EXR RGB
         // Okay
         break;

      case IF_COLOR_SPACE_INVALID:
      default:
         // No good
         return FORMAT_ERROR_INVALID_COLOR_SPACE;
      }

   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isTransferCharacteristicValid
//
// Description: Validates an image format's transfer characteristic
//
// Arguments    EColorSpace transferCharacteristic
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_TRANSFER_CHARACTERISTICS if invalid
//
//------------------------------------------------------------------------
int CImageInfo::isTransferCharacteristicValid(
                                        EColorSpace transferCharacteristic)
{
   // Transfer characteristic is always valid because IF_COLOR_SPACE_INVALID
   // is used to indicate that the default transfer function for the
   // selected COLOR SPACE should be used (no ocverride).
   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isFrameAspectRatioValid
//
// Description: Validates an image format's frame aspect ratio
//              Valid range is greater than 0 and less than 10.
//              Range is limited to detect wild data rather than
//              for any geometric reason
//
// Arguments    MTI_REAL32 frameAspectRatio
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_FRAME_ASPECT_RATIO if ratio is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isFrameAspectRatioValid(MTI_REAL32 frameAspectRatio)
{
   if (frameAspectRatio <= 0.0 || frameAspectRatio > 10.0)
      return FORMAT_ERROR_INVALID_FRAME_ASPECT_RATIO;

   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isHorizontalIntervalPixelsValid
//
// Description: Validates an image format's horizontal blanking interval
//              pixel count
//              Valid range is 0 to 10000.
//              Range is limited to detect wild data rather than
//              for any geometric reason
//
// Arguments    MTI_UINT32 horizontalIntervalPixels
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_HORIZONTAL_INTERVAL_PIXELS if pixels is
//              invalid
//
//------------------------------------------------------------------------
int CImageInfo::
isHorizontalIntervalPixelsValid(MTI_UINT32 horizontalIntervalPixels)
{
   if (horizontalIntervalPixels > 10000)
      return FORMAT_ERROR_INVALID_HORIZONTAL_INTERVAL_PIXELS;

   return 0; // Okay
}
//------------------------------------------------------------------------
//
// Function:    isImageFormatTypeValid
//
// Description: Validates an image format's image format type
//
// Arguments    EImageFormatType imageFormatType
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_IMAGE_FORMAT_TYPE if type is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isImageFormatTypeValid(EImageFormatType imageFormatType)
{
   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625_4795 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25 :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25 :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_5994_DVS :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625_4795_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_48_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625_50_DVS :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398_DVS :   // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS : // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :     // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :   // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_25_DVS :   // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080P_25_DVS :     // 1080 progressive-scan, 25 frames/second

      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit

      case IF_TYPE_1080PSFI_2398 :      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit
      
      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_720P_50_DVS :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :      // 720 progressive-scan, 60 frames/second

      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_INTERNAL :     // Internal Format
      case IF_TYPE_FILE_TGA :     // Targa file format
      case IF_TYPE_FILE_TGA2 :    // Targa2 file format
      case IF_TYPE_FILE_BMP :    // BMP file format
      case IF_TYPE_FILE_EXR :     // EXR file format

      // "Dual Link" formats from DVS card
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         // Ok
         break;

      // "Progressive" SD formats for when the data is line-interleaved in
      // one file per frame


      case IF_TYPE_INVALID:
      default :
         // Unexpected image format type
         return FORMAT_ERROR_INVALID_IMAGE_FORMAT_TYPE;
      }
      
   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isLinesPerFrameValid
//
// Description: Validates an image format's lines per frame
//              Valid range is greater than 0 and less than 10000.
//              Range is limited to detect wild data rather than
//              for any geometric reason
//
// Arguments    MTI_UINT32 linesPerFrame
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_LINES_PER_FRAME if lines per frame is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isLinesPerFrameValid(MTI_UINT32 linesPerFrame)
{
   if (linesPerFrame < 1 || linesPerFrame > 10000)
      return FORMAT_ERROR_INVALID_LINES_PER_FRAME;

   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isPixelAspectRatioValid
//
// Description: Validates an image format's pixel aspect ratio
//              Valid range is greater than 0 and less than 10.
//              Range is limited to detect wild data rather than
//              for any geometric reason
//
// Arguments    MTI_REAL32 pixelAspectRatio
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_PIXEL_ASPECT_RATIO if ratio is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isPixelAspectRatioValid(MTI_REAL32 pixelAspectRatio)
{
   if (pixelAspectRatio <= 0.0 || pixelAspectRatio > 10.0)
      return FORMAT_ERROR_INVALID_PIXEL_ASPECT_RATIO;

   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isPixelComponentsValid
//
// Description: Validates an image format's pixel components
//
// Arguments    EPixelComponents pixelComponents
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_PIXEL_COMPONENTS if components is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isPixelComponentsValid(EPixelComponents pixelComponents)
{
   switch (pixelComponents)
      {
//      case IF_PIXEL_COMPONENTS_LUMINANCE :   // Luminance only, monochrome
      case IF_PIXEL_COMPONENTS_RGB :         // RGB
      case IF_PIXEL_COMPONENTS_RGBA :        // RGB + alpha matte channel
      case IF_PIXEL_COMPONENTS_YUV422 :      // CbYCrY
      case IF_PIXEL_COMPONENTS_YUV4224 :     // YUV + alpha, CbYACrYA
      case IF_PIXEL_COMPONENTS_YUV444 :      // CbYCr
      case IF_PIXEL_COMPONENTS_BGR :         // BGR, e.g. Targa
      case IF_PIXEL_COMPONENTS_BGRA :        // BGR + alpha matte channel
      case IF_PIXEL_COMPONENTS_Y :           // Y
      case IF_PIXEL_COMPONENTS_YYY :         // YYY
         // Okay
         break;

      case IF_PIXEL_COMPONENTS_INVALID :
      default :
         // Bad
         return FORMAT_ERROR_INVALID_PIXEL_COMPONENTS;
      }
   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isPixelPackingValid
//
// Description: Validates an image format's pixel packing
//
// Arguments    EPixelPacking pixelPacking
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_PIXEL_PACKING if packing is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isPixelPackingValid(EPixelPacking pixelPacking)
{
   switch(pixelPacking)
      {
      case IF_PIXEL_PACKING_8Bits_IN_1Byte :

      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes :   // Typical 10bit YUV422
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L : // 10 bit RGB in 32-bit word
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R : // 10 bit RGB in 32-bit word
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L : // 10 bits in 16-bit word
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R : // 10 bits in 16-bit word
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS :  // std DVS 10-Bit YUV422
      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:  // old DVS 10-Bit YUV422

      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes :    // 12-bits padded
      case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:  // reverses spec
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE :  // 12-bits left justified, Little-Endian
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE :  // 12-bits left justified, Big-Endian
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE : // 16-bits, Big-Endian byte ordering
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE : // 16-bits, Little-Endian byte ordering
      case IF_PIXEL_PACKING_1_8Bits_IN_2Bytes:      // 8-bits, native byte ordering
      case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes:     // 10-bits, native byte ordering
      case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes :    // 12-bits native byte ordering
      case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT: // 16-bits, native byte ordering
      case IF_PIXEL_PACKING_1_Half_IN_2Bytes:       // 16-bit float, Little-Endian byte ordering

         // Okay
         break;

      case IF_PIXEL_PACKING_INVALID :
      default:
         // Bad
         return FORMAT_ERROR_INVALID_PIXEL_PACKING;
      }
   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isPixelsPerLineValid
//
// Description: Validates an image format's pixels per line
//              Valid range is greater than 0 and less than 10000.
//              Range is limited to detect wild data rather than
//              for any geometric reason
//
// Arguments    MTI_UINT32 pixelsPerLine
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_PIXELS_PER_LINE if pixels per line is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isPixelsPerLineValid(MTI_UINT32 pixelsPerLine)
{
   if (pixelsPerLine < 1 || pixelsPerLine > 10000)
      return FORMAT_ERROR_INVALID_PIXELS_PER_LINE;

   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isVerticalIntervalRowsValid
//
// Description: Validates an image format's vertical blanking interval rows.
//              Valid range is 0 to 10000.
//              Range is limited to detect wild data rather than
//              for any geometric reason
//
// Arguments    MTI_UINT32 verticalIntervalRows
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_VERTICAL_INTERVAL_ROWS if rows is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isVerticalIntervalRowsValid(MTI_UINT32 verticalIntervalRows)
{
   if (verticalIntervalRows > 10000)
      return FORMAT_ERROR_INVALID_VERTICAL_INTERVAL_ROWS;

   return 0; // Okay
}

//------------------------------------------------------------------------
//
// Function:    isGammaValid
//
// Description: Validates a gamma value. We rather arbitrarily limit
//              gamma adjustments to the range 0.1 to 10 to detect
//              wild values.
//
// Arguments    MTI_REAL32 gamma
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_GAMMA if gamma is invalid
//
//------------------------------------------------------------------------
int CImageInfo::isGammaValid(MTI_REAL32 gamma)
{
   if (gamma < 0.1 || gamma > 10.0)
      return FORMAT_ERROR_INVALID_GAMMA;

   return 0; // Okay
}

//////////////////////////////////////////////////////////////////////////////
// Color Space Functions
//////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    queryNominalComponentValue
//
// Description: Returns default pixel component values.
//
//              All RGB and YUV pixel component types return three component
//              values, even if there is an alpha channel.  The order of the
//              component values is always R, G, B or Y, U, V.
//
// Arguments    EComponentValueType valueType   Type of component values
//                                              requested: Min, max, white,
//                                              black, etc.
//              EPixelComponents pixelComponents
//              EColorSpace colorSpace
//              int bitsPerComponent
//              MTI_UINT16 componentValue[]   Caller's array to accept
//                                            component values.  Assumed to
//                                            be large enough to accept
//                                            default number of pixels given
//                                            the pixelComponents argument
//
//
// Returns:     0 if okay
//              FORMAT_ERROR_INVALID_PIXEL_PACKING if packing is invalid
//
//------------------------------------------------------------------------
int CImageInfo::queryNominalComponentValue(EComponentValueType valueType,
                                           EPixelComponents pixelComponents,
                                           EColorSpace colorSpace,
                                           bool isVideoFormat,
                                           int bitsPerComponent,
                                           MTI_UINT16 componentValue[])
{
   MTI_UINT16 *srcComponentValues;
   MTI_UINT16 multiplier = 1;

   switch (pixelComponents)
      {
      case IF_PIXEL_COMPONENTS_YUV422 :      // CbYCrY
      case IF_PIXEL_COMPONENTS_YUV4224 :     // YUV + alpha, CbYACrYA
      case IF_PIXEL_COMPONENTS_YUV444 :      // CbYCr
         // YUV Video color components
         if (bitsPerComponent == 8)
            srcComponentValues = componentValuesYUV8[valueType];
         else if (bitsPerComponent == 10)
            srcComponentValues = componentValuesYUV10[valueType];
         else if (bitsPerComponent > 10)
            {
            srcComponentValues = componentValuesYUV8[valueType];
            multiplier = 1 << (bitsPerComponent - 8);
            }
         else
            return FORMAT_ERROR_INVALID_BITS_PER_COMPONENT;

         break;

//      case IF_PIXEL_COMPONENTS_LUMINANCE :   // Luminance only, monochrome
      case IF_PIXEL_COMPONENTS_RGB :         // RGB
      case IF_PIXEL_COMPONENTS_RGBA :        // RGB + alpha matte channel
      case IF_PIXEL_COMPONENTS_BGR :         // BGR, e.g. Targa
      case IF_PIXEL_COMPONENTS_BGRA :        // BGR + alpha matte channel
      case IF_PIXEL_COMPONENTS_Y :           // Y
      case IF_PIXEL_COMPONENTS_YYY :         // YYY
         switch (colorSpace)
            {
            case IF_COLOR_SPACE_LINEAR:          // Typically RGB
            case IF_COLOR_SPACE_LOG:             // Typically RGB
               // RGB color components
               if (bitsPerComponent == 8)
                  srcComponentValues = (isVideoFormat)
                                           ? componentValuesRGBVideo8[valueType]
                                           : componentValuesRGB8[valueType];
               else if (bitsPerComponent == 10)
                  srcComponentValues = (isVideoFormat)
                                         ? componentValuesRGBVideo10[valueType]
                                         : componentValuesRGB10[valueType];
               else if (bitsPerComponent == 12)
                  srcComponentValues = componentValuesRGB12[valueType];
               else if (bitsPerComponent == 16)
                  srcComponentValues = componentValuesRGB16[valueType];
               else if (bitsPerComponent > 0 && bitsPerComponent < 16)
                  {
                  srcComponentValues = componentValuesRGB1[valueType];
		  // WTF is this?? How about a frickin' comment?
                  if (valueType == IF_COMPONENT_VALUE_TYPE_FILL)
                     multiplier = (1 << (bitsPerComponent - 1));
                  else
                     multiplier = (1 << bitsPerComponent) - 1;
                  }
               else
                  return FORMAT_ERROR_INVALID_BITS_PER_COMPONENT;
               break;

            case IF_COLOR_SPACE_EXR:             // EXR RGB
               // RGB color components
					srcComponentValues = componentValuesRGBHalf[valueType];
               break;

            case IF_COLOR_SPACE_SMPTE_240:       // Typically video
            case IF_COLOR_SPACE_CCIR_709:        // Typically HD video, also SD video
            case IF_COLOR_SPACE_CCIR_601_BG:     // 601 System B or G, Typically 625
            case IF_COLOR_SPACE_CCIR_601_M:      // 601 System M, Typically 525
               // I think there are RGB versions of these video standards, but
               // I'm not sure we ever run into them.  Treat as error for now.

            default:
               // No good
               return FORMAT_ERROR_INVALID_COLOR_SPACE;
            }
         break;

      case IF_PIXEL_COMPONENTS_INVALID :
      default :
         // Bad
         return FORMAT_ERROR_INVALID_PIXEL_COMPONENTS;
      }


   if (componentValue != 0)
      {
      // Iterate over the number of components
      int componentCount = queryNominalComponentCount(pixelComponents);
      if (componentCount > 3)
         componentCount = 3;    // TTT Skip alpha channel for now
      for (int i = 0; i < componentCount; ++i)
         componentValue[i] = srcComponentValues[i] * multiplier;
      }

   return 0; // Okay
}


//////////////////////////////////////////////////////////////////////////////
// HACKS
//////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    isImageFormatCompatibleWithDVS
//
// Description: Returns true if the image format type is one supported by DVS.
//
// Arguments    EImageFormatType imageFormatType   The Type to check
//
// Returns:     non-zero if OK
//
//------------------------------------------------------------------------
/* static */
bool CImageInfo::isImageFormatCompatibleWithDVS(
                          EImageFormatType imageFormatType,
                          EPixelPacking pixelPacking)
{
   bool retVal = true;

   switch (imageFormatType)
      {
      case IF_TYPE_525_4795 :     // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48 :       // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_625_4795 :     // Special proxy format
      case IF_TYPE_625_48 :       // Special proxy format
      case IF_TYPE_720P_50 :      // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994 :    // 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60 :      // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50 :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994 :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60 :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398 :    // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398 :  // 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24 :      // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24 :    // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25 :      // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25 :    // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PI_2398 :      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_24 :        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PI_25 :        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_2398 :    // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_24 :      // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_1080PSFI_25 :      // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
      case IF_TYPE_FILE_DPX :     // SMPTE file format
      case IF_TYPE_FILE_CINEON :  // Kodak CINEON file format (similar to DPX)
      case IF_TYPE_FILE_TIFF :    // TIFF graphics file format
      case IF_TYPE_FILE_SGI :     // Silicon Graphics graphics file format
      case IF_TYPE_FILE_TGA :    // TGA graphics file format
      case IF_TYPE_FILE_TGA2 :    // TGA-2 graphics file format
      case IF_TYPE_FILE_BMP :    // BMP graphics file format
      case IF_TYPE_FILE_EXR :     // EXR file format
      case IF_TYPE_INTERNAL :     // Internal format, typically 16-bit
      default:
         retVal = false;
         break;

      // Non-DVS SD formats are OK for 8 bit data only (they tell us to
      // pack the data differently)
      case IF_TYPE_525_5994 :     // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60 :       // Digital NTSC, "Standard Definition"
      case IF_TYPE_625_50 :       // Digital PAL, "Standard Def"
      case IF_TYPE_625P_25 :      // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_525IP_5994 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60 :       // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398 :     // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24 :       // Digital NTSC, "Standard Def", Progressive
         if (pixelPacking != IF_PIXEL_PACKING_8Bits_IN_1Byte)
            retVal = false;
         break;

      // DVS-compatible formats
      case IF_TYPE_525_5994_DVS : // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_60_DVS :   // Digital NTSC, "Standard Definition"
      case IF_TYPE_525_4795_DVS : // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525_48_DVS :   // Special 525 proxy format (no 3:2 pulldown)
      case IF_TYPE_525IP_5994_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525IP_60_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_2398_DVS : // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_525P_24_DVS :   // Digital NTSC, "Standard Def", Progressive
      case IF_TYPE_625_4795_DVS : // Special proxy format
      case IF_TYPE_625_48_DVS :   // Special proxy format
      case IF_TYPE_625P_25_DVS :  // Digital PAL, "Standard Def", Progressive
      case IF_TYPE_720P_50_DVS :  // 720 progressive-scan, 50 frames/second
      case IF_TYPE_720P_5994_DVS :// 720 progressive-scan, 59.94 frames/second
      case IF_TYPE_720P_60_DVS :  // 720 progressive-scan, 60 frames/second
      case IF_TYPE_1080I_50_DVS :     // 1080 interlaced, 50 fields/second
      case IF_TYPE_1080I_5994_DVS :   // 1080 interlaced, 59.94 fields/second
      case IF_TYPE_1080I_60_DVS :     // 1080 interlaced, 60 fields/second
      case IF_TYPE_1080P_2398_DVS :// 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080PSF_2398_DVS:// 1080 progressive-scan, 23.98 frames/second
      case IF_TYPE_1080P_24_DVS :  // 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080PSF_24_DVS :// 1080 progressive-scan, 24 frames/second
      case IF_TYPE_1080P_25_DVS :  // 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PSF_25_DVS :// 1080 progressive-scan, 25 frames/second
      case IF_TYPE_1080PI_2398_DVS :  // 1080P/23.98   DVS 10 bit
      case IF_TYPE_1080PI_24_DVS :    // 1080P/24      DVS 10 bit
      case IF_TYPE_1080PI_25_DVS :    // 1080P/25      DVS 10 bit
      case IF_TYPE_1080PSFI_2398_DVS :  // 1080PsF/23.98 DVS 10 bit
      case IF_TYPE_1080PSFI_24_DVS :    // 1080PsF/24    DVS 10 bit
      case IF_TYPE_1080PSFI_25_DVS :    // 1080PsF/25    DVS 10 bit
      case IF_TYPE_2048_1536PSF_24_DVS :
      case IF_TYPE_2048_1536P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_DVS :
      case IF_TYPE_2048_1556PSF_15_DVS :
      case IF_TYPE_2048_1556PSF_24_DVS :
      case IF_TYPE_2048_1556P_24_DVS :
      case IF_TYPE_2048_1556PSF_1498_25_DVS :
      case IF_TYPE_2048_1556PSF_15_25_DVS :
         break;
      }
   if (retVal == 0)
      {

      switch (pixelPacking)
         {
         case IF_PIXEL_PACKING_INVALID:
         case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:     // Typical 10bit YUV422
         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:   // 10 bit RGB in 32-bit word
         case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:   // 10 bits in 16-bit word
         case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:   // 10 bits in 16-bit word
         case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:     // 12 bits packed
         case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:  // reverses spec
         case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:   // 12 bits, left justified, Little-Endian
         case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:   // 12 bits, left justified, Big-Endian
         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:  // 16-bits, Big-Endian byte ordering
         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:  // 16-bits, Little-Endian byte ordering
         case IF_PIXEL_PACKING_1_8Bits_IN_2Bytes:      // 8-bits, native byte ordering
         case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes:     // 10-bits, native byte ordering
         case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes:     // 12-bits, native byte ordering
         case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT: // 16-bits, native byte ordering
         case IF_PIXEL_PACKING_1_Half_IN_2Bytes:       // 16-bit float, Little-Endian byte ordering
            retVal = false;   // Not DVS compatible
            break;

            // These are DVS-compatible
         case IF_PIXEL_PACKING_8Bits_IN_1Byte:
         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:   // 10 bit RGB in 32-bit word (DPX style)
         case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS: // 10 bit RGB in 32-bit word (Old DVS style)
			break;

		 default:
		 	break;
         }
      }


   return retVal;
}

//------------------------------------------------------------------------
//
// Function:    GetDpxIsDefaultLog
//
// Description: Is the default colorspace for DPX LOG or LINEAR?
//
// Arguments    None.
//
// Returns:     TRUE if the default colorspace for DPZ files is LOG;
//              FALSE if the default colorspace is LINEAR.
//
//------------------------------------------------------------------------
/* static */
bool CImageInfo::GetDpxIsDefaultLog()
{
  if (!DpxIsDefaultLogCacheIsValid)
   {
    CIniFile *ini = CPMPOpenLocalMachineIniFile ();
    if (ini)
     {
      DpxIsDefaultLogCache = ini->ReadBoolCreate ("General", "DpxDefaultLog",
                                                  DpxIsDefaultLogCache);
      DpxIsDefaultLogCacheIsValid = true;
     }

    delete ini;
   }

  return DpxIsDefaultLogCache;
}

