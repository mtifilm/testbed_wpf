// AudioInfo.h
//
// Constants and enumeration types for CAudioFormat
//
// Created by: John Starr, January 12, 2001
//
//////////////////////////////////////////////////////////////////////

#ifndef AUDIO_INFO_H
#define AUDIO_INFO_H

#include "formatDLL.h"
#include "machine.h"

//////////////////////////////////////////////////////////////////////

// Standard audio sampling rates, in samples per second
#define AI_STD_SAMPLE_RATE_44056 (44100/1.001)  // 44.056 KHz
#define AI_STD_SAMPLE_RATE_44100 (44100)        // 44.1 KHz
#define AI_STD_SAMPLE_RATE_48000 (48000)        // 48 KHz

//////////////////////////////////////////////////////////////////////

#define AI_SAMPLE_BIT_COUNT_MIN  8
#define AI_SAMPLE_BIT_COUNT_MAX  32

//////////////////////////////////////////////////////////////////////

// NB: Enumerator values are explicitly set to make file compatibility
//     easier.  New enumerators should be added with explicit values and
//     not repeat a value.

//////////////////////////////////////////////////////////////////////
// Audio File Type
// Enumeration for audio file types
enum EAudioFileType
{
   AI_TYPE_INVALID = 0,          // Illegal file type
   AI_TYPE_AIFF = 1,             // AIFF SGI/Apple Audio Interface File    
   AI_TYPE_AIFC = 2,             // AIFF-C SGI/Apple Extended AIFF 
   AI_TYPE_SD2 = 3,              // SD2 SoundDesigner II
   AI_TYPE_RAW = 4               // Unformatted audio data without header
};

//////////////////////////////////////////////////////////////////////
// Sample Rate
// Enumeration for standard audio sample rates
enum EAudioSampleRate
{
   AI_SAMPLE_RATE_INVALID = 0,
   AI_SAMPLE_RATE_NON_STANDARD = 1,     // Non-standard sample rate 
   AI_SAMPLE_RATE_44056 = 2,             // 44.056 KHz (44.1/1.001)
   AI_SAMPLE_RATE_44100 = 3,             // 44.1 KHz
   AI_SAMPLE_RATE_48000 = 4,             // 48 KHz
};

//////////////////////////////////////////////////////////////////////
// Sample Data Type
// Enumeration for audio sample data types
enum EAudioSampleType
{
   AI_SAMPLE_DATA_TYPE_INVALID = 0,
   AI_SAMPLE_DATA_TYPE_INT = 1,     // Signed integer
   AI_SAMPLE_DATA_TYPE_UINT = 2,    // Unsigned integer
   AI_SAMPLE_DATA_TYPE_FLOAT = 3    // Floating point
};

//////////////////////////////////////////////////////////////////////
// Sample Data Packing
// Enumeration for audio sample data packing
enum EAudioSamplePacking
{
   AI_SAMPLE_DATA_PACKING_INVALID = 0,
   AI_SAMPLE_DATA_PACKING_1_Byte = 1,     
   AI_SAMPLE_DATA_PACKING_2_Bytes_L = 2,    
   AI_SAMPLE_DATA_PACKING_2_Bytes_R = 3,    
   AI_SAMPLE_DATA_PACKING_3_Bytes_L = 4,    
   AI_SAMPLE_DATA_PACKING_3_Bytes_R = 5,    
   AI_SAMPLE_DATA_PACKING_4_Bytes_L = 6,    
   AI_SAMPLE_DATA_PACKING_4_Bytes_R = 7    
};

//////////////////////////////////////////////////////////////////////
// Sample Data Endian
// Enumeration for audio sample data endian
enum EAudioSampleEndian
{
   AI_SAMPLE_DATA_ENDIAN_INVALID = 0,
   AI_SAMPLE_DATA_ENDIAN_BIG = 1,     
   AI_SAMPLE_DATA_ENDIAN_LITTLE = 2,    
};

//////////////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CAudioInfo
{
public:
   // Static Query Functions
   static EAudioSamplePacking querySampleDataPacking(
                                              const string& audioPackingString);
   static string querySampleDataPackingName(EAudioSamplePacking audioPacking);
   static EAudioSampleType querySampleDataType(
                                           const string& audioSampleTypeString);
   static string querySampleDataTypeName(EAudioSampleType sampleType);
   static EAudioSampleRate querySampleRate(const string& sampleRateName,
                                           double &newSampleRate);
   static string querySampleRateName(EAudioSampleRate sampleRate);
   static double queryStandardSampleRate(EAudioSampleRate sampleRateEnum);

   // Validation Functions
   static int isChannelCountValid(int channelCount);
   static int isSampleDataEndianValid(EAudioSampleEndian sampleDataEndian);
   static int isSampleDataPackingValid(EAudioSamplePacking sampleDataPacking);
   static int isSampleDataSizeValid(int sampleDataSize);
   static int isSampleDataTypeValid(EAudioSampleType sampleDataType);
   static int isSampleRateEnumValid(EAudioSampleRate sampleRateEnum);
};

//////////////////////////////////////////////////////////////////////
#endif // #ifndef AUDIO_INFO_H
