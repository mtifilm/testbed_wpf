#ifndef LineEngineH
#define LineEngineH

#include "machine.h"
#include "formatDLL.h"
#include "ImageFormat3.h"


// LINE-ENGINE class for graphical operations on video fields.

class MTI_FORMATDLL_API CLineEngine
{

protected:

   int
      dwide,
      dhigh,
      dpitch,

      curx,
      cury,

      fgndColor,
      bgndColor,

      rowMult,
      pelMult,
      fontWidth,
      fontHeight,

      fldSize;

   bool
      intrlc;

   // this table will accclerate access to
   // the individual field frame buffers

   struct OFENTRY
   {
      int rowfld;
      int rowoff;
   };

   OFENTRY *rowTbl;

   // variables for clipping lines ---------------------------------------------

   // edge vertices
   int VPreX, VPreY;
   int VCurX, VCurY;

   // Cohn-Sutherland codes
   unsigned char tcur,tpre;

   // crossing coordinates
   int XCrs, YCrs;

   // for clipping calculations
   double delxi, delyi;
   double lftxi, rgtxi;
   double botyi, topyi;

public:

   CLineEngine() { };

   virtual ~CLineEngine() { };

   virtual void setFrameBufferDimensions(int w, int h, int pitch, bool il) = 0;
   int getFrameWidth() { return dwide; };
   int getFrameHeight() { return dhigh; };

   int getFieldSize() const;

#define TLFT 0x8
#define TTOP 0x4
#define TRGT 0x2
#define TBOT 0x1
   int tCode(int, int);

   virtual int allocateInternalFrameBuffer() = 0;
   virtual void setExternalFrameBuffer(unsigned char **frmbuf) = 0;
   virtual void setFGColor(int) = 0;
   virtual void setFGColor(MTI_UINT16,MTI_UINT16,MTI_UINT16) = 0;
   virtual void setBGColor(int) = 0;
   virtual void setBGColor(MTI_UINT16,MTI_UINT16,MTI_UINT16) = 0;

   virtual void movTo(int x, int y) = 0; // unclipped
   virtual void linTo(int x, int y) = 0; // unclipped
   virtual void drawDot() = 0;
   virtual void drawRectangle(int l,int t, int r, int b)  = 0;
   virtual void drawSpot(int x, int y);

   void setFontSize(int siz);
   int getFontWidth() const;
   int getFontHeight() const;

   virtual void drawNumeral(int x, int y, int num) = 0;

   void fillField();
   void drawString(int x, int y, const char *str);
   void hiliteString(int x, int y, const char *str);
   void moveTo(int x, int y); // clipped
   void lineTo(int x, int y); // clipped
   void splineTo(int x1, int y1, int x2, int y2, int x3, int y3);
 };


class MTI_FORMATDLL_API CLineEngineFactory
{
  public:
    static CLineEngine *makePixelEng(CImageFormat const &imageFormat);
    static void destroyPixelEng(CLineEngine *pixelEng);
};


///////////////////////////////////////////////////////////////
//
// Here Y is specified for every pixel,  U and V every other.
// The values are 8 bits wide. Thus, two pixels correspond to
// U:Y:V:Y = 4 bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngYUV8: public CLineEngine
{

private:

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int uvphase;

   unsigned char *linebuf;

   unsigned char fgColor[4];

   unsigned char bgColor[4];

   inline void setFGY0();
   inline void setFGY1();

   inline void setBGY0();
   inline void setBGY1();

public:

   CLineEngYUV8();

   ~CLineEngYUV8();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here Y is specified for every pixel,  U and  V every other.
// The values are 10 bits wide. Thus, two pixels correspond to
// U:Y:V:Y = 5 bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngYUV10: public CLineEngine
{

private:

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int uvphase;

   unsigned char *linebuf;

   unsigned char fgColor[5];

   unsigned char bgColor[5];

   inline void setFGY0();
   inline void setFGY1();

   inline void setBGY0();
   inline void setBGY1();

public:

   CLineEngYUV10();

   ~CLineEngYUV10();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here the pattern of packing repeats for every six pixels.
// Three values are packed in 30 bits of a 32-bit DWORD, in LE
// style, with the unused bits at the high end. Four DWORDS rep-
// resent UYV:YUY:VYU:YVY.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngDVS10: public CLineEngine
{

private:

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int uvphase;

   unsigned char *linebuf;

   unsigned int fgColor[3];

   unsigned int bgColor[3];

   inline void setFGY0();
   inline void setFGY1();
   inline void setFGY2();
   inline void setFGY3();
   inline void setFGY4();
   inline void setFGY5();

   inline void setBGY0();
   inline void setBGY1();
   inline void setBGY2();
   inline void setBGY3();
   inline void setBGY4();
   inline void setBGY5();

public:

   CLineEngDVS10();

   ~CLineEngDVS10();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here the pattern of packing repeats for every six pixels.
// Three values are packed in 30 bits of a 32-bit DWORD, in BE
// style, with the unused bits at the low end. Four DWORDS rep-
// resent UYV:YUY:VYU:YVY. This is the "Clipster" format.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngDVS10BE: public CLineEngine
{

private:

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int uvphase;

   unsigned char *linebuf;

   unsigned int fgColor[3];

   unsigned int bgColor[3];

   inline void setFGY0();
   inline void setFGY1();
   inline void setFGY2();
   inline void setFGY3();
   inline void setFGY4();
   inline void setFGY5();

   inline void setBGY0();
   inline void setBGY1();
   inline void setBGY2();
   inline void setBGY3();
   inline void setBGY4();
   inline void setBGY5();

public:

   CLineEngDVS10BE();

   ~CLineEngDVS10BE();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here the pattern of packing repeats for every six pixels.
// The most-significant 8 bits of 3 10-bit values are packed LE
// into the first 3 bytes. The fourth byte contains the least-
// significant 2-bits of the 3-values, right aligned.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngDVS10a: public CLineEngine
{

private:

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int uvphase;

   unsigned char *linebuf;

   unsigned int fgColor[3];

   unsigned int bgColor[3];

   inline void setFGY0();
   inline void setFGY1();
   inline void setFGY2();
   inline void setFGY3();
   inline void setFGY4();
   inline void setFGY5();

   inline void setBGY0();
   inline void setBGY1();
   inline void setBGY2();
   inline void setBGY3();
   inline void setBGY4();
   inline void setBGY5();

public:

   CLineEngDVS10a();

   ~CLineEngDVS10a();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here R, G and B are specified for every pixel. The values
// are 8 bits wide. One pixel corresponds to 3 bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGB8: public CLineEngine
{

protected:

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   unsigned char *linebuf;

   unsigned char fgColor[3];

   unsigned char bgColor[3];

public:

   CLineEngRGB8();

   ~CLineEngRGB8();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here R, G and B are specified for every pixel. The values
// are 10 bits wide. One pixel corresponds to 4 bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGB10: public CLineEngine
{

private:

   unsigned int
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   unsigned int *linebuf;

   unsigned int fgColor;

   unsigned int bgColor;

public:

   CLineEngRGB10();

   ~CLineEngRGB10();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here R, G and B are specified for every pixel. The values
// are 16 bits wide. One pixel corresponds to 6 bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGB16: public CLineEngine
{

protected:

   unsigned short
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   unsigned short *linebuf;

   unsigned short fgColor[3];

   unsigned short bgColor[3];

public:

   CLineEngRGB16();

   ~CLineEngRGB16();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   virtual void setFGColor(int col);

   virtual void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   virtual void setBGColor(int col);

   virtual void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

class MTI_FORMATDLL_API CLineEngRGB16BE: public CLineEngRGB16
{

public:

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

};

class MTI_FORMATDLL_API CLineEngRGB16LE: public CLineEngRGB16
{

public:

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

};

///////////////////////////////////////////////////////////////
//
// Here R, G, B and A are specified for every pixel. The values
// are 16 bits wide. One pixel corresponds to 8 bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGBA16: public CLineEngine
{

protected:

   unsigned short
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   unsigned short *linebuf;

   unsigned short fgColor[3];

   unsigned short bgColor[3];

public:

   CLineEngRGBA16();

   ~CLineEngRGBA16();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   virtual void setFGColor(int col);

   virtual void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   virtual void setBGColor(int col);

   virtual void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

class MTI_FORMATDLL_API CLineEngRGBA16BE: public CLineEngRGBA16
{

public:

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

};

class MTI_FORMATDLL_API CLineEngRGBA16LE: public CLineEngRGBA16
{

public:

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

};

class MTI_FORMATDLL_API CLineEngRGBAHalf: public CLineEngRGBA16
{

public:

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

};

class MTI_FORMATDLL_API CLineEngYUV16: public CLineEngRGB16
{

public:

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

};

///////////////////////////////////////////////////////////////
//
// Here B, R and G are specified for every pixel. The values
// are 8 bits wide. One pixel corresponds to 3 bytes.
//
//  This is used for TARGA images
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngBGR8: public CLineEngRGB8
{
public:

   CLineEngBGR8();

   ~CLineEngBGR8();

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);
};

///////////////////////////////////////////////////////////////
//
// Here the pattern of packing repeats for every three pixels.
// Three values are packed in 30 bits of a 32-bit DWORD, in BE
// style, with the unused bits at the lo end. Four DWORDS rep-
// resent RGB:ARG:BAR:GBA.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGBA10: public CLineEngine
{

private:

   int dphase;

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int rgbphase;

   unsigned char *linebuf;

   unsigned int fgColor[3];

   unsigned int bgColor[3];

   inline void setFGY0();
   inline void setFGY1();
   inline void setFGY2();

   inline void setBGY0();
   inline void setBGY1();
   inline void setBGY2();

public:

   CLineEngRGBA10();

   ~CLineEngRGBA10();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here we have a single channel per pixel. The values are 10
// bits wide, packed 3 at a time into a 32-bit big-endian int.
// The lowest two bits of the big-endian are unused. Three
// pixels correspond to 4 bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngMON10: public CLineEngine
{

private:

   int dphase;

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int phase;

   unsigned char *linebuf;

   unsigned char fgColor[6];

   unsigned char bgColor[6];

   inline void setFGY0();
   inline void setFGY1();
   inline void setFGY2();

   inline void setBGY0();
   inline void setBGY1();
   inline void setBGY2();

public:

   CLineEngMON10();

   ~CLineEngMON10();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here we have a single channel per pixel. The values are 10
// bits wide, packed 4 at a time into five successive bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngMON10P: public CLineEngine
{

private:

   int dphase;

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int phase;

   unsigned char *linebuf;

   unsigned char fgColor[8];

   unsigned char bgColor[8];

   inline void setFGY0();
   inline void setFGY1();
   inline void setFGY2();
   inline void setFGY3();

   inline void setBGY0();
   inline void setBGY1();
   inline void setBGY2();
   inline void setBGY3();

public:

   CLineEngMON10P();

   ~CLineEngMON10P();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here Y is specified for every pixel. The values
// are 16 bits wide. One pixel corresponds to 2 bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngMON16: public CLineEngine
{
protected:

   unsigned short *internalFrameBuffer = nullptr;
   unsigned short *targetFrameBuffer = nullptr;
   unsigned short *curptr = nullptr;;
   unsigned short *linebuf = nullptr;;
   unsigned short fgColor = 0;
   unsigned short bgColor = 0xFFFF;

public:

   CLineEngMON16();
   ~CLineEngMON16();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);
   int allocateInternalFrameBuffer();
   void setExternalFrameBuffer(unsigned char **);
   virtual void setFGColor(int col);
   virtual void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);
   virtual void setBGColor(int col);
   virtual void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);
   void linTo(int x, int y);
   void drawDot();
   void drawSpot(int x, int y);
   void drawRectangle(int l, int b, int r, int t);
   void drawNumeral(int x, int y, int num);
};

class MTI_FORMATDLL_API CLineEngMON16BE: public CLineEngMON16
{
public:

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);
   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);
};

class MTI_FORMATDLL_API CLineEngMON16LE: public CLineEngMON16
{

public:

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);
   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);
};

///////////////////////////////////////////////////////////////
//
// Here U, Y and V are specified for every pixel. The values
// are 8 bits wide. One pixel corresponds to 3 bytes.
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngUYV8: public CLineEngine
{

protected:

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   unsigned char *linebuf;

   unsigned char fgColor[3];

   unsigned char bgColor[3];

public:

   CLineEngUYV8();

   ~CLineEngUYV8();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here U, Y and V are specified for every pixel. The values
// are 10 bits wide. One pixel corresponds to 4 bytes.
//
//        29        19         9
//      --------------------------------
//      | |    V    |    Y    |    U   |   4 bytes
//      -------------------------------- 
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngUYV10: public CLineEngine
{

private:

   unsigned int
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   unsigned int *linebuf;

   unsigned int fgColor;

   unsigned int bgColor;

public:

   CLineEngUYV10();

   ~CLineEngUYV10();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 u, MTI_UINT16 v, MTI_UINT16 y);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here R, G and B are specified for every pixel. The values
// are 12 bits wide. The pattern repeats every 8 pixels =
// (36 bytes).
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGB12: public CLineEngine
{

protected:

   int dphase;

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int rgbphase;

   unsigned short *linebuf;

   unsigned short fgColor[3];

   unsigned short bgColor[3];

   inline void setFGY0();
   inline void setFGY1();
   inline void setFGY2();
   inline void setFGY3();
   inline void setFGY4();
   inline void setFGY5();
   inline void setFGY6();
   inline void setFGY7();

   inline void setBGY0();
   inline void setBGY1();
   inline void setBGY2();
   inline void setBGY3();
   inline void setBGY4();
   inline void setBGY5();
   inline void setBGY6();
   inline void setBGY7();

public:

   CLineEngRGB12();

   ~CLineEngRGB12();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   virtual void setFGColor(int col);

   virtual void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   virtual void setBGColor(int col);

   virtual void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here R, G and B are specified for every pixel. The values
// are 12 bits wide. The pattern repeats every 8 pixels =
// (36 bytes).
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGB12LR: public CLineEngine
{

protected:

   int dphase;

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int rgbphase;

   unsigned short *linebuf;

   unsigned short fgColor[3];

   unsigned short bgColor[3];

   inline void setFGY0();
   inline void setFGY1();
   inline void setFGY2();
   inline void setFGY3();
   inline void setFGY4();
   inline void setFGY5();
   inline void setFGY6();
   inline void setFGY7();

   inline void setBGY0();
   inline void setBGY1();
   inline void setBGY2();
   inline void setBGY3();
   inline void setBGY4();
   inline void setBGY5();
   inline void setBGY6();
   inline void setBGY7();

public:

   CLineEngRGB12LR();

   ~CLineEngRGB12LR();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   virtual void setFGColor(int col);

   virtual void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   virtual void setBGColor(int col);

   virtual void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

///////////////////////////////////////////////////////////////
//
// Here R, G and B are specified for every pixel. The values
// are 12 bits wide. The pattern repeats every 8 pixels =
// (36 bytes).
//
///////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGB12P: public CLineEngine
{

protected:

   int dphase;

   unsigned char
      *internalFrameBuffer[2],
      *frameBuffer[2],
      *curptr;

   int rgbphase;

   unsigned short *linebuf;

   unsigned short fgColor[3];

   unsigned short bgColor[3];

   inline void setFGY0();
   inline void setFGY1();
   inline void setFGY2();
   inline void setFGY3();
   inline void setFGY4();
   inline void setFGY5();
   inline void setFGY6();
   inline void setFGY7();

   inline void setBGY0();
   inline void setBGY1();
   inline void setBGY2();
   inline void setBGY3();
   inline void setBGY4();
   inline void setBGY5();
   inline void setBGY6();
   inline void setBGY7();

public:

   CLineEngRGB12P();

   ~CLineEngRGB12P();

   void setFrameBufferDimensions(int w, int h, int pitch, bool il);

   int allocateInternalFrameBuffer();

   void setExternalFrameBuffer(unsigned char **);

   virtual void setFGColor(int col);

   virtual void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   virtual void setBGColor(int col);

   virtual void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void movTo(int x, int y);

   void linTo(int x, int y);

   void drawDot();

   void drawSpot(int x, int y);

   void drawRectangle(int l, int b, int r, int t);

   void drawNumeral(int x, int y, int num);

};

/////////////////////////////////////////////////////////////////
//
// Here R, G and B are specified for every pixel. The values are
// 12 bits wide, left-justified in 16-bit little-endian words
//
/////////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGB12LLE: public CLineEngRGB16
{

public:

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

};

/////////////////////////////////////////////////////////////////
//
// Here R, G and B are specified for every pixel. The values are
// 12 bits wide, left-justified in 16-bit big-endian words
//
/////////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CLineEngRGB12LBE: public CLineEngRGB16
{

public:

   void setFGColor(int col);

   void setFGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

   void setBGColor(int col);

   void setBGColor(MTI_UINT16 r, MTI_UINT16 g, MTI_UINT16 b);

};


#endif

