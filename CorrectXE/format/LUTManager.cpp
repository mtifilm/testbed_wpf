/*
	File:	LUTManager.cpp
	By:	Kevin Manbeck
	Date:	December 2003

	The LUT Manager contains a static list of LUTs for converting
	image data to an 8-bit display.

	A LUT must be allocated and released by the calling application.
	A LUT which has been released by the caller may remain in RAM
	in case the caller needs it again later.

*/

#include "LUTManager.h"
#include "ColorSpaceConvert.h"
#include "err_format.h"
#include "HalfDisplayLut.h"
#include "HRTimer.h"
#include "IniFile.h"

// Initialize the static data.  The vectLUT is a list of display lookup
// tables.  After the caller releases a LUT, it is maintained for a
// period of time incase the caller needs it again.  When the program
// terminates, all remaining LUTs are released
vector <CDisplayLUT *> CLUTManager::vectLUT;


/*
 *
 *	The DisplayFormat class.  Used to describe the display characteristics
 *
 */

CDisplayFormat::CDisplayFormat ()
{
  fDisplayGamma = 1.;
  for (int iComp = 0; iComp < MAX_COMPONENT_COUNT; iComp++)
   {
    faDisplayMin[iComp] = 0.;
    faDisplayMax[iComp] = 1.;
   }

  //iSourceColorSpace = IF_COLOR_SPACE_LINEAR; - vble removed

  bSourceLogSpace = false;
}  /* CDisplayFormat */

CDisplayFormat::~CDisplayFormat ()
{
}  /* ~CDisplayFormat */

/*
 *
 *	The CLUT class.  Used to allocate and initialize display lookup tables.
 *	Also used to convert image data to displayable form.
 *
 */

static void calculateYUVLUT(void *v, int iJob);

CDisplayLUT::CDisplayLUT ()
:nStripes(1)  // Easier to debug than 2!
{
  ucpLUT = 0;
  iUsageCount = 0;
  iStaleCount = 0;
  iLUTType = TYPE_NONE;

  // Determine the degree of multithreading
  // Removed because there really is no point to multithreading this!

  // allocate a color space converter for each mthread
  for (int i=0;i<nStripes;i++) {
     csc[i] = new CColorSpaceConvert;
  }

  // set up the mthread structure
  tsThread.PrimaryFunction   =
     (void (*)(void *,int))&calculateYUVLUT;
  tsThread.SecondaryFunction = NULL;
  tsThread.CallBackFunction  = NULL;
  tsThread.vpApplicationData = this;
  tsThread.iNThread          = nStripes;
  tsThread.iNJob             = nStripes;

  // allocate the mthreads
  if (nStripes > 1) {
     int iRet = MThreadAlloc(&tsThread);
     if (iRet) {
        TRACE_0(errout << "Convert: MThreadAlloc failed, iRet=" << iRet);
     }
  }
}  /* CDisplayLUT */

CDisplayLUT::~CDisplayLUT ()
{

  if (nStripes > 1) {
     int iRet = MThreadFree(&tsThread);
  }

  for (int i=0;i<nStripes;i++)
     delete csc[i];

  free (ucpLUT);
  ucpLUT = 0;
}  /* ~CDisplayLUT */


int CDisplayLUT::Init(const CImageFormat &newImageFormat, const CDisplayFormat &newDisplayFormat)
{
   // make a local copy of the image format
   ifSrc = newImageFormat;

   // make a local copy of the destination format
   dfDst = newDisplayFormat;

   // create a CImageFormat for use by the color space converter
   // note that we have to mess with the frame dimensions in
   // order to make the csc.Init call work properly
   CImageFormat ifDst;
   ifDst.setToDefaults(IF_TYPE_FILE_SGI, IF_PIXEL_PACKING_8Bits_IN_1Byte);
   ifDst.setPixelsPerLine(ifSrc.getPixelsPerLine());
   ifDst.setVerticalIntervalRows(ifSrc.getVerticalIntervalRows());
   ifDst.setLinesPerFrame(ifSrc.getLinesPerFrame());

   // override the min and max values
	MTI_UINT16 componentValuesMin[MAX_COMPONENT_COUNT];
	componentValuesMin[0] = dfDst.faDisplayMin[0] * 255.;
	componentValuesMin[1] = dfDst.faDisplayMin[1] * 255.;
	componentValuesMin[2] = dfDst.faDisplayMin[2] * 255.;
	ifDst.setComponentValueMin(componentValuesMin);

	MTI_UINT16 componentValuesMax[MAX_COMPONENT_COUNT];
	componentValuesMax[0] = dfDst.faDisplayMax[0] * 255.;
	componentValuesMax[1] = dfDst.faDisplayMax[1] * 255.;
	componentValuesMax[2] = dfDst.faDisplayMax[2] * 255.;
	ifDst.setComponentValueMax(componentValuesMax);

   // override the gamma value
   ifDst.setGamma(dfDst.fDisplayGamma);

   // allocate the LUT
   if (ifSrc.getPixelComponents() == IF_PIXEL_COMPONENTS_YUV422
   || ifSrc.getPixelComponents() == IF_PIXEL_COMPONENTS_YUV444)
   {
      // the SRC is in YUV.  We need a 3-D LUT.
      // we use 8 bits for Y and 7 bits for U and V
      ucpLUT = (MTI_UINT8*) malloc(128 * 128 * 256 * 3);
      if (ucpLUT == NULL)
			return FORMAT_ERROR_MALLOC;

      iLUTType          = TYPE_YUV;
      iBitsPerComponent = ifSrc.getBitsPerComponent();

   }
   else
	{
      // Src is in RGB or LUMINANCE.  Just create a 1-D LUT
      EPixelPacking packing = ifSrc.getPixelPacking();
      if (packing == IF_PIXEL_PACKING_8Bits_IN_1Byte)
      {
         ucpLUT = (MTI_UINT8*) malloc(3 * (1 << 8));

         iLUTType = TYPE_8BIT;
      }
      else if (packing == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L
            || packing == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R
            || packing == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L
            || packing == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R
            || packing == IF_PIXEL_PACKING_4_10Bits_IN_5Bytes)
      {
         ucpLUT = (MTI_UINT8*) malloc(3 * (1 << 10));

         iLUTType = TYPE_10BIT;
      }
      else if (packing == IF_PIXEL_PACKING_2_12Bits_IN_3Bytes
            || packing == IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR
            || packing == IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE
            || packing == IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE)
      {
         ucpLUT = (MTI_UINT8*) malloc(3 * (1 << 12));

         iLUTType = TYPE_12BIT;
      }
      else if (packing == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE
            || packing == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE)
      {
         ucpLUT = (MTI_UINT8*) malloc(3 * (1 << 16));
         iLUTType = TYPE_16BIT;
      }
      else if (packing == IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT)
      {
         ucpLUT = (MTI_UINT8*) malloc(3 * (1 << 16));
         iLUTType = TYPE_16BIT_INT;
      }
      else if (packing == IF_PIXEL_PACKING_1_Half_IN_2Bytes)
      {
         ucpLUT = (MTI_UINT8*) malloc(3 * (1 << 16));
         iLUTType = TYPE_HALF;
      }
      else
      {
			return FORMAT_ERROR_BAD_PACKING;
      }

      if (ucpLUT == NULL)
			return FORMAT_ERROR_MALLOC;
	}

	iStaleCount = 0;
	iUsageCount = 1;

	if (iLUTType == TYPE_HALF)
	{
		unsigned short black[3];
		unsigned short white[3];
		double gamma = ifDst.getGamma();
		ifSrc.getComponentValueMin(black);
		ifSrc.getComponentValueMax(white);
		HalfDisplayLut halfLutR(black[0], white[0], componentValuesMin[0], componentValuesMax[0], gamma);
		HalfDisplayLut halfLutG(black[1], white[1], componentValuesMin[1], componentValuesMax[1], gamma);
		HalfDisplayLut halfLutB(black[2], white[2], componentValuesMin[2], componentValuesMax[2], gamma);
		unsigned char *red = ucpLUT;
		unsigned char *green = red + 65536;
		unsigned char *blue = green + 65536;
		halfLutR.FillLut(red);
		halfLutG.FillLut(green);
		halfLutB.FillLut(blue);

		return 0;
	}

	// use the color space converters in the class
	for (int i = 0; i < nStripes; i++)
	{
		int iRet;
		iRet = csc[i]->Init(&ifSrc, &ifDst, NULL, 0, dfDst.bSourceLogSpace);
		if (iRet)
			return iRet;
	}

   // populate the LUT with values
   if (iLUTType == TYPE_YUV)
   {
      if (nStripes == 1)
      {
         // if we're only doing one stripe, call the function directly
         tsThread.PrimaryFunction(this, 0);
      }
      else
      {
         // fire up one thread for each stripe of the LUT table
         int iRet = MThreadStart(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadStart failed, iRet=" << iRet);
         }
      }
   }
   else if (iLUTType == TYPE_8BIT)
   {
      MTI_UINT16 usCnt, usR, usG, usB;
      MTI_UINT8 *ucp = ucpLUT;
      for (usCnt = 0; usCnt < 256; usCnt++)
      {
         csc[0]->ConvertOneValue(usCnt, 0, 0, &usR, &usG, &usB);
         *ucp++ = usR;
      }
      for (usCnt = 0; usCnt < 256; usCnt++)
      {
         csc[0]->ConvertOneValue(0, usCnt, 0, &usR, &usG, &usB);
         *ucp++ = usG;
      }
      for (usCnt = 0; usCnt < 256; usCnt++)
      {
         csc[0]->ConvertOneValue(0, 0, usCnt, &usR, &usG, &usB);
         *ucp++ = usB;
      }
   }
   else if (iLUTType == TYPE_10BIT)
   {
      MTI_UINT16 usCnt, usR, usG, usB;
      MTI_UINT8 *ucp = ucpLUT;
      for (usCnt = 0; usCnt < 256; usCnt++)
      {
         csc[0]->ConvertOneValue(4*usCnt, 0, 0, &usR, &usG, &usB);
         *ucp++ = usR;
      }
      for (usCnt = 0; usCnt < 256; usCnt++)
      {
         csc[0]->ConvertOneValue(0, 4*usCnt, 0, &usR, &usG, &usB);
         *ucp++ = usG;
      }
      for (usCnt = 0; usCnt < 256; usCnt++)
      {
         csc[0]->ConvertOneValue(0, 0, 4*usCnt, &usR, &usG, &usB);
         *ucp++ = usB;
      }
   }
   else if (iLUTType == TYPE_12BIT)
   {
      MTI_UINT16 usCnt, usR, usG, usB;
      MTI_UINT8 *ucp = ucpLUT;
      for (usCnt = 0; usCnt < 256; usCnt++)
      {
         csc[0]->ConvertOneValue(16*usCnt, 0, 0, &usR, &usG, &usB);
         *ucp++ = usR;
      }
      for (usCnt = 0; usCnt < 256; usCnt++)
      {
         csc[0]->ConvertOneValue(0, 16*usCnt, 0, &usR, &usG, &usB);
         *ucp++ = usG;
      }
      for (usCnt = 0; usCnt < 256; usCnt++)
      {
         csc[0]->ConvertOneValue(0, 0, 16*usCnt, &usR, &usG, &usB);
         *ucp++ = usB;
      }
   }
   else if (iLUTType == TYPE_16BIT)
   {
      MTI_UINT8 *ucpLutR = ucpLUT;
      MTI_UINT8 *ucpLutG = ucpLUT + 256;
      MTI_UINT8 *ucpLutB = ucpLUT + 512;
      MTI_UINT16 outR, outG, outB;

      for (MTI_UINT32 index = 0; index < 256; ++index)
      {
         MTI_UINT16 in = (MTI_UINT16)index;
         csc[0]->ConvertOneValue(256 * in, 256 * in, 256 * in, &outR, &outG, &outB);
         ucpLutR[in] = outR,
         ucpLutG[in] = outG,
         ucpLutB[in] = outB;
      }
   }
   else if (iLUTType == TYPE_16BIT_INT)
   {
      // Assumes always RGB->RGB
      MTI_UINT8 *ucpLutR = ucpLUT;
      MTI_UINT8 *ucpLutG = ucpLUT +  65536;
      MTI_UINT8 *ucpLutB = ucpLUT + 131072;
      MTI_UINT16 outR, outG, outB;

      for (MTI_UINT32 index = 0; index < 65536; ++index)
      {
         MTI_UINT16 in = (MTI_UINT16)index;
         csc[0]->ConvertOneValue(in, in, in, &outR, &outG, &outB);
         ucpLutR[in] = outR,
         ucpLutG[in] = outG,
         ucpLutB[in] = outB;
      }
	}

   return 0;
} /* Init */

// calculates one section of the LUT table
// based on the index iJob (typ = 0, 1)
//
static void calculateYUVLUT(void *v, int iJob)
{
   CDisplayLUT *vp = (CDisplayLUT *) v;

   int uentries = 256 / vp->nStripes;
   int usUbeg = uentries * iJob;
   int usUend = usUbeg + uentries;

   MTI_UINT8 *ucp = vp->ucpLUT + iJob*uentries*128*128*3;

   MTI_UINT16 usY, usU, usV, usR, usG, usB;

   // The value of iCoarsen must be a power of 2
   int iCoarsen = 8;

   // when our source image format is 10-bit we'll
   // want to adjust the arguments to ConvertOneValue
   // by shifting left by 4, transforming the 0-256
   // range to 0-1024
   int iShft = vp->iBitsPerComponent - 8;

   float fA0, fA1, fA2,
         fB0, fB1, fB2,
         fDelta0, fDelta1, fDelta2;

   int iRefine;

   // 7 bits for U and V, 8 bits for Y. We use
   // linear interpolation between Y entries to
   // improve the speed of the LUT calculation.
   // Note how iShft is used.
   for (usU = usUbeg; usU < usUend; usU += 2) {

      for (usV = 0; usV < 256; usV += 2) {

         // prime the usY pipeline with the first usR, usG, and usB
         vp->csc[iJob]->ConvertOneValue (0, (usU<<iShft), (usV<<iShft), &usR, &usG, &usB);

         for (usY = iCoarsen; usY < 256 ; usY += iCoarsen) { // coarse pass

#ifdef __sgi
            ucp[0] = usR;
            ucp[1] = usG;
            ucp[2] = usB;
#endif

#ifdef _WINDOWS
            ucp[0] = usB;
            ucp[1] = usG;
            ucp[2] = usR;
#endif
            ucp += 3;

            fA0 = usR;
            fA1 = usG;
            fA2 = usB;

            vp->csc[iJob]->ConvertOneValue ((usY<<iShft), (usU<<iShft), (usV<<iShft), &usR, &usG, &usB);

            fB0 = usR;
            fB1 = usG;
            fB2 = usB;

            fDelta0 = (fB0-fA0) / (float)iCoarsen;
            fDelta1 = (fB1-fA1) / (float)iCoarsen;
            fDelta2 = (fB2-fA2) / (float)iCoarsen;

            for (iRefine=1; iRefine < iCoarsen; iRefine++) {

               fA0 += fDelta0;
               fA1 += fDelta1;
               fA2 += fDelta2;

#ifdef __sgi
               ucp[0] = fA0 + .5;
               ucp[1] = fA1 + .5;
               ucp[2] = fA2 + .5;
#endif

#ifdef _WINDOWS
               ucp[0] = fA2 + .5;
               ucp[1] = fA1 + .5;
               ucp[2] = fA0 + .5;
#endif
               ucp += 3;
            }
         }

         for (usY = 256 - iCoarsen; usY < 256; usY++) {

            vp->csc[iJob]->ConvertOneValue ((usY<<iShft), (usU<<iShft), (usV<<iShft), &usR, &usG, &usB);

#ifdef __sgi
            ucp[0] = usR;
            ucp[1] = usG;
            ucp[2] = usB;
#endif

#ifdef _WINDOWS
            ucp[0] = usB;
            ucp[1] = usG;
            ucp[2] = usR;
#endif
            ucp += 3;
         }
      }
   }
}

bool CDisplayLUT::IsEqual (const CImageFormat &newImageFormat,
		const CDisplayFormat &newDisplayFormat)
{
  // This function determines if the parameters in newImageFormat and
  // newDisplayFormat are equivalent to the parameters used to generate
  // "this" CDisplayLUT

  // examine the CImageFormat
  if (newImageFormat.getPixelComponents() != ifSrc.getPixelComponents())
    return false;

  // note:  ImageFormat color space comes from iSourceColorSpace -
  // not any more - it's already set before the call

  MTI_UINT16 oldMin[MAX_COMPONENT_COUNT], newMin[MAX_COMPONENT_COUNT];
  MTI_UINT16 oldMax[MAX_COMPONENT_COUNT], newMax[MAX_COMPONENT_COUNT];

  ifSrc.getComponentValueMin (oldMin);
  newImageFormat.getComponentValueMin (newMin);
  ifSrc.getComponentValueMax (oldMax);
  newImageFormat.getComponentValueMax (newMax);
  for (int iComp = 0; iComp < MAX_COMPONENT_COUNT; iComp++)
   {
    if (newMin[iComp] != oldMin[iComp])
      return false;
    if (newMax[iComp] != oldMax[iComp])
      return false;
   }

  if (newImageFormat.getPixelPacking() != ifSrc.getPixelPacking())
    return false;

  if (newImageFormat.getGamma() != ifSrc.getGamma())
    return false;


  // examine the CDisplayFormat
  if (newDisplayFormat.fDisplayGamma != dfDst.fDisplayGamma)
    return false;

  for (int iComp = 0; iComp < MAX_COMPONENT_COUNT; iComp++)
   {
    if (newDisplayFormat.faDisplayMin[iComp] != dfDst.faDisplayMin[iComp])
      return false;

    if (newDisplayFormat.faDisplayMax[iComp] != dfDst.faDisplayMax[iComp])
      return false;
   }

  // the iSourceColorSpace has been removed from CDisplayFormat
  //if (newDisplayFormat.iSourceColorSpace != dfDst.iSourceColorSpace)
  //  return false;

  if (newDisplayFormat.bSourceLogSpace != dfDst.bSourceLogSpace)
    return false;

  return true;
}  /* IsEqual */

MTI_UINT8 *CDisplayLUT::getTablePtr ()
{
  return ucpLUT;
}  /* getTablePtr */

/*
 *
 *	The LUTManager class.  Used to keep track of all allocated
 *	DisplayLUTs
 *
 */

CLUTManager::CLUTManager ()
{
}  /* CLUTManager */

CLUTManager::~CLUTManager ()
{
}  /* ~CLUTManager */

CDisplayLUT *CLUTManager::Create (const CImageFormat &newImageFormat,
		const CDisplayFormat &newDisplayFormat)
{
  // run through the DisplayLUTs in the list and see if we have a match
  vector <CDisplayLUT *>::iterator pos;
  for (pos = vectLUT.begin(); pos != vectLUT.end(); pos++)
   {
    if ((*pos)->IsEqual (newImageFormat, newDisplayFormat))
     {
      break;  // out of for loop
     }
   }

  if (pos != vectLUT.end())
   {
    // found a match.  Increment the count and return
    (*pos)->iUsageCount++;
    return (*pos);
   }
  else
   {
    // before we allocate a new LUT, see if we can delete some stale ones
    ControlStale ();

    // must allocate a new one
    CDisplayLUT *dlp = new CDisplayLUT;
	 int iRet = dlp->Init (newImageFormat, newDisplayFormat);
    if (iRet)
     {
      delete dlp;
      return 0;
     }

    vectLUT.push_back (dlp);

    return dlp;
   }

}  /* Create */

void CLUTManager::Release (const CDisplayLUT *lut)
{
  // run through the DisplayLUTs in the list and release the specified lut
  vector <CDisplayLUT *>::iterator pos;
  for (pos = vectLUT.begin(); pos != vectLUT.end(); pos++)
   {
    if ((*pos) == lut)
     {
      break;  // out of for loop
     }
   }

  if (pos != vectLUT.end())
   {
    // found the specified LUT
    (*pos)->iUsageCount--;
   }

  return;
}  /* Release */

void CLUTManager::ControlStale ()
{
  // increment the stale count
  IncrementStaleCount();

  // remove stale LUTs
  while (RemoveStale ());

  return;
}  /* ControlStale */

void CLUTManager::IncrementStaleCount ()
{
  // run through the DisplayLUTs in the list and increment the stale count
  vector <CDisplayLUT *>::iterator pos;
  for (pos = vectLUT.begin(); pos != vectLUT.end(); pos++)
   {
    if ((*pos)->iUsageCount <= 0)
     {
      (*pos)->iStaleCount++;
     }
   }

  return;
}  /* IncrementStaleCount */

bool CLUTManager::RemoveStale ()
{
  // run through the DisplayLUTs remove any stale LUT
  vector <CDisplayLUT *>::iterator pos;
  for (pos = vectLUT.begin(); pos != vectLUT.end(); pos++)
   {
    if ((*pos)->iStaleCount > 4)
     {
      delete (*pos);
      vectLUT.erase (pos);
      return true;
     }
   }

  return false;
}  /* RemoveStale */

void CLUTManager::Destroy ()
{
  // run through the DisplayLUTs in the list and delete them
  vector <CDisplayLUT *>::iterator pos, posStale;
  for (pos = vectLUT.begin(); pos != vectLUT.end(); pos++)
   {
    delete (*pos);
   }

  vectLUT.clear();

  return;
}  /* Destroy */
