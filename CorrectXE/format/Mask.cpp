#include "Mask.h"
#include "MTImalloc.h"
#include <stdio.h>

// edge orientations:
// 0 - horizontal to the right
// 1 - less than 45 deg below the horizon to the right
// 2 - 45 deg below horizon to the right
// 3 - more than 45 deg below the horizon to the right
// 4 - vertical down
// 5 - more than 45 deg below the horizon to the left
// 6 - 45 deg below horizon to the left
// 7 - less than 45 deg below the horizon to the left

CMask::CMask()
{
   // dispatch table for stretch methods
   // indexed by edge orientation (edgCod)
   strTbl[0] = CMask::StretchXEdge0;
   strTbl[1] = CMask::StretchXEdge1;
   strTbl[2] = CMask::StretchXEdge2;
   strTbl[3] = CMask::StretchXEdge3;
   strTbl[4] = CMask::StretchXEdge4;
   strTbl[5] = CMask::StretchXEdge5;
   strTbl[6] = CMask::StretchXEdge6;
   strTbl[7] = CMask::StretchXEdge7;

   // dispatch table for step methods
   // indexed by edge orientation (edgCod)
   stpTbl[0] = CMask::StepYEdge0;
   stpTbl[1] = CMask::StepYEdge1;
   stpTbl[2] = CMask::StepYEdge2;
   stpTbl[3] = CMask::StepYEdge3;
   stpTbl[4] = CMask::StepYEdge4;
   stpTbl[5] = CMask::StepYEdge5;
   stpTbl[6] = CMask::StepYEdge6;
   stpTbl[7] = CMask::StepYEdge7;

   // allocate buffer for edge records
   edgcnt = EDGES_INIT;
   edgbuf = (EDGE *)malloc(EDGES_INIT*sizeof(EDGE));  // Was MTImalloc
   if (edgbuf==NULL) edgcnt = 0;

   // single scan-line buffer
   scnbuf = NULL;
}

CMask::~CMask()
{
   delete [] scnbuf;

   // free the edge buffer
   if (edgbuf!=NULL) free(edgbuf); // was MTIfree(edgbuf);
}

// attaches a mask buffer to the engine
void CMask::initMask(int wd, int ht, MTI_UINT16 *frm)
{
   // dimensions in pels
   wdth = wd;
   hght = ht;
   dpitch = wd;

   // set the frame buffer
   frmbuf = frm;

   // init active region to the whole mask
   region.left   = 0;
   region.top    = 0;
   region.right  = wdth-1;
   region.bottom = hght-1;
}

// here UL is inclusive, LR is exclusive
void CMask::setActiveRegion(int lft, int top,
                            int rgt, int bot)
{
   region.left   = lft;
   region.top    = top;
   region.right  = rgt-1;
   region.bottom = bot-1;
}

void CMask::setDrawColor(MTI_UINT16 col)
{
   fcolr = col;
}

// fills the active region with the draw color
void CMask::fillActiveRegion()
{
   MTI_UINT16 *dst = region.top * dpitch + region.left + frmbuf;
   int rowinc = dpitch - (region.right - region.left + 1);
   for (int i=0;i<(region.bottom-region.top+1);i++) {
      for (int j=0;j<(region.right - region.left + 1);j++) {
         *dst++ = fcolr;
      }
      dst += rowinc;
   }
}

void CMask::drawRectangle(int lft,int top,int rgt,int bot)
{
   if (lft < 0) lft = 0;
   if (top < 0) top = 0;
   if (rgt > (wdth-1)) rgt = wdth-1;
   if (bot > (hght-1)) bot = hght-1;

   MTI_UINT16 *dst = top * dpitch + lft + frmbuf;
   int rowinc = dpitch - (rgt - lft + 1);
   for (int i=0;i<(bot - top + 1);i++) {
      for (int j=0;j<(rgt - lft + 1);j++) {
         *dst++ = fcolr;
      }
      dst += rowinc;
   }
}

void CMask::moveTo(int x, int y)
{
   curx = x;
   cury = y;
   curptr = y*dpitch + x + frmbuf;
}

void CMask::drawDot()
{
   *curptr = fcolr;
}

void CMask::drawDot(int x, int y)
{
   curx = x;
   cury = y;
   curptr = y*dpitch + x + frmbuf;
   *curptr = fcolr;
}

void CMask::lineTo(int x, int y)
{
   int delx, dely;
    int brx,bry,pen;

   if ((dely=y-cury)==0) { // horz
      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {
            *curptr = fcolr;
            curptr++;
         }
      }
      else { // lft
         while (delx++!=0) {
            *curptr = fcolr;
            curptr--;
         }
      }
   }
   else {
      if ((delx=x-curx)==0) { // vert
         if ((dely=y-cury) > 0) { // dn
            while (dely--!=0) {
               *curptr = fcolr;
               curptr += dpitch;
            }
         }
         else { // up
            while (dely++!=0) {
               *curptr = fcolr;
               curptr -= dpitch;
            }
         }
      }
      else { // now both deltas are non-zero
         if (dely > 0) { // dn
            if (delx > 0) { // rgt
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {
                     *curptr = fcolr;
                     curptr++;
                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        curptr += dpitch;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {
                     *curptr = fcolr;
                     curptr += dpitch;
                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr++;
                     }
                  }
               }
            }
            else { // lft
               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {
                     *curptr = fcolr;
                     curptr--;
                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        curptr += dpitch;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {
                     *curptr = fcolr;
                     curptr += dpitch;
                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curptr--;
                     }
                  }
               }
            }
         }
         else { // up
            dely = -dely;
            if (delx > 0) { // rgt
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {
                     *curptr = fcolr;
                     curptr++;
                     if ((pen-=bry) < 0) {
                        pen += brx;
                        curptr -= dpitch;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {
                     *curptr = fcolr;
                     curptr -= dpitch;
                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr++;
                     }
                  }
               }
            }
            else { // up-lft
               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {
                     *curptr = fcolr;
                     curptr--;
                     if ((pen-=bry) < 0) {
                        pen += brx;
                        curptr -= dpitch;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {
                     *curptr = fcolr;
                     curptr -= dpitch;
                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curptr--;
                     }
                  }
               }
            }
         }
      }
   }

   // update CP
   curx = x;
   cury = y;
}

int CMask::getTotalEdges(POINT *lasso)
{
   for (int i=1;i<MAX_LASSO_PTS;i++) {

      if ((lasso[i].x == lasso[0].x)&&
          (lasso[i].y == lasso[0].y))
         return i;
   }
   return 0;
}

int CMask::getTotalChords(BEZIER_POINT *bezier, int chordleng)
{
   int delx, dely;
   int segmentLength;
   int totalChords = 0;

   // Estimate the length of each spline segment by
   // summing the absolute deltas from attach pt to
   // ctrl pt to ctrl pt to attach pt. Divide this
   // value by the chord length supplied as arg to
   // get the number of chords which will be used
   // by the spline fill algorithm to outline the
   // spline. (this works just like ROIMask).

   for (int i=0;;i++) {

      segmentLength = 0;
      if ((delx = bezier[i].xctrlnxt - bezier[i].x)<0) delx = -delx;
      if ((dely = bezier[i].yctrlnxt - bezier[i].y)<0) dely = -dely;
      segmentLength += (delx + dely);
      if ((delx = bezier[i+1].xctrlpre - bezier[i].xctrlnxt)<0) delx = -delx;
      if ((dely = bezier[i+1].yctrlpre - bezier[i].yctrlnxt)<0) dely = -dely;
      segmentLength += (delx + dely);
      if ((delx = bezier[i+1].x - bezier[i+1].xctrlpre)<0) delx = -delx;
      if ((dely = bezier[i+1].y - bezier[i+1].yctrlpre)<0) dely = -dely;
      segmentLength += (delx + dely);

      totalChords += segmentLength/chordleng;

      if ((i>0)&&
          (bezier[i+1].xctrlpre == bezier[0].xctrlpre)&&
          (bezier[i+1].yctrlpre == bezier[0].yctrlpre)&&
          (bezier[i+1].x        == bezier[0].x       )&&
          (bezier[i+1].y        == bezier[0].y       )&&
          (bezier[i+1].xctrlnxt == bezier[0].xctrlnxt)&&
          (bezier[i+1].yctrlnxt == bezier[0].yctrlnxt))
         break;
   }

   return(totalChords);
}

int CMask::initFill(int edges)
{
   // we'll be DRAWING runs into the MASK
   hatchMode = DRAW_RUN;

   // remove single scan-line buffer
   delete[] scnbuf;
   scnbuf = NULL;

   if (edges > edgcnt) { // edge-buffer not big enough, need to reallocate

      if (edgbuf!=NULL) free(edgbuf);          // Was MTIfree
      edgcnt = 0;
      edgbuf = (EDGE *)malloc(edges*sizeof(EDGE));  // was MTImalloc
      if (edgbuf == NULL) return MASK_ERR_INSUFF_EDGES;
      edgcnt = edges;
   }

   iedgptr = 0;
   return 0;

}


int CMask::initRun(MTI_UINT16 *dstrun, const MTI_UINT16 *srcfrm,
                    int srcpitch, int srccomponents,
                    int edges)
{
   // we'll be GENERATING runs for PIXEL REGIONS
   hatchMode = GRAB_RUN;
   endOfRun  = dstrun;
   srcOfRuns = srcfrm;
   dpitch = srcpitch;
   pxlComponentCount = srccomponents;

   // allocate a new single scan-line buffer
   delete[] scnbuf;
   scnbuf = new unsigned char[srcpitch];

   if (edges > edgcnt) { // edge-buffer not big enough, need to reallocate

      if (edgbuf!=NULL) free(edgbuf);                // was MTIfree
      edgcnt = 0;
      edgbuf = (EDGE *) malloc(edges*sizeof(EDGE));   // was MTImalloc
      if (edgbuf == NULL) return MASK_ERR_INSUFF_EDGES;
      edgcnt = edges;
   }

   iedgptr = 0;
   return 0;
}

void CMask::moveToFill(int x, int y)
{
   curx = x;
   cury = y;
}

#if 0
void CMask::lineToFill(int x, int y)
{
   int delx,dely;

   if (y==cury) { // horz

      edgptr->ybot = edgptr->ytop = y;
      if (x >= curx) {
         edgptr->xbot = curx;
         edgptr->xtop = x;
      }
      else {
         edgptr->xbot = x;
         edgptr->xtop = curx;
      }
   }
   else {
      if (y>cury) {
         edgptr->xbot = curx;
         edgptr->ybot = cury;
         edgptr->xtop = x;
         edgptr->ytop = y;
      }
      else {
         edgptr->xbot = x;
         edgptr->ybot = y;
         edgptr->xtop = curx;
         edgptr->ytop = cury;
      }
   }

   // before the first stretch, init XMIN & XMAX this
   edgptr->xmin = edgptr->xmax = edgptr->xbot;


   dely=edgptr->ytop-edgptr->ybot;
   if (dely==0) { // horizontal

      edgptr->StretchX = CMask::StretchXLin0; // orientation 0
      edgptr->StepY    = CMask::StepYLin0;
   }
   else { // motion in y
      delx=edgptr->xtop-edgptr->xbot;
      if (delx==0) { // vertical
         edgptr->StretchX = CMask::StretchXLin4;
         edgptr->StepY    = CMask::StepYLin4;
      }
      else {
         if (delx > 0) {
            edgptr->StretchX = CMask::StretchXLin2;
            edgptr->StepY    = CMask::StepYLin2;
            if (delx>dely) {
               edgptr->StretchX = CMask::StretchXLin1;
               edgptr->StepY    = CMask::StepYLin1;
               edgptr->pen = delx;
            }
            else {
               if (delx<dely) {
                  edgptr->StretchX = CMask::StretchXLin3;
                  edgptr->StepY    = CMask::StepYLin3;
                  edgptr->pen = dely;
               }
            }
         }
         else {
            delx = -delx;
            edgptr->StretchX = CMask::StretchXLin6;
            edgptr->StepY    = CMask::StepYLin6;
            if (delx<dely) {
               edgptr->StretchX = CMask::StretchXLin5;
               edgptr->StepY    = CMask::StepYLin5;
               edgptr->pen = dely;
            }
            else {
               if (delx>dely) {
                  edgptr->StretchX = CMask::StretchXLin7;
                  edgptr->StepY    = CMask::StepYLin7;
                  edgptr->pen = delx;
               }
            }
         }

         // Bresenham constants
         edgptr->brx = delx<<1;
         edgptr->bry = dely<<1;
      }
   }

   edgptr++; // advance filling ptr

   // advance CP x and y
   curx = x;
   cury = y;
   return;
}
#endif

void CMask::lineToFill(int x, int y)
{
   int delx,dely;

   if (y==cury) { // horizontal edge

      // ALLOW NULL EDGES - Bugzilla ref #1556
      //if (x==curx) return;

      edgbuf[iedgptr].ybot = edgbuf[iedgptr].ytop = y;
      if (x > curx) {
         edgbuf[iedgptr].xbot = curx;
         edgbuf[iedgptr].xtop = x;
      }
      else {
         edgbuf[iedgptr].xbot = x;
         edgbuf[iedgptr].xtop = curx;
      }
   }
   else { // vertical or oblique edge

      if (y>cury) {
         edgbuf[iedgptr].xbot = curx;
         edgbuf[iedgptr].ybot = cury;
         edgbuf[iedgptr].xtop = x;
         edgbuf[iedgptr].ytop = y;
      }
      else {
         edgbuf[iedgptr].xbot = x;
         edgbuf[iedgptr].ybot = y;
         edgbuf[iedgptr].xtop = curx;
         edgbuf[iedgptr].ytop = cury;
      }
   }

   // before the first stretch, init XMIN & XMAX this
   edgbuf[iedgptr].xmin = edgbuf[iedgptr].xmax = edgbuf[iedgptr].xbot;


   dely=edgbuf[iedgptr].ytop-edgbuf[iedgptr].ybot;
   if (dely==0) { // horizontal
      edgbuf[iedgptr].edgCod = 0;
   }
   else { // motion in y
      delx=edgbuf[iedgptr].xtop-edgbuf[iedgptr].xbot;
      if (delx==0) { // vertical
         edgbuf[iedgptr].edgCod = 4;
      }
      else {
         if (delx > 0) {
            edgbuf[iedgptr].edgCod = 2;
            if (delx>dely) {
               edgbuf[iedgptr].edgCod = 1;
               edgbuf[iedgptr].pen = delx;
            }
            else {
               if (delx<dely) {
                  edgbuf[iedgptr].edgCod = 3;
                  edgbuf[iedgptr].pen = dely;
               }
            }
         }
         else {
            delx = -delx;
            edgbuf[iedgptr].edgCod = 6;
            if (delx<dely) {
               edgbuf[iedgptr].edgCod = 5;
               edgbuf[iedgptr].pen = dely;
            }
            else {
               if (delx>dely) {
                  edgbuf[iedgptr].edgCod = 7;
                  edgbuf[iedgptr].pen = delx;
               }
            }
         }

         // Bresenham constants
         edgbuf[iedgptr].brx = delx<<1;
         edgbuf[iedgptr].bry = dely<<1;
      }
   }

   iedgptr++; // advance filling ptr

   // advance CP x and y
   curx = x;
   cury = y;
   return;
}

// draw a spline segment from (curx, cury) to (x3, y3), using
// (x1, y1)  and ( x2, y2) as the intermediate control points.
// To draw an entire spline, call moveToFill(x0, y0), followed
// by a series of calls to splineToFill, supplying control points
// and attachment points
void CMask::splineToFill(int x1, int y1, // nxt ctrl point, current  vertex
                         int x2, int y2, // pre ctrl point, terminal vertex
                         int x3, int y3, // terminal vertex
                         int chordleng)
{
   int x0 = curx; int y0 = cury;
   int delx, dely;

   // coefficients for X
   double u0 = x0;
   double u1 = 3*(x1 - x0);
   double u2 = 3*(x2 - 2*x1 + x0);
   double u3 = (x3 - 3*x2 + 3*x1 - x0);

   // coefficients for Y
   double v0 = y0;
   double v1 = 3*(y1 - y0);
   double v2 = 3*(y2 - 2*y1 + y0);
   double v3 = (y3 - 3*y2 + 3*y1 - y0);

   // "length" of spline
   int length = 0;
   if ((delx = x1 - x0)<0) delx = -delx;
   length += delx;
   if ((dely = y1 - y0)<0) dely = -dely;
   length += dely;
   if ((delx = x2 - x1)<0) delx = -delx;
   length += delx;
   if ((dely = y2 - y1)<0) dely = -dely;
   length += dely;
   if ((delx = x3 - x2)<0) delx = -delx;
   length += delx;
   if ((dely = y3 - y2)<0) dely = -dely;
   length += dely;

   // number of segments
   int nseg = length / chordleng;

   // must be at least one
   if (nseg == 0) nseg = 1;

   // now draw the spline segments
   double t;
   for (int i=1; i<nseg; i++) {
      t = (double)i/(double)nseg;
      lineToFill((((u3*t+u2)*t+u1)*t+u0)+.5, (((v3*t+v2)*t+v1)*t+v0)+.5);
   }

   // get the last chord
   lineToFill(x3, y3);
}

void CMask::fillPolygon()
{
   EDGINDEX icuredg, ipreedg, inxtedg;
   int rowcov, lft;

   if (iedgptr > 0) { // we have edges in the buffer

      //     S O R T   E D G E S

      // the edges in scan order (increasing y = down)
      ilexlst = iedgeSort(0,iedgptr-1);

      // the edges crossing a scan-line
      iedglst = NULLEDG;

      do {  //     M A I N   L O O P  for scan-lines

         // edge list is empty, advance the scan to next edge
         if (iedglst==NULLEDG) {

            // set CP incl cury
            moveTo(0,edgbuf[ilexlst].ybot);
         }

         // clear the single scan-line buffer for the next line
         if (scnbuf != NULL) {
            for (int i=0;i<dpitch;i++)
               scnbuf[i] = 0;
         }

         //     I N S E R T   P H A S E

         // pull in all the edges which have lower y at this scan=line and
         // insert them into the edge-list in their proper order
         while ((ilexlst!=NULLEDG)&&(edgbuf[ilexlst].ybot==cury)) {

            // stretch the edge to define xmin and xmax at cury
            strTbl[edgbuf[ilexlst].edgCod](&edgbuf[ilexlst]);

            if (edgbuf[ilexlst].ybot==edgbuf[ilexlst].ytop) { // horz edge

               // we draw it but do not insert into the edge-list
               hatchLine(edgbuf[ilexlst].xmin,edgbuf[ilexlst].xmax);

            }
            else {

               // the edge is vertical or oblique. Insert into edge-list
               ipreedg = NULLEDG;
               icuredg = iedglst;
               while ((icuredg!=NULLEDG)&&(edgbuf[ilexlst].xmin>edgbuf[icuredg].xmin)) {
                  ipreedg = icuredg;
                  icuredg = edgbuf[icuredg].ifwd;
               }
               insertEdge(ilexlst,ipreedg,icuredg);
            }

            // advance to the next edge in y-order
            ilexlst = edgbuf[ilexlst].inxtlex;
         }

         //     D R A W   P H A S E

         if (cury >= region.top) { // draw the scan line

            // beginning with the left edge of the mask, trace the edge-list in
            // x-order drawing the hatch lines when the covering is 1
            rowcov = 0;
            lft = region.left;
            icuredg = iedglst;
            while (icuredg!=NULLEDG) {
               if (rowcov) {
                  hatchLine(lft,edgbuf[icuredg].xmax);
               }
               lft = edgbuf[icuredg].xmin;
               rowcov = 1 - rowcov;
               icuredg = edgbuf[icuredg].ifwd;
            }
            if (rowcov) hatchLine(lft,region.right);

         }

         // we're done with this scan-line.
         // if hatchMode == GRAB_RUN
         //    transform single scan-line buffer contents into runs
         if (hatchMode == GRAB_RUN) {

            int k = 0;
            while (k < dpitch) {

               // go fwd to first set byte
               while ((scnbuf[k]==0)&&(k<dpitch)) k++;

               int l = k;
               while ((scnbuf[l]==0xff)&&(l<dpitch)) l++;

               if (l > k) { // we have a run

                  *endOfRun++ = k;
                  *endOfRun++ = cury;
                  *endOfRun++ = (l - k);

                  if (srcOfRuns != NULL) {

                     const MTI_UINT16 *frmptr =
                        srcOfRuns + (cury * dpitch + k) * pxlComponentCount;

                     for (int i=k;i<l;i++) {
                        for (int j=0;j<pxlComponentCount;j++)
                          *endOfRun++ = *frmptr++;
                     }
                  }
               }

               k = l;
            }

         } // end hatchMode == GRAB_RUN

         // done with this scan-line; advance CP's y-value
         curptr += dpitch;

         // if we pass the bot y, we're all done
         if (++cury > region.bottom) iedglst = ilexlst = NULLEDG;

         //     U P D A T E   P H A S E
         //
         // Go through the edge list and update all the edges to
         // reflect their xmin's and xmax's at the next y-level

         icuredg = iedglst;
         while (icuredg!=NULLEDG) {

            inxtedg = edgbuf[icuredg].ifwd;

            // step in y, then stretch in x
            stpTbl[edgbuf[icuredg].edgCod](&edgbuf[icuredg]);
            strTbl[edgbuf[icuredg].edgCod](&edgbuf[icuredg]);

            if ((edgbuf[icuredg].ytop==cury)&&(cury!=region.bottom)) { // TTT - see ROIMask.cpp

               // we're at the end of this edge's relevance
               hatchLine(edgbuf[icuredg].xmin, edgbuf[icuredg].xmax);
               deleteEdge(icuredg);
            }
            else {

               // if the update causes the edge-list to be disordered in x,
               // re-insert the edge into the edge-list in the correct place
               ipreedg = edgbuf[icuredg].ibwd;
               while ((ipreedg!=NULLEDG)&&(edgbuf[ipreedg].xmin > edgbuf[icuredg].xmin))
                  ipreedg = edgbuf[ipreedg].ibwd;
               if (ipreedg!=edgbuf[icuredg].ibwd) { // re-insert into list
                  deleteEdge(icuredg);
                  if (ipreedg==NULLEDG) insertEdge(icuredg,ipreedg,iedglst);
                  else insertEdge(icuredg,ipreedg,edgbuf[ipreedg].ifwd);
               }
            }
            icuredg = inxtedg;
         }

      } while ((ilexlst!=NULLEDG)||(iedglst!=NULLEDG));
   }
}

// edge-node sort using recursive algorithm
EDGINDEX CMask::iedgeSort(EDGINDEX iloptr, EDGINDEX ihiptr)
{
  EDGINDEX imidptr, ichn1, ichn2, itemp, ihead, itail;

  if (iloptr==ihiptr) { // only one node
    edgbuf[iloptr].inxtlex = NULLEDG;
    return iloptr;
  }

  if ((ihiptr - iloptr)==1) { // only two nodes

    if ((edgbuf[iloptr].ybot<edgbuf[ihiptr].ybot)||
       ((edgbuf[iloptr].ybot==edgbuf[ihiptr].ybot)&&
       (edgbuf[iloptr].xbot<=edgbuf[ihiptr].xbot))) {

        edgbuf[iloptr].inxtlex = ihiptr;
        edgbuf[ihiptr].inxtlex = NULLEDG;
        return iloptr;
    }
    else {
        edgbuf[ihiptr].inxtlex = iloptr;
        edgbuf[iloptr].inxtlex = NULLEDG;
        return ihiptr;
    }
  }

  // more than two nodes: split into two halves and sort each separately
  imidptr = (iloptr + (ihiptr - iloptr)/2);
  ichn1 = iedgeSort(iloptr,imidptr);
  ichn2 = iedgeSort(imidptr+1,ihiptr);

  // define the head of the merged chain
  if ((edgbuf[ichn2].ybot<edgbuf[ichn1].ybot)||
     ((edgbuf[ichn2].ybot==edgbuf[ichn1].ybot)&&
     (edgbuf[ichn2].xbot<edgbuf[ichn1].xbot))) {
    itemp = ichn1;
    ichn1 = ichn2;
    ichn2 = itemp;
  }
  ihead = itail = ichn1;
  ichn1 = edgbuf[ichn1].inxtlex;

  // as above, CHN1 is always the chain that advances
  while (ichn1 != NULLEDG) {
    if ((edgbuf[ichn2].ybot<edgbuf[ichn1].ybot)||
       ((edgbuf[ichn2].ybot==edgbuf[ichn1].ybot)&&
       (edgbuf[ichn2].xbot<edgbuf[ichn1].xbot))) {

      itemp = ichn1;
      ichn1 = ichn2;
      ichn2 = itemp;
    }
    edgbuf[itail].inxtlex = ichn1;
    itail = ichn1;
    ichn1 = edgbuf[ichn1].inxtlex;
  }
  edgbuf[itail].inxtlex = ichn2;

  return ihead;
}

// insert an edge into the SCAN LIST
void CMask::insertEdge(EDGINDEX iedg, EDGINDEX ipre, EDGINDEX inxt)
{
  edgbuf[iedg].ibwd = ipre;
  if (ipre!=NULLEDG) {
    edgbuf[ipre].ifwd = iedg;
  }
  else {
    iedglst = iedg;
  }
  edgbuf[iedg].ifwd = inxt;
  if (inxt!=NULLEDG) {
    edgbuf[inxt].ibwd = iedg;
  }
}

// delete an edge from the SCAN LIST
void CMask::deleteEdge(EDGINDEX iedg)
{
  EDGINDEX ipre,inxt;

  ipre = edgbuf[iedg].ibwd;
  inxt = edgbuf[iedg].ifwd;
  if (ipre!=NULLEDG) {
    edgbuf[ipre].ifwd = inxt;
  }
  else {
    iedglst = inxt;
  }
  if (inxt!=NULLEDG) {
    edgbuf[inxt].ibwd = ipre;
  }
}

// draw a hatchline. The y is from the current CP, whose x is 0
void CMask::hatchLine(int lft, int rgt)
{
   if ((cury < region.top)||(cury > region.bottom)) return;
   if (lft < region.left ) lft = region.left;
   if (rgt > region.right) rgt = region.right;
   if (lft > rgt) return;

   if (hatchMode == DRAW_RUN) {

      MTI_UINT16 *dst = curptr + lft;
      for (int i=0;i<(rgt - lft + 1);i++)
         *dst++ = fcolr;
   }
   else { // hatchMode = GRAB_RUN

      // draw the hatchline into the single scan-line buffer
      for (int i=lft;i<=rgt;i++)
         scnbuf[i] = 0xff;
   }
}

// the full sequence for filling a Lasso
//
void CMask::drawLasso(POINT *lasso)
{
   initFill(getTotalEdges(lasso));
   moveToFill(lasso[0].x,lasso[0].y);
   for (int i=1;;i++) {

      lineToFill(lasso[i].x,lasso[i].y);

      if ((lasso[i].x==lasso[0].x)&&
          (lasso[i].y==lasso[0].y))
         break;
   }
   fillPolygon();
}

// the full sequence for filling a Bezier
//
void CMask::drawBezier(BEZIER_POINT *bezier)
{
   int chordleng = 4;
   if (initFill(getTotalChords(bezier, chordleng))==-1)
      return;

   moveToFill(bezier[0].x,bezier[0].y);
   for (int i=1;;i++) {

      splineToFill(bezier[i-1].xctrlnxt, bezier[i-1].yctrlnxt,
                   bezier[i  ].xctrlpre, bezier[i  ].yctrlpre,
                   bezier[i  ].x       , bezier[i  ].y       ,
                   chordleng );

      if ((bezier[i].xctrlpre==bezier[0].xctrlpre)&&
          (bezier[i].yctrlpre==bezier[0].yctrlpre)&&
          (bezier[i].x       ==bezier[0].x       )&&
          (bezier[i].y       ==bezier[0].y       )&&
          (bezier[i].xctrlpre==bezier[0].xctrlpre)&&
          (bezier[i].yctrlpre==bezier[0].yctrlpre))
         break;
   }
   fillPolygon();
}

// after GRABbing runs, you'll want to return the end ptr
MTI_UINT16 *CMask::getEndOfRuns()
{
   return endOfRun;
} 

// code to stretch XMIN and XMAX for a just-set Y-value
void CMask::StretchXEdge0(EDGE *edg)
{
   // for a horizontal edge, XMAX is set to XTOP
   edg->xmax = edg->xtop;
   return;
}

void CMask::StretchXEdge1(EDGE *edg)
{
   // for a shallow edge to lower right, we
   // use as many Bresenham cycles as we need
   while (((edg->pen -= edg->bry)>0)&&(edg->xmax<edg->xtop)) {
      edg->xmax++;
   }
   return;
}

void CMask::StretchXEdge2(EDGE *edg)
{
   // line at 45 edg to lower right
   return;
}

void CMask::StretchXEdge3(EDGE *edg)
{
   // for edges from 45 to 135, XMAX = XMIN
   // hence there is no stretching needed
   return;
}

void CMask::StretchXEdge4(EDGE *edg)
{
   // for edges from 45 to 135, XMAX = XMIN
   // hence there is no stretching needed
   return;
}

void CMask::StretchXEdge5(EDGE *edg)
{
   // for edges from 45 to 135, XMAX = XMIN
   // hence there is no stretching needed
   return;
}

void CMask::StretchXEdge6(EDGE *edg)
{
   // for edges from 45 to 135, XMAX = XMIN
   // hence there is no stretching needed
   return;
}

void CMask::StretchXEdge7(EDGE *edg)
{
   // shallow line to lower left
   while (((edg->pen -= edg->bry)>0)&&(edg->xmin>edg->xtop)) {
      edg->xmin--;
   }
   return;
}

// code to reset XMIN and XMAX for a Bresenham step in Y
void CMask::StepYEdge0(EDGE *edg)
{
   // horizontal lines live for only one scan line
   return;
}

void CMask::StepYEdge1(EDGE *edg)
{
   // shallow line to lower right
   edg->pen += edg->brx;
   edg->xmin = edg->xmax += 1;
   return;
}

void CMask::StepYEdge2(EDGE *edg)
{
   // line at 45 deg to lower right
   edg->xmin = edg->xmax += 1;
   return;
}

void CMask::StepYEdge3(EDGE *edg)
{
   // steep line to lower right
   if ((edg->pen -= edg->brx) <= 0) {
      edg->pen += edg->bry;
      edg->xmin = edg->xmax += 1;
   }
   return;
}

void CMask::StepYEdge4(EDGE *edg)
{
   // vertical line
   return;
}

void CMask::StepYEdge5(EDGE *edg)
{
   // steep line to lower left
   if ((edg->pen -= edg->brx) <= 0) {
      edg->pen += edg->bry;
      edg->xmax = edg->xmin -= 1;
   }
   return;
}

void CMask::StepYEdge6(EDGE *edg)
{
   // 45 deg line to lower left
   edg->xmax = edg->xmin -= 1;
   return;
}

void CMask::StepYEdge7(EDGE *edg)
{
   // shallow line to lower left
   edg->pen += edg->brx;
   edg->xmax = edg->xmin -= 1;
   return;
}

void CMask::setPadRadius(int rad)
{
   padRadius = rad;
   padRadiusSq = rad*rad;
}

void CMask::setEnableColor(MTI_UINT16 enblcol)
{
   // before a pixel is set by padPixel, it
   // is tested to see what value it already
   // contains. If the existing value matches
   // the ENABLE color, the pixel is set to
   // the REPLACE color.
   enableColor = enblcol;
}

void CMask::setReplaceColor(MTI_UINT16 replcol)
{
   // (see above)
   replaceColor = replcol;
}

void CMask::padPixel()
{
   // create the padding rectangle and make sure
   // that it's within the active region of the mask
   RECT pad;
   pad.left = curx - padRadius;
   if (pad.left < region.left)
      pad.left = region.left;
   pad.top = cury - padRadius;
   if (pad.top < region.top)
      pad.top = region.top;
   pad.right = curx + padRadius;
   if (pad.right > region.right)
      pad.right = region.right;
   pad.bottom = cury + padRadius;
   if (pad.bottom > region.bottom)
      pad.bottom = region.bottom;

   for (int y=pad.top; y<=pad.bottom; y++) {

      int delysq = (y-cury)*(y-cury);
      MTI_UINT16 *rowbeg = y*dpitch + frmbuf;

      for (int x=pad.left;x<=pad.right; x++) {

         int delxsq = (x-curx)*(x-curx);
         MTI_UINT16 *ptr = (rowbeg + x);

         if ((delxsq+delysq)<padRadiusSq) {

            if (*ptr==enableColor)
               *ptr = replaceColor;

         } 
      } 
   }
}

void CMask::fattenTo(int x, int y)
{
   int delx, dely;
    int brx,bry,pen;

   if ((dely=y-cury)==0) { // horz
      if ((delx=x-curx) >= 0) { // rgt
         while (delx--!=0) {
            padPixel();
            curx++;
         }
      }
      else { // lft
         while (delx++!=0) {
            padPixel();
            curx--;
         }
      }
   }
   else {
      if ((delx=x-curx)==0) { // vert
         if ((dely=y-cury) > 0) { // dn
            while (dely--!=0) {
               padPixel();
               cury++;

            }
         }
         else { // up
            while (dely++!=0) {
               padPixel();
               cury--;
            }
         }
      }
      else { // now both deltas are non-zero
         if (dely > 0) { // dn
            if (delx > 0) { // rgt
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-rgt shallow
                  pen = delx;
                  while (delx--!=0) {
                     padPixel();
                     curx++;
                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        cury++;
                     }
                  }
               }
               else { // dn-rgt steep
                  pen = dely;
                  while (dely--!=0) {
                     padPixel();
                     cury++;
                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx++;
                     }
                  }
               }
            }
            else { // lft
               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // dn-lft shallow
                  pen = delx;
                  while (delx--!=0) {
                     padPixel();
                     curx--;
                     if ((pen-=bry) <= 0) {
                        pen += brx;
                        cury++;
                     }
                  }
               }
               else { // dn-lft steep
                  pen = dely;
                  while (dely--!=0) {
                     padPixel();
                     cury++;
                     if ((pen-=brx) <= 0) {
                        pen += bry;
                        curx--;
                     }
                  }
               }
            }
         }
         else { // up
            dely = -dely;
            if (delx > 0) { // rgt
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-rgt shallow
                  pen = delx;
                  while (delx--!=0) {
                     padPixel();
                     curx++;
                     if ((pen-=bry) < 0) {
                        pen += brx;
                        cury--;
                     }
                  }
               }
               else { // up-rgt steep
                  pen = dely;
                  while (dely--!=0) {
                     padPixel();
                     cury--;
                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx++;
                     }
                  }
               }
            }
            else { // up-lft
               delx = -delx;
               brx = delx<<1; bry = dely<<1;
               if (brx >= bry) { // up-lft shallow
                  pen = delx;
                  while (delx--!=0) {
                     padPixel();
                     curx--;
                     if ((pen-=bry) < 0) {
                        pen += brx;
                        cury--;
                     }
                  }
               }
               else { // up-lft steep
                  pen = dely;
                  while (dely--!=0) {
                     padPixel();
                     cury--;
                     if ((pen-=brx) < 0) {
                        pen += bry;
                        curx--;
                     }
                  }
               }
            }
         }
      }
   }
}

// method to XOR the mask contents into an existing
// RGBA buffer for display purposes. The input srcrect
// is mapped to the DSTWDTH x DSTHGHT using the same
// scaling algorithm as in CONVERT. This is used for
// visualizing masks owned by the DISPLAYER
void CMask::xorMask(
                     RECT *srcrect,
                     unsigned int *dstbuf,
                     int dstwdth,
                     int dsthght
                   )
{
   int rctwdth = srcrect->right - srcrect->left + 1;
   int rcthght = srcrect->bottom - srcrect->top + 1;

   double xscal = (double)rctwdth / (double) dstwdth;
   double yscal = (double)rcthght / (double) dsthght;

   MTI_UINT16 *toplft = srcrect->top * dpitch + srcrect->left + frmbuf;

   unsigned int *dst = dstbuf;

   for (int i=0;i<dsthght;i++) {

      MTI_UINT16 *srcrow = toplft + ((int)(i*yscal))*dpitch;

      for (int j=0;j<dstwdth;j++) {

         MTI_UINT16 srcw = *(srcrow + (int)(j*xscal));

         switch(srcw) {

            case 1:
               *dst++ ^= 0xff000000;
            break;
 
            case 2:
               *dst++ ^= 0x00ff0000;
            break;

            case 3:
               *dst++ ^= 0x0000ff00;
            break;

            case 4:
               *dst++ ^= 0xffffff00;
            break;

            default:
               dst++;
            break;

         }
      }
   }
}

