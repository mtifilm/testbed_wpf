// ImageInfo.h
//
// Constants and enumeration types for CImageFormat
//
//////////////////////////////////////////////////////////////////////

#ifndef IMAGE_INFO_H
#define IMAGE_INFO_H

#include "formatDLL.h"
#include "machine.h"
#include "VideoInfo.h"

#ifndef AVID_SDK_CONFLICT
#include <vector>
#endif

//////////////////////////////////////////////////////////////////////
// Handy constants for various image format types

// Standard-Definition Frame Aspect Ratio
#define IF_FRAME_ASPECT_RATIO_SD          (4./3.)    // (width/height)
// High-Definition Frame Aspect Ratio
#define IF_FRAME_ASPECT_RATIO_HD          (16./9.)   // (width/height)

// High-Definition Pixel Aspect Ratio
#define IF_PIXEL_ASPECT_RATIO_HD          (1./1.)    // (width/height)


// IF_TYPE_525
#define IF_ACTIVE_ROWS_PER_FRAME_525      486
#define IF_ACTIVE_ROWS_PER_FIELD_525      (IF_ACTIVE_ROWS_PER_FRAME_525/2)
#define IF_PIXELS_PER_ROW_525             720
#define IF_PIXEL_ASPECT_RATIO_525         (9./10.)    // (width/height)

// IF_TYPE_625
#define IF_ACTIVE_ROWS_PER_FRAME_625      576
#define IF_ACTIVE_ROWS_PER_FIELD_625      (IF_ACTIVE_ROWS_PER_FRAME_625/2)
#define IF_PIXELS_PER_ROW_625             720
#define IF_PIXEL_ASPECT_RATIO_625         (16./15.)   // (width/height)

// IF_TYPE_720P_50, IF_TYPE_720P_60
#define IF_ACTIVE_ROWS_PER_FRAME_720P     720
#define IF_ACTIVE_ROWS_PER_FIELD_720P     (IF_ACTIVE_ROWS_PER_FRAME_720P)
#define IF_PIXELS_PER_ROW_720P            1280

// IF_TYPE_1080I_50, IF_TYPE_1080I_60
#define IF_ACTIVE_ROWS_PER_FRAME_1080I    1080
#define IF_ACTIVE_ROWS_PER_FIELD_1080I    (IF_ACTIVE_ROWS_PER_FRAME_1080I/2)
#define IF_PIXELS_PER_ROW_1080I           1920

// IF_TYPE_1080P_24, IF_TYPE_1080P_25
#define IF_ACTIVE_ROWS_PER_FRAME_1080P    1080
#define IF_ACTIVE_ROWS_PER_FIELD_1080P    (IF_ACTIVE_ROWS_PER_FRAME_1080P)
#define IF_PIXELS_PER_ROW_1080P           1920

//////////////////////////////////////////////////////////////////////

// Frame rate adjustment ratio for drop frame
// Handy for calculations such as 59.94 = 
#define IF_DF_FRAME_RATE_RATIO           (1.000/1.001)

//////////////////////////////////////////////////////////////////////

// NB: Enumerator values are explicitly set to make file compatibility
//     easier.  New enumerators should be added with explicit values and
//     not repeat a value.

//////////////////////////////////////////////////////////////////////
// Image Format Type
enum EImageFormatType
{
   IF_TYPE_INVALID = 0,            // Illegal format type

   IF_TYPE_525_5994 = 1,           // 525/59.94     SD Interlaced "NTSC"
   IF_TYPE_625_50 = 2,             // 625/50        SD Interlaced "PAL"
   IF_TYPE_720P_60 = 3,            // 720P/60       HD Progressive
   IF_TYPE_720P_50 = 4,            // 720P/50       HD Progressive
   IF_TYPE_1080I_60 = 5,           // 1080I/60      HD Interlaced
   IF_TYPE_1080I_50 = 6,           // 1080I/50      HD Interlaced
   IF_TYPE_1080P_24 = 7,           // 1080P/24      HD Progressive
   IF_TYPE_1080P_25 = 8,           // 1080P/25      HD Progressive

   IF_TYPE_FILE_DPX = 9,           // SMPTE file format
   IF_TYPE_FILE_CINEON = 10,       // Kodak CINEON file format (similar to DPX)
   IF_TYPE_FILE_TIFF = 11,         // TIFF graphics file format
   IF_TYPE_FILE_SGI = 12,          // Silicon Graphics graphics file format
                                   // NB: DPX, Cineon, TIFF and SGI file formats
                                   //     are typically RGB

   IF_TYPE_525_60 = 13,            // 525/60        SD Interlaced "NTSC"
   IF_TYPE_720P_5994 = 14,         // 720P/59.94    HD Progressive
   IF_TYPE_1080I_5994 = 15,        // 1080I/59.94   HD Interlaced
   IF_TYPE_1080P_2398 = 16,        // 1080P/23.98   HD Progressive
   IF_TYPE_1080PSF_2398 = 17,      // 1080PsF/23.98 HD Progressive, segmented field
   IF_TYPE_1080PSF_24 = 18,        // 1080PsF/24    HD Progressive, segmented field
   IF_TYPE_1080PSF_25 = 19,        // 1080PsF/25    HD Progressive, segmented field

   IF_TYPE_625_4795 = 20,          // 625/47.95     SD Interlaced
   IF_TYPE_625_48 = 21,            // 625/48        SD Interlaced
                                   // NB: 625_4795 and 625_48 are 625 with
                                   //     non-standard frame rates used as proxies
                                   //     with 1080P/23.98 and /24 respectively.
                                   //     These are not valid video I/O formats

   IF_TYPE_525_5994_DVS = 22,      // 525/59.94     SD on DVS Video Card
   IF_TYPE_525_60_DVS = 23,        // 525/60        SD on DVS Video Card
   IF_TYPE_625_4795_DVS = 24,      // 625/47.95     SD on DVS Video Card
   IF_TYPE_625_48_DVS = 25,        // 625/48        SD on DVS Video Card
                                   // NB: 625_4795 and 625_48 are proxy formats
                                   //     and are not valid video formats
                                   //     on the DVS Video Card
   IF_TYPE_625_50_DVS = 26,        // 625/50        SD on DVS Video Card
   IF_TYPE_720P_5994_DVS = 27,     // 720P/59.94    HD on DVS Video Card
   IF_TYPE_720P_50_DVS = 28,       // 720P/50       HD on DVS Video Card
   IF_TYPE_720P_60_DVS = 29,       // 720P/60       HD on DVS Video Card
   IF_TYPE_1080I_50_DVS = 30,      // 1080I/50      HD on DVS Video Cardd
   IF_TYPE_1080I_5994_DVS = 31,    // 1080I/59.94   HD on DVS Video Card
   IF_TYPE_1080I_60_DVS = 32,      // 1080I/60      HD on DVS Video Card
   IF_TYPE_1080P_2398_DVS = 33,    // 1080P/23.98   HD on DVS Video Card
   IF_TYPE_1080P_24_DVS = 34,      // 1080P/24      HD on DVS Video Card
   IF_TYPE_1080P_25_DVS = 35,      // 1080P/25      HD on DVS Video Card
   IF_TYPE_1080PSF_2398_DVS = 36,  // 1080PsF/23.98 HD on DVS Video Card
   IF_TYPE_1080PSF_24_DVS = 37,    // 1080PsF/24    HD on DVS Video Card
   IF_TYPE_1080PSF_25_DVS = 38,    // 1080PsF/25    HD on DVS Video Card

   IF_TYPE_INTERNAL = 39,          // Internal format, typically 16-bit
                                   // progressive, YUV444 or RGB

   IF_TYPE_FILE_TGA = 40,          // Targa file
   IF_TYPE_FILE_TGA2 = 41,         // Targa-2 file
   IF_TYPE_FILE_BMP = 42,         // Windows BMP file

   // "Dual Link" formats from DVS card
   IF_TYPE_2048_1536PSF_24_DVS = 44,
   IF_TYPE_2048_1536P_24_DVS = 45,
   IF_TYPE_2048_1556PSF_1498_DVS = 46,
   IF_TYPE_2048_1556PSF_15_DVS = 47,
   IF_TYPE_2048_1556PSF_24_DVS = 48,
   IF_TYPE_2048_1556P_24_DVS = 49,

   // 525 Proxy Formats - a work in progress
   // Nominally 23.98/24 frames-per-second WITHOUT 3:2 pulldown
   IF_TYPE_525_4795 = 50,          // 525/47.95   SGI 10 bit, SGI or DVS 8-bit
   IF_TYPE_525_48 = 51,            // 525/48      SGI 10 bit, SGI or DVS 8-bit
   IF_TYPE_525_4795_DVS = 52,      // 525/47.95   DVS 10 bit
   IF_TYPE_525_48_DVS = 53,        // 525/48      DVS 10 bit

   // 1080 Progressive
   // Media stored as two fields, odd & even lines, as though it was interlaced
   IF_TYPE_1080PI_2398 = 54,      // 1080P/23.98   SGI 10 bit, SGI or DVS 8-bit
   IF_TYPE_1080PI_24 = 55,        // 1080P/24      SGI 10 bit, SGI or DVS 8-bit
   IF_TYPE_1080PI_25 = 56,        // 1080P/25      SGI 10 bit, SGI or DVS 8-bit
   IF_TYPE_1080PI_2398_DVS = 57,  // 1080P/23.98   DVS 10 bit
   IF_TYPE_1080PI_24_DVS = 58,    // 1080P/24      DVS 10 bit
   IF_TYPE_1080PI_25_DVS = 59,    // 1080P/25      DVS 10 bit

   // 1080 Progressive, Segmented Field
   // Media stored as two fields, odd & even lines, as though it was interlaced
   IF_TYPE_1080PSFI_2398 = 60,      // 1080PsF/23.98 SGI 10 bit, SGI or DVS 8-bit
   IF_TYPE_1080PSFI_24 = 61,        // 1080PsF/24    SGI 10 bit, SGI or DVS 8-bit
   IF_TYPE_1080PSFI_25 = 62,        // 1080PsF/25    SGI 10 bit, SGI or DVS 8-bit
   IF_TYPE_1080PSFI_2398_DVS = 63,  // 1080PsF/23.98 DVS 10 bit
   IF_TYPE_1080PSFI_24_DVS = 64,    // 1080PsF/24    DVS 10 bit
   IF_TYPE_1080PSFI_25_DVS = 65,    // 1080PsF/25    DVS 10 bit

   // "Dual Link" formats (25 fps playback/timecode).  These are
   // "magic" IF_TYPEs.  The video transport is done at 14.98, or 15,
   // but playback is at 25 fps.  They're for HSDL, to use mod 25 TC.
   IF_TYPE_2048_1556PSF_1498_25_DVS = 66,
   IF_TYPE_2048_1556PSF_15_25_DVS = 67,

   // SD Formats
   // Media stored as one frame per file, with field lines interleaved as
   // if the format were progressive/segmented field
   IF_TYPE_525IP_5994 = 68,         // 525/P29.97     SD PROGRESSIVE "NTSC"
   IF_TYPE_525IP_60 = 69,           // 525/P24        SD PROGRESSIVE "NTSC"
   IF_TYPE_525P_2398 = 70,       // 525/P23.98     SD FILM FRAMES "NTSC"
   IF_TYPE_525P_24 = 71,           // 525/P24        SD FILM FRAMES "NTSC"
   IF_TYPE_625P_25 = 72,           // 625/P25        SD PROGRESSIVE "PAL"
   IF_TYPE_525IP_5994_DVS = 73,     // 525/P29.97     SD PROGRESSIVE "NTSC"
   IF_TYPE_525IP_60_DVS = 74,       // 525/P24        SD PROGRESSIVE "NTSC"
   IF_TYPE_525P_2398_DVS = 75,     // 525/P23.98     SD FILM FRAMES "NTSC"
   IF_TYPE_525P_24_DVS = 76,       // 525/P24        SD FILM FRAMES "NTSC"
   IF_TYPE_625P_25_DVS = 77,       // 625/P25        SD PROGRESSIVE "PAL"

   IF_TYPE_FILE_EXR = 78           // OpenEXR file format

};

#ifndef AVID_SDK_CONFLICT
typedef vector <EImageFormatType> EImageFormatTypeList;
#endif

//////////////////////////////////////////////////////////////////////
// Pixel Components
// NB: If enumerator value is less than 256, then it corresponds
//     to the coding convention for the DPX file format for the
//     DPX Image Element Descriptiors (Table 1 in DPX standard)
enum EPixelComponents
{
   IF_PIXEL_COMPONENTS_INVALID = 0,
//   IF_PIXEL_COMPONENTS_LUMINANCE = 6,      // Luminance only, B&W   NO LONGER SUPPORTED
   IF_PIXEL_COMPONENTS_Y = 6,              // Y only
   IF_PIXEL_COMPONENTS_RGB = 50,
   IF_PIXEL_COMPONENTS_RGBA = 51,          // A = alpha matte channel
   IF_PIXEL_COMPONENTS_YUV422 = 100,       // CbYCrY
   IF_PIXEL_COMPONENTS_YUV4224 = 101,      // YUV + alpha, CbYACrYA
   IF_PIXEL_COMPONENTS_YUV444 = 102,       // CbYCr
//   IF_PIXEL_COMPONENTS_Y = 256,            // Like LUMINANCE, but expanded to 3 channels (HACK)
   IF_PIXEL_COMPONENTS_YYY = 257,          // Like RGB, but all channels should have same values
   IF_PIXEL_COMPONENTS_BGR = 258,          // Like RGB but B & R are swapped
   IF_PIXEL_COMPONENTS_BGRA = 259,         // Like RGBA but B & R are swapped
};

//////////////////////////////////////////////////////////////////////
// Color Space
// NB: If enumerator value is less than 256, then it corresponds
//     to the coding convention for the DPX file format for the
//     DPX Transfer Characteristics and Colorimetric Specifications
//     (Tables 5A & 5B in DPX standard)
enum EColorSpace
{
   IF_COLOR_SPACE_INVALID = 0,
   IF_COLOR_SPACE_LINEAR = 2,          // Typically RGB
   IF_COLOR_SPACE_LOG = 3,             // Typically RGB
   IF_COLOR_SPACE_SMPTE_240 = 5,       // Typically video
   IF_COLOR_SPACE_CCIR_709 = 6,        // Typically HD video, also SD video
   IF_COLOR_SPACE_CCIR_601_BG = 7,     // 601 System B or G, Typically 625
   IF_COLOR_SPACE_CCIR_601_M = 8,      // 601 System M, Typically 525
   IF_COLOR_SPACE_EXR = 256,           // EXR RGB
};

//////////////////////////////////////////////////////////////////////
// Pixel Packing
// Defines how components of a pixel are packed within a data word
enum EPixelPacking
{
   IF_PIXEL_PACKING_INVALID = 0,
   IF_PIXEL_PACKING_8Bits_IN_1Byte = 1,
   IF_PIXEL_PACKING_4_10Bits_IN_5Bytes = 2,      // Typical 10bit YUV422
   IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L = 3,    // 10 bit RGB in 32-bit word
   IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R = 4,    // 10 bit RGB in 32-bit word
   IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L = 5,    // 10 bits in 16-bit word
   IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R = 6,    // 10 bits in 16-bit word
   IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE = 7,   // 16-bits, Big-Endian byte ordering
   IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE = 8,   // 16-bits, Little-Endian byte ordering
   IF_PIXEL_PACKING_1_8Bits_IN_2Bytes = 9,       // 8-bits, native byte ordering
   IF_PIXEL_PACKING_1_10Bits_IN_2Bytes = 10,     // 10-bits, native byte ordering
   IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT = 11, // 16-bits, native byte ordering
   IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS = 12, // DVS 10-Bit YUV422 in 32 bits
   IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa = 13,// DVS 3x8-bits (MSB) + 3x2bits (LSB)
   IF_PIXEL_PACKING_2_12Bits_IN_3Bytes = 14,     // 12 bits, padded, follows  spec
   IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR = 15,  // 12 bits, padded, reverses spec
   IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE = 16,   // 12 bits in 16 bit word, left justified, Little-Endian byte ordering
   IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE = 17,   // 12 bits in 16 bit word, left justified, Big-Endian byte ordering
   IF_PIXEL_PACKING_1_12Bits_IN_2Bytes = 18,     // 12 bits, native byte ordering
   IF_PIXEL_PACKING_1_Half_IN_2Bytes = 19,       // 16-bit HALF floating point format
};

//////////////////////////////////////////////////////////////////////
// Frame Rate
// Enumeration for standard video frame rates
enum EFrameRate
{
   IF_FRAME_RATE_INVALID = 0,
   IF_FRAME_RATE_NON_STANDARD = 1,     // Non-standard frame rate 
   IF_FRAME_RATE_2398 = 2,             // 23.976 fps (24/1.001)
   IF_FRAME_RATE_2400 = 3,             // 24.00 fps (Standard film rate)
   IF_FRAME_RATE_2500 = 4,             // 25.00 fps (PAL)
   IF_FRAME_RATE_2997 = 5,             // 29.97 fps (NTSC, 30/1.001) 
   IF_FRAME_RATE_3000 = 6,             // 30.00 fps
   IF_FRAME_RATE_5000 = 7,             // 50.00 fps 
   IF_FRAME_RATE_5994 = 8,             // 59.94 fps (60/1.001)
   IF_FRAME_RATE_6000 = 9,             // 60.00 fps
   IF_FRAME_RATE_1498 = 10,            // 14.98 fps (15/1.001)
   IF_FRAME_RATE_1500 = 11,            // 15.00 fps
// These are "magic" IF_FRAME_RATEs.  The video transport is done at
// 14.98, or 15, or whatever, but playback is at 25 fps.  They're for
// HSDL, to use mod 25 TC.
   IF_FRAME_RATE_1498_25 = 12,         // 14.98 fps (15/1.001) / 25 fps TC
   IF_FRAME_RATE_1500_25 = 13,         // 15.00 fps / 25 fps TC
   IF_FRAME_RATE_2997_25 = 14,         // 29.97 fps (30/1.001) / 25 fps TC
   IF_FRAME_RATE_3000_25 = 15          // 30.00 fps / 25 fps TC
};

//////////////////////////////////////////////////////////////////////
// Image Data Flag
// Marks if frame's media file contains real image data
// or if frame's media file is uninitialized with a simulated
// black or colorbar image  
enum EImageDataFlag
{
   II_IMAGE_DATA_FLAG_INVALID = 0,
   II_IMAGE_DATA_FLAG_REAL = 1,
   II_IMAGE_DATA_FLAG_SIMULATED_BLACK = 2,
   II_IMAGE_DATA_FLAG_SIMULATED_COLORBAR = 3
};

//////////////////////////////////////////////////////////////////////
// Pulldown Type
//
enum EPulldown
{
   IF_PULLDOWN_INVALID = 0,      // Invalid pulldown
   IF_PULLDOWN_NONE = 1,         // No pulldown (625, 720P/50, 1080I/50)
   IF_PULLDOWN_FRAME_3_2 = 2,    // Frame-based 3:2 pulldown (720P/60)
   IF_PULLDOWN_FIELD_3_2 = 3,    // Field-based 3:2 pulldown (non-525, 1080I/60)
   IF_PULLDOWN_SWAPPED_FIELD_3_2 = 4, // Field-based 3:2 pulldown from video
                                     // with odd/even fields swapped (525)
   IF_PULLDOWN_PAL_FD = 5        // TTT Kludge for PAL field dominance problem
};
//////////////////////////////////////////////////////////////////////


enum EComponentValueType
{
   IF_COMPONENT_VALUE_TYPE_MIN = 0,
   IF_COMPONENT_VALUE_TYPE_MAX = 1,
   IF_COMPONENT_VALUE_TYPE_BLACK = 2,
   IF_COMPONENT_VALUE_TYPE_WHITE = 3,
   IF_COMPONENT_VALUE_TYPE_FILL = 4,
   IF_COMPONENT_VALUE_TYPE_COUNT = 5   // Always the last one
};

#define MAX_COMPONENT_COUNT  3     // TTT doesn't include alpha channel

enum EImageResolution
{
   IF_IMAGE_RESOLUTION_INVALID = -1,
   IF_IMAGE_RESOLUTION_STANDARD_DEF = 0,
   IF_IMAGE_RESOLUTION_HIGH_DEF = 1,
   IF_IMAGE_RESOLUTION_2K_DATA = 2,
   IF_IMAGE_RESOLUTION_4K_DATA = 3,
   IF_IMAGE_RESOLUTION_6K_DATA = 4,
   IF_IMAGE_RESOLUTION_8K_DATA = 5,
   IF_IMAGE_RESOLUTION_COUNT = 6   // Always the last one
};

//////////////////////////////////////////////////////////////////////
// Alpha Matte

// 20090123: I added the default setting to handle the case where the
//           clip scheme does not have an entry for AlphaMatteType.
//           For most formats we want 'default' to be 'None' but for
//           10-bit RGB clips stored in DPX files we want to it to mean
//           1_BIT_IN_PADDING (a.k.a. the "CINTEL hack").
// NOTE!!!   DEFAULT is intended to only be returned as a result from
//           reading a clip scheme file... it should NEVER be written
//           to a .clp file - it should be resolved before being written!

enum EAlphaMatteType
{
   IF_ALPHA_MATTE_DEFAULT = -1,
   IF_ALPHA_MATTE_INVALID = 0,
   IF_ALPHA_MATTE_NONE,              // Alpha Matte not present
   IF_ALPHA_MATTE_1_BIT,             //  1-bit per pixel
	IF_ALPHA_MATTE_8_BIT,             //  8-bit per pixel
   IF_ALPHA_MATTE_16_BIT,            // 16-bit per pixel
   IF_ALPHA_MATTE_2_BIT_EMBEDDED, //  2-bit per pixel hack for 10 bit DPX files
   IF_ALPHA_MATTE_8_BIT_INTERLEAVED, //  8-bit A in RGBA
   IF_ALPHA_MATTE_10_BIT_INTERLEAVED,// 10-bit A in RGBA
   IF_ALPHA_MATTE_16_BIT_INTERLEAVED // 16-bit A in RGBA
};

//////////////////////////////////////////////////////////////////////

#ifndef AVID_SDK_CONFLICT
class MTI_FORMATDLL_API CImageInfo
{
public:
   // Static Query Functions
   static EVideoFieldRate convertFrameRateToFieldRate(EFrameRate frameRate,
                                                      bool interlaced);
   static EFrameRate convertFieldRateToFrameRate(EVideoFieldRate fieldRate,
                                                 bool interlaced);
   static EAlphaMatteType queryAlphaMatteType(const string& typeName);
   static string queryAlphaMatteTypeName(EAlphaMatteType alphaMatteType);
   static bool queryAlphaMatteTypeCanBeHidden(EAlphaMatteType alphaMatteType);
   static MTI_UINT8 queryBitsPerComponent(EPixelPacking pixelPacking);
   static double queryBytesPerPixel(EPixelComponents pixelComponents,
                                    EPixelPacking pixelPacking);
   static MTI_UINT32 queryBytesForPixels(EPixelComponents pixelComponents,
                                         EPixelPacking pixelPacking,
                                         MTI_UINT32 pixelCount);
   static EColorSpace queryColorSpace(const string& colorSpaceName);
   static string queryColorSpaceName(EColorSpace colorSpace);
   static void queryFilmFramesFrameRate(EPulldown pulldown,
                                        EFrameRate *frameRateEnum,
                                        double *frameRate);
   static EFrameRate queryFrameRate(const string& frameRateName,
                                    double &newFrameRate);
   static string queryFrameRateName(EFrameRate frameRate);
   static EImageFormatType queryImageFormatType(
                                             const string& imageFormatTypeName);
   static EImageFormatType queryImageFormatTypeDescriptionTable(int index);
   static int queryImageFormatTypeDescription(EImageFormatType imageFormat,
                                              string &descr1, string &descr2);
   static int queryImageFormatTypeDescriptionCount();
   static string queryImageFormatTypeName(EImageFormatType imageFormat);
   static string queryImageFormatTypeSimpleName(EImageFormatType imageFormat);
   static bool queryIsCadenceRepairSupported(EImageFormatType imageFormat);
   static bool queryIsFormat525(EImageFormatType imageFormat);
   static bool queryIsFormat720P(EImageFormatType imageFormat);
   static bool queryIsFormatConventionalVTR(EImageFormatType imageFormatType);
   static bool queryIsFormatFile(EImageFormatType imageFormat);
   static bool queryIsFormatVideo(EImageFormatType imageFormat);

   static EAlphaMatteType queryNominalAlphaMatteType(
                                             EImageFormatType imageFormatType);
   static EColorSpace queryNominalColorSpace(EImageFormatType imageFormatType);
   static int queryNominalComponentCount(EPixelComponents pixelComponents);
   static int queryNominalComponentsPerPixel(EPixelComponents pixelComponents);
   static EVideoFieldRate  queryNominalFieldRateEnum(
                                              EImageFormatType imageFormatType);
   static double queryNominalFrameAspectRatio(EImageFormatType imageFormatType);
   static double queryNominalFrameRate(EImageFormatType imageFormatType);
   static EFrameRate queryNominalFrameRateEnum(
                                              EImageFormatType imageFormatType);
   static int queryNominalHandleCount(EImageFormatType imageFormatType);
   static bool queryNominalInterlaced(EImageFormatType imageFormatType);
   static MTI_UINT32 queryNominalLineAlignment(
                                             EImageFormatType imageFormatType);
   static int queryNominalLinesPerFrame(EImageFormatType imageFormatType);
   static double queryNominalPixelAspectRatio(EImageFormatType imageFormatType);
   static EPixelComponents queryNominalPixelComponents(
                                              EImageFormatType imageFormatType);
   static int queryNominalPixelsPerLine(EImageFormatType imageFormatType);

   static EPulldown queryNominalPulldown(EImageFormatType imageFormatType);
   static double queryNominalGamma(EImageFormatType imageFormatType);
   static EColorSpace queryNominalTransferCharacteristic(
                                              EImageFormatType imageFormatType);

   static EPixelComponents queryPixelComponents(
                                            const string& pixelComponentsName);
   static string queryPixelComponentsName(EPixelComponents pixelComponents);
   static EPixelPacking queryPixelPacking(const string& pixelPackingName);
   static string queryPixelPackingName(EPixelPacking pixelPacking);
   static double queryStandardFrameRate(EFrameRate frameRateEnum);
   static int queryNominalTimecodeFrameRate(EImageFormatType imageFormatType);

   // Color Space
   int static queryNominalComponentValue(EComponentValueType valueType,
                                         EPixelComponents pixelComponents,
                                         EColorSpace colorSpace,
                                         bool isVideoFormat,
                                         int bitsPerComponent,
                                         MTI_UINT16 componentValue[]);

   // Static validation functions
   static int isAlphaMatteTypeValid(EAlphaMatteType alphaMatteType);
   static int isColorSpaceValid(EColorSpace colorSpace);
   static int isFrameAspectRatioValid(MTI_REAL32 frameAspectRatio);
   static int isGammaValid(MTI_REAL32 gamma);
   static int isHorizontalIntervalPixelsValid(
                                          MTI_UINT32 horizontalIntervalPixels);
   static int isImageFormatTypeValid(EImageFormatType imageFormatType);
   static int isLinesPerFrameValid(MTI_UINT32 linesPerFrame);
   static int isPixelAspectRatioValid(MTI_REAL32 pixelAspectRatio);
   static int isPixelComponentsValid(EPixelComponents pixelComponents);
   static int isPixelPackingValid(EPixelPacking pixelPacking);
   static int isPixelsPerLineValid(MTI_UINT32 pixelsPerLine);
   static int isTransferCharacteristicValid(EColorSpace transferCharacteristic);
   static int isVerticalIntervalRowsValid(MTI_UINT32 verticalIntervalRows);

   // Hacks
   static bool isImageFormatCompatibleWithDVS(EImageFormatType imageFormatType,
                                              EPixelPacking pixelPacking);
   static bool GetDpxIsDefaultLog();


private:
   static MTI_UINT16 getComponentMultiplier(EColorSpace colorSpace,
                                            int bitsPerComponent);
};
#endif // AVID_SDK_CONFLICT

extern MTI_FORMATDLL_API CIniAssociations ImageFormatAssociations;
//////////////////////////////////////////////////////////////////////

#endif // #ifndef IMAGE_INFO_H







