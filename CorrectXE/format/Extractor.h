//
// include file for class CExtractor
//
#ifndef ExtractorH
#define ExtractorH

#include "machine.h"
#include "formatDLL.h"
#include "mthread.h"
#include "ImageInfo.h"

////////////////////////////////////////////////////////////

class CImageFormat;

class MTI_FORMATDLL_API CExtractor
{

public:

   CExtractor();

   ~CExtractor();

   void extractActivePicture(
                MTI_UINT16 *dstpels,       // array of 16-bit "444 data" (YUV or RGB)
                MTI_UINT8 **srcimg,        // -> source field(s) (YUV422 or RGB variant)
                const CImageFormat *srcClipImageFormat, // image format of source clip)
                const CImageFormat *srcFrameImageFormat // image format of source frame)
                );

   void replaceActivePicture(MTI_UINT8 **, const CImageFormat *, MTI_UINT16 *);

   int extractSubregion(MTI_UINT16 *, MTI_UINT8 **, const CImageFormat *, const RECT &);
   int extractSubregion(MTI_UINT16 *, MTI_UINT8 **, const CImageFormat *, RECT *);

   int replaceSubregion(MTI_UINT8 **, const CImageFormat *, const RECT &, MTI_UINT16 *);
   int replaceSubregion(MTI_UINT8 **, const CImageFormat *, RECT *, MTI_UINT16 *);

   int extractSubregionWithinFrame(MTI_UINT16 *, MTI_UINT8 **, const CImageFormat *, const RECT &);
   int extractSubregionWithinFrame(MTI_UINT16 *, MTI_UINT8 **, const CImageFormat *, RECT *);

   int replaceSubregionWithinFrame(MTI_UINT8 **, const CImageFormat *, const RECT &, MTI_UINT16 *);
   int replaceSubregionWithinFrame(MTI_UINT8 **, const CImageFormat *, RECT *, MTI_UINT16 *);

   int extractPoint(MTI_UINT16 * ry, MTI_UINT16 * gu, MTI_UINT16 * bv, 
                    MTI_UINT8 **, const CImageFormat *,  int x,  int y);

   static int getAlphaMatteSize(EAlphaMatteType alphatype);

   ///////////////////////////////////////////////////////////////

   // -> 16-bit 444 progressive field
   MTI_UINT16 *pels;

   // dbl ptr to formatted frame
   MTI_UINT8 **frame;

   // no fields in frame
   int frmFieldCount;

   // width of frame in pels
   int frmWdth;

   // height of frame in pels
   int frmHght;
   
   // pitch in bytes of frame
   int frmPitch;

   // active rectangle of frame
   RECT actRect;

   // width of active rectangle in pels
   int actWdth;

   // height of active rectangle in pels
   int actHght;

   // type of Alpha matte
   EAlphaMatteType clipAlphaType;
   EAlphaMatteType frameAlphaType;

   // Flags to cause copy/creation of Alpha matte.
   // When copyAlpha==true, zeroAlpha==true means we want
   // to fill the destination alpha with zeros instead of
   // copying it from the source.
   bool copyAlpha;
   bool zeroAlpha;

   ///////////////////////////////////////////////////////////////

   int nStripes; // number of stripes in frame (1,2,4,8, or 16)

   MTHREAD_STRUCT tsThread;

   ///////////////////////////////////////////////////////////////

   MTI_UINT32 alphaHackMask;

};

#endif

