// ImageFormat.h: interface for the CImageFormat class.
//
//////////////////////////////////////////////////////////////////////

#ifndef ImageFormat3H
#define ImageFormat3H

#include <string>
#include <iostream>
using std::string;
using std::ostream;

#include "formatDLL.h"
#include "ImageInfo.h"
#include "MediaFormat.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CIniFile;

//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Color Space

// The SColorSpace structure contains
// The component order is always YUV or RGB and is independent of the
// Image Format's pixel components type.

class MTI_FORMATDLL_API CImageColorSpace
{
public:
   int SetToDefaults(EPixelComponents pixelComponents, EColorSpace colorSpace,
                     bool isVideoFormat, int bitsPerComponent);

   bool compare(const CImageColorSpace &rhs, int componentCount) const;

   int readSection(CIniFile *iniFile, const string& sectionPrefix);
   int writeSection(CIniFile *iniFile, const string& sectionPrefix);

   // Normal serizalization
   bool serialize(ostream &osStream) const;
   static CImageColorSpace *deserialize(istream &isStream);

public:
   // Minimum and maximum component values
   MTI_UINT16 componentMin[MAX_COMPONENT_COUNT];
   MTI_UINT16 componentMax[MAX_COMPONENT_COUNT];

   // Convenience component values
   MTI_UINT16 componentBlack[MAX_COMPONENT_COUNT];
   MTI_UINT16 componentWhite[MAX_COMPONENT_COUNT];
   MTI_UINT16 componentFill[MAX_COMPONENT_COUNT];

   // 3 x 3 matrix to transform from given color space to RGB
   double transformToRGB[MAX_COMPONENT_COUNT][MAX_COMPONENT_COUNT];

   // 3 x 3 matrix to transform from RGB to given color space
   double transformFromRGB[MAX_COMPONENT_COUNT][MAX_COMPONENT_COUNT];

private:
   static string sectionNameSuffix;
   static string componentMinKey;
   static string componentMaxKey;
   static string componentBlackKey;
   static string componentWhiteKey;
   static string componentFillKey;
};

/*
 R = Y*transformToRGB[0][0] + U*transformToRGB[0][1] + V*transformToRGB[0][2];
 G = Y*transformToRGB[1][0] + U*transformToRGB[1][1] + V*transformToRGB[1][2];
 B = Y*transformToRGB[2][0] + U*transformToRGB[2][1] + V*transformToRGB[2][2];
*/

//////////////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CImageFormat : public CMediaFormat
{
public:
   CImageFormat();
   CImageFormat(const CImageFormat &src);
   virtual ~CImageFormat();

   // Virtual "default constructor" returns new instance with same type
   // as *this.  Note: this requires that compiler support "covariant return
   // types."  If compiler does not support it, then conditionally compile
   // to return CMediaFormat* and see what else breaks.
//   CImageFormat* construct() const {return new CImageFormat;};
   CMediaFormat* construct() const {return new CImageFormat;};
   // Virtual "copy constructor"
//   CImageFormat* clone() const {return new CImageFormat(*this);};
   CMediaFormat* clone() const {return new CImageFormat(*this);};
   CMediaFormat& assign(const CMediaFormat& src);

   CImageFormat& operator=(const CImageFormat& rhs);
   bool compare(const CImageFormat &rhs, bool allowImageFileExceptions=false) const;

   // Accessors
   RECT getActivePictureRect() const;
   EAlphaMatteType getAlphaMatteType() const;
   EAlphaMatteType getAlphaMatteHiddenType() const;
   bool hasNonHiddenAlphaMatte() const;
   MTI_UINT8 getBitsPerComponent() const;
   MTI_UINT32 getBytesPerField() const;
   MTI_UINT32 getBytesPerFieldExcAlpha() const;   // what a crock
   MTI_UINT32 getAlphaBytesPerField() const;      // hack-o-rama
   EColorSpace getColorSpace() const;
   int getComponentCount() const;
   int getComponentCountExcAlpha() const;
   int getComponentsPerPixel() const;
   MTI_REAL32 getFrameAspectRatio() const;
   MTI_UINT32 getFramePitch() const;
   MTI_UINT32 getHorizontalIntervalPixels() const;
   EImageFormatType getImageFormatType() const;
   MTI_UINT32 getLinesPerFrame() const;
   MTI_REAL32 getNominalPixelAspectRatio() const;
   MTI_REAL32 getPixelAspectRatio() const;
   EPixelComponents getPixelComponents() const;
   EPixelPacking  getPixelPacking() const;
   MTI_UINT32 getPixelsPerLine() const;
   MTI_UINT32 getTotalFrameHeight() const;
   MTI_UINT32 getTotalFrameWidth() const;
   MTI_UINT32 getVerticalIntervalRows() const;
   EImageResolution getImageResolution() const;
   MTI_REAL32 getGamma() const;
   EColorSpace getTransferCharacteristic() const;

   // Color Space
   CImageColorSpace const * getColorSpaceValues() const;
   void getComponentValueBlack(MTI_UINT16 componentValues[]) const;
   void getComponentValueWhite(MTI_UINT16 componentValues[]) const;
   void getComponentValueFill(MTI_UINT16 componentValues[]) const;
   void getComponentValueMin(MTI_UINT16 componentValues[]) const;
   void getComponentValueMax(MTI_UINT16 componentValues[]) const;
   void getComponentValueBlackFill(MTI_UINT16 componentValues[]) const;
   void getComponentValueWhiteFill(MTI_UINT16 componentValues[]) const;


   // Mutators
   void setAlphaMatteType(EAlphaMatteType newAlphaMatteType);
   void setAlphaMatteHiddenType(EAlphaMatteType newAlphaMatteType);
   void setColorSpace(EColorSpace newColorSpace);
   void setFrameAspectRatio(MTI_REAL32 newFrameAspectRatio);
   void setImageFormatType(EImageFormatType newImageFormat);
   void setHorizontalIntervalPixels(MTI_UINT32 newHorizontalIntervalPixels);
   void setInterlaced(bool newInterlaced);
   void setLinesPerFrame(MTI_UINT32 newLinesPerFrame);
   void setPixelAspectRatio(MTI_REAL32 newPixelAspectRatio);
   void setPixelComponents(EPixelComponents newPixelComponents);
   void setPixelPacking(EPixelPacking newPixelPacking);
   void setPixelsPerLine(MTI_UINT32 newPixelsPerLine);
   void setVerticalIntervalRows(MTI_UINT32 newVerticalIntervalRows);
   void setGamma(MTI_REAL32 newGamma);
   void setTransferCharacteristic(EColorSpace newTransferCharacteristic);

   // Color Space
   void setComponentValueBlack(MTI_UINT16 componentValues[]);
   void setComponentValueFill(MTI_UINT16 componentValues[]);
   void setComponentValueMax(MTI_UINT16 componentValues[]);
   void setComponentValueMin(MTI_UINT16 componentValues[]);
   void setComponentValueWhite(MTI_UINT16 componentValues[]);
   void setColorSpaceValues (const CImageFormat &nativeImageFormat);

   int setToDefaults(EImageFormatType newImageFormatType,
                     EPixelPacking newPixelPacking);
   int setToDefaults(EImageFormatType newImageFormatType, 
                     EPixelPacking newPixelPacking,
                     EPixelComponents newPixelComponents,
                     MTI_UINT32 newLinesPerFrame, MTI_UINT32 newPixelsPerLine);
   int setToInternalFormat(const CImageFormat &nativeImageFormat);

   // Validation functions
   int validate() const;

   int readSection(CIniFile *iniFile, const string& sectionPrefix);
   int readClipInitSection(CIniFile *iniFile, const string& sectionPrefix);
   int readClipInitSection2(CIniFile *iniFile, const string& sectionPrefix);
   static EImageFormatType readImageFormatType(CIniFile *iniFile,
                                               const string& sectionPrefix);
   int writeSection(CIniFile *iniFile, const string& sectionPrefix);
   int writeClipInitSection(CIniFile *iniFile, const string& sectionPrefix);

   // Normal serizalization
   bool serialize(ostream &osStream) const;
   static CImageFormat *deserialize(istream &isStream);

   void dump(MTIostringstream& stream) const;      // For debugging

private:
   EImageFormatType imageFormatType;

   EPixelComponents pixelComponents;
   EColorSpace colorSpace;
   EPixelPacking  pixelPacking;  // How components are packed in bytes & words

   MTI_UINT32 linesPerFrame;
   MTI_UINT32 pixelsPerLine;

   MTI_REAL32 pixelAspectRatio; 
   MTI_REAL32 frameAspectRatio;

   MTI_UINT32 bytesPerField;     // Number of bytes per field
   MTI_UINT32 bytesPerFieldExcAlpha; // Gag me
   MTI_UINT32 alphaBytesPerField;    // Raaalph
   MTI_UINT32 framePitch;        // Number of bytes between pixels in
                                 // the same column in successive rows

   MTI_UINT32 verticalIntervalRows;  // Number of rows occupied by the vertical
                                     // blanking interval.  0 if none
   MTI_UINT32 horizontalIntervalPixels;   // Number of pixels occupied by the
                                          // horizontal blanking interval.
                                          // 0 if none.

   EAlphaMatteType alphaMatteType;
   EAlphaMatteType alphaMatteHiddenType;

   MTI_REAL32 gamma;
   EColorSpace transferCharacteristic;

   CImageColorSpace colorSpaceValues;

   void resetBytesPerField();

private:
   static string horizontalIntervalPixelsKey;
   static string imageFormatSectionName;
   static string imageFormatTypeKey;
   static string pixelComponentsKey;
   static string colorSpaceKey;
   static string pixelPackingKey;
   static string realPixelPackingKey;
   static string linesPerFrameKey;
   static string pixelsPerLineKey;
   static string pixelAspectRatioKey;
   static string frameAspectRatioKey;
   static string verticalIntervalRowsKey;
   static string gammaKey;
   static string transferCharacteristicKey;
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef IMAGEFORMAT3_H

