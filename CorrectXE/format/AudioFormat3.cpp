// AudioFormat3.cpp: implementation of the CAudioFormat class.
//
// Created by: John Starr, December 17, 2001
//
/* CVS Info:
$Header: /usr/local/filmroot/format/source/AudioFormat3.cpp,v 1.5 2005/10/06 12:46:17 mlm Exp $
*/
//////////////////////////////////////////////////////////////////////

#include "AudioFormat3.h"
#include "err_format.h"
#include "IniFile.h"
#include "MTIstringstream.h"
#include "timecode.h"
#include <typeinfo>

//////////////////////////////////////////////////////////////////////
// Static Data Members Initialization
//////////////////////////////////////////////////////////////////////

string CAudioFormat::audioFormatSectionName = "AudioFormat";
string CAudioFormat::channelCountKey = "ChannelCount";
string CAudioFormat::sampleDataPackingKey = "SampleDataPacking";
string CAudioFormat::sampleDataSizeKey = "SampleDataSize";
string CAudioFormat::sampleDataTypeKey = "SampleDataType";
string CAudioFormat::sampleRateKey = "SampleRate";
string CAudioFormat::samplesPerFieldKey = "SamplesPerField";

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAudioFormat::CAudioFormat()
: CMediaFormat(MEDIA_FORMAT_TYPE_AUDIO3)
{

}

// Copy Constructor
CAudioFormat::CAudioFormat(const CAudioFormat &src)
: CMediaFormat(src.getMediaFormatType())
{
   *this = src;
}

CAudioFormat::~CAudioFormat()
{

}

//   CMediaFormat* CAudioFormat::construct() const
//   {
//	 return new CAudioFormat;
//   }
//
//   CMediaFormat* CAudioFormat::clone() const
//   {
//	 return new CAudioFormat(*this);
//   }

//------------------------------------------------------------------------
//
// Function:    operator =
//
// Description: 
//
// Arguments    
//
// Returns:     
//
//------------------------------------------------------------------------
CAudioFormat& CAudioFormat::operator =(const CAudioFormat &rhs)
{
   if (this == &rhs)       // Check for self-assignment
      return *this;
   
   // Copy direct base class's member variables
   CMediaFormat::operator=(rhs);

   channelCount = rhs.channelCount;

   sampleRateEnum = rhs.sampleRateEnum;
   sampleRate = rhs.sampleRate;

   samplesPerField = rhs.samplesPerField;

   sampleDataType   = rhs.sampleDataType;
   sampleDataSize = rhs.sampleDataSize;
   sampleDataPacking = rhs.sampleDataPacking;
   sampleDataEndian = rhs.sampleDataEndian;

   return *this;
}

CMediaFormat& CAudioFormat::assign(const CMediaFormat &src)
{
   if (this == &src)      // Check for self-assignment
      return *this;

   // If the types do not match, throw an exception.  This should
   // never happen unless there is a coding or logic error
   if(getMediaFormatType() != src.getMediaFormatType())
      throw std::bad_cast();

   *this = static_cast<const CAudioFormat&>(src);

   return *this;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

int CAudioFormat::getBytesPerSample() const
{
   int bytesPerSample = 0;

   switch(sampleDataPacking)
      {
 
      case AI_SAMPLE_DATA_PACKING_1_Byte :     
         bytesPerSample = 1;
         break;
      case AI_SAMPLE_DATA_PACKING_2_Bytes_L :    
      case AI_SAMPLE_DATA_PACKING_2_Bytes_R :    
         bytesPerSample = 2;
         break;
      case AI_SAMPLE_DATA_PACKING_3_Bytes_L :    
      case AI_SAMPLE_DATA_PACKING_3_Bytes_R :    
         bytesPerSample = 3;
         break;
      case AI_SAMPLE_DATA_PACKING_4_Bytes_L :    
      case AI_SAMPLE_DATA_PACKING_4_Bytes_R :
         bytesPerSample = 4;
         break;
      case AI_SAMPLE_DATA_PACKING_INVALID :
      default :
         // Error: unexpected sampleDataPacking
         bytesPerSample = 0;  // 0 will probably cause trouble for caller
         break;
      }

   return bytesPerSample;
}

int CAudioFormat::getChannelCount() const
{
   return channelCount;
}

//------------------------------------------------------------------------
//
// Function:    getSampleCountAtTimecode
//
// Description: Determines the number of audio samples for a given field
//              at a particular timecode. The number of samples is calculated
//              rather than read from a field in a clip.
//
// Arguments    const CTimecode& timecode    
//              int fieldNumber              If clip is interlaced, selects
//                                           first or second field.
//                                             == 0 - first field
//                                             > 0 - second field
//
// Returns:     Number of audio samples in the field at the given timecode     
//
//------------------------------------------------------------------------
int CAudioFormat::getSampleCountAtTimecode(const CTimecode& timecode, 
                                           int fieldNumber) const
{
   // Adjust the field number: Force to 0 if not interlaced, 0 or 1 if 
   // interlaced
   if (fieldNumber > 0 && getInterlaced())
      fieldNumber = 1;
   else
      fieldNumber = 0;

   fieldNumber += timecode.getAbsoluteTime() * getFieldCount();

   MTI_INT64 sampleIndex = (MTI_INT64)(fieldNumber * samplesPerField);
   MTI_INT64 sampleIndexPlus1 = (MTI_INT64)((fieldNumber+1) * samplesPerField);

   return(sampleIndexPlus1 - sampleIndex);
}

EAudioSampleEndian CAudioFormat::getSampleDataEndian() const
{
   return sampleDataEndian;
}

EAudioSamplePacking CAudioFormat::getSampleDataPacking() const
{
   return sampleDataPacking;
}

int CAudioFormat::getSampleDataSize() const
{
   return sampleDataSize;
}

EAudioSampleType CAudioFormat::getSampleDataType() const
{
   return sampleDataType;
}

double CAudioFormat::getSamplesPerField() const
{
   return samplesPerField;
}

double CAudioFormat::getSampleRate() const
{
   return sampleRate;
}

EAudioSampleRate CAudioFormat::getSampleRateEnum() const
{
   return sampleRateEnum;
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CAudioFormat::setChannelCount(int newChannelCount)
{
   channelCount = newChannelCount;
}

void CAudioFormat::setSampleDataEndian(EAudioSampleEndian newSampleDataEndian)
{
   sampleDataEndian = newSampleDataEndian;
}

void CAudioFormat::setSampleDataPacking(EAudioSamplePacking newSampleDataPacking)
{
   sampleDataPacking = newSampleDataPacking;
}

void CAudioFormat::setSampleDataSize(int newSampleDataSize)
{
   sampleDataSize = newSampleDataSize;
}

void CAudioFormat::setSampleDataType(EAudioSampleType newSampleDataType)
{
   sampleDataType = newSampleDataType;
}

void CAudioFormat::setSamplesPerField(double newSamplesPerField)
{
   samplesPerField = newSamplesPerField;
}

void CAudioFormat::setSampleRate(EAudioSampleRate newSampleRateEnum,
                                 double newSampleRate)
{
   sampleRateEnum = newSampleRateEnum;

   if (sampleRateEnum == AI_SAMPLE_RATE_NON_STANDARD)
      {
      // Caller is supplying a non-standard sample rate,
      // so copy newSampleRate argument to sampleRate member
      sampleRate = newSampleRate;
      }
   else
      {
      // Caller is requesting a standard sample rate,
      // so query for the corresponding floating pt
      // sample rate value and set sampleRate member
      sampleRate = CAudioInfo::queryStandardSampleRate(newSampleRateEnum);
      }
}


void CAudioFormat::setToDefaults(EImageFormatType newImageFormatType,
                                 int newChannelCount,
                                 EAudioSampleRate newSampleRateEnum,
                                 double newSampleRate,
                                 EAudioSampleType newSampleDataType,
                                 int newSampleDataSize,
                                 EAudioSamplePacking newSampleDataPacking)
{
   setInterlaced(CImageInfo::queryNominalInterlaced(newImageFormatType));

   setChannelCount(newChannelCount);
   setSampleRate(newSampleRateEnum, newSampleRate);
   setSampleDataType(newSampleDataType);
   setSampleDataSize(newSampleDataSize);
   setSampleDataPacking(newSampleDataPacking);

   // Calculate samples per field
   double frameRate = CImageInfo::queryNominalFrameRate(newImageFormatType);
   double fieldRate = frameRate * getFieldCount();
   if (fieldRate > 0.0)
      setSamplesPerField(getSampleRate() / fieldRate);

   // Set data endian to local default
#if ENDIAN==MTI_LITTLE_ENDIAN
   setSampleDataEndian(AI_SAMPLE_DATA_ENDIAN_LITTLE);
#else
   setSampleDataEndian(AI_SAMPLE_DATA_ENDIAN_BIG);
#endif
}

void CAudioFormat::setToDefaults()
{
   // Set data endian to local default
#if ENDIAN==MTI_LITTLE_ENDIAN
   setSampleDataEndian(AI_SAMPLE_DATA_ENDIAN_LITTLE);
#else
   setSampleDataEndian(AI_SAMPLE_DATA_ENDIAN_BIG);
#endif
}

//////////////////////////////////////////////////////////////////////
// Validation functions
//////////////////////////////////////////////////////////////////////

int CAudioFormat::validate() const
{
   int retVal;

   // Validate superclasses
   retVal = CMediaFormat::validate();
   if (retVal != 0)
      return retVal;

   // Validate channel count.  Upper limit is arbitrary
   retVal = CAudioInfo::isChannelCountValid(getChannelCount());
   if (retVal != 0)
      return retVal;

   // Validate sample data endian.
   retVal = CAudioInfo::isSampleDataEndianValid(getSampleDataEndian());
   if (retVal != 0)
      return retVal;

   // Validate sample data packing
   retVal = CAudioInfo::isSampleDataPackingValid(getSampleDataPacking());
   if (retVal != 0)
      return retVal;

   // Validate the sample data size
   retVal = CAudioInfo::isSampleDataSizeValid(getSampleDataSize());
   if (retVal != 0)
      return retVal;

   // Validate the sample data type
   retVal = CAudioInfo::isSampleDataTypeValid(getSampleDataType());
   if (retVal != 0)
      return retVal;

   // Validate Sample Rate
   retVal = CAudioInfo::isSampleRateEnumValid(getSampleRateEnum());
   if (retVal != 0)
      return retVal;

   if (getSampleRate()
                  != CAudioInfo::queryStandardSampleRate(getSampleRateEnum()))
      return FORMAT_ERROR_INVALID_SAMPLE_RATE;

   // Validate the samples per field
   // TTT Should check this against data type, channels, etc.
   if (getSamplesPerField() < 1.0)
      return FORMAT_ERROR_INVALID_SAMPLES_PER_FIELD;

   return 0;   // Success
}

//////////////////////////////////////////////////////////////////////
// File Interface
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
//
// Function:    readSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CAudioFormat::readSection(CIniFile *iniFile, const string& sectionPrefix)
{
   int retVal;

   string sectionName = sectionPrefix + "\\" + audioFormatSectionName;

   if (!iniFile->SectionExists(sectionName))
      return -361; // ERROR: Audio Format section does not exist

   retVal = CMediaFormat::readSection(iniFile, sectionName);
   if (retVal != 0)
      return retVal;  // ERROR: Could not read base class CMediaFormat's
                      //        portion of the Audio Format section

   // Some working strings
   string valueString, emptyString;

   // Initialize this CAudioFormat
   setToDefaults();

   // Read Interlaced if it is present in the ini file
   bool newInterlaced = getInterlaced();
   if (iniFile->KeyExists(sectionName, interlacedKey))
      {
      // Read Interlaced
      newInterlaced = iniFile->ReadBool(sectionName, interlacedKey,
                                        newInterlaced);
      }

   // Read Channel Count
   if (!iniFile->KeyExists(sectionName, channelCountKey))
      return -362; // ERROR: Missing Channel Count Key
   int newChannelCount = iniFile->ReadInteger(sectionName, channelCountKey, -1);
   retVal = CAudioInfo::isChannelCountValid(newChannelCount);
   if (retVal != 0)
      return retVal;

   // Read Sample Rate
   if (!iniFile->KeyExists(sectionName, sampleRateKey))
      return -364; // ERROR: Missing Sample Rate Key
   valueString = iniFile->ReadString(sectionName, sampleRateKey, emptyString);
   double newSampleRate = 0.0;
   EAudioSampleRate newSampleRateEnum;
   newSampleRateEnum = CAudioInfo::querySampleRate(valueString, newSampleRate);
   retVal = CAudioInfo::isSampleRateEnumValid(newSampleRateEnum);
   if (retVal != 0)
      return retVal;

   // Read Sample Data Type (Integer, Unsigned Integer or Floating)
   if (!iniFile->KeyExists(sectionName, sampleDataTypeKey))
      return -366; // ERROR: Missing Sample Data Type Key
   valueString = iniFile->ReadString(sectionName, sampleDataTypeKey,
                                     emptyString);
   EAudioSampleType newSampleDataType;
   newSampleDataType = CAudioInfo::querySampleDataType(valueString);
   retVal = CAudioInfo::isSampleDataTypeValid(newSampleDataType);
   if (retVal != 0)
      return retVal;

   // Read Sample Data Size (number of bits 8 - 32)
   if (!iniFile->KeyExists(sectionName, sampleDataSizeKey))
      return -366; // ERROR: Missing Sample Data Size Key
   int newSampleDataSize = iniFile->ReadInteger(sectionName, sampleDataSizeKey,
                                                -1);
   retVal = CAudioInfo::isSampleDataSizeValid(newSampleDataSize);
   if (retVal != 0)
      return retVal;

   // Read Sample Data Packing
   if (!iniFile->KeyExists(sectionName, sampleDataPackingKey))
      return -368; // ERROR: Missing Sample Data Packing Key
   valueString = iniFile->ReadString(sectionName, sampleDataPackingKey,
                                     emptyString);
   EAudioSamplePacking newSampleDataPacking;
   newSampleDataPacking = CAudioInfo::querySampleDataPacking(valueString);
   retVal = CAudioInfo::isSampleDataPackingValid(newSampleDataPacking);
   if (retVal != 0)
      return retVal;

   // Read Samples Per Field
   if (!iniFile->KeyExists(sectionName, samplesPerFieldKey))
      return -369; // ERROR: Missing Samples Per Field Key
   double newSamplesPerField;
   newSamplesPerField = iniFile->ReadDouble(sectionName, samplesPerFieldKey,
                                            -1.0);
   if (newSamplesPerField < 1.0)
      return FORMAT_ERROR_INVALID_SAMPLES_PER_FIELD;

   setInterlaced(newInterlaced);
   setChannelCount(newChannelCount);
   setSampleRate(newSampleRateEnum, newSampleRate);
   setSampleDataType(newSampleDataType);
   setSampleDataSize(newSampleDataSize);
   setSampleDataPacking(newSampleDataPacking);
   setSamplesPerField(newSamplesPerField);

   // Success
   return 0;

} // readSection

//------------------------------------------------------------------------
//
// Function:    readClipInitSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CAudioFormat::readClipInitSection(CIniFile *iniFile,
                                       const string& sectionPrefix)
{
   int retVal;

   string sectionName = sectionPrefix + "\\" + audioFormatSectionName;

   if (!iniFile->SectionExists(sectionName))
      return -361; // ERROR: Audio Format section does not exist

   retVal = CMediaFormat::readClipInitSection(iniFile, sectionName);
   if (retVal != 0)
      return retVal;  // ERROR: Could not read base class CMediaFormat's
                      //        portion of the Audio Format section

   // Some working strings
   string valueString, emptyString;

   // Initialize this CAudioFormat
   setToDefaults();

   // Read Interlaced if it is present in the ini file
   if (iniFile->KeyExists(sectionName, interlacedKey))
      {
      // Read Interlaced
      bool newInterlaced = iniFile->ReadBool(sectionName, interlacedKey,
                                             getInterlaced());
      setInterlaced(newInterlaced);
      }

   // Read Channel Count
   if (iniFile->KeyExists(sectionName, channelCountKey))
      {
      int newChannelCount = iniFile->ReadInteger(sectionName, channelCountKey,
                                                 -1);

      retVal = CAudioInfo::isChannelCountValid(newChannelCount);
      if (retVal != 0)
         return retVal;

      setChannelCount(newChannelCount);
      }

   // Read Sample Rate
   if (iniFile->KeyExists(sectionName, sampleRateKey))
      {
      valueString = iniFile->ReadString(sectionName, sampleRateKey,
                                        emptyString);
      EAudioSampleRate newSampleRateEnum;
      double newSampleRate = 0.0;
      newSampleRateEnum = CAudioInfo::querySampleRate(valueString,
                                                      newSampleRate);

      retVal = CAudioInfo::isSampleRateEnumValid(newSampleRateEnum);
      if (retVal != 0)
         return retVal;

      setSampleRate(newSampleRateEnum, newSampleRate);
      }

   // Read Sample Data Type (Integer, Unsigned Integer or Floating)
   if (iniFile->KeyExists(sectionName, sampleDataTypeKey))
      {
      valueString = iniFile->ReadString(sectionName, sampleDataTypeKey,
                                        emptyString);
      EAudioSampleType newSampleDataType;
      newSampleDataType = CAudioInfo::querySampleDataType(valueString);
      
      retVal = CAudioInfo::isSampleDataTypeValid(newSampleDataType);
      if (retVal != 0)
         return retVal;

      setSampleDataType(newSampleDataType);
     }

   // Read Sample Data Size (number of bits 8 - 32)
   if (iniFile->KeyExists(sectionName, sampleDataSizeKey))
      {
      int newSampleDataSize = iniFile->ReadInteger(sectionName,
                                                   sampleDataSizeKey, -1);
      retVal = CAudioInfo::isSampleDataSizeValid(newSampleDataSize);
      if (retVal != 0)
         return retVal;
      setSampleDataSize(newSampleDataSize);
      }

   // Read Sample Data Packing
   if (iniFile->KeyExists(sectionName, sampleDataPackingKey))
      {
      valueString = iniFile->ReadString(sectionName, sampleDataPackingKey,
                                        emptyString);
      EAudioSamplePacking newSampleDataPacking;
      newSampleDataPacking = CAudioInfo::querySampleDataPacking(valueString);

      retVal = CAudioInfo::isSampleDataPackingValid(newSampleDataPacking);
      if (retVal != 0)
         return retVal;

      setSampleDataPacking(newSampleDataPacking);
      }

   if (iniFile->KeyExists(sectionName, samplesPerFieldKey))
      {
      double newSamplesPerField;
      newSamplesPerField = iniFile->ReadDouble(sectionName, samplesPerFieldKey,
                                               -1.0);
      if (newSamplesPerField < 1.0)
         return FORMAT_ERROR_INVALID_SAMPLES_PER_FIELD;

      setSamplesPerField(newSamplesPerField);
      }

   // Success
   return 0;

} // readClipInitSection

//------------------------------------------------------------------------
//
// Function:    writeSection
//
// Description:
//
// Arguments
//
// Returns:
//
//------------------------------------------------------------------------
int CAudioFormat::writeSection(CIniFile *iniFile, const string& sectionPrefix)
{
   int retVal;

   string sectionName = sectionPrefix + "\\" + audioFormatSectionName;

   retVal = CMediaFormat::writeSection(iniFile, sectionName);
   if (retVal != 0)
      {
      // ERROR: Could not write base class CMediaFormat's portion
      //        of the Audio Format section
      return retVal;
      }

   // Write Channel Count
   iniFile->WriteInteger(sectionName, channelCountKey, getChannelCount());

   // Write Sample Rate (samples per second)
   string sampleRateString;
   if (getSampleRateEnum() == AI_SAMPLE_RATE_NON_STANDARD)
      {
      // Non-standard sample rate, so convert sample rate double to a string
      // using default formatting of MTIostringstream
      MTIostringstream ostr;
      ostr << getSampleRate();
      sampleRateString = ostr.str();
      }
   else
      {
      // If the sampleRateEnum is not non-standard, assume it is a standard
      // sample rate.  Given the standard sample rate, get a string that
      // corresponds to the standard samples-per-second (this avoids problems
      // with floating point formating and rounding)
      sampleRateString = CAudioInfo::querySampleRateName(getSampleRateEnum());
      }
   iniFile->WriteString(sectionName, sampleRateKey, sampleRateString);

   // Write Data Type
   iniFile->WriteString(sectionName, sampleDataTypeKey,
                    CAudioInfo::querySampleDataTypeName(getSampleDataType()));

   // Write Sample Data Size
   iniFile->WriteInteger(sectionName, sampleDataSizeKey, getSampleDataSize());

   // Write Sample Data Packing
   iniFile->WriteString(sectionName, sampleDataPackingKey,
               CAudioInfo::querySampleDataPackingName(getSampleDataPacking()));

   // Write Samples Per Field
   iniFile->WriteDouble(sectionName, samplesPerFieldKey, getSamplesPerField());

   return 0;

} // writeSection

int CAudioFormat::writeClipInitSection(CIniFile *iniFile,
                                        const string& sectionPrefix)
{
   return writeSection(iniFile, sectionPrefix);
}

//////////////////////////////////////////////////////////////////////
// Test and Debugging Functions
//////////////////////////////////////////////////////////////////////

void CAudioFormat::dump(MTIostringstream &str) const
{
   str << "Audio Format" << std::endl;
   CMediaFormat::dump(str);

   str << "  channelCount: " << channelCount
       << "  sampleRate: " << sampleRate << " (" << sampleRateEnum << ")"
       << std::endl;
   str << "  samplesPerField: " << samplesPerField << std::endl;
   str << "  sampleDataType: " << sampleDataType
       << "  sampleDataSize: " << sampleDataSize
       << "  sampleDataPacking: " << sampleDataPacking << std::endl;
   str << "  sampleDataEndian: " << sampleDataEndian << std::endl;

}



