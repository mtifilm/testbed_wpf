// ImageFormat3.cpp: implementation of the CImageFormat class.
//
//////////////////////////////////////////////////////////////////////

#include "err_format.h"
#include "ImageFormat3.h"
#include "IniFile.h"
#include <math.h>
#include <typeinfo>

//////////////////////////////////////////////////////////////////////
// Static Data Members Initialization
//////////////////////////////////////////////////////////////////////

// Clip File Image Format Section
string CImageFormat::horizontalIntervalPixelsKey = "HorizontalIntervalPixels";
string CImageFormat::imageFormatSectionName = "ImageFormat";
string CImageFormat::imageFormatTypeKey = "ImageFormatType";
string CImageFormat::pixelComponentsKey = "PixelComponents";
string CImageFormat::colorSpaceKey = "ColorSpace";
string CImageFormat::pixelPackingKey = "PixelPacking";
string CImageFormat::realPixelPackingKey = "RealPixelPacking"; // Stupid Y format back-compatibility hack!
string CImageFormat::linesPerFrameKey = "LinesPerFrame";
string CImageFormat::pixelsPerLineKey = "PixelsPerLine";
string CImageFormat::pixelAspectRatioKey = "PixelAspectRatio";
string CImageFormat::frameAspectRatioKey = "FrameAspectRatio";
string CImageFormat::verticalIntervalRowsKey = "VerticalIntervalRows";
static string alphaMatteTypeKey = "AlphaMatteType";
static string alphaMatteHiddenTypeKey = "AlphaMatteHiddenType";
string CImageFormat::gammaKey = "Gamma";
string CImageFormat::transferCharacteristicKey = "TransferCharacteristic";

// Color Space Section
string CImageColorSpace::sectionNameSuffix = "ColorSpace";
string CImageColorSpace::componentMinKey = "ComponentMin";
string CImageColorSpace::componentMaxKey = "ComponentMax";
string CImageColorSpace::componentBlackKey = "ComponentBlack";
string CImageColorSpace::componentWhiteKey = "ComponentWhite";
string CImageColorSpace::componentFillKey = "ComponentFill";

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Default Constructor
CImageFormat::CImageFormat() : CMediaFormat(MEDIA_FORMAT_TYPE_VIDEO3), imageFormatType(IF_TYPE_INVALID),
      pixelComponents(IF_PIXEL_COMPONENTS_INVALID), colorSpace(IF_COLOR_SPACE_INVALID), linesPerFrame(1),
      // haha I would LOVE to know why these
      pixelsPerLine(1), // are inited to 1 instead of 0!!!
      pixelAspectRatio(1.0), frameAspectRatio(1.0), pixelPacking(IF_PIXEL_PACKING_INVALID), bytesPerField(1), alphaBytesPerField(0),
      bytesPerFieldExcAlpha(1), framePitch(1), verticalIntervalRows(0), horizontalIntervalPixels(0), alphaMatteType(IF_ALPHA_MATTE_INVALID),
      alphaMatteHiddenType(IF_ALPHA_MATTE_INVALID), gamma(1.0), transferCharacteristic(IF_COLOR_SPACE_INVALID)
{
}

// Copy Constructor
CImageFormat::CImageFormat(const CImageFormat &src) : CMediaFormat(src.getMediaFormatType())
{
   *this = src; // Use assignment operator for deep copy
}

CImageFormat::~CImageFormat()
{
}

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// Function:    operator =
//
// Description:
//
// Arguments
//
// Returns:
//
// ------------------------------------------------------------------------
CImageFormat& CImageFormat:: operator = (const CImageFormat & rhs)
{
   if (this == &rhs)
   { // Check for self-assignment
      return*this;
   }

   // Copy direct base class's member variables
   CMediaFormat:: operator = (rhs);

   imageFormatType = rhs.imageFormatType;
   pixelComponents = rhs.pixelComponents;
   colorSpace = rhs.colorSpace;
   gamma = rhs.gamma;
   transferCharacteristic = rhs.transferCharacteristic;
   pixelPacking = rhs.pixelPacking;
   linesPerFrame = rhs.linesPerFrame;
   pixelsPerLine = rhs.pixelsPerLine;
   pixelAspectRatio = rhs.pixelAspectRatio;
   frameAspectRatio = rhs.frameAspectRatio;
   verticalIntervalRows = rhs.verticalIntervalRows;
   horizontalIntervalPixels = rhs.horizontalIntervalPixels;
   alphaMatteType = rhs.alphaMatteType;
   alphaMatteHiddenType = rhs.alphaMatteHiddenType;

   colorSpaceValues = rhs.colorSpaceValues;

   resetBytesPerField();

   return *this;
}

CMediaFormat& CImageFormat::assign(const CMediaFormat &src)
{
   if (this == &src)
   { // Check for self-assignment
      return*this;
   }

   // If the types do not match, throw an exception.  This should
   // never happen unless there is a coding or logic error
   if (getMediaFormatType() != src.getMediaFormatType())
   {
      throw std:: bad_cast();
   }

   *this = static_cast< const CImageFormat&>(src);

   return *this;
}

bool CImageFormat::compare(const CImageFormat &rhs, bool allowImageFileExceptions) const
{
   if (this == &rhs)
   { // Check for self-compare
      return true;
   }

   // Compare direct base class's member variables
   if (!CMediaFormat::compare(rhs))
   {
      return false;
   }

   // These always apply
   if (linesPerFrame != rhs.linesPerFrame)
   {
      return false;
   }

   if (pixelsPerLine != rhs.pixelsPerLine)
   {
      return false;
   }

   if (verticalIntervalRows != rhs.verticalIntervalRows)
   {
      return false;
   }

   if (horizontalIntervalPixels != rhs.horizontalIntervalPixels)
   {
      return false;
   }

   // We are much looser when dealing with image files
   if (!allowImageFileExceptions)
   {
      if (imageFormatType != rhs.imageFormatType)
      {
         return false;
      }

      if (pixelComponents != rhs.pixelComponents)
      { // QQQ added
         return false;
      }

      if (colorSpace != rhs.colorSpace)
      {
         return false;
      }

      if (gamma != rhs.gamma)
      {
         return false;
      }

      if (transferCharacteristic != rhs.transferCharacteristic)
      {
         return false;
      }

      if (pixelPacking != rhs.pixelPacking)
      {
         return false;
      }

      if (fabs(pixelAspectRatio - rhs.pixelAspectRatio) > 0.001)
      {
         return false;
      }

      if (fabs(frameAspectRatio - rhs.frameAspectRatio) > 0.001)
      {
         return false;
      }

      if (bytesPerField != rhs.bytesPerField)
      {
         return false;
      }

      if (alphaBytesPerField != rhs.alphaBytesPerField)
      {
         return false;
      }

      if (bytesPerFieldExcAlpha != rhs.bytesPerFieldExcAlpha)
      {
         return false;
      }

      if (framePitch != rhs.framePitch)
      {
         return false;
      }
      if (!colorSpaceValues.compare(rhs.colorSpaceValues, getComponentCount()))
      {
         return false;
      }

      if (alphaMatteType != rhs.alphaMatteType)
      {
         return false;
      }

      // doesn't seem right to enforce equality of hidden values
      // if (alphaMatteHiddenType != rhs.alphaMatteHiddebType)
      // return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////

RECT CImageFormat::getActivePictureRect() const
{
   RECT rect;

   rect.left = (int)horizontalIntervalPixels;
   rect.top = (int)verticalIntervalRows;
   rect.right = (int)(rect.left + pixelsPerLine - 1);
   rect.bottom = (int)(rect.top + linesPerFrame - 1);

   return rect;
}

EAlphaMatteType CImageFormat::getAlphaMatteType() const
{
   return alphaMatteType;
}

EAlphaMatteType CImageFormat::getAlphaMatteHiddenType() const
{
   return alphaMatteHiddenType;
}

bool CImageFormat::hasNonHiddenAlphaMatte() const
{
   bool retVal = ((alphaMatteType == IF_ALPHA_MATTE_1_BIT)
               || (alphaMatteType == IF_ALPHA_MATTE_8_BIT)
               || (alphaMatteType == IF_ALPHA_MATTE_16_BIT)
               || (alphaMatteType == IF_ALPHA_MATTE_2_BIT_EMBEDDED)
               || (alphaMatteType == IF_ALPHA_MATTE_8_BIT_INTERLEAVED)
               || (alphaMatteType == IF_ALPHA_MATTE_10_BIT_INTERLEAVED)
               || (alphaMatteType == IF_ALPHA_MATTE_16_BIT_INTERLEAVED));
   return retVal;
}

MTI_UINT8 CImageFormat::getBitsPerComponent() const
{
   return CImageInfo::queryBitsPerComponent(getPixelPacking());
}

MTI_UINT32 CImageFormat::getBytesPerField() const
{
   return bytesPerField;
}

// I hate this
MTI_UINT32 CImageFormat::getBytesPerFieldExcAlpha() const
{
   return bytesPerFieldExcAlpha;
}

// This too
MTI_UINT32 CImageFormat::getAlphaBytesPerField() const
{
   return alphaBytesPerField;
}

EColorSpace CImageFormat::getColorSpace() const
{
   return colorSpace;
}

MTI_REAL32 CImageFormat::getGamma() const
{
   return gamma;
}

EColorSpace CImageFormat::getTransferCharacteristic() const
{
   return transferCharacteristic;
}

int CImageFormat::getComponentCount() const
{
   // Returns the number of separate components in this image format
   // E.g., YUV422 and YUV444 both return 3

   return CImageInfo::queryNominalComponentCount(getPixelComponents());
}

int CImageFormat::getComponentCountExcAlpha() const
{
   int retVal = CImageInfo::queryNominalComponentCount(getPixelComponents());
   if (pixelComponents == IF_PIXEL_COMPONENTS_RGBA || pixelComponents == IF_PIXEL_COMPONENTS_YUV4224)
   {
      retVal = 3;
   }

   return retVal;
}

int CImageFormat::getComponentsPerPixel() const
{
   // Returns the number of components in a single pixel.
   // E.g., YUV422 returns 2 and YUV444 returns 3

   return CImageInfo::queryNominalComponentsPerPixel(getPixelComponents());
}

CImageColorSpace const * CImageFormat::getColorSpaceValues() const
{
   return &colorSpaceValues;
}

void CImageFormat::getComponentValueBlack(MTI_UINT16 componentValues[]) const
{
   // Component values for Black
   componentValues[0] = colorSpaceValues.componentBlack[0];
   componentValues[1] = colorSpaceValues.componentBlack[1];
   componentValues[2] = colorSpaceValues.componentBlack[2];
}

void CImageFormat::getComponentValueWhite(MTI_UINT16 componentValues[]) const
{
   // Component values for White
   componentValues[0] = colorSpaceValues.componentWhite[0];
   componentValues[1] = colorSpaceValues.componentWhite[1];
   componentValues[2] = colorSpaceValues.componentWhite[2];
}

void CImageFormat::getComponentValueFill(MTI_UINT16 componentValues[]) const
{
   // Component values for filling by image processing
   componentValues[0] = colorSpaceValues.componentFill[0];
   componentValues[1] = colorSpaceValues.componentFill[1];
   componentValues[2] = colorSpaceValues.componentFill[2];
}

void CImageFormat::getComponentValueMin(MTI_UINT16 componentValues[]) const
{
   // Minimum legal component values
   componentValues[0] = colorSpaceValues.componentMin[0];
   componentValues[1] = colorSpaceValues.componentMin[1];
   componentValues[2] = colorSpaceValues.componentMin[2];
}

void CImageFormat::getComponentValueMax(MTI_UINT16 componentValues[]) const
{
   // Maximum legal component values
   componentValues[0] = colorSpaceValues.componentMax[0];
   componentValues[1] = colorSpaceValues.componentMax[1];
   componentValues[2] = colorSpaceValues.componentMax[2];
}

void CImageFormat::getComponentValueBlackFill(MTI_UINT16 componentValues[]) const
{
   getComponentValueBlack(componentValues);

   // A HACK - per Larry: if the BLACK point isn't specified (i.e. it equals
   // the MIN value) ust the "legal" value which is based on 16 in 8-bit land.
   if (colorSpaceValues.componentMin[0] == componentValues[0] && colorSpaceValues.componentMin[1] == componentValues[1]
         && colorSpaceValues.componentMin[2] == componentValues[2])
   {
      componentValues[0] = (MTI_UINT16)((((MTI_UINT32)colorSpaceValues.componentMax[0] + 1) * 16) / 256);
      componentValues[1] = (MTI_UINT16)((((MTI_UINT32)colorSpaceValues.componentMax[1] + 1) * 16) / 256);
      componentValues[2] = (MTI_UINT16)((((MTI_UINT32)colorSpaceValues.componentMax[2] + 1) * 16) / 256);
   }
}

void CImageFormat::getComponentValueWhiteFill(MTI_UINT16 componentValues[]) const
{
   getComponentValueWhite(componentValues);

   // A HACK - per Larry: if the WHITE point isn't specified (i.e. it equals
   // the MAX value) ust the "legal" value which is based on 235 in 8-bit land.
   if (colorSpaceValues.componentMax[0] == componentValues[0] && colorSpaceValues.componentMax[1] == componentValues[1]
         && colorSpaceValues.componentMax[2] == componentValues[2])
   {
      componentValues[0] = (MTI_UINT16)((((MTI_UINT32)colorSpaceValues.componentMax[0] + 1) * 235) / 256);
      componentValues[1] = (MTI_UINT16)((((MTI_UINT32)colorSpaceValues.componentMax[1] + 1) * 235) / 256);
      componentValues[2] = (MTI_UINT16)((((MTI_UINT32)colorSpaceValues.componentMax[2] + 1) * 235) / 256);
   }
}

MTI_REAL32 CImageFormat::getFrameAspectRatio() const
{
   return frameAspectRatio;
}

MTI_UINT32 CImageFormat::getFramePitch() const
{
   // Frame pitch, in bytes
   return framePitch;
}

MTI_UINT32 CImageFormat::getHorizontalIntervalPixels() const
{
   // Number of pixels in horizontal interval.  Assumed to be to the
   // left of the active picture region
   return horizontalIntervalPixels;
}

EImageFormatType CImageFormat::getImageFormatType() const
{
   return imageFormatType;
}

MTI_UINT32 CImageFormat::getLinesPerFrame() const
{
   // Number of lines per frame in active picture region
   return linesPerFrame;
}

MTI_REAL32 CImageFormat::getNominalPixelAspectRatio() const
{
   return CImageInfo::queryNominalPixelAspectRatio(getImageFormatType());
}

MTI_REAL32 CImageFormat::getPixelAspectRatio() const
{
   return pixelAspectRatio;
}

EPixelComponents CImageFormat::getPixelComponents() const
{
   return pixelComponents;
}

EPixelPacking CImageFormat::getPixelPacking() const
{
   return pixelPacking;
}

MTI_UINT32 CImageFormat::getPixelsPerLine() const
{
   // Number of pixels per line in active picture region
   return pixelsPerLine;
}

MTI_UINT32 CImageFormat::getTotalFrameHeight() const
{
   // Total height, in lines, of the frame, including both the active
   // picture region and the vertical interval rows
   return linesPerFrame + verticalIntervalRows;
}

MTI_UINT32 CImageFormat::getTotalFrameWidth() const
{
   // Total width, in pixels, of the frame, including both the active
   // picture region and the horizontal interval pixels
   return pixelsPerLine + horizontalIntervalPixels;
}

MTI_UINT32 CImageFormat::getVerticalIntervalRows() const
{
   return verticalIntervalRows;
}

EImageResolution CImageFormat::getImageResolution() const
{
   if (pixelsPerLine < 1000)
   {
      return IF_IMAGE_RESOLUTION_STANDARD_DEF;
   }
   else if (pixelsPerLine < 2000)
   {
      return IF_IMAGE_RESOLUTION_HIGH_DEF;
   }
   else if (pixelsPerLine < 4000)
   {
      return IF_IMAGE_RESOLUTION_2K_DATA;
   }
   else if (pixelsPerLine < 6000)
   {
      return IF_IMAGE_RESOLUTION_4K_DATA;
   }
   else if (pixelsPerLine < 8000)
   {
      return IF_IMAGE_RESOLUTION_6K_DATA;
   }
   else
   {
      return IF_IMAGE_RESOLUTION_8K_DATA;
   }
}

//////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////

void CImageFormat::setAlphaMatteType(EAlphaMatteType newAlphaMatteType)
{
   alphaMatteType = newAlphaMatteType;
   resetBytesPerField();
}

void CImageFormat::setAlphaMatteHiddenType(EAlphaMatteType newAlphaMatteType)
{
   alphaMatteHiddenType = newAlphaMatteType;
   resetBytesPerField();
}

void CImageFormat::setColorSpace(EColorSpace newColorSpace)
{
   colorSpace = newColorSpace;
}

void CImageFormat::setGamma(MTI_REAL32 newGamma)
{
   gamma = newGamma;
}

void CImageFormat::setTransferCharacteristic(EColorSpace newTransferCharacteristic)
{
   transferCharacteristic = newTransferCharacteristic;
}

void CImageFormat::setColorSpaceValues(const CImageFormat &nativeImageFormat)
{
   // Copy color space values directly from native image format without
   // converting to 16-bit range.
   colorSpaceValues = nativeImageFormat.colorSpaceValues;
}

void CImageFormat::setComponentValueBlack(MTI_UINT16 componentValues[])
{
   colorSpaceValues.componentBlack[0] = componentValues[0];
   colorSpaceValues.componentBlack[1] = componentValues[1];
   colorSpaceValues.componentBlack[2] = componentValues[2];
}

void CImageFormat::setComponentValueFill(MTI_UINT16 componentValues[])
{
   colorSpaceValues.componentFill[0] = componentValues[0];
   colorSpaceValues.componentFill[1] = componentValues[1];
   colorSpaceValues.componentFill[2] = componentValues[2];
}

void CImageFormat::setComponentValueMax(MTI_UINT16 componentValues[])
{
   colorSpaceValues.componentMax[0] = componentValues[0];
   colorSpaceValues.componentMax[1] = componentValues[1];
   colorSpaceValues.componentMax[2] = componentValues[2];
}

void CImageFormat::setComponentValueMin(MTI_UINT16 componentValues[])
{
   colorSpaceValues.componentMin[0] = componentValues[0];
   colorSpaceValues.componentMin[1] = componentValues[1];
   colorSpaceValues.componentMin[2] = componentValues[2];
}

void CImageFormat::setComponentValueWhite(MTI_UINT16 componentValues[])
{
   colorSpaceValues.componentWhite[0] = componentValues[0];
   colorSpaceValues.componentWhite[1] = componentValues[1];
   colorSpaceValues.componentWhite[2] = componentValues[2];
}

void CImageFormat::setFrameAspectRatio(MTI_REAL32 newFrameAspectRatio)
{
   frameAspectRatio = newFrameAspectRatio;

   // Recalculate the pixelAspectRatio to match the new pixelAspectRatio
   if (linesPerFrame > 0)
   {
      pixelAspectRatio = frameAspectRatio / ((double)pixelsPerLine / (double)linesPerFrame);
   }
   else
   {
      pixelAspectRatio = 1.0; // Default aspect ratio if linePerFrame not set
   }
}

void CImageFormat::setHorizontalIntervalPixels(MTI_UINT32 newHorizontalIntervalPixels)
{
   horizontalIntervalPixels = newHorizontalIntervalPixels;

   // Recalculate number of bytes per field
   resetBytesPerField();
}

void CImageFormat::setImageFormatType(EImageFormatType newImageFormat)
{
   imageFormatType = newImageFormat;
}

void CImageFormat::setInterlaced(bool newInterlaced)
{
   CMediaFormat::setInterlaced(newInterlaced);

   resetBytesPerField();
}

void CImageFormat::setLinesPerFrame(MTI_UINT32 newLinesPerFrame)
{
   linesPerFrame = newLinesPerFrame;

   // Recalculate number of bytes per field
   resetBytesPerField();
}

void CImageFormat::setPixelAspectRatio(MTI_REAL32 newPixelAspectRatio)
{
   pixelAspectRatio = newPixelAspectRatio;

   // Recalculate the frameAspectRatio to match the new pixelAspectRatio
   if (linesPerFrame > 0)
   {
      frameAspectRatio = pixelAspectRatio * ((double)pixelsPerLine / (double)linesPerFrame);
   }
   else
   {
      frameAspectRatio = 0.0; // Default aspect ratio if linePerFrame not set
   }
}

void CImageFormat::setPixelComponents(EPixelComponents newPixelComponents)
{
   pixelComponents = newPixelComponents;

   // Recalculate number of bytes per field
   resetBytesPerField();
}

void CImageFormat::setPixelPacking(EPixelPacking newPixelPacking)
{
   pixelPacking = newPixelPacking;

   // Recalculate number of bytes per field
   resetBytesPerField();
}

void CImageFormat::setPixelsPerLine(MTI_UINT32 newPixelsPerLine)
{
   pixelsPerLine = newPixelsPerLine;

   // Recalculate number of bytes per field
   resetBytesPerField();
}

void CImageFormat::setVerticalIntervalRows(MTI_UINT32 newVerticalIntervalRows)
{
   verticalIntervalRows = newVerticalIntervalRows;

   // Recalculate number of bytes per field
   resetBytesPerField();
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// Function:    setToDefaults
//
// Description: Sets all parameters in an CImageFormat instance to
// default values given the basic parameters of
// Image Format Type and Pixel Packing type
//
// Arguments:   EImageFormatType newImageFormatType   Image format type
// int newPixelPacking                   Pixel packing code
//
// Returns:     0 indicates Success,
// Non-zero is an error and CImageFormat contents are undefined
//
//////////////////////////////////////////////////////////////////////
int CImageFormat::setToDefaults(EImageFormatType newImageFormatType, EPixelPacking newPixelPacking)
{
   int retVal;
   EPixelComponents defaultPixelComponents;
   EColorSpace defaultColorSpace;
   MTI_UINT32 defaultLinesPerFrame;
   MTI_UINT32 defaultPixelsPerLine;
   MTI_REAL32 defaultPixelAspectRatio;
   MTI_REAL32 defaultFrameAspectRatio;
   MTI_UINT32 defaultVerticalIntervalRows;
   MTI_UINT32 defaultHorizontalIntervalPixels;
   bool defaultInterlaced;
   MTI_REAL32 defaultGamma;
   EColorSpace defaultTransferCharacteristic;
   EAlphaMatteType defaultAlphaMatteType;
   EAlphaMatteType defaultAlphaMatteHiddenType;

   // Check the image format type
   retVal = CImageInfo::isImageFormatTypeValid(newImageFormatType);
   if (retVal != 0)
   {
      return retVal;
   }

   // Get default values
   defaultLinesPerFrame = CImageInfo::queryNominalLinesPerFrame(newImageFormatType);
   defaultPixelsPerLine = CImageInfo::queryNominalPixelsPerLine(newImageFormatType);

   defaultColorSpace = CImageInfo::queryNominalColorSpace(newImageFormatType);
   defaultPixelComponents = CImageInfo::queryNominalPixelComponents(newImageFormatType);
   defaultFrameAspectRatio = CImageInfo::queryNominalFrameAspectRatio(newImageFormatType);
   defaultPixelAspectRatio = CImageInfo::queryNominalPixelAspectRatio(newImageFormatType);

   defaultInterlaced = CImageInfo::queryNominalInterlaced(newImageFormatType);
   defaultGamma = CImageInfo::queryNominalGamma(newImageFormatType);
   defaultTransferCharacteristic = CImageInfo::queryNominalTransferCharacteristic(newImageFormatType);
   // Vertical Interval Rows and Horizontal Interval Pixels
   defaultVerticalIntervalRows = 0;
   defaultHorizontalIntervalPixels = 0;

   defaultAlphaMatteType = IF_ALPHA_MATTE_NONE;
   // was:     = CImageInfo::queryNominalAlphaMatteType(newImageFormatType);
   defaultAlphaMatteHiddenType = IF_ALPHA_MATTE_NONE;

   // Set all values in the CImageFormat instance
   setColorSpace(defaultColorSpace);
   imageFormatType = newImageFormatType;
   CMediaFormat::setInterlaced(defaultInterlaced);
   linesPerFrame = defaultLinesPerFrame;
   pixelAspectRatio = defaultPixelAspectRatio;
   frameAspectRatio = defaultFrameAspectRatio;
   pixelComponents = defaultPixelComponents;
   pixelPacking = newPixelPacking;
   pixelsPerLine = defaultPixelsPerLine;
   verticalIntervalRows = defaultVerticalIntervalRows;
   horizontalIntervalPixels = defaultHorizontalIntervalPixels;
   alphaMatteType = defaultAlphaMatteType;
   alphaMatteHiddenType = defaultAlphaMatteHiddenType;
   gamma = defaultGamma;
   transferCharacteristic = defaultTransferCharacteristic;

   // Set Color Space default values
   colorSpaceValues.SetToDefaults(pixelComponents, colorSpace, CImageInfo::queryIsFormatVideo(imageFormatType), getBitsPerComponent());

   // Recalculate bytesPerField after linesPerFrame, verticalIntervalRows,
   // pixelsPerLine pixelComponents, pixelPacking, interlaced
   // and horizontalIntervalPixels have been set
   resetBytesPerField();

   // Success
   return 0;

} // setToDefaults

//////////////////////////////////////////////////////////////////////
//
// Function:    setToDefaults
//
// Description: Sets all parameters in an CImageFormat instance to
// default values given the basic parameters of
// Image Format Type and Pixel Packing type
//
// Arguments:   EImageFormatType newImageFormatType   Image format type
// int newPixelPacking                   Pixel packing code
// EPixelComponents newPixelComponents
// MTI_UINT32 newLinesPerFrame
// MTI_UINT32 newPixelsPerLine
//
// Returns:     0 indicates Success,
// Non-zero is an error and CImageFormat contents are undefined
//
//////////////////////////////////////////////////////////////////////
int CImageFormat::setToDefaults(EImageFormatType newImageFormatType, EPixelPacking newPixelPacking, EPixelComponents newPixelComponents,
      MTI_UINT32 newLinesPerFrame, MTI_UINT32 newPixelsPerLine)
{
   int retVal;

   retVal = setToDefaults(newImageFormatType, newPixelPacking);
   if (retVal != 0)
   {
      return retVal;
   } // ERROR

   // Set all values in the CImageFormat instance
   pixelComponents = newPixelComponents;
   linesPerFrame = newLinesPerFrame;
   pixelsPerLine = newPixelsPerLine;

   // Calculate the frame aspect ratio based on the frame dimensions and
   // the default pixel aspect ratio
   setPixelAspectRatio(pixelAspectRatio);

   // Set,again, Color Space default values, including caller's new pixel
   // components
   colorSpaceValues.SetToDefaults(pixelComponents, colorSpace, CImageInfo::queryIsFormatVideo(imageFormatType), getBitsPerComponent());

   // Recalculate bytesPerField after linesPerFrame, verticalIntervalRows,
   // pixelsPerLine pixelComponents and pixelPacking
   // have been set
   resetBytesPerField();

   // Success
   return 0;

} // setToDefaults

//////////////////////////////////////////////////////////////////////
//
// Function:    setToInternalFormat
//
// Description: Sets all parameters in an CImageFormat instance to
// Internal Format based on a native format CImageFormat.
// The Internal Format is always 16-bit progressive with
// three components for YUV and RGB and one component
// for Y-only monochrome.
//
// Arguments:   CImageFormat &nativeImageFormat
//
// Returns:     0 indicates Success,
// Non-zero is an error and CImageFormat contents are undefined
//
//////////////////////////////////////////////////////////////////////
int CImageFormat::setToInternalFormat(const CImageFormat &nativeImageFormat)
{
   // Image Format type is Internal
   EImageFormatType newImageFormatType = IF_TYPE_INTERNAL;

   // Pixel Components is either YUV 4:4:4 or RGB
   EPixelComponents newPixelComponents;
   EPixelComponents nativePixelComponents = nativeImageFormat.getPixelComponents();
   switch (nativePixelComponents)
   {
   case IF_PIXEL_COMPONENTS_RGB: // RGB
   case IF_PIXEL_COMPONENTS_BGR: // BGR (e.g. Targa file)
   case IF_PIXEL_COMPONENTS_YUV444: // CbYCr
//   case IF_PIXEL_COMPONENTS_LUMINANCE: // Luminance only, monochrome
   case IF_PIXEL_COMPONENTS_YUV4224: // YUV + alpha, CbYACrYA
      // Use native pixel components type
      newPixelComponents = nativePixelComponents;
      break;

   case IF_PIXEL_COMPONENTS_Y: // Y
   case IF_PIXEL_COMPONENTS_YYY: // YYY
      newPixelComponents = IF_PIXEL_COMPONENTS_YYY;
      break;

   case IF_PIXEL_COMPONENTS_RGBA: // RGB + alpha matte channel
   case IF_PIXEL_COMPONENTS_BGRA: // RGB + alpha matte channel
      // when extracted, we have an interleaved RGB + 16-bit alpha matte
      newPixelComponents = IF_PIXEL_COMPONENTS_RGB;
      break;

   case IF_PIXEL_COMPONENTS_YUV422: // CbYCrY
      // YUV422 explanded into YUV444
      newPixelComponents = IF_PIXEL_COMPONENTS_YUV444;
      break;

   case IF_PIXEL_COMPONENTS_INVALID:
   default:
      // Bad
      return FORMAT_ERROR_INVALID_PIXEL_COMPONENTS;
   }

   // Pixel Packing is always 16-bits, either float or unsigned int.
   bool isHalf = nativeImageFormat.getPixelPacking() == IF_PIXEL_PACKING_1_Half_IN_2Bytes;
   EPixelPacking newPixelPacking = isHalf ? IF_PIXEL_PACKING_1_Half_IN_2Bytes : IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_INT;

   // Progressive
   bool newInterlaced = false;

   // Set all image format values peculiar to Internal Format
   imageFormatType = newImageFormatType;
   pixelPacking = newPixelPacking;
   pixelComponents = newPixelComponents;
   CMediaFormat::setInterlaced(newInterlaced);

   // Copy other values as-is from the caller's native image format
   setColorSpace(nativeImageFormat.getColorSpace());
   linesPerFrame = nativeImageFormat.getLinesPerFrame();
   pixelAspectRatio = nativeImageFormat.getPixelAspectRatio();
   frameAspectRatio = nativeImageFormat.getFrameAspectRatio();
   pixelsPerLine = nativeImageFormat.getPixelsPerLine();
   verticalIntervalRows = nativeImageFormat.getVerticalIntervalRows();
   horizontalIntervalPixels = nativeImageFormat.getHorizontalIntervalPixels();
   alphaMatteType = nativeImageFormat.getAlphaMatteType();
   alphaMatteHiddenType = nativeImageFormat.getAlphaMatteHiddenType();
   gamma = nativeImageFormat.gamma;
   transferCharacteristic = nativeImageFormat.transferCharacteristic;

   // Copy color space values directly from native image format without
   // converting to 16-bit range.
   colorSpaceValues = nativeImageFormat.colorSpaceValues;

   // Recalculate bytesPerField after linesPerFrame, verticalIntervalRows,
   // pixelsPerLine pixelComponents, pixelPacking, interlaced
   // and horizontalIntervalPixels have been set
   resetBytesPerField();

   // if (alphaMatteType == IF_ALPHA_MATTE_8_BIT_INTERLEAVED)
   // {
   // bytesPerField += linesPerFrame * pixelsPerLine * 2;
   // }
   //
   // if (alphaMatteType == IF_ALPHA_MATTE_10_BIT_INTERLEAVED
   // || alphaMatteType == IF_ALPHA_MATTE_16_BIT_INTERLEAVED)
   // {
   // bytesPerField += linesPerFrame * pixelsPerLine * 2;
   // }

   // Success
   return 0;

} // setToDefaults

//////////////////////////////////////////////////////////////////////
// Private Mutators
//////////////////////////////////////////////////////////////////////

void CImageFormat::resetBytesPerField()
{
   // Recalculate the number of bytes per field and frame pitch
   // if any of the following member variables have changed:
   // linesPerFrame, verticalIntervalRows, pixelsPerLine
   // pixelComponents, pixelPacking, interlaced

   // Calculate the number of bytes required to store a line of pixels
   MTI_UINT32 bytesPerLine;
   MTI_UINT32 totalPixelsPerLine = pixelsPerLine + horizontalIntervalPixels;
   bytesPerLine = CImageInfo::queryBytesForPixels(pixelComponents, pixelPacking, totalPixelsPerLine);

   // Calculate the frame pitch from the bytesPerLine and line alignment
   MTI_UINT32 lineAlignment;
   lineAlignment = CImageInfo::queryNominalLineAlignment(imageFormatType);
   framePitch = (bytesPerLine + lineAlignment - 1) / lineAlignment;
   framePitch *= lineAlignment;

   // Calculate bytes per frame
   MTI_UINT32 bytesPerFrame;
   bytesPerFrame = (linesPerFrame + verticalIntervalRows) * framePitch;

   // TBD: This may need to be adjusted for DVS since it does not include
   // the last line of vertical blanking interval data

   // Calculate the number of bytes per field by dividing bytesPerFrame
   // by the number of fields in a frame
   bytesPerField = bytesPerFrame / getFieldCount();

   // ...But these cases fall outside the box
   if (/* pixelComponents == IF_PIXEL_COMPONENTS_LUMINANCE
   || */ pixelComponents == IF_PIXEL_COMPONENTS_Y
   || pixelComponents == IF_PIXEL_COMPONENTS_YYY)
   {

      int pixels, triples, rowbyts;

      switch (pixelPacking)
      {

      case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

         pixels = totalPixelsPerLine * (linesPerFrame + verticalIntervalRows);

         // round up to the nearest triple
         triples = (pixels + 2) / 3;

         bytesPerFrame = triples * 4;

         bytesPerField = bytesPerFrame; // / getFieldCount()

         break;

#if 0 // may need to use this
      case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:

         rowbyts = ((totalPixelsPerLine + 3 / 4) * 5 + 3) & 0xfffffffc;

         bytesPerFrame = rowbyts * (linesPerFrame + verticalIntervalRows);

         bytesPerField = bytesPerFrame; // / getFieldCount()

         break;
#endif
      default:
         break;
      }
   }

   // For raw formats where the alpha matte is contained in a second image
   // (not interleaved), we need to jack up the buffer data size to include
   // that data. We (hopefully!) are guaranteed that the alpha data
   // immediately follows the RGB image data.
   int fieldPixelCount = linesPerFrame * pixelsPerLine / getFieldCount();
   switch (alphaMatteType)
   {
   case IF_ALPHA_MATTE_1_BIT:
      alphaBytesPerField = (fieldPixelCount + 7) / 8;
      break;
   case IF_ALPHA_MATTE_8_BIT:
      alphaBytesPerField = fieldPixelCount;
      break;
   case IF_ALPHA_MATTE_16_BIT:
      alphaBytesPerField = fieldPixelCount * 2;
      break;
   case IF_ALPHA_MATTE_2_BIT_EMBEDDED:
   case IF_ALPHA_MATTE_8_BIT_INTERLEAVED:
   case IF_ALPHA_MATTE_10_BIT_INTERLEAVED:
   case IF_ALPHA_MATTE_16_BIT_INTERLEAVED:
   case IF_ALPHA_MATTE_NONE:
      alphaBytesPerField = 0;
      break;
   default:
      // MTIassert(false);
      alphaBytesPerField = 0;
      break;
   }

   // For INTERNAL FORMAT buffers, the interleaved alpha was pulled out
   // so the buffers are indistiguishable from the non-interleaved alpha
   // buffers. In that case we need to account for the alpha as well.
   if (imageFormatType == IF_TYPE_INTERNAL)
   {
      switch (alphaMatteType)
      {
      case IF_ALPHA_MATTE_8_BIT_INTERLEAVED:
         alphaBytesPerField = fieldPixelCount * 1;
         break;

      case IF_ALPHA_MATTE_2_BIT_EMBEDDED:
         alphaBytesPerField = (fieldPixelCount + 3) / 4;
         break;

      case IF_ALPHA_MATTE_10_BIT_INTERLEAVED:
      case IF_ALPHA_MATTE_16_BIT_INTERLEAVED:
         alphaBytesPerField = fieldPixelCount * 2;
         break;

      default:
         break;
      }
   }

   // Add size of alpha matte to bytesPerField
   bytesPerFieldExcAlpha = bytesPerField;
   bytesPerField += alphaBytesPerField;

}

//////////////////////////////////////////////////////////////////////
// Validation functions
//////////////////////////////////////////////////////////////////////

int CImageFormat::validate() const
{
   int retVal;

   // Validate class super-types
   retVal = CMediaFormat::validate();
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate Image Format Type
   retVal = CImageInfo::isImageFormatTypeValid(getImageFormatType());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate color space
   retVal = CImageInfo::isColorSpaceValid(getColorSpace());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate frame aspect ratio
   retVal = CImageInfo::isFrameAspectRatioValid(getFrameAspectRatio());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate lines per frame
   retVal = CImageInfo::isLinesPerFrameValid(getLinesPerFrame());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate pixel aspect ratio
   retVal = CImageInfo::isPixelAspectRatioValid(getPixelAspectRatio());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate Pixel Components
   retVal = CImageInfo::isPixelComponentsValid(getPixelComponents());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate Pixel Packing
   retVal = CImageInfo::isPixelPackingValid(getPixelPacking());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate pixels per line
   retVal = CImageInfo::isPixelsPerLineValid(getPixelsPerLine());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate vertical interval rows
   retVal = CImageInfo::isVerticalIntervalRowsValid(getVerticalIntervalRows());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate horizontal interval pixels
   retVal = CImageInfo::isHorizontalIntervalPixelsValid(getHorizontalIntervalPixels());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate alpha matte type
   retVal = CImageInfo::isAlphaMatteTypeValid(getAlphaMatteType());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate alpha matte hidden type
   retVal = CImageInfo::isAlphaMatteTypeValid(getAlphaMatteHiddenType());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate gamma
   retVal = CImageInfo::isGammaValid(getGamma());
   if (retVal != 0)
   {
      return retVal;
   }

   // Validate transfer characteristics
   retVal = CImageInfo::isTransferCharacteristicValid(getTransferCharacteristic());
   if (retVal != 0)
   {
      return retVal;
   }

   return 0; // Success
}

//////////////////////////////////////////////////////////////////////
// File Interface
//////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// Function:    readImageFormatType
//
// Description:
//
// Arguments
//
// Returns:
//
// ------------------------------------------------------------------------
EImageFormatType CImageFormat::readImageFormatType(CIniFile *iniFile, const string& sectionPrefix)
{
   string sectionName;

   if (sectionPrefix.empty())
   {
      sectionName = imageFormatSectionName;
   }
   else
   {
      sectionName = sectionPrefix + "\\" + imageFormatSectionName;
   }

   // Verify that the Image Format section exists and the Image Format type
   // key exists within the section
   if (!iniFile->SectionExists(sectionName) || !iniFile->KeyExists(sectionName, imageFormatTypeKey))
   {
      // ERROR: Image Format key does not exist
      return IF_TYPE_INVALID;
   }

   // Some working strings
   string valueString, emptyString;

   // Read Image Format Type
   valueString = iniFile->ReadString(sectionName, imageFormatTypeKey, emptyString);
   EImageFormatType newImageFormatType;
   newImageFormatType = CImageInfo::queryImageFormatType(valueString);

   return newImageFormatType;
}

// ------------------------------------------------------------------------
//
// Function:    readSection
//
// Description:
//
// Arguments
//
// Returns:
//
// ------------------------------------------------------------------------
int CImageFormat::readSection(CIniFile *iniFile, const string& sectionPrefix)
{
   int retVal;

   string sectionName = sectionPrefix + "\\" + imageFormatSectionName;
   if (!iniFile->SectionExists(sectionName))
   {
      // ERROR: Image Format section does not exist
      return -341;
   }

   // Verify that the appropriate keys exist within the section
   // These keys are not optional and do not have default values
   if (!iniFile->KeyExists(sectionName, imageFormatTypeKey) || !iniFile->KeyExists(sectionName, pixelPackingKey))
   {
      // ERROR: Image Format section key does not exist
      return -342;
   }

   retVal = CMediaFormat::readSection(iniFile, sectionName);
   if (retVal != 0)
   {
      // ERROR: Could not read base class CMediaFormat's portion
      // of the Image Format section
      return retVal;
   }

   // Some working strings
   string valueString, defaultString, emptyString;

   // Read Image Format Type
   valueString = iniFile->ReadString(sectionName, imageFormatTypeKey, emptyString);
   EImageFormatType newImageFormatType;
   newImageFormatType = CImageInfo::queryImageFormatType(valueString);
   retVal = CImageInfo::isImageFormatTypeValid(newImageFormatType);
   if (retVal != 0)
   {
      return retVal;
   }

   // Read Pixel Packing
   // For Y format backwards compatibility, use RealPixelPacking if it exists.
   valueString = iniFile->ReadString(sectionName, realPixelPackingKey, emptyString);
   EPixelPacking newPixelPacking = CImageInfo::queryPixelPacking(valueString);
   // Careful! isPixelPackingValid returns 0 if TRUE!!!
   if (CImageInfo::isPixelPackingValid(newPixelPacking) != 0)
   {
      valueString = iniFile->ReadString(sectionName, pixelPackingKey, emptyString);
      newPixelPacking = CImageInfo::queryPixelPacking(valueString);
      retVal = CImageInfo::isPixelPackingValid(newPixelPacking);
      if (retVal != 0)
      {
         // ERROR
         return retVal;
      }
   }

   // Set Default values for this CImageFormat instance
   setToDefaults(newImageFormatType, newPixelPacking);

   // Read Interlaced if it is present in the ini file
   bool newInterlaced = getInterlaced();
   if (iniFile->KeyExists(sectionName, interlacedKey))
   {
      // Read Interlaced
      newInterlaced = iniFile->ReadBool(sectionName, interlacedKey, newInterlaced);
   }

   // Read Pixel Components
   defaultString = CImageInfo::queryPixelComponentsName(getPixelComponents());
   valueString = iniFile->ReadString(sectionName, pixelComponentsKey, defaultString);
   EPixelComponents newPixelComponents;
   newPixelComponents = CImageInfo::queryPixelComponents(valueString);
   retVal = CImageInfo::isPixelComponentsValid(newPixelComponents);
   if (retVal != 0)
   {
      // ERROR
      return retVal;
   }

   // Read Alpha Matte Type
   EAlphaMatteType newAlphaMatteType;
   if (iniFile->KeyExists(sectionName, alphaMatteTypeKey))
   {
      defaultString = CImageInfo::queryAlphaMatteTypeName(getAlphaMatteType());
      valueString = iniFile->ReadString(sectionName, alphaMatteTypeKey, defaultString);
      newAlphaMatteType = CImageInfo::queryAlphaMatteType(valueString);
      retVal = CImageInfo::isAlphaMatteTypeValid(newAlphaMatteType);
      if (retVal != 0)
      {
         return retVal;
      }
   }
   else
   {
      // Default if keyword is not present
      newAlphaMatteType = IF_ALPHA_MATTE_DEFAULT;
   }

   // Read Alpha Matte Hidden Type
   EAlphaMatteType newAlphaMatteHiddenType;
   if (iniFile->KeyExists(sectionName, alphaMatteHiddenTypeKey))
   {
      defaultString = CImageInfo::queryAlphaMatteTypeName(getAlphaMatteHiddenType());
      valueString = iniFile->ReadString(sectionName, alphaMatteHiddenTypeKey, defaultString);
      newAlphaMatteHiddenType = CImageInfo::queryAlphaMatteType(valueString);
      retVal = CImageInfo::isAlphaMatteTypeValid(newAlphaMatteHiddenType);
      if (retVal != 0)
      {
         return retVal;
      }
   }
   else
   {
      // Default if keyword is not present
      newAlphaMatteHiddenType = IF_ALPHA_MATTE_NONE;
   }

   // Read Color Space
   defaultString = CImageInfo::queryColorSpaceName(getColorSpace());
   valueString = iniFile->ReadString(sectionName, colorSpaceKey, defaultString);
   EColorSpace newColorSpace = CImageInfo::queryColorSpace(valueString);
   retVal = CImageInfo::isColorSpaceValid(newColorSpace);
   if (retVal != 0)
   {
      return retVal;
   }

   // Read Gamma
   double newGamma = iniFile->ReadDouble(sectionName, gammaKey, getGamma());

   // Read Transfer Characteristics (it uses Color Space values)
   // Note that is is OK for the TC to be IF_COLOR_SPACE_INVALID
   defaultString = "xyzzy";
   EColorSpace newTransferCharacteristic = getTransferCharacteristic();
   valueString = iniFile->ReadString(sectionName, transferCharacteristicKey, defaultString);
   if (valueString != defaultString)
   {
      newTransferCharacteristic = CImageInfo::queryColorSpace(valueString);
      // but if it's in a clip or scheme file, it must be a valid COLOR SPACE.
      // So in the following, I really do want IsColorSpaceValid(),
      // not IsTransferCharacteristicValid()
      retVal = CImageInfo::isColorSpaceValid(newTransferCharacteristic);
      if (retVal != 0)
      {
         return FORMAT_ERROR_INVALID_TRANSFER_CHARACTERISTICS;
      }
   }

   // Read Lines per Frame
   int newLinesPerFrame = iniFile->ReadInteger(sectionName, linesPerFrameKey, getLinesPerFrame());

   // Read Pixels per Line
   int newPixelsPerLine = iniFile->ReadInteger(sectionName, pixelsPerLineKey, getPixelsPerLine());

   // Read Pixel Aspect Ratio
   double newPixelAspectRatio = iniFile->ReadDouble(sectionName, pixelAspectRatioKey, getPixelAspectRatio());

   // Read Frame Aspect Ratio
   double newFrameAspectRatio = iniFile->ReadDouble(sectionName, frameAspectRatioKey, getFrameAspectRatio());

   // Read Vertical Interval Rows
   int newVerticalIntervalRows = iniFile->ReadInteger(sectionName, verticalIntervalRowsKey, getVerticalIntervalRows());

   int newHorizontalIntervalPixels = iniFile->ReadInteger(sectionName, horizontalIntervalPixelsKey, getHorizontalIntervalPixels());

   // Set all values in the CImageFormat instance
   CMediaFormat::setInterlaced(newInterlaced);
   setColorSpace(newColorSpace);
   setGamma(newGamma);
   setTransferCharacteristic(newTransferCharacteristic);
   linesPerFrame = newLinesPerFrame;
   pixelAspectRatio = newPixelAspectRatio;
   frameAspectRatio = newFrameAspectRatio;
   pixelComponents = newPixelComponents;
   pixelsPerLine = newPixelsPerLine;
   verticalIntervalRows = newVerticalIntervalRows;
   horizontalIntervalPixels = newHorizontalIntervalPixels;
   alphaMatteType = newAlphaMatteType;
   alphaMatteHiddenType = newAlphaMatteHiddenType;

   // More Y backward compatibility shit.
   if (pixelComponents == IF_PIXEL_COMPONENTS_Y)
   {
      if (pixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L)
      {
         pixelPacking = IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L;
      }
      else if (pixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R)
      {
         pixelPacking = IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R;
      }
   }

   // ******** EMBEDDED ALPHA HACK-O-RAMA (aka CINTEL_HACK )*************
   // unless specified otherwise in the clip scheme file,
   // we now assume that ALL 10-10-10-2 RGB DPX files
   // have alpha data embedded (the "alpha" bit is 1 if either of the
   // two padding bits, i.e. the "2" in 10-10-10-2, is non-zero).
   //
   if (getAlphaMatteType() == IF_ALPHA_MATTE_DEFAULT)
   {
      // if (getImageFormatType() == IF_TYPE_FILE_DPX &&
      // getPixelPacking() == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L &&
      // getPixelComponents() == IF_PIXEL_COMPONENTS_RGB)
      // {
      // setAlphaMatteType(IF_ALPHA_MATTE_1_BIT_CINTEL_HACK);
      // }
      // else
      // {
      setAlphaMatteType(IF_ALPHA_MATTE_NONE);
      // }
   }
   // *******************************************************************

   // Recalculate bytesPerField after linesPerFrame, verticalIntervalRows,
   // pixelsPerLine pixelComponents, pixelPacking, interlaced
   // and horizontalIntervalPixels have been set
   resetBytesPerField();

   // Read the Color Space component values
   retVal = colorSpaceValues.readSection(iniFile, sectionName);
   if (retVal != 0)
   {
      return retVal;
   }

   // Success
   return 0;

} // readSection

// ------------------------------------------------------------------------
//
// Function:    readClipInitSection
//
// Description:
//
// Arguments
//
// Returns:
//
// ------------------------------------------------------------------------
int CImageFormat::readClipInitSection(CIniFile *iniFile, const string& sectionPrefix)
{
   int retVal;

   string sectionName = sectionPrefix + "\\" + imageFormatSectionName;
   if (!iniFile->SectionExists(sectionName))
   {
      return 0;
   } // ERROR: Image Format section does not exist

   retVal = CMediaFormat::readClipInitSection(iniFile, sectionName);
   if (retVal != 0)
   {
      // ERROR: Could not read base class CMediaFormat's portion
      // of the Image Format section
      return retVal;
   }

   // Some working strings
   string valueString, defaultString, emptyString;

   // Read Image Format Type
   EImageFormatType newImageFormatType;
   if (iniFile->KeyExists(sectionName, imageFormatTypeKey))
   {
      valueString = iniFile->ReadString(sectionName, imageFormatTypeKey, emptyString);
      newImageFormatType = CImageInfo::queryImageFormatType(valueString);

      retVal = CImageInfo::isImageFormatTypeValid(newImageFormatType);
      if (retVal != 0)
      {
         return retVal;
      }
   }
   else
   {
      // The image format type is missing, so keep the current value
      newImageFormatType = getImageFormatType();
   }

   // Read Pixel Packing
   // For stupid Y format back compatibility hack, look for realPixelPacking first!
   valueString = iniFile->ReadString(sectionName, realPixelPackingKey, emptyString);
   EPixelPacking newPixelPacking = CImageInfo::queryPixelPacking(valueString);
   if (CImageInfo::isPixelPackingValid(newPixelPacking) == 0)
   {
      // We're good - nothing else to do.
   }
   else if (iniFile->KeyExists(sectionName, pixelPackingKey))
   {
      valueString = iniFile->ReadString(sectionName, pixelPackingKey, emptyString);
      newPixelPacking = CImageInfo::queryPixelPacking(valueString);

      retVal = CImageInfo::isPixelPackingValid(newPixelPacking);
      if (retVal != 0)
      {
         // ERROR
         return retVal;
      }
   }
   else
   {
      // The pixel packing type is missing, so keep the current value
      newPixelPacking = getPixelPacking();
   }

   // Set Default values for this CImageFormat instance
   setToDefaults(newImageFormatType, newPixelPacking);

   // Read Interlaced if it is present in the ini file
   if (iniFile->KeyExists(sectionName, interlacedKey))
   {
      // Read Interlaced
      bool newInterlaced = iniFile->ReadBool(sectionName, interlacedKey, getInterlaced());
      CMediaFormat::setInterlaced(newInterlaced);
   }

   // Read Pixel Components
   if (iniFile->KeyExists(sectionName, pixelComponentsKey))
   {
      valueString = iniFile->ReadString(sectionName, pixelComponentsKey, emptyString);
      EPixelComponents newPixelComponents;
      newPixelComponents = CImageInfo::queryPixelComponents(valueString);

      retVal = CImageInfo::isPixelComponentsValid(newPixelComponents);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      pixelComponents = newPixelComponents; // New value
   }

   // More Y backward compatibility shit.
   if (pixelComponents == IF_PIXEL_COMPONENTS_Y)
   {
      if (pixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L)
      {
         pixelPacking = IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L;
      }
      else if (pixelPacking == IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R)
      {
         pixelPacking = IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R;
      }
   }

   // Read Alpha Matte Type
   if (iniFile->KeyExists(sectionName, alphaMatteTypeKey))
   {
      defaultString = CImageInfo::queryAlphaMatteTypeName(getAlphaMatteType());
      valueString = iniFile->ReadString(sectionName, alphaMatteTypeKey, defaultString);
      EAlphaMatteType newAlphaMatteType = CImageInfo::queryAlphaMatteType(valueString);
      retVal = CImageInfo::isAlphaMatteTypeValid(newAlphaMatteType);
      if (retVal != 0)
      {
         return retVal;
      }

      setAlphaMatteType(newAlphaMatteType);
   }

   // Read Alpha Matte Hidden Type
   if (iniFile->KeyExists(sectionName, alphaMatteHiddenTypeKey))
   {
      defaultString = CImageInfo::queryAlphaMatteTypeName(getAlphaMatteHiddenType());
      valueString = iniFile->ReadString(sectionName, alphaMatteHiddenTypeKey, defaultString);
      EAlphaMatteType newAlphaMatteHiddenType = CImageInfo::queryAlphaMatteType(valueString);
      retVal = CImageInfo::isAlphaMatteTypeValid(newAlphaMatteHiddenType);
      if (retVal != 0)
      {
         return retVal;
      }

      setAlphaMatteHiddenType(newAlphaMatteHiddenType);
   }

   // Read Color Space
   if (iniFile->KeyExists(sectionName, colorSpaceKey))
   {
      valueString = iniFile->ReadString(sectionName, colorSpaceKey, emptyString);
      EColorSpace newColorSpace = CImageInfo::queryColorSpace(valueString);

      retVal = CImageInfo::isColorSpaceValid(newColorSpace);
      if (retVal != 0)
      {
         return retVal;
      }

      setColorSpace(newColorSpace); // New value
   }

   // Read Gamma
   if (iniFile->KeyExists(sectionName, gammaKey))
   {
      double newGamma = iniFile->ReadDouble(sectionName, gammaKey, -1.0);

      retVal = CImageInfo::isGammaValid(newGamma);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      setGamma(newGamma);
   }

   // Read Transfer Characteristics
   if (iniFile->KeyExists(sectionName, transferCharacteristicKey))
   {
      valueString = iniFile->ReadString(sectionName, transferCharacteristicKey, emptyString);
      if (valueString != emptyString)
      {
         EColorSpace newTransferCharacteristic = CImageInfo::queryColorSpace(valueString);

         // If it's in a clip or scheme file, it must be a valid COLOR SPACE.
         // So in the following, I really do want IsColorSpaceValid(),
         // not IsTransferCharacteristicValid()
         retVal = CImageInfo::isColorSpaceValid(newTransferCharacteristic);
         if (retVal != 0)
         {
            return FORMAT_ERROR_INVALID_TRANSFER_CHARACTERISTICS;
         }

         setTransferCharacteristic(newTransferCharacteristic);
      }
   }

   // Read Lines per Frame
   if (iniFile->KeyExists(sectionName, linesPerFrameKey))
   {
      int newLinesPerFrame = iniFile->ReadInteger(sectionName, linesPerFrameKey, -1);
      retVal = CImageInfo::isLinesPerFrameValid(newLinesPerFrame);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      linesPerFrame = newLinesPerFrame; // New value
   }

   // Read Pixels per Line
   if (iniFile->KeyExists(sectionName, pixelsPerLineKey))
   {
      int newPixelsPerLine = iniFile->ReadInteger(sectionName, pixelsPerLineKey, -1);

      retVal = CImageInfo::isPixelsPerLineValid(newPixelsPerLine);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      pixelsPerLine = newPixelsPerLine; // Set new value
   }

   bool needAspectRatioRecalc = true;

   // Read Pixel Aspect Ratio
   if (iniFile->KeyExists(sectionName, pixelAspectRatioKey))
   {
      double newPixelAspectRatio = iniFile->ReadDouble(sectionName, pixelAspectRatioKey, -1.0);

      retVal = CImageInfo::isPixelAspectRatioValid(newPixelAspectRatio);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      // Set Pixel Aspect Ratio and recalculate Frame Aspect Ratio
      setPixelAspectRatio(newPixelAspectRatio);
      needAspectRatioRecalc = false;
   }

   // Read Frame Aspect Ratio
   if (iniFile->KeyExists(sectionName, frameAspectRatioKey))
   {
      double newFrameAspectRatio = iniFile->ReadDouble(sectionName, frameAspectRatioKey, -1.0);
      retVal = CImageInfo::isFrameAspectRatioValid(newFrameAspectRatio);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      // Set Frame Aspect Ratio and recalculate Pixel Aspect Ratio
      setFrameAspectRatio(newFrameAspectRatio);
      needAspectRatioRecalc = false;
   }

   // Read Vertical Interval Rows
   if (iniFile->KeyExists(sectionName, verticalIntervalRowsKey))
   {
      int newVerticalIntervalRows = iniFile->ReadInteger(sectionName, verticalIntervalRowsKey, -1);

      retVal = CImageInfo::isVerticalIntervalRowsValid(newVerticalIntervalRows);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      verticalIntervalRows = newVerticalIntervalRows;
   }

   // Read Horizontal Interval Pixels
   if (iniFile->KeyExists(sectionName, horizontalIntervalPixelsKey))
   {
      int newHorizontalIntervalPixels = iniFile->ReadInteger(sectionName, horizontalIntervalPixelsKey, -1);

      retVal = CImageInfo::isHorizontalIntervalPixelsValid(newHorizontalIntervalPixels);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      horizontalIntervalPixels = newHorizontalIntervalPixels;
   }

   // Recalculate bytesPerField and framePitch after linesPerFrame,
   // verticalIntervalRows, pixelsPerLine pixelComponents, pixelPacking
   // interlaced and horizontalIntervalPixels have been set.
   resetBytesPerField();

   // Recalculate the frame aspect ratio based on the frame dimensions and
   // the  pixel aspect ratio
   if (needAspectRatioRecalc)
   {
      setPixelAspectRatio(pixelAspectRatio);
   }

   // Read the Color Space component values
   retVal = colorSpaceValues.readSection(iniFile, sectionName);
   if (retVal != 0)
   {
      return retVal;
   }

   // ******** EMBEDDED ALPHA HACK-O-RAMA (aka CINTEL_HACK )*************
   // unless specified otherwise in the clip scheme file,
   // we now assume that ALL 10-10-10-2 RGB DPX files
   // have alpha data embedded (the "alpha" bit is 1 if either of the
   // two padding bits, i.e. the "2" in 10-10-10-2, is non-zero).
   //
   if (getAlphaMatteType() == IF_ALPHA_MATTE_DEFAULT)
   {
      // if (getImageFormatType() == IF_TYPE_FILE_DPX &&
      // getPixelPacking() == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L &&
      // getPixelComponents() == IF_PIXEL_COMPONENTS_RGB)
      // {
      // setAlphaMatteType(IF_ALPHA_MATTE_1_BIT_CINTEL_HACK);
      // }
      // else
      // {
      setAlphaMatteType(IF_ALPHA_MATTE_NONE);
      // }
   }
   // *******************************************************************

   // Success
   return 0;

} // readClipInitSection

// ------------------------------------------------------------------------
//
// Function:    readClipInitSection2
//
// Description: Special-purpose function used to read the image format
// section of a Clip Scheme a second time.  This function
// is used to override parameters set from Image File
// with parameters that come from a Clip Scheme.  If the
// parameter is not present in the Clip Scheme, the existing
// value is left as it was.
//
// The parameters read are:
// Color Space
// Color Component Values
// Gamma
// Transfer Characteristics
// Pixel Aspect Ratio
// Frame Aspect Ratio
// Pixel Packing  (override only if bytes-per-pixels match)
//
// Arguments
//
// Returns:
//
// ------------------------------------------------------------------------
int CImageFormat::readClipInitSection2(CIniFile *iniFile, const string& sectionPrefix)
{
   int retVal;

   string sectionName = sectionPrefix + "\\" + imageFormatSectionName;
   if (!iniFile->SectionExists(sectionName))
   {
      return 0;
   } // ERROR: Image Format section does not exist

   // Some working strings
   string valueString, emptyString;

   // Read Pixel Components
   if (iniFile->KeyExists(sectionName, pixelComponentsKey))
   {
      valueString = iniFile->ReadString(sectionName, pixelComponentsKey, emptyString);
      EPixelComponents newPixelComponents;
      newPixelComponents = CImageInfo::queryPixelComponents(valueString);

      retVal = CImageInfo::isPixelComponentsValid(newPixelComponents);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      pixelComponents = newPixelComponents; // New value
   }

   // Read Color Space
   if (iniFile->KeyExists(sectionName, colorSpaceKey))
   {
      valueString = iniFile->ReadString(sectionName, colorSpaceKey, emptyString);
      EColorSpace newColorSpace = CImageInfo::queryColorSpace(valueString);

      retVal = CImageInfo::isColorSpaceValid(newColorSpace);
      if (retVal != 0)
      {
         return retVal;
      }

      setColorSpace(newColorSpace); // New value
   }

   // Read the Color Space component values
   retVal = colorSpaceValues.readSection(iniFile, sectionName);
   if (retVal != 0)
   {
      return retVal;
   }

   // Read Gamma
   if (iniFile->KeyExists(sectionName, gammaKey))
   {
      double newGamma = iniFile->ReadDouble(sectionName, gammaKey, -1.0);

      retVal = CImageInfo::isGammaValid(newGamma);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      setGamma(newGamma);
   }

   // Read Transfer Characteristics
   if (iniFile->KeyExists(sectionName, transferCharacteristicKey))
   {
      valueString = iniFile->ReadString(sectionName, transferCharacteristicKey, emptyString);
      EColorSpace newTransferCharacteristic = CImageInfo::queryColorSpace(valueString);

      // If it's in a clip or scheme file, it must be a valid COLOR SPACE.
      // So in the following, I really do want IsColorSpaceValid(),
      // not IsTransferCharacteristicValid()
      retVal = CImageInfo::isColorSpaceValid(newTransferCharacteristic);
      if (retVal != 0)
      {
         return retVal;
      }

      setTransferCharacteristic(newTransferCharacteristic); // New value
   }

   // Read Pixel Aspect Ratio
   if (iniFile->KeyExists(sectionName, pixelAspectRatioKey))
   {
      double newPixelAspectRatio = iniFile->ReadDouble(sectionName, pixelAspectRatioKey, -1.0);

      retVal = CImageInfo::isPixelAspectRatioValid(newPixelAspectRatio);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      // Set Pixel Aspect Ratio and recalculate Frame Aspect Ratio
      setPixelAspectRatio(newPixelAspectRatio);
   }

   // Read Frame Aspect Ratio
   if (iniFile->KeyExists(sectionName, frameAspectRatioKey))
   {
      double newFrameAspectRatio = iniFile->ReadDouble(sectionName, frameAspectRatioKey, -1.0);
      retVal = CImageInfo::isFrameAspectRatioValid(newFrameAspectRatio);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      // Set Frame Aspect Ratio and recalculate Pixel Aspect Ratio
      setFrameAspectRatio(newFrameAspectRatio);
   }

   // Read Pixel Packing (but replace only if the bytes-per-pixel's match)
   // QQQ not bothering to do the reapPixelPacking Y format backward compatibility hack here
   //     BECAUSE I HAVE NO FUCKING CLUE WHAT THIS SHIT IS. It seems to be associated with
   //     Clip Scheme files, which aren't of much use nowadays!
   if (iniFile->KeyExists(sectionName, pixelPackingKey))
   {
      valueString = iniFile->ReadString(sectionName, pixelPackingKey, "None");
      EPixelPacking newPixelPacking = CImageInfo::queryPixelPacking(valueString);
      retVal = CImageInfo::isPixelPackingValid(newPixelPacking);
      if (retVal != 0)
      {
         return retVal;
      } // ERROR

      if (CImageInfo::queryBytesPerPixel(pixelComponents, newPixelPacking) == CImageInfo::queryBytesPerPixel(pixelComponents, pixelPacking))
      {
         // Set Frame Aspect Ratio and recalculate Pixel Aspect Ratio
         setPixelPacking(newPixelPacking);
      }
   }

   // Read Alpha Matte Type
   EAlphaMatteType newAlphaMatteType;
   if (iniFile->KeyExists(sectionName, alphaMatteTypeKey))
   {
      string defaultString = CImageInfo::queryAlphaMatteTypeName(getAlphaMatteType());
      valueString = iniFile->ReadString(sectionName, alphaMatteTypeKey, defaultString);
      newAlphaMatteType = CImageInfo::queryAlphaMatteType(valueString);
      retVal = CImageInfo::isAlphaMatteTypeValid(newAlphaMatteType);
      if (retVal != 0)
      {
         return retVal;
      }
   }
   else
   {
      // // SPECIAL CASE: Set to 1_BIT_IN_PADDING if creating a
      // // 10-bit-RGB-DPX-file based clip
      // if (getImageFormatType() == IF_TYPE_FILE_DPX &&
      // getPixelPacking() == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L &&
      // getPixelComponents() == IF_PIXEL_COMPONENTS_RGB)
      // {
      // setAlphaMatteType(IF_ALPHA_MATTE_1_BIT_CINTEL_HACK);
      // }
   }

   // Success
   return 0;

} // readClipInitSection2

// ------------------------------------------------------------------------
//
// Function:    writeSection
//
// Description:
//
// Arguments
//
// Returns:
//
// ------------------------------------------------------------------------
int CImageFormat::writeSection(CIniFile *iniFile, const string& sectionPrefix)
{
   int retVal;

   string sectionName = sectionPrefix + "\\" + imageFormatSectionName;

   retVal = CMediaFormat::writeSection(iniFile, sectionName);
   if (retVal != 0)
   {
      // ERROR: Could not write base class CMediaFormat's portion
      // of the Image Format section
      return retVal;
   }

   // Write Image Format Type
   iniFile->WriteString(sectionName, imageFormatTypeKey, CImageInfo::queryImageFormatTypeName(getImageFormatType()));

   // Write Pixel Components
   iniFile->WriteString(sectionName, pixelComponentsKey, CImageInfo::queryPixelComponentsName(getPixelComponents()));

   // Write Color Space
   iniFile->WriteString(sectionName, colorSpaceKey, CImageInfo::queryColorSpaceName(getColorSpace()));

   // Write Gamma
   iniFile->WriteDouble(sectionName, gammaKey, getGamma());

   // Write Transfer Characteristics
   // But don't try to write if it's not really set
   if (getTransferCharacteristic() != IF_COLOR_SPACE_INVALID)
   {
      iniFile->WriteString(sectionName, transferCharacteristicKey, CImageInfo::queryColorSpaceName(getTransferCharacteristic()));
   }

   // Write Pixel Packing
   // Stupid backward compatibility hack: If 10-bit Y format, write bogus pixel packing
   // as PixelPacking, and write the real pixel packing as RealPixelPacking.
   if (getPixelComponents() == IF_PIXEL_COMPONENTS_Y && getPixelPacking() == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R)
   {
      iniFile->WriteString(sectionName, pixelPackingKey, CImageInfo::queryPixelPackingName(IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R));
      iniFile->WriteString(sectionName, realPixelPackingKey, CImageInfo::queryPixelPackingName(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R));
   }
   else if (getPixelComponents() == IF_PIXEL_COMPONENTS_Y && getPixelPacking() == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L)
   {
      iniFile->WriteString(sectionName, pixelPackingKey, CImageInfo::queryPixelPackingName(IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L));
      iniFile->WriteString(sectionName, realPixelPackingKey, CImageInfo::queryPixelPackingName(IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L));
   }
   else
   {
      iniFile->WriteString(sectionName, pixelPackingKey, CImageInfo::queryPixelPackingName(getPixelPacking()));
   }

   // Write Lines per Frame
   iniFile->WriteInteger(sectionName, linesPerFrameKey, getLinesPerFrame());

   // Write Pixels per Line
   iniFile->WriteInteger(sectionName, pixelsPerLineKey, getPixelsPerLine());

   // Write Pixel Aspect Ratio
   iniFile->WriteDouble(sectionName, pixelAspectRatioKey, getPixelAspectRatio());

   // Write Frame Aspect Ratio
   iniFile->WriteDouble(sectionName, frameAspectRatioKey, getFrameAspectRatio());

   // Write Vertical Interval Rows
   iniFile->WriteInteger(sectionName, verticalIntervalRowsKey, getVerticalIntervalRows());

   // Write Horizontal Interval Pixels
   iniFile->WriteInteger(sectionName, horizontalIntervalPixelsKey, getHorizontalIntervalPixels());

   // Write Alpha Matte Type
   iniFile->WriteString(sectionName, alphaMatteTypeKey, CImageInfo::queryAlphaMatteTypeName(getAlphaMatteType()));

   // Write Alpha Matte Hidden Type
   iniFile->WriteString(sectionName, alphaMatteHiddenTypeKey, CImageInfo::queryAlphaMatteTypeName(getAlphaMatteHiddenType()));

   retVal = colorSpaceValues.writeSection(iniFile, sectionName);
   if (retVal != 0)
   {
      return retVal;
   }

   return 0;

} // writeSection

int CImageFormat::writeClipInitSection(CIniFile *iniFile, const string& sectionPrefix)
{
   return writeSection(iniFile, sectionPrefix);
}

//////////////////////////////////////////////////////////////////////
// Serialize Methods
//////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// Function:    serialize
//
// Description:  Saves the class data
//
// Arguments  ostream - Stream to write to
//
// Returns:  true if success, false otherwise
//
// ------------------------------------------------------------------------
bool CImageFormat::serialize(ostream &osStream) const
{

   if (!CMediaFormat::serialize(osStream))
   {
      return false;
   }

   osStream.write((char *)&imageFormatType, sizeof(EImageFormatType));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&pixelComponents, sizeof(EPixelComponents));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&colorSpace, sizeof(EColorSpace));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&pixelPacking, sizeof(EPixelPacking));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&linesPerFrame, sizeof(MTI_UINT32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&pixelsPerLine, sizeof(MTI_UINT32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&pixelAspectRatio, sizeof(MTI_REAL32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&frameAspectRatio, sizeof(MTI_REAL32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&bytesPerField, sizeof(MTI_UINT32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&bytesPerFieldExcAlpha, sizeof(MTI_UINT32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&alphaBytesPerField, sizeof(MTI_UINT32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&framePitch, sizeof(MTI_UINT32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&verticalIntervalRows, sizeof(MTI_UINT32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&horizontalIntervalPixels, sizeof(MTI_UINT32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&alphaMatteType, sizeof(EAlphaMatteType));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&alphaMatteHiddenType, sizeof(EAlphaMatteType));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&gamma, sizeof(MTI_REAL32));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&transferCharacteristic, sizeof(EColorSpace));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   colorSpaceValues.serialize(osStream);

   return true;
}

// ------------------------------------------------------------------------
//
// Function:    deserialize
//
// Description:  Creates a class from the serialized data
//
// Arguments  isStream - Stream to read from
//
// Returns:  a CImageFormat pointer if success, Null otherwise
//
// ------------------------------------------------------------------------
CImageFormat *CImageFormat::deserialize(istream &isStream)
{
   CImageFormat *cif = new CImageFormat();

   cif->CMediaFormat::readFromStream(isStream);

   // NOTE: Failure creates a memory leak but I don't care

   isStream.read((char *)&cif->imageFormatType, sizeof(EImageFormatType));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->pixelComponents, sizeof(EPixelComponents));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->colorSpace, sizeof(EColorSpace));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->pixelPacking, sizeof(EPixelPacking));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->linesPerFrame, sizeof(MTI_UINT32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->pixelsPerLine, sizeof(MTI_UINT32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->pixelAspectRatio, sizeof(MTI_REAL32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->frameAspectRatio, sizeof(MTI_REAL32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->bytesPerField, sizeof(MTI_UINT32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->bytesPerFieldExcAlpha, sizeof(MTI_UINT32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->alphaBytesPerField, sizeof(MTI_UINT32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->framePitch, sizeof(MTI_UINT32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->verticalIntervalRows, sizeof(MTI_UINT32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->horizontalIntervalPixels, sizeof(MTI_UINT32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->alphaMatteType, sizeof(EAlphaMatteType));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->alphaMatteHiddenType, sizeof(EAlphaMatteType));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->gamma, sizeof(MTI_REAL32));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&cif->transferCharacteristic, sizeof(EColorSpace));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   // read the color space
   CImageColorSpace *ics = CImageColorSpace::deserialize(isStream);
   if (ics == NULL)
   {
      throw "Could not read color space";
   }

   cif->colorSpaceValues = *ics;
   delete ics;

   return cif;
}

//////////////////////////////////////////////////////////////////////
// Testing Methods
//////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// Function:    dump
//
// Description:
//
// Arguments
//
// Returns:     none
//
// ------------------------------------------------------------------------
void CImageFormat::dump(MTIostringstream &str) const
{
   str << "Image Format" << std::endl;

   str << "  Image Format Type: " << CImageInfo::queryImageFormatTypeName(getImageFormatType());
   str << " (" << getImageFormatType() << ")" << std::endl;

   str << "  pixelComponents: " << getPixelComponents() << "  colorSpace: " << getColorSpace() << std::endl << "  gamma: " << getGamma()
         << std::endl << "  transferCharacteristic: " << getTransferCharacteristic() << std::endl << "  bitsPerComponent: " <<
         (unsigned int)getBitsPerComponent() << "  pixelPacking: " << CImageInfo::queryPixelPackingName(getPixelPacking())
         << " (" << getPixelPacking() << ")" << std::endl;

   str << "  linesPerFrame: " << getLinesPerFrame() << "  pixelsPerLine: " << getPixelsPerLine() << std::endl;

   str << "  pixelAspectRatio: " << getPixelAspectRatio() << "  frameAspectRatio: " << getFrameAspectRatio() << std::endl;

   str << "  bytesPerField: " << getBytesPerField() << endl;
   str << "  verticalIntervalRows: " << getVerticalIntervalRows() << "  horizontalIntervalPixels: " << getHorizontalIntervalPixels()
         << std::endl;

   CMediaFormat::dump(str);
}

//////////////////////////////////////////////////////////////////////
// Image Color Space Class
//////////////////////////////////////////////////////////////////////

int CImageColorSpace::SetToDefaults(EPixelComponents pixelComponents, EColorSpace colorSpace, bool isVideoFormat, int bitsPerComponent)
{
   CImageInfo::queryNominalComponentValue(IF_COMPONENT_VALUE_TYPE_MIN, pixelComponents, colorSpace, isVideoFormat, bitsPerComponent,
         componentMin);
   CImageInfo::queryNominalComponentValue(IF_COMPONENT_VALUE_TYPE_MAX, pixelComponents, colorSpace, isVideoFormat, bitsPerComponent,
         componentMax);
   CImageInfo::queryNominalComponentValue(IF_COMPONENT_VALUE_TYPE_BLACK, pixelComponents, colorSpace, isVideoFormat, bitsPerComponent,
         componentBlack);
   CImageInfo::queryNominalComponentValue(IF_COMPONENT_VALUE_TYPE_WHITE, pixelComponents, colorSpace, isVideoFormat, bitsPerComponent,
         componentWhite);
   CImageInfo::queryNominalComponentValue(IF_COMPONENT_VALUE_TYPE_FILL, pixelComponents, colorSpace, isVideoFormat, bitsPerComponent,
         componentFill);

   return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Compare
///////////////////////////////////////////////////////////////////////////////

bool CImageColorSpace::compare(const CImageColorSpace &rhs, int componentCount) const
{
   for (int i = 0; i < componentCount; ++i)
   {
      if (componentMin[i] != rhs.componentMin[i])
      {
         return false;
      }
      if (componentMax[i] != rhs.componentMax[i])
      {
         return false;
      }
      if (componentWhite[i] != rhs.componentWhite[i])
      {
         return false;
      }
      if (componentBlack[i] != rhs.componentBlack[i])
      {
         return false;
      }
      if (componentFill[i] != rhs.componentFill[i])
      {
         return false;
      }
   }

   for (int i = 0; i < componentCount; ++i)
   {
      for (int j = 0; j < componentCount; ++i)
      {
         if (transformToRGB[i][j] != rhs.transformToRGB[i][j])
         {
            return false;
         }
         if (transformFromRGB[i][j] != rhs.transformFromRGB[i][j])
         {
            return false;
         }
      }
   }

   return true;
}

//////////////////////////////////////////////////////////////////////
// Serialize Methods
//////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// Function:    serialize
//
// Description:  Saves the class data
//
// Arguments  ostream - Stream to write to
//
// Returns:  true if success, false otherwise
//
// ------------------------------------------------------------------------
bool CImageColorSpace::serialize(ostream &osStream) const
{
   osStream.write((char *)&componentMin, sizeof(componentMin));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&componentMax, sizeof(componentMax));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&componentBlack, sizeof(componentBlack));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&componentWhite, sizeof(componentWhite));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&componentFill, sizeof(componentFill));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&transformToRGB, sizeof(transformToRGB));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   osStream.write((char *)&transformFromRGB, sizeof(transformFromRGB));
   if (osStream.rdstate() != ios::goodbit)
   {
      return false;
   }

   return true;
}

CImageColorSpace *CImageColorSpace::deserialize(istream &isStream)
{
   CImageColorSpace *ics = new CImageColorSpace();

   isStream.read((char *)&ics->componentMin, sizeof(ics->componentMin));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&ics->componentMax, sizeof(ics->componentMax));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&ics->componentBlack, sizeof(ics->componentBlack));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&ics->componentWhite, sizeof(ics->componentWhite));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&ics->componentFill, sizeof(ics->componentFill));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&ics->transformToRGB, sizeof(ics->transformToRGB));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   isStream.read((char *)&ics->transformFromRGB, sizeof(ics->transformFromRGB));
   if (isStream.rdstate() != ios::goodbit)
   {
      return NULL;
   }

   return ics;
}

///////////////////////////////////////////////////////////////////////////////
// Read & Write Ini Files
///////////////////////////////////////////////////////////////////////////////

int CImageColorSpace::readSection(CIniFile *iniFile, const string& sectionPrefix)
{
   string sectionName = sectionPrefix + "\\" + sectionNameSuffix;
   if (!iniFile->SectionExists(sectionName))
   {
      return 0;
   } // use same values as on entry to this function

   IntegerList valueList; // scratch pad for reading component values
   int index, count;

	// Component Min
   valueList = iniFile->ReadIntegerList(sectionName, componentMinKey);
	count = std::min<int>(MAX_COMPONENT_COUNT, (int)valueList.size());
   for (index = 0; index < count; ++index)
   {
      int value = valueList[index];
      if (value < 0 || value > 65535)
      {
         return FORMAT_ERROR_INVALID_COLOR_SPACE_COMPONENT;
      }
      componentMin[index] = (MTI_UINT16)value;
   }

   // Component Max
   valueList = iniFile->ReadIntegerList(sectionName, componentMaxKey);
	count = std::min<int>(MAX_COMPONENT_COUNT, (int)valueList.size());
	for (index = 0; index < count; ++index)
	{
		int value = valueList[index];
		if (value < 0 || value > 65535)
		{
			return FORMAT_ERROR_INVALID_COLOR_SPACE_COMPONENT;
		}
		componentMax[index] = (MTI_UINT16)value;
	}

	// Component Black
	valueList = iniFile->ReadIntegerList(sectionName, componentBlackKey);
	count = std::min<int>(MAX_COMPONENT_COUNT, (int)valueList.size());
   for (index = 0; index < count; ++index)
   {
      int value = valueList[index];
      if (value < 0 || value > 65535)
      {
         return FORMAT_ERROR_INVALID_COLOR_SPACE_COMPONENT;
      }
      componentBlack[index] = (MTI_UINT16)value;
   }

   // Component White
   valueList = iniFile->ReadIntegerList(sectionName, componentWhiteKey);
	count = std::min<int>(MAX_COMPONENT_COUNT, (int)valueList.size());
   for (index = 0; index < count; ++index)
   {
      int value = valueList[index];
      if (value < 0 || value > 65535)
      {
         return FORMAT_ERROR_INVALID_COLOR_SPACE_COMPONENT;
      }
      componentWhite[index] = (MTI_UINT16)value;
   }

   // Component Fill
   valueList = iniFile->ReadIntegerList(sectionName, componentFillKey);
   count = std::min<int>(MAX_COMPONENT_COUNT, (int)valueList.size());
   for (index = 0; index < count; ++index)
   {
      int value = valueList[index];
      if (value < 0 || value > 65535)
      {
         return FORMAT_ERROR_INVALID_COLOR_SPACE_COMPONENT;
      }
      componentFill[index] = (MTI_UINT16)value;
   }

   return 0;

} // readSection()

int CImageColorSpace::writeSection(CIniFile *iniFile, const string& sectionPrefix)
{
   string sectionName = sectionPrefix + "\\" + sectionNameSuffix;

   IntegerList valueList; // scratch pad for writing component values
   int index;

   // Component Min
   valueList.clear();
   for (index = 0; index < MAX_COMPONENT_COUNT; ++index)
   {
      valueList.push_back(componentMin[index]);
   }
   iniFile->WriteIntegerList(sectionName, componentMinKey, valueList);

   // Component Max
   valueList.clear();
   for (index = 0; index < MAX_COMPONENT_COUNT; ++index)
   {
      valueList.push_back(componentMax[index]);
   }
   iniFile->WriteIntegerList(sectionName, componentMaxKey, valueList);

   // Component Black
   valueList.clear();
   for (index = 0; index < MAX_COMPONENT_COUNT; ++index)
   {
      valueList.push_back(componentBlack[index]);
   }
   iniFile->WriteIntegerList(sectionName, componentBlackKey, valueList);

   // Component White
   valueList.clear();
   for (index = 0; index < MAX_COMPONENT_COUNT; ++index)
   {
      valueList.push_back(componentWhite[index]);
   }
   iniFile->WriteIntegerList(sectionName, componentWhiteKey, valueList);

   // Component Fill
   valueList.clear();
   for (index = 0; index < MAX_COMPONENT_COUNT; ++index)
   {
      valueList.push_back(componentFill[index]);
   }
   iniFile->WriteIntegerList(sectionName, componentFillKey, valueList);

   return 0;

} // writeSection()
