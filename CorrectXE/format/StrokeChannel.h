#ifndef StrokeChannelH
#define StrokeChannelH

#include "machine.h"
#include "time.h"
#include "formatDLL.h"             // Export/Import Defs
#include "MTImalloc.h"
#include <vector>
using std::vector;
#include <new>      // for std::bad_alloc

typedef MTI_UINT32 VEDGINDEX;
#define NULLVEDG (VEDGINDEX) 0xffffffff

#define STROKE_CHANNEL_CAPACITY_IN_PICELS 8192
#define STROKE_CHANNEL_BUFFER_OVERHEAD (3 * 64)


// Parent Class for pixel file
class MTI_FORMATDLL_API CStrokeChannel
{

public:

   const int BufferCapacityInPixels = 8192;
   const int BufferHeaderSizeInBytes = (3 * 64);

   CStrokeChannel();
   virtual ~CStrokeChannel();

   int openStrokeChannel(int pxlComponentCount,
                        string& pxlFileName);

   int setStrokeChannelFilePtr(MTI_INT64 chaddr);

   MTI_INT64 getStrokeChannelFilePtr();

   int openStrokeChannelForRestore(MTI_INT64 chaddr, int chbytes);

   MTI_UINT16* getNextRun();

   int putDifferenceRunsToChannel(MTI_UINT16 *iniFrame,
                                  MTI_UINT16 *finFrame,
                                  MTI_UINT16 *srcFrame,
                                  int frmWdth, int frmHght,
                                  bool evn, bool odd);

   int getDifferenceRunsFmChannel(int chaddr, int chbytes,
                                  MTI_UINT16 *trg,
                                  int frmWdth, int frmHght);
   int closeStrokeChannel();

   int closeAndTruncateStrokeChannel();

   /////////////////////////////////////////////////////////////////////////////

   MTI_INT32   pixelComponents;

   string      pixelFileName;

   MTI_INT32   pxlFileHandle;

   MTI_INT32   pxlBufByts;
   MTI_UINT16 *pxlBuf;
   MTI_UINT16 *pxlBufStop;
   MTI_UINT16 *pxlFillPtr;

   MTI_UINT16 *pxlRegionPtr;
   MTI_UINT16 *pxlRunPtr;
   MTI_INT64   pxlFilePtr;

   MTI_INT64   pxlBegFilePtr;
   MTI_INT64   pxlEndFilePtr;
   MTI_INT32   pxlBytes;

   MTI_INT64  rgnBegFilePtr;
   MTI_INT64  rgnEndFilePtr;

   MTI_INT64  runBegFilePtr;
   MTI_INT64  runEndFilePtr;

};


#endif // #ifndef StrokeChannelH




