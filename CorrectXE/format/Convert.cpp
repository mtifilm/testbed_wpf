//
// implementation file for class CConvert
//

#include "Convert.h"

#define INTERNAL_RGB_LUT_ALWAYS_LINEAR 1
#if !INTERNAL_RGB_LUT_ALWAYS_LINEAR
#include "ColorSpaceConvert.h"
#endif

#include "HalfDisplayLut.h"
#include "HRTimer.h"
#include "ImageFormat3.h"
#include "ImageInfo.h"
#include "IniFile.h"
#include "LUTManager.h"
#include "MTImalloc.h"
#include "SysInfo.h"

#ifdef _DEBUG
//#define NO_MULTI
#endif

#define KTRY try{
#define KCATCH }catch(...){TRACE_0(errout << "ERROR: CONVERT THREAD CRASHED"); MTIassert(false);}

//#define DPX12_NO_ROW_PADDING // define this if the ROWS of 12-bit DPX are NOT padded
#define DPX_16_BIT_RED_FIRST

///////////////////////////////////////////////////////////////////
// TRUNK uses a different #define for controlling ASM
// Let's use the same define in M5 to keep down the number of differences
// This code will make things work on both the trunk and M5
#if !defined(MTI_ASM_X86_INTEL)
  #if defined(_WINDOWS) && !defined(_WIN64)
    #define MTI_ASM_X86_INTEL 1
  #else
    #define MTI_ASM_X86_INTEL 0
  #endif
#endif
#if MTI_ASM_X86_INTEL && defined(_WIN64)
#error NO ASSEMBLY CODE ALLOWED IN 64 BITS
#endif
///////////////////////////////////////////////////////////////////

static void rowRGB(void *outrgb, void *row, void *v);
static void makeRowGrey(MTI_UINT8 *rgb, void *v);
static void makeRowPosNeg(MTI_UINT8 *rgb, void *v);
static void rowALPHA(MTI_UINT8 *outrgb, MTI_UINT8 *row, void *v);

static void null(void *v, int iJob);

static void stripeYUV_8(void *v, int iJob);
static void stripeYUV_10(void *v, int iJob);
static void stripeDVS_10(void *v, int iJob);
static void stripeDVS_10a(void *v, int iJob);
static void stripeDVS_10BE(void *v, int iJob);
static void stripeUYV_8(void *v, int iJob);
static void stripeUYV_10(void *v, int iJob);

static void stripeRGB_8(void *v, int iJob);
static void stripeRGB_8_LOG(void *v, int iJob);
static void stripeRGB_10(void *v, int iJob);
static void stripeRGB_10_LOG(void *v, int iJob);
static void stripeRGB_12(void *v, int iJob);
static void stripeRGB_12_LOG(void *v, int iJob);
static void stripeRGB_12P(void *v, int iJob);
static void stripeRGB_12P_LOG(void *v, int iJob);
static void stripeRGB_16_BE(void *v, int iJob);
static void stripeRGB_16_BE_LOG(void *v, int iJob);
static void stripeRGB_16_LE(void *v, int iJob);
static void stripeRGB_16_LE_LOG(void *v, int iJob);
static void stripeRGB_Half(void *v, int iJob);

static void stripeBGR_8(void *v, int iJob);
static void stripeBGR_10(void *v, int iJob);
static void stripeBGR_10_LOG(void *v, int iJob);
static void stripeBGR_16_LE(void *v, int iJob);
static void stripeBGR_16_LE_LOG(void *v, int iJob);

static void stripeRGBA_10(void *v, int iJob);
static void stripeRGBA_10_LOG(void *v, int iJob);
static void stripeRGBA_16_LE(void *v, int iJob);
static void stripeRGBA_16_LE_LOG(void *v, int iJob);
static void stripeRGBA_Half(void *v, int iJob);

static void stripeBGRA_10(void *v, int iJob);
static void stripeBGRA_10_LOG(void *v, int iJob);
static void stripeBGRA_16_LE(void *v, int iJob);
static void stripeBGRA_16_LE_LOG(void *v, int iJob);

static void stripeYYY_10(CConvert *vp, unsigned char *lut, int iJob, bool forDisplay);
static void stripeYYY_10_LINEAR(void *v, int iJob);
static void stripeYYY_10_LOG(void *v, int iJob);

static void stripeYYY_10_IN_16(CConvert *vp, unsigned char *lut, int iJob, bool forDisplay);
static void stripeYYY_10_IN_16_LINEAR(void *v, int iJob);
static void stripeYYY_10_IN_16_LOG(void *v, int iJob);

static void stripeYYY_16_LE(CConvert *vp, unsigned char *lut, int iJob, bool forDisplay);
static void stripeYYY_16_LE_LINEAR(void *v, int iJob);
static void stripeYYY_16_LE_LOG(void *v, int iJob);

static void stripeMON_10L(void *v, int iJob);
static void stripeMON_10L_LOG(void *v, int iJob);
static void stripeMON_10R(void *v, int iJob);
static void stripeMON_10R_LOG(void *v, int iJob);
static void stripeMON_10P(void *v, int iJob);
static void stripeMON_10P_LOG(void *v, int iJob);
static void stripeMON_16_LE(void *v, int iJob);
static void stripeMON_16_BE(void *v, int iJob);

// static variables to manage internal LUT tables
int CConvert::instanceCount = 0;
unsigned char * CConvert::RGBTbl    = NULL;
unsigned char * CConvert::Linear8   = NULL;
unsigned char * CConvert::Linear16  = NULL;
unsigned char * CConvert::AntiLog8  = NULL;
unsigned char * CConvert::AntiLog16 = NULL;
unsigned char * CConvert::AntiHalf  = NULL;
unsigned int  *CConvert::GreyTbl = NULL;
unsigned char *CConvert::PosTbl  = NULL;
unsigned char *CConvert::NegTbl  = NULL;

////////////////////////////////////////////////////////////////////

// Display LUT lookup function for HALFs.
//
inline static MTI_UINT8 halfLookup(MTI_UINT8 *lut, MTI_UINT16 src)
{
	// It's a 12-bit LUT! We interpolate between entries.
   ////////////////////////////////////////////////////////////////////////////
   // CAREFUL: there is a discontinuity between indices 2047 and 2048!
   ////////////////////////////////////////////////////////////////////////////
   MTI_UINT16 srcOver16 = src / 16;
   MTI_UINT16 lutValueLow = lut[srcOver16];
   if ((srcOver16 & 0x07ff) == 0x07FF)
   {
      // can't interpolate at 2047 (discontinuity) or at 4095 (end of table)
      return lutValueLow;
   }

   MTI_UINT16 srcPercent16 = src % 16;
   MTI_UINT16 lutValueHigh = lut[srcOver16 + 1];
   return lutValueLow + (((lutValueHigh - lutValueLow) * srcPercent16) / 16);
}

////////////////////////////////////////////////////////////////////

// Compute slice rows (startRow -> endRow, exclusive) from job number.
inline static void computeSliceRowsFromJob(
   int totalNumberOfRows,
   int thisJob,
   int totalNumberOfJobs,
   int &startRow,   // output
   int &endRow)     // EXCLUSIVE, output
{
   int nominalRowsPerStripe = totalNumberOfRows / totalNumberOfJobs;
   int extraRows = totalNumberOfRows % totalNumberOfJobs;
   startRow = (thisJob * nominalRowsPerStripe) + std::min<int>(thisJob, extraRows);
   endRow = startRow + nominalRowsPerStripe + ((thisJob < extraRows) ? 1 : 0);
}

////////////////////////////////////////////////////////////////////
//
//    C O N S T R U C T O R
//
CConvert::CConvert(int numStripes)
: nStripes(numStripes)
{
	nStripes = numStripes;

   if (nStripes == 0) {
      nStripes = SysInfo::AvailableProcessorCoreCount();
   }

   nStripes = std::min<int>(nStripes, MAX_CONVERSION_STRIPES);

#ifdef NO_MULTI
   nStripes = 1;
#endif

   tsThread.PrimaryFunction   = (void (*)(void *,int))&null;
   tsThread.SecondaryFunction = NULL;
   tsThread.CallBackFunction  = NULL;
   tsThread.vpApplicationData = this;
   tsThread.iNThread          = nStripes;
   tsThread.iNJob             = nStripes;

   /////////////////////////////////////////////////////////////////

   alphaColor = 0x00000000; // alpha disabled on construction

   /////////////////////////////////////////////////////////////////
   //
   // Build the internal YUV LUT
   //
   instanceCount++;
   if (instanceCount == 1) {

      RGBTbl = new unsigned char[128*128*256*3];
      MTI_UINT8 *dst = RGBTbl;
      for (int iU=-128;iU<128;iU+=2) {

         double RU = 0.0;
         double GU = (-.39176172) * iU;
         double BU =  2.0172305 * iU;

         for (int iV=-128;iV<128;iV+=2) {

            double RUV = RU +    1.5960273 * iV;
            double GUV = GU + (-.81296875) * iV;
            double BUV = BU;

            for (int iY=-16;iY<240;iY+=1) {

               double YIncr = 1.1643828 * iY;

               double RUVY = RUV + YIncr;
               if (RUVY < 0.  ) RUVY = 0.0;
               if (RUVY > 255.) RUVY = 255.0;

               double GUVY = GUV + YIncr;
               if (GUVY < 0.  ) GUVY = 0.0;
               if (GUVY > 255.) GUVY = 255.0;

               double BUVY = BUV + YIncr;
               if (BUVY < 0.  ) BUVY = 0.0;
               if (BUVY > 255.) BUVY = 255.0;
#if ENDIAN == MTI_BIG_ENDIAN
               *dst++ = (MTI_UINT8)RUVY;
               *dst++ = (MTI_UINT8)GUVY;
               *dst++ = (MTI_UINT8)BUVY;
#else
               *dst++ = (MTI_UINT8)BUVY;
               *dst++ = (MTI_UINT8)GUVY;
               *dst++ = (MTI_UINT8)RUVY;
#endif
            }
         }
      }

      /////////////////////////////////////////////////////////////////
      //
      // Build the internal linear LUT's
      //
      Linear8   = new unsigned char[3 *   256];
      Linear16  = new unsigned char[3 * 65536];

      MTI_UINT8 *lutR = Linear8;
      MTI_UINT8 *lutG = Linear8 + 256;
      MTI_UINT8 *lutB = Linear8 + 512;
      for (int i = 0; i < 256; ++i)
      {
         lutR[i] = lutG[i] = lutB[i] = i;
      }

      lutR = Linear16;
      lutG = Linear16 + 65536;
      lutB = Linear16 + (2 * 65536);
      for (int i = 0; i < 65536; ++i)
      {
         lutR[i] = lutG[i] = lutB[i] = i/256;
      }

      /////////////////////////////////////////////////////////////////
      //
      // Build the internal log LUT's
      //
      AntiLog8  = new unsigned char[3 *   256];
      AntiLog16 = new unsigned char[3 * 65536];
      AntiHalf  = new unsigned char[3 * 65536];

      CImageFormat LOGfmt;    // Log RGB image format (used for both 8 & 10 bits)
      CImageFormat LINfmt;    // Linear RGB image format (always 8 bits)
      LINfmt.setToDefaults(IF_TYPE_FILE_SGI, IF_PIXEL_PACKING_8Bits_IN_1Byte);
      LINfmt.setColorSpace(IF_COLOR_SPACE_LINEAR);

      // bit buckets to catch results
      MTI_UINT16 LinR, LinG, LinB;

#if INTERNAL_RGB_LUT_ALWAYS_LINEAR

      MTImemcpy(AntiLog8, Linear8, 3 * 256);
      MTImemcpy(AntiLog16, Linear16, 3 * 65536);

#else

      // allocate a converter
      CColorSpaceConvert clg;

      // build the  8-bit table
      LOGfmt.setToDefaults(IF_TYPE_FILE_DPX, IF_PIXEL_PACKING_8Bits_IN_1Byte);
      LOGfmt.setColorSpace(IF_COLOR_SPACE_LOG);
      clg.Init(&LOGfmt, &LINfmt);

      lutR = AntiLog8;
      lutG = AntiLog8 + 256;
      lutB = AntiLog8 + 512;
      for (int i = 0; i < 256; ++i)
      {
         clg.ConvertOneValue(i, i, i, &LinR, &LinG, &LinB);
         lutR[i] = LinR;
         lutG[i] = LinG;
         lutB[i] = LinB;
      }

      //////////////////////////////////////////////////////////////////
      //
      // Build the 16-bit table.
      LOGfmt.setToDefaults(IF_TYPE_FILE_TIFF, IF_PIXEL_PACKING_1_16Bits_IN_2Bytes);
      LOGfmt.setColorSpace(IF_COLOR_SPACE_LOG);
      clg.Init(&LOGfmt, &LINfmt);

      lutR = AntiLog16;
      lutG = AntiLog16 +  65536;
      lutB = AntiLog16 + 131072;
      for (MTI_UINT32 index = 0; index < 65536; ++index)
      {
         MTI_UINT16 in = (MTI_UINT16)index;
         clg.ConvertOneValue(in, in, in, &LinR, &LinG, &LinB);
         lutR[index] = (MTI_UINT8)LinR;
         lutG[index] = (MTI_UINT8)LinG;
         lutB[index] = (MTI_UINT8)LinB;
      }

#endif //INTERNAL_RGB_LUT_ALWAYS_LINEAR

		//////////////////////////////////////////////////////////////////
		// Copy the Half Display Lut to R, G and B tables.
		// The half (which has had its signs flipped) is used as an unsigned
		// short index into the tables.
		lutR = AntiHalf;
		lutG = AntiHalf +  65536;
		lutB = AntiHalf + 131072;

		MTImemcpy(lutR, HalfDisplayLut::GetDefaultLutPtr(), 65536);
		MTImemcpy(lutG, HalfDisplayLut::GetDefaultLutPtr(), 65536);
		MTImemcpy(lutB, HalfDisplayLut::GetDefaultLutPtr(), 65536);

		//////////////////////////////////////////////////////////////////
      //
      // build a "grey" table for converting single channels to grey
      GreyTbl = new unsigned int[256];
      for (int i = 0; i < 256; i++)
      {
         GreyTbl[i] = 0x7f000000 + (i << 16) + (i << 8) + i;
      }

      //////////////////////////////////////////////////////////////////
      //
      // build tables for difference onion-skinning
      PosTbl = new unsigned char[256];
      NegTbl = new unsigned char[256];
      for (int i = 0; i < 256; i++)
      {
         PosTbl[i] = 128 + (i >> 1);
         NegTbl[i] = 128 - (i >> 1);
      }

   }  //  <-- MOVED GreyTbl PosTbl NegTbl creation inside the "if"!!!  20081118

   //////////////////////////////////////////////////////////////////
   //
   xstrTbl = new int[MAXCOL];
   ystrTbl = new ROWENTRY[MAXROW];

   /////////////////////////////////////////////////////////////////
   //
   for (int i = 0; i < MAX_CONVERSION_STRIPES; i++)
   {
      RGBrow[i] = new unsigned int[MAX_FRAME_WIDTH];
   }

   for (int i = 0; i < MAX_CONVERSION_STRIPES; i++)
   {
      ALPHArow[i] = new unsigned int[MAX_FRAME_WIDTH];
   }

   /////////////////////////////////////////////////////////////////
   //
   // init state variables
   curFmt = NULL;
   curSrcWdth = 0;
   curSrcHght = 0;
   curSrcPitch = 0;
   curSrcColorSpace = 0;
   curSrcPelComponents = 0;
   curSrcPelPacking = 0;
   curInterlaced = false;
   curAlphaMatteType = IF_ALPHA_MATTE_INVALID;
   curDstWdth = 0;
   curDstHght = 0;
   curDstPitch = 0;
   curSrcBuf = NULL;
   curDstBuf = NULL;
   curConvertType = Unknown;
}

/////////////////////////////////////////////////////////////////////
//
//    D E S T R U C T O R
//
CConvert::
~CConvert()
{
   instanceCount--;
   if (instanceCount == 0) {

      delete [] RGBTbl;
      RGBTbl = NULL;
      delete [] Linear8;
      Linear8 = NULL;
      delete [] Linear16;
      Linear16 = NULL;
      delete [] AntiLog8;
      AntiLog8 = NULL;
      delete [] AntiLog16;
      AntiLog16 = NULL;
      delete [] AntiHalf;
      AntiHalf = NULL;
      delete [] GreyTbl;
      GreyTbl = NULL;
      delete [] PosTbl;
      PosTbl = NULL;
      delete [] NegTbl;
      NegTbl = NULL;

   }

   delete [] xstrTbl;

   delete [] ystrTbl;

   for (int i=0;i<16;i++)
      delete [] ALPHArow[i];

   for (int i=0;i<16;i++)
      delete [] RGBrow[i];
}

/////////////////////////////////////////////////////////////////////

void CConvert::
setAlphaColor(MTI_UINT32 alphacol)
{
   // accept the RGB but clear the A
   alphaColor = (alphacol&0xffffff);
}

unsigned int CConvert::
getAlphaColor()
{
   return alphaColor;
}

/////////////////////////////////////////////////////////////////////
//
//    c o n v e r t
//
void CConvert::
convertWtf(
        unsigned char **srcbuf,      // -> src image fields
        const CImageFormat *srcfmt,  // -> src image format
        RECT *srcrect,               // -> subrectangle to be displayed
        unsigned int *dstbuf,        // -> RGBA dst buffer
        int dstwdth,                 // wdth  of dst bitmap
        int dsthght,                 // hght  of dst bitmap
        int dstpitch,                // pitch of dst bitmap (bytes)
        unsigned char *lutptr        // -> external LUT
	)
{
   MTIassert(dstwdth <= MAXCOL);
   MTIassert(dsthght <= MAXROW);

	CHRTimer timer;

	// if a non-null LUT ptr is passed in, use it
   rgbtbl = RGBTbl;
   linear8 = Linear8;
   linear16 = Linear16;
   antilog8 = AntiLog8;
	antilog16 = AntiLog16;
	antihalf = AntiHalf;
   if (lutptr != NULL)
      rgbtbl = linear8 = antilog8 = antilog16 = antihalf = lutptr;

   // resume as before
   double step, run;
   int xoffs,
       intrun;

   dstpitch /= 4;

   // extract the following from the src image-format
   int srcwdth          = srcfmt->getPixelsPerLine();
   int srchght          = srcfmt->getLinesPerFrame();
   int srcpitch         = srcfmt->getFramePitch();
   int srccolorspace    = srcfmt->getColorSpace();
   int srcpelcomponents = srcfmt->getPixelComponents();
   int srcpelpacking    = srcfmt->getPixelPacking();
   bool srcil           = srcfmt->getInterlaced();
   EAlphaMatteType srcAlphaMatteType = srcfmt->getAlphaMatteType();

   // force DPX mono format to be non-interlaced
   if (/* srcpelcomponents == IF_PIXEL_COMPONENTS_LUMINANCE
       || */ srcpelcomponents == IF_PIXEL_COMPONENTS_Y
       || srcpelcomponents == IF_PIXEL_COMPONENTS_YYY) {       // xyzzy1

         srcil = false;
   }

   // continue as before
   curSrcBuf = srcbuf;
   curDstBuf = dstbuf;

   int winwdth = srcrect->right - srcrect->left + 1;
   int winhght = srcrect->bottom - srcrect->top + 1;

   if (
        (srcwdth != curSrcWdth)
      ||(srchght != curSrcHght)
      ||(srcpitch != curSrcPitch)
      ||(srccolorspace != curSrcColorSpace)
      ||(srcpelcomponents != curSrcPelComponents)
      ||(srcpelpacking    != curSrcPelPacking)
      ||(srcil   != curInterlaced)
      ||(srcAlphaMatteType != curAlphaMatteType)
      ||(dstwdth != curDstWdth)
      ||(dsthght != curDstHght)
      ||(dstpitch != curDstPitch)
      ||(srcrect->left   != curSrcRect.left)
      ||(srcrect->top    != curSrcRect.top)
      ||(srcrect->right  != curSrcRect.right)
      ||(srcrect->bottom != curSrcRect.bottom)
      ||(curConvertType  != PlainConvert)

       ) { // change control tables

      ///////////////////////////////////////////////////
      //
      // generate col table

      step = (double)winwdth/(double)dstwdth;
      run = 0.5*step;
      for (int i=0;i<dstwdth;i++) {
         xstrTbl[i] = (int)run;
         run += step;
      }

      ///////////////////////////////////////////////////
      //
      // generate row table and select stripe
      tsThread.PrimaryFunction = (void (*)(void *,int))&null;

      switch(srcpelcomponents) {

         case IF_PIXEL_COMPONENTS_YUV422:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  xoffs = 2 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeYUV_8;
                  break;

               case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
                  xoffs = 5 * (srcrect->left >> 1);
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeYUV_10;
                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
                  xoffs = (srcrect->left/6)<<4;
                  phase           = (srcrect->left>>1)%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeDVS_10;
                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:
                  xoffs = (srcrect->left/6)<<4;
                  phase           = (srcrect->left>>1)%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeDVS_10a;
                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L: // CLIPSTER
                  xoffs = (srcrect->left/6)<<4;
                  phase           = (srcrect->left>>1)%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeDVS_10BE;
                  break;

               default:
                  MTIassert(false);
                  TRACE_0(errout << "INTERNAL ERROR: In CONVERT: Unknown pixel packing " << srcpelpacking << " for type YUV422");
                  break;
            }

         break;

         case IF_PIXEL_COMPONENTS_YUV444:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  xoffs = 3 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeUYV_8;
                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
                  xoffs = 4 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeUYV_10;
                  break;

               default:
                  MTIassert(false);
                  TRACE_0(errout << "INTERNAL ERROR: In CONVERT: Unknown pixel packing " << srcpelpacking << " for type YUV444");
                  break;
            }

         break;

         case IF_PIXEL_COMPONENTS_RGB:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  xoffs = 3 * srcrect->left;
                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
                                             ? (void (*)(void *,int))&stripeRGB_8_LOG
                                             : (void (*)(void *,int))&stripeRGB_8;
                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  xoffs = 4 * srcrect->left;
                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
                                             ? (void (*)(void *,int))&stripeRGB_10_LOG
                                             : (void (*)(void *,int))&stripeRGB_10;
                  break;

               case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
                  //
                  xoffs = (srcrect->left>>3)*36;
                  phase = (srcrect->left>>1)%1;
                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
#ifndef DPX12_NO_ROW_PADDING
                                             ? (void (*)(void *,int))&stripeRGB_12_LOG
                                             : (void (*)(void *,int))&stripeRGB_12;
#else
															? (void (*)(void *,int))&stripeRGB_12P_LOG
                                             : (void (*)(void *,int))&stripeRGB_12P;
#endif
                  break;

					case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:    // close enough
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
                  xoffs = 6 * srcrect->left;
                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
															? (void (*)(void *,int))&stripeRGB_16_BE_LOG
															: (void (*)(void *,int))&stripeRGB_16_BE;
                  break;

               case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:    // close enough
               case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
                  xoffs = 6 * srcrect->left;
                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
                                             ? (void (*)(void *,int))&stripeRGB_16_LE_LOG
                                             : (void (*)(void *,int))&stripeRGB_16_LE;
                  break;

               case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
                  // This is for Open EXR - we pretend the half floats are log-encoded 16-bit uints
                  xoffs = 6 * srcrect->left;
                  tsThread.PrimaryFunction = (void (*)(void *,int))&stripeRGB_Half;
                  break;

               default:
                  MTIassert(false);
                  TRACE_0(errout << "INTERNAL ERROR: In CONVERT: Unknown pixel packing " << srcpelpacking << " for type RGB");
                  break;
            }

         break;

         case IF_PIXEL_COMPONENTS_BGR:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  xoffs = 3 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeBGR_8;
                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  //
                  xoffs = 4 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeBGR_10;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeBGR_10_LOG;
                  }
                  //
                  break;

					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
						xoffs = 6 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeBGR_16_LE;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeBGR_16_LE_LOG;
                  }
                  break;

               default:
                  MTIassert(false);
                  TRACE_0(errout << "INTERNAL ERROR: In CONVERT: Unknown pixel packing " << srcpelpacking << " for type BGR");
                  break;
            }

         break;

         case IF_PIXEL_COMPONENTS_RGBA:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  //
                  xoffs = (srcrect->left/3)<<4;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction = (void (*)(void *,int))&stripeRGBA_10;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeRGBA_10_LOG;
                  }
                  //
                  break;

					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
						//
                  xoffs = 8 * srcrect->left;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction = (void (*)(void *,int))&stripeRGBA_16_LE;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeRGBA_16_LE_LOG;
                  }
                  //
                  break;

               case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
                  //
                  xoffs = 8 * srcrect->left;
                  tsThread.PrimaryFunction = (void (*)(void *,int))&stripeRGBA_Half;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeRGBA_16_LE_LOG;
                  }
                  //
                  break;

               default:
                  MTIassert(false);
                  TRACE_0(errout << "INTERNAL ERROR: In CONVERT: Unknown pixel packing " << srcpelpacking << " for type RGBA");
                  break;
            }

         break;

         case IF_PIXEL_COMPONENTS_BGRA:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  //
                  xoffs = (srcrect->left/3)<<4;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction = (void (*)(void *,int))&stripeBGRA_10;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeBGRA_10_LOG;
                  }
                  //
                  break;

					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
						//
                  xoffs = 8 * srcrect->left;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction = (void (*)(void *,int))&stripeBGRA_16_LE;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeBGRA_16_LE_LOG;
                  }
                  //
                  break;

               default:
                  MTIassert(false);
                  TRACE_0(errout << "INTERNAL ERROR: In CONVERT: Unknown pixel packing " << srcpelpacking << " for type BGRA");
                  break;
            }

         break;

//         case IF_PIXEL_COMPONENTS_LUMINANCE:
         case IF_PIXEL_COMPONENTS_Y:
         case IF_PIXEL_COMPONENTS_YYY:     // xyzzy1

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  xoffs = (srcrect->left/3)<<2;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeMON_10L;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeMON_10L_LOG;
                  }

                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
                  xoffs = (srcrect->left/3)<<2;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeMON_10R;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeMON_10R_LOG;
                  }

                  break;

               case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
                  xoffs = (srcrect->left/4)*5;
                  phase = srcrect->left%4;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&stripeMON_10P;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&stripeMON_10P_LOG;
                  }

                  break;

					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
						xoffs = srcrect->left * 2;
                  tsThread.PrimaryFunction = (void (*)(void *,int))&stripeMON_16_LE;

                  break;

               default:
                  MTIassert(false);
                  TRACE_0(errout << "INTERNAL ERROR: In CONVERT: Unknown pixel packing " << srcpelpacking << " for type YYY");
                  break;

            }

         break;

      }

      step = (double)winhght/(double)dsthght;
      run = (double) srcrect->top + .5*step;

      if (srcil) { // interlaced

         for (int i=0;i<dsthght;i++) {
            intrun = (int)run;
            ystrTbl[i].fld  = intrun&1;
            ystrTbl[i].offs = srcpitch * (intrun >> 1) + xoffs;
            run += step;
         }
      }
      else { // non-interlaced

         // break out the (funny) 10-bit DPX RGBA case - this does
         // not have a proper row pitch; the rows are all packed and
         // the phase of the first pixel of each row can vary
         if ((srcpelcomponents == IF_PIXEL_COMPONENTS_RGBA)&&
             (srcpelpacking == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L)) {

            for (int i=0;i<dsthght;i++) {
               intrun = (int)run;
               ystrTbl[i].fld  = 0x00;
               int pel = ((srcwdth * intrun)+(srcrect->left));
               ystrTbl[i].offs  = (pel/3)*16;
               ystrTbl[i].phase = pel%3;
               run += step;
            }
         }

         // break out the (funny) 10-bit DPX mono case - this does
         // not have a proper row pitch; the rows are all packed and
         // the phase of the first pixel of each row can vary
         else if ((/* srcpelcomponents == IF_PIXEL_COMPONENTS_LUMINANCE
                  || */ srcpelcomponents == IF_PIXEL_COMPONENTS_Y)
            && (srcpelpacking == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L
                  || srcpelpacking == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R)) {

            for (int i=0;i<dsthght;i++) {
               intrun = (int)run;
               ystrTbl[i].fld  = 0x00;
               int pel = ((srcwdth * intrun)+(srcrect->left));
               ystrTbl[i].offs  = (pel/3)*4;
               ystrTbl[i].phase = pel%3;
               run += step;
            }
         }
         // break out the second (funny) 10-bit DPX mono case
         else if ((/* srcpelcomponents == IF_PIXEL_COMPONENTS_LUMINANCE
                  || */ srcpelcomponents == IF_PIXEL_COMPONENTS_Y) &&
             (srcpelpacking == IF_PIXEL_PACKING_4_10Bits_IN_5Bytes)) {

            for (int i=0;i<dsthght;i++) {
               intrun = (int)run;
               ystrTbl[i].fld  = 0x00;
               ystrTbl[i].offs = srcpitch * intrun + xoffs;
               ystrTbl[i].phase = phase;
               run += step;
            }
         }
         else {

            for (int i=0;i<dsthght;i++) {
               intrun = (int)run;
               ystrTbl[i].fld  = 0x00;
               ystrTbl[i].offs = srcpitch * intrun + xoffs;
               run += step;
            }
         }
      }

      //////////////////////////////////////////////////////////
      //
      // set current values
      //
      curSrcWdth          = srcwdth;
      curSrcHght          = srchght;
      curSrcPitch         = srcpitch;
      curSrcColorSpace    = srccolorspace;
      curSrcPelComponents = srcpelcomponents;
      curSrcPelPacking    = srcpelpacking;
      curInterlaced       = srcil;
      curAlphaMatteType   = srcAlphaMatteType;
		curWinWdth          = winwdth;
		curWinHght          = winhght;
		curDstWdth          = dstwdth;
      curDstHght          = dsthght;
      curDstPitch         = dstpitch;
      curSrcRect          = *srcrect;
      curConvertType      = PlainConvert;
   } // change control tables

   /////////////////////////////////////////////////////////////
   //
   // convert the frame by stripe
#ifdef NO_MULTI
   for (int i = 0; i < nStripes; i++)
   {
      tsThread.PrimaryFunction(this,i);
   }
#else
   // now allocate, run, and free the mthreads
   if (nStripes > 1)
   {
      int iRet = MThreadAlloc(&tsThread);
      if (iRet)
      {
            TRACE_0(errout << "Convert: MThreadAlloc failed, error=" << iRet);
      }
      else
      {
         iRet = MThreadStart(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadStart failed, error=" << iRet);
         }

         iRet = MThreadFree(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadFree failed, error=" << iRet);
         }
      }
   }
   else // nStripes <= 0
   {
      // If we're only doing one stripe, call the function directly.
      tsThread.PrimaryFunction(this,0);
   }
#endif

	TRACE_3(errout << "***************** INTERNAL CONVERT took " << timer.Read() << " msec");
}

///////////////////////////////////////////////////////////////

static void null(void *v, int iJob)
{
#ifdef __sgi
   printf("NULL STRIPE\n");
#endif
   return;
}

///////////////////////////////////////////////////////////////

static void stripeYUV_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
         unsigned int r0;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src  ));
#else
#if MTI_ASM_X86_INTEL
            _asm {
            mov     ebx,src
            mov     eax,[ebx]
            bswap   eax
            mov     r0,eax
            }
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+(src[3]);
#endif
#endif
            src += 4;

            base = ((r0>>10)&msku)+((r0>>1)&mskv);
            lutptr = lut + 3 * (base + ((r0>>16)&msky));
            dst[0] = lutptr[0];
            dst[1] = lutptr[1];
            dst[2] = lutptr[2];
            dst[3] = 0x7f;
            lutptr = lut + 3 * (base + ((r0    )&msky));
            dst[4] = lutptr[0];
            dst[5] = lutptr[1];
            dst[6] = lutptr[2];
            dst[7] = 0x7f;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeYUV_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src  ));
            r1 = src[4];
#else
#if MTI_ASM_X86_INTEL
			_asm {
            mov    ebx,src
            mov    eax,[ebx]
            bswap  eax
            mov    r0,eax
            movzx  eax,BYTE PTR[ebx+4]
            mov    r1,eax
            }
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+(src[3]);
            r1 = src[4];
#endif
#endif
            src += 5;

            base = ((r0>>10)&msku)+((r0<<3)&mskv);
            lutptr = lut + 3 * (base + ((r0>>14)&msky));
            dst[0] = lutptr[0];
            dst[1] = lutptr[1];
            dst[2] = lutptr[2];
            dst[3] = 0x7f;
            r0 = (r0<<8)+r1;
            lutptr = lut + 3 * (base + ((r0 >> 2)&msky));
            dst[4] = lutptr[0];
            dst[5] = lutptr[1];
            dst[6] = lutptr[2];
            dst[7] = 0x7f;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeDVS_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
         unsigned int r0, r1, r2, r3;

#if ENDIAN==MTI_BIG_ENDIAN
         r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+(src[0]);
         r1 = (src[7]<<24)+(src[6]<<16)+(src[5]<<8)+(src[4]);
         r2 = (src[11]<<24)+(src[10]<<16)+(src[9]<<8)+(src[8]);
         r3 = (src[15]<<24)+(src[14]<<16)+(src[13]<<8)+(src[12]);
#else
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
#endif
         src += 16;

         if (rowphase==0) goto p0;
         if (rowphase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               base = ((r0<<12)&msku)+((r0>>15)&mskv);
               lutptr = lut + 3 * (base + ((r0>>12)&msky));
               dst[0] = lutptr[0];
               dst[1] = lutptr[1];
               dst[2] = lutptr[2];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r1>>2)&msky));
               dst[4] = lutptr[0];
               dst[5] = lutptr[1];
               dst[6] = lutptr[2];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p1:; // phase 1

               base = ((r1<<2)&msku)+((r2<<5)&mskv);
               lutptr = lut + 3 * (base + ((r1>>22)&msky));
               dst[0] = lutptr[0];
               dst[1] = lutptr[1];
               dst[2] = lutptr[2];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r2>>12)&msky));
               dst[4] = lutptr[0];
               dst[5] = lutptr[1];
               dst[6] = lutptr[2];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               base = ((r2>>8)&msku)+((r3>>5)&mskv);
               lutptr = lut + 3 * (base + ((r3>>2)&msky));
               dst[0] = lutptr[0];
               dst[1] = lutptr[1];
               dst[2] = lutptr[2];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r3>>22)&msky));
               dst[4] = lutptr[0];
               dst[5] = lutptr[1];
               dst[6] = lutptr[2];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN==MTI_BIG_ENDIAN
               r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+(src[0]);
               r1 = (src[7]<<24)+(src[6]<<16)+(src[5]<<8)+(src[4]);
               r2 = (src[11]<<24)+(src[10]<<16)+(src[9]<<8)+(src[8]);
               r3 = (src[15]<<24)+(src[14]<<16)+(src[13]<<8)+(src[12]);
#else
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
#endif
               src += 16;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeDVS_10a(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
         unsigned int r0, r1, r2, r3;

#if ENDIAN==MTI_BIG_ENDIAN
         r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+(src[0]);
         r1 = (src[7]<<24)+(src[6]<<16)+(src[5]<<8)+(src[4]);
         r2 = (src[11]<<24)+(src[10]<<16)+(src[9]<<8)+(src[8]);
         r3 = (src[15]<<24)+(src[14]<<16)+(src[13]<<8)+(src[12]);
#else
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
#endif
         src += 16;

         if (rowphase==0) goto p0;
         if (rowphase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               base = ((r0<<14)&msku)+((r0>>9)&mskv);
               lutptr = lut + 3 * (base + ((r0>>8)&msky));
               dst[0] = lutptr[0];
               dst[1] = lutptr[1];
               dst[2] = lutptr[2];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + (r1&msky));
               dst[4] = lutptr[0];
               dst[5] = lutptr[1];
               dst[6] = lutptr[2];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p1:; // phase 1

               base = ((r1<<6)&msku)+((r2<<7)&mskv);
               lutptr = lut + 3 * (base + ((r1>>16)&msky));
               dst[0] = lutptr[0];
               dst[1] = lutptr[1];
               dst[2] = lutptr[2];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r2>>8)&msky));
               dst[4] = lutptr[0];
               dst[5] = lutptr[1];
               dst[6] = lutptr[2];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               base = ((r2>>2)&msku)+((r3>>1)&mskv);
               lutptr = lut + 3 * (base + (r3&msky));
               dst[0] = lutptr[0];
               dst[1] = lutptr[1];
               dst[2] = lutptr[2];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r3>>16)&msky));
               dst[4] = lutptr[0];
               dst[5] = lutptr[1];
               dst[6] = lutptr[2];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN==MTI_BIG_ENDIAN
               r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+(src[0]);
               r1 = (src[7]<<24)+(src[6]<<16)+(src[5]<<8)+(src[4]);
               r2 = (src[11]<<24)+(src[10]<<16)+(src[9]<<8)+(src[8]);
               r3 = (src[15]<<24)+(src[14]<<16)+(src[13]<<8)+(src[12]);
#else
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
#endif
               src += 16;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeDVS_10BE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
         unsigned int r0, r1, r2, r3;

#if ENDIAN==MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
#else
#if MTI_ASM_X86_INTEL
         _asm { // swap endian
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         }
#else
         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+(src[3]);
         r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+(src[7]);
         r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+(src[11]);
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+(src[15]);
#endif
#endif
         src += 16;

         if (rowphase==0) goto p0;
         if (rowphase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               base = ((r0>>10)&msku)+((r0<<3)&mskv);
               lutptr = lut + 3 * (base + ((r0>>14)&msky));
               dst[0] = lutptr[0];
               dst[1] = lutptr[1];
               dst[2] = lutptr[2];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r1>>24)&msky));
               dst[4] = lutptr[0];
               dst[5] = lutptr[1];
               dst[6] = lutptr[2];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p1:; // phase 1

               base = ((r1    )&msku)+((r2>>17)&mskv);
               lutptr = lut + 3 * (base + ((r1>> 4)&msky));
               dst[0] = lutptr[0];
               dst[1] = lutptr[1];
               dst[2] = lutptr[2];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r2>>14)&msky));
               dst[4] = lutptr[0];
               dst[5] = lutptr[1];
               dst[6] = lutptr[2];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               base = ((r2<<10)&msku)+((r3>>7)&mskv);
               lutptr = lut + 3 * (base + ((r3>>24)&msky));
               dst[0] = lutptr[0];
               dst[1] = lutptr[1];
               dst[2] = lutptr[2];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r3>>4)&msky));
               dst[4] = lutptr[0];
               dst[5] = lutptr[1];
               dst[6] = lutptr[2];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN==MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
#else
#if MTI_ASM_X86_INTEL
			   _asm { // swap endian
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               }
#else
               r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+(src[3]);
               r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+(src[7]);
               r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+(src[11]);
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+(src[15]);
#endif
#endif
               src += 16;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeUYV_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = (src[2]<<16)+(src[1]<<8)+src[0];
            r1 = (src[5]<<16)+(src[4]<<8)+src[3];
#else
            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+3));
#endif
            src += 6;

            lutptr = lut + 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            dst[0] = lutptr[0];
            dst[1] = lutptr[1];
            dst[2] = lutptr[2];
            dst[3] = 0x7f;

            lutptr = lut + 3 * (((r1<<14)&msku)+((r1>>9)&mskv)+((r1>>8)&msky));
            dst[4] = lutptr[0];
            dst[5] = lutptr[1];
            dst[6] = lutptr[2];
            dst[7] = 0x7f;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeUYV_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
            r1 = (src[7]<<24)+(src[6]<<16)+(src[5]<<8)+src[4];
#else
            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+4));
#endif
            src += 8;

            lutptr = lut + 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            dst[0] = lutptr[0];
            dst[1] = lutptr[1];
            dst[2] = lutptr[2];
            dst[3] = 0x7f;

            lutptr = lut + 3 * (((r1<<12)&msku)+((r1>>15)&mskv)+((r1>>12)&msky));
            dst[4] = lutptr[0];
            dst[5] = lutptr[1];
            dst[6] = lutptr[2];
            dst[7] = 0x7f;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeRGB_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8+256;
   unsigned char *linearB = vp->linear8+512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         while (dst < rowend) {

            dst[0] = linearB[src[2]]; // B
            dst[1] = linearG[src[1]]; // G
            dst[2] = linearR[src[0]]; // R
            dst[3] = 0x7f;            // A

            dst[4] = linearB[src[5]]; // B
            dst[5] = linearG[src[4]]; // G
            dst[6] = linearR[src[3]]; // R
            dst[7] = 0x7f;            // A

            src += 6;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeRGB_8_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8+256;
   unsigned char *antilogB = vp->antilog8+512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         while (dst < rowend) {

            dst[0] = antilogB[src[2]]; // B
            dst[1] = antilogG[src[1]]; // G
            dst[2] = antilogR[src[0]]; // R
            dst[3] = 0x7f;             // A

            dst[4] = antilogB[src[5]]; // B
            dst[5] = antilogG[src[4]]; // G
            dst[6] = antilogR[src[3]]; // R
            dst[7] = 0x7f;             // A

            src += 6;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeBGR_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8+256;
   unsigned char *linearB = vp->linear8+512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         while (dst < rowend) {

            dst[0] = linearB[src[0]]; // B
            dst[1] = linearG[src[1]]; // G
            dst[2] = linearR[src[2]]; // R
            dst[3] = 0x7f;            // A

            dst[4] = linearB[src[3]]; // B
            dst[5] = linearG[src[4]]; // G
            dst[6] = linearR[src[5]]; // R
            dst[7] = 0x7f;            // A

            src += 6;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeRGB_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+4));
#else
#if MTI_ASM_X86_INTEL
			_asm {
            mov    ebx,src
            mov    eax,[ebx]
            bswap  eax
            mov    r0,eax
            mov    eax,[ebx+4]
            bswap  eax
            mov    r1,eax
            }
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
#endif
#endif
            src += 8;

            dst[0] = linearB[(r0>> 4)&0xff]; // B
            dst[1] = linearG[(r0>>14)&0xff]; // G
            dst[2] = linearR[(r0>>24)&0xff]; // R
            dst[3] = 0x7f;                   // A

            dst[4] = linearB[(r1>> 4)&0xff]; // B
            dst[5] = linearG[(r1>>14)&0xff]; // G
            dst[6] = linearR[(r1>>24)&0xff]; // R
            dst[7] = 0x7f;                   // A
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeRGB_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+4));
#else
#if MTI_ASM_X86_INTEL
            _asm {
            mov    ebx,src
            mov    eax,[ebx]
            bswap  eax
            mov    r0,eax
            mov    eax,[ebx+4]
            bswap  eax
            mov    r1,eax
            }
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
#endif
#endif
            src += 8;

            dst[0] = antilogB[(r0>> 4)&0xff]; // B
            dst[1] = antilogG[(r0>>14)&0xff]; // G
            dst[2] = antilogR[(r0>>24)&0xff]; // R
            dst[3] = 0x7f;                   // A

            dst[4] = antilogB[(r1>> 4)&0xff]; // B
            dst[5] = antilogG[(r1>>14)&0xff]; // G
            dst[6] = antilogR[(r1>>24)&0xff]; // R
            dst[7] = 0x7f;                   // A
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeBGR_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+4));
#else
#if MTI_ASM_X86_INTEL
			_asm {
            mov    ebx,src
            mov    eax,[ebx]
            bswap  eax
            mov    r0,eax
            mov    eax,[ebx+4]
            bswap  eax
            mov    r1,eax
            }
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
#endif
#endif
            src += 8;

            dst[2] = linearB[(r0>> 4)&0xff]; // B
            dst[1] = linearG[(r0>>14)&0xff]; // G
            dst[0] = linearR[(r0>>24)&0xff]; // R
            dst[3] = 0x7f;                   // A

            dst[6] = linearB[(r1>> 4)&0xff]; // B
            dst[5] = linearG[(r1>>14)&0xff]; // G
            dst[4] = linearR[(r1>>24)&0xff]; // R
            dst[7] = 0x7f;                   // A
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeBGR_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+4));
#else
#if MTI_ASM_X86_INTEL
            _asm {
            mov    ebx,src
            mov    eax,[ebx]
            bswap  eax
            mov    r0,eax
            mov    eax,[ebx+4]
            bswap  eax
            mov    r1,eax
            }
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
#endif
#endif
            src += 8;

            dst[2] = antilogB[(r0>> 4)&0xff]; // B
            dst[1] = antilogG[(r0>>14)&0xff]; // G
            dst[0] = antilogR[(r0>>24)&0xff]; // R
            dst[3] = 0x7f;                   // A

            dst[6] = antilogB[(r1>> 4)&0xff]; // B
            dst[5] = antilogG[(r1>>14)&0xff]; // G
            dst[4] = antilogR[(r1>>24)&0xff]; // R
            dst[7] = 0x7f;                   // A
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

#ifndef DPX12_NO_ROW_PADDING
static void stripeRGB_12(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (rowphase==0) goto p0;
         if (rowphase==2) goto p2;
         if (rowphase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[2] = linearR[(r0>> 4)&0xff]; // R
               dst[1] = linearG[(r0>>16)&0xff]; // G
               dst[0] = linearB[((r0>>28)+(r1<<4))&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p1:;

               dst[6] = linearR[(r1>> 8)&0xff]; // R
               dst[5] = linearG[(r1>>20)&0xff]; // G
               dst[4] = linearB[(r2    )&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[2] = linearR[(r2>>12)&0xff]; // R
               dst[1] = linearG[(r2>>24)&0xff]; // G
               dst[0] = linearB[(r3>> 4)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p3:;

               dst[6] = linearR[(r3>>16)&0xff]; // R
               dst[5] = linearG[((r3>>28)+(r4<<4))&0xff]; // G
               dst[4] = linearB[(r4>> 8)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[2] = linearR[(r4>>20)&0xff]; // R
               dst[1] = linearG[(r5    )&0xff]; // G
               dst[0] = linearB[(r5>>12)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p5:;

               dst[6] = linearR[(r5>>24)&0xff]; // R
               dst[5] = linearG[(r6>> 4)&0xff]; // G
               dst[4] = linearB[(r6>>16)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[2] = linearR[((r6>>28)+(r7<<4))&0xff]; // R
               dst[1] = linearG[(r7>> 8)&0xff]; // G
               dst[0] = linearB[(r7>>20)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p7:;

               dst[6] = linearR[(r8    )&0xff]; // R
               dst[5] = linearG[(r8>>12)&0xff]; // G
               dst[4] = linearB[(r8>>24)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeRGB_12_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8+256;
   unsigned char *antilogB = vp->antilog8+512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (rowphase==0) goto p0;
         if (rowphase==2) goto p2;
         if (rowphase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[2] = antilogR[(r0>> 4)&0xff]; // R
               dst[1] = antilogG[(r0>>16)&0xff]; // G
               dst[0] = antilogB[((r0>>28)+(r1<<4))&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p1:;

               dst[6] = antilogR[(r1>> 8)&0xff]; // R
               dst[5] = antilogG[(r1>>20)&0xff]; // G
               dst[4] = antilogB[(r2    )&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[2] = antilogR[(r2>>12)&0xff]; // R
               dst[1] = antilogG[(r2>>24)&0xff]; // G
               dst[0] = antilogB[(r3>> 4)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p3:;

               dst[6] = antilogR[(r3>>16)&0xff]; // R
               dst[5] = antilogG[((r3>>28)+(r4<<4))&0xff]; // G
               dst[4] = antilogB[(r4>> 8)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[2] = antilogR[(r4>>20)&0xff]; // R
               dst[1] = antilogG[(r5    )&0xff]; // G
               dst[0] = antilogB[(r5>>12)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p5:;

               dst[6] = antilogR[(r5>>24)&0xff]; // R
               dst[5] = antilogG[(r6>> 4)&0xff]; // G
               dst[4] = antilogB[(r6>>16)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[2] = antilogR[((r6>>28)+(r7<<4))&0xff]; // R
               dst[1] = antilogG[(r7>> 8)&0xff]; // G
               dst[0] = antilogB[(r7>>20)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p7:;

               dst[6] = antilogR[(r8    )&0xff]; // R
               dst[5] = antilogG[(r8>>12)&0xff]; // G
               dst[4] = antilogB[(r8>>24)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void stripeRGB_12P(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==2) goto p2;
         if (vp->ystrTbl[i].phase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[2] = linearR[(r0>> 4)&0xff]; // R
               dst[1] = linearG[(r0>>16)&0xff]; // G
               dst[0] = linearB[((r0>>28)+(r1<<4))&0xff]; // B
               dst[3] = 0x7f;                   // A

            p1:;

               dst[6] = linearR[(r1>> 8)&0xff]; // R
               dst[5] = linearG[(r1>>20)&0xff]; // G
               dst[4] = linearB[(r2    )&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[2] = linearR[(r2>>12)&0xff]; // R
               dst[1] = linearG[(r2>>24)&0xff]; // G
               dst[0] = linearB[(r3>> 4)&0xff]; // B
               dst[3] = 0x7f;                   // A

            p3:;

               dst[6] = linearR[(r3>>16)&0xff]; // R
               dst[5] = linearG[((r3>>28)+(r4<<4))&0xff]; // G
               dst[4] = linearB[(r4>> 8)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[2] = linearR[(r4>>20)&0xff]; // R
               dst[1] = linearG[(r5    )&0xff]; // G
               dst[0] = linearB[(r5>>12)&0xff]; // B
               dst[3] = 0x7f;                   // A

            p5:;

               dst[6] = linearR[(r5>>24)&0xff]; // R
               dst[5] = linearG[(r6>> 4)&0xff]; // G
               dst[4] = linearB[(r6>>16)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[2] = linearR[((r6>>28)+(r7<<4))&0xff]; // R
               dst[1] = linearG[(r7>> 8)&0xff]; // G
               dst[0] = linearB[(r7>>20)&0xff]; // B
               dst[3] = 0x7f;                   // A

            p7:;

               dst[6] = linearR[(r8    )&0xff]; // R
               dst[5] = linearG[(r8>>12)&0xff]; // G
               dst[4] = linearB[(r8>>24)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeRGB_12P_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==2) goto p2;
         if (vp->ystrTbl[i].phase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[2] = antilogR[(r0>> 4)&0xff]; // R
               dst[1] = antilogG[(r0>>16)&0xff]; // G
               dst[0] = antilogB[((r0>>28)+(r1<<4))&0xff]; // B
               dst[3] = 0x7f;                   // A

            p1:;

               dst[6] = antilogR[(r1>> 8)&0xff]; // R
               dst[5] = antilogG[(r1>>20)&0xff]; // G
               dst[4] = antilogB[(r2    )&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[2] = antilogR[(r2>>12)&0xff]; // R
               dst[1] = antilogG[(r2>>24)&0xff]; // G
               dst[0] = antilogB[(r3>> 4)&0xff]; // B
               dst[3] = 0x7f;                   // A

            p3:;

               dst[6] = antilogR[(r3>>16)&0xff]; // R
               dst[5] = antilogG[((r3>>28)+(r4<<4))&0xff]; // G
               dst[4] = antilogB[(r4>> 8)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[2] = antilogR[(r4>>20)&0xff]; // R
               dst[1] = antilogG[(r5    )&0xff]; // G
               dst[0] = antilogB[(r5>>12)&0xff]; // B
               dst[3] = 0x7f;                   // A

            p5:;

               dst[6] = antilogR[(r5>>24)&0xff]; // R
               dst[5] = antilogG[(r6>> 4)&0xff]; // G
               dst[4] = antilogB[(r6>>16)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[2] = antilogR[((r6>>28)+(r7<<4))&0xff]; // R
               dst[1] = antilogG[(r7>> 8)&0xff]; // G
               dst[0] = antilogB[(r7>>20)&0xff]; // B
               dst[3] = 0x7f;                   // A

            p7:;

               dst[6] = antilogR[(r8    )&0xff]; // R
               dst[5] = antilogG[(r8>>12)&0xff]; // G
               dst[4] = antilogB[(r8>>24)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#endif // DPX12_NO_ROW_PADDING

///////////////////////////////////////////////////////////////

static void stripeRGB_16_BE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     =  (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst     = row;

         while (dst < rowend) {

#ifdef DPX_16_BIT_RED_FIRST

            dst[0] = linearB[src[4]];  // B
            dst[1] = linearG[src[2]];  // G
            dst[2] = linearR[src[0]];  // R
            dst[3] = 0x7f;             // A

            dst[4] = linearB[src[10]]; // B
            dst[5] = linearG[src[8]];  // G
            dst[6] = linearR[src[6]];  // R
            dst[7] = 0x7f;             // A

#else // BLUE FIRST

            dst[0] = linearB[src[0]];  // B
            dst[1] = linearG[src[2]];  // G
            dst[2] = linearR[src[4]];  // R
            dst[3] = 0x7f;             // A

            dst[4] = linearB[src[6]];  // B
            dst[5] = linearG[src[8]];  // G
            dst[6] = linearR[src[10]]; // R
            dst[7] = 0x7f;             // A

#endif

            src += 12;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeRGB_16_BE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *presrc = NULL;
   for (int i = begRow; i < endRow; ++i)
   {
      src = curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs;

      // convert one full row of source
      if (src != presrc)
      {
         presrc = src;
         dst     = row;
         while (dst < rowend)
         {

#ifdef DPX_16_BIT_RED_FIRST

            dst[0] = antilogB[src[4]];  // B
            dst[1] = antilogG[src[2]];  // G
            dst[2] = antilogR[src[0]];  // R
            dst[3] = 0x7f;              // A

            dst[4] = antilogB[src[10]]; // B
            dst[5] = antilogG[src[8]];  // G
            dst[6] = antilogR[src[6]];  // R
            dst[7] = 0x7f;              // A

#else // BLUE FIRST

            dst[0] = antilogB[src[0]];  // B
            dst[1] = antilogG[src[2]];  // G
            dst[2] = antilogR[src[4]];  // R
            dst[3] = 0x7f;              // A

            dst[4] = antilogB[src[6]];  // B
            dst[5] = antilogG[src[8]];  // G
            dst[6] = antilogR[src[10]]; // R
            dst[7] = 0x7f;              // A

#endif

            src += 12;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeRGB_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *prevsrc = NULL;
   for (int i=begRow;i<endRow;i++)
   {
      src = (unsigned char *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (src != prevsrc)
      {
         prevsrc = src;
         dst    = row;

         while (dst < rowend)
         {
            dst[0] = linearB[src[5]];  // B
            dst[1] = linearG[src[3]];  // G
            dst[2] = linearR[src[1]];  // R
            dst[3] = 0x7f;             // A

            dst[4] = linearB[src[11]]; // B
            dst[5] = linearG[src[9]];  // G
            dst[6] = linearR[src[7]];  // R
            dst[7] = 0x7f;             // A

            src += 12;
            dst += 8;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeRGB_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     =  (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *prevsrc = NULL;
   for (int i = begRow; i < endRow; ++i)
   {
      src = (unsigned char *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (src != prevsrc)
      {
         prevsrc = src;
         dst     = row;

         while (dst < rowend)
         {
            dst[0] = antilogB[src[5]];  // B
            dst[1] = antilogG[src[3]];  // G
            dst[2] = antilogR[src[1]];  // R
            dst[3] = 0x7f;              // A

            dst[4] = antilogB[src[11]]; // B
            dst[5] = antilogG[src[9]];  // G
            dst[6] = antilogR[src[7]];  // R
            dst[7] = 0x7f;              // A

            src += 12;
            dst += 8;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeRGBA_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *prevsrc = NULL;
   for (int i=begRow;i<endRow;i++)
   {
      src = (unsigned char *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (src != prevsrc)
      {
         prevsrc = src;
         dst    = row;

         while (dst < rowend)
         {
            dst[0] = linearB[src[5]];  // B
            dst[1] = linearG[src[3]];  // G
            dst[2] = linearR[src[1]];  // R
            dst[3] = 0x7f;             // A

            dst[4] = linearB[src[13]]; // B
            dst[5] = linearG[src[11]]; // G
            dst[6] = linearR[src[9]];  // R
            dst[7] = 0x7f;             // A

            src += 16;
            dst += 8;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeRGBA_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     =  (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *prevsrc = NULL;
   for (int i = begRow; i < endRow; ++i)
   {
      src = (unsigned char *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (src != prevsrc)
      {
         prevsrc = src;
         dst     = row;

         while (dst < rowend)
         {
            dst[0] = antilogB[src[5]];  // B
            dst[1] = antilogG[src[3]];  // G
            dst[2] = antilogR[src[1]];  // R
            dst[3] = 0x7f;              // A

            dst[4] = antilogB[src[13]]; // B
            dst[5] = antilogG[src[11]];  // G
            dst[6] = antilogR[src[9]];  // R
            dst[7] = 0x7f;              // A

            src += 16;
            dst += 8;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeBGR_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *prevsrc = NULL;
   for (int i=begRow;i<endRow;i++)
   {
      src = (unsigned char *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (src != prevsrc)
      {
         prevsrc = src;
         dst    = row;

         while (dst < rowend)
         {
            dst[2] = linearB[src[5]];  // B
            dst[1] = linearG[src[3]];  // G
            dst[0] = linearR[src[1]];  // R
            dst[3] = 0x7f;             // A

            dst[6] = linearB[src[11]]; // B
            dst[5] = linearG[src[9]];  // G
            dst[4] = linearR[src[7]];  // R
            dst[7] = 0x7f;             // A

            src += 12;
            dst += 8;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeBGR_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     =  (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *prevsrc = NULL;
   for (int i = begRow; i < endRow; ++i)
   {
      src = (unsigned char *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (src != prevsrc)
      {
         prevsrc = src;
         dst     = row;

         while (dst < rowend)
         {
            dst[2] = antilogB[src[5]];  // B
            dst[1] = antilogG[src[3]];  // G
            dst[0] = antilogR[src[1]];  // R
            dst[3] = 0x7f;              // A

            dst[6] = antilogB[src[11]]; // B
            dst[5] = antilogG[src[9]];  // G
            dst[4] = antilogR[src[7]];  // R
            dst[7] = 0x7f;              // A

            src += 12;
            dst += 8;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeBGRA_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *prevsrc = NULL;
   for (int i=begRow;i<endRow;i++)
   {
      src = (unsigned char *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (src != prevsrc)
      {
         prevsrc = src;
         dst    = row;

         while (dst < rowend)
         {
            dst[2] = linearR[src[5]];  // R
            dst[1] = linearG[src[3]];  // G
            dst[0] = linearB[src[1]];  // B
            dst[3] = 0x7f;             // A

            dst[6] = linearR[src[13]]; // R
            dst[5] = linearG[src[11]]; // G
            dst[4] = linearB[src[9]];  // B
            dst[7] = 0x7f;             // A

            src += 16;
            dst += 8;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeBGRA_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     =  (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *prevsrc = NULL;
   for (int i = begRow; i < endRow; ++i)
   {
      src = (unsigned char *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (src != prevsrc)
      {
         prevsrc = src;
         dst     = row;

         while (dst < rowend)
         {
            dst[2] = antilogB[src[5]];  // B
            dst[1] = antilogG[src[3]];  // G
            dst[0] = antilogR[src[1]];  // R
            dst[3] = 0x7f;              // A

            dst[6] = antilogR[src[13]]; // R
            dst[5] = antilogG[src[11]]; // G
            dst[4] = antilogB[src[9]];  // B
            dst[7] = 0x7f;              // A

            src += 16;
            dst += 8;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeRGB_Half(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert*) v;

   unsigned char *halfLutR   = vp->antihalf;
   unsigned char *halfLutG   = vp->antihalf + 65536;
	unsigned char *halfLutB   = vp->antihalf + 131072;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl              = vp->xstrTbl;
   int *xstrTblEnd           = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl         = vp->ystrTbl;
   unsigned char *dst;
   unsigned short *srcw;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb   = (vp->curDstBuf + begRow * vp->curDstPitch);
   unsigned int rgbadv = (vp->curDstPitch - vp->curDstWdth);

	unsigned short *presrcw = NULL;

   for (int i = begRow; i < endRow; i++)
   {
      srcw = (unsigned short *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (srcw != presrcw)
      {
         // convert one full row of source
         presrcw = srcw;
         dst = row;

         while (dst < rowend)
         {
				dst[0] = halfLutR[srcw[0]];  // R
				dst[1] = halfLutG[srcw[1]];  // G
				dst[2] = halfLutB[srcw[2]];  // B
				dst[3] = 0x7f;               // A

            srcw += 3;
            dst += 4;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; xstrTblPtr++)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeRGBA_Half(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert*) v;

	unsigned char *halfLutR = vp->antihalf;
	unsigned char *halfLutG = vp->antihalf + 65536;
	unsigned char *halfLutB = vp->antihalf + 131072;
	unsigned char **curSrcBuf = vp->curSrcBuf;
	int *xstrTbl = vp->xstrTbl;
	int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
	ROWENTRY *ystrTbl = vp->ystrTbl;
	MTI_UINT8 *dst;
	MTI_UINT16 *srcw;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

	unsigned char *row = (unsigned char *)vp->RGBrow[iJob];
	unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

	unsigned int *rgb = (vp->curDstBuf + begRow * vp->curDstPitch);
	unsigned int rgbadv = (vp->curDstPitch - vp->curDstWdth);

	MTI_UINT16 *presrcw = NULL;
	for (int i = begRow; i < endRow; i++)
	{
		srcw = (MTI_UINT16*)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

		if (srcw != presrcw)
		{
			// convert one full row of source
			presrcw = srcw;
			dst = row;

			while (dst < rowend)
			{
            // ARGB -> BGRA
				dst[0] = halfLutB[srcw[1]];  // B
				dst[1] = halfLutG[srcw[2]];  // G
				dst[2] = halfLutR[srcw[3]];  // R
				dst[3] = 0x7f; // A

				srcw += 4;
				dst += 4;
			}
		}

		// now output one row of the finished stripe
		unsigned int *irow = vp->RGBrow[iJob];
		for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; xstrTblPtr++)
		{
			*rgb++ = irow[*xstrTblPtr];
		}

		rgb += rgbadv;

	} // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeRGBA_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   //int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
#else
         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
         r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
         r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
         src += 16;

         //if (rowphase==0) goto p0;
         //if (rowphase==1) goto p1;
         //goto p2;
         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               dst[0] = linearB[(r0>> 4)&0xff]; // B
               dst[1] = linearG[(r0>>14)&0xff]; // G
               dst[2] = linearR[(r0>>24)&0xff]; // R
               dst[3] = 0x7f;                   // A

               dst += 4;

               if (dst == rowend) break;

            p1:; // phase 1

               dst[0] = linearB[(r2>>24)&0xff]; // B
               dst[1] = linearG[(r1>> 4)&0xff]; // G
               dst[2] = linearR[(r1>>14)&0xff]; // R
               dst[3] = 0x7f;                   // A

               dst += 4;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[0] = linearB[(r3>>14)&0xff]; // B
               dst[1] = linearG[(r3>>24)&0xff]; // G
               dst[2] = linearR[(r2>> 4)&0xff]; // R
               dst[3] = 0x7f;                   // A

               dst += 4;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
#else
               r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
               r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
               r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
               src += 16;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeRGBA_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   //int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
#else
         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
         r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
         r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
         src += 16;

         //if (rowphase==0) goto p0;
         //if (rowphase==1) goto p1;
         //goto p2;
         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               dst[0] = antilogB[(r0>> 4)&0xff]; // B
               dst[1] = antilogG[(r0>>14)&0xff]; // G
               dst[2] = antilogR[(r0>>24)&0xff]; // R
               dst[3] = 0x7f;                    // A

               dst += 4;

               if (dst == rowend) break;

            p1:; // phase 1

               dst[0] = antilogB[(r2>>24)&0xff]; // B
               dst[1] = antilogG[(r1>> 4)&0xff]; // G
               dst[2] = antilogR[(r1>>14)&0xff]; // R
               dst[3] = 0x7f;                    // A

               dst += 4;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[0] = antilogB[(r3>>14)&0xff]; // B
               dst[1] = antilogG[(r3>>24)&0xff]; // G
               dst[2] = antilogR[(r2>> 4)&0xff]; // R
               dst[3] = 0x7f;                    // A

               dst += 4;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
#else
               r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
               r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
               r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
               src += 16;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeBGRA_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   //int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3;

         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
         r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
         r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         src += 16;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               dst[2] = linearR[(r0>> 4)&0xff]; // R
               dst[1] = linearG[(r0>>14)&0xff]; // G
               dst[0] = linearB[(r0>>24)&0xff]; // B
               dst[3] = 0x7f;                   // A

               dst += 4;

               if (dst == rowend) break;

            p1:; // phase 1

               dst[2] = linearR[(r2>>24)&0xff]; // R
               dst[1] = linearG[(r1>> 4)&0xff]; // G
               dst[0] = linearB[(r1>>14)&0xff]; // B
               dst[3] = 0x7f;                   // A

               dst += 4;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[2] = linearR[(r3>>14)&0xff]; // R
               dst[1] = linearG[(r3>>24)&0xff]; // G
               dst[0] = linearB[(r2>> 4)&0xff]; // B
               dst[3] = 0x7f;                   // A

               dst += 4;

               if (dst == rowend) break;

               // next pixel-sextuple
               r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
               r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
               r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               src += 16;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeBGRA_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   //int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3;

         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
         r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
         r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         src += 16;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               dst[2] = antilogR[(r0>> 4)&0xff]; // R
               dst[1] = antilogG[(r0>>14)&0xff]; // G
               dst[0] = antilogB[(r0>>24)&0xff]; // B
               dst[3] = 0x7f;                    // A

               dst += 4;

               if (dst == rowend) break;

            p1:; // phase 1

               dst[2] = antilogR[(r2>>24)&0xff]; // R
               dst[1] = antilogG[(r1>> 4)&0xff]; // G
               dst[0] = antilogB[(r1>>14)&0xff]; // B
               dst[3] = 0x7f;                    // A

               dst += 4;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[2] = antilogR[(r3>>14)&0xff]; // R
               dst[1] = antilogG[(r3>>24)&0xff]; // G
               dst[0] = antilogB[(r2>> 4)&0xff]; // B
               dst[3] = 0x7f;                    // A

               dst += 4;

               if (dst == rowend) break;

               // next pixel-sextuple
               r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
               r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
               r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               src += 16;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeYYY_10(CConvert *vp, unsigned char *lut, int iJob, bool forDisplay)
{
KTRY
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;

   for (int i = begRow; i < endRow; i++)
   {
      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);
      if (src != presrc)
      {
         // convert one row
         presrc = src;
         dst = row;

         // loopCount counts the number of source bytes or number of output
         // components which are both 4 in this case.
         // CAREFUL - the buffer width is ROUNDED DOWN to a multiple of two!
         // What the FUCK!
         int loopCount = (rowend - row) & ~1;
         for (int j = 0; j < loopCount; j += 4)
         {
            // We only copy byte 0 of the source which, because the source is
            // big-endian, holds the upper 8 bits of one of the components.
            dst[j] = dst[j + 1] = dst[j + 2] = lut[src[j]]; // YYY
            dst[j + 3] = 0x7f;                              // A
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; xstrTblPtr++)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      if (forDisplay)
      {
// It's already gray!
//         // if grey enabled, massage the row
//         if ((vp->rgbMask & 0xff000000) == 0xff000000)
//         {
//            makeRowGrey((MTI_UINT8*)rgb, (void*)vp);
//         }

         if (vp->pnMask & 3)
         {
            makeRowPosNeg((MTI_UINT8*)rgb, (void*)vp);
         }
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeYYY_10_LINEAR(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->linear8;
   stripeYYY_10(vp, lut, iJob, false);
KCATCH
}

static void stripeYYY_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->antilog8;
   stripeYYY_10(vp, lut, iJob, false);
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeYYY_10_IN_16(CConvert *vp, unsigned char *lut, int iJob, bool forDisplay)
{
KTRY
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned short *srcw;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned short *prevsrcw = NULL;
   for (int i=begRow;i<endRow;i++)
   {
      srcw = (unsigned short *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (srcw != prevsrcw)
      {
         prevsrcw = srcw;
         dst    = row;

         while (dst < rowend)
         {
            dst[0] = dst[1] = dst[2] = lut[(srcw[0] >> 2) & 0x00FF]; // YYY
            dst[3] = 0x7f;                                           // A
            srcw += 3;
            dst += 4;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      if (forDisplay)
      {
// It's already gray!
//         // if grey enabled, massage the row
//         if ((vp->rgbMask & 0xff000000) == 0xff000000)
//         {
//            makeRowGrey((MTI_UINT8*)rgb, (void*)vp);
//         }

         if (vp->pnMask & 3)
         {
            makeRowPosNeg((MTI_UINT8*)rgb, (void*)vp);
         }
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeYYY_10_IN_16_LINEAR(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->linear8;
   stripeYYY_10_IN_16(vp, lut, iJob, false);
KCATCH
}

static void stripeYYY_10_IN_16_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->antilog8;
   stripeYYY_10_IN_16(vp, lut, iJob, false);
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeYYY_16_LE(CConvert *vp, unsigned char *lut, int iJob, bool forDisplay)
{
KTRY
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *prevsrc = NULL;
   for (int i=begRow;i<endRow;i++)
   {
      src = (unsigned char *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      // convert one full row of source
      if (src != prevsrc)
      {
         prevsrc = src;
         dst    = row;

         while (dst < rowend)
         {
            dst[0] = dst[1] = dst[2] = lut[src[1]];  // YYY
            dst[3] = 0x7f;                              // A

            src += 6;
            dst += 4;
        }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      if (forDisplay)
      {
// It's already gray!
//         // if grey enabled, massage the row
//         if ((vp->rgbMask & 0xff000000) == 0xff000000)
//         {
//            makeRowGrey((MTI_UINT8*)rgb, (void*)vp);
//         }

         if (vp->pnMask & 3)
         {
            makeRowPosNeg((MTI_UINT8*)rgb, (void*)vp);
         }
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeYYY_16_LE_LINEAR(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->linear8;
   stripeYYY_16_LE(vp, lut, iJob, false);
KCATCH
}

static void stripeYYY_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->antilog8;
   stripeYYY_16_LE(vp, lut, iJob, false);
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeMON_10X(void *v, int iJob, bool alignmentIsL, bool isLog)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = isLog ? vp->antilog8 : vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;
         if (alignmentIsL)
         {
            r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
         }
         else
         {
            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         }
         src += 4;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true)
         {

         p0: ; // phase 0

            r1 = lut[(r0 >> 2) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p1: ; // phase 1

            r1 = lut[(r0 >> 12) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p2: ; // phase 2

            r1 = lut[(r0 >> 22) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

            // next pixel-triple
            if (alignmentIsL)
            {
               r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
            }
            else
            {
               r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
            }
            src += 4;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeMON_10L(void *v, int iJob)
{
   stripeMON_10X(v, iJob, true, false);
}

static void stripeMON_10L_LOG(void *v, int iJob)
{
   stripeMON_10X(v, iJob, true, true);
}

static void stripeMON_10R(void *v, int iJob)
{
   stripeMON_10X(v, iJob, false, false);
}

static void stripeMON_10R_LOG(void *v, int iJob)
{
   stripeMON_10X(v, iJob, true, true);
}

///////////////////////////////////////////////////////////////
#if 0
static void stripeMON_10L(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;

         r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
         src += 4;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true)
         {

         p0: ; // phase 0

            r1 = linear[(r0 >> 2) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p1: ; // phase 1

            r1 = linear[(r0 >> 12) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p2: ; // phase 2

            r1 = linear[(r0 >> 22) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

            // next pixel-triple

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src));
#else
#if MTI_ASM_X86_INTEL
            _asm
            {
               mov ebx, src mov eax, [ebx]bswap eax mov r0, eax
            }
#else
            r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
#endif
#endif
            src += 4;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
static void stripeMON_10L_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilog = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;

         r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
         src += 4;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true)
         {

         p0: ; // phase 0

            r1 = antilog[(r0 >> 2) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p1: ; // phase 1

            r1 = antilog[(r0 >> 12) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p2: ; // phase 2

            r1 = antilog[(r0 >> 22) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

            // next pixel-triple

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src));
#else
#if MTI_ASM_X86_INTEL
            _asm
            {
               mov ebx, src mov eax, [ebx]bswap eax mov r0, eax
            }
#else
            r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
#endif
#endif
            src += 4;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void stripeMON_10R(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;

#if ENDIAN==MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src  ));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         }
#else
         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
#endif
#endif
         src += 4;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true)
         {

         p0: ; // phase 0

            r1 = linear[r0 & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p1: ; // phase 1

            r1 = linear[(r0 >> 10) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p2: ; // phase 2

            r1 = linear[(r0 >> 20) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
          dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

            // next pixel-triple

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               }
#else
            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
#endif
#endif
            src += 4;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeMON_10R_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilog = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;

#if ENDIAN==MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src  ));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         }
#else
         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
#endif
#endif
         src += 4;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true)
         {

         p0: ; // phase 0

            r1 = antilog[r0 & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p1: ; // phase 1

            r1 = antilog[(r0 >> 10) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p2: ; // phase 2

            r1 = antilog[(r0 >> 20) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

            // next pixel-triple

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               }
#else
            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
#endif
#endif
            src += 4;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#endif
///////////////////////////////////////////////////////////////

static void stripeMON_10P(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2;

#if ENDIAN==MTI_BIG_ENDIAN
         r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
         r1 = src[4];
#else
         r0 = *((unsigned int *)src);
         r1 = src[4];
#endif
         src += 5;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         if (vp->ystrTbl[i].phase==2) goto p2;
         goto p3;

         while (true) {

            p0:; // phase 0

               r2 = linear[(r0>>2)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p1:; // phase 1

               r2 = linear[(r0>>12)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p2:; // phase 2

               r2 = linear[(r0>>22)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p3:; // phase 3

               r2 = linear[(r1)];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

               // next pixel-triple
#if ENDIAN==MTI_BIG_ENDIAN
              r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
              r1 = src[4];
#else
              r0 = *((unsigned int *)src);
              r1 = src[4];
#endif
              src += 5;

         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeMON_10P_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilog = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2;

#if ENDIAN==MTI_BIG_ENDIAN
         r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
         r1 = src[4];
#else
         r0 = *((unsigned int *)src);
         r1 = src[4];
#endif
         src += 5;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         if (vp->ystrTbl[i].phase==2) goto p2;
         goto p3;

         while (true) {

            p0:; // phase 0

               r2 = antilog[(r0>>2)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p1:; // phase 1

               r2 = antilog[(r0>>12)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p2:; // phase 2

               r2 = antilog[(r0>>22)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p3:; // phase 3

               r2 = antilog[(r1)];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

               // next pixel-triple
#if ENDIAN==MTI_BIG_ENDIAN
              r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
              r1 = src[4];
#else
              r0 = *((unsigned int *)src);
              r1 = src[4];
#endif
              src += 5;

         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeMON_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *presrc = NULL;
   for (int i = begRow; i < endRow; ++i)
   {
      src = curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs;

      // convert one full row of source
      if (src != presrc)
      {
         presrc = src;
         dst    = row;

         while (dst < rowend)
         {
            dst[0] = dst[1] = dst[2] = linear[src[1]]; // Y -> BGR
            dst[3] = 0x7f;                             // A

            dst[4] = dst[5] = dst[6] = linear[src[3]]; // Y -> BGR
            dst[7] = 0x7f;                             // A

            src += 4;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void stripeMON_16_BE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src;
   unsigned char *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);

   unsigned char *presrc = NULL;
   for (int i = begRow; i < endRow; ++i)
   {
      src = curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs;

      // convert one full row of source
      if (src != presrc)
      {
         presrc = src;
         dst    = row;

         while (dst < rowend)
         {
            dst[0] = dst[1] = dst[2] = linear[src[0]]; // Y -> BGR
            dst[3] = 0x7f;                             // A

            dst[4] = dst[5] = dst[6] = linear[src[2]]; // Y -> BGR
            dst[7] = 0x7f;                             // A

            src += 4;
            dst += 8;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
      {
         *rgb++ = irow[*xstrTblPtr];
      }

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeYUV_8(void *v, int iJob);
static void dstripeYUV_10(void *v, int iJob);
static void dstripeDVS_10(void *v, int iJob);
static void dstripeDVS_10a(void *v, int iJob);
static void dstripeDVS_10BE(void *v, int iJob);
static void dstripeUYV_8(void *v, int iJob);
static void dstripeUYV_10(void *v, int iJob);

static void dstripeRGB_8(void *v, int iJob);
static void dstripeRGB_8_LOG(void *v, int iJob);
static void dstripeRGB_10(void *v, int iJob);
static void dstripeRGB_10_LOG(void *v, int iJob);
static void dstripeBGR_10(void *v, int iJob);
static void dstripeBGR_10_LOG(void *v, int iJob);
static void dstripeRGB_10_A1(void *v, int iJob);
static void dstripeRGB_10_LOG_A1(void *v, int iJob);
static void dstripeRGB_10_A2(void *v, int iJob);
static void dstripeRGB_10_LOG_A2(void *v, int iJob);
static void dstripeRGB_10_A8(void *v, int iJob);
static void dstripeRGB_10_LOG_A8(void *v, int iJob);
static void dstripeRGB_10_A16(void *v, int iJob);
static void dstripeRGB_10_LOG_A16(void *v, int iJob);
static void dstripeRGB_12(void *v, int iJob);
static void dstripeRGB_12_LOG(void *v, int iJob);
static void dstripeRGB_12_LR(void *v, int iJob);
static void dstripeRGB_12_LR_LOG(void *v, int iJob);
static void dstripeRGB_12P(void *v, int iJob);
static void dstripeRGB_12P_LOG(void *v, int iJob);
static void dstripeRGB_16_BE(void *v, int iJob);
static void dstripeRGB_16_BE_LOG(void *v, int iJob);
static void dstripeRGB_16_LE(void *v, int iJob);
static void dstripeRGB_16_LE_LOG(void *v, int iJob);
static void dstripeRGB_Half(void *v, int iJob);

static void dstripeBGR_8(void *v, int iJob);
static void dstripeBGR_10(void *v, int iJob);
static void dstripeBGR_10_LOG(void *v, int iJob);
static void dstripeBGR_16_LE(void *v, int iJob);
static void dstripeBGR_16_LE_LOG(void *v, int iJob);

static void dstripeRGBA_10(void *v, int iJob);
static void dstripeRGBA_10_LOG(void *v, int iJob);
static void dstripeRGBA_16_LE(void *v, int iJob);
static void dstripeRGBA_16_LE_LOG(void *v, int iJob);
static void dstripeRGBA_Half(void *v, int iJob);

static void dstripeBGRA_10(void *v, int iJob);
static void dstripeBGRA_10_LOG(void *v, int iJob);
static void dstripeBGRA_16_LE(void *v, int iJob);
static void dstripeBGRA_16_LE_LOG(void *v, int iJob);


static void dstripeYYY_10_LINEAR(void *v, int iJob);
static void dstripeYYY_10_LOG(void *v, int iJob);

static void dstripeYYY_10_IN_16_LINEAR(void *v, int iJob);
static void dstripeYYY_10_IN_16_LOG(void *v, int iJob);

static void dstripeYYY_16_LE_LINEAR(void *v, int iJob);
static void dstripeYYY_16_LE_LOG(void *v, int iJob);

static void dstripeMON_10L(void *v, int iJob);
static void dstripeMON_10L_LOG(void *v, int iJob);
static void dstripeMON_10R(void *v, int iJob);
static void dstripeMON_10R_LOG(void *v, int iJob);
static void dstripeMON_10P(void *v, int iJob);
static void dstripeMON_10P_LOG(void *v, int iJob);
static void dstripeMON_16_LE(void *v, int iJob);
static void dstripeMON_16_BE(void *v, int iJob);

/////////////////////////////////////////////////////////////////////
//
//    d i s p l a y C o n v e r t   (does RGBA for the OpenGL on PC)
//
void CConvert::displayConvert(
        unsigned char **srcbuf,      // -> src image fields
        const CImageFormat *srcfmt,  // -> src image format
        RECT *srcrect,               // -> subrectangle to be displayed
        unsigned int *dstbuf,        // -> RGBA dst buffer
        int dstwdth,                 // wdth  of dst bitmap
        int dsthght,                 // hght  of dst bitmap
        int dstpitch,                // pitch of dst bitmap (bytes)
		unsigned char *lutptr,       // -> external LUT
        unsigned int rgbmask,        // RGB channels mask
		unsigned int pnmask,         // pos-neg mask
		bool isWiperComparand,
		int *comparandDividerPositionPerRow
	)
{
   MTIassert(srcbuf[0] != nullptr);
   if (srcbuf[0] == nullptr)
   {
      TRACE_0(errout << "INTERNAL ERROR: displayConvert was called with a null source buffer!");
      return;
   }

   MTIassert(srcfmt != nullptr);
   if (srcfmt == nullptr)
   {
      TRACE_0(errout << "INTERNAL ERROR: displayConvert was called with a null image format!");
      return;
   }

   MTIassert(dstbuf != nullptr);
   if (dstbuf == nullptr)
   {
      TRACE_0(errout << "INTERNAL ERROR: displayConvert was called with a null dest buffer!");
      return;
   }

	CHRTimer hrTimer;

   // save the RGB channels mask
   rgbMask = rgbmask;

   // save the POS-NEG mask
   pnMask = pnmask;

   // if a non-null LUT ptr is passed in, use it
   rgbtbl = RGBTbl;
   linear8 = Linear8;
   linear16 = Linear16;
   antilog8 = AntiLog8;
   antilog16 = AntiLog16;
   antihalf = AntiHalf;
   if (lutptr != NULL)
	  rgbtbl = linear8 = linear16 = antilog8 = antilog16 = antihalf = lutptr;

	_isWiperComparand = isWiperComparand;
	_comparandDividerPositionPerRow = comparandDividerPositionPerRow;

   // resume as before
	double step, run;
   int xoffs,
       intrun;

   dstpitch /= 4;

   // extract the following from the src image-format
   int srcwdth          = srcfmt->getPixelsPerLine();
   int srchght          = srcfmt->getLinesPerFrame();
   int srcpitch         = srcfmt->getFramePitch();
   int srccolorspace    = srcfmt->getColorSpace();
   int srcpelcomponents = srcfmt->getPixelComponents();
   int srcpelpacking    = srcfmt->getPixelPacking();
   bool srcil           = srcfmt->getInterlaced();
   EAlphaMatteType srcAlphaMatteType = srcfmt->getAlphaMatteType();

   // force DPX mono format to be non-interlaced
   if (/* srcpelcomponents == IF_PIXEL_COMPONENTS_LUMINANCE
   || */ srcpelcomponents == IF_PIXEL_COMPONENTS_Y
   || srcpelcomponents == IF_PIXEL_COMPONENTS_YYY       // xyzzy1
      )
   {

         srcil = false;

   }

	// continue as before
   curSrcBuf = srcbuf;
   curDstBuf = dstbuf;

   int winwdth = srcrect->right - srcrect->left + 1;
	int winhght = srcrect->bottom - srcrect->top + 1;

   if (
        (srcwdth != curSrcWdth)
      ||(srchght != curSrcHght)
      ||(srcpitch != curSrcPitch)
      ||(srccolorspace != curSrcColorSpace)
      ||(srcpelcomponents != curSrcPelComponents)
      ||(srcpelpacking    != curSrcPelPacking)
      ||(srcAlphaMatteType != curAlphaMatteType)
      ||(srcil   != curInterlaced)
      ||(dstwdth != curDstWdth)
      ||(dsthght != curDstHght)
      ||(dstpitch != curDstPitch)
      ||(srcrect->left   != curSrcRect.left)
      ||(srcrect->top    != curSrcRect.top)
      ||(srcrect->right  != curSrcRect.right)
      ||(srcrect->bottom != curSrcRect.bottom)
      ||(curConvertType  != NormalDisplayConvert)

       )
   { // change control tables

      ///////////////////////////////////////////////////
      //
      // generate col table

      step = (double)winwdth/(double)dstwdth;
      run = 0.5*step;
      for (int i=0;i<dstwdth;i++) {
         xstrTbl[i] = (int)run;
         run += step;
      }

      ///////////////////////////////////////////////////
      //
      // generate row table and select stripe
      tsThread.PrimaryFunction = (void (*)(void *,int))&null;

      switch(srcpelcomponents) {

         case IF_PIXEL_COMPONENTS_YUV422:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  xoffs = 2 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeYUV_8;
                  break;

               case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
                  xoffs = 5 * (srcrect->left >> 1);
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeYUV_10;
                  break;

               // standard DVS format: 3x10-bits little endian, right aligned
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
                  xoffs = (srcrect->left/6)<<4;
                  phase           = (srcrect->left>>1)%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeDVS_10;
                  break;

               // old DVS format: 3x8-bits (MSB) + 3x2-bits (LSB)
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVSa:
                  xoffs = (srcrect->left/6)<<4;
                  phase           = (srcrect->left>>1)%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeDVS_10a;
                  break;

               // clipster DVS format: 3x10-bits big endian, left aligned
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  xoffs = (srcrect->left/6)<<4;
                  phase           = (srcrect->left>>1)%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeDVS_10BE;
                  break;

               default:
                  break;
            }

         break;

         case IF_PIXEL_COMPONENTS_YUV444:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  xoffs = 3 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeUYV_8;
                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
                  xoffs = 4 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeUYV_10;
                  break;

               default:
                  break;
            }

        break;

        case IF_PIXEL_COMPONENTS_RGB:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  //
                  xoffs = 3 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_8;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeRGB_8_LOG;
                  }
                  //
                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  //
                  xoffs = 4 * srcrect->left;

						if (srcAlphaMatteType == IF_ALPHA_MATTE_1_BIT)
                  {
                     //  1-bit alpha channel is present
							tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_10_A1;
                     if (srccolorspace == IF_COLOR_SPACE_LOG) {
                        tsThread.PrimaryFunction =
                            (void (*)(void *,int))&dstripeRGB_10_LOG_A1;
                     }
                  }
                  else if (srcAlphaMatteType == IF_ALPHA_MATTE_2_BIT_EMBEDDED)
                  {
                     //  2-bit alpha channel is present
							tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_10_A2;
                     if (srccolorspace == IF_COLOR_SPACE_LOG) {
                        tsThread.PrimaryFunction =
                            (void (*)(void *,int))&dstripeRGB_10_LOG_A2;
                     }
                  }
                  else if (srcAlphaMatteType == IF_ALPHA_MATTE_8_BIT)
                  {
                     //  8-bit alpha channel is present
                     tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_10_A8;
                     if (srccolorspace == IF_COLOR_SPACE_LOG) {
                        tsThread.PrimaryFunction =
                            (void (*)(void *,int))&dstripeRGB_10_LOG_A8;
                     }
                  }
                  else if (srcAlphaMatteType == IF_ALPHA_MATTE_16_BIT)
                  {
                     // 16-bit alpha channel is present
                     tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_10_A16;
                     if (srccolorspace == IF_COLOR_SPACE_LOG) {
                        tsThread.PrimaryFunction =
                            (void (*)(void *,int))&dstripeRGB_10_LOG_A16;
                     }
                  }
                  else
                  {
                     // alpha channel is absent
                     tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_10;
                     if (srccolorspace == IF_COLOR_SPACE_LOG) {
                        tsThread.PrimaryFunction =
                            (void (*)(void *,int))&dstripeRGB_10_LOG;
                     }
                  }

                  break;

               case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
                  // these may be changed to the PACKED versions
                  srcpitch = ((srcwdth*36+31)/32)*4;
                  xoffs = (srcrect->left>>3)*36;
                  phase = (srcrect->left)%8;
#ifndef DPX12_NO_ROW_PADDING
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_12;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeRGB_12_LOG;
                  }
#else
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_12P;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeRGB_12P_LOG;
                  }
#endif
                  break;

               case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
                  // these may be changed to the PACKED versions
                  srcpitch = ((srcwdth*36+31)/32)*4;
                  xoffs = (srcrect->left>>3)*36;
                  phase = (srcrect->left)%8;
#ifndef DPX12_NO_ROW_PADDING
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_12_LR;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeRGB_12_LOG;
                  }
#else
                  //tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_12P;
                  //if (srccolorspace == IF_COLOR_SPACE_LOG) {
                  //   tsThread.PrimaryFunction =
                  //       (void (*)(void *,int))&dstripeRGB_12P_LOG;
                  //}
#endif
                  break;

					// 12-bit unpacked is same as 16-bit.
					case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
////				case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:        // UGH, was byte-swapped when read
						xoffs = 6 * srcrect->left;
						tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_16_BE;
					   if (srccolorspace == IF_COLOR_SPACE_LOG) {
							tsThread.PrimaryFunction =
					          (void (*)(void *,int))&dstripeRGB_16_BE_LOG;
					   }
					   break;

					// 12-bit unpacked is same as 16-bit.
					case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:    // close enough
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:      // UGH, was byte-swapped when read
						xoffs = 6 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGB_16_LE;
                  if (srccolorspace == IF_COLOR_SPACE_LOG || srccolorspace == IF_COLOR_SPACE_EXR) {
                     tsThread.PrimaryFunction =
								 (void (*)(void *,int))&dstripeRGB_16_LE_LOG;
                  }
                  break;

               case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
                  xoffs = 6 * srcrect->left;
                  tsThread.PrimaryFunction = (void (*)(void *,int))&dstripeRGB_Half;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
								 (void (*)(void *,int))&dstripeRGB_16_LE_LOG;
                  }
                  break;

               default:
                  break;

            }

         break;


         case IF_PIXEL_COMPONENTS_BGR:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  xoffs = 3 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeBGR_8;
                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  xoffs = 4 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeBGR_10;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeBGR_10_LOG;
                  }
                  break;

					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
						xoffs = 6 * srcrect->left;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeBGR_16_LE;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeBGR_16_LE_LOG;
                  }
						break;

               default:
                  break;

            }

         break;

         case IF_PIXEL_COMPONENTS_RGBA:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  //
                  xoffs = (srcrect->left/3)<<4;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeRGBA_10;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeRGBA_10_LOG;
                  }
                  //
                  break;

					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
						xoffs = 8 * srcrect->left;
                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG || srccolorspace == IF_COLOR_SPACE_EXR)
															? (void (*)(void *,int))&dstripeRGBA_16_LE_LOG
															: (void (*)(void *,int))&dstripeRGBA_16_LE;
                  break;

               case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
                  xoffs = 8 * srcrect->left;
                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
                                             ? (void (*)(void *,int))&dstripeRGBA_16_LE_LOG
                                             : (void (*)(void *,int))&dstripeRGBA_Half;
                  break;
           }

         break;

         case IF_PIXEL_COMPONENTS_BGRA:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  //
                  xoffs = (srcrect->left/3)<<4;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeBGRA_10;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeBGRA_10_LOG;
                  }
                  //
                  break;

					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
						xoffs = 8 * srcrect->left;
                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
                                             ? (void (*)(void *,int))&dstripeBGRA_16_LE_LOG
                                             : (void (*)(void *,int))&dstripeBGRA_16_LE;
                  break;
           }

         break;

//         case IF_PIXEL_COMPONENTS_Y:
//         case IF_PIXEL_COMPONENTS_YYY:
//
//            switch(srcpelpacking) {
//
//               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
//                  xoffs = 4 * srcrect->left;
//                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
//                                             ? (void (*)(void *,int))&dstripeYYY_10_LOG
//                                             : (void (*)(void *,int))&dstripeYYY_10_LINEAR;
//                  break;
//
//               case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
//               case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
//                  // Ugggh, in this hack, _L and _R refer to the SOURCE alignment
//                  // at this point the 10 bits are always right-aligned in the 16 bits!
//                  xoffs = 6 * srcrect->left;
//                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
//                                             ? (void (*)(void *,int))&dstripeYYY_10_IN_16_LOG
//                                             : (void (*)(void *,int))&dstripeYYY_10_IN_16_LINEAR;
//                  break;
//
//					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
//					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
//						xoffs = 6 * srcrect->left;
//                  tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
//                                             ? (void (*)(void *,int))&dstripeYYY_16_LE_LOG
//                                             : (void (*)(void *,int))&dstripeYYY_16_LE_LINEAR;
//                  break;
//
//               default:
//                  MTIassert(false);
//                  break;
//            }
//
//         break;

//         case IF_PIXEL_COMPONENTS_LUMINANCE:
         case IF_PIXEL_COMPONENTS_Y:
         case IF_PIXEL_COMPONENTS_YYY:        // xyzzy1

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  xoffs = (srcrect->left/3)<<2;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeMON_10L;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeMON_10L_LOG;
                  }

                  break;

               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
                  xoffs = (srcrect->left/3)<<2;
                  phase = srcrect->left%3;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeMON_10R;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeMON_10R_LOG;
                  }

                  break;

               case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
                  xoffs = (srcrect->left/4)*5;
                  phase = srcrect->left%4;
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeMON_10P;
                  if ((srccolorspace == IF_COLOR_SPACE_LOG)||(lutptr != NULL)) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeMON_10P_LOG;
                  }

                  break;

					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
						xoffs = 2 * srcrect->left;
                  tsThread.PrimaryFunction = (void (*)(void *,int))&dstripeMON_16_LE;
                  break;

               default:
                  break;
            }

         break;

      }

      step = (double)winhght/(double)dsthght;
      run = (double) srcrect->top + .5*step;

      if (srcil) { // interlaced

         for (int i=0;i<dsthght;i++) {
            intrun = (int)run;
            ystrTbl[i].fld  = intrun&1;
            ystrTbl[i].offs = srcpitch * (intrun >> 1) + xoffs;
            run += step;
         }
      }
      else { // non-interlaced

         // break out the (funny) 10-bit DPX RGBA case - this does
         // not have a proper row pitch; the rows are all packed and
         // the phase of the first pixel of each row can vary
         if ((srcpelcomponents == IF_PIXEL_COMPONENTS_RGBA)&&
             (srcpelpacking == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L)) {

            for (int i=0;i<dsthght;i++) {
               intrun = (int)run;
               ystrTbl[i].fld  = 0x00;
               int pel = ((srcwdth * intrun)+(srcrect->left));
               ystrTbl[i].offs  = (pel/3)*16;
               ystrTbl[i].phase = pel%3;
               run += step;
            }
         }
#ifdef DPX12_NO_ROW_PADDING
         // break out the (funny) 12-bit DPX RGB case - this does
         // not have a proper row pitch; the rows are all packed and
         // the phase of the first pixel of each row can vary
         else if ((srcpelcomponents == IF_PIXEL_COMPONENTS_RGB)&&
             ((srcpelpacking == IF_PIXEL_PACKING_2_12Bits_IN_3Bytes)||
				  (srcpelpacking == IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR))) {

            for (int i=0;i<dsthght;i++) {
               intrun = (int)run;
               ystrTbl[i].fld  = 0x00;
               int pel = ((srcwdth * intrun)+(srcrect->left));
               ystrTbl[i].offs  = (pel/8)*36;
               ystrTbl[i].phase = pel%8;
               run += step;
            }
         }
#endif
         // break out the (funny) 10-bit DPX mono case - this does
         // not have a proper row pitch; the rows are all packed and
         // the phase of the first pixel of each row can vary
         else if (srcpelcomponents == IF_PIXEL_COMPONENTS_Y
               && (srcpelpacking == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L
                  ||srcpelpacking == IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R)) {

            for (int i=0;i<dsthght;i++) {
               intrun = (int)run;
               ystrTbl[i].fld  = 0x00;
               int pel = ((srcwdth * intrun)+(srcrect->left));
               ystrTbl[i].offs  = (pel/3)*4;
               ystrTbl[i].phase = pel%3;
               run += step;
            }

         }
         // break out the (funny) 10-bit DPX mono case - this does
         // not have a proper row pitch; the rows are all packed and
         // the phase of the first byte of each row can vary
         else if ((/* srcpelcomponents == IF_PIXEL_COMPONENTS_LUMINANCE
                  || */ srcpelcomponents == IF_PIXEL_COMPONENTS_Y)&&
             (srcpelpacking == IF_PIXEL_PACKING_4_10Bits_IN_5Bytes)) {

            for (int i=0;i<dsthght;i++) {
               intrun = (int)run;
               ystrTbl[i].fld  = 0x00;
               ystrTbl[i].offs = srcpitch * intrun + xoffs;
               ystrTbl[i].phase = phase;
               run += step;
            }

         }
         else {

            for (int i=0;i<dsthght;i++) {
               intrun = (int)run;
               ystrTbl[i].fld  = 0x00;
               ystrTbl[i].offs = srcpitch * intrun + xoffs;
               run += step;
            }
         }
      }

      //////////////////////////////////////////////////////////
      //
      // set current values
      //
      curSrcWdth          = srcwdth;
      curSrcHght          = srchght;
		curSrcPitch         = srcpitch;
      curSrcColorSpace    = srccolorspace;
      curSrcPelComponents = srcpelcomponents;
      curSrcPelPacking    = srcpelpacking;
      curInterlaced       = srcil;
      curAlphaMatteType   = srcAlphaMatteType;
		curWinWdth          = winwdth;
		curWinHght          = winhght;
		curDstWdth          = dstwdth;
      curDstHght          = dsthght;
      curDstPitch         = dstpitch;
      curSrcRect          = *srcrect;
      curConvertType      = NormalDisplayConvert;

	} // change control tables

   /////////////////////////////////////////////////////////////
   //
   // convert the frame by stripe
#ifdef NO_MULTI
   for (int i = 0; i < nStripes; i++)
   {
      tsThread.PrimaryFunction(this,i);
   }
#else
   // now allocate, run, and free the mthreads
   if (nStripes > 1)
   {
      int iRet = MThreadAlloc(&tsThread);
      if (iRet)
      {
            TRACE_0(errout << "Convert: MThreadAlloc failed, error=" << iRet);
      }
      else
      {
         iRet = MThreadStart(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadStart failed, error=" << iRet);
         }

         iRet = MThreadFree(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadFree failed, error=" << iRet);
         }
      }
   }
   else // nStripes <= 0
   {
      // If we're only doing one stripe, call the function directly.
      tsThread.PrimaryFunction(this,0);
   }
#endif

	TRACE_3(errout << "***************** DISPLAYCONVERT took " << hrTimer.Read() << " msec");
}

///////////////////////////////////////////////////////////////
//
// this routine outputs the row using the rgbmask and x stretch table

#if !MTI_ASM_X86_INTEL
void rowRGB(void *outrgb, void *row, void *v)
{
   CConvert *vp = (CConvert *)v;
   unsigned int rgbMask = vp->rgbMask;
	unsigned int *greyTbl = CConvert::GreyTbl;
	int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   unsigned int *rowout = (unsigned int *)row;
   unsigned int *rgbout = (unsigned int *)outrgb;
	unsigned int rawrgb;

	if ((rgbMask&0xff000000)==0) {
	  auto mask = (rgbMask | 0xff000000);
		for (;xstrTbl<xstrTblEnd;xstrTbl++) {
		 *rgbout++ = rowout[*xstrTbl]&mask;
		}
   }
   else {

      switch (rgbMask) {

			case 0xff000000:
				for (;xstrTbl<xstrTblEnd;xstrTbl++) {
					*rgbout++ = 0;
				}
			break;

			case 0xff0000ff:
				for (;xstrTbl<xstrTblEnd;xstrTbl++) {
					rawrgb = rowout[*xstrTbl];
					*rgbout++ = greyTbl[rawrgb&0xff];
				}
			break;

			case 0xff00ff00:
				for (;xstrTbl<xstrTblEnd;xstrTbl++) {
					rawrgb = rowout[*xstrTbl];
					*rgbout++ = greyTbl[((rawrgb&0xff00)>>8)];
				}
			break;

			case 0xffff0000:
				for (;xstrTbl<xstrTblEnd;xstrTbl++) {
					rawrgb = rowout[*xstrTbl];
				*rgbout++ = greyTbl[((rawrgb&0xff0000)>>16)];
				}
			break;

			case 0xff00ffff:
				for (;xstrTbl<xstrTblEnd;xstrTbl++) {
					rawrgb = rowout[*xstrTbl];
					*rgbout++ = greyTbl[((rawrgb&0xff)+((rawrgb&0xff00)>>8))>>1];
				}
			break;

			case 0xffff00ff:
				for (;xstrTbl<xstrTblEnd;xstrTbl++) {
					rawrgb = rowout[*xstrTbl];
					*rgbout++ = greyTbl[((rawrgb&0xff)+((rawrgb&0xff0000)>>16))>>1];
				}
			break;

			case 0xffffff00:
				for (;xstrTbl<xstrTblEnd;xstrTbl++) {
					rawrgb = rowout[*xstrTbl];
					*rgbout++ = greyTbl[(((rawrgb&0xff00)>>8)+((rawrgb&0xff0000)>>16))>>1];
				}
			break;

			case 0xffffffff:
				for (;xstrTbl<xstrTblEnd;xstrTbl++) {
					rawrgb = rowout[*xstrTbl];
					*rgbout++ = greyTbl[((rawrgb&0xff)+((rawrgb&0xff00)>>8)+((rawrgb&0xff0000)>>16))/3];
				}
			break;
		}
   }

   unsigned int pnMask  = vp->pnMask;
   unsigned char *posTbl = CConvert::PosTbl;
   unsigned char *negTbl = CConvert::NegTbl;
   unsigned char *begrgb = (unsigned char *)outrgb;
   unsigned char *endrgb = begrgb + 4*vp->curDstWdth;

   // $L200:test   pnMask,PNMASK_ANY
   //       jz     $L300
   if (pnMask & PNMASK_ANY)
   {
      //       mov    ebx,posTbl
      //       test   pnMask,PNMASK_POS
      //       jz     $L201
      //       mov    ebx,negTbl
      unsigned char *ebx = (pnMask & PNMASK_POS) ? posTbl : negTbl;

      // $L201:mov    esi,begrgb
      unsigned char *esi = begrgb;

      //       mov    edi,endrgb
      unsigned char *edi = endrgb;

      //       xor    eax,eax
      unsigned char al = 0;
      unsigned int eax = 0;

      unsigned char dl;
      unsigned int edx;

      do
      {
         // $L202:mov    edx,[esi]
         edx = *(unsigned int *)esi;
         dl = (unsigned char)(edx & 0xFF);

         //       mov    al,dl
         eax = al = dl;

         //       mov    dl,[ebx+eax]
         dl = ebx[eax];
         edx = (edx &~ 0xFF) | dl;

         //       ror    edx,8
         edx = (edx >> 8) | (dl << 24);
         dl = (unsigned char)(edx & 0xFF);

         //       mov    al,dl
         eax = al = dl;

         //       mov    dl,[ebx+eax]
         dl = ebx[eax];
         edx = (edx &~ 0xFF) | dl;

         //       ror    edx,8
         edx = (edx >> 8) | (dl << 24);
         dl = (unsigned char)(edx & 0xFF);

         //       mov    al,dl
         eax = al = dl;

         //       mov    dl,[ebx+eax]
         dl = ebx[eax];
         edx = (edx &~ 0xFF) | dl;

         //       ror    edx,16
         edx = (edx >> 16) | (edx << 16);

         //       mov    [esi],edx
         *(unsigned int *)esi = edx;

         //       add    esi,4
         esi += 4;

      //       cmp    esi,edi
      //       jb     $L202
      } while (esi < edi);
   }
   // $L300:
}
#else
void rowRGB(void *out, void *vrow, void *v)
{
   CConvert *vp = (CConvert *)v;

   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask  = vp->pnMask;
   unsigned int *greyTbl = CConvert::GreyTbl;
   unsigned char *posTbl = CConvert::PosTbl;
   unsigned char *negTbl = CConvert::NegTbl;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *) (vp->xstrTbl + vp->curDstWdth);
   unsigned char *begrgb = (unsigned char *)out;
   unsigned char *outrgb = begrgb;
   unsigned char *endrgb = begrgb + 4*vp->curDstWdth;
   unsigned char *row = (unsigned char *)vrow;

      _asm {
      mov    edi,outrgb
      mov    esi,xstrTbl
      mov    eax,xstrTblEnd
      mov    ebx,row
      mov    ecx,rgbMask
      test   ecx,0xff000000
      je     $COL
      cmp    ecx,0xff000000
      je     $COL
      cmp    ecx,0xff0000ff
      je     $R
      cmp    ecx,0xff00ff00
      je     $G
      cmp    ecx,0xffff0000
      je     $B
      cmp    ecx,0xff00ffff
      je     $RG
      cmp    ecx,0xffff00ff
      je     $RB
      cmp    ecx,0xffffff00
      je     $GB
      jmp    $RGB

$COL: or     ecx,0xff000000
$L100:mov    edx,[esi]
      mov    edx,[ebx+4*edx]
      and    edx,ecx
      mov    [edi],edx
      add    edi,4
      add    esi,4
      cmp    esi,eax
      jb     $L100
      jmp    $L200

$R:   mov    eax,greyTbl
$L101:mov    edx,[esi]
      mov    edx,[ebx+4*edx]
      and    edx,0xff
      mov    edx,[eax+4*edx]
      mov    [edi],edx
      add    edi,4
      add    esi,4
      cmp    esi,xstrTblEnd
      jb     $L101
      jmp    $L200

$G:   mov    eax,greyTbl
$L102:mov    edx,[esi]
      mov    edx,[ebx+4*edx]
      and    edx,0xff00
      shr    edx,8
      mov    edx,[eax+4*edx]
      mov    [edi],edx
      add    edi,4
      add    esi,4
      cmp    esi,xstrTblEnd
      jb     $L102
      jmp    $L200

$B:   mov    eax,greyTbl
$L103:mov    edx,[esi]
      mov    edx,[ebx+4*edx]
      and    edx,0xff0000
      shr    edx,16
      mov    edx,[eax+4*edx]
      mov    [edi],edx
      add    edi,4
      add    esi,4
      cmp    esi,xstrTblEnd
      jb     $L103
      jmp    $L200

$RG:  mov    ecx,greyTbl
$L104:mov    edi,[esi]
      sal    edi,2
      add    edi,ebx
      movzx  eax,[edi]
      movzx  edx,[edi+1]
      add    eax,edx
      shr    eax,1
      mov    edx,[ecx+4*eax]
      mov    edi,outrgb
      mov    [edi],edx
      add    outrgb,4
      add    esi,4
      cmp    esi,xstrTblEnd
      jb     $L104
      jmp    $L200

$RB:  mov    ecx,greyTbl
$L105:mov    edi,[esi]
      sal    edi,2
      add    edi,ebx
      movzx  eax,[edi]
      movzx  edx,[edi+2]
      add    eax,edx
      shr    eax,1
      mov    edx,[ecx+4*eax]
      mov    edi,outrgb
      mov    [edi],edx
      add    outrgb,4
      add    esi,4
      cmp    esi,xstrTblEnd
      jb     $L105
      jmp    $L200

$GB:  mov    ecx,greyTbl
$L106:mov    edi,[esi]
      sal    edi,2
      add    edi,ebx
      movzx  eax,[edi+1]
      movzx  edx,[edi+2]
      add    eax,edx
      shr    eax,1
      mov    edx,[ecx+4*eax]
      mov    edi,outrgb
      mov    [edi],edx
      add    outrgb,4
      add    esi,4
      cmp    esi,xstrTblEnd
      jb     $L106
      jmp    $L200

$RGB:
$L107:mov    edi,[esi]
      sal    edi,2
      add    edi,ebx
      movzx  eax,[edi]
      movzx  edx,[edi+1]
      add    eax,edx
      movzx  edx,[edi+2]
      add    eax,edx
      xor    edx,edx
      mov    ecx,3
      div    ecx
      mov    ecx,greyTbl
      mov    edx,[ecx+4*eax]
      mov    edi,outrgb
      mov    [edi],edx
      add    outrgb,4
      add    esi,4
      cmp    esi,xstrTblEnd
      jb     $L107
      jmp    $L200

      // if "pos" or "neg" feature is enabled
      // for difference onion-skinning, process
      // each output channel accordingly

$L200:test   pnMask,PNMASK_ANY
      jz     $L300
      mov    ebx,posTbl
      test   pnMask,PNMASK_POS
      jz     $L201
      mov    ebx,negTbl

$L201:mov    esi,begrgb
      mov    edi,endrgb
      xor    eax,eax
$L202:mov    edx,[esi]
      mov    al,dl
      mov    dl,[ebx+eax]
      ror    edx,8
      mov    al,dl
      mov    dl,[ebx+eax]
      ror    edx,8
      mov    al,dl
      mov    dl,[ebx+eax]
      ror    edx,16
      mov    [esi],edx
      add    esi,4
      cmp    esi,edi
      jb     $L202
$L300:
      }
      return;
 }
#endif

///////////////////////////////////////////////////////////////

void rowRGBwithLimit(void *outrgb, void *row, void *v, int limit)
{
	CConvert *vp = (CConvert*)v;
	unsigned int rgbMask = vp->rgbMask;
	unsigned int *greyTbl = CConvert::GreyTbl;
	int *xstrTbl = vp->xstrTbl;
	int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
	unsigned int *rowout = (unsigned int *)row;
	unsigned int *rgbout = (unsigned int *)outrgb;
	unsigned int rawrgb;
	unsigned char *begrgb = (unsigned char *)outrgb;
	unsigned char *endrgb = begrgb + 4 * vp->curDstWdth;
	unsigned char *lastRgbWritten = endrgb;

	if ((rgbMask & 0xff000000) == 0)
	{
		auto mask = (rgbMask | 0xff000000);
		auto count = xstrTblEnd - xstrTbl;
		for (auto i = 0; i < count; i++)
		{
			if (i >= limit)
			{
				lastRgbWritten = (unsigned char *) rgbout;
				break;
			}

			*rgbout++ = rowout[xstrTbl[i]] & mask;
		}
	}
	else
	{
		switch (rgbMask)
		{
			case 0xff000000 :
			for (; xstrTbl < xstrTblEnd; xstrTbl++)
			{
				if (*xstrTbl >= limit)
				{
					lastRgbWritten = (unsigned char *) rgbout;
					break;
				}

				*rgbout++ = 0;
			}

			break;

			case 0xff0000ff :
			for (; xstrTbl < xstrTblEnd; xstrTbl++)
			{
				if (*xstrTbl >= limit)
				{
					lastRgbWritten = (unsigned char *) rgbout;
					break;
				}

				rawrgb = rowout[*xstrTbl];
				*rgbout++ = greyTbl[rawrgb & 0xff];
			}

			break;

			case 0xff00ff00 :
			for (; xstrTbl < xstrTblEnd; xstrTbl++)
			{
				if (*xstrTbl >= limit)
				{
					lastRgbWritten = (unsigned char *) rgbout;
					break;
				}

				rawrgb = rowout[*xstrTbl];
				*rgbout++ = greyTbl[((rawrgb & 0xff00) >> 8)];
			}

			break;

			case 0xffff0000 :
			for (; xstrTbl < xstrTblEnd; xstrTbl++)
			{
				if (*xstrTbl >= limit)
				{
					break;
				}

				rawrgb = rowout[*xstrTbl];
				*rgbout++ = greyTbl[((rawrgb & 0xff0000) >> 16)];
			}

			break;

			case 0xff00ffff :
			for (; xstrTbl < xstrTblEnd; xstrTbl++)
			{
				if (*xstrTbl >= limit)
				{
					lastRgbWritten = (unsigned char *) rgbout;
					break;
				}

				rawrgb = rowout[*xstrTbl];
				*rgbout++ = greyTbl[((rawrgb & 0xff) + ((rawrgb & 0xff00) >> 8)) >> 1];
			}

			break;

			case 0xffff00ff :
			for (; xstrTbl < xstrTblEnd; xstrTbl++)
			{
				if (*xstrTbl >= limit)
				{
					lastRgbWritten = (unsigned char *) rgbout;
					break;
				}

				rawrgb = rowout[*xstrTbl];
				*rgbout++ = greyTbl[((rawrgb & 0xff) + ((rawrgb & 0xff0000) >> 16)) >> 1];
			}

			break;

			case 0xffffff00 :
			for (; xstrTbl < xstrTblEnd; xstrTbl++)
			{
				if (*xstrTbl >= limit)
				{
					lastRgbWritten = (unsigned char *) rgbout;
					break;
				}

				rawrgb = rowout[*xstrTbl];
				*rgbout++ = greyTbl[(((rawrgb & 0xff00) >> 8) + ((rawrgb & 0xff0000) >> 16)) >> 1];
			}

			break;

			case 0xffffffff :
			for (; xstrTbl < xstrTblEnd; xstrTbl++)
			{
				if (*xstrTbl >= limit)
				{
					lastRgbWritten = (unsigned char *) rgbout;
					break;
				}

				rawrgb = rowout[*xstrTbl];
				*rgbout++ = greyTbl[((rawrgb & 0xff) + ((rawrgb & 0xff00) >> 8) + ((rawrgb & 0xff0000) >> 16)) / 3];
			}

			break;
		}
	}

	unsigned int pnMask = vp->pnMask;
	unsigned char *posTbl = CConvert::PosTbl;
	unsigned char *negTbl = CConvert::NegTbl;

	// $L200:test   pnMask,PNMASK_ANY
	// jz     $L300
	if (pnMask & PNMASK_ANY)
	{
		// mov    ebx,posTbl
		// test   pnMask,PNMASK_POS
		// jz     $L201
		// mov    ebx,negTbl
		unsigned char *ebx = (pnMask & PNMASK_POS) ? posTbl : negTbl;

		// $L201:mov    esi,begrgb
		unsigned char *esi = begrgb;

		// mov    edi,endrgb
		unsigned char *edi = endrgb;

		// xor    eax,eax
		unsigned char al = 0;
		unsigned int eax = 0;

		unsigned char dl;
		unsigned int edx;

		do
		{
			if (esi > lastRgbWritten)
			{
				break;
			}

			// $L202:mov    edx,[esi]
			edx = *(unsigned int *)esi;
			dl = (unsigned char)(edx & 0xFF);

			// mov    al,dl
			eax = al = dl;

			// mov    dl,[ebx+eax]
			dl = ebx[eax];
			edx = (edx&~0xFF) | dl;

			// ror    edx,8
			edx = (edx >> 8) | (dl << 24);
			dl = (unsigned char)(edx & 0xFF);

			// mov    al,dl
			eax = al = dl;

			// mov    dl,[ebx+eax]
			dl = ebx[eax];
			edx = (edx&~0xFF) | dl;

			// ror    edx,8
			edx = (edx >> 8) | (dl << 24);
			dl = (unsigned char)(edx & 0xFF);

			// mov    al,dl
			eax = al = dl;

			// mov    dl,[ebx+eax]
			dl = ebx[eax];
			edx = (edx&~0xFF) | dl;

			// ror    edx,16
			edx = (edx >> 16) | (edx << 16);

			// mov    [esi],edx
			*(unsigned int *)esi = edx;

			// add    esi,4
			esi += 4;

			// cmp    esi,edi
			// jb     $L202
		}
		while (esi < edi);
	}
	// $L300:
}

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
void makeRowGrey(MTI_UINT8 *rgb, void *v)
{
	CConvert *vp = (CConvert *) v;

	MTI_UINT8 *rgbend   = rgb + 4*vp->curDstWdth;
	MTI_UINT32 rgbMask  = vp->rgbMask;
	MTI_UINT32 *greyTbl = vp->GreyTbl;

	MTI_UINT8 *esi;
   MTI_UINT8 *edi;
   MTI_UINT32 eax;
   MTI_UINT32 *ebx;
   MTI_UINT32 ecx;
   MTI_UINT32 edx;

   //       mov    esi,rgb
   esi = rgb;

   //       mov    edi,rgbend
   edi = rgbend;

   //       mov    ebx,greyTbl
   ebx = greyTbl;

   //       mov    ecx,rgbMask
   ecx = rgbMask;

   switch (ecx)
   {
      //       cmp    ecx,0xff000000
      //       je     $L200
      //       cmp    ecx,0xff0000ff
      //       je     $R
      //       cmp    ecx,0xff00ff00
      //       je     $G
      //       cmp    ecx,0xffff0000
      //       je     $B
      //       cmp    ecx,0xff00ffff
      //       je     $RG
      //       cmp    ecx,0xffff00ff
      //       je     $RB
      //       cmp    ecx,0xffffff00
      //       je     $GB
      //       jmp    $RGB


      case 0xff000000:
         break;

      // $R:
      case 0xff0000ff:

         do {

            // $L101:movzx  eax,[esi]
            eax = esi[0];

            //       mov    eax,[ebx+4*eax]
            eax = ebx[eax];

            //       mov    [esi],eax
            *(MTI_UINT32 *)esi = eax;

            //       add    esi,4
            esi += 4;

         //       cmp    esi,edi
         //       jb     $L101
         } while (esi < edi);

         //       jmp    $L200
         break;

      // $G:
      case 0xff00ff00:

         do {

            // $L102:movzx  eax,[esi+1]
            eax = esi[1];

            //       mov    eax,[ebx+4*eax]
            eax = ebx[eax];

            //       mov    [esi],eax
            *(MTI_UINT32 *)esi = eax;

            //       add    esi,4
            esi += 4;

         //       cmp    esi,edi
         //       jb     $L102
         } while (esi < edi);

         //       jmp    $L200
         break;

      // $B:
      case 0xffff0000:

         do {

            // $L103:movzx  eax,[esi+2]
            eax = esi[2];

            //       mov    eax,[ebx+4*eax]
            eax = ebx[eax];

            //       mov    [esi],eax
            *(MTI_UINT32 *)esi = eax;

            //       add    esi,4
            esi += 4;

         //       cmp    esi,edi
         //       jb     $L103
         } while (esi < edi);

         //       jmp    $L200
         break;

      // $RG:
      case 0xff00ffff:

         do {
            // $L104:movzx  eax,[esi]
            eax = esi[0];

            //       movzx  edx,[esi+1]
            edx = esi[1];

            //       add    eax,edx
            eax += edx;

            //       sar    eax,1
            eax >>= 1;

            //       mov    eax,[ebx+4*eax]
            eax = ebx[eax];

            //       mov    [esi],eax
            *(MTI_UINT32 *)esi = eax;

            //       add    esi,4
            esi += 4;

         //       cmp    esi,edi
         //       jb     $L104
         } while (esi < edi);

         //       jmp    $L200
         break;

      // $RB:
      case 0xffff00ff:

         do {
            // $L105:movzx  eax,[esi]
            eax = esi[0];

            //       movzx  edx,[esi+2]
            edx = esi[2];

            //       add    eax,edx
            eax += edx;

            //       sar    eax,1
            eax >>= 1;

            //       mov    eax,[ebx+4*eax]
            eax = ebx[eax];

            //       mov    [esi],eax
            *(MTI_UINT32 *)esi = eax;

            //       add    esi,4
            esi += 4;

         //       cmp    esi,edi
         //       jb     $L105
         } while (esi < edi);

         //       jmp    $L200
         break;

      // $GB:
      case 0xffffff00:

         do {
            // $L106:movzx  eax,[esi+1]
            eax = esi[1];

            //       movzx  edx,[esi+2]
            edx = esi[2];

            //       add    eax,edx
            eax += edx;

            //       sar    eax,1
            eax >>= 1;

            //       mov    eax,[ebx+4*eax]
            eax = ebx[eax];

            //       mov    [esi],eax
            *(MTI_UINT32 *)esi = eax;

            //       add    esi,4
            esi += 4;

         //       cmp    esi,edi
         //       jb     $L106
         } while (esi < edi);

         //       jmp    $L200
         break;

      // $RGB:
      default:
      case 0xffffffff:

         do {
            // $L107:movzx  eax,[esi]
            eax = esi[0];

            //       movzx  edx,[esi+1]
            edx = esi[1];

            //       add    eax,edx
            eax += edx;

            //       movzx  edx,[esi+2]
            edx = esi[2];

            //       add    eax,edx
            eax += edx;

            //       xor    edx,edx
            //       mov    ecx,3
            //       div    ecx
            eax /= 3;

            //       mov    eax,[ebx+4*eax]
            eax = ebx[eax];

            //       mov    [esi],eax
            *(MTI_UINT32 *)esi = eax;

            //       add    esi,4
            esi += 4;

         //       cmp    esi,edi
         //       jb     $L107
         } while (esi < edi);

         //       jmp    $L200
         break;

   // $L200:
   }
}
#else
void makeRowGrey(MTI_UINT8 *rgb, void *v)
{
   CConvert *vp = (CConvert *) v;

   MTI_UINT8 *rgbend   = rgb + 4*vp->curDstWdth;
   MTI_UINT32 rgbMask  = vp->rgbMask;
   MTI_UINT32 *greyTbl = vp->GreyTbl;

      _asm {
      mov    esi,rgb
      mov    edi,rgbend
      mov    ebx,greyTbl
      mov    ecx,rgbMask
      cmp    ecx,0xff000000
      je     $L200
      cmp    ecx,0xff0000ff
      je     $R
      cmp    ecx,0xff00ff00
      je     $G
      cmp    ecx,0xffff0000
      je     $B
      cmp    ecx,0xff00ffff
      je     $RG
      cmp    ecx,0xffff00ff
      je     $RB
      cmp    ecx,0xffffff00
      je     $GB
      jmp    $RGB

$R:
$L101:movzx  eax,[esi]
      mov    eax,[ebx+4*eax]
      mov    [esi],eax
      add    esi,4
      cmp    esi,edi
      jb     $L101
      jmp    $L200

$G:
$L102:movzx  eax,[esi+1]
      mov    eax,[ebx+4*eax]
      mov    [esi],eax
      add    esi,4
      cmp    esi,edi
      jb     $L102
      jmp    $L200

$B:
$L103:movzx  eax,[esi+2]
      mov    eax,[ebx+4*eax]
      mov    [esi],eax
      add    esi,4
      cmp    esi,edi
      jb     $L103
      jmp    $L200

$RG:
$L104:movzx  eax,[esi]
      movzx  edx,[esi+1]
      add    eax,edx
      sar    eax,1
      mov    eax,[ebx+4*eax]
      mov    [esi],eax
      add    esi,4
      cmp    esi,edi
      jb     $L104
      jmp    $L200

$RB:
$L105:movzx  eax,[esi]
      movzx  edx,[esi+2]
      add    eax,edx
      sar    eax,1
      mov    eax,[ebx+4*eax]
      mov    [esi],eax
      add    esi,4
      cmp    esi,edi
      jb     $L105
      jmp    $L200

$GB:
$L106:movzx  eax,[esi+1]
      movzx  edx,[esi+2]
      add    eax,edx
      sar    eax,1
      mov    eax,[ebx+4*eax]
      mov    [esi],eax
      add    esi,4
      cmp    esi,edi
      jb     $L106
      jmp    $L200

$RGB:
$L107:movzx  eax,[esi]
      movzx  edx,[esi+1]
      add    eax,edx
      movzx  edx,[esi+2]
      add    eax,edx
      xor    edx,edx
      mov    ecx,3
      div    ecx
      mov    eax,[ebx+4*eax]
      mov    [esi],eax
      add    esi,4
      cmp    esi,edi
      jb     $L107

$L200:
      }
      return;
}
#endif

#if !MTI_ASM_X86_INTEL
void makeRowPosNeg(MTI_UINT8 *rgb, void *v)
{
   CConvert *vp = (CConvert *) v;

   MTI_UINT8 *rgbend   = rgb + 4*vp->curDstWdth;
   MTI_UINT32 pnMask  = vp->pnMask;
   MTI_UINT8 *pnTbl;

   if (pnMask == PNMASK_POS) {
      pnTbl = vp->PosTbl;
   }

   if (pnMask==PNMASK_NEG) {
      pnTbl = vp->NegTbl;
   }

//    mov    esi,rgb
   MTI_UINT8 *esi = rgb;

//    mov    edi,rgbend
   MTI_UINT8 *edi = rgbend;

//    mov    ebx,pnTbl
   MTI_UINT8 *ebx = pnTbl;

   MTI_UINT32 eax;
   #define al ((MTI_UINT8)(eax & 0xFF))

   do {
      // $L101:

      //    movzx  eax,BYTE PTR[esi]
      eax = esi[0];

      //    mov    eax,[ebx+eax]
      eax = *(MTI_UINT32 *)(ebx + eax);

      //    mov    [esi],al
      esi[0] = al;

      //    movzx  eax,BYTE PTR[esi+1]
      eax = esi[1];

      //    mov    eax,[ebx+eax]
      eax = *(MTI_UINT32 *)(ebx + eax);

      //    mov    [esi+1],al
      esi[1] = al;

      //    movzx  eax,BYTE PTR[esi+2]
      eax = esi[2];

      //    mov    eax,[ebx+eax]
      eax = *(MTI_UINT32 *)(ebx + eax);

      //    mov    [esi+2],al
      esi[2] = al;

      //    add    esi,4
      esi += 4;

   //    cmp    esi,edi
   //    jb     $L101
   } while (esi < edi);

   #undef al
}
#else
void makeRowPosNeg(MTI_UINT8 *rgb, void *v)
{
   CConvert *vp = (CConvert *) v;

   MTI_UINT8 *rgbend   = rgb + 4*vp->curDstWdth;
   MTI_UINT32 pnMask  = vp->pnMask;
   MTI_UINT8 *pnTbl;

   if (pnMask==PNMASK_POS) {
      pnTbl = vp->PosTbl;
   }
   if (pnMask==PNMASK_NEG) {
      pnTbl = vp->NegTbl;
   }

   _asm {
   mov    esi,rgb
   mov    edi,rgbend
   mov    ebx,pnTbl

$L101:

   movzx  eax,BYTE PTR[esi]
   mov    eax,[ebx+eax]
   mov    [esi],al
   movzx  eax,BYTE PTR[esi+1]
   mov    eax,[ebx+eax]
   mov    [esi+1],al
   movzx  eax,BYTE PTR[esi+2]
   mov    eax,[ebx+eax]
   mov    [esi+2],al
   add    esi,4
   cmp    esi,edi
   jb     $L101
   }
}
#endif


// merges the alpha color into the output row. this must
// have been processed in such a way that zeroes appear in
// those places where alpha has been detected.
void rowALPHA(MTI_UINT8 *outrgb, MTI_UINT8 *row, void *v)
{
   CConvert *vp = (CConvert *)v;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   unsigned int *rowout = (unsigned int *)row;
   unsigned int *rgbout = (unsigned int *)outrgb;

   for (;xstrTbl<xstrTblEnd;xstrTbl++) {
      *rgbout += rowout[*xstrTbl];
      rgbout++;
   }
}
///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeYUV_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned int rgbMask = vp->rgbMask;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
	ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
          unsigned int r0;

         while (dst < rowend) {

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = *((int *)src);
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
#endif
            src += 4;

            base = ((r0>>10)&msku)+((r0>>1)&mskv);
            lutptr = lut + 3 * (base + ((r0>>16)&msky));
            dst[0] = lutptr[2];
            dst[1] = lutptr[1];
            dst[2] = lutptr[0];
            dst[3] = 0x7f;
            lutptr = lut + 3 * (base + ((r0    )&msky));
            dst[4] = lutptr[2];
            dst[5] = lutptr[1];
            dst[6] = lutptr[0];
            dst[7] = 0x7f;
            dst += 8;
			}
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeYUV_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         while (dst < rowend) {

            _asm {
            mov    ebx,src
            mov    esi,lut
            mov    edi,dst

            mov    eax,[ebx]
            bswap  eax
            mov    ebx,eax
            mov    ecx,eax
            mov    edx,eax

            // base = ((r0>>10)&msku)+((r0>>1)&mskv));
            shr    eax,10
            and    eax,msku
            shr    edx,1
            and    edx,mskv
            add    edx,eax

            // r = 3 * (base + ((r0>>16)&msky));
            shr    ebx,16
            and    ebx,msky

            add    ebx,edx
            mov    eax,ebx
            sal    ebx,1
            add    ebx,eax

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    al,[esi+ebx]
            mov    ah,0x7f
            shl    eax,16
            mov    al,[esi+ebx+2]
            mov    ah,[esi+ebx+1]
            mov    [edi],eax

            // r = 3 * (base + ((r0    )&msky));
            and    ecx,msky

            add    ecx,edx
            mov    eax,ecx
            sal    ecx,1
            add    ecx,eax

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    al,[esi+ecx]
            mov    ah,0x7f
            shl    eax,16
            mov    al,[esi+ecx+2]
            mov    ah,[esi+ecx+1]
            mov    [edi+4],eax
            add     dst,8
            }

            src += 4;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeYUV_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned int rgbMask = vp->rgbMask;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = *((int *)src);
            r1 = src[4];
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = src[4];
#endif
            src += 5;

            base = ((r0>>10)&msku)+((r0<<3)&mskv);
            lutptr = lut + 3 * (base + ((r0>>14)&msky));
            dst[0] = lutptr[2];
            dst[1] = lutptr[1];
            dst[2] = lutptr[0];
            dst[3] = 0x7f;
            r0 = (r0<<8)+r1;
            lutptr = lut + 3 * (base + ((r0 >> 2)&msky));
            dst[4] = lutptr[2];
            dst[5] = lutptr[1];
            dst[6] = lutptr[0];
            dst[7] = 0x7f;
            dst += 8;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeYUV_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         while (dst < rowend) {

            _asm {
            mov    ebx,src
            mov    esi,lut
            mov    edi,dst

            mov    eax,[ebx]
            bswap  eax
            mov    ecx,eax
            shl    ecx,8
            mov    cl,[ebx+4]
            mov    ebx,eax
            mov    edx,eax

            // base = (((r0>>10)&msku)+((r0<<3)&mskv));
            shr    eax,10
            and    eax,msku
            shl    edx,3
            and    edx,mskv
            add    edx,eax

            // r = 3 * (base + ((r0>>14)&msky));
            shr    ebx,14
            and    ebx,msky

            add    ebx,edx
            mov    eax,ebx
            sal    ebx,1
            add    ebx,eax

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    al,[esi+ebx]
            mov    ah,0x7f
            shl    eax,16
            mov    al,[esi+ebx+2]
            mov    ah,[esi+ebx+1]
            mov    [edi],eax

            // r0 = (r0<<8)+r1;
            // r = 3 * (base + ((r0>>2)&msky));
            shr    ecx,2
            and    ecx,msky

            add    ecx,edx
            mov    eax,ecx
            sal    ecx,1
            add    ecx,eax

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    al,[esi+ecx]
            mov    ah,0x7f
            shl    eax,16
            mov    al,[esi+ecx+2]
            mov    ah,[esi+ecx+1]
            mov    [edi+4],eax
            add     dst,8
            }

            src += 5;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#endif
///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeDVS_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned int rgbMask = vp->rgbMask;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

    for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
         unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
         r1 = (src[7]<<24)+(src[6]<<16)+(src[5]<<8)+src[4];
         r2 = (src[11]<<24)+(src[10]<<16)+(src[9]<<8)+src[8];
         r3 = (src[15]<<24)+(src[14]<<16)+(src[13]<<8)+src[12];
#else
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
#endif
         src += 16;

         if (rowphase==0) goto p0;
         if (rowphase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               base = ((r0<<12)&msku)+((r0>>15)&mskv);
               lutptr = lut + 3 * (base + ((r0>>12)&msky));
               dst[0] = lutptr[2];
               dst[1] = lutptr[1];
               dst[2] = lutptr[0];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r1>>2)&msky));
               dst[4] = lutptr[2];
               dst[5] = lutptr[1];
               dst[6] = lutptr[0];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p1:; // phase 1

               base = ((r1<<2)&msku)+((r2<<5)&mskv);
               lutptr = lut + 3 * (base + ((r1>>22)&msky));
               dst[0] = lutptr[2];
               dst[1] = lutptr[1];
               dst[2] = lutptr[0];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r2>>12)&msky));
               dst[4] = lutptr[2];
               dst[5] = lutptr[1];
               dst[6] = lutptr[0];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               base = ((r2>>8)&msku)+((r3>>5)&mskv);
               lutptr = lut + 3 * (base + ((r3>>2)&msky));
               dst[0] = lutptr[2];
               dst[1] = lutptr[1];
               dst[2] = lutptr[0];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r3>>22)&msky));
               dst[4] = lutptr[2];
               dst[5] = lutptr[1];
               dst[6] = lutptr[0];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
               r1 = (src[7]<<24)+(src[6]<<16)+(src[5]<<8)+src[4];
               r2 = (src[11]<<24)+(src[10]<<16)+(src[9]<<8)+src[8];
               r3 = (src[15]<<24)+(src[14]<<16)+(src[13]<<8)+src[12];
#else
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
#endif
               src += 16;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeDVS_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         if (rowphase==0) goto p0;
         if (rowphase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               _asm {
               mov    ebx,src
               mov    esi,lut
               mov    edi,dst

               // base = ((r0<<12)&msku)+((r0>>15)&mskv);
               mov    eax,[ebx]
               shl    eax,12
               and    eax,msku

               mov    edx,[ebx]
               shr    edx,15
               and    edx,mskv
               add    edx,eax

               // r = 3 * (base + ((r0>>12)&msky));
               mov    ebx,[ebx]
               shr    ebx,12
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi],eax

               // r = 3 * (base + ((r1>>2)&msky));
               mov    ebx,src
               mov    ebx,[ebx+4]
               shr    ebx,2
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi+4],eax
               add     dst,8
               }

               if (dst == rowend) break;

            p1:; // phase 1

               _asm {
               mov    ebx,src
               mov    esi,lut
               mov    edi,dst

               // base = ((r1<<2)&msku)+((r2<<5)&mskv);
               mov    eax,[ebx+4]
               shl    eax,2
               and    eax,msku

               mov    edx,[ebx+8]
               shl    edx,5
               and    edx,mskv
               add    edx,eax

               // r = 3 * (base + ((r1>>22)&msky));
               mov    ebx,[ebx+4]
               shr    ebx,22
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi],eax

               // r = 3 * (base + ((r2>>12)&msky));
               mov    ebx,src
               mov    ebx,[ebx+8]
               shr    ebx,12
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi+4],eax
               add     dst,8
               }

               if (dst == rowend) break;

            p2:; // phase 2

               _asm {
               mov    ebx,src
               mov    esi,lut
               mov    edi,dst

               // base = ((r2>>8)&msku)+((r3>>5)&mskv);
               mov    eax,[ebx+8]
               shr    eax,8
               and    eax,msku

               mov    edx,[ebx+12]
               shr    edx,5
               and    edx,mskv
               add    edx,eax

               // r = 3 * (base + ((r3>>2)&msky));
               mov    ebx,[ebx+12]
               shr    ebx,2
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi],eax

               // r = 3 * (base + ((r3>>22)&msky)));
               mov    ebx,src
               mov    ebx,[ebx+12]
               shr    ebx,22
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi+4],eax
               add     dst,8
               }

               if (dst == rowend) break;

               src += 16;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeDVS_10a(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned int rgbMask = vp->rgbMask;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
         unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
         r1 = (src[7]<<24)+(src[6]<<16)+(src[5]<<8)+src[4];
         r2 = (src[11]<<24)+(src[10]<<16)+(src[9]<<8)+src[8];
         r3 = (src[15]<<24)+(src[14]<<16)+(src[13]<<8)+src[12];
#else
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
#endif
         src += 16;

         if (rowphase==0) goto p0;
         if (rowphase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               base = ((r0<<14)&msku)+((r0>>9)&mskv);
               lutptr = lut + 3 * (base + ((r0>>8)&msky));
               dst[0] = lutptr[2];
               dst[1] = lutptr[1];
               dst[2] = lutptr[0];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + (r1&msky));
               dst[4] = lutptr[2];
               dst[5] = lutptr[1];
               dst[6] = lutptr[0];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p1:; // phase 1

               base = ((r1<<6)&msku)+((r2<<7)&mskv);
               lutptr = lut + 3 * (base + ((r1>>16)&msky));
               dst[0] = lutptr[2];
               dst[1] = lutptr[1];
               dst[2] = lutptr[0];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r2>>8)&msky));
               dst[4] = lutptr[2];
               dst[5] = lutptr[1];
               dst[6] = lutptr[0];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               base = ((r2>>2)&msku)+((r3>>1)&mskv);
               lutptr = lut + 3 * (base + (r3&msky));
               dst[0] = lutptr[2];
               dst[1] = lutptr[1];
               dst[2] = lutptr[0];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r3>>16)&msky));
               dst[4] = lutptr[2];
               dst[5] = lutptr[1];
               dst[6] = lutptr[0];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
               r1 = (src[7]<<24)+(src[6]<<16)+(src[5]<<8)+src[4];
               r2 = (src[11]<<24)+(src[10]<<16)+(src[9]<<8)+src[8];
               r3 = (src[15]<<24)+(src[14]<<16)+(src[13]<<8)+src[12];
#else
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
#endif
               src += 16;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeDVS_10a(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         if (rowphase==0) goto p0;
         if (rowphase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               _asm {
               mov    ebx,src
               mov    esi,lut
               mov    edi,dst

               // base = ((r0<<14)&msku)+((r0>>9)&mskv);
               mov    eax,[ebx]
               shl    eax,14
               and    eax,msku

               mov    edx,[ebx]
               shr    edx,9
               and    edx,mskv
               add    edx,eax

               // r = 3 * (base + ((r0>>8)&msky));
               mov    ebx,[ebx]
               shr    ebx,8
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi],eax

               // r = 3 * (base + ((r1)&msky));
               mov    ebx,src
               mov    ebx,[ebx+4]
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi+4],eax
               add     dst,8
               }

               if (dst == rowend) break;

            p1:; // phase 1

               _asm {
               mov    ebx,src
               mov    esi,lut
               mov    edi,dst

               // base = ((r1<<6)&msku)+((r2<<7)&mskv);
               mov    eax,[ebx+4]
               shl    eax,6
               and    eax,msku

               mov    edx,[ebx+8]
               shl    edx,7
               and    edx,mskv
               add    edx,eax

               // r = 3 * (base + ((r1>>16)&msky));
               mov    ebx,[ebx+4]
               shr    ebx,16
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi],eax

               // r = 3 * (base + ((r2>>8)&msky));
               mov    ebx,src
               mov    ebx,[ebx+8]
               shr    ebx,8
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi+4],eax
               add     dst,8
               }

               if (dst == rowend) break;

            p2:; // phase 2

               _asm {
               mov    ebx,src
               mov    esi,lut
               mov    edi,dst

               // base = ((r2>>2)&msku)+((r3>>1)&mskv);
               mov    eax,[ebx+8]
               shr    eax,2
               and    eax,msku

               mov    edx,[ebx+12]
               shr    edx,1
               and    edx,mskv
               add    edx,eax

               // r = 3 * (base + ((r3)&msky));
               mov    ebx,[ebx+12]
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi],eax

               // r = 3 * (base + ((r3>>16)&msky)));
               mov    ebx,src
               mov    ebx,[ebx+12]
               shr    ebx,16
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi+4],eax
               add     dst,8
               }

               if (dst == rowend) break;

               src += 16;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeDVS_10BE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned int rgbMask = vp->rgbMask;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

    for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         int base;
         unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
#else
         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+(src[3]);
         r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+(src[7]);
         r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+(src[11]);
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+(src[15]);
#endif
         src += 16;

         if (rowphase==0) goto p0;
         if (rowphase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               base = ((r0>>10)&msku)+((r0<<3)&mskv);
               lutptr = lut + 3 * (base + ((r0>>14)&msky));
               dst[0] = lutptr[2];
               dst[1] = lutptr[1];
               dst[2] = lutptr[0];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r1>>24)&msky));
               dst[4] = lutptr[2];
               dst[5] = lutptr[1];
               dst[6] = lutptr[0];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p1:; // phase 1

               base = ((r1    )&msku)+((r2>>17)&mskv);
               lutptr = lut + 3 * (base + ((r1>> 4)&msky));
               dst[0] = lutptr[2];
               dst[1] = lutptr[1];
               dst[2] = lutptr[0];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r2>>14)&msky));
               dst[4] = lutptr[2];
               dst[5] = lutptr[1];
               dst[6] = lutptr[0];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               base = ((r2<<10)&msku)+((r3>>7)&mskv);
               lutptr = lut + 3 * (base + ((r3>>24)&msky));
               dst[0] = lutptr[2];
               dst[1] = lutptr[1];
               dst[2] = lutptr[0];
               dst[3] = 0x7f;
               lutptr = lut + 3 * (base + ((r3>>4)&msky));
               dst[4] = lutptr[2];
               dst[5] = lutptr[1];
               dst[6] = lutptr[0];
               dst[7] = 0x7f;
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
#else
               r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+(src[3]);
               r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+(src[7]);
               r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+(src[11]);
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+(src[15]);
#endif
               src += 16;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeDVS_10BE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         if (rowphase==0) goto p0;
         if (rowphase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

               _asm {
               mov    ebx,src
               mov    esi,lut
               mov    edi,dst

               // base = ((r0>>10)&msku)+((r0<<3)&mskv);
               mov    eax,[ebx]
               bswap  eax
               shr    eax,10
               and    eax,msku

               mov    edx,[ebx]
               bswap  edx
               shl    edx,3
               and    edx,mskv
               add    edx,eax

               // r = 3 * (base + ((r0>>14)&msky));
               mov    ebx,[ebx]
               bswap  ebx
               shr    ebx,14
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi],eax

               // r = 3 * (base + ((r1>>24)&msky));
               mov    ebx,src
               mov    ebx,[ebx+4]
               bswap  ebx
               shr    ebx,24
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi+4],eax
               add     dst,8
               }

               if (dst == rowend) break;

            p1:; // phase 1

               _asm {
               mov    ebx,src
               mov    esi,lut
               mov    edi,dst

               // base = ((r1)&msku)+((r2>>17)&mskv);
               mov    eax,[ebx+4]
               bswap  eax
               and    eax,msku

               mov    edx,[ebx+8]
               bswap  edx
               shr    edx,17
               and    edx,mskv
               add    edx,eax

               // r = 3 * (base + ((r1>>4)&msky));
               mov    ebx,[ebx+4]
               bswap  ebx
               shr    ebx,4
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi],eax

               // r = 3 * (base + ((r2>>14)&msky));
               mov    ebx,src
               mov    ebx,[ebx+8]
               bswap  ebx
               shr    ebx,14
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi+4],eax
               add     dst,8
               }

               if (dst == rowend) break;

            p2:; // phase 2

               _asm {
               mov    ebx,src
               mov    esi,lut
               mov    edi,dst

               // base = ((r2<<10)&msku)+((r3>>7)&mskv);
               mov    eax,[ebx+8]
               bswap  eax
               shl    eax,10
               and    eax,msku

               mov    edx,[ebx+12]
               bswap  edx
               shr    edx,7
               and    edx,mskv
               add    edx,eax

               // r = 3 * (base + ((r3>>24)&msky));
               mov    ebx,[ebx+12]
               bswap  ebx
               shr    ebx,24
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi],eax

               // r = 3 * (base + ((r3>>4)&msky));
               mov    ebx,src
               mov    ebx,[ebx+12]
               bswap  ebx
               shr    ebx,4
               and    ebx,msky
               add    ebx,edx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    al,[esi+ebx]
               mov    ah,0x7f
               shl    eax,16
               mov    al,[esi+ebx+2]
               mov    ah,[esi+ebx+1]
               mov    [edi+4],eax
               add     dst,8
               }

               if (dst == rowend) break;

               src += 16;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#endif

///////////////////////////////////////////////////////////////
#if !MTI_ASM_X86_INTEL
static void dstripeUYV_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);


   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = (src[2]<<16)+(src[1]<<8)+src[0];
            r1 = (src[5]<<16)+(src[4]<<8)+src[3];
#else
            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+3));
#endif
            src += 6;

            lutptr = lut + 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            dst[0] = lutptr[2];
            dst[1] = lutptr[1];
            dst[2] = lutptr[0];
            dst[3] = 0x7f;

            lutptr = lut + 3 * (((r1<<14)&msku)+((r1>>9)&mskv)+((r1>>8)&msky));
            dst[4] = lutptr[2];
            dst[5] = lutptr[1];
            dst[6] = lutptr[0];
            dst[7] = 0x7f;
            dst += 8;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeUYV_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->rgbtbl;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // source
               mov    edi,dst                  // -> RGBA output rows

               mov    eax,src
               mov    eax,[eax]
               mov    ebx,eax
               mov    ecx,eax

               // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
               shl    eax,14
               and    eax,msku
               shr    ebx,9
               and    ebx,mskv
               shr    ecx,8
               and    ecx,msky
               add    ebx,eax
               add    ebx,ecx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    dl,[esi+ebx]
               mov    dh,0x7f
               ror    edx,16
               mov    dl,[esi+ebx+2]
               mov    dh,[esi+ebx+1]
               mov    [edi],edx

               add    src,3
               add    dst,4
               }
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;		// output the converted row

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         unsigned char *rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    edi,rgbrow               // -> RGBA output rows
            xor    ebx,ebx                  // clear upper portion

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    eax,[edx]                // index into row
            mov    edx,eax                  //
            sal    edx,1                    //
            add    eax,edx                  // * 3
            add    eax,src                  // beg of row

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            shl    eax,14
            and    eax,msku
            shr    ebx,9
            and    ebx,mskv
            shr    ecx,8
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 1 out of 8
            mov    [edi],edx

            mov    edx,pTbl
            mov    eax,[edx+4]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            shl    eax,14
            and    eax,msku
            shr    ebx,9
            and    ebx,mskv
            shr    ecx,8
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx

            mov    edx,pTbl
            mov    eax,[edx+8]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            shl    eax,14
            and    eax,msku
            shr    ebx,9
            and    ebx,mskv
            shr    ecx,8
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx

            mov    edx,pTbl
            mov    eax,[edx+12]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            shl    eax,14
            and    eax,msku
            shr    ebx,9
            and    ebx,mskv
            shr    ecx,8
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx

            mov    edx,pTbl
            mov    eax,[edx+16]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            shl    eax,14
            and    eax,msku
            shr    ebx,9
            and    ebx,mskv
            shr    ecx,8
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx

            mov    edx,pTbl
            mov    eax,[edx+20]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            shl    eax,14
            and    eax,msku
            shr    ebx,9
            and    ebx,mskv
            shr    ecx,8
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx

            mov    edx,pTbl
            mov    eax,[edx+24]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            shl    eax,14
            and    eax,msku
            shr    ebx,9
            and    ebx,mskv
            shr    ecx,8
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx

            mov    edx,pTbl
            mov    eax,[edx+28]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            shl    eax,14
            and    eax,msku
            shr    ebx,9
            and    ebx,mskv
            shr    ecx,8
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    edi,32
            mov    rgbrow,edi
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    edi,rgbrow
            xor    ebx,ebx

            mov    edx,pTbl
            mov    eax,[edx]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<14)&msku)+((r0>>9)&mskv)+((r0>>8)&msky));
            shl    eax,14
            and    eax,msku
            shr    ebx,9
            and    ebx,mskv
            shr    ecx,8
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // advance to next output row
			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeUYV_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->rgbtbl;
   unsigned char *lutptr;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src!=presrc) { // convert one row

         presrc = src;

         dst    = row;

          unsigned int r0, r1;

         while (dst < rowend) {

            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+4));
            src += 8;

            lutptr = lut + 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            dst[0] = lutptr[2];
            dst[1] = lutptr[1];
            dst[2] = lutptr[0];
            dst[3] = 0x7f;

            lutptr = lut + 3 * (((r1<<12)&msku)+((r1>>15)&mskv)+((r1>>12)&msky));
            dst[4] = lutptr[2];
            dst[5] = lutptr[1];
            dst[6] = lutptr[0];
            dst[7] = 0x7f;
            dst += 8;

         }
      }

		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeUYV_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->rgbtbl;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // used to access LUTs
               mov    ecx,0xff                 // used to mask 8 bits
               mov    edi,dst                  // -> RGBA output rows

               mov    eax,src
               mov    eax,[eax]
               mov    ebx,eax
               mov    ecx,eax

               // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
               shl    eax,12
               and    eax,msku
               shr    ebx,15
               and    ebx,mskv
               shr    ecx,12
               and    ecx,msky
               add    ebx,eax
               add    ebx,ecx

               mov    ecx,ebx
               sal    ebx,1
               add    ebx,ecx

               // lut[r].R, lut[r].G, lut[r].B, 0x7f
               mov    dl,[esi+ebx]
               mov    dh,0x7f
               ror    edx,16
               mov    dl,[esi+ebx+2]
               mov    dh,[esi+ebx+1]
               mov    [edi],edx

               add    src,4
               add    dst,4
               }
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    ecx,0xff                 // used to mask 8 bits
            mov    edi,rgbrow                  // -> RGBA output rows

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    eax,[edx]                // index into row
            sal    eax,2                    // integer per pel
            add    eax,src                  // beg of row

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            shl    eax,12
            and    eax,msku
            shr    ebx,15
            and    ebx,mskv
            shr    ecx,12
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx

            mov    edx,pTbl
            mov    eax,[edx+4]
            sal    eax,2
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            shl    eax,12
            and    eax,msku
            shr    ebx,15
            and    ebx,mskv
            shr    ecx,12
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx


            mov    edx,pTbl
            mov    eax,[edx+8]
            sal    eax,2
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            shl    eax,12
            and    eax,msku
            shr    ebx,15
            and    ebx,mskv
            shr    ecx,12
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx


            mov    edx,pTbl
            mov    eax,[edx+12]
            sal    eax,2
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            shl    eax,12
            and    eax,msku
            shr    ebx,15
            and    ebx,mskv
            shr    ecx,12
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx


            mov    edx,pTbl
            mov    eax,[edx+16]
            sal    eax,2
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            shl    eax,12
            and    eax,msku
            shr    ebx,15
            and    ebx,mskv
            shr    ecx,12
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx


            mov    edx,pTbl
            mov    eax,[edx+20]
            sal    eax,2
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            shl    eax,12
            and    eax,msku
            shr    ebx,15
            and    ebx,mskv
            shr    ecx,12
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx


            mov    edx,pTbl
            mov    eax,[edx+24]
            sal    eax,2
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            shl    eax,12
            and    eax,msku
            shr    ebx,15
            and    ebx,mskv
            shr    ecx,12
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx


            mov    edx,pTbl
            mov    eax,[edx+28]
            sal    eax,2
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            shl    eax,12
            and    eax,msku
            shr    ebx,15
            and    ebx,mskv
            shr    ecx,12
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    ecx,0xff

            mov    edx,pTbl
            mov    eax,[edx]
            sal    eax,2
            add    eax,src

            mov    eax,[eax]
            mov    ebx,eax
            mov    ecx,eax

            // r = 3 * (((r0<<12)&msku)+((r0>>15)&mskv)+((r0>>12)&msky));
            shl    eax,12
            and    eax,msku
            shr    ebx,15
            and    ebx,mskv
            shr    ecx,12
            and    ecx,msky
            add    ebx,eax
            add    ebx,ecx

            mov    ecx,ebx
            sal    ebx,1
            add    ebx,ecx

            // lut[r].R, lut[r].G, lut[r].B, 0x7f
            mov    dl,[esi+ebx]
            mov    dh,0x7f
            ror    edx,16
            mov    dl,[esi+ebx+2]
            mov    dh,[esi+ebx+1]

            and    edx,rgbMaskAlpha
            mov    edi,rgbrow
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // advance to next output row
			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeRGB_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8+256;
   unsigned char *linearB = vp->linear8+512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         while (dst < rowend) {

            dst[0] = linearR[src[0]]; // R
            dst[1] = linearG[src[1]]; // G
            dst[2] = linearB[src[2]]; // B
            dst[3] = 0x7f;            // A

            dst[4] = linearR[src[3]]; // R
            dst[5] = linearG[src[4]]; // G
            dst[6] = linearB[src[5]]; // B
            dst[7] = 0x7f;            // A

            src += 6;
            dst += 8;
         }
      }

		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeRGB_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // source
               mov    edi,dst                  // -> RGBA output rows
               xor    ebx,ebx                  // clear upper portion

               mov    eax,src
               mov    bl,[eax+2]
               mov    dl,BYTE PTR[esi+ebx+512]
               mov    dh,0x7f
               ror    edx,16

               mov    bl,[eax]
               mov    dl,BYTE PTR[esi+ebx]
               mov    bl,[eax+1]
               mov    dh,BYTE PTR[esi+ebx+256]

               mov    [edi],edx

               add    src,3
               add    dst,4
               }
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    edi,rgbrow               // -> RGBA output rows
            xor    ebx,ebx                  // clear upper portion

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    eax,[edx]                // index into row
            mov    edx,eax                  //
            sal    edx,1                    //
            add    eax,edx                  // * 3
            add    eax,src                  // beg of row

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx

            mov    edx,pTbl
            mov    eax,[edx+4]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx

            mov    edx,pTbl
            mov    eax,[edx+8]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx

            mov    edx,pTbl
            mov    eax,[edx+12]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx

            mov    edx,pTbl
            mov    eax,[edx+16]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx

            mov    edx,pTbl
            mov    eax,[edx+20]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx

            mov    edx,pTbl
            mov    eax,[edx+24]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx

            mov    edx,pTbl
            mov    eax,[edx+28]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    edi,rgbrow
            xor    ebx,ebx

            mov    edx,pTbl
            mov    eax,[edx]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // advance to next output row
			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeRGB_8_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8+256;
   unsigned char *antilogB = vp->antilog8+512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         while (dst < rowend) {

            dst[0] = antilogR[src[0]]; // R
            dst[1] = antilogG[src[1]]; // G
            dst[2] = antilogB[src[2]]; // B
            dst[3] = 0x7f;             // A

            dst[4] = antilogR[src[3]]; // R
            dst[5] = antilogG[src[4]]; // G
            dst[6] = antilogB[src[5]]; // B
            dst[7] = 0x7f;             // A

            src += 6;
            dst += 8;
         }
      }

		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeRGB_8_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->antilog8;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // source
               mov    edi,dst                  // -> RGBA output rows
               xor    ebx,ebx                  // clear upper portion

               mov    eax,src
               mov    bl,[eax+2]
               mov    dl,BYTE PTR[esi+ebx+512]
               mov    dh,0x7f
               ror    edx,16

               mov    bl,[eax]
               mov    dl,BYTE PTR[esi+ebx]
               mov    bl,[eax+1]
               mov    dh,BYTE PTR[esi+ebx+256]

               mov    [edi],edx

               add    src,3
               add    dst,4
               }
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    edi,rgbrow               // -> RGBA output rows
            xor    ebx,ebx                  // clear upper portion

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    eax,[edx]                // index into row
            mov    edx,eax                  //
            sal    edx,1                    //
            add    eax,edx                  // * 3
            add    eax,src                  // beg of row

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx

            mov    edx,pTbl
            mov    eax,[edx+4]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx

            mov    edx,pTbl
            mov    eax,[edx+8]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx

            mov    edx,pTbl
            mov    eax,[edx+12]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx

            mov    edx,pTbl
            mov    eax,[edx+16]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx

            mov    edx,pTbl
            mov    eax,[edx+20]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx

            mov    edx,pTbl
            mov    eax,[edx+24]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx

            mov    edx,pTbl
            mov    eax,[edx+28]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    edi,rgbrow
            xor    ebx,ebx

            mov    edx,pTbl
            mov    eax,[edx]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // advance to next output row
			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeBGR_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8+256;
   unsigned char *linearB = vp->linear8+512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         while (dst < rowend) {

            dst[0] = linearR[src[2]]; // R
            dst[1] = linearG[src[1]]; // G
            dst[2] = linearB[src[0]]; // B
            dst[3] = 0x7f;            // A

            dst[4] = linearR[src[5]]; // R
            dst[5] = linearG[src[4]]; // G
            dst[6] = linearB[src[3]]; // B
            dst[7] = 0x7f;            // A

            src += 6;
            dst += 8;
         }
      }

		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeBGR_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // source
               mov    edi,dst                  // -> RGBA output rows
               xor    ebx,ebx                  // clear upper portion

               mov    eax,src
               mov    bl,[eax]
               mov    dl,BYTE PTR[esi+ebx+512]
               mov    dh,0x7f
               ror    edx,16

               mov    bl,[eax+2]
               mov    dl,BYTE PTR[esi+ebx]
               mov    bl,[eax+1]
               mov    dh,BYTE PTR[esi+ebx+256]

               mov    [edi],edx

               add    src,3
               add    dst,4
               }
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    edi,rgbrow               // -> RGBA output rows
            xor    ebx,ebx                  // clear upper portion

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    eax,[edx]                // index into row
            mov    edx,eax                  //
            sal    edx,1                    //
            add    eax,edx                  // * 3
            add    eax,src                  // beg of row

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx

            mov    edx,pTbl
            mov    eax,[edx+4]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx

            mov    edx,pTbl
            mov    eax,[edx+8]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx

            mov    edx,pTbl
            mov    eax,[edx+12]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx

            mov    edx,pTbl
            mov    eax,[edx+16]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx

            mov    edx,pTbl
            mov    eax,[edx+20]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx

            mov    edx,pTbl
            mov    eax,[edx+24]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx

            mov    edx,pTbl
            mov    eax,[edx+28]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    edi,rgbrow
            xor    ebx,ebx

            mov    edx,pTbl
            mov    eax,[edx]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            add    eax,src

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+2]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+1]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // advance to next output row
			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeRGB_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+4));
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
#endif
            src += 8;

            dst[0] = linearR[(r0>>24)&0xff]; // R
            dst[1] = linearG[(r0>>14)&0xff]; // G
            dst[2] = linearB[(r0>> 4)&0xff]; // B
            dst[3] = 0x7f;                   // A

            dst[4] = linearR[(r1>>24)&0xff]; // R
            dst[5] = linearG[(r1>>14)&0xff]; // G
            dst[6] = linearB[(r1>> 4)&0xff]; // B
            dst[7] = 0x7f;                   // A
            dst += 8;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeRGB_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // used to access LUTs
               mov    ecx,0xff                 // used to mask 8 bits
               mov    edi,dst                  // -> RGBA output rows

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,ecx                  // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,ecx                  // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    [edi],edx

               add    src,4
               add    dst,4
               }
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    ecx,0xff                 // used to mask 8 bits
            mov    edi,rgbrow               // -> RGBA output rows

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    edx,[edx]                // index into row
            sal    edx,2                    // integer per pel
            add    edx,src                  // beg of row
            mov    eax,[edx]                // get pel
            bswap  eax                      // DPX RGB

            mov    ebx,eax
            sar    ebx,4                    // B = bits 4-11
            and    ebx,ecx                  // mask 8 bits
            mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
            mov    dh,0x7f                  // A component
            ror    edx,16                   // AB in hi bytes

            mov    ebx,eax
            shr    ebx,24                   // R = bits 24-31
            mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

            mov    ebx,eax
            sar    ebx,14                   // G = bits 14-21
            and    ebx,ecx                  // mask 8 bits
            mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx


            mov    edx,pTbl
            mov    edx,[edx+4]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx


            mov    edx,pTbl
            mov    edx,[edx+8]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx


            mov    edx,pTbl
            mov    edx,[edx+12]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx


            mov    edx,pTbl
            mov    edx,[edx+16]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx


            mov    edx,pTbl
            mov    edx,[edx+20]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx


            mov    edx,pTbl
            mov    edx,[edx+24]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx


            mov    edx,pTbl
            mov    edx,[edx+28]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    ecx,0xff

            mov    edx,pTbl
            mov    edx,[edx]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            mov    edi,rgbrow
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // advance to next output row
			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeRGB_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

#if ENDIAN==MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src  ));
            r1 = *((unsigned int *)(src+4));
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
#endif
            src += 8;

            dst[0] = antilogR[(r0>>24)&0xff]; // R
            dst[1] = antilogG[(r0>>14)&0xff]; // G
            dst[2] = antilogB[(r0>> 4)&0xff]; // B
            dst[3] = 0x7f;                    // A

            dst[4] = antilogR[(r1>>24)&0xff]; // R
            dst[5] = antilogG[(r1>>14)&0xff]; // G
            dst[6] = antilogB[(r1>> 4)&0xff]; // B
            dst[7] = 0x7f;                    // A
            dst += 8;
         }
      }

		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeRGB_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->antilog8;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // used to access LUTs
               mov    ecx,0xff                 // used to mask 8 bits
               mov    edi,dst                  // -> RGBA output rows

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,ecx                  // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,ecx                  // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    [edi],edx

               add    src,4
               add    dst,4
               }
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    ecx,0xff                 // used to mask 8 bits
            mov    edi,rgbrow               // -> RGBA output rows

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    edx,[edx]                // index into row
            sal    edx,2                    // integer per pel
            add    edx,src                  // beg of row
            mov    eax,[edx]                // get pel
            bswap  eax                      // DPX RGB

            mov    ebx,eax
            sar    ebx,4                    // B = bits 4-11
            and    ebx,ecx                  // mask 8 bits
            mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
            mov    dh,0x7f                  // A component
            ror    edx,16                   // AB in hi bytes

            mov    ebx,eax
            shr    ebx,24                   // R = bits 24-31
            mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

            mov    ebx,eax
            sar    ebx,14                   // G = bits 14-21
            and    ebx,ecx                  // mask 8 bits
            mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx


            mov    edx,pTbl
            mov    edx,[edx+4]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx


            mov    edx,pTbl
            mov    edx,[edx+8]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx


            mov    edx,pTbl
            mov    edx,[edx+12]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx


            mov    edx,pTbl
            mov    edx,[edx+16]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx


            mov    edx,pTbl
            mov    edx,[edx+20]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx


            mov    edx,pTbl
            mov    edx,[edx+24]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx


            mov    edx,pTbl
            mov    edx,[edx+28]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    ecx,0xff

            mov    edx,pTbl
            mov    edx,[edx]
            sal    edx,2
            add    edx,src
            mov    eax,[edx]
            bswap  eax

            mov    ebx,eax
            sar    ebx,4
            and    ebx,ecx
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    ebx,eax
            shr    ebx,24
            mov    dl,BYTE PTR[esi+ebx]

            mov    ebx,eax
            sar    ebx,14
            and    ebx,ecx
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            mov    edi,rgbrow
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // advance to next output row
			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif

///////////////////////////////////////////////////////////////

static void dstripeBGR_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            src += 8;

            dst[0] = linearR[(r0>> 4)&0xff]; // R
            dst[1] = linearG[(r0>>14)&0xff]; // G
            dst[2] = linearB[(r0>>24)&0xff]; // B
            dst[3] = 0x7f;                   // A

            dst[4] = linearR[(r1>> 4)&0xff]; // R
            dst[5] = linearG[(r1>>14)&0xff]; // G
            dst[6] = linearB[(r1>>24)&0xff]; // B
            dst[7] = 0x7f;                   // A
            dst += 8;
         }
      }

		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void dstripeBGR_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

          unsigned int r0, r1;

         while (dst < rowend) {

            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            src += 8;

            dst[0] = antilogR[(r0>> 4)&0xff]; // R
            dst[1] = antilogG[(r0>>14)&0xff]; // G
            dst[2] = antilogB[(r0>>24)&0xff]; // B
            dst[3] = 0x7f;                    // A

            dst[4] = antilogR[(r1>> 4)&0xff]; // R
            dst[5] = antilogG[(r1>>14)&0xff]; // G
            dst[6] = antilogB[(r1>>24)&0xff]; // B
            dst[7] = 0x7f;                    // A
            dst += 8;
         }
      }

		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void dstripeRGB_10_A1(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert *) v;

	// R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
	unsigned char *lut = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;

   // the Alpha map's RIGHT AFTER the DPX image; set the ptr to it.
   unsigned char *alphaMap = curSrcBuf[0] + (vp->curSrcWdth * vp->curSrcHght*4);
   unsigned char *alphasrc, alphamsk;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in bytes to pel

         src = (curSrcBuf[ystrTbl[i].fld] + rowOffset); // -> 1st pel in row

         rowOffset >>= 2;
         alphasrc = alphaMap + (rowOffset >> 3);      // -> 1st byte in Alpha map
         alphamsk = ((unsigned char)1)<<(rowOffset&0x7);// masks 1st bit in Alpha

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            alphadst = alpharow;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               // alphamsk set => color from alphaColor
               if ((alphasrc[0]&alphamsk)!=0) {
                  *((MTI_UINT32 *)dst) = 0x7f000000;
                  *((MTI_UINT32 *)alphadst) = alphaColor;
               }
               else { // color from src
                  dst[0] = lut[src[0]];
                  dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
                  dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];
                  *((MTI_UINT32 *)alphadst) = 0;
               }

               // completed one pixel
               src += 4;
               alphamsk <<= 1;
               if (alphamsk == 0) {
                  alphasrc++;
                  alphamsk = 1;
               }
               dst += 4;
               alphadst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    edx,alphasrc
               mov    cl, [edx]

               test   alphamsk,cl              // alpha bit is set?
               je     $L1                      // == bit 0 of BLUE component

               mov    edi,dst                  // -> into RGBrow[iJob]
               mov    [edi],0x7f000000         // this contributes A = 0x7F

               mov    edi,alphadst             // -> into ALPHArow[iJob]
               mov    edx,alphaColor           // this has the A zeroed so
               mov    [edi],edx                // that on merge sum is 0x7F

               jmp    SHORT $L2

$L1:           mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,0xff                 // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,0xff                 // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    edi,dst
               mov    [edi],edx
               mov    edi,alphadst
               mov    [edi],0

$L2:           add    src,4                    // next DPX pel

               sal    alphamsk,1               // next Alpha bit
               jnc    $L3
               inc    alphasrc
               mov    alphamsk,1

$L3:           add    dst,4                    // next pel in RGBrow[iJob]
               add    alphadst,4               // next pel in ALPHArow[iJob]
               }
#endif
            }
         }

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe

   }
   else {

		// convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

			if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               dst[0] = lut[src[0]];
               dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
               dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];
               dst[3] = 0x7f;

               // completed one pixel
               src += 4;
               dst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs
               mov    ecx,0xff                 // used to mask 8 bits
               mov    edi,dst                  // -> RGBA output rows

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,ecx                  // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
					ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,ecx                  // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    [edi],edx

               add    src,4
               add    dst,4
               }
#endif
            }
			 }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

static void dstripeRGB_10_LOG_A1(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;

   // the Alpha map's RIGHT AFTER the DPX image; set the ptr to it.
   unsigned char *alphaMap = curSrcBuf[0] + (vp->curSrcWdth * vp->curSrcHght*4);
   unsigned char *alphasrc, alphamsk;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in bytes to pel

         src = (curSrcBuf[ystrTbl[i].fld] + rowOffset); // -> 1st pel in row

         rowOffset >>= 2;
         alphasrc = alphaMap + (rowOffset >> 3);      // -> 1st byte in Alpha map
         alphamsk = ((unsigned char)1)<<(rowOffset&0x7);// masks 1st bit in Alpha

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            alphadst = alpharow;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               // alphamsk set => color from alphaColor
               if ((alphasrc[0]&alphamsk)!=0) {
                  *((MTI_UINT32 *)dst) = 0x7f000000;
                  *((MTI_UINT32 *)alphadst) = alphaColor;
               }
               else { // color from src
                  dst[0] = lut[src[0]];
                  dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
                  dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];
                  *((MTI_UINT32 *)alphadst) = 0;
               }

               // completed one pixel
               src += 4;
               alphamsk <<= 1;
               if (alphamsk == 0) {
                  alphasrc++;
                  alphamsk = 1;
               }
               dst += 4;
               alphadst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    edx,alphasrc
               mov    cl, [edx]

               test   alphamsk,cl              // alpha bit is set?
               je     $L1                      // == bit 0 of BLUE component

               mov    edi,dst                  // -> into RGBrow[iJob]
               mov    [edi],0x7f000000         // this contributes A = 0x7F

               mov    edi,alphadst             // -> into ALPHArow[iJob]
               mov    edx,alphaColor           // this has the A zeroed so
               mov    [edi],edx                // that on merge sum is 0x7F

               jmp    SHORT $L2

$L1:           mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,0xff                 // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,0xff                 // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    edi,dst
               mov    [edi],edx
               mov    edi,alphadst
               mov    [edi],0

$L2:           add    src,4                    // next DPX pel

               sal    alphamsk,1               // next Alpha bit
               jnc    $L3
               inc    alphasrc
               mov    alphamsk,1

$L3:           add    dst,4                    // next pel in RGBrow[iJob]
               add    alphadst,4               // next pel in ALPHArow[iJob]
               }
#endif
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               dst[0] = lut[src[0]];
               dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
               dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];
               dst[3] = 0x7f;

               // completed one pixel
               src += 4;
               dst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs
               mov    ecx,0xff                 // used to mask 8 bits
               mov    edi,dst                  // -> RGBA output rows

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,ecx                  // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,ecx                  // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    [edi],edx

               add    src,4
               add    dst,4
               }
#endif
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

///////////////////////////////////////////////////////////////


// NOTE: THIS IS IDENTICAL TO dstripeRGB_10_LOG_A2
// except for the line:
// unsigned char *lut = vp->linear8;
static void dstripeRGB_10_A2(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert*) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgb = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv = 4 * vp->curDstPitch;
   unsigned char *row = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor & 0xffffff)
   {
      // Alpha display is turned on.
      // Convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output
      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      const int bitsPerAlpha = 2;
      const unsigned char twoBitAlphaMask = (1 << bitsPerAlpha) - 1;

      for (int i = begRow; i < endRow; i++)
      {
         int rowOffset = ystrTbl[i].offs; // offset in bytes to pel
         src = (curSrcBuf[ystrTbl[i].fld] + rowOffset); // -> 1st pel in row

         if (src != presrc)
         {
            // convert one full row of source
            presrc = src;
            dst = row;
            alphadst = alpharow;

            while (dst < rowend)
            {
               if ((src[3] & twoBitAlphaMask) != 0)
               {
                  *((MTI_UINT32 *)dst) = 0x7f000000;
                  *((MTI_UINT32 *)alphadst) = alphaColor;
               }
               else
               {
                  // color from src
                  dst[0] = lut[src[0]];
                  dst[1] = lut[256 + (MTI_UINT8)((src[1] << 2) + (src[2] >> 6))];
                  dst[2] = lut[512 + (MTI_UINT8)((src[2] << 4) + (src[3] >> 4))];
                  *((MTI_UINT32*)alphadst) = 0;
               }

               // completed one pixel
               src += 4;
               dst += 4;
               alphadst += 4;
            }
         }

         // output the converted row
         if (vp->_isWiperComparand)
         {
            rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
         }
         else
         {
            rowRGB(rgb, row, vp);
         }

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

         rgb += rgbadv;

      } // for each row of stripe
   }
   else
   {
      // Convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      for (int i = begRow; i < endRow; i++)
      {
         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);
         if (src != presrc)
         {
            // convert one full row of source
            presrc = src;
            dst = row;

            while (dst < rowend)
            {
               dst[0] = lut[src[0]];
               dst[1] = lut[256 + (MTI_UINT8)((src[1] << 2) + (src[2] >> 6))];
               dst[2] = lut[512 + (MTI_UINT8)((src[2] << 4) + (src[3] >> 4))];
               dst[3] = 0x7f;

               // completed one pixel
               src += 4;
               dst += 4;
            }
         }

         if (vp->_isWiperComparand)
         {
            rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
         }
         else
         {
            rowRGB(rgb, row, vp);
         }

         rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

// NOTE: THIS IS IDENTICAL TO dstripeRGB_10_A2
// except for the line:
// unsigned char *lut = vp->antilog8;
static void dstripeRGB_10_LOG_A2(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgb = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv = 4 * vp->curDstPitch;
   unsigned char *row = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor & 0xffffff)
   {
      // Alpha display is turned on.
      // Convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output
      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      const int bitsPerAlpha = 2;
      const unsigned char twoBitAlphaMask = (1 << bitsPerAlpha) - 1;

      for (int i = begRow; i < endRow; i++)
      {
         int rowOffset = ystrTbl[i].offs; // offset in bytes to pel
         src = (curSrcBuf[ystrTbl[i].fld] + rowOffset); // -> 1st pel in row

         if (src != presrc)
         {
            // convert one full row of source
            presrc = src;
            dst = row;
            alphadst = alpharow;

            while (dst < rowend)
            {
               if ((src[3] & twoBitAlphaMask) != 0)
               {
                  *((MTI_UINT32 *)dst) = 0x7f000000;
                  *((MTI_UINT32 *)alphadst) = alphaColor;
               }
               else
               {
                  // color from src
                  dst[0] = lut[src[0]];
                  dst[1] = lut[256 + (MTI_UINT8)((src[1] << 2) + (src[2] >> 6))];
                  dst[2] = lut[512 + (MTI_UINT8)((src[2] << 4) + (src[3] >> 4))];
                  *((MTI_UINT32*)alphadst) = 0;
               }

               // completed one pixel
               src += 4;
               dst += 4;
               alphadst += 4;
            }
         }

         // output the converted row
         if (vp->_isWiperComparand)
         {
            rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
         }
         else
         {
            rowRGB(rgb, row, vp);
         }

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

         rgb += rgbadv;

      } // for each row of stripe
   }
   else
   {
      // Convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      for (int i = begRow; i < endRow; i++)
      {
         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);
         if (src != presrc)
         {
            // convert one full row of source
            presrc = src;
            dst = row;

            while (dst < rowend)
            {
               dst[0] = lut[src[0]];
               dst[1] = lut[256 + (MTI_UINT8)((src[1] << 2) + (src[2] >> 6))];
               dst[2] = lut[512 + (MTI_UINT8)((src[2] << 4) + (src[3] >> 4))];
               dst[3] = 0x7f;

               // completed one pixel
               src += 4;
               dst += 4;
            }
         }

         if (vp->_isWiperComparand)
         {
            rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
         }
         else
         {
            rowRGB(rgb, row, vp);
         }

         rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

///////////////////////////////////////////////////////////////

static void dstripeRGB_10_A8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;

   // the Alpha map's RIGHT AFTER the DPX image; set the ptr to it.
   unsigned char *alphaMap = curSrcBuf[0] + (vp->curSrcWdth * vp->curSrcHght*4);
   unsigned char *alphasrc;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in bytes to pel

         src = (curSrcBuf[ystrTbl[i].fld] + rowOffset); // -> 1st pel in row

         rowOffset >>= 2;
         alphasrc = alphaMap + rowOffset;      // -> 1st byte in Alpha map

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            alphadst = alpharow;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               // alphamsk set => color from alphaColor
               if (alphasrc[0]!=0) {
                  *((MTI_UINT32 *)dst) = 0x7f;
                  *((MTI_UINT32 *)alphadst) = alphaColor;
               }
               else { // color from src
                  dst[0] = lut[src[0]];
                  dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
                  dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];
                  *((MTI_UINT32 *)alphadst) = 0;
               }

               // completed one pixel
               src += 4;
               alphasrc++;
               dst += 4;
               alphadst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    edx,alphasrc
               mov    cl, [edx]

               or     cl,cl                    // alpha bits set?
               je     $L1                      // == bit 0 of BLUE component

               mov    edi,dst                  // -> into RGBrow[iJob]
               mov    [edi],0x7f000000         // this contributes A = 0x7F

               mov    edi,alphadst             // -> into ALPHArow[iJob]
               mov    edx,alphaColor           // this has the A zeroed so
               mov    [edi],edx                // that on merge sum is 0x7F

               jmp    SHORT $L2

$L1:           mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,0xff                 // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,0xff                 // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    edi,dst
               mov    [edi],edx
               mov    edi,alphadst
               mov    [edi],0

$L2:           add    src,4                    // next DPX pel

               inc    alphasrc                 // next Alpha byte

               add    dst,4                    // next pel in RGBrow[iJob]
               add    alphadst,4               // next pel in ALPHArow[iJob]
               }
#endif
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			// ...then ADD the alpha onto it
			rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe

   }
   else {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               dst[0] = lut[src[0]];
               dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
               dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];

               // completed one pixel
               src += 4;
               dst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs
               mov    ecx,0xff                 // used to mask 8 bits
               mov    edi,dst                  // -> RGBA output rows

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,ecx                  // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,ecx                  // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    [edi],edx

               add    src,4
               add    dst,4
               }
#endif
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

static void dstripeRGB_10_LOG_A8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;

   // the Alpha map's RIGHT AFTER the DPX image; set the ptr to it.
   unsigned char *alphaMap = curSrcBuf[0] + (vp->curSrcWdth * vp->curSrcHght*4);
   unsigned char *alphasrc;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in bytes to pel

         src = (curSrcBuf[ystrTbl[i].fld] + rowOffset); // -> 1st pel in row

         rowOffset >>= 2;
         alphasrc = alphaMap + rowOffset;      // -> 1st byte in Alpha map

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            alphadst = alpharow;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               // alphamsk set => color from alphaColor
               if (alphasrc[0]!=0) {
                  *((MTI_UINT32 *)dst) = 0x7f;
                  *((MTI_UINT32 *)alphadst) = alphaColor;
               }
               else { // color from src
                  dst[0] = lut[src[0]];
                  dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
                  dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];
                  *((MTI_UINT32 *)alphadst) = 0;
               }

               // completed one pixel
               src += 4;
               alphasrc++;
               dst += 4;
               alphadst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    edx,alphasrc
               mov    cl, [edx]

               or     cl,cl                    // alpha bit is set?
               je     $L1                      // == bit 0 of BLUE component

               mov    edi,dst                  // -> into RGBrow[iJob]
               mov    [edi],0x7f000000         // this contributes A = 0x7F

               mov    edi,alphadst             // -> into ALPHArow[iJob]
               mov    edx,alphaColor           // this has the A zeroed so
               mov    [edi],edx                // that on merge sum is 0x7F

               jmp    SHORT $L2

$L1:           mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,0xff                 // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,0xff                 // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    edi,dst
               mov    [edi],edx
               mov    edi,alphadst
               mov    [edi],0

$L2:           add    src,4                    // next DPX pel

               inc    alphasrc                 // next Alpha byte

               add    dst,4                    // next pel in RGBrow[iJob]
               add    alphadst,4               // next pel in ALPHArow[iJob]
               }
#endif
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               dst[0] = lut[src[0]];
               dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
               dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];

               // completed one pixel
               src += 4;
               dst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs
               mov    ecx,0xff                 // used to mask 8 bits
               mov    edi,dst                  // -> RGBA output rows

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,ecx                  // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,ecx                  // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    [edi],edx

               add    src,4
               add    dst,4
               }
#endif
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

///////////////////////////////////////////////////////////////

static void dstripeRGB_10_A16(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;

   // the Alpha map's RIGHT AFTER the DPX image; set the ptr to it.
   MTI_UINT16 *alphaMap = (MTI_UINT16 *)(curSrcBuf[0] + (vp->curSrcWdth * vp->curSrcHght*4));
   MTI_UINT16 *alphasrc;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
  unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in bytes to pel

         src = (curSrcBuf[ystrTbl[i].fld] + rowOffset); // -> 1st pel in row

         rowOffset >>= 2;
         alphasrc = alphaMap + rowOffset;      // -> 1st word in Alpha map

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            alphadst = alpharow;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               // alphamsk set => color from alphaColor
               if (alphasrc[0]!=0) {
                  *((MTI_UINT32 *)dst) = 0x7f;
                  *((MTI_UINT32 *)alphadst) = alphaColor;
               }
               else { // color from src
                  dst[0] = lut[src[0]];
                  dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
                  dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];
                  *((MTI_UINT32 *)alphadst) = 0;
               }

               // completed one pixel
               src += 4;
               alphasrc++;
               dst += 4;
               alphadst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    edx,alphasrc
               mov    cx,[edx]

               or     cx,cx                    // alpha bits set?
               je     $L1                      // == bit 0 of BLUE component

               mov    edi,dst                  // -> into RGBrow[iJob]
               mov    [edi],0x7f000000         // this contributes A = 0x7F

               mov    edi,alphadst             // -> into ALPHArow[iJob]
               mov    edx,alphaColor           // this has the A zeroed so
               mov    [edi],edx                // that on merge sum is 0x7F

               jmp    SHORT $L2

$L1:           mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,0xff                 // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,0xff                 // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    edi,dst
               mov    [edi],edx
               mov    edi,alphadst
               mov    [edi],0

$L2:           add    src,4                    // next DPX pel

               add    alphasrc,2               // next Alpha word

               add    dst,4                    // next pel in RGBrow[iJob]
               add    alphadst,4               // next pel in ALPHArow[iJob]
               }
#endif
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe

   }
   else {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               dst[0] = lut[src[0]];
               dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
               dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];

               // completed one pixel
               src += 4;
               dst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs
               mov    ecx,0xff                 // used to mask 8 bits
               mov    edi,dst                  // -> RGBA output rows

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,ecx                  // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,ecx                  // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    [edi],edx

               add    src,4
               add    dst,4
               }
#endif
            }
         }

			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

static void dstripeRGB_10_LOG_A16(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;

   // the Alpha map's RIGHT AFTER the DPX image; set the ptr to it.
   MTI_UINT16 *alphaMap = (MTI_UINT16 *)(curSrcBuf[0] + (vp->curSrcWdth * vp->curSrcHght*4));
   MTI_UINT16 *alphasrc;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in bytes to pel

         src = (curSrcBuf[ystrTbl[i].fld] + rowOffset); // -> 1st pel in row

         rowOffset >>= 2;
         alphasrc = alphaMap + rowOffset;      // -> 1st word in Alpha map

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            alphadst = alpharow;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               // alphamsk set => color from alphaColor
               if (alphasrc[0]!=0) {
                  *((MTI_UINT32 *)dst) = 0x7f;
                  *((MTI_UINT32 *)alphadst) = alphaColor;
               }
               else { // color from src
                  dst[0] = lut[src[0]];
                  dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
                  dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];
                  *((MTI_UINT32 *)alphadst) = 0;
               }

               // completed one pixel
               src += 4;
               alphasrc++;
               dst += 4;
               alphadst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    edx,alphasrc
               mov    cx, [edx]

               or     cx,cx                    // alpha bit is set?
               je     $L1                      // == bit 0 of BLUE component

               mov    edi,dst                  // -> into RGBrow[iJob]
               mov    [edi],0x7f000000         // this contributes A = 0x7F

               mov    edi,alphadst             // -> into ALPHArow[iJob]
               mov    edx,alphaColor           // this has the A zeroed so
               mov    [edi],edx                // that on merge sum is 0x7F

               jmp    SHORT $L2

$L1:           mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,0xff                 // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,0xff                 // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    edi,dst
               mov    [edi],edx
               mov    edi,alphadst
               mov    [edi],0

$L2:           add    src,4                    // next DPX pel

               add    alphasrc,2               // next Alpha word

               add    dst,4                    // next pel in RGBrow[iJob]
               add    alphadst,4               // next pel in ALPHArow[iJob]
               }
#endif
            }
         }

         // output the converted row...
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src!=presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

#if !MTI_ASM_X86_INTEL
               dst[0] = lut[src[0]];
               dst[1] = lut[256 + (MTI_UINT8)((src[1]<<2)+(src[2]>>6))];
               dst[2] = lut[512 + (MTI_UINT8)((src[2]<<4)+(src[3]>>4))];

               // completed one pixel
               src += 4;
               dst += 4;
#else
               _asm {
               mov    esi,lut                  // used to access LUTs
               mov    ecx,0xff                 // used to mask 8 bits
               mov    edi,dst                  // -> RGBA output rows

               mov    edx,src
               mov    eax,[edx]
               bswap  eax

               mov    ebx,eax
               sar    ebx,4                    // B = bits 4-11
               and    ebx,ecx                  // mask 8 bits
               mov    dl,BYTE PTR[esi+ebx+512] // LUT gives B
               mov    dh,0x7f                  // A component
               ror    edx,16                   // AB in hi bytes

               mov    ebx,eax
               shr    ebx,24                   // R = bits 24-31
               mov    dl,BYTE PTR[esi+ebx]     // LUT gives R

               mov    ebx,eax
               sar    ebx,14                   // G = bits 14-21
               and    ebx,ecx                  // mask 8 bits
               mov    dh,BYTE PTR[esi+ebx+256] // LUT gives G

               mov    [edi],edx

               add    src,4
               add    dst,4
               }
#endif
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

static void dstripeRGB_12(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (rowphase==0) goto p0;
         if (rowphase==2) goto p2;
         if (rowphase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[0] = linearR[(r0>> 4)&0xff]; // R
               dst[1] = linearG[(r0>>16)&0xff]; // G
               dst[2] = linearB[((r0>>28)+(r1<<4))&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p1:;

               dst[4] = linearR[(r1>> 8)&0xff]; // R
               dst[5] = linearG[(r1>>20)&0xff]; // G
               dst[6] = linearB[(r2    )&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[0] = linearR[(r2>>12)&0xff]; // R
               dst[1] = linearG[(r2>>24)&0xff]; // G
               dst[2] = linearB[(r3>> 4)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p3:;

               dst[4] = linearR[(r3>>16)&0xff]; // R
               dst[5] = linearG[((r3>>28)+(r4<<4))&0xff]; // G
               dst[6] = linearB[(r4>> 8)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[0] = linearR[(r4>>20)&0xff]; // R
               dst[1] = linearG[(r5    )&0xff]; // G
               dst[2] = linearB[(r5>>12)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p5:;

               dst[4] = linearR[(r5>>24)&0xff]; // R
               dst[5] = linearG[(r6>> 4)&0xff]; // G
               dst[6] = linearB[(r6>>16)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[0] = linearR[((r6>>28)+(r7<<4))&0xff]; // R
               dst[1] = linearG[(r7>> 8)&0xff]; // G
               dst[2] = linearB[(r7>>20)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p7:;

               dst[4] = linearR[(r8    )&0xff]; // R
               dst[5] = linearG[(r8>>12)&0xff]; // G
               dst[6] = linearB[(r8>>24)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeRGB_12_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8+256;
   unsigned char *antilogB = vp->antilog8+512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (rowphase==0) goto p0;
         if (rowphase==2) goto p2;
         if (rowphase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[0] = antilogR[(r0>> 4)&0xff]; // R
               dst[1] = antilogG[(r0>>16)&0xff]; // G
               dst[2] = antilogB[((r0>>28)+(r1<<4))&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p1:;

               dst[4] = antilogR[(r1>> 8)&0xff]; // R
               dst[5] = antilogG[(r1>>20)&0xff]; // G
               dst[6] = antilogB[(r2    )&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[0] = antilogR[(r2>>12)&0xff]; // R
               dst[1] = antilogG[(r2>>24)&0xff]; // G
               dst[2] = antilogB[(r3>> 4)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p3:;

               dst[4] = antilogR[(r3>>16)&0xff]; // R
               dst[5] = antilogG[((r3>>28)+(r4<<4))&0xff]; // G
               dst[6] = antilogB[(r4>> 8)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[0] = antilogR[(r4>>20)&0xff]; // R
               dst[1] = antilogG[(r5    )&0xff]; // G
               dst[2] = antilogB[(r5>>12)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p5:;

               dst[4] = antilogR[(r5>>24)&0xff]; // R
               dst[5] = antilogG[(r6>> 4)&0xff]; // G
               dst[6] = antilogB[(r6>>16)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[0] = antilogR[((r6>>28)+(r7<<4))&0xff]; // R
               dst[1] = antilogG[(r7>> 8)&0xff]; // G
               dst[2] = antilogB[(r7>>20)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p7:;

               dst[4] = antilogR[(r8    )&0xff]; // R
               dst[5] = antilogG[(r8>>12)&0xff]; // G
               dst[6] = antilogB[(r8>>24)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeRGB_12_LR(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (rowphase==0) goto p0;
         if (rowphase==2) goto p2;
         if (rowphase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[0] = linearR[(r0>>24)&0xff]; // R
               dst[1] = linearG[(r0>>12)&0xff]; // G
               dst[2] = linearB[(r0    )&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p1:;

               dst[4] = linearR[(r1>>20)&0xff]; // R
               dst[5] = linearG[(r1>> 8)&0xff]; // G
               dst[6] = linearB[((r1<<4)+(r2>>28))&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[0] = linearR[(r2>>16)&0xff]; // R
               dst[1] = linearG[(r2>> 4)&0xff]; // G
               dst[2] = linearB[(r3>>24)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p3:;

               dst[4] = linearR[(r3>>12)&0xff]; // R
               dst[5] = linearG[(r3    )&0xff]; // G
               dst[6] = linearB[(r4>>20)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[0] = linearR[(r4>> 8)&0xff]; // R
               dst[1] = linearG[((r4<<4)+(r5>>28))&0xff]; // G
               dst[2] = linearB[(r5>>16)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p5:;

               dst[4] = linearR[(r5>> 4)&0xff]; // R
               dst[5] = linearG[(r6>>24)&0xff]; // G
               dst[6] = linearB[(r6>>12)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[0] = linearR[(r6    )&0xff]; // R
               dst[1] = linearG[(r7>>20)&0xff]; // G
               dst[2] = linearB[(r7>> 8)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p7:;

               dst[4] = linearR[((r7<<4)+(r8>>28))&0xff]; // R
               dst[5] = linearG[(r8>>16)&0xff]; // G
               dst[6] = linearB[(r8>> 4)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeRGB_12_LR_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8+256;
   unsigned char *antilogB = vp->antilog8+512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (rowphase==0) goto p0;
         if (rowphase==2) goto p2;
         if (rowphase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[0] = antilogR[(r0>>24)&0xff]; // R
               dst[1] = antilogG[(r0>>12)&0xff]; // G
               dst[2] = antilogB[(r0    )&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p1:;

               dst[4] = antilogR[(r1>>20)&0xff]; // R
               dst[5] = antilogG[(r1>> 8)&0xff]; // G
               dst[6] = antilogB[((r1<<4)+(r2>>28))&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[0] = antilogR[(r2>>16)&0xff]; // R
               dst[1] = antilogG[(r2>> 4)&0xff]; // G
               dst[2] = antilogB[(r3>>24)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p3:;

               dst[4] = antilogR[(r3>>12)&0xff]; // R
               dst[5] = antilogG[(r3    )&0xff]; // G
               dst[6] = antilogB[(r4>>20)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[0] = antilogR[(r4>> 8)&0xff]; // R
               dst[1] = antilogG[((r4<<4)+(r5>>28))&0xff]; // G
               dst[2] = antilogB[(r5>>16)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p5:;

               dst[4] = antilogR[(r5>> 4)&0xff]; // R
               dst[5] = antilogG[(r6>>24)&0xff]; // G
               dst[6] = antilogB[(r6>>12)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[0] = antilogR[(r6    )&0xff]; // R
               dst[1] = antilogG[(r7>>20)&0xff]; // G
               dst[2] = antilogB[(r7>> 8)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p7:;

               dst[4] = antilogR[((r7<<4)+(r8>>28))&0xff]; // R
               dst[5] = antilogG[(r8>>16)&0xff]; // G
               dst[6] = antilogB[(r8>> 4)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
			}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeRGB_12P(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==2) goto p2;
         if (vp->ystrTbl[i].phase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[0] = linearR[(r0>> 4)&0xff]; // R
               dst[1] = linearG[(r0>>16)&0xff]; // G
               dst[2] = linearB[((r0>>28)+(r1<<4))&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p1:;

               dst[4] = linearR[(r1>> 8)&0xff]; // R
               dst[5] = linearG[(r1>>20)&0xff]; // G
               dst[6] = linearB[(r2    )&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[0] = linearR[(r2>>12)&0xff]; // R
               dst[1] = linearG[(r2>>24)&0xff]; // G
               dst[2] = linearB[(r3>> 4)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p3:;

               dst[4] = linearR[(r3>>16)&0xff]; // R
               dst[5] = linearG[((r3>>28)+(r4<<4))&0xff]; // G
               dst[6] = linearB[(r4>> 8)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[0] = linearR[(r4>>20)&0xff]; // R
               dst[1] = linearG[(r5    )&0xff]; // G
               dst[2] = linearB[(r5>>12)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p5:;

               dst[4] = linearR[(r5>>24)&0xff]; // R
               dst[5] = linearG[(r6>> 4)&0xff]; // G
               dst[6] = linearB[(r6>>16)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[0] = linearR[((r6>>28)+(r7<<4))&0xff]; // R
               dst[1] = linearG[(r7>> 8)&0xff]; // G
               dst[2] = linearB[(r7>>20)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p7:;

               dst[4] = linearR[(r8    )&0xff]; // R
               dst[5] = linearG[(r8>>12)&0xff]; // G
               dst[6] = linearB[(r8>>24)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeRGB_12P_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2, r3, r4, r5, r6, r7, r8;

#if ENDIAN == MTI_BIG_ENDIAN
         r0 = *((unsigned int *)(src   ));
         r1 = *((unsigned int *)(src+ 4));
         r2 = *((unsigned int *)(src+ 8));
         r3 = *((unsigned int *)(src+12));
         r4 = *((unsigned int *)(src+16));
         r5 = *((unsigned int *)(src+20));
         r6 = *((unsigned int *)(src+24));
         r7 = *((unsigned int *)(src+28));
         r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
         _asm {
         mov    ebx,src
         mov    eax,[ebx]
         bswap  eax
         mov    r0,eax
         mov    eax,[ebx+4]
         bswap  eax
         mov    r1,eax
         mov    eax,[ebx+8]
         bswap  eax
         mov    r2,eax
         mov    eax,[ebx+12]
         bswap  eax
         mov    r3,eax
         mov    eax,[ebx+16]
         bswap  eax
         mov    r4,eax
         mov    eax,[ebx+20]
         bswap  eax
         mov    r5,eax
         mov    eax,[ebx+24]
         bswap  eax
         mov    r6,eax
         mov    eax,[ebx+28]
         bswap  eax
         mov    r7,eax
         mov    eax,[ebx+32]
         bswap  eax
         mov    r8,eax
         }
#else
         r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
         r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
         r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
         r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
         r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
         r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
         r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
         r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
         r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
         src += 36;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==2) goto p2;
         if (vp->ystrTbl[i].phase==4) goto p4;
         goto p6;

         while (true) {

            p0:; // phase 0

               dst[0] = antilogR[(r0>> 4)&0xff]; // R
               dst[1] = antilogG[(r0>>16)&0xff]; // G
               dst[2] = antilogB[((r0>>28)+(r1<<4))&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p1:;

               dst[4] = antilogR[(r1>> 8)&0xff]; // R
               dst[5] = antilogG[(r1>>20)&0xff]; // G
               dst[6] = antilogB[(r2    )&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p2:; // phase 2

               dst[0] = antilogR[(r2>>12)&0xff]; // R
               dst[1] = antilogG[(r2>>24)&0xff]; // G
               dst[2] = antilogB[(r3>> 4)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p3:;

               dst[4] = antilogR[(r3>>16)&0xff]; // R
               dst[5] = antilogG[((r3>>28)+(r4<<4))&0xff]; // G
               dst[6] = antilogB[(r4>> 8)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p4:; // phase 4

               dst[0] = antilogR[(r4>>20)&0xff]; // R
               dst[1] = antilogG[(r5    )&0xff]; // G
               dst[2] = antilogB[(r5>>12)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p5:;

               dst[4] = antilogR[(r5>>24)&0xff]; // R
               dst[5] = antilogG[(r6>> 4)&0xff]; // G
               dst[6] = antilogB[(r6>>16)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

            p6:; // phase 6

               dst[0] = antilogR[((r6>>28)+(r7<<4))&0xff]; // R
               dst[1] = antilogG[(r7>> 8)&0xff]; // G
               dst[2] = antilogB[(r7>>20)&0xff]; // B
               dst[3] = 0x7f;                   // A

            //p7:;

               dst[4] = antilogR[(r8    )&0xff]; // R
               dst[5] = antilogG[(r8>>12)&0xff]; // G
               dst[6] = antilogB[(r8>>24)&0xff]; // B
               dst[7] = 0x7f;                   // A
               dst += 8;

               if (dst == rowend) break;

               // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
               r0 = *((unsigned int *)(src   ));
               r1 = *((unsigned int *)(src+ 4));
               r2 = *((unsigned int *)(src+ 8));
               r3 = *((unsigned int *)(src+12));
               r4 = *((unsigned int *)(src+16));
               r5 = *((unsigned int *)(src+20));
               r6 = *((unsigned int *)(src+24));
               r7 = *((unsigned int *)(src+28));
               r8 = *((unsigned int *)(src+32));
#else
#if MTI_ASM_X86_INTEL
               _asm {
               mov    ebx,src
               mov    eax,[ebx]
               bswap  eax
               mov    r0,eax
               mov    eax,[ebx+4]
               bswap  eax
               mov    r1,eax
               mov    eax,[ebx+8]
               bswap  eax
               mov    r2,eax
               mov    eax,[ebx+12]
               bswap  eax
               mov    r3,eax
               mov    eax,[ebx+16]
               bswap  eax
               mov    r4,eax
               mov    eax,[ebx+20]
               bswap  eax
               mov    r5,eax
               mov    eax,[ebx+24]
               bswap  eax
               mov    r6,eax
               mov    eax,[ebx+28]
               bswap  eax
               mov    r7,eax
               mov    eax,[ebx+32]
               bswap  eax
               mov    r8,eax
               }
#else
               r0 = (src[0]<<24) +(src[1]<<16) +(src[2]<<8) +src[3];
               r1 = (src[4]<<24) +(src[5]<<16) +(src[6]<<8) +src[7];
               r2 = (src[8]<<24) +(src[9]<<16) +(src[10]<<8)+src[11];
               r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
               r4 = (src[16]<<24)+(src[17]<<16)+(src[18]<<8)+src[19];
               r5 = (src[20]<<24)+(src[21]<<16)+(src[22]<<8)+src[23];
               r6 = (src[24]<<24)+(src[25]<<16)+(src[26]<<8)+src[27];
               r7 = (src[28]<<24)+(src[29]<<16)+(src[30]<<8)+src[31];
               r8 = (src[32]<<24)+(src[33]<<16)+(src[34]<<8)+src[35];
#endif
#endif
               src += 36;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeRGB_16_BE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst    = row;

         while (dst < rowend) {

#ifdef DPX_16_BIT_RED_FIRST

            dst[0] = linearR[src[0]];  // R
            dst[1] = linearG[src[2]];  // G
            dst[2] = linearB[src[4]];  // B
            dst[3] = 0x7f;             // A

            dst[4] = linearR[src[6]];  // R
            dst[5] = linearG[src[8]];  // G
            dst[6] = linearB[src[10]]; // B
            dst[7] = 0x7f;             // A

#else // BLUE FIRST

            dst[0] = linearR[src[4]];  // R
            dst[1] = linearG[src[2]];  // G
            dst[2] = linearB[src[0]];  // B
            dst[3] = 0x7f;             // A

            dst[4] = linearR[src[10]]; // R
            dst[5] = linearG[src[8]];  // G
            dst[6] = linearB[src[6]];  // B
            dst[7] = 0x7f;             // A

#endif

            src += 12;
            dst += 8;

         }

      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeRGB_16_BE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // source
               mov    edi,dst                  // -> RGBA output rows
               xor    ebx,ebx                  // clear upper portion

               mov    eax,src
               mov    bl,[eax+4]
               mov    dl,BYTE PTR[esi+ebx+512]
               mov    dh,0x7f
               ror    edx,16

               mov    bl,[eax]
               mov    dl,BYTE PTR[esi+ebx]
               mov    bl,[eax+2]
               mov    dh,BYTE PTR[esi+ebx+256]

               mov    [edi],edx

               add    src,6
               add    dst,4
               }
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    edi,rgbrow               // -> RGBA output rows
            xor    ebx,ebx                  // clear upper portion

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    eax,[edx]                // index into row
            mov    edx,eax                  //
            sal    edx,1                    //
            add    eax,edx                  //
            sal    eax,1                    // * 6
            add    eax,src                  // beg of row

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx

            mov    edx,pTbl
            mov    eax,[edx+4]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx

            mov    edx,pTbl
            mov    eax,[edx+8]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx

            mov    edx,pTbl
            mov    eax,[edx+12]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx

            mov    edx,pTbl
            mov    eax,[edx+16]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx

            mov    edx,pTbl
            mov    eax,[edx+20]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx

            mov    edx,pTbl
            mov    eax,[edx+24]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx

            mov    edx,pTbl
            mov    eax,[edx+28]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    edi,rgbrow
            xor    ebx,ebx

            mov    edx,pTbl
            mov    eax,[edx]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			// advance to next output row
			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif

#if !MTI_ASM_X86_INTEL
static void dstripeRGB_16_BE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst    = row;

         while (dst < rowend) {

#ifdef DPX_16_BIT_RED_FIRST

            dst[0] = antilogR[src[0]];  // R
            dst[1] = antilogG[src[2]];  // G
            dst[2] = antilogB[src[4]];  // B
            dst[3] = 0x7f;              // A

            dst[4] = antilogR[src[6]];  // R
            dst[5] = antilogG[src[8]];  // G
            dst[6] = antilogB[src[10]]; // B
            dst[7] = 0x7f;              // A

#else // BLUE FIRST

            dst[0] = antilogR[src[4]];  // R
            dst[1] = antilogG[src[2]];  // G
            dst[2] = antilogB[src[0]];  // B
            dst[3] = 0x7f;              // A

            dst[4] = antilogR[src[10]]; // R
            dst[5] = antilogG[src[8]];  // G
            dst[6] = antilogB[src[6]];  // B
            dst[7] = 0x7f;              // A

#endif

            src += 12;
            dst += 8;

         }

      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeRGB_16_BE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->antilog8;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // source
               mov    edi,dst                  // -> RGBA output rows
               xor    ebx,ebx                  // clear upper portion

               mov    eax,src
               mov    bl,[eax+4]
               mov    dl,BYTE PTR[esi+ebx+512]
               mov    dh,0x7f
               ror    edx,16

               mov    bl,[eax]
               mov    dl,BYTE PTR[esi+ebx]
               mov    bl,[eax+2]
               mov    dh,BYTE PTR[esi+ebx+256]

               mov    [edi],edx

               add    src,6
               add    dst,4
               }
            }
         }

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    edi,rgbrow               // -> RGBA output rows
            xor    ebx,ebx                  // clear upper portion

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    eax,[edx]                // index into row
            mov    edx,eax                  //
            sal    edx,1                    //
            add    eax,edx                  //
            sal    eax,1                    // * 6
            add    eax,src                  // beg of row

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx

            mov    edx,pTbl
            mov    eax,[edx+4]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx

            mov    edx,pTbl
            mov    eax,[edx+8]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx

            mov    edx,pTbl
            mov    eax,[edx+12]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx

            mov    edx,pTbl
            mov    eax,[edx+16]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx

            mov    edx,pTbl
            mov    eax,[edx+20]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx

            mov    edx,pTbl
            mov    eax,[edx+24]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx

            mov    edx,pTbl
            mov    eax,[edx+28]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    edi,rgbrow
            xor    ebx,ebx

            mov    edx,pTbl
            mov    eax,[edx]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+4]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+2]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // advance to next output row
			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif
///////////////////////////////////////////////////////////////

#if !MTI_ASM_X86_INTEL
static void dstripeRGB_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst    = row;

         while (dst < rowend) {

            dst[0] = linearR[src[1]];  // R
            dst[1] = linearG[src[3]];  // G
            dst[2] = linearB[src[5]];  // B
            dst[3] = 0x7f;             // A

            dst[4] = linearR[src[7]];  // R
            dst[5] = linearG[src[9]];  // G
            dst[6] = linearB[src[11]]; // B
            dst[7] = 0x7f;             // A

            src += 12;
            dst += 8;

         }

      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeRGB_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // source
               mov    edi,dst                  // -> RGBA output rows
               xor    ebx,ebx                  // clear upper portion

               mov    eax,src
               mov    bl,[eax+5]
               mov    dl,BYTE PTR[esi+ebx+512]
               mov    dh,0x7f
               ror    edx,16

               mov    bl,[eax+1]
               mov    dl,BYTE PTR[esi+ebx]
               mov    bl,[eax+3]
               mov    dh,BYTE PTR[esi+ebx+256]

               mov    [edi],edx

               add    src,6
               add    dst,4
               }
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    edi,rgbrow               // -> RGBA output rows
            xor    ebx,ebx                  // clear upper portion

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    eax,[edx]                // index into row
            mov    edx,eax                  //
            sal    edx,1                    //
            add    eax,edx                  //
            sal    eax,1                    // * 6
            add    eax,src                  // beg of row

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx

            mov    edx,pTbl
            mov    eax,[edx+4]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx

            mov    edx,pTbl
            mov    eax,[edx+8]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx

            mov    edx,pTbl
            mov    eax,[edx+12]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx

            mov    edx,pTbl
            mov    eax,[edx+16]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx

            mov    edx,pTbl
            mov    eax,[edx+20]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx

            mov    edx,pTbl
            mov    eax,[edx+24]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx

            mov    edx,pTbl
            mov    eax,[edx+28]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    edi,rgbrow
            xor    ebx,ebx

            mov    edx,pTbl
            mov    eax,[edx]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

         // if grey enabled, massage the row
         if ((rgbMask&0xff000000)==0xff000000)
            makeRowGrey((MTI_UINT8*)rgb, (void*)vp);

         if (pnMask&3)
            makeRowPosNeg((MTI_UINT8*)rgb, (void*)vp);

         // advance to next output row
         rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif
#if !MTI_ASM_X86_INTEL
static void dstripeRGB_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst    = row;

         while (dst < rowend) {

            dst[0] = antilogR[src[1]];  // R
            dst[1] = antilogG[src[3]];  // G
            dst[2] = antilogB[src[5]];  // B
            dst[3] = 0x7f;              // A

            dst[4] = antilogR[src[7]];  // R
            dst[5] = antilogG[src[9]];  // G
            dst[6] = antilogB[src[11]]; // B
            dst[7] = 0x7f;              // A

            src += 12;
            dst += 8;

         }

      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeRGB_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->antilog8;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;
   unsigned int rgbMaskAlpha = rgbMask | 0xff000000;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned char *xstrTbl    = (unsigned char *)vp->xstrTbl;
   unsigned char *xstrTblEnd = (unsigned char *)(vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *rgbrow;
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;

   if (vp->curDstWdth >= 2*vp->curWinWdth) {

      // convert a full row before doing the nearest-neighbor sampling
      // this pathway works better when the resultant image is larger
      // than the source window (i.e., zooming IN). Vble rgb -> output

      unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
      unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

      unsigned char *presrc = NULL;
      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one full row of source

            presrc = src;

            dst    = row;

            while (dst < rowend) {

               _asm {
               mov    esi,lut                  // source
               mov    edi,dst                  // -> RGBA output rows
               xor    ebx,ebx                  // clear upper portion

               mov    eax,src
               mov    bl,[eax+5]
               mov    dl,BYTE PTR[esi+ebx+512]
               mov    dh,0x7f
               ror    edx,16

               mov    bl,[eax+1]
               mov    dl,BYTE PTR[esi+ebx]
               mov    bl,[eax+3]
               mov    dh,BYTE PTR[esi+ebx+256]

               mov    [edi],edx

               add    src,6
               add    dst,4
               }
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      // use nearest-neighbor sampling to select which pels get converted
      // this pathway works better when the resultant image is smaller
      // than the source window (i.e., zooming ALL). Vble rgb -> output

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         rgbrow = rgb;

         // ptr to nearest-neighbor column indices in stretch table
         unsigned char *pTbl = xstrTbl;
         unsigned char *pEnd = xstrTbl + 4*(vp->curDstWdth & 0xFFFFFFF8);
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut                  // used to access LUTs
            mov    edi,rgbrow               // -> RGBA output rows
            xor    ebx,ebx                  // clear upper portion

            // INT *pTbl gives the index into the row
            // of the corresponding nearest-neighbor
            //
            mov    edx,pTbl                 // -> stretch table
            mov    eax,[edx]                // index into row
            mov    edx,eax                  //
            sal    edx,1                    //
            add    eax,edx                  //
            sal    eax,1                    // * 6
            add    eax,src                  // beg of row

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha              // RGBA masked
            // pixel 1 out of 8
            mov    [edi],edx

            mov    edx,pTbl
            mov    eax,[edx+4]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 2 out of 8
            mov    [edi+4],edx

            mov    edx,pTbl
            mov    eax,[edx+8]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 3 out of 8
            mov    [edi+8],edx

            mov    edx,pTbl
            mov    eax,[edx+12]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 4 out of 8
            mov    [edi+12],edx

            mov    edx,pTbl
            mov    eax,[edx+16]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 5 out of 8
            mov    [edi+16],edx

            mov    edx,pTbl
            mov    eax,[edx+20]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 6 out of 8
            mov    [edi+20],edx

            mov    edx,pTbl
            mov    eax,[edx+24]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 7 out of 8
            mov    [edi+24],edx

            mov    edx,pTbl
            mov    eax,[edx+28]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            // pixel 8 out of 8
            mov    [edi+28],edx

            add    rgbrow,32
            add    pTbl,32
            }
         }

         // complete the odd pixels in row
         pEnd = xstrTblEnd;
         while (pTbl < pEnd) {

            _asm {
            mov    esi,lut
            mov    edi,rgbrow
            xor    ebx,ebx

            mov    edx,pTbl
            mov    eax,[edx]
            mov    edx,eax
            sal    edx,1
            add    eax,edx
            sal    eax,1
            add    eax,src

            mov    bl,[eax+5]
            mov    dl,BYTE PTR[esi+ebx+512]
            mov    dh,0x7f
            ror    edx,16

            mov    bl,[eax+1]
            mov    dl,BYTE PTR[esi+ebx]
            mov    bl,[eax+3]
            mov    dh,BYTE PTR[esi+ebx+256]

            and    edx,rgbMaskAlpha
            mov    [edi],edx
            add    rgbrow,4
            add    pTbl,4
            }
         }

         // if grey enabled, massage the row
         if ((rgbMask&0xff000000)==0xff000000)
            makeRowGrey((MTI_UINT8*)rgb, (void*)vp);

         if (pnMask&3)
            makeRowPosNeg((MTI_UINT8*)rgb, (void*)vp);

         // advance to next output row
         rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}
#endif

static void dstripeBGR_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst    = row;

         while (dst < rowend) {

            dst[0] = linearR[src[5]];  // R
            dst[1] = linearG[src[3]];  // G
            dst[2] = linearB[src[1]];  // B
            dst[3] = 0x7f;             // A

            dst[4] = linearR[src[11]]; // R
            dst[5] = linearG[src[9]];  // G
            dst[6] = linearB[src[7]];  // B
            dst[7] = 0x7f;             // A

            src += 12;
            dst += 8;
         }
      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeBGR_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst    = row;

         while (dst < rowend) {

            dst[0] = antilogR[src[5]];  // R
            dst[1] = antilogG[src[3]];  // G
            dst[2] = antilogB[src[1]];  // B
            dst[3] = 0x7f;              // A

            dst[4] = antilogR[src[11]]; // R
            dst[5] = antilogG[src[9]];  // G
            dst[6] = antilogB[src[7]];  // B
            dst[7] = 0x7f;              // A

            src += 12;
            dst += 8;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeRGB_Half(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

	unsigned char *halfLutR = vp->antihalf;
   unsigned char *halfLutG = vp->antihalf + 65536;
   unsigned char *halfLutB = vp->antihalf + 131072;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst;
   unsigned short *srcw;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned short *presrcw = NULL;

   for (int i = begRow; i < endRow; i++)
   {
      srcw = (unsigned short *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (srcw != presrcw)
      {
         // convert one full row of source
         presrcw = srcw;
         dst     = row;

         while (dst < rowend)
			{
				dst[0] = halfLutR[srcw[2] /* ^ 0x8000 */];  // R
				dst[1] = halfLutG[srcw[1] /* ^ 0x8000 */];  // G
				dst[2] = halfLutB[srcw[0] /* ^ 0x8000 */];  // B
				dst[3] = 0x7f;                        // A

            srcw += 3;
            dst += 4;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeRGBA_16_LE(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert*) v;

	unsigned char *linearR = vp->linear8;
	unsigned char *linearG = vp->linear8 + 256;
	unsigned char *linearB = vp->linear8 + 512;
	unsigned char **curSrcBuf = vp->curSrcBuf;

	int *xstrTbl = vp->xstrTbl;
	int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
	ROWENTRY *ystrTbl = vp->ystrTbl;
	unsigned char *src;
	unsigned char *dst;
	unsigned int rgbMask = vp->rgbMask;
	unsigned int pnMask = vp->pnMask;

	unsigned char *alphadst;
	unsigned char alpha = vp->alphaVal;
	int alph2 = ((int)alpha) << 24;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

	unsigned char *row = (unsigned char *)vp->RGBrow[iJob]; unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
	unsigned char *rgb = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch)); int rgbadv = 4 * vp->curDstPitch;
	unsigned char *presrc = NULL;

	unsigned int alphaColor = vp->alphaColor;
	if (alphaColor & 0xffffff)
	{
		// alpha display is turned on
		unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];

		for (int i = begRow; i < endRow; i++)
		{
			int rowOffset = ystrTbl[i].offs; // offset in words to RGB pixel
			src = curSrcBuf[ystrTbl[i].fld] + rowOffset;

			if (src != presrc)
			{
				// convert one full row of source
				presrc = src;
				dst = row;
				alphadst = alpharow;
				while (dst < rowend)
				{
					if (src[7] & 0xC0)
					{
						*(int *)dst      = alph2;
                  *(int *)alphadst = alphaColor;
					}
					else
					{
						dst[0] = linearR[src[1]]; // R
						dst[1] = linearG[src[3]]; // G
						dst[2] = linearB[src[5]]; // B
						dst[3] = 0x7f; // A

                  *(int *)alphadst = 0;
					}

					src += 8;
					dst += 4;
					alphadst += 4;
				}
			}

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			// ...then ADD the alpha onto it
			rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
		}
	}
	else
	{
		for (int i = begRow; i < endRow; i++)
		{
			src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

			if (src != presrc)
			{
				// convert one full row of source
				presrc = src;
				dst = row;
				while (dst < rowend)
				{
					dst[0] = linearR[src[1]]; // R
					dst[1] = linearG[src[3]]; // G
					dst[2] = linearB[src[5]]; // B
					dst[3] = 0x7f; // A

					src += 8;
					dst += 4;
				}
			}

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;
		}
	}
KCATCH
}

static void dstripeRGBA_16_LE_LOG(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert*) v;

	unsigned char *antilogR = vp->antilog8;
	unsigned char *antilogG = vp->antilog8 + 256;
	unsigned char *antilogB = vp->antilog8 + 512;
	unsigned char **curSrcBuf = vp->curSrcBuf;

	int *xstrTbl = vp->xstrTbl;
	int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
	ROWENTRY *ystrTbl = vp->ystrTbl;
	unsigned char *src;
	unsigned char *dst;
	unsigned int rgbMask = vp->rgbMask;
	unsigned int pnMask = vp->pnMask;

	unsigned char *alphadst;
	unsigned char alpha = vp->alphaVal;
	int alph2 = ((int)alpha) << 24;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

	unsigned int alphaColor = vp->alphaColor;
	if (alphaColor & 0xffffff)
	{
		// alpha display is turned on
		unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];

		for (int i = begRow; i < endRow; i++)
		{
			int rowOffset = ystrTbl[i].offs; // offset in words to RGB pixel
			src = curSrcBuf[ystrTbl[i].fld] + rowOffset;

			if (src != presrc)
			{
				// convert one full row of source
				presrc = src;
				dst = row;
				alphadst = alpharow;
				while (dst < rowend)
				{
					if (src[7] & 0xC0)
					{
						*(int *)dst      = alph2;
						*(int *)alphadst = alphaColor;
					}
					else
					{
						dst[0] = antilogR[src[1]];  // R
						dst[1] = antilogG[src[3]];  // G
						dst[2] = antilogB[src[5]];  // B
						dst[3] = 0x7f;              // A

                  *(int *)alphadst = 0;
					}

					src += 8;
					dst += 4;
					alphadst += 4;
				}
			}

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			// ...then ADD the alpha onto it
			rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
		}
	}
	else
	{
		for (int i = begRow; i < endRow; i++)
		{
			src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

			if (src != presrc)
			{
				// convert one full row of source
				presrc = src;
				dst = row;
				while (dst < rowend)
				{
					dst[0] = antilogR[src[1]];  // R
					dst[1] = antilogG[src[3]];  // G
					dst[2] = antilogB[src[5]];  // B
					dst[3] = 0x7f;              // A

					src += 8;
					dst += 4;
				}
			}

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;
		}
	}
KCATCH
}

#include "ImageDatumConvert.h"

static void dstripeRGBA_Half(void *v, int iJob)
{
KTRY
#if NO_YELLOW_HALF
   CConvert *vp = (CConvert *) v;

	unsigned char *halfLutR = vp->antihalf;
   unsigned char *halfLutG = vp->antihalf +  65536;
	unsigned char *halfLutB = vp->antihalf + 131072;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst;
   unsigned short *srcw;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned short *presrcw = NULL;

   for (int i = begRow; i < endRow; i++)
   {
      srcw = (unsigned short *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (srcw != presrcw)
      {
         // convert one full row of source
         presrcw = srcw;
         dst     = row;

         while (dst < rowend)
         {
				dst[0] = halfLutR[srcw[3] ^ 0x8000];  // R
				dst[1] = halfLutG[srcw[2] ^ 0x8000];  // G
				dst[2] = halfLutB[srcw[1] ^ 0x8000];  // B
				dst[3] = 0x7f;                        // A

            srcw += 4;
            dst += 4;
         }
      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe

#else

   CConvert *vp = (CConvert *) v;

	unsigned char *halfLutR = vp->antihalf;
   unsigned char *halfLutG = vp->antihalf +  65536;
	unsigned char *halfLutB = vp->antihalf + 131072;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst;
   unsigned short *srcw;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

	unsigned char *alphadst;
	unsigned char alpha = vp->alphaVal;
	int alph2 = ((int)alpha) << 24;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned short *presrcw = NULL;


	unsigned int alphaColor = vp->alphaColor;
	if (alphaColor & 0xffffff)
	{
      // Stupid FotoKem hack - they turn Scanity 2-bit alpha dirt map into float
      // (0, 1, 2 are defective and 3 is not). They map 3 -> 1.0 so here I declare
      // anything under 0.75 to be defective! Note that in our internal half format
      // the sign bit is flipped, so BC00 == 1.0, B800 == 0.5 and so B980 is 0.6875
      // (.5 + .125 + .0625)
      const unsigned short Point6875 = 0xB980;
      float temp;
      vector<std::pair<int,int> > yellowList;

		// alpha display is turned on
		unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];

      for (int i = begRow; i < endRow; i++)
      {
         srcw = (unsigned short *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (srcw != presrcw)
         {
            // convert one full row of source
            presrcw  = srcw;
            dst      = row;
				alphadst = alpharow;

            while (dst < rowend)
            {
					if (srcw[3] <= Point6875 && srcw[3] != 0U && srcw[3] != 0x8000U)
					{
                  temp = ImageDatum::toFloat(srcw[3], true);
						*(int *)dst      = alph2;
						*(int *)alphadst = alphaColor;
                  yellowList.emplace_back((dst - row) / 4, i);
					}
					else
					{
						dst[0] = halfLutR[srcw[3]];  // R
						dst[1] = halfLutG[srcw[2]];  // G
						dst[2] = halfLutB[srcw[1]];  // B
                  dst[3] = 0x7f;               // A

                  *(int *)alphadst = 0;
               }

               srcw += 4;
               dst += 4;
					alphadst += 4;
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			// ...then ADD the alpha onto it
			rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
      }

      TRACE_3(errout << "YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY");
      for (auto pos : yellowList)
      {
         TRACE_3(errout << "    (" << pos.first << ", " << pos.second << ")");
      }
      TRACE_3(errout << "YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY");
   }
	else
	{
      for (int i = begRow; i < endRow; i++)
      {
         srcw = (unsigned short *)(curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (srcw != presrcw)
         {
            // convert one full row of source
            presrcw = srcw;
            dst     = row;

            while (dst < rowend)
            {
					dst[0] = halfLutR[srcw[3]];  // R
					dst[1] = halfLutG[srcw[2]];  // G
               dst[2] = halfLutB[srcw[1]];  // B
               dst[3] = 0x7f;               // A

               srcw += 4;
               dst += 4;
            }
         }

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;
      }
   }
#endif
KCATCH
}

static void dstripeRGBA_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   //int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
	unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

	unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one row

            presrc = src;

            dst = row;
            alphadst = alpharow;

            unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src   ));
            r1 = *((unsigned int *)(src+ 4));
            r2 = *((unsigned int *)(src+ 8));
            r3 = *((unsigned int *)(src+12));
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
            r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
            src += 16;

            //if (rowphase==0) goto p0;
            //if (rowphase==1) goto p1;
            //goto p2;
            if (vp->ystrTbl[i].phase==0) goto p0;
            if (vp->ystrTbl[i].phase==1) goto p1;
            goto p2;

            while (true) {

               p0:; // phase 0

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r1 & 0xc0000000) == 0xc0000000) {

                     *(int *)dst = 0x7f000000;
							*(int *)alphadst = alphaColor;
                  }
                  else {
                     dst[0] = linearR[(r0>>24)&0xff]; // R
                     dst[1] = linearG[(r0>>14)&0xff]; // G
                     dst[2] = linearB[(r0>> 4)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
						alphadst += 4;

                  if (dst == rowend) break;

               p1:; // phase 1

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r2 & 0x00300000) == 0x00300000) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {

                     dst[0] = linearR[(r1>>14)&0xff]; // R
                     dst[1] = linearG[(r1>> 4)&0xff]; // G
                     dst[2] = linearB[(r2>>24)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

               p2:; // phase 2

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r3 & 0x00000c00) == 0x00000c00) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {

                     dst[0] = linearR[(r2>> 4)&0xff]; // R
                     dst[1] = linearG[(r3>>24)&0xff]; // G
                     dst[2] = linearB[(r3>>14)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

                  // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
                  r0 = *((unsigned int *)(src   ));
                  r1 = *((unsigned int *)(src+ 4));
                  r2 = *((unsigned int *)(src+ 8));
                  r3 = *((unsigned int *)(src+12));
#else
                  r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
                  r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
                  r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
                  r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
                  src += 16;
            }
         }

			// output the converted row...
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			// ...then ADD the alpha onto it
			rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one row

            presrc = src;

            dst = row;

            unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src   ));
            r1 = *((unsigned int *)(src+ 4));
            r2 = *((unsigned int *)(src+ 8));
            r3 = *((unsigned int *)(src+12));
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
            r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
            src += 16;

            //if (rowphase==0) goto p0;
            //if (rowphase==1) goto p1;
            //goto p2;
            if (vp->ystrTbl[i].phase==0) goto q0;
            if (vp->ystrTbl[i].phase==1) goto q1;
            goto q2;

            while (true) {

               q0:; // phase 0

                  dst[0] = linearR[(r0>>24)&0xff]; // R
                  dst[1] = linearG[(r0>>14)&0xff]; // G
                  dst[2] = linearB[(r0>> 4)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

               q1:; // phase 1

                  dst[0] = linearR[(r1>>14)&0xff]; // R
                  dst[1] = linearG[(r1>> 4)&0xff]; // G
                  dst[2] = linearB[(r2>>24)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

               q2:; // phase 2

                  dst[0] = linearR[(r2>> 4)&0xff]; // R
                  dst[1] = linearG[(r3>>24)&0xff]; // G
                  dst[2] = linearB[(r3>>14)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

                  // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
                  r0 = *((unsigned int *)(src   ));
                  r1 = *((unsigned int *)(src+ 4));
                  r2 = *((unsigned int *)(src+ 8));
                  r3 = *((unsigned int *)(src+12));
#else
                  r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
                  r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
                  r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
                  r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
                  src += 16;
            }
         }

         // output the converted row...
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

static void dstripeRGBA_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   //int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one row

            presrc = src;

            dst = row;
            alphadst = alpharow;

            unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src   ));
            r1 = *((unsigned int *)(src+ 4));
            r2 = *((unsigned int *)(src+ 8));
            r3 = *((unsigned int *)(src+12));
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
            r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
            src += 16;

            //if (rowphase==0) goto p0;
            //if (rowphase==1) goto p1;
            //goto p2;
            if (vp->ystrTbl[i].phase==0) goto p0;
            if (vp->ystrTbl[i].phase==1) goto p1;
            goto p2;

            while (true) {

               p0:; // phase 0

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r1 & 0xc0000000)==0xc0000000) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {
                     dst[0] = antilogR[(r0>>24)&0xff]; // R
                     dst[1] = antilogG[(r0>>14)&0xff]; // G
                     dst[2] = antilogB[(r0>> 4)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

               p1:; // phase 1

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r2 & 0x00300000) == 0x00300000) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {

                     dst[0] = antilogR[(r1>>14)&0xff]; // R
                     dst[1] = antilogG[(r1>> 4)&0xff]; // G
                     dst[2] = antilogB[(r2>>24)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

               p2:; // phase 2

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r3 & 0x00000c00) == 0x00000c00) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {

                     dst[0] = antilogR[(r2>> 4)&0xff]; // R
                     dst[1] = antilogG[(r3>>24)&0xff]; // G
                     dst[2] = antilogB[(r3>>14)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

                  // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
                  r0 = *((unsigned int *)(src   ));
                  r1 = *((unsigned int *)(src+ 4));
                  r2 = *((unsigned int *)(src+ 8));
                  r3 = *((unsigned int *)(src+12));
#else
                  r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
                  r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
                  r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
                  r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
                  src += 16;
            }
         }

			// output the converted row...
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one row

            presrc = src;

            dst = row;

            unsigned int r0, r1, r2, r3;

#if ENDIAN == MTI_BIG_ENDIAN
            r0 = *((unsigned int *)(src   ));
            r1 = *((unsigned int *)(src+ 4));
            r2 = *((unsigned int *)(src+ 8));
            r3 = *((unsigned int *)(src+12));
#else
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
            r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
            src += 16;

            //if (rowphase==0) goto p0;
            //if (rowphase==1) goto p1;
            //goto p2;
            if (vp->ystrTbl[i].phase==0) goto q0;
            if (vp->ystrTbl[i].phase==1) goto q1;
            goto q2;

            while (true) {

               q0:; // phase 0

                  dst[0] = antilogR[(r0>>24)&0xff]; // R
                  dst[1] = antilogG[(r0>>14)&0xff]; // G
                  dst[2] = antilogB[(r0>> 4)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

               q1:; // phase 1

                  dst[0] = antilogR[(r1>>14)&0xff]; // R
                  dst[1] = antilogG[(r1>> 4)&0xff]; // G
                  dst[2] = antilogB[(r2>>24)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

               q2:; // phase 2

                  dst[0] = antilogR[(r2>> 4)&0xff]; // R
                  dst[1] = antilogG[(r3>>24)&0xff]; // G
                  dst[2] = antilogB[(r3>>14)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

                  // next pixel-sextuple
#if ENDIAN == MTI_BIG_ENDIAN
                  r0 = *((unsigned int *)(src   ));
                  r1 = *((unsigned int *)(src+ 4));
                  r2 = *((unsigned int *)(src+ 8));
                  r3 = *((unsigned int *)(src+12));
#else
                  r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
                  r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
                  r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
                  r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
#endif
                  src += 16;
            }
         }

         // output the converted row...
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

static void dstripeBGRA_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   //int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one row

            presrc = src;

            dst = row;
            alphadst = alpharow;

            unsigned int r0, r1, r2, r3;

            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
            r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
            src += 16;

            if (vp->ystrTbl[i].phase==0) goto p0;
            if (vp->ystrTbl[i].phase==1) goto p1;
            goto p2;

            while (true) {

               p0:; // phase 0

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r1 & 0xc0000000) == 0xc0000000) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {
                     dst[0] = linearR[(r0>> 4)&0xff]; // R
                     dst[1] = linearG[(r0>>14)&0xff]; // G
                     dst[2] = linearB[(r0>>24)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

               p1:; // phase 1

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r2 & 0x00300000) == 0x00300000) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {

                     dst[0] = linearR[(r1>>24)&0xff]; // R
                     dst[1] = linearG[(r1>> 4)&0xff]; // G
                     dst[2] = linearB[(r2>>14)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

               p2:; // phase 2

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r3 & 0x00000c00) == 0x00000c00) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {

                     dst[0] = linearR[(r2>>14)&0xff]; // R
                     dst[1] = linearG[(r3>>24)&0xff]; // G
                     dst[2] = linearB[(r3>> 4)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

                  // next pixel-sextuple
                  r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
                  r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
                  r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
                  r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
                  src += 16;
            }
         }

         // output the converted row...
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			// ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one row

            presrc = src;

            dst = row;

            unsigned int r0, r1, r2, r3;

            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
            r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
            src += 16;

            if (vp->ystrTbl[i].phase==0) goto q0;
            if (vp->ystrTbl[i].phase==1) goto q1;
            goto q2;

            while (true) {

               q0:; // phase 0

                  dst[0] = linearR[(r0>> 4)&0xff]; // R
                  dst[1] = linearG[(r0>>14)&0xff]; // G
                  dst[2] = linearB[(r0>>24)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

               q1:; // phase 1

                  dst[0] = linearR[(r1>>24)&0xff]; // R
                  dst[1] = linearG[(r1>> 4)&0xff]; // G
                  dst[2] = linearB[(r2>>14)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

               q2:; // phase 2

                  dst[0] = linearR[(r2>>14)&0xff]; // R
                  dst[1] = linearG[(r3>>24)&0xff]; // G
                  dst[2] = linearB[(r3>> 4)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

                  // next pixel-sextuple
                  r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
                  r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
                  r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
                  r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
                  src += 16;
            }
         }

         // output the converted row...
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

static void dstripeBGRA_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst, *alphadst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   //int rowphase = vp->phase;
   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));
   unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one row

            presrc = src;

            dst = row;
            alphadst = alpharow;

            unsigned int r0, r1, r2, r3;

            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
            r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
            src += 16;

            if (vp->ystrTbl[i].phase==0) goto p0;
            if (vp->ystrTbl[i].phase==1) goto p1;
            goto p2;

            while (true) {

               p0:; // phase 0

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r1 & 0xc0000000)==0xc0000000) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {
                     dst[0] = antilogR[(r0>> 4)&0xff]; // R
                     dst[1] = antilogG[(r0>>14)&0xff]; // G
                     dst[2] = antilogB[(r0>>24)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

               p1:; // phase 1

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r2 & 0x00300000) == 0x00300000) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {

                     dst[0] = antilogR[(r1>>24)&0xff]; // R
                     dst[1] = antilogG[(r1>> 4)&0xff]; // G
                     dst[2] = antilogB[(r2>>14)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

               p2:; // phase 2

                  // ARRI HACK: Make pixel yellow if alpha value is >= 0x300
                  if ((r3 & 0x00000c00) == 0x00000c00) {

                     *(int *)dst = 0x7f000000;
                     *(int *)alphadst = alphaColor;
                  }
                  else {

                     dst[0] = antilogR[(r2>>14)&0xff]; // R
                     dst[1] = antilogG[(r3>>24)&0xff]; // G
                     dst[2] = antilogB[(r3>> 4)&0xff]; // B
                     dst[3] = 0x7f;                   // A
                     *(int *)alphadst = 0;
                  }

                  dst += 4;
                  alphadst += 4;

                  if (dst == rowend) break;

                  // next pixel-sextuple
                  r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
                  r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
                  r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
                  r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
                  src += 16;
            }
         }

         // output the converted row...
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;

      } // for each row of stripe
   }
   else {

      for (int i=begRow;i<endRow;i++) {

         src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

         if (src != presrc) { // convert one row

            presrc = src;

            dst = row;

            unsigned int r0, r1, r2, r3;

            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
            r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
            r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
            src += 16;

            if (vp->ystrTbl[i].phase==0) goto q0;
            if (vp->ystrTbl[i].phase==1) goto q1;
            goto q2;

            while (true) {

               q0:; // phase 0

                  dst[0] = antilogR[(r0>> 4)&0xff]; // R
                  dst[1] = antilogG[(r0>>14)&0xff]; // G
                  dst[2] = antilogB[(r0>>24)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

               q1:; // phase 1

                  dst[0] = antilogR[(r1>>24)&0xff]; // R
                  dst[1] = antilogG[(r1>> 4)&0xff]; // G
                  dst[2] = antilogB[(r2>>14)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

               q2:; // phase 2

                  dst[0] = antilogR[(r2>>14)&0xff]; // R
                  dst[1] = antilogG[(r3>>24)&0xff]; // G
                  dst[2] = antilogB[(r3>> 4)&0xff]; // B
                  dst[3] = 0x7f;                   // A

                  dst += 4;

                  if (dst == rowend) break;

                  // next pixel-sextuple
                  r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
                  r1 = (src[4]<<24)+(src[5]<<16)+(src[6]<<8)+src[7];
                  r2 = (src[8]<<24)+(src[9]<<16)+(src[10]<<8)+src[11];
                  r3 = (src[12]<<24)+(src[13]<<16)+(src[14]<<8)+src[15];
                  src += 16;
            }
         }

         // output the converted row...
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;

      } // for each row of stripe
   }
KCATCH
}

static void dstripeBGRA_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst    = row;

         while (dst < rowend) {

            dst[0] = linearR[src[5]];  // R
            dst[1] = linearG[src[3]];  // G
            dst[2] = linearB[src[1]];  // B
            dst[3] = 0x7f;             // A

            dst[4] = linearR[src[13]]; // R
            dst[5] = linearG[src[11]]; // G
            dst[6] = linearB[src[9]];  // B
            dst[7] = 0x7f;             // A

            src += 16;
            dst += 8;
         }
      }

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

static void dstripeBGRA_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned char *rgb    = (unsigned char *)(vp->curDstBuf + (begRow * vp->curDstPitch));
   int rgbadv            = 4 * vp->curDstPitch;
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst    = row;

         while (dst < rowend) {

            dst[0] = antilogR[src[5]];  // R
            dst[1] = antilogG[src[3]];  // G
            dst[2] = antilogB[src[1]];  // B
            dst[3] = 0x7f;              // A

            dst[4] = antilogR[src[13]]; // R
            dst[5] = antilogG[src[11]]; // G
            dst[6] = antilogB[src[8]];  // B
            dst[7] = 0x7f;              // A

            src += 16;
            dst += 8;
         }
      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void dstripeYYY_10_LINEAR(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->linear8;
   stripeYYY_10(vp, lut, iJob, true);
KCATCH
}

static void dstripeYYY_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->antilog8;
   stripeYYY_10(vp, lut, iJob, true);
KCATCH
}

/////////////////////////////////////////////////////////////////

static void dstripeYYY_10_IN_16_LINEAR(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->linear8;
   stripeYYY_10_IN_16(vp, lut, iJob, true);
KCATCH
}

static void dstripeYYY_10_IN_16_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->antilog8;
   stripeYYY_10_IN_16(vp, lut, iJob, true);
KCATCH
}

/////////////////////////////////////////////////////////////////

static void dstripeYYY_16_LE_LINEAR(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->linear8;
   stripeYYY_16_LE(vp, lut, iJob, true);
KCATCH
}

static void dstripeYYY_16_LE_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char *lut = vp->antilog8;
   stripeYYY_16_LE(vp, lut, iJob, true);
KCATCH
}

/////////////////////////////////////////////////////////////////
#if 1
void dstripeMON_10X(void *v, int iJob, bool alignmentIsL, bool isLog)
{
KTRY
   CConvert *vp = (CConvert*) v;

   unsigned char *lut = isLog ? vp->antilog8 : vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned int rgbMask = vp->rgbMask;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
//   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   int rgbadv            = vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i = begRow; i < endRow; i++)
   {
      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc)
      { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;

         // Input is big-endian, so load swapped.
         if (alignmentIsL)
         {
            r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
         }
         else
         {
            r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
         }

         src += 4;

         if (vp->ystrTbl[i].phase == 0)
         {
            goto p0;
         }

         if (vp->ystrTbl[i].phase == 1)
         {
            goto p1;
         }

         goto p2;

         while (true)
         {

         p0: ; // phase 0

            r1 = lut[(r0 >> 2) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p1: ; // phase 1

            r1 = lut[(r0 >> 12) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p2: ; // phase 2

            r1 = lut[(r0 >> 22) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

            // next pixel-triple
            if (alignmentIsL)
            {
               r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
            }
            else
            {
               r0 = (src[0] << 24) + (src[1] << 16) + (src[2] << 8) + src[3];
            }
            src += 4;
         }
      }

      // output the converted row
      if (vp->_isWiperComparand)
      {
         rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
      }
      else
      {
         rowRGB(rgb, row, vp);
      }

      rgb += rgbadv;

   } // for each row of stripe

KCATCH
}

static void dstripeMON_10L(void *v, int iJob)
{
   dstripeMON_10X(v, iJob, true, false);
}

static void dstripeMON_10L_LOG(void *v, int iJob)
{
   dstripeMON_10X(v, iJob, true, true);
}

static void dstripeMON_10R(void *v, int iJob)
{
   dstripeMON_10X(v, iJob, false, false);
}

static void dstripeMON_10R_LOG(void *v, int iJob)
{
   dstripeMON_10X(v, iJob, false, true);
}
#endif
/////////////////////////////////////////////////////////////////
#if 0
static void dstripeMON_10L(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned int rgbMask = vp->rgbMask;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
//   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   int rgbadv            = vp->curDstPitch;
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;

         // Byte swapped load
         r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
         src += 4;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true)
         {

         p0: ; // phase 0

            r1 = linear[(r0 >> 2) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p1: ; // phase 1

            r1 = linear[(r0 >> 12) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p2: ; // phase 2

            r1 = linear[(r0 >> 22) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

            // next pixel-triple
            r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
            src += 4;
         }
      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

/////////////////////////////////////////////////////////////////

static void dstripeMON_10L_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilog = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned int rgbMask = vp->rgbMask;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
//   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   int rgbadv            = vp->curDstPitch;
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;

         // Byte swapped load
         r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
         src += 4;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true)
         {

         p0: ; // phase 0

            r1 = antilog[(r0 >> 2) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p1: ; // phase 1

            r1 = antilog[(r0 >> 12) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p2: ; // phase 2

            r1 = antilog[(r0 >> 22) & 0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

            // next pixel-triple
            r0 = (src[0] << 22) + (src[1] << 14) + (src[2] << 6) + (src[3] >> 2);
            src += 4;
         }
      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

/////////////////////////////////////////////////////////////////

static void dstripeMON_10R(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned int rgbMask = vp->rgbMask;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
//   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   int rgbadv            = vp->curDstPitch;
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;

         // Input is big-endian, so load swapped.
         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
         src += 4;

         if (vp->ystrTbl[i].phase == 0)
         {
            goto p0;
         }

         if (vp->ystrTbl[i].phase == 1)
         {
            goto p1;
         }

         goto p2;

         while (true)
         {

         p0: ; // phase 0

            r1 = linear[(r0 >> 2) & 0xff];
            // r1 = linear[(r0)&0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p1: ; // phase 1

            r1 = linear[(r0 >> 12) & 0xff];
            // r1 = linear[(r0>>10)&0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

         p2: ; // phase 2

            //r1 = linear[r0 & 0xff];
            r1 = linear[(r0>>22)&0xff];
            dst[0] = dst[1] = dst[2] = r1;
            dst[3] = 0x7f;
            dst += 4;

            if (dst == rowend)
            {
               break;
            }

            // next pixel-triple
            r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
            src += 4;
         }
      }

//      // now output one row of the finished stripe
//      unsigned int *irow = vp->RGBrow[iJob];
//      if (rgbMask&0x00ffffff) {
//         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
//            *rgb++ = irow[*xstrTblPtr];
//         }
//      }
//      else {
//         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
//            *rgb++ = 0;
//         }
//      }
//
//      if (vp->pnMask&3)
//         makeRowPosNeg((MTI_UINT8*)rgb, (void*)vp);

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

/////////////////////////////////////////////////////////////////

static void dstripeMON_10R_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilog = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned int rgbMask = vp->rgbMask;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
//   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   int rgbadv            = vp->curDstPitch;
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1;

         // Byte swapped load
         r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
         src += 4;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         goto p2;

         while (true) {

            p0:; // phase 0

	       r1 = antilog[(r0>>2)&0xff];
               dst[0] = r1;
               dst[1] = r1;
               dst[2] = r1;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p1:; // phase 1

               r1 = antilog[(r0>>12)&0xff];
               dst[0] = r1;
               dst[1] = r1;
               dst[2] = r1;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p2:; // phase 2

               r1 = antilog[(r0>>22)&0xff];
               dst[0] = r1;
               dst[1] = r1;
               dst[2] = r1;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

               // next pixel-triple
               r0 = (src[0]<<24)+(src[1]<<16)+(src[2]<<8)+src[3];
               src += 4;
         }
      }

//      // now output one row of the finished stripe
//      unsigned int *irow = vp->RGBrow[iJob];
//      if (rgbMask&0x00ffffff) {
//         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
//            *rgb++ = irow[*xstrTblPtr];
//         }
//      }
//      else {
//         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
//            *rgb++ = 0;
//         }
//      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#endif
//////////////////////////////////////////////////////////////////////

static void dstripeMON_10P(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned int rgbMask = vp->rgbMask;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
//   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   int rgbadv            = vp->curDstPitch;
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2;

#if ENDIAN==MTI_BIG_ENDIAN
         r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
         r1 = src[4];
#else
         r0 = *((unsigned int *)src);
         r1 = src[4];
#endif
         src += 5;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         if (vp->ystrTbl[i].phase==2) goto p2;
         goto p3;

         while (true) {

            p0:; // phase 0

               r2 = linear[(r0>>2)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p1:; // phase 1

               r2 = linear[(r0>>12)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p2:; // phase 2

               r2 = linear[(r0>>22)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p3:; // phase 3

               r2 = linear[(r1)];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

               // next pixel-triple
#if ENDIAN==MTI_BIG_ENDIAN
              r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
              r1 = src[4];
#else
              r0 = *((unsigned int *)src);
              r1 = src[4];
#endif
              src += 5;

         }
      }

//      // now output one row of the finished stripe
//      unsigned int *irow = vp->RGBrow[iJob];
//      if (rgbMask&0x00ffffff) {
//         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
//            *rgb++ = irow[*xstrTblPtr];
//         }
//      }
//      else {
//         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
//            *rgb++ = 0;
//         }
//      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

/////////////////////////////////////////////////////////////////

static void dstripeMON_10P_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *antilog = vp->antilog8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   unsigned int rgbMask = vp->rgbMask;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + (vp->curWinWdth&0xFFFFFFFE));

   unsigned int *rgb     = (vp->curDstBuf + begRow*vp->curDstPitch);
//   unsigned int rgbadv   = (vp->curDstPitch - vp->curDstWdth);
   int rgbadv            = vp->curDstPitch;
   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one row

         presrc = src;

         dst = row;

         unsigned int r0, r1, r2;

#if ENDIAN==MTI_BIG_ENDIAN
         r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
         r1 = src[4];
#else
         r0 = *((unsigned int *)src);
         r1 = src[4];
#endif
         src += 5;

         if (vp->ystrTbl[i].phase==0) goto p0;
         if (vp->ystrTbl[i].phase==1) goto p1;
         if (vp->ystrTbl[i].phase==2) goto p2;
         goto p3;

         while (true) {

            p0:; // phase 0

               r2 = antilog[(r0>>2)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p1:; // phase 1

               r2 = antilog[(r0>>12)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p2:; // phase 2

               r2 = antilog[(r0>>22)&0xff];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

            p3:; // phase 3

               r2 = antilog[(r1)];
               dst[0] = r2;
               dst[1] = r2;
               dst[2] = r2;
               dst[3] = 0x7f;
               dst += 4;

               if (dst == rowend) break;

               // next pixel-triple
#if ENDIAN==MTI_BIG_ENDIAN
              r0 = (src[3]<<24)+(src[2]<<16)+(src[1]<<8)+src[0];
              r1 = src[4];
#else
              r0 = *((unsigned int *)src);
              r1 = src[4];
#endif
              src += 5;

         }
      }

//      // now output one row of the finished stripe
//      unsigned int *irow = vp->RGBrow[iJob];
//      if (rgbMask&0x00ffffff) {
//         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
//            *rgb++ = irow[*xstrTblPtr];
//         }
//      }
//      else {
//         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
//            *rgb++ = 0;
//         }
//      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

/////////////////////////////////////////////////////////////////

#if 0
static void dstripeMON_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     = vp->curDstBuf + (begRow * vp->curDstPitch);
   unsigned int rgbadv   = vp->curDstPitch - vp->curDstWdth;

   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs;

      if (src != presrc)
      {
         // convert one full row of source
         presrc = src;
         dst    = row;

         while (dst < rowend)
         {
            dst[0] = dst[1] = dst[2] = linear[src[1]]; // Y -> RGB
            dst[3] = 0x7f;                             // A

            dst[4] = dst[5] = dst[6] = linear[src[3]]; // Y -> RGB
            dst[7] = 0x7f;                             // A

            src += 4;
            dst += 8;
         }
      }

//      // now output one row of the finished stripe
//      unsigned int *irow = vp->RGBrow[iJob];
//      if (rgbMask & 0x00ffffff)
//      {
//         for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
//         {
//            *rgb++ = irow[*xstrTblPtr];
//         }
//      }
//      else
//      {
//         for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
//         {
//            *rgb++ = 0;
//         }
//      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

      // How come some formats do something with the pnmask but others don't?? QQQ

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#else
static void dstripeMON_16_LE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);
   unsigned int *rgb     = vp->curDstBuf + (begRow * vp->curDstPitch);
   int rgbadv            = vp->curDstPitch;
   unsigned char *presrc = NULL;

   for (int i=begRow;i<endRow;i++) {

      src = (curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs);

      if (src != presrc) { // convert one full row of source

         presrc = src;

         dst    = row;

         while (dst < rowend) {

            auto v = linearR[src[1]];
            dst[0] = v;    // R
            dst[1] = v;    // G
            dst[2] = v;    // B
            dst[3] = 0x7f; // A

            src += 2;
            dst += 4;
         }
      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

		rgb += rgbadv;

   } // for each row of stripe
KCATCH
}
#endif

/////////////////////////////////////////////////////////////////

static void dstripeMON_16_BE(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char *linear = vp->linear8;
   unsigned char **curSrcBuf = vp->curSrcBuf;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = vp->xstrTbl + vp->curDstWdth;
   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *src, *dst;
   unsigned int rgbMask = vp->rgbMask;
   unsigned int pnMask = vp->pnMask;

   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   unsigned int *rgb     = vp->curDstBuf + (begRow * vp->curDstPitch);
   unsigned int rgbadv   = vp->curDstPitch;

   unsigned char *presrc = NULL;
   for (int i=begRow;i<endRow;i++) {

      src = curSrcBuf[ystrTbl[i].fld] + ystrTbl[i].offs;

      if (src != presrc)
      {
         // convert one full row of source
         presrc = src;
         dst    = row;

         while (dst < rowend)
         {
            dst[0] = dst[1] = dst[2] = linear[src[0]]; // Y -> RGB
            dst[3] = 0x7f;                             // A

            dst[4] = dst[5] = dst[6] = linear[src[2]]; // Y -> RGB
            dst[7] = 0x7f;                             // A

            src += 4;
            dst += 8;
         }
      }

//      // now output one row of the finished stripe
//      unsigned int *irow = vp->RGBrow[iJob];
//      if (rgbMask & 0x00ffffff)
//      {
//         for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
//         {
//            *rgb++ = irow[*xstrTblPtr];
//         }
//      }
//      else
//      {
//         for (int *xstrTblPtr = xstrTbl; xstrTblPtr < xstrTblEnd; ++xstrTblPtr)
//         {
//            *rgb++ = 0;
//         }
//      }

      // output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}

      // How come some formats do something with the pnmask but others don't??

      rgb += rgbadv;

   } // for each row of stripe
KCATCH
}

// 11/24/04
// These have been modified to convert one pixel at a time
// (instead of pixel pairs) because the number of pixels in
// the argument display rectangle is not necessarily even

static void dstripeIntYUV_8(void *v, int iJob);
static void dstripeIntYUV_10(void *v, int iJob);

static void dstripeIntRGB_8(void *v, int iJob);
static void dstripeIntRGB_8_LOG(void *v, int iJob);

static void dstripeIntRGB_10(void *v, int iJob);
static void dstripeIntRGB_10_LOG(void *v, int iJob);
static void dstripeIntRGB_10_A1(void *v, int iJob);
static void dstripeIntRGB_10_LOG_A1(void *v, int iJob);
static void dstripeIntRGB_10_A2(void *v, int iJob);
static void dstripeIntRGB_10_LOG_A2(void *v, int iJob);
static void dstripeIntRGB_10_A8(void *v, int iJob);
static void dstripeIntRGB_10_LOG_A8(void *v, int iJob);
static void dstripeIntRGB_10_A16(void *v, int iJob);
static void dstripeIntRGB_10_LOG_A16(void *v, int iJob);
static void dstripeIntRGB_10_A10(void *v, int iJob);
static void dstripeIntRGB_10_LOG_A10(void *v, int iJob);

static void dstripeIntRGB_12(void *v, int iJob);
static void dstripeIntRGB_12_LOG(void *v, int iJob);

static void dstripeIntRGB_16(void *v, int iJob);
static void dstripeIntRGB_16_LOG(void *v, int iJob);
static void dstripeIntRGB_16_A16(void *v, int iJob);
static void dstripeIntRGB_16_LOG_A16(void *v, int iJob);

static void dstripeIntRGB_Half(void *v, int iJob);

static void dstripeIntMON_10(void *v, int iJob);
static void dstripeIntMON_10_LOG(void *v, int iJob);
static void dstripeIntMON_16(void *v, int iJob);

/////////////////////////////////////////////////////////////////////
//
//    d i s p l a y C o n v e r t I n t e r m e d i a t e
//
void CConvert::
displayConvertIntermediate(
        MTI_UINT16 *srcbuf,          // -> src intermediate
        const CImageFormat *srcfmt,  // -> src image format
        RECT *srcrect,               // -> subrectangle to be displayed
        unsigned int *dstbuf,        // -> RGBA dst buffer
        int dstwdth,                 // wdth  of dst bitmap
        int dsthght,                 // hght  of dst bitmap
        int dstpitch,                // pitch of dst bitmap (bytes)
        unsigned char *lutptr,       // -> external LUT
        unsigned int rgbmask,        // top byte == 0xff => gray
        unsigned int pnmask,         // pos-neg mask
        unsigned char alpha          // alpha value
	)
{
   // save the mask (incl gray flag)
   rgbMask = rgbmask;

   // save the POS-NEG mask
   pnMask = pnmask;

   // save the ALPHA value
	alphaVal = alpha;

	// Not the compare frame.
	_isWiperComparand = false;
	_comparandDividerPositionPerRow = nullptr;

   // if a non-null LUT ptr is passed in, use it
   rgbtbl = RGBTbl;
   linear8 = Linear8;
   linear16 = Linear16;
   antilog8 = AntiLog8;
   antilog16 = AntiLog16;
	antihalf = AntiHalf;
   if (lutptr != NULL)
      rgbtbl = linear8 = linear16 = antilog8 = antilog16 = antihalf = lutptr;

   // resume as before
   double step, run;
   int intrun;

   dstpitch /= 4;

   // extract the following from the src image-format
   int srcwdth          = srcfmt->getPixelsPerLine();
   int srchght          = srcfmt->getLinesPerFrame();
   int srcpitch         = srcfmt->getFramePitch();
   int srccolorspace    = srcfmt->getColorSpace();
   int srcpelcomponents = srcfmt->getPixelComponents();
   int srcpelpacking    = srcfmt->getPixelPacking();
   EAlphaMatteType srcAlphaMatteType = srcfmt->getAlphaMatteType();

   // continue as before
   curSrcInt = srcbuf;
   curDstBuf = dstbuf;

   int winwdth = srcrect->right - srcrect->left + 1;
   int winhght = srcrect->bottom - srcrect->top + 1;

   if (
        (srcwdth != curSrcWdth)
      ||(srchght != curSrcHght)
      ||(srcpitch != curSrcPitch)
      ||(srccolorspace != curSrcColorSpace)
      ||(srcpelcomponents != curSrcPelComponents)
      ||(srcpelpacking    != curSrcPelPacking)
      ||(srcAlphaMatteType != curAlphaMatteType)
      ||(dstwdth != curDstWdth)
      ||(dsthght != curDstHght)
      ||(dstpitch != curDstPitch)
      ||(srcrect->left   != curSrcRect.left)
      ||(srcrect->top    != curSrcRect.top)
      ||(srcrect->right  != curSrcRect.right)
      ||(srcrect->bottom != curSrcRect.bottom)
      ||(curConvertType  != IntermediateDisplayConvert)

       ) { // change control tables

      ///////////////////////////////////////////////////
      //
      // generate col table

      step = (double)winwdth/(double)dstwdth;
      run = 0.5*step;
      for (int i=0;i<dstwdth;i++) {
		 xstrTbl[i] = (int)run;
		 run += step;
      }

      ///////////////////////////////////////////////////
      //
      // generate row table and select stripe

      switch(srcpelcomponents) {

         case IF_PIXEL_COMPONENTS_YUV422:
         case IF_PIXEL_COMPONENTS_YUV444:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeIntYUV_8;
                  break;

               case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_DVS:
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeIntYUV_10;
                  break;

               default:
                  break;
            }

         break;

         case IF_PIXEL_COMPONENTS_RGB:
         case IF_PIXEL_COMPONENTS_BGR:
         case IF_PIXEL_COMPONENTS_RGBA:
         case IF_PIXEL_COMPONENTS_BGRA:
         case IF_PIXEL_COMPONENTS_Y: // GOES HERE BECAUSE INTERNAL FORMAT HAS 3 COMPONENTS!!
         case IF_PIXEL_COMPONENTS_YYY:

            switch(srcpelpacking) {

               case IF_PIXEL_PACKING_8Bits_IN_1Byte:
                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeIntRGB_8;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
						 (void (*)(void *,int))&dstripeIntRGB_8_LOG;
                  }
                  break;

               case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_L:
               case IF_PIXEL_PACKING_1_10Bits_IN_2Bytes_R:
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:

                  switch (srcAlphaMatteType)
                  {
                     case IF_ALPHA_MATTE_1_BIT:
                     {
                        // 1-bit alpha channel is present
                        tsThread.PrimaryFunction = (void(*)(void *, int)) & dstripeIntRGB_10_A1;
                        if (srccolorspace == IF_COLOR_SPACE_LOG)
                        {
                           tsThread.PrimaryFunction = (void(*)(void *, int)) & dstripeIntRGB_10_LOG_A1;
                        }

                        break;
                     }
                     case IF_ALPHA_MATTE_2_BIT_EMBEDDED:
                     {
                        // 2-bit alpha channel is present
                        tsThread.PrimaryFunction = (void(*)(void *, int)) & dstripeIntRGB_10_A2;
                        if (srccolorspace == IF_COLOR_SPACE_LOG)
                        {
                           tsThread.PrimaryFunction = (void(*)(void *, int)) & dstripeIntRGB_10_LOG_A2;
                        }

                        break;
                     }
                     case IF_ALPHA_MATTE_8_BIT:
                     {
                        //  8-bit alpha channel is present
                        tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeIntRGB_10_A8;
                        if (srccolorspace == IF_COLOR_SPACE_LOG) {
                           tsThread.PrimaryFunction =
                               (void (*)(void *,int))&dstripeIntRGB_10_LOG_A8;
                        }

                        break;
                     }
                     case IF_ALPHA_MATTE_16_BIT:
                     {
                        // 16-bit alpha channel is present
                        tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeIntRGB_10_A16;
                        if (srccolorspace == IF_COLOR_SPACE_LOG) {
                           tsThread.PrimaryFunction =
                               (void (*)(void *,int))&dstripeIntRGB_10_LOG_A16;
                        }

                        break;
                     }
                     case IF_ALPHA_MATTE_10_BIT_INTERLEAVED:
                     {
                        // 10-bit alpha channel is present
                        tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeIntRGB_10_A10;
                        if (srccolorspace == IF_COLOR_SPACE_LOG) {
                           tsThread.PrimaryFunction =
                               (void (*)(void *,int))&dstripeIntRGB_10_LOG_A10;
                        }

                        break;
                     }
                     default:
                     {
                        // alpha channel is absent
                        tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeIntRGB_10;
                        if (srccolorspace == IF_COLOR_SPACE_LOG) {
                           tsThread.PrimaryFunction =
                               (void (*)(void *,int))&dstripeIntRGB_10_LOG;
                        }

                        break;
                     }
                  }

                  break;

               case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes:
               case IF_PIXEL_PACKING_2_12Bits_IN_3Bytes_LR:
               case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_BE:
               case IF_PIXEL_PACKING_1_12Bits_IN_2Bytes_L_LE:

                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeIntRGB_12;
                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
                     tsThread.PrimaryFunction =
                         (void (*)(void *,int))&dstripeIntRGB_12_LOG;
                  }

                  break;

					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
						if (srcAlphaMatteType == IF_ALPHA_MATTE_16_BIT_INTERLEAVED)
						{
							tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
																	? (void (*)(void *,int))&dstripeIntRGB_16_LOG_A16
																	: (void (*)(void *,int))&dstripeIntRGB_16_A16;
						}
						else
						{
							tsThread.PrimaryFunction = (srccolorspace == IF_COLOR_SPACE_LOG)
																	? (void (*)(void *,int))&dstripeIntRGB_16_LOG
																	: (void (*)(void *,int))&dstripeIntRGB_16;
						}

						break;

               case IF_PIXEL_PACKING_1_Half_IN_2Bytes:
						tsThread.PrimaryFunction = (void (*)(void *,int))&dstripeIntRGB_Half;
                  break;

               default:
                  break;
            }

         break;

//         case IF_PIXEL_COMPONENTS_LUMINANCE:
//         // NOT IF_PIXEL_COMPONENTS_Y, internal format is 3 components!
//
//            switch(srcpelpacking) {
//
//               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_L:
//               case IF_PIXEL_PACKING_3_10Bits_IN_4Bytes_R:
//               case IF_PIXEL_PACKING_4_10Bits_IN_5Bytes:
//                  tsThread.PrimaryFunction   = (void (*)(void *,int))&dstripeIntMON_10;
//                  if (srccolorspace == IF_COLOR_SPACE_LOG) {
//                     tsThread.PrimaryFunction =
//                         (void (*)(void *,int))&dstripeIntMON_10_LOG;
//                  }
//
//					case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_BE:
//               case IF_PIXEL_PACKING_1_16Bits_IN_2Bytes_LE:
//                  tsThread.PrimaryFunction = (void (*)(void *,int))&dstripeIntMON_16;
//
//                  break;
//
//               default:
//                  break;
//            }
//
//         break;

      }

      step = (double)winhght/(double)dsthght;
      run = (double) srcrect->top + .5*step;

      // luminance intermediate has only one component per pel
      // NOT IF_PIXEL_COMPONENTS_Y, internal format is 3 components!
//      if (srcpelcomponents == IF_PIXEL_COMPONENTS_LUMINANCE) {
//
//         for (int i=0;i<dsthght;i++) {
//            intrun = (int)run;
//            ystrTbl[i].offs = (srcwdth * intrun + srcrect->left);
//            run += step;
//         }
//      }
//      else {
//
         for (int i=0;i<dsthght;i++) {
            intrun = (int)run;
            ystrTbl[i].offs = 3*(srcwdth * intrun + srcrect->left);
            run += step;
         }
//      }

      //////////////////////////////////////////////////////////
      //
      // set current values
      //
      curSrcWdth          = srcwdth;
      curSrcHght          = srchght;
      curSrcPitch         = srcpitch;
      curSrcColorSpace    = srccolorspace;
      curSrcPelComponents = srcpelcomponents;
      curSrcPelPacking    = srcpelpacking;
      curAlphaMatteType   = srcAlphaMatteType;
      curWinWdth          = winwdth;
      curWinHght          = winhght;
      curDstWdth          = dstwdth;
      curDstHght          = dsthght;
      curDstPitch         = dstpitch;
      curSrcRect          = *srcrect;
      curConvertType      = IntermediateDisplayConvert;
   } // change control tables

   /////////////////////////////////////////////////////////////
   //
   // without the mthreads we would call the functions serially:
#ifdef NO_MULTI
   for (int  i = 0; i < nStripes; i++)
   {
      tsThread.PrimaryFunction(this,i);
   }
#else
   // now allocate, run, and free the mthreads
   if (nStripes > 1)
   {
      int iRet = MThreadAlloc(&tsThread);
      if (iRet)
      {
            TRACE_0(errout << "Convert: MThreadAlloc failed, error=" << iRet);
      }
      else
      {
         iRet = MThreadStart(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadStart failed, error=" << iRet);
         }

         iRet = MThreadFree(&tsThread);
         if (iRet)
         {
            TRACE_0(errout << "Convert: MThreadFree failed, error=" << iRet);
         }
      }
   }
   else // nStripes <= 0
   {
      // If we're only doing one stripe, call the function directly.
      tsThread.PrimaryFunction(this,0);
   }
#endif
}

static void dstripeIntYUV_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   MTI_UINT16 *presrcw = NULL;

   for (int i=begRow;i<endRow;i++) {

       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);

      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw!=presrcw) { // convert one row

         presrcw = srcw;

         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];
         unsigned char *rt = (unsigned char *)vp->rgbtbl;
         unsigned char *lut;

         for (int j=0;j<vp->curWinWdth;j++) {

            lut = rt+3*(((srcw[1]<<14)&msku)+((srcw[2]<<7)&mskv)+((srcw[0])&msky));
            *dst++ = lut[2];
            *dst++ = lut[1];
            *dst++ = lut[0];
            *dst++ = alpha;

            srcw += 3;
         }
      }

      // output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}

static void dstripeIntYUV_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   MTI_UINT16 *presrcw = NULL;

   for (int i=begRow;i<endRow;i++) {

       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);

      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw!=presrcw) { // convert one row

         presrcw = srcw;

         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];
         unsigned char *rt = (unsigned char *)vp->rgbtbl;
         unsigned char *lut;

         for (int j=0;j<vp->curWinWdth;j++) {

            lut = rt+3*(((srcw[1]<<12)&msku)+((srcw[2]<<5)&mskv)+((srcw[0]>>2)&msky));
            *dst++ = lut[2];
            *dst++ = lut[1];
            *dst++ = lut[0];
            *dst++ = alpha;

            srcw += 3;
         }
      }

      // output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}

static void dstripeIntRGB_8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8+256;
   unsigned char *linearB = vp->linear8+512;

   MTI_UINT16 *presrcw = NULL;

   for (int i=begRow;i<endRow;i++) {

       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);

      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw!=presrcw) { // convert one row

         presrcw = srcw;

         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

         for (int j=0;j<vp->curWinWdth;j++) {

            *dst++ = linearR[srcw[0]];
            *dst++ = linearG[srcw[1]];
            *dst++ = linearB[srcw[2]];
            *dst++ = alpha;

            srcw += 3;
         }
      }

      // output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}

static void dstripeIntRGB_8_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8+256;
   unsigned char *antilogB = vp->antilog8+512;

   MTI_UINT16 *presrcw = NULL;

   for (int i=begRow;i<endRow;i++) {

       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);

      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw!=presrcw) { // convert one row

         presrcw = srcw;

         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

         for (int j=0;j<vp->curWinWdth;j++) {

            *dst++ = antilogR[srcw[0]];
            *dst++ = antilogG[srcw[1]];
            *dst++ = antilogB[srcw[2]];
            *dst++ = alpha;

            srcw += 3;
         }
      }

		// output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}

static void dstripeIntRGB_10(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert*) v;

	unsigned char alpha = vp->alphaVal;
	unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

	unsigned char *linearR = vp->linear8;
	unsigned char *linearG = vp->linear8 + 256;
	unsigned char *linearB = vp->linear8 + 512;

	MTI_UINT16 *presrcw = NULL;

	for (int i = begRow; i < endRow; i++)
	{
		unsigned int *rgb = (vp->curDstBuf) + i * (vp->curDstPitch);
		MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

		if (srcw != presrcw)
		{
			// convert one row
			presrcw = srcw;

			unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

			for (int j = 0; j < vp->curWinWdth; j++)
			{
				*dst++ = linearR[srcw[0] >> 2];
				*dst++ = linearG[srcw[1] >> 2];
				*dst++ = linearB[srcw[2] >> 2];
				*dst++ = alpha;

				srcw += 3;
			}
		}

		// output the converted row
		if (vp->_isWiperComparand)
		{
			rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
		}
		else
		{
			rowRGB(rgb, row, vp);
		}
	} // for each row of stripe
KCATCH
}

static void dstripeIntRGB_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;

   MTI_UINT16 *presrcw = NULL;

   for (int i=begRow;i<endRow;i++) {

       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);

      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw!=presrcw) { // convert one row

         presrcw = srcw;

         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

         for (int j=0;j<vp->curWinWdth;j++) {

            *dst++ = antilogR[srcw[0]>>2];
            *dst++ = antilogG[srcw[1]>>2];
            *dst++ = antilogB[srcw[2]>>2];
            *dst++ = alpha;

            srcw += 3;
         }
      }

      // output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}

static void dstripeIntRGB_16(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert *) v;

	unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

	unsigned char *linearR = vp->linear8;
	unsigned char *linearG = vp->linear8 + 256;
	unsigned char *linearB = vp->linear8 + (2 * 256);

	MTI_UINT8 *presrcb = NULL;

	for (int i = begRow; i < endRow; ++i)
	{
		 unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);
		MTI_UINT8 *srcb = (MTI_UINT8 *)(vp->curSrcInt + vp->ystrTbl[i].offs);

		// convert one row
		if (srcb != presrcb)
		{
			presrcb = srcb;
			unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

			if (vp->curSrcPelComponents == IF_PIXEL_COMPONENTS_YYY
			|| vp->curSrcPelComponents == IF_PIXEL_COMPONENTS_Y)
			{
				for (int j = 0; j < vp->curWinWdth; ++j)
				{
               dst[0] = dst[1] = dst[2] =
                  (MTI_UINT8) (((MTI_UINT32) linearR[srcb[1]]
                                + (MTI_UINT32) linearG[srcb[3]]
                                + (MTI_UINT32) linearB[srcb[5]])
                                          / 3);
               dst[3] = alpha;

               srcb += 6;
					dst += 4;
            }
         }
         else
         {
            for (int j = 0; j < vp->curWinWdth; ++j)
            {
               dst[0] = linearR[srcb[1]];
               dst[1] = linearG[srcb[3]];
					dst[2] = linearB[srcb[5]];
               dst[3] = alpha;

               srcb += 6;
               dst += 4;
            }
         }
      }

		// output the converted row
		rowRGB(rgb, row, vp);

	} // for each row of stripe
KCATCH
}

static void dstripeIntRGB_16_A16(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert *) v;

	unsigned char alpha = vp->alphaVal;
	int alph2 = ((int)alpha)<<24;

	unsigned char *linearR = vp->linear8;
	unsigned char *linearG = vp->linear8 + 256;
	unsigned char *linearB = vp->linear8 + 512;

	MTI_UINT16 *curSrcInt = vp->curSrcInt;

	// the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
	MTI_UINT16 *alphaMap = curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3);
	MTI_UINT16 *alphasrcw;

	ROWENTRY *ystrTbl = vp->ystrTbl;
	unsigned char *dst, *alphadst;

	int rowsPerStripe = vp->curDstHght / vp->nStripes;
	int begRow = iJob*rowsPerStripe;
	int endRow = begRow + rowsPerStripe;
	if (iJob == (vp->nStripes-1))
		endRow = vp->curDstHght;

	unsigned char *rgb;
	rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
	unsigned int rgbadv   = 4*vp->curDstPitch;

	unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
	unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

	MTI_UINT16 *presrcw = NULL;

	unsigned int alphaColor = vp->alphaColor;
	if (alphaColor&0xffffff) { // alpha display is turned on

		unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
		int rowOffset;

		for (int i=begRow;i<endRow;i++) {

			rowOffset = ystrTbl[i].offs; // offset in words to pel

			MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

			rowOffset /= 3;
			alphasrcw = alphaMap + rowOffset; // 1st word in alpha map

			if (srcw != presrcw) { // convert one full row of source

				presrcw = srcw;

				dst     = row;

				alphadst = alpharow;

				while (dst < rowend) {

					unsigned int foo = *alphasrcw & 0xFF00;
					if (foo != 0xFF00 && foo != 0x0000)
					{
						*(int *)dst      = alph2;
						*(int *)alphadst = alphaColor;
					}
					else
					{
						unsigned char *srcb = (unsigned char *)srcw;
						dst[0] = linearR[srcb[1]];
						dst[1] = linearG[srcb[3]];
						dst[2] = linearB[srcb[5]];
						dst[3] = alpha;

                  *(int *)alphadst = 0;
					}

					srcw += 3;
					alphasrcw++;
					dst += 4;
					alphadst += 4;
				}
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			// ...then ADD the alpha onto it
			rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
		}
	}
	else {                     // alpha display is off

		for (int i=begRow;i<endRow;i++) {

			MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

			if (srcw != presrcw) { // convert one full row of source

				presrcw = srcw;

            dst     = row;

				while (dst < rowend) {

					unsigned char *srcb = (unsigned char *)srcw;
					dst[0] = linearR[srcb[1]];
					dst[1] = linearG[srcb[3]];
					dst[2] = linearB[srcb[5]];
					dst[3] = alpha;

					srcw += 3;
					dst += 4;
				}
			}

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;
		}
	}
KCATCH
}

static void dstripeIntRGB_16_LOG(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert *) v;

	unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *antilogR = vp->antilog8;
	unsigned char *antilogG = vp->antilog8 + 256;
	unsigned char *antilogB = vp->antilog8 + 512;

   MTI_UINT8 *presrcb = NULL;

   for (int i = begRow; i < endRow; i++)
   {
       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);
      MTI_UINT8 *srcb = (MTI_UINT8 *) (vp->curSrcInt + vp->ystrTbl[i].offs);

      if (srcb != presrcb)
      {
         // convert one row
         presrcb = srcb;
         unsigned char *dst = (unsigned char *) vp->RGBrow[iJob];

         if (vp->curSrcPelComponents == IF_PIXEL_COMPONENTS_YYY
         || vp->curSrcPelComponents == IF_PIXEL_COMPONENTS_Y)
         {
            for (int j = 0; j < vp->curWinWdth; ++j)
            {
               dst[0] = dst[1] = dst[2] = ((MTI_UINT32) antilogR[srcb[1]]
                                           + (MTI_UINT32) antilogG[srcb[3]]
                                           + (MTI_UINT32) antilogB[srcb[5]])
                                          / 3;
               dst[3] = alpha;

               srcb += 6;
               dst += 4;
            }
         }
         else
         {
            for (int j = 0; j < vp->curWinWdth; ++j)
            {
               dst[0] = antilogR[srcb[1]];
               dst[1] = antilogG[srcb[3]];
               dst[2] = antilogB[srcb[5]];
               dst[3] = alpha;

               srcb += 6;
               dst += 4;
            }
         }
      }

      // output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}

static void dstripeIntRGB_16_LOG_A16(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert *) v;

	unsigned char alpha = vp->alphaVal;
	int alph2 = ((int)alpha)<<24;

	unsigned char *antilogR = vp->antilog8;
	unsigned char *antilogG = vp->antilog8 + 256;
	unsigned char *antilogB = vp->antilog8 + 512;

	// R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
	//unsigned char *lut = vp->linear8;
	MTI_UINT16 *curSrcInt = vp->curSrcInt;

	// the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
	MTI_UINT16 *alphaMap = curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3);
	MTI_UINT16 *alphasrcw;

	ROWENTRY *ystrTbl = vp->ystrTbl;
	unsigned char *dst, *alphadst;

	int rowsPerStripe = vp->curDstHght / vp->nStripes;
	int begRow = iJob*rowsPerStripe;
	int endRow = begRow + rowsPerStripe;
	if (iJob == (vp->nStripes-1))
		endRow = vp->curDstHght;

   unsigned char *rgb;
	rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
	unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
	unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

			rowOffset = ystrTbl[i].offs; // offset in words to pel

         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         rowOffset /= 3;
         alphasrcw = alphaMap + rowOffset; // 1st word in alpha map

         if (srcw != presrcw) { // convert one full row of source

				presrcw = srcw;

            dst     = row;

            alphadst = alpharow;

				while (dst < rowend) {

					unsigned int foo = *alphasrcw & 0xFF00;
					if (foo != 0xFF00 && foo != 0x0000) {

						*(int *)dst      = alph2;
                  *(int *)alphadst = alphaColor;
					}
					else {

						unsigned char *srcb = (unsigned char *)srcw;
						dst[0] = antilogR[srcb[1]];
						dst[1] = antilogG[srcb[3]];
						dst[2] = antilogB[srcb[5]];
						dst[3] = alpha;

                  *(int *)alphadst = 0;
					}
               srcw += 3;
               alphasrcw++;
					dst += 4;
               alphadst += 4;
            }
			}

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
			rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
		}
   }
   else {                     // alpha display is off

      for (int i=begRow;i<endRow;i++) {

			MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw) { // convert one full row of source

				presrcw = srcw;

				dst     = row;

            while (dst < rowend) {

					unsigned char *srcb = (unsigned char *)srcw;
					dst[0] = antilogR[srcb[1]];
					dst[1] = antilogG[srcb[3]];
					dst[2] = antilogB[srcb[5]];
					dst[3] = alpha;

					srcw += 3;
               dst += 4;
            }
			}

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;
		}
   }
KCATCH
}

static void dstripeIntRGB_Half(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *halfLutR = vp->antihalf;
	unsigned char *halfLutG = vp->antihalf + 65536;
	unsigned char *halfLutB = vp->antihalf + 131072;
   MTI_UINT16 *presrcw = NULL;

   for (int i = begRow; i < endRow; i++)
   {
       unsigned int *rgb = vp->curDstBuf + (i * vp->curDstPitch);
      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw != presrcw)
      {
         // convert one row
         presrcw = srcw;
         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

         for (int j = 0; j < vp->curWinWdth; j++)
         {
				dst[0] = halfLutR[srcw[0]];  // R
				dst[1] = halfLutG[srcw[1]];  // G
				dst[2] = halfLutB[srcw[2]];  // B
				dst[3] = alpha;                                   // A

            srcw += 3;
            dst += 4;
         }
      }

		// output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}

static void dstripeIntRGB_10_A1(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   int alphaShiftedLeft24 = ((int)alpha) << 24;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;
   MTI_UINT16 *curSrcInt = vp->curSrcInt;

   // the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
   MTI_UINT8 *alphaMap = (unsigned char *)(curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3));

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst, *alphadst;

   int rowsPerStripe = vp->curDstHght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = vp->curDstHght;

   unsigned char *rgb;
   rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff)
   {
      // alpha display is turned on
      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      for (int i=begRow;i<endRow;i++)
      {
         int rowOffset = ystrTbl[i].offs; // offset in words to pel
         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         const int AlphaBitsPerPixel = 1;
         const int AlphaBitsMask = (1 << AlphaBitsPerPixel) - 1;
         const int ComponentsPerPixel = 3;
         const int BitsPerByte = 8;
         const int AlphasPerByte = BitsPerByte / AlphaBitsPerPixel;

         int alphaPixelOffset = rowOffset / ComponentsPerPixel;
         int alphaByteOffset = alphaPixelOffset / AlphasPerByte;
         int alphaShift = AlphaBitsPerPixel * (alphaPixelOffset & (AlphasPerByte - 1));

         unsigned char *alphasrc = alphaMap + alphaByteOffset;
         unsigned char alphamsk = (unsigned char) (AlphaBitsMask << alphaShift);

         if (srcw != presrcw)
         {
            // convert one full row of source
            presrcw = srcw;
            dst     = row;
            alphadst = alpharow;
            while (dst < rowend)
            {
               if ((*alphasrc) & alphamsk)
               {
                  *(int *)dst = alphaShiftedLeft24;
                  *(int *)alphadst = alphaColor;
               }
               else
               {
                  dst[0] = lut[srcw[0] >> 2];
                  dst[1] = lut[256 + (srcw[1] >> 2)];
                  dst[2] = lut[512 + (srcw[2] >> 2)];
                  dst[3] = alpha;

                  *(int *)alphadst = 0;
               }

               srcw += 3;

               alphamsk <<= AlphaBitsPerPixel;
               if (alphamsk == 0)
               {
                  alphamsk = AlphaBitsMask;
                  alphasrc++;
               }

               dst += 4;
               alphadst += 4;
            }
         }

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
      }
   }
   else
   {
      // alpha display is off
      for (int i = begRow; i < endRow; i++)
      {
         MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw)
         {
            // convert one full row of source
            presrcw = srcw;
            dst = row;

            while (dst < rowend)
            {
               dst[0] = lut[srcw[0] >> 2];
               dst[1] = lut[256 + (srcw[1] >> 2)];
               dst[2] = lut[512 + (srcw[2] >> 2)];
               dst[3] = alpha;

               srcw += 3;
               dst += 4;
            }
         }

         // output the converted row
         if (vp->_isWiperComparand)
         {
            rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
         }
         else
         {
            rowRGB(rgb, row, vp);
         }

         rgb += rgbadv;
      }
   }
KCATCH
}

static void dstripeIntRGB_10_LOG_A1(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   int alphaShiftedLeft24 = ((int)alpha) << 24;

   unsigned char *lutR = vp->antilog8;
   unsigned char *lutG = vp->antilog8 + 256;
   unsigned char *lutB = vp->antilog8 + 512;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   //unsigned char *lut = vp->antilog8;
   MTI_UINT16 *curSrcInt = vp->curSrcInt;

   // the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
   MTI_UINT8 *alphaMap = (unsigned char *)(curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3));

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst, *alphadst;

   int rowsPerStripe = vp->curDstHght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = vp->curDstHght;

   unsigned char *rgb;
   rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff)
   {
      // alpha display is turned on
      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      for (int i=begRow;i<endRow;i++)
      {
         int rowOffset = ystrTbl[i].offs; // offset in words to pel
         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         const int AlphaBitsPerPixel = 2;
         const int AlphaBitsMask = 0x03;
         const int ComponentsPerPixel = 3;
         const int BitsPerByte = 8;
         const int AlphasPerByte = BitsPerByte / AlphaBitsPerPixel;

         int alphaOffset = rowOffset / ComponentsPerPixel;
         int alphaByteOffset = alphaOffset / AlphasPerByte;
         int alphaShift = AlphaBitsPerPixel * (alphaOffset & (( 1 << AlphaBitsPerPixel) - 1));

         unsigned char *alphasrc = alphaMap + (alphaOffset / AlphasPerByte);
         unsigned char alphamsk = (unsigned char) (AlphaBitsMask << alphaShift);

         if (srcw != presrcw)
         {
            // convert one full row of source
            presrcw = srcw;
            dst     = row;
            alphadst = alpharow;
            while (dst < rowend)
            {
               if ((*alphasrc) & alphamsk)
               {
                  *(int *)dst = alphaShiftedLeft24;
                  *(int *)alphadst = alphaColor;
               }
               else
               {
                  dst[0] = lutR[srcw[0] >> 2];
                  dst[1] = lutG[srcw[1] >> 2];
                  dst[2] = lutB[srcw[2] >> 2];
                  dst[3] = alpha;

                  *(int *)alphadst = 0;
               }

               srcw += 3;

               alphamsk <<= AlphaBitsPerPixel;
               if (alphamsk == 0)
               {
                  alphamsk = AlphaBitsMask;
                  alphasrc++;
               }

               dst += 4;
               alphadst += 4;
            }
         }

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
      }
   }
   else
   {
      // alpha display is off
      for (int i = begRow; i < endRow; i++)
      {
         MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw)
         {
            // convert one full row of source
            presrcw = srcw;
            dst = row;

            while (dst < rowend)
            {
               dst[0] = lutR[srcw[0] >> 2];
               dst[1] = lutG[srcw[1] >> 2];
               dst[2] = lutB[srcw[2] >> 2];
               dst[3] = alpha;

               srcw += 3;
               dst += 4;
            }
         }

         // output the converted row
         if (vp->_isWiperComparand)
         {
            rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
         }
         else
         {
            rowRGB(rgb, row, vp);
         }

         rgb += rgbadv;
      }
   }
KCATCH
}

// NOTE: THIS IS IDENTICAL TO dstripeIntRGB_10_LOG_A2
// except for the line:
// unsigned char *lut = vp->linear8;
static void dstripeIntRGB_10_A2(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char alphaVal = vp->alphaVal;
   int alphaValShifted24 = ((int)alphaVal) << 24;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->linear8;

   MTI_UINT16 *curSrcInt = vp->curSrcInt;

   // the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
   MTI_UINT8 *alphaMap = (unsigned char *)(curSrcInt + (vp->curSrcWdth * vp->curSrcHght * 3));

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst, *alphadst;

   int rowsPerStripe = vp->curDstHght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
	  endRow = vp->curDstHght;

   unsigned char *rgb;
   rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor & 0xffffff)
   {
      // alpha display is turned on
      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i = begRow; i < endRow; i++)
      {
         rowOffset = ystrTbl[i].offs; // offset in words to pel
         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         const int AlphaBitsPerPixel = 2;
         const int AlphaBitsMask = (1 << AlphaBitsPerPixel) - 1;
         const int ComponentsPerPixel = 3;
         const int BitsPerByte = 8;
         const int AlphasPerByte = BitsPerByte / AlphaBitsPerPixel;

         int alphaPixelOffset = rowOffset / ComponentsPerPixel;
         int alphaByteOffset = alphaPixelOffset / AlphasPerByte;
         int alphaShift = AlphaBitsPerPixel * (alphaPixelOffset & (AlphasPerByte - 1));

         unsigned char *alphasrc = alphaMap + alphaByteOffset;
         unsigned char alphamsk = (unsigned char) (AlphaBitsMask << alphaShift);

         if (srcw != presrcw)
         {
            // convert one full row of source
            presrcw = srcw;
            dst = row;
            alphadst = alpharow;

            while (dst < rowend)
            {
               if ((*alphasrc) & alphamsk)
               {
                  *(int *)dst = alphaValShifted24;
                  *(int *)alphadst = alphaColor;
               }
               else
               {
                  dst[0] = lut[srcw[0] >> 2];
                  dst[1] = lut[256 + (srcw[1] >> 2)];
                  dst[2] = lut[512 + (srcw[2] >> 2)];
                  dst[3] = alphaVal;

                  *(int *)alphadst = 0;
               }

               srcw += 3;

               alphamsk <<= AlphaBitsPerPixel;
               if (alphamsk == 0x00)
               {
                  alphamsk = AlphaBitsMask;
                  alphasrc++;
               }

               dst += 4;
               alphadst += 4;
            }
         }

         // output the converted row
         if (vp->_isWiperComparand)
         {
            rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
         }
         else
         {
            rowRGB(rgb, row, vp);
         }

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

         rgb += rgbadv;
      }
   }
   else
   {

      for (int i=begRow;i<endRow;i++) {

         MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

            dst     = row;

            while (dst < rowend) {

               dst[0] = lut[srcw[0] >> 2];
               dst[1] = lut[256 + (srcw[1] >> 2)];
               dst[2] = lut[512 + (srcw[2] >> 2)];
               dst[3] = alphaVal;

               srcw += 3;
               dst += 4;
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         rgb += rgbadv;
      }
   }
KCATCH
}

// NOTE: THIS IS IDENTICAL TO dstripeIntRGB_10_A2
// except for the line:
// unsigned char *lut = vp->antilog8;
static void dstripeIntRGB_10_LOG_A2(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char alphaVal = vp->alphaVal;
   int alphaValShifted24 = ((int)alphaVal) << 24;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *lut = vp->antilog8;

   MTI_UINT16 *curSrcInt = vp->curSrcInt;

   // the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
   MTI_UINT8 *alphaMap = (unsigned char *)(curSrcInt + (vp->curSrcWdth * vp->curSrcHght * 3));

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst, *alphadst;

   int rowsPerStripe = vp->curDstHght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
	  endRow = vp->curDstHght;

   unsigned char *rgb;
   rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor & 0xffffff)
   {
      // alpha display is turned on
      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i = begRow; i < endRow; i++)
      {
         rowOffset = ystrTbl[i].offs; // offset in words to pel
         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         const int AlphaBitsPerPixel = 2;
         const int AlphaBitsMask = (1 << AlphaBitsPerPixel) - 1;
         const int ComponentsPerPixel = 3;
         const int BitsPerByte = 8;
         const int AlphasPerByte = BitsPerByte / AlphaBitsPerPixel;

         int alphaPixelOffset = rowOffset / ComponentsPerPixel;
         int alphaByteOffset = alphaPixelOffset / AlphasPerByte;
         int alphaShift = AlphaBitsPerPixel * (alphaPixelOffset & (AlphasPerByte - 1));

         unsigned char *alphasrc = alphaMap + alphaByteOffset;
         unsigned char alphamsk = (unsigned char) (AlphaBitsMask << alphaShift);

         if (srcw != presrcw)
         {
            // convert one full row of source
            presrcw = srcw;
            dst = row;
            alphadst = alpharow;

            while (dst < rowend)
            {
               if ((*alphasrc) & alphamsk)
               {
                  *(int *)dst = alphaValShifted24;
                  *(int *)alphadst = alphaColor;
               }
               else
               {
                  dst[0] = lut[srcw[0] >> 2];
                  dst[1] = lut[256 + (srcw[1] >> 2)];
                  dst[2] = lut[512 + (srcw[2] >> 2)];
                  dst[3] = alphaVal;

                  *(int *)alphadst = 0;
               }

               srcw += 3;

               alphamsk <<= AlphaBitsPerPixel;
               if (alphamsk == 0)
               {
                  alphamsk = AlphaBitsMask;
                  alphasrc++;
               }

               dst += 4;
               alphadst += 4;
            }
         }

         // output the converted row
         if (vp->_isWiperComparand)
         {
            rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
         }
         else
         {
            rowRGB(rgb, row, vp);
         }

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

         rgb += rgbadv;
      }
   }
   else
   {
      // alpha display is off
      for (int i = begRow; i < endRow; i++)
      {
         MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;
         if (srcw != presrcw)
         {
            // convert one full row of source
            presrcw = srcw;
            dst = row;
            while (dst < rowend)
            {
               dst[0] = lut[srcw[0] >> 2];
               dst[1] = lut[256 + (srcw[1] >> 2)];
               dst[2] = lut[512 + (srcw[2] >> 2)];
               dst[3] = alphaVal;

               srcw += 3;
               dst += 4;
            }
         }

         // output the converted row
         if (vp->_isWiperComparand)
         {
            rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
         }
         else
         {
            rowRGB(rgb, row, vp);
         }

         rgb += rgbadv;
      }
   }
KCATCH
}

static void dstripeIntRGB_10_A8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   int alph2 = ((int)alpha)<<24;

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   //unsigned char *lut = vp->linear8;
   MTI_UINT16 *curSrcInt = vp->curSrcInt;

   // the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
   MTI_UINT8 *alphaMap = (unsigned char *)(curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3));
   MTI_UINT8 *alphasrc;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst, *alphadst;

   int rowsPerStripe = vp->curDstHght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = vp->curDstHght;

   unsigned char *rgb;
   rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in words to pel

         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         rowOffset /= 3;
         alphasrc = alphaMap + rowOffset; // 1st byte in alpha map

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

            dst     = row;

            alphadst = alpharow;

            while (dst < rowend) {

               if (*alphasrc) {

                  *(int *)dst      = alph2;
                  *(int *)alphadst = alphaColor;
               }
               else {

                  dst[0] = linearR[srcw[0]>>2];
                  dst[1] = linearG[srcw[1]>>2];
                  dst[2] = linearB[srcw[2]>>2];
                  dst[3] = alpha;

                  *(int *)alphadst = 0;
               }
               srcw += 3;
               alphasrc++;
               dst += 4;
               alphadst += 4;
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
      }
   }
   else {                     // alpha display is off

      for (int i=begRow;i<endRow;i++) {

         MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

            dst     = row;

            while (dst < rowend) {

               dst[0] = linearR[srcw[0]>>2];
               dst[1] = linearG[srcw[1]>>2];
               dst[2] = linearB[srcw[2]>>2];
               dst[3] = alpha;

               srcw += 3;
               dst += 4;
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;
      }
   }
KCATCH
}

static void dstripeIntRGB_10_LOG_A8(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   int alph2 = ((int)alpha)<<24;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;

   MTI_UINT16 *curSrcInt = vp->curSrcInt;

   // the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
   MTI_UINT8 *alphaMap = (unsigned char *)(curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3));
   MTI_UINT8 *alphasrc;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst, *alphadst;

   int rowsPerStripe = vp->curDstHght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = vp->curDstHght;

   unsigned char *rgb;
   rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in words to pel

         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         rowOffset /= 3;
         alphasrc = alphaMap + rowOffset; // 1st byte in alpha map

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

            dst     = row;

            alphadst = alpharow;

            while (dst < rowend) {

               if (*alphasrc) {

                  *(int *)dst      = alph2;
						*(int *)alphadst = alphaColor;
               }
               else {

                  dst[0] = antilogR[srcw[0]>>2];
                  dst[1] = antilogG[srcw[1]>>2];
                  dst[2] = antilogB[srcw[2]>>2];
                  dst[3] = alpha;

                  *(int *)alphadst = 0;
               }
               srcw += 3;
               alphasrc++;
               dst += 4;
               alphadst += 4;
            }
         }

			// output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
      }
   }
   else {                     // alpha display is off

      for (int i=begRow;i<endRow;i++) {

         MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

            dst     = row;

            while (dst < rowend) {

               dst[0] = antilogR[srcw[0]>>2];
               dst[1] = antilogG[srcw[1]>>2];
               dst[2] = antilogB[srcw[2]>>2];
               dst[3] = alpha;

               srcw += 3;
               dst += 4;
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;
      }
   }
KCATCH
}

static void dstripeIntRGB_10_A16(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert *) v;

	unsigned char alpha = vp->alphaVal;
	int alph2 = ((int)alpha)<<24;

	unsigned char *linearR = vp->linear8;
	unsigned char *linearG = vp->linear8 + 256;
	unsigned char *linearB = vp->linear8 + 512;

	// R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   //unsigned char *lut = vp->linear8;
   MTI_UINT16 *curSrcInt = vp->curSrcInt;

   // the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
	MTI_UINT16 *alphaMap = curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3);
	MTI_UINT16 *alphasrcw;

	ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst, *alphadst;

	int rowsPerStripe = vp->curDstHght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
	if (iJob == (vp->nStripes-1))
      endRow = vp->curDstHght;

	unsigned char *rgb;
	rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = 4*vp->curDstPitch;

	unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
	unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

	MTI_UINT16 *presrcw = NULL;

	unsigned int alphaColor = vp->alphaColor;
	if (alphaColor&0xffffff) { // alpha display is turned on

		unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

		for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in words to pel

         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

			rowOffset /= 3;
			alphasrcw = alphaMap + rowOffset; // 1st word in alpha map

			if (srcw != presrcw) { // convert one full row of source

				presrcw = srcw;

				dst     = row;

				alphadst = alpharow;

				while (dst < rowend) {

               if (*alphasrcw) { // only has to be non-zero

                  *(int *)dst      = alph2;
						*(int *)alphadst = alphaColor;
               }
               else {

                  dst[0] = linearR[srcw[0]>>2];
						dst[1] = linearG[srcw[1]>>2];
                  dst[2] = linearB[srcw[2]>>2];
                  dst[3] = alpha;

                  *(int *)alphadst = 0;
					}
               srcw += 3;
               alphasrcw++;
               dst += 4;
               alphadst += 4;
				}
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
		}
   }
   else {                     // alpha display is off

      for (int i=begRow;i<endRow;i++) {

         MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw) { // convert one full row of source

				presrcw = srcw;

            dst     = row;

            while (dst < rowend) {

               dst[0] = linearR[srcw[0]>>2];
               dst[1] = linearG[srcw[1]>>2];
               dst[2] = linearB[srcw[2]>>2];
               dst[3] = alpha;

               srcw += 3;
               dst += 4;
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;
		}
	}
KCATCH
}

static void dstripeIntRGB_10_LOG_A16(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert *) v;

	unsigned char alpha = vp->alphaVal;
	int alph2 = ((int)alpha)<<24;

	unsigned char *antilogR = vp->antilog8;
	unsigned char *antilogG = vp->antilog8 + 256;
	unsigned char *antilogB = vp->antilog8 + 512;

	// R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
	//unsigned char *lut = vp->linear8;
	MTI_UINT16 *curSrcInt = vp->curSrcInt;

	// the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
	MTI_UINT16 *alphaMap = curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3);
	MTI_UINT16 *alphasrcw;

	ROWENTRY *ystrTbl = vp->ystrTbl;
	unsigned char *dst, *alphadst;

	int rowsPerStripe = vp->curDstHght / vp->nStripes;
	int begRow = iJob*rowsPerStripe;
	int endRow = begRow + rowsPerStripe;
	if (iJob == (vp->nStripes-1))
		endRow = vp->curDstHght;

   unsigned char *rgb;
	rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
	unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
	unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

			rowOffset = ystrTbl[i].offs; // offset in words to pel

         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         rowOffset /= 3;
         alphasrcw = alphaMap + rowOffset; // 1st word in alpha map

         if (srcw != presrcw) { // convert one full row of source

				presrcw = srcw;

            dst     = row;

            alphadst = alpharow;

				while (dst < rowend) {

               if (*alphasrcw) { // only has to be non-zero

                  *(int *)dst      = alph2;
                  *(int *)alphadst = alphaColor;
					}
               else {

						dst[0] = antilogR[srcw[0]>>2];
                  dst[1] = antilogG[srcw[1]>>2];
                  dst[2] = antilogB[srcw[2]>>2];
						dst[3] = alpha;

                  *(int *)alphadst = 0;
					}
               srcw += 3;
               alphasrcw++;
					dst += 4;
               alphadst += 4;
            }
			}

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
			rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
		}
   }
   else {                     // alpha display is off

      for (int i=begRow;i<endRow;i++) {

			MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

				dst     = row;

            while (dst < rowend) {

               dst[0] = antilogR[srcw[0]>>2];
               dst[1] = antilogG[srcw[1]>>2];
					dst[2] = antilogB[srcw[2]>>2];
               dst[3] = alpha;

					srcw += 3;
               dst += 4;
            }
			}

         // output the converted row
			rowRGB(rgb, row, vp);

			rgb += rgbadv;
		}
   }
KCATCH
}

static void dstripeIntRGB_10_A10(void *v, int iJob)
{
KTRY
	CConvert *vp = (CConvert *) v;

	unsigned char alpha = vp->alphaVal;
	int alph2 = ((int)alpha)<<24;

	unsigned char *linearR = vp->linear8;
	unsigned char *linearG = vp->linear8 + 256;
	unsigned char *linearB = vp->linear8 + 512;

	// R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   //unsigned char *lut = vp->linear8;
   MTI_UINT16 *curSrcInt = vp->curSrcInt;

   // the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
   MTI_UINT16 *alphaMap = curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3);
   MTI_UINT16 *alphasrcw;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst, *alphadst;

   int rowsPerStripe = vp->curDstHght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = vp->curDstHght;

   unsigned char *rgb;
   rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
   if (alphaColor&0xffffff) { // alpha display is turned on

      unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
      int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in words to pel

         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         rowOffset /= 3;
         alphasrcw = alphaMap + rowOffset; // 1st word in alpha map

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

            dst     = row;

            alphadst = alpharow;

            while (dst < rowend) {

               // for ARRI format the presence of dirt is
               // IFF top 2 bits of the 10-bit value are set

               if (*alphasrcw >= 0x300) {

                  *(int *)dst      = alph2;
                  *(int *)alphadst = alphaColor;
               }
               else {

                  dst[0] = linearR[srcw[0]>>2];
                  dst[1] = linearG[srcw[1]>>2];
                  dst[2] = linearB[srcw[2]>>2];
                  dst[3] = alpha;

                  *(int *)alphadst = 0;
               }
               srcw += 3;
               alphasrcw++;
               dst += 4;
               alphadst += 4;
            }
         }

			// output the converted row
			rowRGB(rgb, row, vp);

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
      }
   }
   else {                     // alpha display is off

      for (int i=begRow;i<endRow;i++) {

         MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

            dst     = row;

            while (dst < rowend) {

               dst[0] = linearR[srcw[0]>>2];
               dst[1] = linearG[srcw[1]>>2];
               dst[2] = linearB[srcw[2]>>2];
               dst[3] = alpha;

               srcw += 3;
               dst += 4;
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         rgb += rgbadv;
      }
   }
KCATCH
}

static void dstripeIntRGB_10_LOG_A10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   int alph2 = ((int)alpha)<<24;

   // R is 1st 256 bytes of LUT, G is 2nd, B is 3rd
   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;

   MTI_UINT16 *curSrcInt = vp->curSrcInt;

   // the Alpha map's RIGHT AFTER the RGB image; set the ptr to it
   MTI_UINT16 *alphaMap = curSrcInt + (vp->curSrcWdth * vp->curSrcHght*3);
   MTI_UINT16 *alphasrcw;

   ROWENTRY *ystrTbl = vp->ystrTbl;
   unsigned char *dst, *alphadst;

   int rowsPerStripe = vp->curDstHght / vp->nStripes;
   int begRow = iJob*rowsPerStripe;
   int endRow = begRow + rowsPerStripe;
   if (iJob == (vp->nStripes-1))
      endRow = vp->curDstHght;

   unsigned char *rgb;
   rgb = (unsigned char *)(vp->curDstBuf + begRow*vp->curDstPitch);
   unsigned int rgbadv   = 4*vp->curDstPitch;

   unsigned char *row    = (unsigned char *)vp->RGBrow[iJob];
   unsigned char *rowend = (unsigned char *)(vp->RGBrow[iJob] + vp->curWinWdth);

   MTI_UINT16 *presrcw = NULL;

   unsigned int alphaColor = vp->alphaColor;
	if (alphaColor&0xffffff) { // alpha display is turned on

		unsigned char *alpharow = (unsigned char *)vp->ALPHArow[iJob];
		int rowOffset;

      for (int i=begRow;i<endRow;i++) {

         rowOffset = ystrTbl[i].offs; // offset in words to pel

         MTI_UINT16 *srcw = vp->curSrcInt + rowOffset;

         rowOffset /= 3;
         alphasrcw = alphaMap + rowOffset; // 1st word in alpha map

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

            dst     = row;

            alphadst = alpharow;

            while (dst < rowend) {

               // for ARRI format the presence of dirt is
               // IFF top 2 bits of the 10-bit value are set

               if (*alphasrcw >= 0x300) {

                  *(int *)dst      = alph2;
						*(int *)alphadst = alphaColor;
               }
               else {

                  dst[0] = antilogR[srcw[0]>>2];
                  dst[1] = antilogG[srcw[1]>>2];
                  dst[2] = antilogB[srcw[2]>>2];
                  dst[3] = alpha;

                  *(int *)alphadst = 0;
               }
               srcw += 3;
               alphasrcw++;
               dst += 4;
               alphadst += 4;
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

         // ...then ADD the alpha onto it
         rowALPHA(rgb, alpharow, vp);

			rgb += rgbadv;
      }
   }
   else {                     // alpha display is off

      for (int i=begRow;i<endRow;i++) {

         MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

         if (srcw != presrcw) { // convert one full row of source

            presrcw = srcw;

            dst     = row;

            while (dst < rowend) {

               dst[0] = antilogR[srcw[0]>>2];
               dst[1] = antilogG[srcw[1]>>2];
               dst[2] = antilogB[srcw[2]>>2];
               dst[3] = alpha;

               srcw += 3;
               dst += 4;
            }
         }

         // output the converted row
			if (vp->_isWiperComparand)
			{
				rowRGBwithLimit(rgb, row, vp, vp->_comparandDividerPositionPerRow[i]);
			}
			else
			{
				rowRGB(rgb, row, vp);
			}

			rgb += rgbadv;
      }
   }
KCATCH
}

///////////////////////////////////////////////////////////////

static void dstripeIntRGB_12(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *linearR = vp->linear8;
   unsigned char *linearG = vp->linear8 + 256;
   unsigned char *linearB = vp->linear8 + 512;

   MTI_UINT16 *presrcw = NULL;

   for (int i=begRow;i<endRow;i++) {

       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);

      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw!=presrcw) { // convert one row

         presrcw = srcw;

         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

         for (int j=0;j<vp->curWinWdth;j++) {

            *dst++ = linearR[srcw[0]>>4];
            *dst++ = linearG[srcw[1]>>4];
            *dst++ = linearB[srcw[2]>>4];
            *dst++ = alpha;

            srcw += 3;
         }
      }

      // output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}

static void dstripeIntRGB_12_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *antilogR = vp->antilog8;
   unsigned char *antilogG = vp->antilog8 + 256;
   unsigned char *antilogB = vp->antilog8 + 512;

   MTI_UINT16 *presrcw = NULL;

   for (int i=begRow;i<endRow;i++) {

       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);

      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw!=presrcw) { // convert one row

         presrcw = srcw;

         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

         for (int j=0;j<vp->curWinWdth;j++) {

            *dst++ = antilogR[srcw[0]>>4];
            *dst++ = antilogG[srcw[1]>>4];
            *dst++ = antilogB[srcw[2]>>4];
            *dst++ = alpha;

            srcw += 3;
         }
      }

      // output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}

///////////////////////////////////////////////////////////////

static void dstripeIntMON_10(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;

   unsigned int rgbMask = vp->rgbMask;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   unsigned char *linear = vp->linear8;
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   MTI_UINT16 *presrcw = NULL;

   for (int i=begRow;i<endRow;i++) {

       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);
       unsigned int r0;

      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw!=presrcw) { // convert one row

         presrcw = srcw;

         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

         for (int j=0;j<vp->curWinWdth;j++) {

            r0 = linear[srcw[0]>>2];
            *dst++ = r0;
            *dst++ = r0;
            *dst++ = r0;
            *dst++ = alpha;

            srcw++;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      if (rgbMask&0x00ffffff) {
         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
            *rgb++ = irow[*xstrTblPtr];
         }
      }
      else {
         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
            *rgb++ = 0;
         }
      }
   } // for each row of stripe
KCATCH
}

static void dstripeIntMON_10_LOG(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;

   unsigned char alpha = vp->alphaVal;

   unsigned int rgbMask = vp->rgbMask;
   int *xstrTbl    = vp->xstrTbl;
   int *xstrTblEnd = (vp->xstrTbl + vp->curDstWdth);
   unsigned char *antilog = vp->antilog8;
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   MTI_UINT16 *presrcw = NULL;

   for (int i=begRow;i<endRow;i++) {

       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);
       unsigned int r0;

      MTI_UINT16 *srcw = vp->curSrcInt + vp->ystrTbl[i].offs;

      if (srcw!=presrcw) { // convert one row

         presrcw = srcw;

         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

         for (int j=0;j<vp->curWinWdth;j++) {

            r0 = antilog[srcw[0]>>2];
            *dst++ = r0;
            *dst++ = r0;
            *dst++ = r0;
            *dst++ = alpha;

            srcw++;
         }
      }

      // now output one row of the finished stripe
      unsigned int *irow = vp->RGBrow[iJob];
      if (rgbMask&0x00ffffff) {
         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
            *rgb++ = irow[*xstrTblPtr];
         }
      }
      else {
         for (int *xstrTblPtr = xstrTbl;xstrTblPtr<xstrTblEnd;xstrTblPtr++) {
            *rgb++ = 0;
         }
      }

   } // for each row of stripe
KCATCH
}

static void dstripeIntMON_16(void *v, int iJob)
{
KTRY
   CConvert *vp = (CConvert *) v;
   unsigned char alpha = vp->alphaVal;
   unsigned int *row = vp->RGBrow[iJob];
   int begRow, endRow;
   computeSliceRowsFromJob(vp->curDstHght, iJob, vp->nStripes, begRow, endRow);

   unsigned char *linear = vp->linear8;
   unsigned char *presrc = NULL;

   for (int i = begRow; i < endRow; ++i)
   {
       unsigned int *rgb = (vp->curDstBuf) + i*(vp->curDstPitch);
      unsigned char *src = (unsigned char *)(vp->curSrcInt + vp->ystrTbl[i].offs);

      // convert one row
      if (src != presrc)
      {
         presrc = src;
         unsigned char *dst = (unsigned char *)vp->RGBrow[iJob];

         for (int j = 0; j < vp->curWinWdth; ++j)
         {
            dst[0] = dst[1] = dst[2] = linear[src[1]];

            src += 2;
            dst += 4;
         }
      }

      // output the converted row
		rowRGB(rgb, row, vp);

   } // for each row of stripe
KCATCH
}
