/*
	File:	LUTManager.h
	By:	Kevin Manbeck
	Date:	December 2003

	The LUT Manager contains a static list of LUTs for converting
	image data to an 8-bit display.

	A LUT must be allocated and released by the calling application.
	A LUT which has been released by the caller may remain in RAM
	in case the caller needs it again at a later date.

*/

#ifndef LutManagerH
#define LutManagerH

#include "ImageInfo.h"
#include "ImageFormat3.h"
#include "formatDLL.h"
#include "mthread.h"

#define TYPE_NONE 0
#define TYPE_YUV 1
#define TYPE_8BIT 2
#define TYPE_10BIT 3
#define TYPE_16BIT 4
#define TYPE_12BIT 5
#define TYPE_16BIT_INT 6
#define TYPE_HALF 7

// the INDEX into the RGB table is assembled from:
//
// the 7 most-significant bits of U in bits 15-21
#define msku 0x3f8000
// the 7 most-significant bits of V in bits 8-14
#define mskv 0x007f00
// the 8 most-significant bits of Y in bits 0-7
#define msky 0x0000ff

class CColorSpaceConvert;

class MTI_FORMATDLL_API CDisplayFormat
{
public:
  CDisplayFormat ();
  ~CDisplayFormat ();

  float
    fDisplayGamma,
    faDisplayMin[3],    // scale is 0 to 1
    faDisplayMax[3];    // scale is 0 to 1

  bool
    bSourceLogSpace;
};

class MTI_FORMATDLL_API CDisplayLUT
{
public:
  CDisplayLUT ();
  ~CDisplayLUT ();

  int Init (const CImageFormat &newImageFormat,
		const CDisplayFormat &newDisplayFormat);

  bool IsEqual (const CImageFormat &newImageFormat,
	const CDisplayFormat &newDisplayFormat);

  void Convert (MTI_UINT8 *ucpSrc, MTI_UINT8 *ucpDst);
  MTI_UINT8 *getTablePtr();

  int
    iUsageCount, // number of callers currently using this LUT
    iStaleCount; // number of times this LUT has not been used

private:
  CImageFormat
    ifSrc;	 // the image format of the image data

  CDisplayFormat
    dfDst;

  int
    iLUTType;	 // see TYPE_* for values.

public:

  int
    iBitsPerComponent; // 8 or 10, for YUV only

  MTI_UINT8
    *ucpLUT;

  int nStripes;  // degree of multithreading

  CColorSpaceConvert *csc[8];

  MTHREAD_STRUCT tsThread;

};

class MTI_FORMATDLL_API CLUTManager
{
public:
  CLUTManager ();
  ~CLUTManager ();

  CDisplayLUT *Create (const CImageFormat &newImageFormat,
	               const CDisplayFormat &newDisplayFormat);
  void Release (const CDisplayLUT *lut);
  void Destroy ();

private:
  static vector <CDisplayLUT *> vectLUT;

  void ControlStale ();
  void IncrementStaleCount ();
  bool RemoveStale ();
};

#endif
