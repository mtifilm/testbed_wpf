object Form1: TForm1
  Left = 192
  Top = 107
  Width = 965
  Height = 640
  Caption = 'Image Formats'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object FormatListView: TListView
    Left = 0
    Top = 0
    Width = 953
    Height = 613
    Columns = <
      item
        AutoSize = True
        Caption = 'Image Format'
      end
      item
        AutoSize = True
        Caption = 'VTR?'
      end
      item
        AutoSize = True
        Caption = 'Description 1'
      end
      item
        AutoSize = True
        Caption = 'Description 2'
      end>
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    GridLines = True
    HideSelection = False
    ReadOnly = True
    ParentFont = False
    TabOrder = 0
    ViewStyle = vsReport
  end
end
