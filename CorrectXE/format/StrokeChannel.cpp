#include "StrokeChannel.h"
#include "MTImalloc.h"
#include "IniFile.h"
#include "ThreadLocker.h"
#include <stdlib.h>
#include <stdio.h>
#if defined(__sgi) || defined(__linux)
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#elif _WIN32
#include <time.h>      // time_t is broken in sys/types.h in RadStudio XE4
#include <sys\stat.h>
#include <windows.h>
#endif
#include "MTIio.h"


CStrokeChannel::CStrokeChannel()
{
   // no buffer yet
   pxlBufByts = 0;
   pxlFillPtr = pxlBuf = 0;
   pxlBufStop = 0;

   // channel file not open yet
   pxlFileHandle = -1;

   // file ptr is 0
   pxlFilePtr = 0;

   // no bytes to read
   pxlBytes = 0;
}

CStrokeChannel::~CStrokeChannel()
{
   // QQQ should we automatically perform a close here?
   // closeStrokeChannel();

   MTIassert(pxlFileHandle == -1);

   MTIfree(pxlBuf);
}

// changed to return
//    -1               on failure
//    .PXL file handle on success
int CStrokeChannel::openStrokeChannel(int pxlComponentCount, string& pxlFileName)
{
   int bufbyts = BufferHeaderSizeInBytes + (pxlComponentCount * BufferCapacityInPixels * sizeof(MTI_UINT16));
   int result;

   // save the name of the channel
   pixelFileName = pxlFileName;

   // the component count is now passed in
   pixelComponents = pxlComponentCount;

   // if size of buffer changes, re-allocate it
   bufbyts = (bufbyts+1)&0xfffffffe;
   if (bufbyts != pxlBufByts) {

      MTIfree(pxlBuf);
      pxlBuf = (MTI_UINT16 *)MTImalloc(bufbyts);
      if (pxlBuf == NULL)
         return(-1);
      pxlBufByts = bufbyts;
      pxlBufStop = pxlBuf + pxlBufByts/2;
   }

   // open the file
   if ((pxlFileHandle = open(pixelFileName.c_str(),
                             O_BINARY|O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1)
      return(-1);

   // advance to set up for appending
   pxlFilePtr = lseek64(pxlFileHandle, 0, SEEK_END);
   if (pxlFilePtr == -1)
      return(-1);

   // get ready to receive pixel data
   pxlFillPtr = pxlBuf;

   // success
   return pxlFileHandle;
}

int CStrokeChannel::setStrokeChannelFilePtr(MTI_INT64 newfileptr)
{
   if (pxlFileHandle < 0)
      return -1;

   pxlFilePtr = lseek64(pxlFileHandle, newfileptr, SEEK_SET);

   if (pxlFilePtr != newfileptr) {
      TRACE_0(errout << "ERROR: CStrokeChannel::setStrokeChannelFilePtr: "
                     << "Unable to set file ptr (" << pxlFilePtr << " != "
                     << newfileptr << ")");
      MTIassert(pxlFilePtr == newfileptr);
      return -1;
   }
   return 0;
}

MTI_INT64 CStrokeChannel::getStrokeChannelFilePtr()
{
   return(pxlFilePtr);
}

int CStrokeChannel::openStrokeChannelForRestore(MTI_INT64 chaddr, int chbytes)
{
   int bytcnt, result;

   MTIassert(pxlFileHandle >= 0);

   // the current file ptr for this .RES record
   pxlBegFilePtr = chaddr;

   // the total bytes to process for this .RES record
   pxlBytes = chbytes;

   // end file ptr for this .RES record
   pxlEndFilePtr = pxlBegFilePtr + pxlBytes;

   // read data from .PXL file
   bytcnt = pxlBytes;
   if (bytcnt > pxlBufByts)
      bytcnt = pxlBufByts;

   if (lseek64(pxlFileHandle, pxlBegFilePtr, SEEK_SET) == -1) {

      return(-1);
   }

   result = read(pxlFileHandle, pxlBuf, bytcnt);
   if (result != bytcnt) {

      return(-1);
   }
   if (result != bytcnt) {

     return(-1);
   }

   // init the runPtr for reading runs
   runEndFilePtr = pxlBegFilePtr;

   return(0);
}

MTI_UINT16* CStrokeChannel::getNextRun()
{
   int bytcnt, result;

   runBegFilePtr = runEndFilePtr;

   if (runBegFilePtr == pxlEndFilePtr) // no more runs
      return(NULL);

   MTI_UINT16 *runPtr = pxlBuf + (int)(runBegFilePtr - pxlBegFilePtr)/2;

   // Haha WTF is this??? Can runPtr[2] be NEGATIVE??? QQQ
   // It's KTF - if the first three wds of the run aren't
   // within the buffer, then runPtr[2] is meaningless and
   // mustn't be accessed!!
   if (((runPtr + 3) >= pxlBufStop)||
       ((runPtr + 3 + runPtr[2]*pixelComponents) >= pxlBufStop)) {

      // read data from .PXL file
      int bytes = pxlEndFilePtr - runBegFilePtr;
      if (bytes > pxlBufByts)
         bytes = pxlBufByts;

      // put the pixel region at the beg
      pxlBegFilePtr = runBegFilePtr;
      if (lseek64(pxlFileHandle, pxlBegFilePtr, SEEK_SET) == -1) {

         TRACE_0(errout << "ERROR: History PXL file seek failed because "
                        << strerror(errno));
         return(NULL);
      }
      result = read(pxlFileHandle, pxlBuf, bytes);
      if (result != bytes) {

         TRACE_0(errout << "ERROR: History PXL file read failed because "
                        << strerror(errno));
         return(NULL);
      }
      if (result != bytes) {

         // Technically, this is legal, in practice on Windows it seems to not happen
         TRACE_0(errout << "ERROR: History PXL file short read (expected  "
                        << bytes << ", got " << result << ")");
         return(NULL);
      }

      runPtr = pxlBuf;
   }

   runEndFilePtr = runBegFilePtr + (3 + runPtr[2]*pixelComponents)*sizeof(MTI_UINT16);

   return(runPtr);
}

int CStrokeChannel::putDifferenceRunsToChannel(MTI_UINT16 *iniFrame,
                                              MTI_UINT16 *finFrame,
                                              MTI_UINT16 *srcFrame,
                                              int frmWdth, int frmHght,
                                              bool evn, bool odd)
{
   int result, bytcnt;

   // buffer for collecting one run at a time
   MTI_UINT16 runBuf[32768];

   // open for putting runs to channel
   pxlFillPtr = pxlBuf;

   // first pass derives the extent

   const MTI_UINT16 *iniptr = iniFrame;
   const MTI_UINT16 *finptr = finFrame;
   const MTI_UINT16 *srcptr;

   int totbyts = 0;

   MTI_UINT16 *dstPtr;

   iniptr = iniFrame;
   finptr = finFrame;
   srcptr = srcFrame;

   for (int i=0;i<frmHght;i++) {

      bool oddLine = i&1;
      if ((!oddLine||!odd)&&(oddLine||!evn)) {
         iniptr += frmWdth*pixelComponents;
         finptr += frmWdth*pixelComponents;
         srcptr += frmWdth*pixelComponents;
         continue;
      }

      bool phase = false;

      for (int j=0;j<frmWdth;j++) {

         bool diff = false;
         for (int k=0;k<pixelComponents;k++) {
            diff = (finptr[k] != iniptr[k]);
            if (diff)
               break;
         }

         if (diff) {

            // found a differing pixel;

            if (!phase) {

               // init new run

               runBuf[0] = j;
               runBuf[1] = i;
               runBuf[2] = 0;

               dstPtr = runBuf + 3;

               phase = true;

            }
            if (phase) {

               // add pixel to current run

               for (int k=0;k<pixelComponents;k++)
                  *dstPtr++ = srcptr[k];

               runBuf[2]++;
            }
         }
         else {

            if (phase) {

               // completed run - add it to the channel

               int wds = 3 + runBuf[2] * pixelComponents;

               if ((pxlFillPtr + wds) >= pxlBufStop) { // no room in buffer

                  // write out current runs to channel file
                  bytcnt = (pxlFillPtr - pxlBuf)*2;
                  result = write(pxlFileHandle,(MTI_UINT8 *)pxlBuf, bytcnt);
                  if (result != bytcnt)
                     return(-1);
                  pxlFilePtr += bytcnt;
                  pxlFillPtr = pxlBuf;

                  totbyts += bytcnt;
               }

               MTImemcpy((void *)pxlFillPtr, (void *)runBuf, sizeof(MTI_UINT16)*wds);
               pxlFillPtr += wds;

               // init for next run
               phase = false;
            }
         }

         iniptr += pixelComponents;
         finptr += pixelComponents;
         srcptr += pixelComponents;
      }

      if (phase) {

         // completed run at end of row

         int wds = 3 + runBuf[2] * pixelComponents;

         if ((pxlFillPtr + wds) >= pxlBufStop) { // no room in buffer

            // write out current runs to channel file
            bytcnt = (pxlFillPtr - pxlBuf)*2;
            result = write(pxlFileHandle,(MTI_UINT8 *)pxlBuf, bytcnt);
            if (result != bytcnt)
               return(-1);
            pxlFilePtr += bytcnt;
            pxlFillPtr = pxlBuf;

            totbyts += bytcnt;
         }

         MTImemcpy((void *)pxlFillPtr, (void *)runBuf, sizeof(MTI_UINT16)*wds);
         pxlFillPtr += wds;
      }
   }

   // write out last buffer of runs to channel file
   bytcnt = (pxlFillPtr - pxlBuf)*2;
   result = write(pxlFileHandle,(MTI_UINT8 *)pxlBuf, bytcnt);
   if (result != bytcnt)
      return(-1);
   pxlFilePtr += bytcnt;
   pxlFillPtr = pxlBuf;

   totbyts += bytcnt;

   return(totbyts) ;
}

int CStrokeChannel::getDifferenceRunsFmChannel(int chaddr, int chbytes,
                                              MTI_UINT16 *trg,
                                              int frmWdth, int frmHght)
{
   if (openStrokeChannelForRestore(chaddr, chbytes) == -1)
      return -1;

   int totbyts = 0;

   MTI_UINT16 *runptr;
   while ((runptr = getNextRun()) != NULL) {

      int runwds = runptr[2]*pixelComponents;

      MTI_UINT16 *srcptr = runptr + 3;

      MTI_UINT16 *dstptr = trg + (runptr[1]*frmWdth + runptr[0])*
                           pixelComponents;

      MTImemcpy((void *)dstptr, (void *)srcptr, runwds*2);

      totbyts += 6 + runwds*2;
   }

   return totbyts;
}

int CStrokeChannel::closeStrokeChannel()
{
   int bytcnt, result;

   if (pxlFileHandle >= 0) {

      // check that pixel data is always flushed
      MTIassert(pxlFillPtr == pxlBuf);

      if (close(pxlFileHandle) < 0)
         return -1;

      pxlFileHandle = -1;

      return 0;
   }

   return 0;
}

int CStrokeChannel::closeAndTruncateStrokeChannel()
{
   int retVal = closeStrokeChannel();
   if (retVal != 0) return retVal;
   return(truncate64(pixelFileName.c_str(), pxlFilePtr));
}

