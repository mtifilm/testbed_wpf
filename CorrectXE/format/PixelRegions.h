// PixelRegions.h: interface for the CPixelRegionList and CPixelRegion classes
//
/*
$Header: /usr/local/filmroot/format/include/PixelRegions.h,v 1.2.2.11 2009/10/26 19:58:54 mbraca Exp $
*/
//////////////////////////////////////////////////////////////////////

#ifndef PixelRegionsH
#define PixelRegionsH

#include "machine.h"
#include "time.h"
#include "formatDLL.h"             // Export/Import Defs
#include "MTImalloc.h"
#include <vector>
using std::vector;
#include <new>      // for std::bad_alloc

typedef MTI_UINT32 VEDGINDEX;
#define NULLVEDG (VEDGINDEX) 0xffffffff
#define MAX_LASSO_PTS 3*2048

// define structure for VERTICAL EDGE
//
struct VEDGE {

   VEDGINDEX inxtlex;
   VEDGINDEX ifwd;
   VEDGINDEX ibwd;

   MTI_INT16 xbot;
   MTI_INT16 ybot;
   MTI_INT16 ytop;
};

class CLineEngine;
class CPixelRegion;

//////////////////////////////////////////////////////////////////////
//
// Stupid hack class because it seems that Borland's vector ABORTS THE APP
// when it runs out of memory!! WTF!!
//
class MTI_FORMATDLL_API KindaLikeAVectorOfPixelRegionPtrs
{
public:
    KindaLikeAVectorOfPixelRegionPtrs();
    ~KindaLikeAVectorOfPixelRegionPtrs();
    void clear();
    CPixelRegion *at(size_t atIndex) const;
    size_t size() const ;
    void push_back(CPixelRegion *pixelRegionPtr);
private:
    size_t count;
    struct PieceOfTheList
    {
       size_t capacity;
       size_t remaining;
       size_t firstIndex;
       CPixelRegion *ptrs[1]; // old C trick - variable length array
    };
    vector<PieceOfTheList *> listPieces;
    PieceOfTheList *allocPiece(size_t firstindex);
};

//////////////////////////////////////////////////////////////////////

// Number of components per pixel, assuming RGB or YUV
#define PIXEL_REGION_COMPONENT_COUNT 3

//////////////////////////////////////////////////////////////////////

// Pixel Region Types
enum EPixelRegionType
{
   PIXEL_REGION_TYPE_POINT,   // Point
   PIXEL_REGION_TYPE_RECT,    // Rectangle
   PIXEL_REGION_TYPE_MASK,    // Mask
   PIXEL_REGION_TYPE_LASSO,   // Lasso
   PIXEL_REGION_TYPE_BEZIER   // Bezier

   // NOTE: Make sure to update CPixelRegionList::CopyPixelRegion when
   //       adding new Pixel Region Types here
};

inline bool IsValidPixelRegionType(int typeAsInt)
{
   if (typeAsInt == (int) PIXEL_REGION_TYPE_POINT ||
       typeAsInt == (int) PIXEL_REGION_TYPE_RECT ||
       typeAsInt == (int) PIXEL_REGION_TYPE_MASK ||
       typeAsInt == (int) PIXEL_REGION_TYPE_LASSO ||
       typeAsInt == (int) PIXEL_REGION_TYPE_BEZIER)
   {
      return true;
   }
   return false;
};

//////////////////////////////////////////////////////////////////////
//
// PIXEL CHANNELS may now have an INDEX in the front of them consisting
// of blocks of RESTORE RECORDS. The feature will be used by SaveRestore.cpp

struct RESTORE_RECORD
{

#define ALL_FRAMES -1
      // index within clip of affected frame
      int frameNum;

#define ALL_FIELDS -1
      // index within frame of affected field
      int fieldIndex;

      // field number - 0-(visibleFieldCount-1)
      int fieldNumber;

      // global save number (caller increments)
      int globalSaveNumber;

      // absolute time of save
      // Was time_t, but that's broken (64 bit in WIn64, long in Win32)
      int saveTime;

      // tool code word
#define TOOL_NONE       0
#define TOOL_DRS        1
#define TOOL_AUTOFILTER 2
#define TOOL_FLICKER    3
#define TOO_STABILIZE   4
#define TOOL_SCRATCH    5
#define TOOL_PAINT      6
      int toolCode;

      // a flag word is available
#define IS_INVALID  0x01
#define IS_RESTORED 0x02
      unsigned int restoreFlag;

      // bounding rectangle of save pixels
      RECT extent;

      // offset into .CMD file of command which
      // generated this save
#define NO_CMD -1
      int cmdFileByteOffset;

      // offset into PIXEL DATA file of 16-bit
      // 444 pixel data for affected field
      MTI_UINT64 pxlFileByteOffset;

      // number of bytes of pixel data saved
      // for this restore record
      int pxlByteCount;

};

struct FPF_RESTORE_RECORD
{
   RESTORE_RECORD resRec;

#ifdef _WIN64

   // None of this crap is used, so I don't feel like fixing the padding
   // differences between the 32 and 64 bit compilers. Instead I just pad
   // to the matching size. I tried just taking this stuff out, but that
   // breaks compatibilyt with old Correct.

   char garbage[64];

#else

   /////////////////////////////////////////////////////////////////////////////
   //
   // Supplementary metadata
   //
   float dirtConfidence;

   float fixConfidence;

   int numPixelsSaved;

   unsigned short userID;

   char machineID[32];

   // 112 bytes up to this point

   // 16 bytes for add' metadata
   char padding[16];

#endif

};

// Parent Class for pixel file
class MTI_FORMATDLL_API CPixelChannel
{
public:
   CPixelChannel();
   virtual ~CPixelChannel();

   int expandIndexSection();

   int openPixelChannel(int bufbyts,
                        int pxlComponentCount,
                        string& pxlFileName,
                        bool useIndex = false);

   FPF_RESTORE_RECORD *getRestoreRecord();

   int nextRestoreRecord();

   void setRestoreWritebackFlag(bool);

   int flushSaves();

   int setPixelChannelFilePtr(MTI_INT64 chaddr);

   MTI_INT64 getPixelChannelFilePtr();

   int openPixelChannelForRestore(MTI_INT64 chaddr, int chbytes);

   MTI_UINT16* getNextPixelRegion();

   MTI_UINT16* getNextRun();

   int putDifferenceRunsToChannel(MTI_UINT16 *iniFrame,
                                  MTI_UINT16 *finFrame,
                                  MTI_UINT16 *srcFrame,
                                  int frmWdth, int frmHght,
                                  bool evn, bool odd,
                                  RECT *ext = NULL);

   int putDifferenceRunsToChannel(MTI_UINT16 *iniFrame,
                                  MTI_UINT16 *finFrame,
                                  MTI_UINT16 *srcFrame,
                                  vector <POINT> *blobOutl,
                                  int frmWdth, int frmHght,
                                  bool evn, bool odd,
                                  RECT *ext = NULL);

   int getDifferenceRunsFmChannel(int chaddr, int chbytes,
                                  MTI_UINT16 *trg,
                                  int frmWdth, int frmHght);

   int discardNFixes(int);

   int closePixelChannel();

   int closeAndTruncatePixelChannel();

   MTI_INT32   pixelComponents;

   string      pixelFileName;

   MTI_INT32   pxlFileHandle;

   MTI_INT32   pxlBufByts;
   MTI_UINT16 *pxlBuf;
   MTI_UINT16 *pxlBufStop;
   MTI_UINT16 *pxlFillPtr;

   int resRecCount;

#define RES_BLK_FCTR 1024
   int resBlkFctr;
   int resBufSize;

   bool resWriteBackFlag;

   MTI_INT32 resBufByts;
   FPF_RESTORE_RECORD *resBuf;
   FPF_RESTORE_RECORD *resBufStop;
   FPF_RESTORE_RECORD *resRecPtr;
   MTI_INT64   resFilePtr;

   MTI_INT64   pxlSectionBeg;

   MTI_UINT16 *pxlRegionPtr;
   MTI_UINT16 *pxlRunPtr;
   MTI_INT64   pxlFilePtr;

   MTI_INT64   pxlBegFilePtr;
   MTI_INT64   pxlEndFilePtr;
   MTI_INT32   pxlBytes;

   MTI_INT64  rgnBegFilePtr;
   MTI_INT64  rgnEndFilePtr;

   MTI_INT64  runBegFilePtr;
   MTI_INT64  runEndFilePtr;

   // current point for edgemaking
   int curx;
   int cury;
   MTI_INT32 curoff;

#define VEDGES_INIT 2048
   int  edgcnt;
   VEDGE *edgbuf;

   VEDGINDEX iedgptr;
   VEDGINDEX ilexlst;
   VEDGINDEX iedglst;

	int getIOError() const { return _errno; }

private:

#define ERR_INSUFF_MEMORY -1
   // make sure you have edges for the scan algorithm
   int initRunGeneration(int edges);

   // begin the trace of the outline
   void moveTo(int x, int y);

   // continue the trace of the outline
   void lineTo(int x, int y);

   // sort the edges in lexicographic order
   VEDGINDEX iedgeSort(VEDGINDEX, VEDGINDEX);

   // insert an edge-record into the scan
   // line list between two other edges
   void insertEdge(VEDGINDEX, VEDGINDEX, VEDGINDEX);

   // delete an edge-record from the
   // scan line list
   void deleteEdge(VEDGINDEX);

	void dumpFPFRestoreRecord(FPF_RESTORE_RECORD *fpfResRecPtr);

	int _errno = 0;
   string _oldHistoryName;
};

#define PURE = 0

//////////////////////////////////////////////////////////////////////

// Abstract (well maybe not so abstract!) Base Class for pixel regions
class MTI_FORMATDLL_API CPixelRegion
{
public:
   virtual ~CPixelRegion();

   virtual EPixelRegionType GetType() const PURE;

   virtual int GetPixelComponentCount() const PURE;

   virtual int GetPixelArrayLength() const PURE;
   virtual MTI_UINT16* GetPixelArray() const PURE;
   virtual bool IsPixelArrayPopulated() const PURE;

   virtual void OpenRuns() PURE;
   virtual MTI_UINT16* GetNextRun() PURE;

   virtual int GetLengthOfRuns(bool copyEven, bool copyOdd) PURE;

   virtual int PutRunsToChannel(CPixelChannel *pxlchn,
                                 bool copyEven, bool copyOdd) PURE;

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual MTI_UINT16 *PutRunsToBuffer(MTI_UINT16 *dstptr,
                                 bool copyEven, bool copyOdd) PURE;
   virtual int GetLengthInBuffer(bool copyEven, bool copyOdd) PURE;

   virtual void GetRunsFromBuffer(const MTI_UINT16 *srcptr, int arrayLength) PURE;

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int PutRegionToChannel(CPixelChannel *pxlchn,
                                 bool copyEven, bool copyOdd) PURE;

   virtual void ReplacePixelsFromFrame(const MTI_UINT16 *srcFrame,
                               int frameWidth, int frameHeight,
                               bool copyEven, bool copyOdd) PURE;

   virtual void ReplacePixelsInFrame(const MTI_UINT16 *dstFrame,
                             int frameWidth, int frameHeight,
                             bool copyEven, bool copyOdd) PURE;

   virtual void CopyPixelsFrameToFrame(const MTI_UINT16 *srcFrame,
                               int frameWidth, int frameHeight,
                               MTI_UINT16 *dstFrame,
                               bool copyEven, bool copyOdd) PURE;

   virtual void CopyPixelsToMask(const MTI_UINT16 *dstMask,
                         int frameWidth, int frameHeight,
                         bool copyEven, bool copyOdd,
                         MTI_UINT16 mask) PURE;

   virtual void HighlightRunsInFrameBuffer(CLineEngine *lineEngine) PURE;


protected:

   CPixelRegion();
   CPixelRegion(const CPixelRegion &src);

   // For debugging, save frame dimensions when we are told them, so we
   // can check the runs against them
   static int debugHackFrameWidth, debugHackFrameHeight;


};

//////////////////////////////////////////////////////////////////////

// Parent Class for SINGLE POINT pixel regions
class MTI_FORMATDLL_API CSinglePixelRegion : public CPixelRegion
{
public:
   virtual ~CSinglePixelRegion();

   virtual int GetPixelComponentCount() const;
   virtual EPixelRegionType GetType() const PURE;   // provided by subclass
   virtual int GetPixelArrayLength() const;
   virtual MTI_UINT16* GetPixelArray() const;
   virtual bool IsPixelArrayPopulated() const;

   virtual void OpenRuns();
   virtual MTI_UINT16* GetNextRun();

   virtual int GetLengthOfRuns(bool copyEven, bool copyOdd);

   virtual int PutRunsToChannel(CPixelChannel *pxlchn,
                                 bool copyEven, bool copyOdd);

   virtual int PutRegionToChannel(CPixelChannel *pxlchn,
                                 bool copyEven, bool copyOdd) PURE;

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual MTI_UINT16 *PutRunsToBuffer(MTI_UINT16 *dstptr,
                                 bool copyEven, bool copyOdd);

   virtual int GetLengthInBuffer(bool copyEven, bool copyOdd) PURE;

   virtual void GetRunsFromBuffer(const MTI_UINT16 *srcptr, int arrayLength);

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual void ReplacePixelsFromFrame(const MTI_UINT16 *srcFrame,
                               int frameWidth, int frameHeight,
                               bool copyEven, bool copyOdd);

   virtual void ReplacePixelsInFrame(const MTI_UINT16 *dstFrame,
                             int frameWidth, int frameHeight,
                             bool copyEven, bool copyOdd);

   virtual void CopyPixelsFrameToFrame(const MTI_UINT16 *srcFrame,
                               int frameWidth, int frameHeight,
                               MTI_UINT16 *dstFrame,
                               bool copyEven, bool copyOdd);

   virtual void CopyPixelsToMask(const MTI_UINT16 *dstMask,
                         int frameWidth, int frameHeight,
                         bool copyEven, bool copyOdd,
                         MTI_UINT16 mask);

   virtual void HighlightRunsInFrameBuffer(CLineEngine *lineEngine);

protected:
    CSinglePixelRegion();
    CSinglePixelRegion(const CSinglePixelRegion& srcPixelRegion);
    //      ZZ1. Eliminate 'type' variable
    CSinglePixelRegion(/* EPixelRegionType newType, */ int pxlComponentCount);

   void operator=(const CSinglePixelRegion& srcPixelRegion);

protected:
   //EPixelRegionType type;     ZZ1. Eliminate stored type!

   // int pixelArrayLength;    // array length in  wds - ZZ2. 0, 3, 4 or 6

   //MTI_UINT16 *pixelArray;     // aZZ3. Eliminated, now in-line

   //MTI_UINT16 *currentRun;   ZZ3. Eliminated

   //MTI_UINT16 *endOfRuns;    ZZ3. Eliminated

   //int currentRunLength;     ZZ3. Eliminated

   // ZZ3 added: ORDER OF THESE IS IMPORTANT - needs to look like a run!!
   struct SinglePixelRun
   {
      MTI_INT16 x;
      MTI_INT16 y;
      MTI_UINT16 population;  // ZZ2. 0 if not populated, else 1
      MTI_UINT16 r_or_y;
      MTI_UINT16 g_or_u;
      MTI_UINT16 b_or_v;

      SinglePixelRun()
      : x(0), y(0), population(0), r_or_y(0), g_or_u(0), b_or_v(0)
      {};
   } inlinePixelArray;
   //

   // ZZ2. These could be smaller to free up some room, but still keep
   // memory footprint at 16 bytes!
   MTI_UINT16 pixelComponents;  // component count ZZ2. only really need a byte
   MTI_INT16 currentRunIndex;   // -1 or 0 ZZ3 are we before or at the run

private:

};

//////////////////////////////////////////////////////////////////////

// Parent Class for MULTI POINT pixel regions
class MTI_FORMATDLL_API CMultiPixelRegion : public CPixelRegion
{
public:

   virtual ~CMultiPixelRegion();

   virtual EPixelRegionType GetType() const PURE;   // provided by subclass

   virtual int GetPixelComponentCount() const;

   virtual int GetPixelArrayLength() const;
   virtual MTI_UINT16* GetPixelArray() const;
   virtual bool IsPixelArrayPopulated() const;

   virtual void OpenRuns();
   virtual MTI_UINT16* GetNextRun();

   virtual int GetLengthOfRuns(bool copyEven, bool copyOdd);

   virtual int PutRunsToChannel(CPixelChannel *pxlchn,
                                 bool copyEven, bool copyOdd);

   virtual int PutRegionToChannel(CPixelChannel *pxlchn,
                                 bool copyEven, bool copyOdd) PURE;

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual MTI_UINT16 *PutRunsToBuffer(MTI_UINT16 *dstptr,
                                 bool copyEven, bool copyOdd);

   virtual int GetLengthInBuffer(bool copyEven, bool copyOdd) PURE;

   virtual void GetRunsFromBuffer(const MTI_UINT16 *srcptr, int arrayLength);

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual void ReplacePixelsFromFrame(const MTI_UINT16 *srcFrame,
                               int frameWidth, int frameHeight,
                               bool copyEven, bool copyOdd);

   virtual void ReplacePixelsInFrame(const MTI_UINT16 *dstFrame,
                             int frameWidth, int frameHeight,
                             bool copyEven, bool copyOdd);

   virtual void CopyPixelsFrameToFrame(const MTI_UINT16 *srcFrame,
                               int frameWidth, int frameHeight,
                               MTI_UINT16 *dstFrame,
                               bool copyEven, bool copyOdd);

   virtual void CopyPixelsToMask(const MTI_UINT16 *dstMask,
                         int frameWidth, int frameHeight,
                         bool copyEven, bool copyOdd,
                         MTI_UINT16 mask);

   virtual void HighlightRunsInFrameBuffer(CLineEngine *lineEngine);

protected:
   CMultiPixelRegion();
   CMultiPixelRegion(const CMultiPixelRegion& srcPixelRegion);
   //      ZZ1. Eliminate 'type' variable
   CMultiPixelRegion(/* EPixelRegionType newType, */ int pxlComponentCount);

   void CopyPixelArray(const CMultiPixelRegion& srcPixelRegion);

   void operator=(const CMultiPixelRegion& srcPixelRegion);

protected:
   //EPixelRegionType type;          ZZ1. Eliminate 'type' variable

   int pixelComponents;        // pixel component count

   int pixelArrayLength;       // array length in  wds

   bool pixelArrayIsPopulated; // true if pixel array is populated

   MTI_UINT16 *pixelArray;     // array of packed runs

   MTI_UINT16 *currentRun;

   MTI_UINT16 *endOfRuns;

   int currentRunLength;

private:

};

//////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------
// PointPixelRegion is serialized in history buffer as
//
//   MTI_UINT16 = PIXEL_REGION_TYPE_POINT
//
// followed by
//
//   one run consisting of a single point: (x,y,1)(YUV)

class MTI_FORMATDLL_API CPointPixelRegion : public CSinglePixelRegion
{
public:
   CPointPixelRegion(const POINT& newPoint);
   CPointPixelRegion(const CPointPixelRegion& pixelRegion);
   CPointPixelRegion(const POINT& newPoint, const MTI_UINT16 *srcFrame,
                     int frameWidth,
                     int frameHeight,
                     int pxlComponentCount);
   virtual ~CPointPixelRegion();

   CPointPixelRegion& operator=(const CPointPixelRegion& srcPixelRegion);

   virtual EPixelRegionType GetType() const;

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int GetLengthInBuffer(bool copyEven, bool copyOdd);

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int PutRegionToChannel(CPixelChannel *pxlchn,
                                  bool copyEven, bool copyOdd);

   virtual void HighlightRunsInFrameBuffer(CLineEngine *lineEngine);

   // SPECIFIC To this CPixelRegion subclass!
   POINT GetPoint() const;

   // More efficient new/delete when we are allocating millions of these
   static void* operator new (size_t size);
   static void operator delete (void *p);

private:
    // Don't add any data to this subclass - we have it all in the
    // CSinglePixelRegion parent so we can keep tabs on the size

   // POINT point;        ZZ3 - eliminated - was redundant with the
   //                           x and y in the pixel array

};

// -------------------------------------------------------------------
// RectPixelRegion is serialized in history buffer as
//
//   MTI_UINT16 = PIXEL_REGION_TYPE_RECT
//
// followed by
//
//   MTI_UINT16 left, top, right, and bottom
//
// followed by
//
//   length of runs in wds
//
// followed by
//
//   runs: (x,y,pixel-count)(YUV)(YUV)etc...

class MTI_FORMATDLL_API CRectPixelRegion : public CMultiPixelRegion
{
public:
   CRectPixelRegion(const RECT& newRect);
   CRectPixelRegion(const CRectPixelRegion& srcPixelRegion);
   CRectPixelRegion(const RECT& newRect,const MTI_UINT16 *srcFrame,
                    int frameWidth,
                    int frameHeight,
                    int pxlComponentCount);
   virtual ~CRectPixelRegion();

   virtual EPixelRegionType GetType() const;

   CRectPixelRegion& operator=(const CRectPixelRegion& srcPixelRegion);

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int GetLengthInBuffer(bool copyEven, bool copyOdd);

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int PutRegionToChannel(CPixelChannel *pxlchn,
                         bool copyEven, bool copyOdd);

   virtual void HighlightRunsInFrameBuffer(CLineEngine *lineEngine);

   // SPECIFIC To this CPixelRegion subclass!
   RECT GetRect() const;

private:
   RECT rect;
};

// -------------------------------------------------------------------
// MaskPixelRegion is serialized in history buffer as
//
//   MTI_UINT16 = PIXEL_REGION_TYPE_MASK
//
// followed by
//
//   MTI_UINT16 left, top, right, and bottom
//
// followed by
//
//   length of runs in wds
//
// followed by
//
//   runs: (x,y,pixel-count)(YUV)(YUV)etc...

class MTI_FORMATDLL_API CMaskPixelRegion : public CMultiPixelRegion
{
public:
   CMaskPixelRegion(const RECT& newRect);

   CMaskPixelRegion(const CMaskPixelRegion& srcPixelRegion);

   CMaskPixelRegion(const RECT& newRect, const MTI_UINT16 *newMask,
                    const MTI_UINT16 *srcFrame,
                    int frameWidth,
                    int frameHeight,
                    int pxlComponentCount,
                    int iBitMask);

   CMaskPixelRegion(const RECT& newRect, const MTI_UINT8 *newMask,
                    const MTI_UINT16 *srcFrame,
                    int frameWidth,
                    int frameHeight,
                    int pxlComponentCount,
                    int iBitMask);

   CMaskPixelRegion(const MTI_UINT16 *finfrm,
                    const MTI_UINT16 *inifrm,
                    int frameWidth,
                    int frameHeight,
                    int pxlComponentCount);

   CMaskPixelRegion(CPixelChannel *pxlchn,
                    MTI_UINT64 chaddr, MTI_UINT32 chbytes,
                    int pxlComponentCount);

   virtual ~CMaskPixelRegion();

   CMaskPixelRegion& operator=(const CMaskPixelRegion& srcPixelRegion);

   virtual EPixelRegionType GetType() const;

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int GetLengthInBuffer(bool copyEven, bool copyOdd);

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int PutRegionToChannel(CPixelChannel *pxlchn,
                         bool copyEven, bool copyOdd);

   virtual void HighlightRunsInFrameBuffer(CLineEngine *lineEngine);

   // SPECIFIC To this CPixelRegion subclass!
   RECT GetRect() const;

private:
   RECT rect;
};

// --------------------------------------------------------------------
// LassoPixelRegion is serialized in history buffer as
//
//   MTI_UINT16 = PIXEL_REGION_TYPE_LASSO
//
// followed by
//
//   MTI_UINT16 left, top, right, and bottom
//
// followed by
//
//   count of lasso vertices to follow

// KT-060225 - Bugzilla 2399 - previous value: 2048
#define MAX_LASSO_PTS 3*2048

//
// followed by
//
//   lasso vertices (16-bit coords, explicit closure)
//
// followed by
//
//   length of runs in wds
//
// followed by
//
//   runs: (x,y,pixel-count)(YUV)(YUV)etc...

class MTI_FORMATDLL_API CLassoPixelRegion : public CMultiPixelRegion
{
public:
   CLassoPixelRegion(const POINT *newLasso);

   CLassoPixelRegion(const CLassoPixelRegion& srcPixelRegion);

   CLassoPixelRegion(const POINT *newLasso,const MTI_UINT16 *srcFrame,
                     int frameWidth,
                     int frameHeight,
                     int pxlComponentCount);

   virtual ~CLassoPixelRegion();

   CLassoPixelRegion& operator=(const CLassoPixelRegion& srcPixelRegion);

   virtual EPixelRegionType GetType() const;

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int GetLengthInBuffer(bool copyEven, bool copyOdd);

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int PutRegionToChannel(CPixelChannel *pxlchn,
                         bool copyEven, bool copyOdd);

   virtual void HighlightRunsInFrameBuffer(CLineEngine *lineEngine);

   // SPECIFIC To this CPixelRegion subclass!

   RECT GetRect() const;

   POINT *GetLasso() const;

private:

   RECT rect;

   int vertexCount;

   POINT *lasso;
};

// --------------------------------------------------------------------
// BezierPixelRegion is serialized in history buffer as
//
//   MTI_UINT16 = PIXEL_REGION_TYPE_BEZIER
//
// followed by
//
//   MTI_UINT16 left, top, right, and bottom
//
// followed by
//
//   count of bezier vertices to follow

// this literal must be <= MAX_LASSO_PTS / 3
// KT-060225 - Bugzilla 2399 - previous value: 256
#define MAX_BEZIER_PTS 2048

//
// followed by
//
//   bezier vertices (16-bit coords, explicit closure)
//
// followed by
//
//   length of runs in wds
//
// followed by
//
//   runs: (x,y,pixel-count)(YUV)(YUV)etc...

class MTI_FORMATDLL_API CBezierPixelRegion : public CMultiPixelRegion
{
public:
   CBezierPixelRegion(const BEZIER_POINT *newBezier);

   CBezierPixelRegion(const CBezierPixelRegion& srcPixelRegion);

   CBezierPixelRegion(const BEZIER_POINT *newBezier,const MTI_UINT16 *srcFrame,
                      int frameWidth,
                      int frameHeight,
                      int pxlComponentCount);

   virtual ~CBezierPixelRegion();

   CBezierPixelRegion& operator=(const CBezierPixelRegion& srcPixelRegion);

   virtual EPixelRegionType GetType() const;

#ifdef __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int GetLengthInBuffer(bool copyEven, bool copyOdd);

#endif // __STUFF_NOT_CALLED_BY_ANYBODY__

   virtual int PutRegionToChannel(CPixelChannel *pxlchn,
                         bool copyEven, bool copyOdd);

   virtual void HighlightRunsInFrameBuffer(CLineEngine *lineEngine);

   // SPECIFIC To this CPixelRegion subclass!

   RECT GetRect() const;

   BEZIER_POINT *GetBezier() const;

private:

   RECT rect;

   int vertexCount;

   BEZIER_POINT *bezier;
};

//////////////////////////////////////////////////////////////////////
// CPixelRegionList - a class which encapsulates a list of Pixel Regions
//                    of mixed type

class MTI_FORMATDLL_API CPixelRegionList
{
public:
   CPixelRegionList();
   CPixelRegionList(const CPixelRegionList &srcPixelRegionList);
   virtual ~CPixelRegionList();

   CPixelRegionList& operator=(const CPixelRegionList& srcPixelRegion);

   // Clear all entries from Pixel Region List
   void Clear();
   void ClearWithoutDeletingRegions();  // for mbraca's MOVE hack

   RECT GetBoundingBox() const;         // Get Bounding Box around all entries
   CPixelRegion* GetEntry(int index) const;   // Get pointer to an entry in list
   int GetEntryCount() const;           // Number of entries in list

   bool IsBoundingBoxValid() const;     // Has Bounding Box been set yet

   // Add POINT
   void AddPoint(const POINT& newPoint);

   void Add(const POINT& newPoint,
            const MTI_UINT16 *srcFrame,
            int frameWidth,
            int frameHeight,
            int pxlComponentCount);

   void Add(const CPointPixelRegion& pixelRegion);

   void Add(CPointPixelRegion* pixelRegion);

   // Add RECT
   void AddRect(const RECT& newRect);

   void Add(const RECT& newRect,
            const MTI_UINT16 *srcFrame,
            int frameWidth,
            int frameHeight,
            int pxlComponentCount);

   void Add(const CRectPixelRegion& pixelRegion);

   void Add(CRectPixelRegion* pixelRegion);

   // Add MASK (16-bit)
   void AddMask(const RECT& newRect);

   void Add(const RECT& newRect, const MTI_UINT16 *newMask,
                        const MTI_UINT16 *srcFrame,
                        int frameWidth,
                        int frameHeight,
                        int pxlComponentCount,
                        int iBitMask);
   // Add MASK (8-bit)
   void Add(const RECT& newRect, const MTI_UINT8 *newMask,
                        const MTI_UINT16 *srcFrame,
                        int frameWidth,
                        int frameHeight,
                        int pxlComponentCount,
                        int iBitMask);

   void Add(const CMaskPixelRegion& pixelRegion);

   void Add(CMaskPixelRegion* pixelRegion);

   // Add LASSO
   void AddLasso(const POINT* newLasso);

   void Add(const POINT* newLasso,
                         const MTI_UINT16 *srcFrame,
                         int frameWidth,
                         int frameHeight,
                         int pxlComponentCount);

   void Add(const CLassoPixelRegion& pixelRegion);

   void Add(CLassoPixelRegion* pixelRegion);

   // Add BEZIER
   void AddBezier(const BEZIER_POINT* newBezier);

   void Add(const BEZIER_POINT* newBezier,
                                const MTI_UINT16 *srcFrame,
                                int frameWidth,
                                int frameHeight,
                                int pxlComponentCount);

   void Add(const CBezierPixelRegion& pixelRegion);

   void Add(CBezierPixelRegion* pixelRegion);

   // Append contents of a Pixel Region List on the end of this list
   void Add(const CPixelRegionList& pixelRegionList);

   // Move contents of a Pixel Region List to the end of this list
   // (equivalent to << Add(source), source.Clear() >>, but more efficient)
   void Move(CPixelRegionList& srcPixelRegionList);

   // Replace all of the pixel values in the pixel region list with
   // new values extracted from the caller's frame buffer
   void ReplacePixelsFromFrame(const MTI_UINT16 *srcFrame,
                               int frameWidth, int frameHeight,
                               bool copyEven, bool copyOdd);

   // Copy the pixels identified in the Pixel Region List from the source frame
   // to the destination frame.  Assume source and destination frames have
   // the same pitch
   void CopyPixelsFrameToFrame(const MTI_UINT16 *srcFrame,
                               int frameWidth, int frameHeight,
                               MTI_UINT16 *dstFrame,
                               bool copyEven, bool copyOdd);

   // Make a copy of the pixel region list whose constituent pixel regions con-
   // tain no runs (not even unpopulated ones), but which can be highlighted
   CPixelRegionList* MakeCopyWithNoRuns();

   // Highlight all the runs in all the pixel region members of the
   // given list, in the frame buffer attached to argument line
   // engine
   void HighlightRunsInFrameBuffer(CLineEngine *lineEngine);

   // put region list to channel
   int PutRegionListToChannel(CPixelChannel *pxlchn,
                                             bool evn, bool odd);
private:
   CPixelRegion* CopyPixelRegion(const CPixelRegion& pixelRegion);
   void DeletePixelRegionList();
   void ExpandBoundingBox(const POINT& point);
   void ExpandBoundingBox(const RECT& rect);
   void ExpandBoundingBox(const POINT& point, int runLength);
   void InitBoundingBox();

private:
   //typedef vector<CPixelRegion*> PixelRegionList;
   //PixelRegionList pixelRegionList;
   KindaLikeAVectorOfPixelRegionPtrs listOfPixelRegions;

   bool boundingBoxValid;
   RECT boundingBox;
};

//////////////////////////////////////////////////////////////////////
// Global function prototypes

inline const MTI_UINT16* ConvertFrameXYToPtr(const MTI_UINT16 *frame, int x, int y,
                                             int pitch, int componentCount)
{
   return frame + (y * pitch + x) * componentCount;
}

inline const MTI_UINT16* ConvertMaskXYToPtr(const MTI_UINT16 *mask,
                                            int x, int y, int pitch)
{
   return mask + (y * pitch + x);
}

inline const MTI_UINT8* ConvertMaskXYToPtr(const MTI_UINT8 *mask,
                                            int x, int y, int pitch)
{
   return mask + (y * pitch + x);
}

/*// function to copy data from runs into 16-bit 444 frame buffer,
// conditional on the contents of a (16-bit per pixel) mask
void CopyRunsToFrameBuffer(const MTI_UINT16 *pixelArray,
                           int pixelArrayLength,
                           MTI_UINT16 *dstFrame,
                           int framePitch,
                           const MTI_UINT16 *mask);
*/
// CPixelRegion GetPixelRegionFromBuffer(const MTI_UINT16 *srcptr);
//////////////////////////////////////////////////////////////////////

#endif // #ifndef PIXELREGION_H
