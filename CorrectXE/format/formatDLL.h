/*
   File Name:  formatDLL.h

   This just defines the export/import macros
   if MTI_FORMATDLL is defined, exports are defined
   else imports are defined
*/

#ifndef FORMATDLL_H
#define FORMATDLL_H

//////////////////////////////////////////////////////////////////////
#include "machine.h"

#ifdef _WIN32                  // Window compilers need export/import

// Define Import/Export for windows
#ifdef MTI_FORMATDLL
#define MTI_FORMATDLL_API  __declspec(dllexport)
#else
#define MTI_FORMATDLL_API  __declspec(dllimport)
#endif

//--------------------UNIX---------------------------
#else

// Needs nothing for SGI, UNIX, LINUX
#define MTI_FORMATDLL_API
#endif   // End of UNIX

#endif   // Header file


