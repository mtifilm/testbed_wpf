// MediaFormat.h: interface for the CMediaFormat class.
//
//////////////////////////////////////////////////////////////////////

#ifndef MEDIA_FORMAT_H
#define MEDIA_FORMAT_H

//#include <iostream>
//using std::ostream;
#include "MTIstringstream.h"
#include <string>
using std::string;

#include "machine.h"
#include "formatDLL.h"

//////////////////////////////////////////////////////////////////////
// Forward Declarations

class CIniFile;

//////////////////////////////////////////////////////////////////////
enum EMediaFormatType
{
   MEDIA_FORMAT_TYPE_INVALID = 0,
   MEDIA_FORMAT_TYPE_VIDEO2 = 1,
   MEDIA_FORMAT_TYPE_AUDIO2 = 2,
   MEDIA_FORMAT_TYPE_VIDEO3 = 3,
   MEDIA_FORMAT_TYPE_AUDIO3 = 4
};

//////////////////////////////////////////////////////////////////////

class MTI_FORMATDLL_API CMediaFormat
{
public:
   CMediaFormat(EMediaFormatType newMediaFormatType);
   CMediaFormat(const CMediaFormat& src);
   virtual ~CMediaFormat();

   // Virtual "default constructor"
//   virtual CMediaFormat* construct() const {return new CMediaFormat(this->getMediaFormatType());};
//   virtual CMediaFormat* clone() const {return new CMediaFormat(*this);};
   virtual CMediaFormat* construct() const = 0;
   virtual CMediaFormat* clone() const = 0;
   virtual CMediaFormat& assign(const CMediaFormat& src) = 0;

   // File interface
   virtual int readSection(CIniFile *iniFile, const string& sectionName);
   virtual int readClipInitSection(CIniFile *iniFile,
                                   const string& sectionName);
   virtual int writeSection(CIniFile *iniFile, const string& sectionName);

   // Accessor functions
   virtual int getFieldCount() const;
   virtual bool  getInterlaced() const;
   virtual EMediaFormatType getMediaFormatType() const;

   // Mutator functions
   virtual void setInterlaced(bool newInterlaced);

   // Static query functions
   static const char* queryMediaFormatTypeName(EMediaFormatType mediaFormatType);

   // Validation functions
   virtual int validate() const;

   // Normal serizalization
   virtual bool serialize(ostream &osStream) const;
   virtual bool readFromStream(istream &isStream);

   // Testing functions
   virtual void dump(MTIostringstream& stream) const;      // For debugging

protected:
   void operator=(const CMediaFormat& src);  // Protected because this is an
                                             // abstract base class, only
                                             // concrete subtypes do assignment
   bool compare(const CMediaFormat& rhs) const;

protected:
   static string interlacedKey;

private:
   EMediaFormatType mediaFormatType;
   bool  interlaced;             // true if interlaced, false if progressive
};

//////////////////////////////////////////////////////////////////////

#endif // #ifndef MEDIA_FORMAT3_H
