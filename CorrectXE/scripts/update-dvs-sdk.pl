#!/usr/bin/perl -w

############################################################################
#
# Modification History:
#
#   5/ 5/2009 - mpr - updated to use "." instead of "+" for concatenation
#                     since new perl seems to be more strict.
#   8/ 6/2007 - mpr - updated to show actual location of "SYSTEM32"
#                     directory.
#                   - Added comments relative to all the steps needed
#                     to use this program.
#   5/16/2006 - mpr - Stripped out "1 file(s) copied." message from output
#                   - Added printout of directories
#   5/12/2006 - Updated to copy all subdirectories and added various
#               options.
#  11/17/2005 - Added 'bin' directory & 'driver' directory
#   9/27/2004 - Creation - Michael Russell
#
# Usage:
#   update-dvs-sdk.pl [-n] [-v] dvs_sdk_src dest_dir
#                      -n = show only mode (like cvs)
#                      -v = verbose mode
#
# Typical Usage Steps:
#
#   1) Unpack new DVS SDK into a directory (e.g. C:\DVS\sdk_2_7p64)
#   2) Checkout latest trunk source (e.g. cvs co cpmp_win_2)
#   3) Change to cpmp directory (e.g. cd cpmp)
#   4) Checkout scripts (this script is in this directory, so this is
#      a bit of a chicken and egg issue) with "cvs co scripts"
#   5) Update trunk source with new SDK using this script:
#        scripts\update-dvs-sdk.pl \DVS\sdk_2_7p64 dvs\sdk
#   6) Do build with new files
#   7) Check for new files which might need to be added to cvs & Installer
#   8) Check for old files which might need to be removed from cvs & Installer
#   9) Checkin new and modified files
#  10) Update tag of checked-in files with:
#        cd dvs
#        cvs tag -R dvs_2_7p64
#
############################################################################

use strict;		# Force declaration of all variables
use Getopt::Std;	# For command line argument parsing

##########
# Globals
##########
my($FALSE) = 0;
my($TRUE)  = !($FALSE);
my($SHOW_ONLY_F) = $FALSE;
my($VERBOSE_F)   = $FALSE;
my(@FILES_RESULT);
my(%SRC_ASSOC);
my(%DST_ASSOC);
my(@SRC_LIST);
my(@DST_LIST);
my($SRC_BASE_DIR);
my($DST_BASE_DIR);

##############
# Parse Args
##############
my(%opts);

getopts('nv', \%opts);

if ($opts{'n'}) {
  $SHOW_ONLY_F = $TRUE;
}

if ($opts{'v'}) {
  $VERBOSE_F = $TRUE;
}

$SRC_BASE_DIR = shift;
$DST_BASE_DIR = shift;

if (($SRC_BASE_DIR) eq "" || ($DST_BASE_DIR eq "")) {
  print STDERR "Usage: $0 [-n] [-v] dvs_sdk_src dest_dir\n",
               "       -n = show only mode (like cvs)\n",
               "       -v = verbose mode\n",
               "  e.g. $0 C:\\DVS\\sdk2.7p18b C:\\mike\\M2-Centaurus\\cpmp\n";
  exit;
}

###############################################
# Sanity check for 'development' subdirectory
###############################################
my($sanity_check_src) = $SRC_BASE_DIR . "\\" . "development";
my($sanity_check_dst) = $DST_BASE_DIR . "\\" . "development";

if (!-d $sanity_check_src) {
  print "Expected to find source directory:\n",
        "   $sanity_check_src\n",
	"but didn't.  Either:\n",
        "  1) you misspecified the directory\n",
	"  2) some problem exists with the directory, or\n",
	"  3) the structure of DVS's SDK has changed significantly enough\n",
	"     that this script must be rewritten.  Sorry.\n";
  exit;
}

if (!-d $sanity_check_dst) {
  print "Expected to find destination directory:\n",
        "   $sanity_check_dst\n",
	"but didn't.  Either:\n",
        "  1) you misspecified the directory, or\n",
	"  2) some problem exists with the directory\n";
  exit;
}

##################################################################
#
# Gets All Files - due to recursive issues with perl, this is
#   just here to call the actual recursive routine and get back
#   its final result.
#
##################################################################
sub get_all_files {
  my($file_or_dir) = $_[0];

  my($file) = get_all_files_worker($file_or_dir);
  if ($file ne "") {
    push(@FILES_RESULT, $file);
  }
}

#######################################################################
#
# Gets All Files (worker) - Recursive routine to get all files in a
#   specified directory.  The array @FILES_RESULT is filled with the
#   files.
#
#######################################################################
sub get_all_files_worker {
  my($file_or_dir) = $_[0];

  my(@files);

  if (($file_or_dir eq ".") || ($file_or_dir eq "..")) {  # skip self & parent
    return;
  }
  elsif ($file_or_dir eq "CVS") {                         # skip CVS
    return;
  }
  elsif (!-d $file_or_dir) {				  # if not dir, return
#    print "  sub file = $file_or_dir\n";
    return($file_or_dir);
  }
  else {
    opendir(DIR, $file_or_dir) or die "Can't open '$file_or_dir'";
      @files = readdir(DIR);
    closedir(DIR);

    foreach(@files) {
      next if (($_ eq ".") || ($_ eq ".."));       # skip self & parent
      next if ($_ eq "CVS");                       # skip CVS
      my($full_dir) = $file_or_dir . "\\" . $_;
#      print "  dir      = $_\n";
#      print "  fulldir  = $full_dir\n";
      my($file) = get_all_files_worker($full_dir);
#printf "in file = <$file>\n";
      if ($file ne "") {
        push(@FILES_RESULT, $file);
      }
    }
  }
}

########################
# Get all source files
########################
get_all_files($SRC_BASE_DIR);

foreach(@FILES_RESULT) {
#  printf "src = <%s>\n", $_;
  my($key) = substr($_, length($SRC_BASE_DIR));
#  printf "skey = <%s>\n", $key;
  $SRC_ASSOC{$key} = 1;
  push(@SRC_LIST, $key);
}

#############################
# Get all destination files
#############################
undef(@FILES_RESULT);
get_all_files($DST_BASE_DIR);

foreach(@FILES_RESULT) {
#  printf "dst = <%s>\n", $_;
  my($key) = substr($_, length($DST_BASE_DIR));
#  printf "dkey = <%s>\n", $key;
  $DST_ASSOC{$key} = 1;
  push(@DST_LIST, $key);
}

############################################################################
# Check for files that would be deleted from destination b/c no longer in
#   source
############################################################################
my($printed_hdr_f) = $FALSE;

foreach(@DST_LIST) {
  my($full_dst_fn) = $DST_BASE_DIR . $_;

  if (!defined($SRC_ASSOC{$_})) {
    if (!$printed_hdr_f) {
      printf "Deleting files...\n";
      $printed_hdr_f = $TRUE;
    }
    print "  $full_dst_fn\n";

    # delete file
    my($command) = "del " . &add_quotes($full_dst_fn);
    do_command($command);
  }
}

############################################################################
# Check for files that would be added from source b/c not in destination
############################################################################
$printed_hdr_f = $FALSE;

foreach(@SRC_LIST) {
  my($full_src_fn) = $SRC_BASE_DIR . $_;
  my($full_dst_fn) = $DST_BASE_DIR . $_;

  if (!defined($DST_ASSOC{$_})) {
    if (!$printed_hdr_f) {
      printf "Adding directories and files...\n";
      $printed_hdr_f = $TRUE;
    }

    my($dir_for_file) = substr($full_dst_fn, 0, rindex($full_dst_fn, "\\"));

    # mkdir if needed
    if (!-d $dir_for_file) {
      print "  $dir_for_file\n";
      my($command) = "mkdir " . &add_quotes($dir_for_file);
      do_command($command);
    }  

    print "  $full_dst_fn\n";

    # add (via copy) new file
    my($command) = "copy " . &add_quotes($full_src_fn) . " "
                       . &add_quotes($full_dst_fn)
		       . " | find /v " . &add_quotes("1 file(s) copied.");
    do_command($command);
  }
}

############################################################################
# Check for files to be overwritten
############################################################################
$printed_hdr_f = $FALSE;

foreach(@SRC_LIST) {
  my($full_src_fn) = $SRC_BASE_DIR . $_;
  my($full_dst_fn) = $DST_BASE_DIR . $_;

  if (defined($DST_ASSOC{$_})) {
    if (!$printed_hdr_f) {
      printf "Overwriting files...\n";
      $printed_hdr_f = $TRUE;
    }
    print "  $full_dst_fn\n";

    # overwrite (via copy) file
    my($command) = "copy " . &add_quotes($full_src_fn) . " "
                       . &add_quotes($full_dst_fn)
		       . " | find /v " . &add_quotes("1 file(s) copied.");
    do_command($command);
  }
}

###################################
# Generate Borland version of lib
###################################
my($dir) = $DST_BASE_DIR . "\\win32\\lib";
my($src_file)     = &add_quotes("$dir\\dvsoem.lib");
my($borland_file) = &add_quotes("$dir\\dvsoemb.lib");

print "***********************\n";
print "* Doing 'coff2omf'... *\n";
print "***********************\n";
my($command) = "coff2omf.exe $src_file $borland_file";
do_command($command);

###################################
# Remind about DLL
###################################
my($src_dir) = $SRC_BASE_DIR . "\\win32\\bin";
my($src_dll) = &add_quotes("$src_dir\\dvsoem.dll");
my($win32_dir) = $ENV{"SystemRoot"} . "\\SYSTEM32";

print "*******************************************************************\n";
print " Don't forget to do copy by hand:\n";
print "    copy $src_dll $win32_dir\n";
print " Also, don't forget to install the latest driver on your system if\n";
print "   appropriate.\n";
print "*******************************************************************\n";

exit;

###################################
# Add quotes to a passed string
###################################
sub add_quotes {
  my($s) = $_[0];

  return('"' . $s . '"');
}

###################################
# Execute a system command
###################################
sub do_command {
  my($command) = $_[0];

  if ($SHOW_ONLY_F) {
    print "  Would do '$command'\n";
  }
  else {
    if ($VERBOSE_F) {
      print "  Running '$command'\n";
    }
    system ($command);
  }
}
