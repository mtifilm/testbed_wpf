# debug-to-release

# debug-to-release.pl <borland project group file (.bpg)>

# alters Borland project files referenced by the argument project
# group to make them release mode instead of debug mode.


# mlm: to-do: this has been adapted from the "releasify" script
# embedded in BuildAllPrograms.fbp2; this script is meant to run
# standalone; the interface of this script should be modified so it
# can replace the embedded script in BuildAllPrograms.fbp2 and also be
# run from the command line.

sub debug2release {
    # write a temporary release bpr from a debug bpr
    my $bprh = shift;
    my $tmpbprh = shift;

    $userdefines = 0;
    $cflag1 = 0;
    $pflags = 0;
    $aflags = 0;
    $lflags = 0;

    while (<$bprh>) {
        if (/USERDEFINES/ or $userdefines) {
            $userdefines = 1;

            s:_DEBUG::;            # _DEBUG changed to NDEBUG (all types)

            s:("/>):;NDEBUG\1:;

            $userdefines = 0 if m:"/>:;
        }

        # options to be removed could be preceded by whitespace, a
        # double quote, or the beginning of a line (in the multi-line
        # case)

        # options to be removed could be succeeded by whitespace, a
        # double quote, or the end of a line (in the multi-line case)

        # options to be added could be added after the initial double
        # quote, somewhere in the middle, or before the ending double
        # quote

        elsif (/CFLAG1/ or $cflag1) {
            $cflag1 = 1;
            #  CFLAG1 options might not all be on one line, match
            #  until close of tag seen
            s:(^|\s|")-Od("|\s|$):\1\2:;  # -Od changed to -O2 (all types)
            s:(^|\s|")-r-("|\s|$):\1\2:;  # -r- removed (all types)
            s:(^|\s|")-y("|\s|$):\1\2:;   # -y removed (all types)
            s:(^|\s|")-v("|\s|$):\1\2:;   # -v removed (all types)

            #s:("/>): -O2\1:;             #replaced with next line
            s:(value="):\1-O2 :;         # borland bug - need -O2 at front

            $cflag1 = 0 if m:"/>:;
        }

        elsif (/PFLAGS/ or $pflags) {
            $pflags = 1;
            s:(^|\s|")-\$Y\+("|\s|$):\1\2:; # -$Y+ changed to -$Y- (dll)
            s:(^|\s|")-\$YD("|\s|$):\1\2:; # -$YD changed to -$Y- (bpl, lib, exe)
            s:(^|\s|")-\$W("|\s|$):\1\2:; # -$W removed (all types)
            s:(^|\s|")-\$O-("|\s|$):\1\2:; # -$O- removed (all types)

            s:("/>): -\$Y-\1:;
            s:("/>): -\$L-\1:; # -$L- added (all types)
            s:("/>): -\$D-\1:; # -$D- added (all types)

            $pflags = 0 if m:"/>:;
        }

        elsif (/AFLAGS/ or $aflags) {
            $aflags = 1;
            s:(^|\s|")/zi("|\s|$):\1\2:; # /zi changed to /zn (all types)

            s:("/>): /zn\1:;

            $aflags = 0 if m:"/>:;
        }

        elsif (/LFLAGS/ or $lflags) {
            $lflags = 1;
            s:(^|\s|")-v("|\s|$):\1\2:; # -v removed (dll, bpl, exe)

            $lflags = 0 if m:"/>:;
        }

        print $tmpbprh $_;
    }
}

sub loop_over_bprs {
    my $bpgh = shift;

    while (<$bpgh>) {
        if (/: (.*\.(bpr|bpk))/) {
            $BPR = $1;
#             $BPR =~ s/%([^%]+)%/$ENV{$1}/;

            $TMPBPR = $BPR . ".tmp";

            print $BPR, "\n";
            print $TMPBPR, "\n";

            open BPR or die "Can't open $BPR: $!\n";
            open TMPBPR, ">$TMPBPR" or die " Can't open $TMPBPR: $!\n";

            debug2release(\*BPR, \*TMPBPR);

            close TMPBPR;
            close BPR;

            rename $TMPBPR, $BPR or die "Could not rename: $!\n";
        }
    }
}

# $FOO = ">c:/foo";
# open FOO or die "Could not open foo: $!\n";
# print FOO "build_type = $ENV{BUILD_TYPE}\n";
# close FOO;

# exit 0 if $ENV{BUILD_TYPE} ne "RELEASE";

# $FBP = $ENV{FBPROJECTENV};

# print $FBP, "\n";

$BPG = shift;

open BPG or die "Can't open $BPG: $!\n";

loop_over_bprs(\*BPG);

close BPG;
