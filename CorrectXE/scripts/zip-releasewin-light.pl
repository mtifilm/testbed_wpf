#!/usr/bin/perl

use Cwd;
use File::Basename;
use File::Path;
use Archive::Zip;
use Getopt::Std;	# For command line argument parsing

my($FALSE)           = 0;
my($TRUE)            = !($FALSE);
my($OVERRIDE_NAME_F) = $FALSE;
my($OVERRIDE_NAME) = "";

# Parse Args
my(%opts);

getopts('o:', \%opts);

if ($opts{'o'}) {			# -n = show only mode (like cvs's -n)
  $OVERRIDE_NAME_F = $TRUE;
  $OVERRIDE_NAME = $opts{'o'};
}

$scriptsdir = cwd();
$builddir = dirname($scriptsdir);
$buildsbase = dirname($builddir);
$build = basename($builddir);
$lightbuildsdir =  $buildsbase . "/LightBuilds";

chdir("../ReleaseWin");

mkpath([$lightbuildsdir]);

if ($OVERRIDE_NAME_F) {
  $build = $OVERRIDE_NAME;
}

$zipname = $lightbuildsdir . "/" . $build . "-light" . ".zip";

my $zip = Archive::Zip->new();

while (defined($next = <*.bpl>)) {
    $zip->addFile($next);
}
while (defined($next = <*.dll>)) {
    $zip->addFile($next);
}
while (defined($next = <*.exe>)) {
    $zip->addFile($next);
}
while (defined($next = <Plugins/*.dll>)) {
    $zip->addFile($next);
}

die 'could not write zip file' unless $zip->writeToFileNamed($zipname) == AZ_OK;

print "$0: Wrote zip file: $zipname\n";
