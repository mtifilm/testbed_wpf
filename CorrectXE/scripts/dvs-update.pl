#!/usr/bin/perl -w
#############################################################################
#
#  Manage dvs versions for build purposes.
#  
#  Modification history:
#    5/07/2009 - mpr - updated to fix problem where "-t" didn't show
#                "dvs_3..." tags.
#    7/14/2005 - mpr - added "-t" & fix for possible extra \r
#    6/30/2005 - Michael Russell - Creation
#  
#   dvs-update.pl [-n] [-s] desired_tag
#                  -n = show only mode (like cvs)
#                  -s = show status of all DVS sub-directories
#                  -t = show 'dvs...' tags in repository
#
#############################################################################

use strict;		# Force declaration of all variables
use Getopt::Std;	# For command line argument parsing

# Globals
my($FALSE)           = 0;
my($TRUE)            = !($FALSE);
my($SHOW_ONLY_F)     = $FALSE;
my($STATUS_ONLY_F)   = $FALSE;
my($SHOWTAGS_ONLY_F) = $FALSE;
my($DIR_PREFIX)      = "DVS";
my($RC)              =  0;
my($RC_OK)           =  0;
my($RC_SYNTAX_ERR)   = -1;
my($RC_NO_TAG_FOUND) = -2;

# Parse Args
my(%opts);
my($desired_tag);
my($desired_dirname);

getopts('nst', \%opts);

if ($opts{'n'}) {			# -n = show only mode (like cvs's -n)
  $SHOW_ONLY_F = $TRUE;
}

if ($opts{'s'}) {			# -s = show status
  $STATUS_ONLY_F = $TRUE;
}

if ($opts{'t'}) {			# -t = show 'dvs...' tags
  $SHOWTAGS_ONLY_F = $TRUE;
}

if ($STATUS_ONLY_F) {
  &get_and_print_status;
  exit $RC;
}
elsif ($SHOWTAGS_ONLY_F) {
  &show_tags;
  exit $RC;
}
else {					# Get current status
  if (!defined($ARGV[0])) {
    print "Usage: $0 [-n] [-s] desired_tag\n";
    print "       -n = show only mode (like cvs)\n";
    print "       -s = show status of all DVS sub-directories\n";
    print "       -t = show 'dvs...' tags in repository\n";
    $RC = $RC_SYNTAX_ERR;
    exit $RC;
  }
  else {
    $desired_tag = shift;
  }
}

# Check for valid tag - format is "dvs_" + any digit + "_" + any digits + "p"
#                                 + any digits + any lowercase char (optional)
#   or
#     simply HEAD
#
if ($desired_tag =~ /^(dvs_\d_\d+p\d+[a-z]*)$/) {
  $desired_tag = $1;				# untaint
}
elsif ($desired_tag =~ /^(HEAD)$/) {		# or "HEAD"
  $desired_tag = $1;				# untaint
}
else {
  print STDERR
    "Specified tag doesn't match expected format - e.g. dvs_2_7p30c | HEAD\n";
  $RC = $RC_SYNTAX_ERR;
  exit $RC;
}

print "Processing request for tag '$desired_tag'\n";
$desired_dirname = $DIR_PREFIX . "-" . $desired_tag;

# Check for "dvs" directory
my($res);
my($currdir_tag);
my($currdir_dirname);

if (! -d "dvs") {
  die "No 'dvs' directory in current directory.  Exiting...";
}

# Grab current tag from "dvs" directory
$currdir_tag = get_tag("dvs");
$currdir_dirname = $DIR_PREFIX . "-" . $currdir_tag;
if ($currdir_tag eq $FALSE) {
  print "Couldn't get tag from 'dvs'.  Exiting...";
  exit $RC_NO_TAG_FOUND;
}

#print "tag = '$currdir_tag'\n";

# Check if already have specified version
if ($currdir_tag eq $desired_tag) {
  printf "'dvs' directory appears to already be tagged as version %s\n",
         $desired_tag;
  print "  so, there's nothing to do.\n";
  exit $RC_OK;
}

# Check if directory already exists
my($did_rename_f) = $FALSE;
if (-d $currdir_dirname) {
  print "Directory with name of tag already exists.  Nothing to do.\n";
  exit $RC_OK;
}
else {					# if doesn't exist, then rename
  if (!$SHOW_ONLY_F) {
    print "Doing: rename 'dvs' -> '$currdir_dirname'\n";
    $res = rename ("dvs", $currdir_dirname);
    if ($res != 1) {
      die "rename 'dvs' -> '$currdir_dirname' failed: $!.  Exiting...";
    }
  }
  else {
    print "Would do: rename 'dvs' -> '$currdir_dirname'\n";
  }
  $did_rename_f = $TRUE;
}

# Do rename or checkout
if ($did_rename_f) {
  if (-d $desired_dirname) { 			# rename to "dvs"
    if (!$SHOW_ONLY_F) {
      print "Doing: rename 'dvs' -> '$currdir_dirname'\n";
      $res = rename ($desired_dirname, "dvs");
      if ($res != 1) {
        die "rename '$desired_dirname' -> 'dvs' failed: $!.  Exiting...";
      }
    }
    else {
      print "Would do: rename '$desired_dirname' -> 'dvs'\n";
    }
  }
  else {					# do checkout
    my($command);
    $command = "cvs checkout -r $desired_tag dvs";

    if (!$SHOW_ONLY_F) {
      print "Doing: $command\n";
      my(@result) = `$command 2>&1`;
      $RC = $?;
      foreach(@result) {
        print $_;
      }
#      run_and_check($command);
    }
    else {
      print "Would do: $command\n"; 
    }
  }
}

exit $RC;			# All done

#################################################
# Run a system command check the return code
#################################################
sub run_and_check {
  my($command) = $_[0];

  my($res);

  $res = 0xffff & system($command);

  printf "system() returned %#04x: ", $res;
  if ($res == 0) {
    print "ran with normal exit\n";
  }
  elsif ($res == 0xff00) {
    print "command failed: $!\n";
  }
  elsif ($res > 0x80) {
    $res >>= 8;
    print "ran with non-zero exit status $res\n";
  }
  else {
    print "ran with ";
    if ($res &   0x80) {
      $res &= ~0x80;
      print "core dump from ";
    }
    print "signal $res\n";
  }
  $RC = ($res != 0);
}

##########################################################
# Get the cvs tags of "dvs" and all "DVS..." directories
##########################################################
sub get_and_print_status {
  my(@files);

  my($tag);

  opendir(DIR, ".") or die "can't opendir '.': $!";
  @files = readdir(DIR);
  closedir DIR;


  print "=====================================================\n";
  print "Current 'dvs' directories\n";
  print "=====================================================\n";
  printf "  %-35s %s\n", "Directory", "Tag";
  printf "  %-35s %s\n", "---------", "---";

  my($found_dir_f) = $FALSE;

  foreach(@files) {
    if ((-d $_) && (($_ =~ /^dvs$/) || ($_ =~ /^$DIR_PREFIX/))) {
      $tag = get_tag($_);
      printf "  %-35s %s\n", $_, $tag;
      $found_dir_f = $TRUE;
    }
  }

  if (!$found_dir_f) {
    print "  None found.\n";
  }
}

###################################################################
# Get the cvs tag out of the dvs header directory 'Entries' file
#   for 'dvs_clib.h'.
###################################################################
sub get_tag {
  my($dir) = $_[0];

  my($entries_fn) = "$dir/sdk/development/header/CVS/Entries";
  my(@lines);
  my($tag) = "";

  if (! -f $entries_fn) {
    print "No Entries file: '$entries_fn'.\n";
    return ($FALSE);
  }

  open(ENTRIES, $entries_fn) or die "Can't open Entries file: '$entries_fn'";
  @lines = <ENTRIES>;
  close(ENTRIES);

  chomp(@lines);

  my($done_f) = $FALSE;
  my($line);
  my($fn, $ver, $date, $skip, $full_tag);

  my($i) = 0;

  while (!$done_f) {
    $line = $lines[$i];
    
    # Get rid of possible extra CR (\r) at end of line (dos/unix issue)
    if (substr($line, length($line) - 1, 1) eq "\r") {
      chop($line);
    }
    $i++;
    if ($i > $#lines) {
      print "Entries file format invalid 1: '$entries_fn'.\n";
      return ($FALSE);
    }
    
#print "'$line'\n";
    ($skip, $fn, $ver, $date, $skip, $full_tag) = split('/', $line);
    if ($fn eq "dvs_clib.h") {
      if ($full_tag eq "") {
	$tag = "HEAD";
	$done_f = $TRUE;
      }
      elsif ($full_tag =~ /^[NT](.+)$/) {
	$tag = $1;
	$done_f = $TRUE;
      }
      else {
        print "Entries file format invalid 2: '$entries_fn'.\n";
        return ($FALSE);
      }
    }
  }

  if ($tag eq "") {
    print "Entries file format invalid 3: '$entries_fn'.\n";
    return ($FALSE);
  }
#print "tag = '$tag'\n";

  return($tag);
}

####################################################################
# Get the cvs tags from the repository & filter out "dvs..." ones
####################################################################
sub show_tags {
  my(@lines);
  my($fn) = "dvs/sdk/development/header/dvs_clib.h";
  my($tag) = "";
  my($found_existing_f) = $FALSE;

  @lines = `cvs status -v $fn 2>&1`;
  $RC = $?;
  chomp(@lines);

  print "=====================================================\n";
  print "Current 'dvs...' tags in repository\n";
  print "=====================================================\n";

  foreach (@lines) {
    if (/Existing Tags:/) {
      $found_existing_f = $TRUE;
      next;
    }

    if ($found_existing_f) {
      if (/\s*(\S*)\s*/) {
        $tag = $1;
	if ($tag =~ /^dvs_\d/) {
          print "  $tag\n";
        }
      }
    }
  }

  # This should print output when an error occurs
  if (!$found_existing_f) {
    foreach (@lines) {
      print "  $_\n";
    }
  }
}
