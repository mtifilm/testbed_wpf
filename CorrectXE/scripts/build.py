# build.py

# requirements (these are the design goals, but do not necessarily
# reflect the current state of the program)
#
# 1. runs without any options
#
# 2. options can be set on command line or in environment
#
# 3. builds all products by default
#
# 4. builds only specified products (and dependencies) when specified
#
# 5. checks out all modules by default
#
# 6. checks out only modules for specified products (and dependencies)
# when specified
#
# 7. allows optional tag for each module to check out
#
# 8. allows incremental updates from source code repository
#
# 9. allows full checkout from source code repository
#
# 10. optionally packages products for distribution
#
# 11. optionally distributes products to distribution sites
#
# 12. optionally runs automated test suite
#
# 13. each run creates a log
#
# 14. log contains the command that invoked the build
#
# 15. log contains environment variable values at the time of the build
#
# 16. log contains the build configuration at the time of the build

## configuration

# copy buildcfg-sample.py to buildcfg.py and edit as necessary

from buildcfg import *

## end configuration

import os
import sys
from subprocess import call
from datetime import datetime
from update_makefiles import update_makefiles

start_make_dir = 'ReleaseWin'
now_str = datetime.today().strftime("%Y%m%d%H%M%S")
build_log_file = 'build-' + now_str + '.log'

build_log_file_path = os.pardir + os.sep + start_make_dir + os.sep + build_log_file

print "building ..."
print "creating log file in " + build_log_file_path

build_log = open(build_log_file_path, 'w')

sys.stdout = build_log
sys.stderr = build_log

print 'mlm remove me: just a test'
sys.stdout.flush() # needed to make above print appear before call()'s
                   # output below

os.chdir(os.pardir)

if mtibuild_do_sc_update:
    call(["cvs", "update"], stdout=sys.stdout, stderr=sys.stderr)

os.chdir(start_make_dir)

if mtibuild_do_build:
    update_makefiles()
    call(["qmake"], stdout=sys.stdout, stderr=sys.stderr)
    call([mtibuild_make_cmd], stdout=sys.stdout, stderr=sys.stderr)
