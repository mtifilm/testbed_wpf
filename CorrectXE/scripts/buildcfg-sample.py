# copy this file to buildcfg.py and edit as necessary

# True | False
mtibuild_do_sc_update = True
mtibuild_do_build = True

mtibuild_make_cmd = 'make.bat' # make.bat (mingw) | nmake (msvc)

# environment

import os

# the examples below are for MinGW
os.environ["QTDIR"] = "C:\\Qt\\4.0.1"
os.environ["PATH"] = "C:\\Qt\\4.0.1\\bin;C:\\MinGW\\bin;C:\\WINDOWS\\system32;C:\\Program Files\\TortoiseCVS"
os.environ["QMAKESPEC"] = "win32-g++"
