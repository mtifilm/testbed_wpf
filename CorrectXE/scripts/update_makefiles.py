# update_makefiles

# Update Makefiles based on dependencies.  Some versions of make
# aren't smart enough to 'make Makefile' if Makefile is out of date.

import os, sys
from subprocess import call
from buildcfg import *

def update_makefile(arg, dirname, names):
    cwd = os.getcwd()
    if arg in names:
        print "updating Makefile in " + dirname
        sys.stdout.flush()
        os.chdir(dirname)
        call([mtibuild_make_cmd, "Makefile"],
             stdout=sys.stdout, stderr=sys.stderr)
        os.chdir(cwd)

def update_makefiles():
    os.path.walk(os.pardir, update_makefile, "Makefile")
