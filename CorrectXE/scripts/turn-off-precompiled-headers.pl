#! /usr/bin/perl

# Changes Borland project files to turn off precompiled headers.

# usage: no-precompiled-headers.pl FinalBuilderProject.fbp2

sub turn_off_pch {
    # write a temporary release bpr from a debug bpr
    my $bprh = shift;
    my $tmpbprh = shift;                   

    $cflag1 = 0;
    $pflags = 0;
    $aflags = 0;
    $lflags = 0;

    while (<$bprh>) {
        # options to be removed could be preceded by whitespace, a
        # double quote, or the beginning of a line (in the multi-line
        # case)

        # options to be removed could be succeeded by whitespace, a
        # double quote, or the end of a line (in the multi-line case)

        # options to be added could be added after the initial double
        # quote, somewhere in the middle, or before the ending double
        # quote

        if (/CFLAG1/ or $cflag1) {
            $cflag1 = 1;
            #  CFLAG1 options might not all be on one line, match
            #  until close of tag seen
            s:(^|\s|")-H=[^ ]+("|\s|$):\1\2:;  # -H=<path> removed
            s:(^|\s|")-Hc("|\s|$):\1\2:;  # -Hc removed

            $cflag1 = 0 if m:"/>:;
        }

        print $tmpbprh $_;
    }
}

sub loop_over_bprs {
    my $fbph = shift;

    while (<$fbph>) {
        if (/"ProjectFile".*>(.*\.(bpr|bpk))/) {
            $BPR = $1;
            $BPR =~ s/%([^%]+)%/$ENV{$1}/;

            $TMPBPR = $BPR . ".tmp";

            print "In:  ", $BPR, "\n";
            print "Out: ", $TMPBPR, "\n";

	    # Only process if file exists to allow Disabling of
	    #  projects which aren't really going to be processed.
	    if (-e $BPR) {
              open BPR or die "  Can't open $BPR: $!\n";
              open TMPBPR, ">$TMPBPR" or die " Can't open $TMPBPR: $!\n";

              turn_off_pch(\*BPR, \*TMPBPR);

              close TMPBPR;
              close BPR;

              rename $TMPBPR, $BPR or die "  Could not rename: $!\n";
	    }
	    else {
	      print "  Skipping $BPR - no such file\n";
	    }
        }
    }
}

$FBP = shift;

print "Turn Off Precompiled Headers\n";
print "----------------------------\n";
print "Processing FB: ", $FBP, "\n";

open FBP or die "Can't open $FBP: $!\n";

loop_over_bprs(\*FBP);
              
close FBP;
