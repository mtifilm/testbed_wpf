//---------------------------------------------------------------------------


#ifndef NakedProxyUnitH
#define NakedProxyUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "MTIBusy.h"
#include <ExtCtrls.hpp>

#include "ProxyBitmap.h"
#include "MTIBusy.h"
#include "timecode.h"

class ProxyDisplayer;

//---------------------------------------------------------------------------
class TNakedProxyFrame : public TFrame
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TPaintBox *ProxyPaintBox;
        TPanel *BusyArrowPanel;
        TMTIBusy *BusyArrows;
        void __fastcall ProxyPaintBoxPaint(TObject *Sender);
private:	// User declarations
        CTimecode TimecodeBeingShown;
        ProxyDisplayer *LastDisplayerUsed;   // QQQ EVIL
public:		// User declarations
        __fastcall TNakedProxyFrame(TComponent* Owner);
        void Reset();
        void ShowFrame(ProxyDisplayer *displayer, const CTimecode &timecode);
        void RedrawFrame();
};
//---------------------------------------------------------------------------
//extern PACKAGE TNakedProxyFrame *NakedProxyFrame;
//---------------------------------------------------------------------------
#endif
