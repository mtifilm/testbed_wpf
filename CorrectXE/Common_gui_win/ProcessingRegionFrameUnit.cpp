//---------------------------------------------------------------------------

#include <vcl.h>
#include <algorithm>
#pragma hdrstop

#include "ProcessingRegionFrameUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TTFProcessingRegion *TFProcessingRegion;
//---------------------------------------------------------------------------
__fastcall TTFProcessingRegion::TTFProcessingRegion(TComponent* Owner)
   : TFrame(Owner)
{
    // Fucking designer only knows about ints
    LeftLeftButton->Tag = 0x00FF000000;
}

//---------------------------------------------------------------------------
void __fastcall TTFProcessingRegion::BoundryButtonClick(TObject *Sender)
{
    union
    {
       long value;
       struct
       {
          // Tag is left to right, memory is right to left
          int bottom : 8;
          int right : 8;
          int top : 8;
          int left : 8;
       };
    } pesudoRect;

   auto speedButton = static_cast<TSpeedButton *>(Sender);
   pesudoRect.value = static_cast<long>(speedButton->Tag);

   int m = HIWORD(GetKeyState(VK_SHIFT)) != 0 ? 10 : 1;

   _boundingBox.left = std::max<int>(0, _boundingBox.left +m * pesudoRect.left);
   _boundingBox.top = std::max<int>(0, _boundingBox.top +  m * pesudoRect.top);
   _boundingBox.right = std::min<int>(_maxWidth, _boundingBox.right + m * pesudoRect.right);
   _boundingBox.bottom = std::min<int>(_maxHeight, _boundingBox.bottom + m * pesudoRect.bottom);

   if (_onBoxChange != nullptr)
   {
      _onBoxChange(this);
   }
}

//---------------------------------------------------------------------------

void TTFProcessingRegion::SetMinMax(int width, int height)
{
   _maxWidth = width;
   _maxHeight = height;
}

RECT &TTFProcessingRegion::GetBoundingBox()
{
   return _boundingBox;
}

void TTFProcessingRegion::SetBoundingBox(const RECT &boundingBox)
{
   _boundingBox = boundingBox;
}

void __fastcall TTFProcessingRegion::AllMatButtonClick(TObject *Sender)
{
   _boundingBox.left = 0;
   _boundingBox.top = 0;
   _boundingBox.right = _maxWidth;
   _boundingBox.bottom = _maxHeight;

   if (_onBoxChange != nullptr)
   {
      _onBoxChange(this);
   }
}
//---------------------------------------------------------------------------

void __fastcall TTFProcessingRegion::LockMattingCBoxClick(TObject *Sender)
{
   auto enable = !LockMattingCBox->Checked;
   for (auto i=0; i < ButtonPanel->ControlCount; i++)
   {
        ButtonPanel->Controls[i]->Enabled = enable;
   }
}
//---------------------------------------------------------------------------

