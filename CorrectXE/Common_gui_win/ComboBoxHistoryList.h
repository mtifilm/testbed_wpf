
#include "HistoryList.h"

/*
 * A utility class used to help display a HistoryList object's contents
 * as the items in a combo box drop-down list.
 */
class ComboBoxHistoryList : public CHistoryList {
public:
    ComboBoxHistoryList(const string &fileName, const string &listName)
    { IniFileName = fileName; IniSection = listName; };

    void rebuildComboBoxList(TComboBox* comboBox)
    {
        comboBox->Items->Clear();
        for (unsigned int i = 0; i < size(); ++i) {
            if (at(i) != "") {
                comboBox->Items->Append(at(i).c_str());
            }
        }
    };

    string getNewestItem()
    {
        if (empty())
            return string("");
        return at(0);
    };
};

 