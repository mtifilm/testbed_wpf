#ifndef ProxyDisplayerH
#define ProxyDisplayerH
//---------------------------------------------------------------------------

#include <vector>
#include "ImageFormat3.h"
#include "ProxyBitmap.h"
#include "timecode.h"
//---------------------------------------------------------------------------

class CConvert;

struct CSOverrides
{
    EPixelPacking packing;
    EColorSpace colorSpace;
    MTI_UINT16 minValue;
    MTI_UINT16 maxValue;
    MTI_REAL32 gamma;
    bool useCCIR709TransferFunction;

    CSOverrides()
    {
        packing = IF_PIXEL_PACKING_INVALID;
        colorSpace = IF_COLOR_SPACE_INVALID;
        minValue = -1;
        maxValue = -1;
        gamma = -1.0;
        useCCIR709TransferFunction = false;
    };

    bool equals(CSOverrides &rhs)
    {
        return (0 == memcmp(this, &rhs, sizeof(*this)));
    };
};
//---------------------------------------------------------------------------


class ProxyDisplayer {
public:

    ProxyDisplayer();
    ~ProxyDisplayer();

    void SetSourceName(const string &name);
    void SetDisplayContext(HDC handle);
    void SelectFraming(int framing)   // FRAMING_SELECT_FILM or _VIDEO
            { m_framing = framing; };
    void SetOverrides(const CSOverrides &overrides);
    int RenderProxy(const CTimecode &timecode, TImage *image,
                    bool drawX = false, bool forceDraw = false);
    int RenderProxy(const CTimecode &timecode, TPaintBox *image,
                    bool drawX = false, bool forceDraw = false);

private:
    struct CacheEntry {
        CTimecode timecode;
        ProxyBitmap *bitmap;

        CacheEntry(CConvert *sharedConverter = 0)
            : timecode(0)
        {
            bitmap = new ProxyBitmap(sharedConverter);
        };
        ~CacheEntry() { delete bitmap; };
    };
    typedef vector<CacheEntry*> Cache;
    typedef vector<CacheEntry*>::iterator CacheIterator;

    Cache m_cache;
    string m_sourceName;
    CConvert *m_converter;
    HDC m_handle;
    int m_framing;
    bool m_haveOverrides;
    CSOverrides m_overrides;

    void reset();
    int drawProxyOnCanvas(const CTimecode &timecode, int width, int height,
                          TCanvas *canvas);
    ProxyBitmap *getBitmap(const CTimecode &timecode, int width, int height);
    int fillBitmap(ProxyBitmap &bitmap, const CTimecode &timecode);
    void flushBitmapFromCache(const CTimecode &timecode);
};
//---------------------------------------------------------------------------

#endif // PROXY_DISPLAYER_H
 