//---------------------------------------------------------------------------


#ifndef MediaLocationUnitH
#define MediaLocationUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <Grids.hpp>
#include <string>
using std::string;
//---------------------------------------------------------------------------
class TMediaLocationFrame : public TFrame
{
__published:	// IDE-managed Components
    TLabel *MediaLocationLabel1;
    TComboBox *DiskSchemeComboBox;
    TCheckBox *AddHandlesCheckBox;
    TLabel *Label1;
    TTimer *DiskSpaceTimer;
    void __fastcall DiskSchemeComboBoxSelect(TObject *Sender);
    void __fastcall DiskSpaceTimerTimer(TObject *Sender);

private:	// User declarations
    string ClipSchemeName;
    string ProtoClipName;
    
    void initDiskSchemeComboBox(const string &defaultName);
    void updateTimeRemainingOnDisk();
    
public:		// User declarations
    void Init(const string &defaultDiskSchemeName,
                               const string &clipSchemeName,
                               const string &protoClipName,
                               const bool defaultAddHandles);
    string GetDiskScheme();
    bool GetAddHandlesFlag();
    __fastcall TMediaLocationFrame(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TMediaLocationFrame *MediaLocationFrame;
//---------------------------------------------------------------------------
#endif
