//---------------------------------------------------------------------------


#ifndef TwoProxyWithSliderUnitH
#define TwoProxyWithSliderUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "SmallLabeledProxyUnit.h"
#include <ComCtrls.hpp>
#include "MTIBusy.h"
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TTwoProxyWithSliderFrame : public TFrame
{
__published:	// IDE-managed Components
    TSmallLabeledProxyFrame *NewFrameProxyFrame;
    TLabel *NewFrameProxyLabel;
    TTrackBar *TrackBar;
    TLabel *OldFrameProxyLabel;
    TSmallLabeledProxyFrame *OldFrameProxyFrame;
    TMTIBusy *BusyArrows;
    TImage *IdleArrow;
private:	// User declarations
public:		// User declarations
    void SetBusy(bool flag);
    void Enable(bool flag);
    __fastcall TTwoProxyWithSliderFrame(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TTwoProxyWithSliderFrame *TwoProxyWithSliderFrame;
//---------------------------------------------------------------------------
#endif
