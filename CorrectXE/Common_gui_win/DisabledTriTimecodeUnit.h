//---------------------------------------------------------------------------


#ifndef DisabledTriTimecodeUnitH
#define DisabledTriTimecodeUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TDisabledTriTimecodePanel : public TFrame
{
__published:	// IDE-managed Components
    TPanel *InPanel;
    TPanel *OutPanel;
    TPanel *DurationPanel;
    TLabel *DurationLabel;
    TLabel *OutLabel;
    TLabel *InLabel;
private:	// User declarations
public:		// User declarations
    __fastcall TDisabledTriTimecodePanel(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TDisabledTriTimecodePanel *DisabledTriTimecodePanel;
//---------------------------------------------------------------------------
#endif
