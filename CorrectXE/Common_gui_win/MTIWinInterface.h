//---------------------------------------------------------------------------

#ifndef MTIWinInterfaceH
#define MTIWinInterfaceH

//
// This is a busy cursor that automatically turns off when destroyed.

class CBusyCursor
{
  class Cheshire;

  public:
    CBusyCursor(bool bBusy=false);
    void Busy(void);
    void NotBusy(void);
    ~CBusyCursor(void);
    static int getBusyCursorType(void);
    static void setBusyCursorType(int BCtype);

  private:
    Cheshire *Smile;
    static int BusyCursorType; // 0 = default, 1 = arrows, 2 = cross
};

bool SetBusyCursorToDefault(void);
bool SetBusyCursorToBusyArrows(void);
bool SetBusyCursorToBusyCross(void);
int PickerCursor(void);
int ClosedHandCursor(void);
int OpenHandCursor(void);
int MagnifierCursor(void);
void LoadMainIcons(void);
bool IsDebuggerAttached(void);




//---------------------------------------------------------------------------
#endif
