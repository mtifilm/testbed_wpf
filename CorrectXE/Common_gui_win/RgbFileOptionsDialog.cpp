//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RgbFileOptionsDialog.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VDoubleEdit"
#pragma resource "*.dfm"
TRgbFileOptionsDialog *RgbFileOptionsDialog;
//---------------------------------------------------------------------------
__fastcall TRgbFileOptionsDialog::TRgbFileOptionsDialog(TComponent* Owner)
    : TForm(Owner)
{
    AllowedColorSpacesMask = (unsigned int) -1;
    Linear10RadioButton->Tag = RGBOPT_CS_LINEAR_10;
    Log10RadioButton->Tag = RGBOPT_CS_LOG_10;
    Linear12RadioButton->Tag = RGBOPT_CS_LINEAR_12;
    Log12RadioButton->Tag = RGBOPT_CS_LOG_12;
    Linear8RadioButton->Tag = RGBOPT_CS_LINEAR_8;
    Linear16RadioButton->Tag = RGBOPT_CS_LINEAR_16;
    SetDefaultValues(RGBOPT_CS_LINEAR_10, RGBOPT_RANGE_FULL, 0, 1023,
                     RGBOPT_GAMMA_17, 1.0);
    CustomMin8 = CustomMin10 = CustomMin12 = CustomMin16 = 0;
    CustomMax8 = 255;
    CustomMax10 = 1023;
    CustomMax12 = 4095;
    CustomMax16 = 65535;

    PixelValueRangeMinVDoubleEdit->dValue = 0.0;
    PixelValueRangeMaxVDoubleEdit->dValue = 1023.0;
    GammaVDoubleEdit->dValue = 1.0;
}
//---------------------------------------------------------------------------

void TRgbFileOptionsDialog::SetAllowedColorSpaces(int mask)
{
    AllowedColorSpacesMask = mask;
    ConformColorSpace();
}
//---------------------------------------------------------------------------

void TRgbFileOptionsDialog::SetDefaultValues(
                int colorSpaceIndex,
                int rangeIndex,
                int rangeMin,
                int rangeMax,
                int gammaIndex,
                float customGamma)
{
    DefaultColorSpaceIndex = colorSpaceIndex;
    DefaultRangeIndex = rangeIndex;
    DefaultRangeMin = rangeMin;
    DefaultRangeMax = rangeMax;
    DefaultGammaIndex = gammaIndex;
    DefaultCustomGamma = customGamma;
    ConformColorSpace();
}

//---------------------------------------------------------------------------
void TRgbFileOptionsDialog::SetDefaults()
{
    ++InhibitCallbacks;

    /*
     * Color Space
     */
    switch (DefaultColorSpaceIndex) {
        default:
            DefaultColorSpaceIndex = RGBOPT_CS_LINEAR_10;
            /* FALL THROUGH */
        case RGBOPT_CS_LINEAR_10:
            Linear10RadioButton->Checked = true;
            break;
        case RGBOPT_CS_LOG_10:
            Log10RadioButton->Checked = true;
            break;
        case RGBOPT_CS_LINEAR_12:
            Linear12RadioButton->Checked = true;
            break;
        case RGBOPT_CS_LOG_12:
            Log12RadioButton->Checked = true;
            break;
        case RGBOPT_CS_LINEAR_8:
            Linear8RadioButton->Checked = true;
            break;
        case RGBOPT_CS_LINEAR_16:
            Linear16RadioButton->Checked = true;
            break;
    }
    ConformColorSpace();

    /*
     * Range... edit fields MUST be set BEFORE radio button
     */
    PixelValueRangeMinVDoubleEdit->dValue = (double) DefaultRangeMin;
    PixelValueRangeMaxVDoubleEdit->dValue = (double) DefaultRangeMax;
    switch (DefaultRangeIndex) {
        default:
            DefaultRangeIndex = RGBOPT_RANGE_FULL;
            /* FALL THROUGH */
        case RGBOPT_RANGE_FULL:
            RangeFullRadioButton->Checked = true;
            break;
        case RGBOPT_RANGE_RESTRICTED:
            RangeRestrictedRadioButton->Checked = true;
            break;
        case RGBOPT_RANGE_CUSTOM:
            RangeCustomRadioButton->Checked = true;
            break;
    }
    ConformRange();

    /*
     * Gamma... edit field MUST be set BEFORE radio button
     */
    GammaVDoubleEdit->dValue = (double) DefaultCustomGamma;
    switch (DefaultGammaIndex) {
        default:
            DefaultGammaIndex = RGBOPT_GAMMA_17;
            /* FALL THROUGH */
        case RGBOPT_GAMMA_17:
            Gamma17RadioButton->Checked = true;
            break;
        case RGBOPT_GAMMA_10:
            Gamma10RadioButton->Checked = true;
            break;
        case RGBOPT_GAMMA_22222:
            Gamma22222RadioButton->Checked = true;
            break;
        case RGBOPT_GAMMA_CUSTOM:
            GammaCustomRadioButton->Checked = true;
            break;
    }
    ConformGamma();

    --InhibitCallbacks;
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::SetDefaultsButtonClick(
      TObject *Sender)
{
    SetDefaults();    
}
//---------------------------------------------------------------------------

void TRgbFileOptionsDialog::GetValues(
            int &colorSpaceIndex,
            int &rangeMin,
            int &rangeMax,
            float &gamma,
            bool &useCCIR709TransferFunction)
{
    if (Linear8RadioButton->Checked) {
        colorSpaceIndex = RGBOPT_CS_LINEAR_8;
        if (RangeFullRadioButton->Checked) {
            rangeMin = 0;
            rangeMax = 255;
        } else if (RangeRestrictedRadioButton->Checked) {
            rangeMin = 16;
            rangeMax = 240;
        } else {
            rangeMin = (int) PixelValueRangeMinVDoubleEdit->dValue;
            rangeMax = (int) PixelValueRangeMaxVDoubleEdit->dValue;
        }
    } else if (Linear16RadioButton->Checked) {
        colorSpaceIndex = RGBOPT_CS_LINEAR_16;
        if (RangeFullRadioButton->Checked || RangeRestrictedRadioButton->Checked) {
            rangeMin = 0;
            rangeMax = 65535;
        } else {
            rangeMin = (int) PixelValueRangeMinVDoubleEdit->dValue;
            rangeMax = (int) PixelValueRangeMaxVDoubleEdit->dValue;
        }
    } else if (Linear10RadioButton->Checked || Log10RadioButton->Checked) {
        if (Linear10RadioButton->Checked) {
            colorSpaceIndex = RGBOPT_CS_LINEAR_10;
        } else {
            colorSpaceIndex = RGBOPT_CS_LOG_10;
        }
        if (RangeFullRadioButton->Checked) {
            rangeMin = 0;
            rangeMax = 1023;
        } else if (RangeRestrictedRadioButton->Checked) {
            rangeMin = 95;
            rangeMax = 685;
        } else {
            rangeMin = (int) PixelValueRangeMinVDoubleEdit->dValue;
            rangeMax = (int) PixelValueRangeMaxVDoubleEdit->dValue;
        }
    } else /* 12 bit linear or log */ {
        if (Linear12RadioButton->Checked) {
            colorSpaceIndex = RGBOPT_CS_LINEAR_12;
        } else {
            colorSpaceIndex = RGBOPT_CS_LOG_12;
        }
        if (RangeFullRadioButton->Checked) {
            rangeMin = 0;
            rangeMax = 4095;
        } else if (RangeRestrictedRadioButton->Checked) {
            rangeMin = 16*16;
            rangeMax = 4096-rangeMin;
        } else {
            rangeMin = (int) PixelValueRangeMinVDoubleEdit->dValue;
            rangeMax = (int) PixelValueRangeMaxVDoubleEdit->dValue;
        }
    }
    useCCIR709TransferFunction = false;
    if (Gamma10RadioButton->Checked) {
        gamma = 1.0;
    } else if (Gamma17RadioButton->Checked) {
        gamma = 1.7;
    } else if (Gamma22222RadioButton->Checked) {
        gamma = 2.2222;
        useCCIR709TransferFunction = true;
    } else {
        gamma = (float) GammaVDoubleEdit->dValue;
    }
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::RangeMinUpDownClick(
      TObject *Sender, TUDBtnType Button)
{
    if (!InhibitCallbacks) {
        int iValue = (int) PixelValueRangeMinVDoubleEdit->dValue;

        iValue += ((Button == btNext)? 1 : -1);
        ++InhibitCallbacks;
        PixelValueRangeMinVDoubleEdit->dValue = (double) iValue;
        if (!RangeCustomRadioButton->Checked) {
            RangeCustomRadioButton->Checked = true;
        }
        --InhibitCallbacks;
        ConformRange();
    }
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::PixelValueRangeMinVDoubleEditDoubleChange(
      TObject *Sender)
{
    if (!InhibitCallbacks) {
        if (!RangeCustomRadioButton->Checked) {
            ++InhibitCallbacks;
            RangeCustomRadioButton->Checked = true;
            --InhibitCallbacks;
        }

        ConformRange();
    }
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::RangeMaxUpDownClick(
      TObject *Sender, TUDBtnType Button)
{
    if (!InhibitCallbacks) {
        int iValue = (int) PixelValueRangeMaxVDoubleEdit->dValue;

        iValue += ((Button == btNext)? 1 : -1);
        ++InhibitCallbacks;
        PixelValueRangeMaxVDoubleEdit->dValue = (double) iValue;
        if (!RangeCustomRadioButton->Checked) {
            RangeCustomRadioButton->Checked = true;
        }
        --InhibitCallbacks;
        ConformRange();
    }
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::PixelValueRangeMaxVDoubleEditDoubleChange(
      TObject *Sender)
{
    if (!InhibitCallbacks) {
        if (!RangeCustomRadioButton->Checked) {
            ++InhibitCallbacks;
            RangeCustomRadioButton->Checked = true;
            --InhibitCallbacks;
        }

        ConformRange();
    }
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::GammaCustomUpDownClick(
      TObject *Sender, TUDBtnType Button)
{
    if (!InhibitCallbacks) {
        double dValue = GammaVDoubleEdit->dValue;

        dValue += ((Button == btNext)? 0.1 : -0.1);
        ++InhibitCallbacks;
        GammaVDoubleEdit->dValue = dValue;
        if (!GammaCustomRadioButton->Checked) {
            GammaCustomRadioButton->Checked = true;
        }
        --InhibitCallbacks;
        ConformGamma();
    }
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::GammaVDoubleEditDoubleChange(
      TObject *Sender)
{
    if (!InhibitCallbacks) {
        if (!GammaCustomRadioButton->Checked) {
            ++InhibitCallbacks;
            GammaCustomRadioButton->Checked = true;
            --InhibitCallbacks;
        }

        ConformGamma();
    }
}
//---------------------------------------------------------------------------

void TRgbFileOptionsDialog::ConformColorSpace()
{
    bool noneChecked = false;

    ++InhibitCallbacks;
    if (AllowedColorSpacesMask & RGBOPT_CS_LINEAR_8_FLAG) {
        if (!Linear8RadioButton->Enabled) {
            Linear8RadioButton->Enabled = true;
        }
    } else {
        if (Linear8RadioButton->Enabled) {
            Linear8RadioButton->Enabled = false;
        }
        if (Linear8RadioButton->Checked) {
            noneChecked = true;
        }
    }
    if (AllowedColorSpacesMask & RGBOPT_CS_LINEAR_16_FLAG) {
        if (!Linear16RadioButton->Enabled) {
            Linear16RadioButton->Enabled = true;
        }
    } else {
        if (Linear16RadioButton->Enabled) {
            Linear16RadioButton->Enabled = false;
        }
        if (Linear16RadioButton->Checked) {
            noneChecked = true;
        }
    }
    if (AllowedColorSpacesMask & RGBOPT_CS_LINEAR_10_FLAG) {
        if (!Linear10RadioButton->Enabled) {
            Linear10RadioButton->Enabled = true;
        }
    } else {
        if (Linear10RadioButton->Enabled) {
            Linear10RadioButton->Enabled = false;
        }
        if (Linear10RadioButton->Checked) {
            noneChecked = true;
        }
    }
    if (AllowedColorSpacesMask & RGBOPT_CS_LOG_10_FLAG) {
        if (!Log10RadioButton->Enabled) {
            Log10RadioButton->Enabled = true;
        }
    } else {
        if (Log10RadioButton->Enabled) {
            Log10RadioButton->Enabled = false;
        }
        if (Log10RadioButton->Checked) {
            noneChecked = true;
        }
    }
    if (AllowedColorSpacesMask & RGBOPT_CS_LINEAR_12_FLAG) {
        if (!Linear12RadioButton->Enabled) {
            Linear12RadioButton->Enabled = true;
        }
    } else {
        if (Linear12RadioButton->Enabled) {
            Linear12RadioButton->Enabled = false;
        }
        if (Linear12RadioButton->Checked) {
            noneChecked = true;
        }
    }
    if (AllowedColorSpacesMask & RGBOPT_CS_LOG_12_FLAG) {
        if (!Log12RadioButton->Enabled) {
            Log12RadioButton->Enabled = true;
        }
    } else {
        if (Log12RadioButton->Enabled) {
            Log12RadioButton->Enabled = false;
        }
        if (Log12RadioButton->Checked) {
            noneChecked = true;
        }
    }
    if (noneChecked) {
        // Biggest to smallest
        if (Linear16RadioButton->Enabled) {
            Linear16RadioButton->Checked = true;
        } else if (Linear12RadioButton->Enabled) {
            Linear12RadioButton->Checked = true;
        } else if (Log12RadioButton->Enabled) {
            Log12RadioButton->Checked = true;
        } else if (Linear10RadioButton->Enabled) {
            Linear10RadioButton->Checked = true;
        } else if (Log10RadioButton->Enabled) {
            Log10RadioButton->Checked = true;
        } else if (Linear8RadioButton->Enabled) {
            Linear8RadioButton->Checked = true;
        }
    }
    --InhibitCallbacks;

    /*
     * The ColorSpace setting can affect the others
     */
    ConformRange();
    ConformGamma();
}
//---------------------------------------------------------------------------

void TRgbFileOptionsDialog::ConformRange()
{
    int minValue = 0;
    int minMinValue = 0;
    int maxValue = 0;
    int maxMaxValue = 65535;
    ++InhibitCallbacks;

    if (Linear8RadioButton->Checked) {
        RangeFullRadioButton->Enabled = true;
        RangeFullRadioButton->Caption = "Full Range (0 - 255)";
        RangeRestrictedRadioButton->Enabled = true;
        RangeRestrictedRadioButton->Caption = "Restricted Range (16 - 240)";
        RangeCustomRadioButton->Enabled = true;
        maxMaxValue = 255;
    } else if (Linear10RadioButton->Checked || Log10RadioButton->Checked) {
        RangeFullRadioButton->Enabled = true;
        RangeFullRadioButton->Caption = "Full Range (0 - 1023)";
        RangeRestrictedRadioButton->Enabled = true;
        RangeRestrictedRadioButton->Caption = "Restricted Range (95 - 685)";
        RangeCustomRadioButton->Enabled = true;
        maxMaxValue = 1023;
    } else if (Linear12RadioButton->Checked || Log12RadioButton->Checked) {
        RangeFullRadioButton->Enabled = true;
        RangeFullRadioButton->Caption = "Full Range (0 - 4096)";
        RangeRestrictedRadioButton->Enabled = true;
        RangeRestrictedRadioButton->Caption = "Restricted Range (256 - 3840)";
        RangeCustomRadioButton->Enabled = true;
        maxMaxValue = 4095;
    } else /* Linear16 */{
        RangeFullRadioButton->Enabled = true;
        RangeFullRadioButton->Caption = "Full Range (0 - 65535)";
        RangeRestrictedRadioButton->Enabled = false;
        RangeRestrictedRadioButton->Caption = "Restricted Range";
        RangeCustomRadioButton->Enabled = true;
    }
    if (RangeCustomRadioButton->Enabled && RangeCustomRadioButton->Checked) {
        PixelValueRangeMinVDoubleEdit->Enabled = true;
        RangeMinUpDown->Enabled = true;
        PixelValueRangeMaxVDoubleEdit->Enabled = true;
        RangeMaxUpDown->Enabled = true;
    } else {
        PixelValueRangeMinVDoubleEdit->Enabled = false;
        RangeMinUpDown->Enabled = false;
        PixelValueRangeMaxVDoubleEdit->Enabled = false;
        RangeMaxUpDown->Enabled = false;
    }
    minValue = (int) PixelValueRangeMinVDoubleEdit->dValue;
    maxValue = (int) PixelValueRangeMaxVDoubleEdit->dValue;

    if (minValue < minMinValue) minValue = minMinValue;
    if (minValue >= maxValue) maxValue = minValue + 1;
    if (maxValue > maxMaxValue) maxValue = maxMaxValue;
    if (maxValue <= minValue) minValue = maxValue - 1;

    if (minValue != (int) PixelValueRangeMinVDoubleEdit->dValue) {
        PixelValueRangeMinVDoubleEdit->dValue = (double) minValue;
    }
    if (maxValue != (int) PixelValueRangeMaxVDoubleEdit->dValue) {
        PixelValueRangeMaxVDoubleEdit->dValue = (double) maxValue;
    }

    --InhibitCallbacks;
}
//---------------------------------------------------------------------------

void TRgbFileOptionsDialog::ConformGamma()
{
    double dValue = 0;
    ++InhibitCallbacks;

    Gamma10RadioButton->Enabled = true;
    Gamma17RadioButton->Enabled = true;
    Gamma22222RadioButton->Enabled = true;
    GammaCustomRadioButton->Enabled = true;

    if (GammaCustomRadioButton->Enabled && GammaCustomRadioButton->Checked) {
        GammaVDoubleEdit->Enabled = true;
        GammaCustomUpDown->Enabled = true;
    } else {
        GammaVDoubleEdit->Enabled = false;
        GammaCustomUpDown->Enabled = false;
    }
    dValue = GammaVDoubleEdit->dValue;
    if (dValue < RGBOPT_GAMMA_CUSTOM_MIN) dValue = RGBOPT_GAMMA_CUSTOM_MIN;
    if (dValue > RGBOPT_GAMMA_CUSTOM_MAX) dValue = RGBOPT_GAMMA_CUSTOM_MAX;

    if (dValue != GammaVDoubleEdit->dValue) {
        GammaVDoubleEdit->dValue = dValue;
    }

    --InhibitCallbacks;
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::Linear10RadioButtonClick(
      TObject *Sender)
{
    PixelValueRangeMinVDoubleEdit->dValue = (double) CustomMin10;
    PixelValueRangeMaxVDoubleEdit->dValue = (double) CustomMax10;
    ConformColorSpace();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::Log10RadioButtonClick(
      TObject *Sender)
{
    PixelValueRangeMinVDoubleEdit->dValue = (double) CustomMin10;
    PixelValueRangeMaxVDoubleEdit->dValue = (double) CustomMax10;
    ConformColorSpace();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::Linear12RadioButtonClick(
      TObject *Sender)
{
    PixelValueRangeMinVDoubleEdit->dValue = (double) CustomMin12;
    PixelValueRangeMaxVDoubleEdit->dValue = (double) CustomMax12;
    ConformColorSpace();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::Log12RadioButtonClick(
      TObject *Sender)
{
    PixelValueRangeMinVDoubleEdit->dValue = (double) CustomMin12;
    PixelValueRangeMaxVDoubleEdit->dValue = (double) CustomMax12;
    ConformColorSpace();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::Linear8RadioButtonClick(
      TObject *Sender)
{
    PixelValueRangeMinVDoubleEdit->dValue = (double) CustomMin8;
    PixelValueRangeMaxVDoubleEdit->dValue = (double) CustomMax8;
    ConformColorSpace();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::Linear16RadioButtonClick(
      TObject *Sender)
{
    PixelValueRangeMinVDoubleEdit->dValue = (double) CustomMin16;
    PixelValueRangeMaxVDoubleEdit->dValue = (double) CustomMax16;
    ConformColorSpace();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::RangeFullRadioButtonClick(
      TObject *Sender)
{
    ConformRange();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::RangeRestrictedRadioButtonClick(
      TObject *Sender)
{
    ConformRange();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::RangeCustomRadioButtonClick(
      TObject *Sender)
{
    ConformRange();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::Gamma10RadioButtonClick(
      TObject *Sender)
{
    ConformGamma();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::Gamma17RadioButtonClick(
      TObject *Sender)
{
    ConformGamma();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::Gamma22222RadioButtonClick(
      TObject *Sender)
{
    ConformGamma();
}
//---------------------------------------------------------------------------

void __fastcall TRgbFileOptionsDialog::GammaCustomRadioButtonClick(
      TObject *Sender)
{
    ConformGamma();
}
//---------------------------------------------------------------------------



void __fastcall TRgbFileOptionsDialog::FormShow(TObject *Sender)
{
   FocusControl(OkButton);     // No idea why it starts out focused on Cancel!   
}
//---------------------------------------------------------------------------

