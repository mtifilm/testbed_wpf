object ThreadControlDialog: TThreadControlDialog
  Left = -682
  Top = 351
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Thread Control'
  ClientHeight = 263
  ClientWidth = 241
  Color = clBtnFace
  DefaultMonitor = dmPrimary
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inline ConcurrentFramesTrackEdit: TTrackEditFrame
    Left = 12
    Top = 8
    Width = 217
    Height = 51
    TabOrder = 0
    inherited MinLabel: TLabel
      Width = 6
      Caption = '1'
    end
    inherited MaxLabel: TLabel
      Left = 164
      Width = 6
      Caption = '8'
    end
    inherited TitleLabel: TLabel
      Width = 89
      Caption = 'Concurrent Frames'
    end
    inherited TrackBar: TTrackBar
      Max = 8
      Min = 1
      Position = 1
    end
  end
  inline ProcessingThreadsTrackEdit: TTrackEditFrame
    Left = 12
    Top = 68
    Width = 217
    Height = 51
    TabOrder = 1
    inherited MinLabel: TLabel
      Width = 6
      Caption = '1'
    end
    inherited MaxLabel: TLabel
      Left = 158
      Width = 12
      Caption = '16'
    end
    inherited TitleLabel: TLabel
      Width = 94
      Caption = 'Processing Threads'
    end
    inherited TrackBar: TTrackBar
      Max = 16
      Min = 1
      Position = 1
    end
  end
  inline SlicesPerThreadTrackEdit: TTrackEditFrame
    Left = 12
    Top = 160
    Width = 217
    Height = 51
    TabOrder = 2
    inherited MinLabel: TLabel
      Width = 6
      Caption = '1'
    end
    inherited MaxLabel: TLabel
      Left = 158
      Width = 12
      Caption = '32'
    end
    inherited TitleLabel: TLabel
      Width = 84
      Caption = 'Slices Per Thread'
    end
    inherited TrackBar: TTrackBar
      Max = 32
      Min = 1
      Position = 1
    end
  end
  object RowSlicesRadioButton: TRadioButton
    Left = 20
    Top = 132
    Width = 85
    Height = 17
    Caption = 'Row Slices'
    Checked = True
    TabOrder = 3
    TabStop = True
  end
  object ColumnSlicesRadioButton: TRadioButton
    Left = 124
    Top = 132
    Width = 101
    Height = 17
    Caption = 'Column Slices'
    TabOrder = 4
  end
  object NoopCheckBox: TCheckBox
    Left = 20
    Top = 228
    Width = 205
    Height = 17
    Caption = 'No-op processing (measure overhead)'
    TabOrder = 5
  end
end
