//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClipSchemePropertiesForm.h"
#include "IniFile.h"
#include "MTIio.h"
#include "BinManager.h"
#include <string.h>
#include <stdio.h>


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TClipSchemePropForm *ClipSchemePropForm;
//---------------------------------------------------------------------------
int TClipSchemePropForm::SetScheme(const string clipSchemeName)
{
   //Use the Clip Scheme name to get the actual ini file
   CClipSchemeManager clipSchemeMgr;
   CIniFile *iniFile = clipSchemeMgr.getClipSchemeIniFile(clipSchemeName);

   if (iniFile == 0)
      {
      std::cerr << "ERROR: Cannot open Clip Initialization File " << clipSchemeName
           << endl;
      return 1;
      }

   //Read the necessary values from the Ini file and set the ValueLabels

   NameValueLabel->Caption            =  iniFile->ReadString("ClipSchemeInfo",
                                         "ClipSchemeName", "---").c_str();
   TypeValueLabel->Caption            = iniFile->ReadString("ClipSchemeInfo",
                                        "ClipSchemeType", "---").c_str();
   DescriptionValueLabel->Caption     = iniFile->ReadString("ClipInformation",
                                        "Description", "---").c_str();
   ImageFormatValueLabel->Caption     = iniFile->ReadString("MainVideo\\ImageFormat",
                                        "ImageFormatType", "---").c_str();
   ColorSpaceValueLabel->Caption      = iniFile->ReadString("MainVideo\\ImageFormat",
                                        "ColorSpace", "---").c_str();
   MinRYValueLabel->Caption           = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentMin[0]", "---").c_str();
   MinGUValueLabel->Caption           = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentMin[1]", "---").c_str();
   MinBVValueLabel->Caption           = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentMin[2]", "---").c_str();
   MaxRYValueLabel->Caption           = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentMax[0]", "---").c_str();
   MaxGUValueLabel->Caption           = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentMax[1]", "---").c_str();
   MaxBVValueLabel->Caption           = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentMax[2]", "---").c_str();
   BlackRYValueLabel->Caption         = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentBlack[0]", "---").c_str();
   BlackGUValueLabel->Caption         = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentBlack[1]", "---").c_str();
   BlackBVValueLabel->Caption         = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentBlack[2]", "---").c_str();
   WhiteRYValueLabel->Caption         = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentWhite[0]", "---").c_str();
   WhiteGUValueLabel->Caption         = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentWhite[1]", "---").c_str();
   WhiteBVValueLabel->Caption         = iniFile->ReadString("MainVideo\\ImageFormat\\ColorSpace",
                                        "ComponentWhite[2]", "---").c_str();
   NumberChannelsValueLabel->Caption  = iniFile->ReadString("AudioTrack1\\AudioFormat",
                                        "ChannelCount", "---").c_str();
   SampleRateValueLabel->Caption         = iniFile->ReadString("AudioTrack1\\AudioFormat",
                                        "SampleRate", "---").c_str();


   //Derive the separate pixel component labels from the PixelComponents
   //SD, HD and .yuv are YUV

   string PixelComponentsString =
          iniFile->ReadString("MainVideo\\ImageFormat", "PixelComponents", "---").c_str();

#ifdef GAAAACK__THIS_IS_NOT_THE_RIGHT_WAY_TO_DO_THIS

   if (TypeValueLabel->Caption == "SD" ||
       TypeValueLabel->Caption == "HD")
     {
      PixelComponentsValueLabel->Caption = "YUV";
      RYLabel->Caption                   = "Y";
      GULabel->Caption                   = "U";
      BVLabel->Caption                   = "V";
     }
   //HSDL are RGB
   else if (TypeValueLabel->Caption == "HSDL")
     {
      PixelComponentsValueLabel->Caption = "RGB";
      RYLabel->Caption                   = "R";
      GULabel->Caption                   = "G";
      BVLabel->Caption                   = "B";
     }
   //File Clip Schemes need further inspection
   else if (TypeValueLabel->Caption == "FILE")
     {
      //YUV files?
      if (PixelComponentsString == "YUV422")
         {
          PixelComponentsValueLabel->Caption = "YUV";
          RYLabel->Caption                   = "Y";
          GULabel->Caption                   = "U";
          BVLabel->Caption                   = "V";
         }
      //Monochrome files?
      else if (PixelComponentsString == "LUMINANCE")
         {
          PixelComponentsValueLabel->Caption = "LUMINANCE";
          RYLabel->Caption                   = "Y";
          GULabel->Caption                   = "";
          BVLabel->Caption                   = "";
         }
      //RGB pixel components?
      else if (PixelComponentsString == "RGB")
         {
          PixelComponentsValueLabel->Caption = "RGB";
          RYLabel->Caption                   = "R";
          GULabel->Caption                   = "G";
          BVLabel->Caption                   = "B";
         }
      //BGR pixel components?
      else if (PixelComponentsString == "BGR")
         {
          PixelComponentsValueLabel->Caption = "BGR";
          RYLabel->Caption                   = "B";
          GULabel->Caption                   = "G";
          BVLabel->Caption                   = "R";
         }
      //unknown or can't be determined
      else
         {
          PixelComponentsValueLabel->Caption = "---";
          RYLabel->Caption                   = "---";
          GULabel->Caption                   = "---";
          BVLabel->Caption                   = "---";
         }
     }
#else
   EPixelComponents PixelComponents = CImageInfo::queryPixelComponents(
                                      PixelComponentsString);
   if (PixelComponents == IF_PIXEL_COMPONENTS_INVALID)
      {
       EImageFormatType ImageFormatType = CImageInfo::queryImageFormatType(
                                     StringToStdString(ImageFormatValueLabel->Caption));
       PixelComponents = CImageInfo::queryNominalPixelComponents(
                                     ImageFormatType);
      }
   if (PixelComponents != IF_PIXEL_COMPONENTS_INVALID)
      {
       PixelComponentsString = CImageInfo::queryPixelComponentsName(
                                           PixelComponents);
      }
   PixelComponentsValueLabel->Caption = PixelComponentsString.c_str();

   switch (PixelComponents)
      {
       default:
       case IF_PIXEL_COMPONENTS_INVALID:
          // "Can't happen"
          RYLabel->Caption                   = "1";
          GULabel->Caption                   = "2";
          BVLabel->Caption                   = "3";
          break;
//       case IF_PIXEL_COMPONENTS_LUMINANCE:
       case IF_PIXEL_COMPONENTS_Y:
       case IF_PIXEL_COMPONENTS_YYY:
          RYLabel->Caption                   = "Y";
          GULabel->Caption                   = "";
          BVLabel->Caption                   = "";
          break;
       case IF_PIXEL_COMPONENTS_RGB:
       case IF_PIXEL_COMPONENTS_RGBA:
          RYLabel->Caption                   = "R";
          GULabel->Caption                   = "G";
          BVLabel->Caption                   = "B";
          break;
       case IF_PIXEL_COMPONENTS_YUV422:
       case IF_PIXEL_COMPONENTS_YUV4224:
       case IF_PIXEL_COMPONENTS_YUV444:
          RYLabel->Caption                   = "Y";
          GULabel->Caption                   = "U";
          BVLabel->Caption                   = "V";
          break;
       case IF_PIXEL_COMPONENTS_BGR:
       case IF_PIXEL_COMPONENTS_BGRA:
          // Oy, bad component names
          RYLabel->Caption                   = "B";
          GULabel->Caption                   = "G";
          BVLabel->Caption                   = "R";
          break;
      }
#endif


   //Derive the BitsPerComponent from the PixelPacking
   //Right now, we only support 8, 10, and 16 bit files, but there may be a better
   //way to do this to support a general case, if one cares to do that...

   string PixelPackingString = iniFile->ReadString("MainVideo\\ImageFormat",
                                        "PixelPacking", "---").c_str();

#ifdef GAAAACK__THIS_IS_NOT_THE_RIGHT_WAY_TO_DO_THIS

   if (strstr(PixelPackingString.c_str(), "8Bits") != NULL)
      {
       BitsPerComponentValueLabel->Caption          = "8";
      }
   else if (strstr(PixelPackingString.c_str(), "10Bits") != NULL)
      {
       BitsPerComponentValueLabel->Caption          = "10";
      }
   else if (strstr(PixelPackingString.c_str(), "16Bits") != NULL)
      {
       BitsPerComponentValueLabel->Caption          = "16";
      }
   else
      {
       BitsPerComponentValueLabel->Caption          = "---";
      }
#else
   EPixelPacking PixelPacking = CImageInfo::queryPixelPacking(
                                            PixelPackingString);
   if (PixelPacking != IF_PIXEL_PACKING_INVALID)
      {
       BitsPerComponentValueLabel->Caption = CImageInfo::queryBitsPerComponent(
                                                         PixelPacking);
      }
   else
      {
       // Bad clip scheme - PixelPacking is a required image format field
       BitsPerComponentValueLabel->Caption = "---";
      }
#endif


   iniFile->FileName = ""; // Don't write the ini file
   DeleteIniFile(iniFile);  // Don't need ini file any more
   return 0;
}
//---------------------------------------------------------------------------

void TClipSchemePropForm::FillClipPropLabelValues()
{
 //Set the values for the labels in the Clip Scheme Properties Form
 NameValueLabel->Caption            = "name";
 TypeValueLabel->Caption            = "type";
 DescriptionValueLabel->Caption     = "label";
 ImageFormatValueLabel->Caption     = "format"; //clipInitInfo.getImageFormatName();
 PixelComponentsValueLabel->Caption = "Pixel Components";
 ColorSpaceValueLabel->Caption      = "ColorSpace";
 MinRYValueLabel->Caption           = "0";
 MinGUValueLabel->Caption           = "0";
 MinBVValueLabel->Caption           = "0";
 MaxRYValueLabel->Caption           = "255";
 MaxGUValueLabel->Caption           = "255";
 MaxBVValueLabel->Caption           = "255";
 BlackRYValueLabel->Caption         = "4";
 BlackGUValueLabel->Caption         = "4";
 BlackBVValueLabel->Caption         = "4";
 WhiteRYValueLabel->Caption         = "240";
 WhiteGUValueLabel->Caption         = "240";
 WhiteBVValueLabel->Caption         = "240";

 //Derive the separate pixel component labels from the PixelComponents
 RYLabel->Caption                   = "R";
 GULabel->Caption                   = "G";
 BVLabel->Caption                   = "B";

 //Derive the BitsPerComponent from the PixelPacking
 BitsPerComponentValueLabel->Caption          = "8";

}

//---------------------------------------------------------------------------
__fastcall TClipSchemePropForm::TClipSchemePropForm(TComponent* Owner)
        : TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TClipSchemePropForm::Button1Click(TObject *Sender)
{
 ModalResult = mrOk;        
}
//---------------------------------------------------------------------------

