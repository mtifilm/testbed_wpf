//---------------------------------------------------------------------------


#pragma hdrstop

#include <vcl.h>
#include "MTIWinInterface.h"
#include "MTIio.h"

// indexes for Screen->Cursors[] array
#define PICKER_CURSOR_INDEX 1000
#define OPEN_HAND_CURSOR_INDEX 1001
#define CLOSED_HAND_CURSOR_INDEX 1002
#define MAGNIFIER_CURSOR_INDEX 1003


//---------------------------------------------------------------------------

#pragma package(smart_init)

int CBusyCursor::BusyCursorType = 0;   //global declaration

//-------------SetBusyCursorToDefault-------------John Mertus----Sep 2003-----

	   bool SetBusyCursorToDefault(void)

//  This restores the busy cursor to the default setting
//
//****************************************************************************
{
	 HCURSOR cr = LoadCursor(NULL, IDC_WAIT);
     if (cr == NULL) return false;
     Screen->Cursors[crHourGlass] = cr;
	 CBusyCursor::setBusyCursorType(0);
     return true;
}


static TMemoryStream *rsBusyCursor = NULL;

//-------------SetBusyCursorToBusyArrows----------John Mertus----Sep 2003-----

       bool SetBusyCursorToBusyArrows(void)

//  This set the busy cursor the MTI default
//  This should be called only a few times, very expensive
//
//****************************************************************************
{

		rsBusyCursor = new TMemoryStream;
        HINSTANCE hDll = ::LoadLibrary("core_code.dll");
        try
		  {
			wchar_t tempCursor[] = L"ANICURSOR";
			TResourceStream *ts = new TResourceStream((int)hDll, String("BusyArrows"), tempCursor);
			rsBusyCursor->LoadFromStream(ts);
            FreeLibrary(hDll);
            delete ts;
          }
        catch (...)
          {
            FreeLibrary(hDll);
            return false;
          }


    char DirBuf[MAX_PATH], FileBuf[MAX_PATH];
    GetTempPath(MAX_PATH, DirBuf);
    GetTempFileName(DirBuf, "tmp", 0, FileBuf);
    rsBusyCursor->SaveToFile(FileBuf);
	Screen->Cursors[crHourGlass] = LoadCursorFromFile(FileBuf);
    DeleteFile(FileBuf);
    CBusyCursor::setBusyCursorType(1);
    return true;
}



//
//  This is the smile for the private data for the CBusyCursor class
//
struct CBusyCursor::Cheshire
{
  TCursor crOldCursor;
  int Nest;
};

//------------------CBusyCursor-------------------John Mertus----Dec 2002-----

      CBusyCursor::CBusyCursor(bool bBusy)

//  Create with bBusy true sets the cursor to busy.
//
//****************************************************************************
{
   Smile = new Cheshire;
   Smile->crOldCursor = Screen->Cursor;
   Smile->Nest = 0;
   if (bBusy) Busy();
}

//----------------------Busy----------------------John Mertus----Dec 2002-----

     void CBusyCursor::Busy()

//   This displays the busy cursor.
//   Calls to Busy/NotBusy must be nested
//
//****************************************************************************
{
  Smile->Nest++;
  Screen->Cursor = crHourGlass;
}

//--------------------NotBusy---------------------John Mertus----Dec 2002-----

   void CBusyCursor::NotBusy()

//  Reset the cursor back to normal
//   Calls to Busy/NotBusy must be nested
//
//****************************************************************************
{
  if (--Smile->Nest > 0) return;
  Screen->Cursor = Smile->crOldCursor;
}



int  CBusyCursor::getBusyCursorType(void)
// this method is necessary to keep track of BusyCursor changes.
// these changes are very expensive, so we only want to change them
// when necessary.
// 0 = default, 1 = arrows, 2 = cross

{
  return BusyCursorType;
}

void CBusyCursor::setBusyCursorType(int BCtype)
// this method is necessary to keep track of BusyCursor changes.
// these changes are very expensive, so we only want to change them
// when necessary.
// 0 = default, 1 = arrows, 2 = cross

{
  BusyCursorType = BCtype;
}




//------------------CBusyCursor-------------------John Mertus----Dec 2002-----

     CBusyCursor::~CBusyCursor()

//  Destroy, restore to the normal state
//
//****************************************************************************
{
  while (Smile->Nest > 0) NotBusy();
  delete Smile;
}

//------------------CBusyCursor-------------------John Mertus----Dec 2003-----

      void LoadMainIcons(void)

// Load the icon, this is because of an apparent bug in Borland that
// doesn't load it if VCL lives in DLL's
//
//****************************************************************************
{
        Application->Icon->Handle = (HICON)LoadImage(HInstance,
                         "MainIcon",
                         IMAGE_ICON,
                         32,32,
                         LR_DEFAULTSIZE);
}

//------------PickerCursor----------------------msawkar

static TMemoryStream *rsPickerCursor = NULL;


int PickerCursor()
{
  if (rsPickerCursor == NULL) {
		rsPickerCursor = new TMemoryStream;
		HINSTANCE hDll = ::LoadLibrary("core_code.dll");
		try
		{
		 wchar_t tempCursor[] = L"RT_CURSOR";
		 TResourceStream *ts = new TResourceStream((int)hDll, String("PICKER"), tempCursor);
			rsPickerCursor->LoadFromStream(ts);
			FreeLibrary(hDll);
			delete ts;
		 }
		catch (...)
		 {
			FreeLibrary(hDll);
		 }

		 char DirBuf[MAX_PATH], FileBuf[MAX_PATH];
		 GetTempPath(MAX_PATH, DirBuf);
		 GetTempFileName(DirBuf, "tmp", 0, FileBuf);
		 rsPickerCursor->SaveToFile(FileBuf);
		 HCURSOR cr = LoadCursorFromFile(FileBuf);
		 Screen->Cursors[PICKER_CURSOR_INDEX] = cr;
		 DeleteFile(FileBuf);
  }
  return PICKER_CURSOR_INDEX;
}

//------------ClosedHandCursor----------------------
// code duplicated because I just don't fucking care.

static TMemoryStream *rsClosedHandCursor = NULL;

int ClosedHandCursor()
{
  if (rsClosedHandCursor == NULL) {
		rsClosedHandCursor = new TMemoryStream;
		HINSTANCE hDll = ::LoadLibrary("core_code.dll");
		try
		{
		 wchar_t tempCursor[] = L"RT_CURSOR";
		 TResourceStream *ts = new TResourceStream((int)hDll, String("CLOSEDHAND"), tempCursor);
			rsClosedHandCursor->LoadFromStream(ts);
			FreeLibrary(hDll);
			delete ts;
		 }
		catch (...)
		 {
			FreeLibrary(hDll);
		 }

		 char DirBuf[MAX_PATH], FileBuf[MAX_PATH];
		 GetTempPath(MAX_PATH, DirBuf);
		 GetTempFileName(DirBuf, "tmp", 0, FileBuf);
		 rsClosedHandCursor->SaveToFile(FileBuf);
		 HCURSOR cr = LoadCursorFromFile(FileBuf);
		 Screen->Cursors[CLOSED_HAND_CURSOR_INDEX] = cr;
		 DeleteFile(FileBuf);
  }
  return CLOSED_HAND_CURSOR_INDEX;
}

//------------OpenHandCursor----------------------
// code duplicated because I just don't fucking care.

static TMemoryStream *rsOpenHandCursor = NULL;

int OpenHandCursor()
{
  if (rsOpenHandCursor == NULL) {
		rsOpenHandCursor = new TMemoryStream;
		HINSTANCE hDll = ::LoadLibrary("core_code.dll");
		try
		{
		 wchar_t tempCursor[] = L"RT_CURSOR";
		 TResourceStream *ts = new TResourceStream((int)hDll, String("OPENHAND"), tempCursor);
			rsOpenHandCursor->LoadFromStream(ts);
			FreeLibrary(hDll);
			delete ts;
		 }
		catch (...)
		 {
			FreeLibrary(hDll);
		 }

		 char DirBuf[MAX_PATH], FileBuf[MAX_PATH];
		 GetTempPath(MAX_PATH, DirBuf);
		 GetTempFileName(DirBuf, "tmp", 0, FileBuf);
		 rsOpenHandCursor->SaveToFile(FileBuf);
		 HCURSOR cr = LoadCursorFromFile(FileBuf);
		 Screen->Cursors[OPEN_HAND_CURSOR_INDEX] = cr;
		 DeleteFile(FileBuf);
  }
  return OPEN_HAND_CURSOR_INDEX;
}


//------------MagnifierCursor----------------------
// code duplicated because I just don't fucking care.

static TMemoryStream *rsMagnifierCursor = NULL;

int MagnifierCursor()
{
  if (rsMagnifierCursor == NULL) {
		rsMagnifierCursor = new TMemoryStream;
		HINSTANCE hDll = ::LoadLibrary("core_code.dll");
		try
		{
		 wchar_t tempCursor[] = L"RT_CURSOR";
		 TResourceStream *ts = new TResourceStream((int)hDll, String("MAGNIFIER"), tempCursor);
			rsMagnifierCursor->LoadFromStream(ts);
			FreeLibrary(hDll);
			delete ts;
		 }
		catch (...)
		 {
			FreeLibrary(hDll);
		 }

		 char DirBuf[MAX_PATH], FileBuf[MAX_PATH];
		 GetTempPath(MAX_PATH, DirBuf);
		 GetTempFileName(DirBuf, "tmp", 0, FileBuf);
		 rsMagnifierCursor->SaveToFile(FileBuf);
		 HCURSOR cr = LoadCursorFromFile(FileBuf);
		 Screen->Cursors[MAGNIFIER_CURSOR_INDEX] = cr;
		 DeleteFile(FileBuf);
  }
  return MAGNIFIER_CURSOR_INDEX;
}



// Turn off the warning about "Function should return a value"
// as this function really does return a defined value
//----------------IsDebuggerAttached---------------John Mertus----July 1999----

     bool  IsDebuggerAttached(void)

//  This is a converted from pascal
//    Returns true if the THREAD has an attached debugger
//
//***************************************************************************
{
	return IsDebuggerPresent();
}


//-------------SetBusyCursorToBusyCross----------John Mertus----Sep 2003-----

	   bool SetBusyCursorToBusyCross(void)

//  This set the busy cursor the Cross hair
//  This should be called only a few times, very expensive
//
//****************************************************************************
{
		rsBusyCursor = new TMemoryStream;
		HINSTANCE hDll = ::LoadLibrary("core_code.dll");
		try
		  {
			wchar_t tempCursor[] = L"ANICURSOR";
			TResourceStream *ts = new TResourceStream((int)hDll, String("ClearCross"), tempCursor);
			if (ts == 0)
			{
			   FreeLibrary(hDll);
			   return false;
			}
			rsBusyCursor->LoadFromStream(ts);
			FreeLibrary(hDll);
			delete ts;
		  }
		catch (...)
		  {
			FreeLibrary(hDll);
			return false;
		  }


	char DirBuf[MAX_PATH], FileBuf[MAX_PATH];
	GetTempPath(MAX_PATH, DirBuf);
	GetTempFileName(DirBuf, "tmp", 0, FileBuf);
	rsBusyCursor->SaveToFile(FileBuf);
	Screen->Cursors[crHourGlass] = LoadCursorFromFile(FileBuf);
	DeleteFile(FileBuf);
	CBusyCursor::setBusyCursorType(2);
	return true;
}
