//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClipListViewUnit.h"

#include "BinDir.h"
#include "IniFile.h"
#include "BinManager.h"
#include "bthread.h"
#include "MTIWinInterface.h"
#include "CommonGuiUtil.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIBusy"
#pragma resource "*.dfm"
//TClipListViewFrame *ClipListViewFrame;

// This (ClipListViewUnit.cpp) is a lightweight BinManager for use with
//   Convert-based items (e.g. Duplicate Frames).  According to Mike
//   Braca, someday, hopefully it will be integrated as a module of
//   the BinManager. - mpr

// Clip List Column Indices
#define COL_INDEX_CLIP_NAME        0
#define COL_INDEX_CLIP_STATUS      1
#define COL_INDEX_IMAGE_TYPE       2
#define COL_INDEX_IN_TIMECODE      3
#define COL_INDEX_OUT_TIMECODE     4
#define COL_INDEX_DURATION         5
#define COL_INDEX_CLIP_DESCRIPTION 6
#define COL_INDEX_CREATE_DATE      7
#define CLIP_LIST_COL_COUNT        8    // Number of columns in clip list

// Operational Mode
#define BM_MODE_NORMAL         0   // Normal "Bin Manager" mode
#define BM_MODE_SELECT_CLIP    1   // "Select a single clip" dialog mode
#define BM_MODE_SELECT_BIN     2   // "Select a bin" dialog mode

//---------------------------------------------------------------------------
__fastcall TClipListViewFrame::TClipListViewFrame(TComponent* Owner)
    : TFrame(Owner)
{
}
//---------------------------------------------------------------------------

void TClipListViewFrame::InitFromIniFile(const string &iniFileName)
{
    if (!iniFileName.empty()) {
        CIniFile *myIniFile = ::CreateIniFile(iniFileName);

        if (myIniFile != 0) {
        /***
            SelectedBinPath = myIniFile->ReadString("SavedSettings",
                                                    "SelectedBin", "");
        ***/
            MyIniFileName = iniFileName;
        }
    }
}
//---------------------------------------------------------------------------

int TClipListViewFrame::OK()
{
    int retVal = 0;

    if ((!MyIniFileName.empty()) && (SelectedClipName != "")) {
        CIniFile *myIniFile = ::CreateIniFile(MyIniFileName);

        if (myIniFile != 0) {
        /***
             myIniFile->WriteString("SavedSettings", "SelectedBin",
                                                        SelectedBinPath);
        ***/
        }
    }

    return retVal;
}
//---------------------------------------------------------------------------

void TClipListViewFrame::SetSelectedClip(const string& clipName)
{
    AnsiString ansiClipName(clipName.c_str());
    TListItem *item = 0;

    if (clipName != "") {
        item = ClipListView->FindCaption(/* start index        */ 0,
                                         /* search string      */ ansiClipName,
                                         /* match first part   */ false,
                                         /* include start item */ true,
                                         /* wrap bottom to top */ false);
    }
    if (item != 0) {
        /*
         * Select it!
         */
        item->Selected = true;

    } else if (ClipListView->SelCount > 0) {
        SelectedClipName = "";
        ClipListView->Selected->Selected = false;
    }
}
//---------------------------------------------------------------------------

string TClipListViewFrame::GetSelectedClip()
{
    return SelectedClipName;
}
//---------------------------------------------------------------------------

void TClipListViewFrame::RebuildClipList(const string &binPath)
{
    CAutoThreadLocker lock(BuildLock);

    SelectedBinPath = binPath;
    if (!BuilderIsRunning) {
        BuilderIsRunning = true;
        {   CAutoThreadUnlocker unlock(BuildLock);
            BusyArrows->Visible = true;
            BusyArrows->Busy = true;
            ArrowCheckTimer->Enabled = true;
            BuilderThreadId = BThreadSpawn(&StartBuilding, this);
        }
    }
}
//---------------------------------------------------------------------------

void TClipListViewFrame::SetClipFormatFilter(const string &filter)
{
    ClipFormatFilter = filter;
}

//---------------------------------------------------------------------------

/* static */
 void TClipListViewFrame::StartBuilding(void *vpAppData, void *vpReserved)
{
  if (BThreadBegin(vpReserved)) {
    exit(1);    // say what??!?
  }
  ((TClipListViewFrame *)vpAppData)->BuilderThread();
}
//---------------------------------------------------------------------------

void TClipListViewFrame::BuilderThread()
{
    //Hangs the app if I do this here:
    //BusyArrows->Visible = true;
    //BusyArrows->Busy = true;

    {   CAutoThreadLocker lock(BuildLock);
        string binPath;

        while (binPath != SelectedBinPath) {
            binPath = SelectedBinPath;
            {   CAutoThreadUnlocker unlock(BuildLock);
                buildClipList(binPath);
            }
        }
        BuilderThreadId = 0;
        BuilderIsRunning = 0;
    }
    //BusyArrows->Visible = false;
    //BusyArrows->Busy = false;
}
//---------------------------------------------------------------------------

void TClipListViewFrame::buildClipList(const string &binPath)
{
    CBinManager binMgr;
    CBinDir binDir;
    bool foundClip = binDir.findFirst(binPath, BIN_SEARCH_FILTER_CLIP);

    /*
     * Carefully clear the list to avoid stupid-looking column headers
     */
    for (int i = 0; i < CLIP_LIST_COL_COUNT; ++i) {
        if (i != COL_INDEX_CLIP_NAME && i !=  COL_INDEX_CLIP_DESCRIPTION) {
            // yes, this looks like a no-op, but it's not!
            ClipListView->Columns->Items[i]->Width =
                    ClipListView->Columns->Items[i]->Width;
        }
    }
    ClipListView->Items->Clear();

    /*
     * Add each of the bin's clips to the view
     */
    ClipListView->Items->BeginUpdate();     // show list all at once when done
    while(foundClip) {
        int status;
        CClipInitInfo *clipInitInfo = 0;

        /*
         * If the bin changed, bail out
         */
        {   CAutoThreadLocker lock(BuildLock);
            if (binPath != SelectedBinPath) {
                break;
            }
        }

        /*
         * Get a clipInitInfo that is filled with all sorts of
         * information about the clip, then add it
         */
        clipInitInfo = binMgr.openClipOrClipIni(binPath, binDir.fileName,
                                                &status);
        if (clipInitInfo != 0 && status == 0) {
            addClipToClipList(*clipInitInfo);
        } else {
            TRACE_1(errout << "Project Manager failed to open clip "
                           << binDir.fileName
                           << " in Bin " << binPath << " Status = " << status);
        }
        if (clipInitInfo != 0) {
            clipInitInfo->closeClip();   // no longer need opened clip
            delete clipInitInfo;
        }

        /*
         * Go on to the next Clip
         */
        foundClip = binDir.findNext();
    }

    /*
     * Set column widths to match the data
     */
    if (ClipListView->Items->Count > 0) {
        for (int i = 0; i < CLIP_LIST_COL_COUNT; ++i) {
            if (i != COL_INDEX_CLIP_NAME && i !=  COL_INDEX_CLIP_DESCRIPTION) {
                // ColumnTextWidth is a constant (-1)
                ClipListView->Columns->Items[i]->Width = ColumnTextWidth;
            }
        }
    }
    ClipListView->Items->EndUpdate();
}
//---------------------------------------------------------------------------

TListItem *TClipListViewFrame::addClipToClipList(
                        const CClipInitInfo &clipInitInfo)
{
    StringList newValues;
    TListItem *listItem = 0;
    TStrings *subItems = 0;

    newValues = makeClipListRowValues(clipInitInfo);
    if (newValues.size() < CLIP_LIST_COL_COUNT) {
        // ERROR, failed to get all the information
        return 0;
    }

    /*
     * Apply format filter -- I really want to gray out and disable the
     * unselectable items, but I don't know how to do that.
     * listItem->Cut = true; doesn't seem to work...
     */
    if (ClipFormatFilter != "*") {
        string clipFormat = clipInitInfo.getImageFormatName();

        if (ClipFormatFilter.size() > clipFormat.size()
        || ClipFormatFilter != clipFormat.substr(0, ClipFormatFilter.size()))
        {
            return 0;
        }
    }

    /*
     * Add a new item to the Clip List
     */
    ClipListView->Items->BeginUpdate();     // want single repaint at end
    listItem = ClipListView->Items->Add();                        // clip name
    listItem->Caption = newValues[COL_INDEX_CLIP_NAME].c_str();
    subItems = listItem->SubItems;

    /*
     * Enter text strings for Clip information columns, left to right
     */
    subItems->Add(newValues[COL_INDEX_CLIP_STATUS].c_str());      // status
    subItems->Add(newValues[COL_INDEX_IMAGE_TYPE].c_str());       // format
    subItems->Add(newValues[COL_INDEX_IN_TIMECODE].c_str());      // in
    subItems->Add(newValues[COL_INDEX_OUT_TIMECODE].c_str());     // out
    subItems->Add(newValues[COL_INDEX_DURATION].c_str());         // duration
    subItems->Add(newValues[COL_INDEX_CLIP_DESCRIPTION].c_str()); // desc
    subItems->Add(newValues[COL_INDEX_CREATE_DATE].c_str());      // date/time

    ClipListView->Items->EndUpdate();   // repaint now

    return listItem;
}

//---------------------------------------------------------------------------
// Make a single row of the Clip List by extracting information from
// a CClipInitInfo
//
StringList TClipListViewFrame::makeClipListRowValues(
                const CClipInitInfo &clipInitInfo)
{

   StringList newValues;
   string timeStr;

   /*
    * Enter text strings for Clip information columns, left to right
    */
   newValues.push_back(clipInitInfo.getClipName());             // clip name
   newValues.push_back(clipInitInfo.getClipStatusString());     // status
   newValues.push_back(clipInitInfo.getImageFormatName());      // format
   clipInitInfo.getInTimecode().getTimeString(timeStr);         // in
   newValues.push_back(timeStr);
   clipInitInfo.getOutTimecode().getTimeString(timeStr);        // out
   newValues.push_back(timeStr);
   clipInitInfo.getDurationTimecode().getTimeString(timeStr);   // duration
   newValues.push_back(timeStr);
   newValues.push_back(clipInitInfo.getDescription());          // description
   newValues.push_back(clipInitInfo.getCreateDateTime());       // date/time
   
   return newValues;
}
//---------------------------------------------------------------------------

void __fastcall TClipListViewFrame::SawDoubleClick(TObject *Sender)
{
    /*
     * Notify any interested parent that we detected a double click;
     * obviously, this is a cheap callback hack
     */
    DoubleClickNotifier->Checked = !DoubleClickNotifier->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TClipListViewFrame::ListItemChanged(TObject *Sender,
      TListItem *Item, TItemChange Change)
{
    bool selectionChanged = false;

    if (Item == NULL || Change != ctState) {
        return;     // short circuit
    }

    if (ClipListView->SelCount == 0) {
        if (SelectedClipName != "") {
            SelectedClipName = "";
            selectionChanged = true;
        }
    } else {
        TListItem *selectedItem = ClipListView->Selected;
        string selectedName = CppStringFromAnsiString(selectedItem->Caption);

        if (SelectedClipName == "" || SelectedClipName != selectedName) {
            SelectedClipName = selectedName;
            selectionChanged = true;
        }
    }
    if (selectionChanged) {
        /*
         * Notify any interested parent that the selection changed;
         * obviously, this is a cheap callback hack
         */
        SelectedClipChangedNotifier->Checked
                            = !SelectedClipChangedNotifier->Checked;
    }
}
//---------------------------------------------------------------------------
void __fastcall TClipListViewFrame::ClipListViewKeyPress(TObject *Sender,
      char &Key)
{
    if (Key == Char(VK_RETURN)) {
        SawDoubleClick(Sender);
    }
}
//---------------------------------------------------------------------------

void __fastcall TClipListViewFrame::ArrowCheckTimerTimer(TObject *Sender)
{
    if (BuilderIsRunning == 0) {
        BusyArrows->Visible = false;
        BusyArrows->Busy = false;
        ArrowCheckTimer->Enabled = false;
    }
}
//---------------------------------------------------------------------------

