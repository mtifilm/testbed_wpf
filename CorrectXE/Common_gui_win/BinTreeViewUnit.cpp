// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BinTreeViewUnit.h"

#include "BinDir.h"
#include "IniFile.h"
#include "MTIDialogs.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
// TBinTreeViewFrame *BinTreeViewFrame;

// ---------------------------------------------------------------------------
__fastcall TBinTreeViewFrame::TBinTreeViewFrame(TComponent* Owner) : TFrame(Owner)
{
}
// ---------------------------------------------------------------------------

void TBinTreeViewFrame::InitFromIniFile(const string &iniFileName)
{
   if (!iniFileName.empty())
   {
      CIniFile *myIniFile = ::CreateIniFile(iniFileName);

      if (myIniFile != 0)
      {
         string binPath = myIniFile->ReadString("SavedSettings", "SelectedBin", "");
         SetSelectedBin(binPath);
         DeleteIniFile(myIniFile);
         _myIniFileName = iniFileName;
      }
   }
}
// ---------------------------------------------------------------------------

int TBinTreeViewFrame::OK()
{
   int retVal = 0;

   if ((!_myIniFileName.empty()) && (_selectedBinPath != ""))
   {
      CIniFile *myIniFile = ::CreateIniFile(_myIniFileName);

      if (myIniFile != 0)
      {
         myIniFile->WriteString("SavedSettings", "SelectedBin", _selectedBinPath);
         DeleteIniFile(myIniFile);
      }
   }

   return retVal;
}
// ---------------------------------------------------------------------------

void TBinTreeViewFrame::RefreshBinTree()
{
//   DBTIMER(RefreshBinTree);

   // Build tree from scratch.
   BinTreeView->Items->Clear();

   // Get the list of Bin roots, but we only use the first one.
   StringList binList;
   CPMPGetBinRoots(&binList);
   if (binList.size() < 1)
   {
      MTIostringstream ostr;
		ostr << "No project directory configured " << endl
           << endl
           << "Please check that CPMP_SHARED_DIR " << endl
           << "environment variable is set correctly. ";
      _MTIErrorDialog(Handle, ostr.str());
      TRACE_0(errout << ostr.str());
      return;
   }

   string rootPath = binList[0];
   if (!DoesDirectoryExist(rootPath))
   {
      MTIostringstream ostr;
      ostr << "Can't find project directory" << endl
           << rootPath << endl
           << endl
           << "Please check that the drive is accessible. ";
      _MTIErrorDialog(Handle, ostr.str());
      TRACE_0(errout << ostr.str());
      return;
   }

   // The REAL bin manager does this.... we don't!
	// Check the storage available on the root node     // no
	//CheckRootNodeStorage(rootPath);                   // no

   // Add the root node.
   BinTreeView->Items->Add(nullptr, rootPath.c_str());
   TTreeNode *rootNode = BinTreeView->Items->Item[0];

	// Add a placeholder "dummy" child node (?why neeed if we're expanding below?)
   AddDummyBinTreeNodeChildIfNestedBins(rootPath, rootNode);

   // Expand the Root node so there is something to look at
   rootNode->Expand(false); // don't recurse!
   BinTreeView->AlphaSort();    // this does recurse.

   // Restore the selected node, or the root node if no selection is stored.
   SetSelectedBin(_selectedBinPath);
}
// ---------------------------------------------------------------------------

void TBinTreeViewFrame::SetSelectedBin(const string& binPath)
{
   // Highlight the selection if it's different; ignore if not
   if ((binPath != _selectedBinPath || BinTreeView->SelectionCount == 0)
   && (BinTreeView->Items != 0 && BinTreeView->Items->Count > 0))
   {
      TTreeNode *rootNode = BinTreeView->Items->Item[0];
      bool selectedSomething = false;

      if (_selectedBinPath != "")
      {
         TTreeNode *selectedBinNode = getBinTreeNodeFromBinPath(binPath);

         if (selectedBinNode != 0)
         {
            selectedBinNode->Selected = true;
            selectedSomething = true;
         }
      }

      if (!selectedSomething)
      {
         rootNode->Selected = true;
      }
   }

   _selectedBinPath = binPath;
}

// ---------------------------------------------------------------------------
string TBinTreeViewFrame::GetSelectedBin()
{
   if (_selectedBinPath == "")
   {
      StringList binList;

      CPMPGetBinRoots(&binList);
      if (binList.size() == 1)
      {
         SetSelectedBin(binList[0]);
      }
   }

   return _selectedBinPath;
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

void TBinTreeViewFrame::AddDummyBinTreeNodeChildIfNestedBins(string &binPath, TTreeNode * parentNode)
{
   CBinDir binDir;
   if (!binDir.hasAtLeastOneNestedBin(binPath))
   {
      TRACE_0(errout << "No nested bins in " << binPath);
      return;
   }

   TRACE_0(errout << "Add dummy node to bin " << binPath);
   TRACE_0(errout << "because we found nested bin " << (binDir.findFirst(binPath, BIN_SEARCH_FILTER_BIN), binDir.fileName));
   TTreeNode *dummyNode = BinTreeView->Items->AddChild(parentNode, "####DUMMY####");
   dummyNode->SelectedIndex = 1; // No idea what this is for!
}
// ---------------------------------------------------------------------------

void TBinTreeViewFrame::AddBinTreeNodeChildrenRecursively(string &binPath, TTreeNode * parentNode)
{
   CBinDir binDir;
   bool foundBin = binDir.findFirst(binPath, BIN_SEARCH_FILTER_BIN);

   while (foundBin)
   {
      // Add tree node
      TTreeNode *newNode = BinTreeView->Items->AddChild(parentNode, binDir.fileName.c_str());
      newNode->SelectedIndex = 1;

      // Now recurse for depth-first traversal of the Bin hierarchy
      string localPath = AddDirSeparator(binPath) + binDir.fileName;
      AddBinTreeNodeChildrenRecursively(localPath, newNode);

      // Go on to the next Bin at this level
      foundBin = binDir.findNext();
   }
}
// ---------------------------------------------------------------------------

void TBinTreeViewFrame::AddOneLevelOfBinTreeNodeChildren(string &binPath, TTreeNode *parentNode, bool forceRefresh)
{
   ///////////////////////////////////////////////////////////////////////////
   // NOTE: If the node already has any children, we assume it has been
   // expanded previously filled in so we will do nothing, unless:
   // - forceRefresh is true, or
   // - the sole child is the virtualization dummy node, in which case
   // the dummy node will be removed before filling in the real children.
   ///////////////////////////////////////////////////////////////////////////

   if (parentNode->Count > 0 && (forceRefresh || StringToStdString(parentNode->getFirstChild()->Text) == "####DUMMY####"))
   {
      parentNode->DeleteChildren();
   }

   if (parentNode->Count > 0)
   {
      return;
   }

   CBinDir binDir;
   bool foundBin = binDir.findFirst(binPath, BIN_SEARCH_FILTER_BIN);
   while (foundBin)
   {
      // Add tree node
      TTreeNode *newNode = BinTreeView->Items->AddChild(parentNode, binDir.fileName.c_str());
      newNode->SelectedIndex = 1; // ?

      // Now maybe add a dummy child if this bin has one or more nested bins.
      string localPath = AddDirSeparator(binPath) + binDir.fileName;

      TRACE_0(errout << "Adding real child node " << localPath);

      AddDummyBinTreeNodeChildIfNestedBins(localPath, newNode);

      // Go on to the next Bin at this level
      foundBin = binDir.findNext();
   }

   parentNode->AlphaSort();
}
// ---------------------------------------------------------------------------

string TBinTreeViewFrame::getBinPathFromBinTreeNode(TTreeNode * node)
{
   // Sanity.
   if (node == nullptr)
   {
      return "";
   }

   // Build path recursively. Recursion terminates at the root, which has no parent.
   TTreeNode *parent = node->Parent;
   string parentPath = parent ? AddDirSeparator(getBinPathFromBinTreeNode(node->Parent)) : "";

   return parentPath + StringToStdString(node->Text);
}
// ---------------------------------------------------------------------------

TTreeNode *TBinTreeViewFrame::getBinTreeNodeFromBinPath(const string& binPathString)
{
   // We assert that there is only one root node!
   // MTIassert(BinTreeView->Items->Count == 1);
//   DBTRACE(BinTreeView->Items->Count);

   if (BinTreeView->Items->Count == 0)
   {
      // Sanity - no root!
      return nullptr;
   }

   STDFILESYSTEM::path binPath(RemoveDirSeparator(binPathString));
   auto binPathIter = binPath.begin();

   // Start at the root node. We assume there is only one.
   TTreeNode *rootNode = BinTreeView->Items->Item[0];
   string currentNodePath = RemoveDirSeparator(StringToStdString(rootNode->Text));
   STDFILESYSTEM::path rootPath(currentNodePath);

   // Make sure binPath includes the root path. This also moves the
   // itereator past the root path part of the bin path.
   for (auto&pathPart : rootPath)
   {
//      DBTRACE2(pathPart, *binPathIter);
//      DBTRACE2(pathPart.string(), binPathIter->string());
      if (pathPart != *binPathIter++)
      {
         // Bad bin path.
//         DBTRACE2("Bad bin path 1", binPathString);
         return nullptr;
      }
   }

   TTreeNode *currentNode = rootNode;

   // Now descend the bin path branch to find the desired node.
   while (binPathIter != binPath.end())
   {
      string binPathComponent = (*binPathIter++).string();
//      DBTRACE(binPathComponent);

      // Note: if the node has children and hasn't been de-virtualized, the
      // child count here will be 1 (the dummy node) no matter how many
      // children the node really has.
      if (currentNode->Count == 0)
      {
         // Node has no children - bin path is bad.
//         DBTRACE2("Bad bin path 2", binPathString);
         return nullptr;
      }

      // De-virtualize the children if necessary.
      if (StringToStdString(currentNode->Item[0]->Text) == "####DUMMY####")
      {
         AddOneLevelOfBinTreeNodeChildren(currentNodePath, currentNode);
      }

      // Scan the current node's children to find a match with the
      // corresponding bin path component.
      TTreeNode *childNode = currentNode->getFirstChild();
      while (childNode)
      {
         if (StringToStdString(childNode->Text) == binPathComponent)
         {
            // Found it!
            break;
         }

         childNode = currentNode->GetNextChild(childNode);
      }

      if (childNode == nullptr)
      {
         // Matching child not found => bad bin path.
//         DBTRACE2("Bad bin path 3", binPathString);
         return nullptr;
      }

      // Down to the next level!
      currentNode = childNode;
      currentNodePath = AddDirSeparator(currentNodePath) + binPathComponent;
   }

   // Success!
   return currentNode;
}

// ---------------------------------------------------------------------------
void __fastcall TBinTreeViewFrame::SelectionChanged(TObject *Sender, TTreeNode *Node)
{
   if (Node == NULL)
   {
      return; // short circuit
   }


   /*
    * Create a complete path from the selected Bin Tree node
    */
   string path  = getBinPathFromBinTreeNode(Node);
   if (path == _selectedBinPath)
   {
      return;
   }
   _selectedBinPath = path;

   /*
    * Notify any interested parent that the selection changed;
    * obviously, this is a cheap callback hack
    */
   SelectionChangedNotifier->Checked = !SelectionChangedNotifier->Checked;
}

// ---------------------------------------------------------------------------
void __fastcall TBinTreeViewFrame::SawDoubleClick(TObject *Sender)
{
   /*
    * Notify any interested parent that we detected a double click;
    * obviously, this is a cheap callback hack
    */
   DoubleClickNotifier->Checked = !DoubleClickNotifier->Checked;
}
// ---------------------------------------------------------------------------
void __fastcall TBinTreeViewFrame::BinTreeViewExpanding(TObject *Sender, TTreeNode *Node,
          bool &AllowExpansion)
{
	AllowExpansion = true;
//	DBTRACE(StringToStdString(Node->Text));

	string binPath = getBinPathFromBinTreeNode(Node);
//	DBTRACE2(StringToStdString(Node->Text), binPath);
	AddOneLevelOfBinTreeNodeChildren(binPath, Node, false);
}
//---------------------------------------------------------------------------

