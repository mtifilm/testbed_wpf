object NakedProxyFrame: TNakedProxyFrame
  Left = 0
  Top = 0
  Width = 140
  Height = 107
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 140
    Height = 107
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Caption = 'ProxyPanel'
    TabOrder = 0
    object ProxyPaintBox: TPaintBox
      Left = 4
      Top = 4
      Width = 132
      Height = 99
      OnPaint = ProxyPaintBoxPaint
    end
    object BusyArrowPanel: TPanel
      Left = 4
      Top = 4
      Width = 132
      Height = 99
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object BusyArrows: TMTIBusy
        Left = 0
        Top = 67
        Width = 16
        Height = 16
        ArrowType = batSmall
      end
    end
  end
end
