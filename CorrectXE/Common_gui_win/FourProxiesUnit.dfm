object FourProxiesFrame: TFourProxiesFrame
  Left = 0
  Top = 0
  Width = 458
  Height = 106
  TabOrder = 0
  inline SmallLabeledProxyFrame4: TSmallLabeledProxyFrame
    Left = 344
    Top = 0
    Width = 114
    Height = 106
    TabOrder = 0
    ExplicitLeft = 344
    ExplicitWidth = 114
  end
  inline SmallLabeledProxyFrame1: TSmallLabeledProxyFrame
    Left = 0
    Top = 0
    Width = 114
    Height = 106
    TabOrder = 1
    ExplicitWidth = 114
  end
  inline SmallLabeledProxyFrame2: TSmallLabeledProxyFrame
    Left = 112
    Top = 0
    Width = 114
    Height = 106
    TabOrder = 2
    ExplicitLeft = 112
    ExplicitWidth = 114
    inherited ProxyPanel: TPanel
      inherited ProxyLabelPanel: TPanel
        Font.Color = clAqua
      end
    end
  end
  inline SmallLabeledProxyFrame3: TSmallLabeledProxyFrame
    Left = 232
    Top = 0
    Width = 114
    Height = 106
    TabOrder = 3
    ExplicitLeft = 232
    ExplicitWidth = 114
    inherited ProxyPanel: TPanel
      inherited ProxyLabelPanel: TPanel
        Font.Color = clAqua
      end
    end
  end
end
