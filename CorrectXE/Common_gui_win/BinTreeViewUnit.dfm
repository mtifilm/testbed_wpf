object BinTreeViewFrame: TBinTreeViewFrame
  Left = 0
  Top = 0
  Width = 104
  Height = 220
  TabOrder = 0
  object BinListHeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 104
    Height = 20
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = '   Bins'
    TabOrder = 0
    DesignSize = (
      104
      20)
    object NewBinImage: TImage
      Left = 74
      Top = 0
      Width = 17
      Height = 20
      Anchors = [akTop, akRight]
      Picture.Data = {
        07544269746D6170EE030000424DEE0300000000000036000000280000001200
        0000110000000100180000000000B8030000C40E0000C40E0000000000000000
        0000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000
        0000000000000000000000000000000000000000000000000000000000000000
        00C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000C8D0D400FFFFC8D0
        D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFF000000C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D40000C8D0D400000000FFFFC8D0D400FFFFC8D0D400FFFFC8
        D0D400FFFFC8D0D400FFFFC8D0D4000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        0000C8D0D4000000C8D0D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFF
        C8D0D400FFFF000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000
        00FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D40000
        00C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000C8D0D400FFFFC8D0
        D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFF000000C8D0D4C8D0D400
        0000C8D0D4C8D0D40000C8D0D400000000FFFFC8D0D400FFFFC8D0D400FFFFC8
        D0D400FFFFC8D0D400FFFFC8D0D4000000C8D0D4000000C8D0D4C8D0D4C8D0D4
        0000C8D0D4000000C8D0D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFF
        C8D0D400FFFF000000000000C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000
        000000000000000000000000000000000000000000000000000000000000C8D0
        D4C8D0D4000000C8D0D4000000C8D0D40000C8D0D4C8D0D400000000FFFFC8D0
        D400FFFFC8D0D4000000C8D0D4C8D0D4C8D0D4000000C8D0D4000000C8D0D4C8
        D0D4C8D0D4C8D0D40000C8D0D4C8D0D4C8D0D4000000000000000000000000C8
        D0D4C8D0D4C8D0D4000000C8D0D4000000C8D0D4000000C8D0D4C8D0D4C8D0D4
        0000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000C8D0D4C8D0D40000C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000
        00C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D40000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
        D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        0000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000}
    end
  end
  object BinTreeView: TTreeView
    Left = 0
    Top = 20
    Width = 104
    Height = 200
    Align = alClient
    Constraints.MinHeight = 200
    Constraints.MinWidth = 100
    HideSelection = False
    Indent = 19
    ReadOnly = True
    TabOrder = 1
    TabStop = False
    OnChange = SelectionChanged
    OnDblClick = SawDoubleClick
    OnExpanding = BinTreeViewExpanding
  end
  object SelectionChangedNotifier: TCheckBox
    Left = 48
    Top = 0
    Width = 17
    Height = 17
    TabOrder = 2
    Visible = False
  end
  object DoubleClickNotifier: TCheckBox
    Left = 56
    Top = 0
    Width = 17
    Height = 17
    TabOrder = 3
    Visible = False
  end
end
