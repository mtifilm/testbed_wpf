object ClipSchemePropForm: TClipSchemePropForm
  Left = 703
  Top = 182
  VertScrollBar.Visible = False
  Caption = 'Clip Scheme Properties'
  ClientHeight = 409
  ClientWidth = 308
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ClipSchemeInfoGroup: TGroupBox
    Left = 8
    Top = 8
    Width = 297
    Height = 97
    Caption = 'Clip Scheme Information'
    TabOrder = 0
    object TypeLabel: TLabel
      Left = 16
      Top = 40
      Width = 30
      Height = 13
      Caption = 'Type: '
    end
    object Label3: TLabel
      Left = 16
      Top = 56
      Width = 3
      Height = 13
    end
    object NameLabel: TLabel
      Left = 16
      Top = 24
      Width = 34
      Height = 13
      Caption = 'Name: '
    end
    object DescriptionLabel: TLabel
      Left = 16
      Top = 56
      Width = 59
      Height = 13
      Caption = 'Description: '
    end
    object ImageFormatLabel: TLabel
      Left = 16
      Top = 72
      Width = 70
      Height = 13
      Caption = 'Image Format: '
    end
    object NameValueLabel: TLabel
      Left = 124
      Top = 24
      Width = 76
      Height = 13
      Caption = 'SGI 16bit Linear'
    end
    object TypeValueLabel: TLabel
      Left = 124
      Top = 40
      Width = 16
      Height = 13
      Caption = 'File'
    end
    object DescriptionValueLabel: TLabel
      Left = 124
      Top = 56
      Width = 113
      Height = 13
      Caption = 'SGI Files (16 Bit, Linear)'
    end
    object ImageFormatValueLabel: TLabel
      Left = 124
      Top = 72
      Width = 18
      Height = 13
      Caption = 'SGI'
    end
  end
  object ColorGroup: TGroupBox
    Left = 8
    Top = 112
    Width = 297
    Height = 185
    Caption = 'Color Specifications'
    TabOrder = 1
    object PixelComponentsLabel: TLabel
      Left = 16
      Top = 24
      Width = 87
      Height = 13
      Caption = 'Pixel Components:'
    end
    object BitsPerComponentLabel: TLabel
      Left = 16
      Top = 40
      Width = 95
      Height = 13
      Caption = 'Bits per Component:'
    end
    object ColorSpaceLabel: TLabel
      Left = 16
      Top = 56
      Width = 61
      Height = 13
      Caption = 'Color Space:'
    end
    object PixelValuesLabel: TLabel
      Left = 16
      Top = 86
      Width = 57
      Height = 13
      Caption = 'Pixel Values'
    end
    object MinimumLabel: TLabel
      Left = 24
      Top = 102
      Width = 44
      Height = 13
      Caption = 'Minimum:'
    end
    object MaximumLabel: TLabel
      Left = 24
      Top = 118
      Width = 47
      Height = 13
      Caption = 'Maximum:'
    end
    object BlackLabel: TLabel
      Left = 24
      Top = 134
      Width = 30
      Height = 13
      Caption = 'Black:'
    end
    object WhiteLabel: TLabel
      Left = 24
      Top = 150
      Width = 31
      Height = 13
      Caption = 'White:'
    end
    object MinRYValueLabel: TLabel
      Left = 124
      Top = 102
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object MinGUValueLabel: TLabel
      Left = 164
      Top = 102
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object MinBVValueLabel: TLabel
      Left = 204
      Top = 102
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object RYLabel: TLabel
      Left = 147
      Top = 88
      Width = 8
      Height = 13
      Caption = 'R'
    end
    object GULabel: TLabel
      Left = 187
      Top = 88
      Width = 8
      Height = 13
      Caption = 'G'
    end
    object BVLabel: TLabel
      Left = 227
      Top = 88
      Width = 7
      Height = 13
      Caption = 'B'
    end
    object MaxRYValueLabel: TLabel
      Left = 124
      Top = 118
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object MaxGUValueLabel: TLabel
      Left = 164
      Top = 118
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object MaxBVValueLabel: TLabel
      Left = 204
      Top = 118
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object BlackRYValueLabel: TLabel
      Left = 124
      Top = 134
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object BlackGUValueLabel: TLabel
      Left = 164
      Top = 134
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object BlackBVValueLabel: TLabel
      Left = 204
      Top = 134
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object WhiteRYValueLabel: TLabel
      Left = 124
      Top = 150
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object WhiteGUValueLabel: TLabel
      Left = 164
      Top = 150
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object WhiteBVValueLabel: TLabel
      Left = 204
      Top = 150
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '65535'
    end
    object PixelComponentsValueLabel: TLabel
      Left = 124
      Top = 24
      Width = 23
      Height = 13
      Caption = 'RGB'
    end
    object BitsPerComponentValueLabel: TLabel
      Left = 124
      Top = 40
      Width = 12
      Height = 13
      Caption = '16'
    end
    object ColorSpaceValueLabel: TLabel
      Left = 124
      Top = 56
      Width = 29
      Height = 13
      Caption = 'Linear'
    end
  end
  object Button1: TButton
    Left = 229
    Top = 384
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 2
    OnClick = Button1Click
  end
  object AudioGroup: TGroupBox
    Left = 8
    Top = 304
    Width = 297
    Height = 73
    Caption = 'Audio Specifications'
    TabOrder = 3
    object NumberChannelsLabel: TLabel
      Left = 16
      Top = 24
      Width = 99
      Height = 13
      Caption = 'Number of Channels:'
    end
    object SampleRateLabel: TLabel
      Left = 16
      Top = 40
      Width = 64
      Height = 13
      Caption = 'Sample Rate:'
    end
    object NumberChannelsValueLabel: TLabel
      Left = 124
      Top = 24
      Width = 26
      Height = 13
      Caption = 'None'
    end
    object SampleRateValueLabel: TLabel
      Left = 124
      Top = 40
      Width = 20
      Height = 13
      Caption = 'N/A'
    end
  end
end
