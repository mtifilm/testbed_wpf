//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "NakedProxyUnit.h"
#include "ProxyDisplayer.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "MTIBusy"
#pragma resource "*.dfm"
//TNakedProxyFrame *NakedProxyFrame;
//---------------------------------------------------------------------------
__fastcall TNakedProxyFrame::TNakedProxyFrame(TComponent* Owner)
        : TFrame(Owner)
{
    Reset();
}
//---------------------------------------------------------------------------

void TNakedProxyFrame::Reset()
{
    ProxyPaintBox->Visible = false;
    BusyArrows->Busy = false;
    BusyArrowPanel->Visible = false;
    LastDisplayerUsed = 0;
    TimecodeBeingShown = CTimecode::NOT_SET;
}
//---------------------------------------------------------------------------

void TNakedProxyFrame::ShowFrame(ProxyDisplayer *displayer,
                                        const CTimecode &timecode)
{
    int retVal = 0;
    const bool DrawXOnProxy = false;

    if ((!ProxyPaintBox->Visible) ||
        (TimecodeBeingShown == CTimecode::NOT_SET) ||
        (timecode != TimecodeBeingShown))
    {

        TimecodeBeingShown = timecode;

        //ProxyPaintBox->Visible = false;
        // Annoying:
        //BusyArrows->Busy = true;
        //BusyArrowPanel->Visible = true;
        retVal = displayer->RenderProxy(timecode, ProxyPaintBox, DrawXOnProxy);

        if (retVal == 0) {
            ProxyPaintBox->Visible = true;
            LastDisplayerUsed = displayer;      // QQQ EVIL
        } else {
           ProxyPaintBox->Visible = false;
           LastDisplayerUsed = 0;
        }
        //BusyArrows->Busy = false;
        //BusyArrowPanel->Visible = false;
    }
}
//---------------------------------------------------------------------------

void TNakedProxyFrame::RedrawFrame()
{
    const bool DrawXOnProxy = false;

    if (LastDisplayerUsed != 0
        && TimecodeBeingShown != CTimecode(CTimecode::NOT_SET))
    {
         LastDisplayerUsed->RenderProxy(TimecodeBeingShown, ProxyPaintBox,
                              DrawXOnProxy, true); // Force redraw of the frame
    }
}
//---------------------------------------------------------------------------

void __fastcall TNakedProxyFrame::ProxyPaintBoxPaint(
      TObject *Sender)
{
    const bool DrawXOnProxy = false;

    if (ProxyPaintBox->Visible && LastDisplayerUsed != 0) {
        LastDisplayerUsed->RenderProxy(TimecodeBeingShown, ProxyPaintBox,
                                        DrawXOnProxy);
    }
}
//---------------------------------------------------------------------------
