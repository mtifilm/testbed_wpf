//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ExecButtonsFrameUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "VTimeCodeEdit"
#pragma link "ColorPanel"
#pragma resource "*.dfm"
#include "MTIio.h"
TExecButtonsFrame *ExecButtonsFrame;
//---------------------------------------------------------------------------
__fastcall TExecButtonsFrame::TExecButtonsFrame(TComponent* Owner)
    : TFrame(Owner), LastButtonClicked(EXEC_BUTTON_NONE)
{
   // These are on the main toolbar
   PreviewFrameButton->Tag = EXEC_BUTTON_PREVIEW_FRAME;
   PreviewAllButton->Tag = EXEC_BUTTON_PREVIEW_ALL;
   RenderFrameButton->Tag = EXEC_BUTTON_RENDER_FRAME;
   RenderAllButton->Tag = EXEC_BUTTON_RENDER_ALL;
	PauseButton->Tag = EXEC_BUTTON_PAUSE;
	ResumeButton->Tag = EXEC_BUTTON_RESUME;
   StopButton->Tag = EXEC_BUTTON_STOP;
	CapturePDLButton->Tag = EXEC_BUTTON_CAPTURE_PDL;

	// A hack to associate the weird panel thingies with the buttons they
	// are covering.
	PreviewFrameButtonPanel->Tag = -EXEC_BUTTON_PREVIEW_FRAME;
	PreviewAllButtonPanel->Tag = -EXEC_BUTTON_PREVIEW_ALL;
	RenderFrameButtonPanel->Tag = -EXEC_BUTTON_RENDER_FRAME;
	RenderAllButtonPanel->Tag = -EXEC_BUTTON_RENDER_ALL;

	// These are on 'resume timecode' widget
   GotoTCButton->Tag = EXEC_BUTTON_GO_TO_RESUME_TIMECODE;
   SetStartTCButton->Tag = EXEC_BUTTON_SET_RESUME_TC;

   PreviewFrameButton->Enabled = true;
   PreviewAllButton->Enabled = true;
   RenderFrameButton->Enabled = true;
   RenderAllButton->Enabled = true;
   PreviewFrameButtonPanel->Enabled = true;
   PreviewAllButtonPanel->Enabled = true;
   RenderFrameButtonPanel->Enabled = true;
   RenderAllButtonPanel->Enabled = true;
   PauseButton->Enabled = false;
   ResumeButton->Enabled = false;
   StopButton->Enabled = false;
   CapturePDLButton->Enabled = true;

   PreviewFrameButton->Visible = true;
   PreviewAllButton->Visible = true;
   RenderFrameButton->Visible = true;
   RenderAllButton->Visible = true;
   PreviewFrameButtonPanel->Visible = true;
   PreviewAllButtonPanel->Visible = true;
   RenderFrameButtonPanel->Visible = true;
   RenderAllButtonPanel->Visible = true;
   PauseButton->Visible = true;
   ResumeButton->Visible = false;
   StopButton->Visible = true;
   CapturePDLButton->Visible = true;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TExecButtonsFrame::ExecButtonClick(TObject *Sender)
{
	TControl *control = dynamic_cast<TControl *>( Sender );
	if (control == nullptr)
	{
		return;
	}

	// HACK: The panels associated with some buttons have the tag negated.
	// If the panel was clicked, we need to check if the button is enabled
	// because the panel is always enabled.
	int tag = control->Tag;
	if (tag >= 0)
	{
		// Clicked directly on the button.
		LastButtonClicked = EExecButtonId(tag);
	}
	else
	{
		// Clicked on the panel associated with the button.
		int buttonTag = -tag;
		TControl *control = findControlByTag(buttonTag, ToolbarPanel);
		if (control == nullptr || !control->Enabled)
		{
			// No associated button, or the associated button is disabled.
			ResumeTCEdit->VExit(0); // Always pop out of edits when you click elsewhere
			return;
		}

		LastButtonClicked = EExecButtonId(buttonTag);
	}

	// Special case: EXEC_BUTTON_CAPTURE_PDL changes to
	// EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS if SHIFT is down!
	if (LastButtonClicked == EXEC_BUTTON_CAPTURE_PDL
	&& HIWORD(GetKeyState(VK_SHIFT)) != 0)
	{
		LastButtonClicked = EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS;
	}

	ButtonPressedNotifier->Click();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

EExecButtonId TExecButtonsFrame::GetLastButtonClicked(void)
{
   return LastButtonClicked;
}
//---------------------------------------------------------------------------

void TExecButtonsFrame::EnableButton(EExecButtonId buttonId)
{
   setButtonEnabledProperty(buttonId, ToolbarPanel, true);
}
//---------------------------------------------------------------------------

void TExecButtonsFrame::DisableButton(EExecButtonId buttonId)
{
   setButtonEnabledProperty(buttonId, ToolbarPanel, false);
}
//---------------------------------------------------------------------------

bool TExecButtonsFrame::IsButtonEnabled(EExecButtonId buttonId)
{
	int tag = int(buttonId);
	TControl *control = findControlByTag(tag, ToolbarPanel);
	MTIassert(control != nullptr);
	if (control == nullptr)
	{
		return false;
	}

	return control->Visible && control->Enabled;
}
//---------------------------------------------------------------------------

void TExecButtonsFrame::ShowButton(EExecButtonId buttonId)
{
   setButtonVisibleProperty(buttonId, true);
}
//---------------------------------------------------------------------------

void TExecButtonsFrame::HideButton(EExecButtonId buttonId)
{
   setButtonVisibleProperty(buttonId, false);
}
//---------------------------------------------------------------------------

void TExecButtonsFrame::SetDownButton(EExecButtonId buttonId)
{
   // For no button down, call with argument EXEC_BUTTON_NONE

	PreviewFrameButtonPanel->BevelInner = bvNone;
	PreviewAllButtonPanel->BevelInner = bvNone;
	RenderFrameButtonPanel->BevelInner = bvNone;
	RenderAllButtonPanel->BevelInner = bvNone;

   switch (buttonId)
   {
      case EXEC_BUTTON_PREVIEW_FRAME:
			PreviewFrameButtonPanel->BevelInner = bvLowered;
			break;
      case EXEC_BUTTON_PREVIEW_ALL:
			PreviewAllButtonPanel->BevelInner = bvLowered;
			break;
		case EXEC_BUTTON_RENDER_FRAME:
			RenderFrameButtonPanel->BevelInner = bvLowered;
			break;
      case EXEC_BUTTON_RENDER_ALL:
			RenderAllButtonPanel->BevelInner = bvLowered;
		 break;
	  default:
	  	break;
   }
}
//---------------------------------------------------------------------------

EExecButtonId TExecButtonsFrame::GetDownButton(void)
{
   EExecButtonId retVal = EXEC_BUTTON_NONE;

	if (PreviewFrameButtonPanel->BevelInner == bvLowered)
	{
		retVal = EXEC_BUTTON_PREVIEW_FRAME;
	}
	else if (PreviewAllButtonPanel->BevelInner == bvLowered)
	{
		retVal = EXEC_BUTTON_PREVIEW_ALL;
	}
	else if (RenderFrameButtonPanel->BevelInner == bvLowered)
	{
		retVal = EXEC_BUTTON_RENDER_FRAME;
	}
	else if (RenderAllButtonPanel->BevelInner == bvLowered)
	{
		retVal = EXEC_BUTTON_RENDER_ALL;
	}

   return retVal;
}
//---------------------------------------------------------------------------

void TExecButtonsFrame::SetButtonHint(EExecButtonId buttonId, const string &newHint)
{
//	TControl *control = findControl(buttonId, ToolbarPanel);
//	MTIassert(control != nullptr);
//	if (control == nullptr)
//	{
//		return;
//	}
//
//	control->Hint = newHint.c_str();
//
//	EExecButtonId panelId = EExecButtonId(-int(buttonId));
//	control = findControl(buttonId, ToolbarPanel);
//	if (control == nullptr)
//	{
//		// No associated panel - we're done.
//		return;
//	}
//
//	control->Hint = newHint.c_str();
//
	switch (buttonId)
	{
		case EXEC_BUTTON_PREVIEW_FRAME:
			PreviewFrameButton->Hint = newHint.c_str();
			PreviewFrameButtonPanel->Hint = newHint.c_str();
			break;
		case EXEC_BUTTON_PREVIEW_ALL:
			PreviewAllButton->Hint = newHint.c_str();
			PreviewAllButtonPanel->Hint = newHint.c_str();
			break;
		case EXEC_BUTTON_RENDER_FRAME:
			RenderFrameButton->Hint = newHint.c_str();
			RenderFrameButtonPanel->Hint = newHint.c_str();
			break;
		case EXEC_BUTTON_RENDER_ALL:
			RenderAllButton->Hint = newHint.c_str();
			RenderAllButtonPanel->Hint = newHint.c_str();
			break;
		case EXEC_BUTTON_PAUSE:
			PauseButton->Hint = newHint.c_str();
			break;
		case EXEC_BUTTON_RESUME:
			ResumeButton->Hint = newHint.c_str();
			break;
		case EXEC_BUTTON_STOP:
			StopButton->Hint = newHint.c_str();
			break;
		case EXEC_BUTTON_CAPTURE_PDL:
			CapturePDLButton->Hint = newHint.c_str();
		 break;
	  default:
		break;
	}
}
//---------------------------------------------------------------------------

string TExecButtonsFrame::GetButtonHint(EExecButtonId buttonId)
{
   wchar_t* retVal;

   switch (buttonId)
   {
      case EXEC_BUTTON_PREVIEW_FRAME:
		 retVal = PreviewFrameButton->Hint.c_str();
         break;
      case EXEC_BUTTON_PREVIEW_ALL:
         retVal = PreviewAllButton->Hint.c_str();
         break;
      case EXEC_BUTTON_RENDER_FRAME:
         retVal = RenderFrameButton->Hint.c_str();
         break;
      case EXEC_BUTTON_RENDER_ALL:
         retVal = RenderAllButton->Hint.c_str();
         break;
      case EXEC_BUTTON_PAUSE:
         retVal = PauseButton->Hint.c_str();
         break;
      case EXEC_BUTTON_RESUME:
         retVal = ResumeButton->Hint.c_str();
         break;
      case EXEC_BUTTON_STOP:
         retVal = StopButton->Hint.c_str();
         break;
      case EXEC_BUTTON_CAPTURE_PDL:
         retVal = CapturePDLButton->Hint.c_str();
		 break;
	  default:
	     break;
   }

   return WCharToStdString(retVal);
}
//---------------------------------------------------------------------------


void TExecButtonsFrame::EnableResumeWidget(void)
{
   ResumeGroupBox->Enabled = true;
}
//---------------------------------------------------------------------------

void TExecButtonsFrame::DisableResumeWidget(void)
{
   // This disables all the controls on the widget without dimming them
   ResumeGroupBox->Enabled = false;
}
//---------------------------------------------------------------------------

void TExecButtonsFrame::SetResumeTimecode(PTimecode ptc)
{
   ResumeTCEdit->tcP = ptc;
}
//---------------------------------------------------------------------------

PTimecode TExecButtonsFrame::GetResumeTimecode(void)
{
   return ResumeTCEdit->tcP;
}
//---------------------------------------------------------------------------

bool TExecButtonsFrame::RunCommand(EExecButtonId commandNumber)
{
   bool OK = false;
   LastButtonClicked = commandNumber;

   switch (commandNumber)
   {
      case EXEC_BUTTON_PREVIEW_FRAME:
         OK =  PreviewFrameButton->Enabled;
         break;
      case EXEC_BUTTON_PREVIEW_NEXT_FRAME:
         OK =  PreviewFrameButton->Enabled;       // no dedicated button
         break;
      case EXEC_BUTTON_PREVIEW_ALL:
         OK =  PreviewAllButton->Enabled;
         break;
      case EXEC_BUTTON_RENDER_FRAME:
         OK =  RenderFrameButton->Enabled;
         break;
      case EXEC_BUTTON_RENDER_NEXT_FRAME:
         OK =  RenderFrameButton->Enabled;       // no dedicated button
         break;
      case EXEC_BUTTON_RENDER_ALL:
         OK =  RenderAllButton->Enabled;
         break;
      case EXEC_BUTTON_STOP:
         OK = StopButton->Enabled;
         break;
      case EXEC_BUTTON_PAUSE_OR_RESUME:          // button states are exclusive
         if (PauseButton->Visible && PauseButton->Enabled)
         {
            commandNumber = EXEC_BUTTON_PAUSE;
            OK = true;
         }
         else if (ResumeButton->Visible && ResumeButton->Enabled)
         {
            commandNumber = EXEC_BUTTON_RESUME;
            OK = true;
         }
         break;
      case EXEC_BUTTON_SET_RESUME_TC:
         OK = SetStartTCButton->Enabled;
         break;
      case EXEC_BUTTON_GO_TO_RESUME_TIMECODE:
         OK = GotoTCButton->Enabled;
         break;
      case EXEC_BUTTON_CAPTURE_PDL:
         OK = CapturePDLButton->Enabled;
         break;
      case EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS:
         OK = true;   // Always works, regardless of CAPTURE_PDL button state
		 break;
	  default:
	  	break;
	}

   if (OK)
   {
      LastButtonClicked = commandNumber;
      ResumeTCEdit->VExit(0);          // Finish editing if necessary
      ButtonPressedNotifier->Click();
   }
   return OK;
}

void TExecButtonsFrame::SetButtonPressedNotifierCallback(TNotifyEvent callback)
{
	ButtonPressedNotifier->OnClick = callback;
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void TExecButtonsFrame::setButtonEnabledProperty(EExecButtonId buttonId,
                                                 TPanel *panel,
                                                 bool flag)
{
	int buttonTag = int(buttonId);
	TControl *control = findControlByTag(buttonTag, panel);
	if (control != nullptr)
	{
		control->Enabled = flag;
	}

	// HACK: We have NO IDEA WHY, but in 3Layer tool a button associated with
	// a panel DOE NOT REPAINT, unless we toggle the panel visibility! WTF!
	int panelTag = -buttonTag;
	control = findControlByTag(panelTag, panel);
	if (control != nullptr)
	{
		control->Visible = false;
		control->Visible = true;
	}
}
//---------------------------------------------------------------------------

void TExecButtonsFrame::setButtonVisibleProperty(EExecButtonId buttonId,
																 bool flag)
{
	int buttonTag = int(buttonId);
	TControl *control = findControlByTag(buttonTag, ToolbarPanel);
	if (control != nullptr)
	{
		control->Visible = flag;
	}
}
//---------------------------------------------------------------------------

TControl *TExecButtonsFrame::findControlByTag(int tag, TPanel *panel)
{
   // ControlCount is a property of TWinControl
   for (int i = 0; i < panel->ControlCount; ++i)
   {
		TControl *control = dynamic_cast<TControl *>(panel->Controls[i]);
		if (control != NULL && control->Tag == tag)
		{
			return control;
		}
	}

	return nullptr; // Not found
}
//---------------------------------------------------------------------------
