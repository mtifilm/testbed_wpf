//----------------------------------------------------------------------------
#ifndef MoveInMarkDialogUnitH
#define MoveInMarkDialogUnitH
//----------------------------------------------------------------------------
#include <System.hpp>
#include <Windows.hpp>
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Controls.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//----------------------------------------------------------------------------
class TMoveInMarkDialog : public TForm
{
__published:        
	TButton *OKBtn;
	TButton *CancelBtn;
	TBevel *Bevel1;
        TButton *Button1;
        TLabel *Label1;
        TLabel *Label4;
private:
public:
	virtual __fastcall TMoveInMarkDialog(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE TMoveInMarkDialog *MoveInMarkDialog;
//----------------------------------------------------------------------------
#endif    
