// ---------------------------------------------------------------------------

#ifndef OneShotDialogsH
#define OneShotDialogsH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <string>

#include "MTIDialogs.h"

namespace OneShotDialogs
{
    struct OneShotData
    {
	int NoShowResult = mrYes;
	bool NoShow = false;
    };
}

// ---------------------------------------------------------------------------
class TOneShotConfirmationDialog : public TForm
{
__published: // IDE-managed Components

    TButton *YesButton;
    TButton *NoButton;
    TImage *CautionImage;
    TLabel *InformationLabel;
    TCheckBox *OneShotCheckBox;

private: // User declarations
	public : // User declarations

    __fastcall TOneShotConfirmationDialog(TComponent* Owner);
    __fastcall TOneShotConfirmationDialog(TComponent* Owner, const std::string &msg);
    static int OneShotShow(TComponent* Owner, const std::string &msg, OneShotDialogs::OneShotData &oneShotData);

};



// ---------------------------------------------------------------------------
extern PACKAGE TOneShotConfirmationDialog *OneShotConfirmationDialog;
// ---------------------------------------------------------------------------
#endif
