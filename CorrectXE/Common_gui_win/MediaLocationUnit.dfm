object MediaLocationFrame: TMediaLocationFrame
  Left = 0
  Top = 0
  Width = 450
  Height = 65
  TabOrder = 0
  DesignSize = (
    450
    65)
  object MediaLocationLabel1: TLabel
    Left = 42
    Top = 5
    Width = 19
    Height = 13
    Caption = 'Disk'
  end
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 37
    Height = 13
    Caption = 'Scheme'
  end
  object DiskSchemeComboBox: TComboBox
    Left = 68
    Top = 8
    Width = 121
    Height = 21
    Style = csDropDownList
    Anchors = [akTop, akRight]
    TabOrder = 0
    OnSelect = DiskSchemeComboBoxSelect
    Items.Strings = (
      'rd1')
  end
  object AddHandlesCheckBox: TCheckBox
    Left = 70
    Top = 36
    Width = 113
    Height = 17
    Caption = 'Add handle frames'
    TabOrder = 1
  end
  object DiskSpaceTimer: TTimer
    Interval = 20000
    OnTimer = DiskSpaceTimerTimer
    Left = 16
    Top = 32
  end
end
