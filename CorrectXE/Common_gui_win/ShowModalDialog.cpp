/*
   File Name:  ShowModalHack.cpp
   Created: June 3, 2013 by John Mertus
   Version 1.00

  This hack is used to show a modal dialog box when the calling is set
  to AlwaysOnTop.   This is because of a bug in Borland

*/

#include "ShowModalDialog.h"
#include "SharedMap.h"
#include <vector>
using std::vector;

static vector<TForm *> ListOfStayOnTopWindows;
#define STAY_ON_TOP_LIST_KEY "StayOnTopList"
#define STAY_ON_TOP_NEST_KEY "StayOnTopOverrideNesting"

// There used to be an elaborate hack here that was no longer needed.
// Old comment:
// Hack - use these to show modal dialogs so we can remove "stay on top"
// property from the bin manager so stupid modal dialogs don't stupidly
// show up BEHIND the stupid bin manager.
int ShowModalDialog(TCustomForm *form)
{
   // If the form is visible you get a popup that says you can't make a visible window modal!
   form->Hide();
   return form->ShowModal();
}

