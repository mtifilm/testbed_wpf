//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SaveToFilesUnit.h"

#include "ComboBoxHistoryList.h"
#include "CommonGuiUtil.h"
#include "ImageFileMediaAccess.h"
#include "IniFile.h"
#include "ShowModalDialog.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//TSaveToFilesFrame *SaveToFilesFrame;
//---------------------------------------------------------------------------
__fastcall TSaveToFilesFrame::TSaveToFilesFrame(TComponent* Owner)
    : TFrame(Owner)
{
}
//---------------------------------------------------------------------------

void TSaveToFilesFrame::InitFromIniFile(const string &iniFileName)
{
    if (!iniFileName.empty()) {
        ComboBoxHistoryList imageFileHistory(iniFileName, "ImageFileHistory");

        MyIniFileName = iniFileName;
        imageFileHistory.SetPoundSubstitute('@');
        imageFileHistory.ReadProperties();
        imageFileHistory.rebuildComboBoxList(PathComboBox);
    }
}
//---------------------------------------------------------------------------

bool TSaveToFilesFrame::IsValid()
{
    return (PathComboBox->Text != "");
}
//---------------------------------------------------------------------------

int TSaveToFilesFrame::OK()
{
    int retVal = 0;

    // CHECK IF OVERWRITING ANY FILES, AND HAVEN'T KVETCHED YET

    /*
     * Add or move filename to top of the combo box history list
     */
    if (IsValid() && !MyIniFileName.empty()) {
        ComboBoxHistoryList imageFileHistory(MyIniFileName, "ImageFileHistory");
        string historyName(CImageFileMediaAccess::GenerateFileName2(GetName(),
                                                       GetFirstFrame()));

        imageFileHistory.SetPoundSubstitute('@');
        imageFileHistory.ReadProperties();
        imageFileHistory.Remove(historyName);
        imageFileHistory.Add(historyName);
        imageFileHistory.rebuildComboBoxList(PathComboBox);
        imageFileHistory.WriteProperties();
    }

    return retVal;
}
//---------------------------------------------------------------------------

string TSaveToFilesFrame::GetName()
{
    return CppStringFromAnsiString(PathComboBox->Text);
}
//---------------------------------------------------------------------------

int TSaveToFilesFrame::GetFirstFrame()
{
    return FirstFrameEdit->Text.ToIntDef(0);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TSaveToFilesFrame::BrowseButtonClick(
      TObject *Sender)
{
    AnsiString fileName = BrowseForNewDestFile();
    PathComboBox->Text = fileName;
    HandleComboBoxReturnKey();
}
//---------------------------------------------------------------------------

namespace {
    void AddExtIfNeeded(AnsiString &name, const AnsiString &ext)
    {
        int extPos = name.Length() - 4 + 1; // 4 = strlen(".xxx"), +1-based

        // For some reason, linker can't find AnsiString::SubString in XE5upd2
        if (extPos <= 0 || AnsiString((AnsiStringToStdString(name).substr(extPos, 4)).c_str()).AnsiCompareIC(ext) != 0)
            name += ext;
    }
}
//---------------------------------------------------------------------------

AnsiString TSaveToFilesFrame::BrowseForNewDestFile()
{
    // FIXME shouldn't these be defined somewhere centrally?
    const char *ext[] = {".cin", ".dpx", ".sgi", ".raw", ".rgb",
                   ".tga", ".tif", ".bmp", ".yuv"};
    AnsiString fileName;
    int formatIndex = 0;
    int formatMax = sizeof(ext) / sizeof(char*);

    SaveDialog->Execute();
    fileName = SaveDialog->FileName;
    if (fileName != "") {
        formatIndex = SaveDialog->FilterIndex - 1; // 1-based
        if (formatIndex >= 0 && formatIndex < formatMax)
            AddExtIfNeeded(fileName, ext[formatIndex]);
    }
    return fileName;
}
//---------------------------------------------------------------------------

/*
 * This is the code that substitutes ####'s for digits to make a template
 * if the user selects or types in a file name instead of a template.
 */
void TSaveToFilesFrame::HandleComboBoxReturnKey()
{
    string fileName = CppStringFromAnsiString(PathComboBox->Text);

    if (fileName != "") {

        if (!CImageFileMediaAccess::IsTemplate(fileName)) {
            string fileNameTemplate(
                CImageFileMediaAccess::MakeTemplate(fileName, false)
                );
            int frameNumber(
                CImageFileMediaAccess::getFrameNumberFromFileName2(fileNameTemplate,
                                                        fileName)
                );

            FirstFrameEdit->Text = AnsiString(frameNumber);
            PathComboBox->Text = AnsiString(fileNameTemplate.c_str());
        }
    }
    // HandleChange();
}
//---------------------------------------------------------------------------
void __fastcall TSaveToFilesFrame::PathComboBoxEnter(
      TObject *Sender)
{
    NameAtComboBoxEntry = CppStringFromAnsiString(PathComboBox->Text);
}
//---------------------------------------------------------------------------
void __fastcall TSaveToFilesFrame::PathComboBoxExit(
      TObject *Sender)
{
    if (NameAtComboBoxEntry != StringToStdString(PathComboBox->Text)) {
        HandleComboBoxReturnKey();
    }
}
//---------------------------------------------------------------------------
void __fastcall TSaveToFilesFrame::PathComboBoxKeyPress(
      TObject *Sender, char &Key)
{
    if (Key == Char(VK_RETURN)) {
        HandleComboBoxReturnKey();
    }
}
//---------------------------------------------------------------------------

void __fastcall TSaveToFilesFrame::PathComboBoxSelect(TObject *Sender)
{
    PathComboBoxExit(Sender);
    PathComboBoxEnter(Sender);
}
//---------------------------------------------------------------------------

