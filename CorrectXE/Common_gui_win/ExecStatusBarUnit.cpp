//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ExecStatusBarUnit.h"
#include "HRTimer.h"
#include "MTIstringstream.h"
#include "ThreadLocker.h"
#include "SafeClasses.h"
#include <iostream>
#include "IniFile.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "ColorPanel"
#pragma resource "*.dfm"
TExecStatusBarFrame *ExecStatusBarFrame;

#define SHORT_INTERVAL 10      // Expedited timer interval for one-shot
#define LONG_INTERVAL 250      // Normal GUI update interval while running

//---------------------------------------------------------------------------
__fastcall TExecStatusBarFrame::TExecStatusBarFrame(TComponent* Owner)
: TFrame(Owner)
, _FramesTotal(0)
, _BumpsPerFrame(1)
, _FramesElapsed(0)
, _Bumps(0)
, _ElapsedTimeInSeconds(0)
, _TimePerFrameInSeconds(0)
, _State(SB_STATE_IDLE)
, _ProgressPanelDarkWidth(0)
{
   _HRTimer = new CHRTimer;
   _ThreadLock = new CThreadLock;
   ElapsedTimer->Enabled = false;

   TotalLabel2->Left = TotalLabel1->Left;
   ElapsedLabel2->Left = ElapsedLabel1->Left;
   RemainingLabel2->Left  = RemainingLabel1->Left;
   DisabledTotalLabel->Left = TotalLabel1->Left;
   DisabledElapsedLabel->Left = ElapsedLabel1->Left;
   DisabledRemainingLabel->Left  = RemainingLabel1->Left;

   StatusMessageValue->Caption = "Idle";
   ToolMessageValue->Caption = "";

   _ToolMessageRightEdgeOffset = BackPanel->Width - (ToolMessageValue->Left
                                                    + ToolMessageValue->Width);
}
//---------------------------------------------------------------------------
__fastcall TExecStatusBarFrame::~TExecStatusBarFrame()
{
   ElapsedTimer->Enabled = false;
   delete _HRTimer;
   delete _ThreadLock;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

void TExecStatusBarFrame::SetIdle(bool setMessage)
{
   {  CAutoThreadLocker lock(*_ThreadLock);

      if (setMessage)
      {
         setStatusMessageInternal("Idle");
      }
		setFrameCountInternal(0, 1, 0, 0.0);
   }

   // All GUI updates are done from the timer to ensure they are done from
   // the main thread
   kickTheTimer();
}
//---------------------------------------------------------------------------

string TExecStatusBarFrame::formatShortDuration(double durationInSeconds)
{
   string retVal;
   int seconds = int(durationInSeconds);
   MTIostringstream os;

   if (seconds > 999)
   {
      os << seconds;
   }
   else if (seconds > 99)
   {
      int deciseconds  = int(durationInSeconds * 10.0) % 10;
      os << seconds << "." << std::setw(1) << deciseconds;
   }
   else if (seconds > 9)
   {
      int centiseconds = int(durationInSeconds * 100.0) % 100;
      os << seconds << "." << std::setw(2) << std::setfill('0') << centiseconds;
   }
   else // seconds <= 9
   {
      int milliseconds = int(durationInSeconds * 1000.0) % 1000;
      os << seconds << "." << std::setw(3) << std::setfill('0')  << milliseconds;
   }

   retVal = os.str();

   return retVal;
}
//---------------------------------------------------------------------------

string TExecStatusBarFrame::formatLongDuration(double durationInSeconds)
{
   string retVal;
   int durationAsInt = int(durationInSeconds);
   int hours = durationAsInt / 3600;
   int minutes = (durationAsInt % 3600) / 60;
   int seconds = durationAsInt % 60;
   MTIostringstream os;

   if (hours > 0)
   {
      os << hours << ":" << std::setw(2) << std::setfill('0') << minutes
                  << ":" << std::setw(2) << std::setfill('0') << seconds;
   }
   else
   {
      os << minutes << ":" << std::setw(2) << std::setfill('0') << seconds;
   }

   retVal = os.str();

   return retVal;
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::updateReadouts(void)
{
   // ONLY TO BE CALLED FROM MAIN THREAD (such as from a TIMER event)

   if (_FramesTotal < 1)
   {
      DisabledPanel->Visible = true;
      DisabledPerFrameValue->Enabled = false;
      DisabledPerFrameValue->Caption = "         sec/f";  // 9 spaces
   }
   else if (_FramesTotal < 2)
   {
      DisabledPanel->Visible = true;
      DisabledPerFrameValue->Enabled = (_FramesTotal == 1);
      string timePerFrameAsString = (_TimePerFrameInSeconds == 0.0)?
                                  string("-.---") :
                                  formatShortDuration(_TimePerFrameInSeconds);
      MTIostringstream os;
      os << timePerFrameAsString << " sec/f";
      DisabledPerFrameValue->Caption = os.str().c_str();
   }
   else
   {
      string totalFramesAsString ;
      string totalTimeAsString = "-:--";
      string framesElapsedAsString;
      string timeElapsedAsString = "-:--";
      string framesRemainingAsString;
      string timeRemainingAsString = "-:--";
      string percentDoneAsString = "0";
      string timePerFrameAsString = "-.---";
      string frameTimeSeparator = " / ";
      MTIostringstream os;

      os << _FramesTotal;
      totalFramesAsString = os.str();
      os.str("");
      os << _FramesElapsed;
      framesElapsedAsString = os.str();
      os.str("");
      os << (_FramesTotal - _FramesElapsed);
      framesRemainingAsString = os.str();
      os.str("");

      // Always show elapsed
      timeElapsedAsString = formatLongDuration(_ElapsedTimeInSeconds);

		if (_FramesElapsed > 0)
      {
			float estimatedTotalTime = _TimePerFrameInSeconds * _FramesTotal;
			float fractionDone = float(_FramesElapsed) / _FramesTotal;

			totalTimeAsString = formatLongDuration(estimatedTotalTime);
			if (_ElapsedTimeInSeconds > estimatedTotalTime)
			{

				timeRemainingAsString = "0:00";
			}
			else
			{
				timeRemainingAsString = formatLongDuration(estimatedTotalTime - _ElapsedTimeInSeconds);
			}

			os << (int(fractionDone * 100.0));
			percentDoneAsString = os.str();
         os.str("");
         timePerFrameAsString = formatShortDuration(_TimePerFrameInSeconds);

		 ProgressPanelDark->Width = ProgressPanelLight->Width * fractionDone;

		 // Some weird bug doesn't want to set the color correctly
		 // Force it here, don't care that there could be a better solution
		 ProgressPanelDark->Color = cl3DDkShadow;

         // If times exceed 1 hour, shorten the frame/time separator to be
         // just a "/" instead of " / "
         if (estimatedTotalTime > (60*60))
            frameTimeSeparator = "/";

      }

		if (_FramesTotal > 0 && _FramesElapsed == _FramesTotal)
      {
         PercentValue1->Left = 0;
         PercentValue2->Left = 0;
      }
      else
      {
         PercentValue1->Left = 2;
         PercentValue2->Left = 2;
      }

      os << percentDoneAsString << "%";
      PercentValue1->Caption = os.str().c_str();
      PercentValue2->Caption = os.str().c_str();
      os.str("");

      os << timePerFrameAsString << " sec/f";
      PerFrameValue1->Caption = os.str().c_str();
      PerFrameValue2->Caption = os.str().c_str();
      os.str("");

      // Leave a digit's width between the labels and the frame counts,
      // unless the max frame value is 6 digits or more, in which case
      // leave no extra space up front
      int frameWidth = totalFramesAsString.size() +
                            ((totalFramesAsString.size() > 5)? 0 : 1);

      // Time string field widths are always the same as the total
      // estimated time string width
      int timeWidth = totalTimeAsString.size();

      // How do you reset the width after doing a setw?
      int ftsWidth = frameTimeSeparator.size();

      os << std::setw(frameWidth) << std::setfill(' ') << std::right << totalFramesAsString
         << std::setw(ftsWidth) << frameTimeSeparator
         << std::setw(timeWidth) << std::setfill(' ') << std::left << totalTimeAsString;
      TotalValue1->Caption = os.str().c_str();
      TotalValue2->Caption = os.str().c_str();
      os.str("");

      os << std::setw(frameWidth) << std::setfill(' ') << std::right << framesElapsedAsString
         << std::setw(ftsWidth) << frameTimeSeparator
         << std::setw(timeWidth) << std::setfill(' ') << std::left << timeElapsedAsString;
      ElapsedValue1->Caption = os.str().c_str();
      ElapsedValue2->Caption = os.str().c_str();
      os.str("");

      os << std::setw(frameWidth) << std::setfill(' ') << std::right << framesRemainingAsString
         << std::setw(ftsWidth) << frameTimeSeparator
         << std::setw(timeWidth) << std::setfill(' ') << std::left << timeRemainingAsString;
      RemainingValue1->Caption = os.str().c_str();
      RemainingValue2->Caption = os.str().c_str();
      os.str("");

      // QQQ rampant magic numbers
      int spaceAfterColon = 1;
      if (_FramesTotal < 10000)
         spaceAfterColon = 4;

      TotalValue1->Left = TotalLabel1->Left + TotalLabel1->Width
                        + spaceAfterColon;
      TotalValue2->Left = TotalValue1->Left;
      ElapsedValue1->Left = ElapsedLabel1->Left + ElapsedLabel1->Width
                          + spaceAfterColon;
      ElapsedValue2->Left = ElapsedValue1->Left;
      RemainingValue1->Left  = RemainingLabel1->Left  + RemainingLabel1->Width
                             + spaceAfterColon;
      RemainingValue2->Left  = RemainingValue1->Left;

      DisabledPanel->Visible = false;
   }
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::SetStatusMessage(const char *newMessage)
{
   {  CAutoThreadLocker lock(*_ThreadLock);

      setStatusMessageInternal(newMessage);
   }

   // All GUI updates are done from the timer to ensure they are done from
   // the main thread
   kickTheTimer();
}
//----------------------------------------------------------------

void TExecStatusBarFrame::PushStatusMessage(const char *newMessage)
{
   {  CAutoThreadLocker lock(*_ThreadLock);

      _StatusMessageStack.push(_StatusMessage);
      setStatusMessageInternal(newMessage);
   }

   // All GUI updates are done from the timer to ensure they are done from
   // the main thread
   kickTheTimer();
}
//----------------------------------------------------------------

void TExecStatusBarFrame::PopStatusMessage(void)
{
   {  CAutoThreadLocker lock(*_ThreadLock);

      if (_StatusMessageStack.size() > 0)
      {
         _StatusMessage = _StatusMessageStack.top();
         _StatusMessageStack.pop();
      }
   }

   // All GUI updates are done from the timer to ensure they are done from
   // the main thread
   kickTheTimer();
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::setStatusMessageInternal(const char *newMessage)
{
   if (AnsiString(newMessage) == _StatusMessage)
      return;

   if (newMessage == NULL)
      newMessage = "";

   _StatusMessage = newMessage;
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::SetToolMessage(const char *newMessage)
{
   {  CAutoThreadLocker lock(*_ThreadLock);

      if (AnsiString(newMessage) == _ToolMessage)
         return;

      if (newMessage == NULL)
         newMessage = "";

      _ToolMessage = newMessage;
   }

   // All GUI updates are done from the timer to ensure they are done from
   // the main thread
   kickTheTimer();
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::SetFrameCount(int newFrameCount, int newBumpsPerFrame, int preElapsedFrames, double timePerFrameInSeconds)
{
   {  CAutoThreadLocker lock(*_ThreadLock);

		setFrameCountInternal(newFrameCount, newBumpsPerFrame, preElapsedFrames, timePerFrameInSeconds);
   }

   // All GUI updates are done from the timer to ensure they are done from
   // the main thread
   kickTheTimer();
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::setFrameCountInternal(int newFrameCount, int newBumpsPerFrame, int preElapsedFrames, double timePerFrameInSeconds)
{
	_FramesTotal = newFrameCount;
	_FramesElapsed = preElapsedFrames;
   _BumpsPerFrame = newBumpsPerFrame;
   _Bumps = 0;
	_ElapsedTimeInSeconds = _FramesElapsed * timePerFrameInSeconds;
	_TimePerFrameInSeconds = timePerFrameInSeconds;

   // NO GUI STUFF OUTSIDE OF MAIN THREAD! Sheesh!
   //ProgressPanelDark->Width = 0;
   kickTheTimer();
}

//----------------------------------------------------------------

void TExecStatusBarFrame::Push()
{
	CAutoThreadLocker lock(*_ThreadLock);
	auto data = std::make_tuple(_FramesTotal, _BumpsPerFrame, _FramesElapsed, _Bumps, _State, _TimePerFrameInSeconds);
	_StateStack.push(data);
}

//----------------------------------------------------------------

void TExecStatusBarFrame::Pop()
{
   if (_StateStack.size() > 0)
   {
      CAutoThreadLocker lock(*_ThreadLock);
      int framesTotal;
      int bumpsPerFrame;
      int framesElapsed;
      int bumps;
      int state;
      double timePerFrameInSeconds;
      std::tie(framesTotal, bumpsPerFrame, framesElapsed, bumps, state, timePerFrameInSeconds) = _StateStack.top();
      setFrameCountInternal(framesTotal, bumpsPerFrame, framesElapsed, timePerFrameInSeconds);
      _StateStack.pop();

//      DBTRACE2(state, (int)_State);
//      switch (state)
//      {
//      	case int(SB_STATE_IDLE):
//           _State = SB_STATE_IDLE;
//           break;
//
//      	case int(SB_STATE_RUNNING):
//           _State = SB_STATE_RUNNING;
//           break;
//
//      	case int(SB_STATE_DONE):
//           _State = SB_STATE_DONE;
//           break;
//      }

      kickTheTimer();
   }
}

void TExecStatusBarFrame::startProgressInternal(void)
{
   if (_FramesTotal < 1)
      return;   // should assert

   _HRTimer->Start();

   _State = SB_STATE_RUNNING;
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::StartProgress(void)
{
   {  CAutoThreadLocker lock(*_ThreadLock);

      startProgressInternal();
   }

   // All GUI updates are done from the timer to ensure they are done from
   // the main thread
   kickTheTimer();
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::bumpFrameInternal(int bumpCount)
{
   _FramesElapsed += bumpCount;
	if (_FramesElapsed >= _FramesTotal)
	{
		_FramesElapsed = _FramesTotal;
	}

   double elapsedTimeInSeconds = _ElapsedTimeInSeconds + (_HRTimer->Read() / 1000.0);
	if (_FramesElapsed > 0)
	{
		_TimePerFrameInSeconds = elapsedTimeInSeconds / _FramesElapsed;
	}
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::BumpProgress(int bumpCount)
{
   {  CAutoThreadLocker lock(*_ThreadLock);

      if (_State != SB_STATE_RUNNING)
         return;   // should assert

      _Bumps += bumpCount;
      if (_Bumps >= _BumpsPerFrame)
      {
         bumpFrameInternal(_Bumps / _BumpsPerFrame);
         _Bumps %= _BumpsPerFrame;
      }
   }

   // All GUI updates are done from the timer to ensure they are done from
   // the main thread
   kickTheTimer();
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::StopProgress(bool processedToEnd)
{
   {  CAutoThreadLocker lock(*_ThreadLock);

      if (_State != SB_STATE_RUNNING)
         return;   // should assert

      if (processedToEnd)
      {
         _Bumps = 0;
			bumpFrameInternal(_FramesTotal - _FramesElapsed);
      }

      // This will trigger one final update
      _State = SB_STATE_DONE;
   }

   // All GUI updates are done from the timer to ensure they are done from
   // the main thread
   kickTheTimer();
}
//---------------------------------------------------------------------------

double TExecStatusBarFrame::GetTimePerFrameInSeconds()
{
	return _TimePerFrameInSeconds;
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::kickTheTimer(void)
{
   // Don't need to check timer state because this is pretty harmless
   ElapsedTimer->Interval = SHORT_INTERVAL;
   ElapsedTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void TExecStatusBarFrame::showMessages(void)
{

   StatusMessageValue->Caption = _StatusMessage;
   ToolMessageValue->Caption = _ToolMessage;

   // Line up the right edge of the tool medssage with the right edge of
   // the legend "sec/f" below it
   int rightEdgeOffsetRelativeToBackPanel =
                         BackPanel->Width -
                         (ReadoutPanel->Left + ProgressPanelLight->Left +
                          PerFrameValue1->Left + PerFrameValue1->Width);
   int rightEdgeOffsetRelativeToProgressPanel =
                         rightEdgeOffsetRelativeToBackPanel -
                         (ReadoutPanel->Left + ProgressPanelLight->Left);
   ToolMessageValue->Left = ProgressPanelLight->Width -
                            rightEdgeOffsetRelativeToProgressPanel -
                            ToolMessageValue->Width;

}
//---------------------------------------------------------------------------

void __fastcall TExecStatusBarFrame::ElapsedTimerTimer(TObject *Sender)
{
   // NO thread lock here else it can deadlock

   showMessages();
   double timeSinceLastReadInSeconds = _HRTimer->Read(true) / 1000.0;

   switch (_State)
   {
      case SB_STATE_RUNNING:
         {  CAutoThreadLocker lock(*_ThreadLock);
            _ElapsedTimeInSeconds += timeSinceLastReadInSeconds;
         }
         break;

      case SB_STATE_DONE:
         {  CAutoThreadLocker lock(*_ThreadLock);
            // Need one last update
            _State = SB_STATE_IDLE;
            _ElapsedTimeInSeconds += timeSinceLastReadInSeconds;
         }
         break;

      default:
      case SB_STATE_IDLE:
         ElapsedTimer->Enabled = false;
         break;
   }

   // hack to initialize the progress bar to 0 at start
	if (_FramesElapsed == 0)
   {
      ProgressPanelDark->Width = 0;
   }

	updateReadouts();
   Refresh();

   // Reset interval in case we were expedited
   if (ElapsedTimer->Interval < LONG_INTERVAL)
      ElapsedTimer->Interval = LONG_INTERVAL;
}
//---------------------------------------------------------------------------
