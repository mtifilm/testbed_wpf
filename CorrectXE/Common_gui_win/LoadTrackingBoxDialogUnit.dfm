object TrackPointLoadDialog: TTrackPointLoadDialog
  Left = 245
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Query'
  ClientHeight = 245
  ClientWidth = 313
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 297
    Height = 89
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 24
    Top = 32
    Width = 262
    Height = 52
    Caption = 
      'The current Mark In differs from the Mark In associated with the' +
      ' saved tracking points.'#13#10#13#10'Please choose how to proceed.'
    WordWrap = True
  end
  object OKBtn: TButton
    Left = 79
    Top = 212
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 159
    Top = 212
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object OptionsRadioGroup: TRadioGroup
    Left = 8
    Top = 104
    Width = 297
    Height = 97
    ItemIndex = 0
    Items.Strings = (
      'Move current Marks to match saved Marks'
      'Ignore the saved tracking points'
      'Remove the saved tracking points permanently'
      'Move tracking points to new Mark In')
    TabOrder = 2
  end
end
