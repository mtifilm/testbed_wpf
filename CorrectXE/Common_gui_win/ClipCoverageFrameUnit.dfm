object ClipCoverageFrame: TClipCoverageFrame
  Left = 0
  Top = 0
  Width = 270
  Height = 62
  TabOrder = 0
  object BackgroundPanel: TPanel
    Left = 0
    Top = 0
    Width = 270
    Height = 62
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object TotalFrameCountLabel: TLabel
      Left = 88
      Top = 7
      Width = 105
      Height = 13
      Alignment = taCenter
      Caption = '999999 Frames in Clip'
    end
    object WarningLabel: TLabel
      Left = 2
      Top = 42
      Width = 121
      Height = 13
      Alignment = taCenter
      Caption = 'Not enough space in clip '
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Visible = False
    end
    object OutTimecodeLabel: TLabel
      Left = 207
      Top = 7
      Width = 60
      Height = 13
      Caption = '01:00:30:00'
    end
    object InTimecodeLabel: TLabel
      Left = 6
      Top = 7
      Width = 60
      Height = 13
      Caption = '01:00:00:00'
    end
    object InfoLabel: TLabel
      Left = 18
      Top = 42
      Width = 243
      Height = 13
      Alignment = taCenter
      Caption = 'Selected 999999 frames (222222 through 333333)'
    end
    object FillerWellPanel: TPanel
      Left = 34
      Top = 22
      Width = 203
      Height = 17
      BevelOuter = bvLowered
      Color = clBtnShadow
      TabOrder = 0
      object FillerPanel: TColorPanel
        Left = 40
        Top = 1
        Width = 17
        Height = 14
        BevelOuter = bvNone
        Color = clAqua
        TabOrder = 0
      end
    end
  end
end
