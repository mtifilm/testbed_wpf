//----------------------------------------------------------------------------
#ifndef LoadTrackingBoxDialogUnitH
#define LoadTrackingBoxDialogUnitH
//----------------------------------------------------------------------------
#include <System.hpp>
#include <Windows.hpp>
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Controls.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//----------------------------------------------------------------------------
class TTrackPointLoadDialog : public TForm
{
__published:        
	TButton *OKBtn;
	TButton *CancelBtn;
	TBevel *Bevel1;
        TLabel *Label1;
        TRadioGroup *OptionsRadioGroup;
private:
public:
	virtual __fastcall TTrackPointLoadDialog(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE TTrackPointLoadDialog *TrackPointLoadDialog;
//----------------------------------------------------------------------------
#endif    
