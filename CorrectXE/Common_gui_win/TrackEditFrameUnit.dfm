object TrackEditFrame: TTrackEditFrame
  Left = 0
  Top = 0
  Width = 217
  Height = 51
  TabOrder = 0
  OnMouseWheelDown = FrameMouseWheelDown
  OnMouseWheelUp = FrameMouseWheelUp
  OnResize = FrameResize
  DesignSize = (
    217
    51)
  object MinLabel: TLabel
    Left = 8
    Top = 32
    Width = 16
    Height = 13
    Caption = 'Min'
    Transparent = True
  end
  object MaxLabel: TLabel
    Left = 150
    Top = 32
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Anchors = [akTop, akRight]
    Caption = 'Max'
    Transparent = True
  end
  object TitleLabel: TColorLabel
    Left = 8
    Top = 0
    Width = 45
    Height = 13
    Caption = 'TitleLabel'
    OnClick = TitleLabelClick
  end
  object TrackBar: TTrackBar
    Left = 4
    Top = 16
    Width = 173
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    Ctl3D = True
    Max = 100
    ParentCtl3D = False
    PageSize = 5
    Frequency = 5
    Position = 50
    TabOrder = 0
    ThumbLength = 15
    TickMarks = tmBoth
    TickStyle = tsNone
    OnChange = TrackBarChange
    OnEnter = TrackBarEnter
    OnExit = TrackBarExit
  end
  object TrackBarDisabledPanel: TPanel
    Left = 10
    Top = 22
    Width = 161
    Height = 8
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 3
    Visible = False
  end
  object EditAlignmentPanel: TPanel
    Left = 177
    Top = 0
    Width = 40
    Height = 51
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object Edit: TEdit
      Left = 0
      Top = 15
      Width = 30
      Height = 19
      TabStop = False
      Alignment = taCenter
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Text = '50'
      OnEnter = EditEnter
      OnExit = EditExit
      OnKeyPress = EditKeyPress
    end
    object EditDisabledPanel: TPanel
      Left = 0
      Top = 15
      Width = 30
      Height = 21
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
      object EditDisabledLabel: TLabel
        Left = 2
        Top = 4
        Width = 25
        Height = 16
        Alignment = taCenter
        AutoSize = False
        Caption = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object NotifyWidget: TCheckBox
    Left = 0
    Top = 16
    Width = 0
    Height = 17
    TabStop = False
    TabOrder = 2
  end
  object RedGreenTick: TColorPanel
    Left = 20
    Top = 13
    Width = 3
    Height = 16
    BevelOuter = bvNone
    Color = clRed
    ParentBackground = False
    TabOrder = 4
    Visible = False
    OnClick = RedGreenTickClick
  end
  object TrackBarMinPositionTick: TColorPanel
    Left = 10
    Top = 13
    Width = 3
    Height = 8
    BevelOuter = bvNone
    Color = clNavy
    ParentBackground = False
    TabOrder = 5
    Visible = False
  end
  object TrackBarMaxPositionTick: TColorPanel
    Left = 168
    Top = 13
    Width = 3
    Height = 8
    Anchors = [akTop, akRight]
    BevelOuter = bvNone
    Color = clNavy
    ParentBackground = False
    TabOrder = 6
    Visible = False
  end
  object DeferredUpdateTimer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = DeferredUpdateTimerTimer
    Left = 104
    Top = 12
  end
end
