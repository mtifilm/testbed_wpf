object SmallLabeledProxyFrame: TSmallLabeledProxyFrame
  Left = 0
  Top = 0
  Width = 115
  Height = 106
  TabOrder = 0
  object ProxyPanel: TPanel
    Left = 0
    Top = 0
    Width = 114
    Height = 106
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object ProxyImage: TImage
      Left = 3
      Top = 21
      Width = 108
      Height = 81
      Visible = False
    end
    object ProxyPaintBox: TPaintBox
      Left = 3
      Top = 21
      Width = 108
      Height = 81
      OnPaint = ProxyPaintBoxPaint
    end
    object ProxyLabelPanel: TPanel
      Left = 3
      Top = 2
      Width = 108
      Height = 18
      BevelOuter = bvNone
      Caption = 'First: 01:00:00:00'
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15792383
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object DisabledLabelPanel: TPanel
      Left = 0
      Top = 0
      Width = 114
      Height = 20
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 2
    end
    object BusyArrowPanel: TPanel
      Left = 3
      Top = 21
      Width = 108
      Height = 81
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object BusyArrows: TMTIBusy
        Left = 38
        Top = 24
        Width = 32
        Height = 32
      end
    end
  end
end
