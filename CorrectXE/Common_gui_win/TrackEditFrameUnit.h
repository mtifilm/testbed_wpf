//---------------------------------------------------------------------------


#ifndef TrackEditFrameUnitH
#define TrackEditFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------

#include "PropX.h"
#include "ColorPanel.h"
#include "ColorLabel.h"
#include <string>
using std::string;
//---------------------------------------------------------------------------

class TTrackEditFrame : public TFrame
{
__published:	// IDE-managed Components
        TTrackBar *TrackBar;
        TLabel *MinLabel;
        TLabel *MaxLabel;
        TPanel *EditAlignmentPanel;
        TEdit *Edit;
        TCheckBox *NotifyWidget;
   TColorLabel *TitleLabel;
        TPanel *TrackBarDisabledPanel;
        TPanel *EditDisabledPanel;
    TLabel *EditDisabledLabel;
	TColorPanel *RedGreenTick;
	TColorPanel *TrackBarMinPositionTick;
	TColorPanel *TrackBarMaxPositionTick;
        TTimer *DeferredUpdateTimer;
        void __fastcall TrackBarChange(TObject *Sender);
        void __fastcall EditExit(TObject *Sender);
        void __fastcall EditKeyPress(TObject *Sender, char &Key);
    void __fastcall EditEnter(TObject *Sender);
    void __fastcall TrackBarEnter(TObject *Sender);
    void __fastcall TrackBarExit(TObject *Sender);
    void __fastcall FrameMouseWheelDown(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled);
    void __fastcall FrameMouseWheelUp(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled);
    void __fastcall FrameResize(TObject *Sender);
    void __fastcall RedGreenTickClick(TObject *Sender);
        void __fastcall DeferredUpdateTimerTimer(TObject *Sender);
   void __fastcall TitleLabelClick(TObject *Sender);

private:	// User declarations
        bool HighlightRightToLeftFlag;
        bool InvertEditBoxNumberFlag;
        bool InhibitNotification;
        int cancelValue;
        bool inhibitUpdates;
        bool redGreenEnableFlag;
        bool redBelowThresholdFlag;
        int redGreenThreshold;
        bool dontNotifyIfMouseButtonIsStillDown;

        void NotifyChanged(void);      // by changing NotifyWidget
        bool CopyTrackBarToEditBox();
        bool CopyEditBoxToTrackBar();
        void DrawHighlight(void);
        void FocusOnTrackBar(void);
        void UpdateRedGreenStuff();


public:		// User declarations
        __fastcall TTrackEditFrame(TComponent* Owner);

        void Enable(bool newEnabled);
        void SetMinAndMax(int newMin, int newMax);
        void GetMinAndMax(int &currentMin, int &currentMax);
        void SetLabels(const string &newTitleLabel,
                                        const string &newMinLabel,
                                        const string &newMaxLabel);
        void SetHighlightRightToLeft(bool newFlag);
        void SetInvertEditBoxNumber(bool newFlag);
        void EnableRedGreenHack(bool enableFlag, bool redBelowThreshold=true);
        void SetRedGreenThreshold(int threshold);
        void SetDontNotifyIfMouseButtonIsStillDown(bool flag);

        void SetValue(int newPos);
        int GetValue();

        void TakeFocus();
        void NotifyTrackBarEntered();
        void NotifyTrackBarExited();

        static void ClearActiveTrackBar();
        static bool ProcessKeyDown(WORD Key, TShiftState Shift);
        static bool ProcessKeyUp(WORD Key, TShiftState Shift);

        CBHook TrackEditValueChange;
        CBHook TrackEditGotFocus;
        CBHook TrackEditEnterKeyPressed;
};
//---------------------------------------------------------------------------
extern PACKAGE TTrackEditFrame *TrackEditFrame;
//---------------------------------------------------------------------------
#endif
