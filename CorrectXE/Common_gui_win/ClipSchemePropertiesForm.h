//---------------------------------------------------------------------------

#ifndef ClipSchemePropertiesFormH
#define ClipSchemePropertiesFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Clip3.h"

//---------------------------------------------------------------------------
class TClipSchemePropForm : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *ClipSchemeInfoGroup;
        TLabel *TypeLabel;
        TLabel *Label3;
        TLabel *NameLabel;
        TLabel *DescriptionLabel;
        TLabel *ImageFormatLabel;
        TGroupBox *ColorGroup;
        TLabel *PixelComponentsLabel;
        TLabel *BitsPerComponentLabel;
        TLabel *ColorSpaceLabel;
        TLabel *PixelValuesLabel;
        TLabel *MinimumLabel;
        TLabel *MaximumLabel;
        TLabel *BlackLabel;
        TLabel *WhiteLabel;
        TLabel *MinRYValueLabel;
        TLabel *MinGUValueLabel;
        TLabel *MinBVValueLabel;
        TLabel *RYLabel;
        TLabel *GULabel;
        TLabel *BVLabel;
        TLabel *MaxRYValueLabel;
        TLabel *MaxGUValueLabel;
        TLabel *MaxBVValueLabel;
        TLabel *BlackRYValueLabel;
        TLabel *BlackGUValueLabel;
        TLabel *BlackBVValueLabel;
        TLabel *WhiteGUValueLabel;
        TLabel *WhiteBVValueLabel;
        TLabel *PixelComponentsValueLabel;
        TLabel *BitsPerComponentValueLabel;
        TLabel *ColorSpaceValueLabel;
        TLabel *NameValueLabel;
        TLabel *TypeValueLabel;
        TLabel *DescriptionValueLabel;
        TLabel *ImageFormatValueLabel;
        TButton *Button1;
        TLabel *WhiteRYValueLabel;
        TGroupBox *AudioGroup;
        TLabel *NumberChannelsLabel;
        TLabel *SampleRateLabel;
        TLabel *NumberChannelsValueLabel;
        TLabel *SampleRateValueLabel;
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations

        CClipInitInfo clipInitInfo;
public:		// User declarations

        int SetScheme(const string clipInitFileName);
        void FillClipPropLabelValues();

        __fastcall TClipSchemePropForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TClipSchemePropForm *ClipSchemePropForm;
//---------------------------------------------------------------------------
#endif
