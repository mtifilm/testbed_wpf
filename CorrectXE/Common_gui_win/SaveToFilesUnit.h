//---------------------------------------------------------------------------


#ifndef SaveToFilesUnitH
#define SaveToFilesUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>

#include <string>
using std::string;
//---------------------------------------------------------------------------

class ComboBoxHistoryList;

//---------------------------------------------------------------------------

class TSaveToFilesFrame : public TFrame
{
__published:	// IDE-managed Components
    TLabel *FilePathLabel;
    TLabel *TemplateLabel;
    TComboBox *PathComboBox;
    TButton *BrowseButton;
    TLabel *FirstFrameLabel1;
    TLabel *FilesFirstFrameLabel2;
    TEdit *FirstFrameEdit;
    TLabel *PoundSignMsgLabel1;
    TLabel *PoundSignMsgLabel2;
    TSaveDialog *SaveDialog;
    void __fastcall BrowseButtonClick(TObject *Sender);
    void __fastcall PathComboBoxEnter(TObject *Sender);
    void __fastcall PathComboBoxExit(TObject *Sender);
    void __fastcall PathComboBoxKeyPress(TObject *Sender,
          char &Key);
    void __fastcall PathComboBoxSelect(TObject *Sender);

private:	// User declarations
    string MyIniFileName;
    string NameAtComboBoxEntry;

    AnsiString BrowseForNewDestFile();
    void HandleComboBoxReturnKey();

public:		// User declarations
    void InitFromIniFile(const string &iniFileName);
    bool IsValid();
    int OK();
    string GetName();
    int GetFirstFrame();

    __fastcall TSaveToFilesFrame(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TSaveToFilesFrame *SaveToFilesFrame;
//---------------------------------------------------------------------------
#endif
