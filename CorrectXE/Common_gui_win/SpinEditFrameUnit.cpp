//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SpinEditFrameUnit.h"
#include <string>
using std::string;
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "CSPIN"
#pragma resource "*.dfm"
TSpinEditFrame *SpinEditFrame;
//---------------------------------------------------------------------------

/* static */
//TSpinEditFrame * TSpinEditFrame::CreateFromTemplate(TComponent* owner,
//                                                    TCSpinEdit *spinEditTemplate,
//                                                    const string& name)
//{
//   // Factory method
//   TSpinEditFrame *newSpinEdit = new TSpinEditFrame(owner);
//   newSpinEdit->Name = name.c_str();
//   newSpinEdit->Parent = spinEditTemplate->Parent;
//   newSpinEdit->Left = spinEditTemplate->Left;
//   newSpinEdit->Top = spinEditTemplate->Top;
//   newSpinEdit->Width = spinEditTemplate->Width;
//   newSpinEdit->Edit->Width = spinEditTemplate->Width - 14;
//   newSpinEdit->DownButtonOutlinePanel->Left = newSpinEdit->Edit->Width - 2;
//   newSpinEdit->UpButtonOutlinePanel->Left = newSpinEdit->Edit->Width - 2;
//   newSpinEdit->ParentFont = true;
//   newSpinEdit->Edit->ParentFont = true;
//   newSpinEdit->OnEnter = spinEditTemplate->OnEnter;
//   newSpinEdit->OnExit = spinEditTemplate->OnExit;
//   newSpinEdit->MinValue = spinEditTemplate->MinValue;
//   newSpinEdit->MaxValue = spinEditTemplate->MaxValue;
//   newSpinEdit->SetValue(spinEditTemplate->Value);
//
//   return newSpinEdit;
//}
////---------------------------------------------------------------------------
//
///* static */
//TSpinEditFrame * TSpinEditFrame::New(TComponent* owner, TWinControl *parent,
//                                     int left, int top, int width,
//                                     const string& name)
//{
//   // Factory method
//   TSpinEditFrame *newSpinEdit = new TSpinEditFrame(owner);
//   newSpinEdit->Name = name.c_str();
//   newSpinEdit->Parent = parent;
//   newSpinEdit->Left = left;
//   newSpinEdit->Top = top;
//   newSpinEdit->Width = width - 14;
//   newSpinEdit->DownButtonOutlinePanel->Left = newSpinEdit->Edit->Width - 2;
//   newSpinEdit->UpButtonOutlinePanel->Left = newSpinEdit->Edit->Width - 2;
//
//   return newSpinEdit;
//}
//---------------------------------------------------------------------------

__fastcall TSpinEditFrame::TSpinEditFrame(TComponent* Owner)
        : TFrame(Owner)
{
   MinValue = 0;
   MaxValue = 100;
   OldValue = 0;
   ActiveFlag = true;
   _OnChange = nullptr;
}

//---------------------------------------------------------------------------
void __fastcall TSpinEditFrame::EditEnter(TObject *Sender)
{
	OldValue = GetValue();
}
//---------------------------------------------------------------------------
void __fastcall TSpinEditFrame::EditExit(TObject *Sender)
{
   SetValueInternal(GetValue());      // forces max/min check
}
//---------------------------------------------------------------------------
void __fastcall TSpinEditFrame::DownButtonClick(TObject *Sender)
{
   // If the Edit has focus, accept the value and take focus away from it.
   EditExit(Sender);
   FocusOnSpinButton();

   int currentValue = GetValue();
   currentValue -= _increment;
   if (currentValue >= MinValue)
   {
      SetValue(currentValue);   // NOT setValueInternal!
   }
   else
   {
      SetValue(MinValue);   // NOT setValueInternal!
   }
}
//---------------------------------------------------------------------------
void __fastcall TSpinEditFrame::UpButtonClick(TObject *Sender)
{
   // If the Edit has focus, accept the value and take focus away from it.
   EditExit(Sender);
   FocusOnSpinButton();

   int currentValue = GetValue();
   currentValue += _increment;
   if (currentValue <= MaxValue)
   {
      SetValue(currentValue);   // NOT setValueInternal!
   }
   else
   {
      SetValue(MaxValue);   // NOT setValueInternal!
   }
}

//---------------------------------------------------------------------------
void __fastcall TSpinEditFrame::EditKeyPress(TObject *Sender, char &Key)
{
   // TODO: USE MTIKeyDefs.h DEFINES!! QQQ

   const char ESC_CODE = 0x1B;
   const char CTRL_Z_CODE = 0x1A;
   const char DEL_CODE = 0x7F;

   if (Key == '\r')
   {
      Key = '\0';
      EditExit(Sender);
      FocusOnSpinButton(); // to avoid leaving focus in an edit control
      return;
   }

   if (Key == ESC_CODE || Key == CTRL_Z_CODE)
   {
      Edit->Text = AnsiString(OldValue);
      Key = '\0';
      EditExit(Sender);
      FocusOnSpinButton(); // to avoid leaving focus in an edit control
      return;
   }

   if (((Key >= ' ' && Key < '0') || (Key > '9' && Key < DEL_CODE))
    && !(Key == '-' && MinValue < 0 && Edit->Text.Trim().IsEmpty()))
   {
      Beep();
      Key = '\0';
   }
}
//---------------------------------------------------------------------------
void __fastcall TSpinEditFrame::FrameMouseWheelDown(TObject *Sender,
      TShiftState Shift, TPoint &MousePos, bool &Handled)
{
   DownButtonClick(Sender);
   Handled = true;
}
//---------------------------------------------------------------------------
void __fastcall TSpinEditFrame::FrameMouseWheelUp(TObject *Sender,
      TShiftState Shift, TPoint &MousePos, bool &Handled)
{
   UpButtonClick(Sender);
   Handled = true;
}
//---------------------------------------------------------------------------

void TSpinEditFrame::Activate(bool newActivated)
{
   if (newActivated != ActiveFlag)
   {
      if (newActivated)
      {
         ActiveFlag = true;
         Enable(true);
         SetValue(OldValue);
      }
      else
      {
         Enable(false);
         Edit->Text = "";
         ActiveFlag = false;
      }
   }
}
//---------------------------------------------------------------------------

void TSpinEditFrame::Enable(bool newEnabled)
{
   if (!ActiveFlag)
      return;

   if (newEnabled && !Edit->Enabled)
   {
      Edit->Enabled = true;
      Edit->Color = clWindow;
      UpButton->Enabled = true;
      DownButton->Enabled = true;
      SetValue(OldValue);
   }
   else if (Edit->Enabled && !newEnabled)
   {
      Edit->Enabled = false;
      Edit->Color = clBtnFace;
      UpButton->Enabled = false;
      DownButton->Enabled = false;
      SetValue(OldValue);
   }
}
//---------------------------------------------------------------------------

void TSpinEditFrame::SetRange(int newMin, int newMax)
{
   if (!ActiveFlag)
      return;

   MinValue = newMin;
   MaxValue = newMax;
   SetValue(OldValue);   // NOT setValueInternal!
}

void TSpinEditFrame::SetRange(int newMin, int newMax, int newIncrement)
{
	_increment = newIncrement;
    SetRange(newMin, newMax);
}

//---------------------------------------------------------------------------

void TSpinEditFrame::GetRange(int &currentMin, int &currentMax)
{
   currentMin = MinValue;
   currentMax = MaxValue;
}
//---------------------------------------------------------------------------

 void __fastcall TSpinEditFrame::SetValue(int newValue)
{
   if (!ActiveFlag)
   {
      OldValue = newValue;
      Edit->Text = "";
      return;
	}

	EditEnter(NULL);
   SetValueInternal(newValue);
   EditExit(NULL);
}
//---------------------------------------------------------------------------

void TSpinEditFrame::SetValueInternal(int newValue)
{
   if (newValue < MinValue) newValue = MinValue;
   if (newValue > MaxValue) newValue = MaxValue;
   AnsiString newText = AnsiString(newValue);
   if ((OldValue != newValue) || (newText != Edit->Text))
   {
	  OldValue = newValue;
	  Edit->Text = AnsiString(newValue);

     // Two ways to notify of change!
	  SpinEditValueChange.Notify();
	  if (_OnChange != nullptr)
     {
        _OnChange(this);
     }
   }
}

//---------------------------------------------------------------------------

int __fastcall TSpinEditFrame::GetValue(void)
{
   if (!ActiveFlag)
      return -1;

   int result;
   if (Edit->Text.IsEmpty())
   {
      result = OldValue;
      Edit->Text = AnsiString(OldValue);
   }
   else
   {
      try
      {
         result = Edit->Text.ToInt();
      }
      catch (...)   // "Can't happen!"
      {
         result = OldValue;
         Edit->Text = AnsiString(OldValue);
      }
   }

   return result;
}
//---------------------------------------------------------------------------

void TSpinEditFrame::FocusOnSpinButton(void)
{
   TCustomForm *parentForm = NULL;
   TControl *currentObject = this;

   do
   {
     TControl *parentObject = currentObject->Parent;
     parentForm = dynamic_cast<TCustomForm *>(parentObject);
     currentObject = parentObject;
   }
   while (parentForm == NULL && currentObject != NULL);

   if (parentForm != NULL)
      parentForm->FocusControl(UpButtonOutlinePanel);

}
//---------------------------------------------------------------------------


