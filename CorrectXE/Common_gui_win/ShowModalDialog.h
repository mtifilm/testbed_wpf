/*
   File Name:  ShowModalDialog.h
   Created: June 3, 2013 by John Mertus
   Version 1.00

  This hack is used to show a modal dialog box when the calling is set
  to AlwaysOnTop.   This is because of a bug in Borland

*/
#include <vcl.h>

extern "C"
{
  int ShowModalDialog(TCustomForm *form);
}
