//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SmallLabeledProxyUnit.h"
#include "ProxyDisplayer.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MTIBusy"
#pragma resource "*.dfm"
//TSmallLabeledProxyFrame *SmallLabeledProxyFrame;

//---------------------------------------------------------------------------
__fastcall TSmallLabeledProxyFrame::TSmallLabeledProxyFrame(TComponent* Owner)
    : TFrame(Owner), TimecodeBeingShown(CTimecode::NOT_SET)
{
    Reset();
}
//---------------------------------------------------------------------------

void TSmallLabeledProxyFrame::Reset()
{
    Label = "";
    ProxyLabelPanel->Caption = "";
    ProxyLabelPanel->Visible = false;
    DisabledLabelPanel->Visible = true;
    AddTimecodeToLabel = true;
    ProxyImage->Visible = false;
    ProxyPaintBox->Visible = false;
    BusyArrows->Busy = false;
    BusyArrowPanel->Visible = false;
    LastDisplayerUsed = 0;
    TimecodeBeingShown = CTimecode(CTimecode::NOT_SET);
}
//---------------------------------------------------------------------------

void TSmallLabeledProxyFrame::SetLabel(const AnsiString &label,
                                       bool addTimecode)
{
    Label = label;
    AddTimecodeToLabel = addTimecode;
    doSetLabel();
}
//---------------------------------------------------------------------------

void TSmallLabeledProxyFrame::ShowFrame(ProxyDisplayer *displayer,
                                        const CTimecode &timecode)
{
    int retVal = 0;

    if ((!ProxyPaintBox->Visible) || (timecode != TimecodeBeingShown)) {

        TimecodeBeingShown = timecode;
        if ((!ProxyLabelPanel->Visible) || AddTimecodeToLabel) {
            doSetLabel();
        }

        //ProxyPaintBox->Visible = false;
        //BusyArrows->Busy = true;
        //BusyArrowPanel->Visible = true;
        retVal = displayer->RenderProxy(timecode, ProxyPaintBox, DrawXOnProxy);

        if (retVal == 0) {
            ProxyPaintBox->Visible = true;
            LastDisplayerUsed = displayer;      // QQQ EVIL
        } else {
           ProxyPaintBox->Visible = false;
           LastDisplayerUsed = 0;
        }
        //BusyArrows->Busy = false;
        //BusyArrowPanel->Visible = false;
    }
}
//---------------------------------------------------------------------------

void TSmallLabeledProxyFrame::RedrawFrame()
{
    if (LastDisplayerUsed != 0
        && TimecodeBeingShown != CTimecode(CTimecode::NOT_SET))
    {
         LastDisplayerUsed->RenderProxy(TimecodeBeingShown, ProxyPaintBox,
                              DrawXOnProxy, true); // Force redraw of the frame
    }
}
//---------------------------------------------------------------------------

void TSmallLabeledProxyFrame::doSetLabel()
{
    if (AddTimecodeToLabel) {
        char tcBuf[101];

        TimecodeBeingShown.getTimeASCII(tcBuf);
        ProxyLabelPanel->Caption = Label +  AnsiString(tcBuf);
    } else {
        ProxyLabelPanel->Caption = Label;
    }
    ProxyLabelPanel->Visible = true;
    DisabledLabelPanel->Visible = false;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TSmallLabeledProxyFrame::ProxyPaintBoxPaint(
      TObject *Sender)
{
    if (ProxyPaintBox->Visible && LastDisplayerUsed != 0) {
        LastDisplayerUsed->RenderProxy(TimecodeBeingShown, ProxyPaintBox,
                                        DrawXOnProxy);
    }
}
//---------------------------------------------------------------------------

