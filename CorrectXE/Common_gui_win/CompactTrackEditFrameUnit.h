//---------------------------------------------------------------------------


#ifndef CompactTrackEditFrameUnitH
#define CompactTrackEditFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------

#include "PropX.h"
#include "ColorPanel.h"
#include <string>
using std::string;
//---------------------------------------------------------------------------

class TCompactTrackEditFrame : public TFrame
{
__published:	// IDE-managed Components
        TTrackBar *TrackBar;
        TPanel *EditAlignmentPanel;
        TEdit *Edit;
        TCheckBox *NotifyWidget;
        TLabel *TitleLabel;
        TPanel *TrackBarDisabledPanel;
        TPanel *EditDisabledPanel;
    TLabel *EditDisabledLabel;
        TTimer *DeferredUpdateTimer;
        void __fastcall TrackBarChange(TObject *Sender);
        void __fastcall EditExit(TObject *Sender);
        void __fastcall EditKeyPress(TObject *Sender, char &Key);
    void __fastcall EditEnter(TObject *Sender);
    void __fastcall TrackBarEnter(TObject *Sender);
    void __fastcall TrackBarExit(TObject *Sender);
    void __fastcall FrameMouseWheelDown(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled);
    void __fastcall FrameMouseWheelUp(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled);
        void __fastcall DeferredUpdateTimerTimer(TObject *Sender);
   void __fastcall FrameMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
   void __fastcall FrameEnter(TObject *Sender);
   void __fastcall TitleLabelClick(TObject *Sender);


private:	// User declarations
        bool HighlightRightToLeftFlag;
        bool InvertEditBoxNumberFlag;
        bool InhibitNotification;
        int cancelValue;
        bool inhibitUpdates;
        bool dontNotifyIfMouseButtonIsStillDown;

        void NotifyChanged(void);      // by changing NotifyWidget
        bool CopyTrackBarToEditBox();
        bool CopyEditBoxToTrackBar();
        void DrawHighlight(void);
        void FocusOnTrackBar(void);
        void UpdateRedGreenStuff();

public:		// User declarations
        __fastcall TCompactTrackEditFrame(TComponent* Owner);

        void Enable(bool newEnabled);
        void SetMinAndMax(int newMin, int newMax);
        void GetMinAndMax(int &currentMin, int &currentMax);
        void SetLabel(const string &newTitleLabel);
        void SetHighlightRightToLeft(bool newFlag);
        void SetInvertEditBoxNumber(bool newFlag);
        void SetDontNotifyIfMouseButtonIsStillDown(bool flag);

        void SetValue(int newPos);
        int GetValue();

        void TakeFocus();
        void NotifyTrackBarEntered();
        void NotifyTrackBarExited();

        static void ClearActiveTrackBar();
        static bool ProcessKeyDown(WORD Key, TShiftState Shift);
        static bool ProcessKeyUp(WORD Key, TShiftState Shift);

        CBHook TrackEditValueChange;
        CBHook TrackEditGotFocus;
        CBHook TrackEditEnterKeyPressed;
};
//---------------------------------------------------------------------------
extern PACKAGE TCompactTrackEditFrame *CompactTrackEditFrame;
//---------------------------------------------------------------------------
#endif
