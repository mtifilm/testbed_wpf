object RgbFileOptionsDialog: TRgbFileOptionsDialog
  Left = 1023
  Top = 354
  AutoSize = True
  BorderStyle = bsDialog
  Caption = 'File Color Space Settings'
  ClientHeight = 320
  ClientWidth = 400
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BackgroundPanel: TPanel
    Left = 0
    Top = 0
    Width = 400
    Height = 320
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      400
      320)
    object CancelButton: TButton
      Left = 316
      Top = 290
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
    end
    object ColorSpaceGroupBox: TGroupBox
      Left = 8
      Top = 8
      Width = 385
      Height = 97
      Caption = ' Color Space '
      TabOrder = 1
      object Log10RadioButton: TRadioButton
        Left = 16
        Top = 48
        Width = 89
        Height = 17
        Caption = '10-bit Log'
        TabOrder = 0
        OnClick = Log10RadioButtonClick
      end
      object Linear10RadioButton: TRadioButton
        Left = 16
        Top = 24
        Width = 89
        Height = 17
        Caption = '10-bit Linear'
        Checked = True
        TabOrder = 1
        TabStop = True
        OnClick = Linear10RadioButtonClick
      end
      object Linear8RadioButton: TRadioButton
        Left = 264
        Top = 24
        Width = 113
        Height = 17
        Caption = '8-bit Linear'
        TabOrder = 2
        OnClick = Linear8RadioButtonClick
      end
      object Linear16RadioButton: TRadioButton
        Left = 264
        Top = 48
        Width = 97
        Height = 17
        Caption = '16-bit Linear'
        TabOrder = 3
        OnClick = Linear16RadioButtonClick
      end
      object Linear12RadioButton: TRadioButton
        Left = 140
        Top = 24
        Width = 89
        Height = 17
        Caption = '12-bit Linear'
        TabOrder = 4
        OnClick = Linear12RadioButtonClick
      end
      object Log12RadioButton: TRadioButton
        Left = 140
        Top = 48
        Width = 89
        Height = 17
        Caption = '12-bit Log'
        TabOrder = 5
        OnClick = Log12RadioButtonClick
      end
    end
    object Gamma: TGroupBox
      Left = 208
      Top = 112
      Width = 185
      Height = 152
      Caption = 'Gamma'
      TabOrder = 2
      object Label3: TLabel
        Left = 104
        Top = 111
        Width = 51
        Height = 13
        Caption = '[ 0.1 - 3.0 ]'
        Visible = False
      end
      object Gamma10RadioButton: TRadioButton
        Left = 16
        Top = 45
        Width = 121
        Height = 17
        Caption = 'Neutral (1.0)'
        TabOrder = 0
        OnClick = Gamma10RadioButtonClick
      end
      object Gamma17RadioButton: TRadioButton
        Left = 16
        Top = 24
        Width = 97
        Height = 17
        Caption = 'Standard (1.7)'
        Checked = True
        TabOrder = 1
        TabStop = True
        OnClick = Gamma17RadioButtonClick
      end
      object GammaCustomRadioButton: TRadioButton
        Left = 16
        Top = 88
        Width = 153
        Height = 17
        Caption = 'Custom Value (0.1 - 3.0):'
        TabOrder = 2
        OnClick = GammaCustomRadioButtonClick
      end
      object GammaVDoubleEdit: VDoubleEdit
        Left = 36
        Top = 108
        Width = 41
        Height = 21
        Decimals = 1
        OnDoubleChange = GammaVDoubleEditDoubleChange
      end
      object GammaCustomUpDown: TUpDown
        Left = 76
        Top = 107
        Width = 17
        Height = 25
        Min = 0
        Position = 0
        TabOrder = 4
        Wrap = False
        OnClick = GammaCustomUpDownClick
      end
      object Gamma22222RadioButton: TRadioButton
        Left = 16
        Top = 66
        Width = 137
        Height = 17
        Caption = 'CCIR 601/709  (2.2222)'
        TabOrder = 5
        OnClick = Gamma22222RadioButtonClick
      end
    end
    object ImportColorSpaceGroupBox: TGroupBox
      Left = 8
      Top = 8
      Width = 385
      Height = 97
      Caption = ' Color Space '
      TabOrder = 3
      object BitsPerCOmponentLabel: TLabel
        Left = 204
        Top = 10
        Width = 94
        Height = 13
        Caption = 'Bits per component:'
      end
      object BitsPerComponentValueLabel: TLabel
        Left = 312
        Top = 10
        Width = 12
        Height = 13
        Caption = '10'
      end
      object ImportLogRadioButton: TRadioButton
        Left = 16
        Top = 64
        Width = 89
        Height = 17
        Caption = 'Log'
        TabOrder = 0
        OnClick = Log10RadioButtonClick
      end
      object ImportLinearRadioButton: TRadioButton
        Left = 16
        Top = 40
        Width = 89
        Height = 17
        Caption = 'Linear'
        Checked = True
        TabOrder = 1
        TabStop = True
        OnClick = Linear10RadioButtonClick
      end
      object ImportSmpte240RadioButton: TRadioButton
        Left = 128
        Top = 40
        Width = 97
        Height = 17
        Caption = 'SMPTE 240'
        TabOrder = 2
        OnClick = Linear8RadioButtonClick
      end
      object ImportCcir709RadioButton: TRadioButton
        Left = 128
        Top = 64
        Width = 89
        Height = 17
        Caption = 'CCIR 709'
        TabOrder = 3
        OnClick = Linear16RadioButtonClick
      end
      object ImportCcir601BgRadioButton: TRadioButton
        Left = 248
        Top = 40
        Width = 97
        Height = 17
        Caption = 'CCIR 601 BG'
        TabOrder = 4
        OnClick = Linear8RadioButtonClick
      end
      object ImportCcir601MRadioButton: TRadioButton
        Left = 248
        Top = 64
        Width = 89
        Height = 17
        Caption = 'CCIR 601 M'
        TabOrder = 5
        OnClick = Linear16RadioButtonClick
      end
    end
    object OkButton: TButton
      Left = 232
      Top = 290
      Width = 75
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 4
    end
    object PixelValueRangeGroupBox: TGroupBox
      Left = 8
      Top = 112
      Width = 185
      Height = 152
      Caption = ' Pixel Value Range '
      TabOrder = 5
      object Label1: TLabel
        Left = 36
        Top = 132
        Width = 17
        Height = 13
        Caption = 'Min'
      end
      object Label2: TLabel
        Left = 112
        Top = 132
        Width = 20
        Height = 13
        Caption = 'Max'
      end
      object PixelValueRangeMinVDoubleEdit: VDoubleEdit
        Left = 36
        Top = 108
        Width = 41
        Height = 21
        Decimals = 0
        OnDoubleChange = PixelValueRangeMinVDoubleEditDoubleChange
      end
      object PixelValueRangeMaxVDoubleEdit: VDoubleEdit
        Left = 112
        Top = 108
        Width = 41
        Height = 21
        Decimals = 0
        OnDoubleChange = PixelValueRangeMaxVDoubleEditDoubleChange
      end
      object RangeMaxUpDown: TUpDown
        Left = 152
        Top = 107
        Width = 17
        Height = 25
        Min = 0
        Position = 0
        TabOrder = 2
        Wrap = False
        OnClick = RangeMaxUpDownClick
      end
      object RangeMinUpDown: TUpDown
        Left = 76
        Top = 107
        Width = 17
        Height = 25
        Min = 0
        Position = 0
        TabOrder = 3
        Wrap = False
        OnClick = RangeMinUpDownClick
      end
      object RangeFullRadioButton: TRadioButton
        Left = 16
        Top = 24
        Width = 129
        Height = 17
        Caption = 'Full Range (0 - 1023)'
        Checked = True
        TabOrder = 4
        TabStop = True
        OnClick = RangeFullRadioButtonClick
      end
      object RangeRestrictedRadioButton: TRadioButton
        Left = 16
        Top = 56
        Width = 161
        Height = 17
        Caption = 'Restricted Range (95 - 685)'
        TabOrder = 5
        OnClick = RangeRestrictedRadioButtonClick
      end
      object RangeCustomRadioButton: TRadioButton
        Left = 16
        Top = 88
        Width = 113
        Height = 17
        Caption = 'Custom Range:'
        TabOrder = 6
        OnClick = RangeCustomRadioButtonClick
      end
    end
    object SetDefaultsButton: TButton
      Left = 8
      Top = 290
      Width = 97
      Height = 21
      Anchors = [akLeft, akBottom]
      Caption = 'Reset to Defaults'
      TabOrder = 6
      OnClick = SetDefaultsButtonClick
    end
    object Panel1: TPanel
      Left = 0
      Top = 276
      Width = 400
      Height = 3
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 7
    end
  end
end
