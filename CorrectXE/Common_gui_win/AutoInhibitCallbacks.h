#ifndef AutoInhibitCallbacksH
#define AutoInhibitCallbacksH

// This can be used to inhibit any click callbacks on controls
// that inherit from TButton such as CheckBox
// Usage
//  void __fastcall TColorBreathingForm::GlobalFlickerRoiAnalysisCheckBoxClick(TObject *Sender)
//  {
//  	AutoInhibitClickCallback inhibitCallback(Sender);
//      Now changing the button won't create a call back
//  }
//     Now channing the button will create a call back

class AutoInhibitClickCallback
{
public:
	AutoInhibitClickCallback(TObject *control)
    {
        // We might want to add other types later
        // E.g. use if (String(ClassRef->ClassName()) == "TCheckBox")
        //
		_control = dynamic_cast<TButton*>(control);
        if (_control == nullptr)
        {
            return;
        }

        _notifyEvent = _control->OnClick;
        _control->OnClick = nullptr;
    }

    ~AutoInhibitClickCallback() {_control->OnClick = _notifyEvent;}

private:
    TButton *_control = nullptr;
    TNotifyEvent _notifyEvent = nullptr;
};

#endif
