object ClipListViewFrame: TClipListViewFrame
  Left = 0
  Top = 0
  Width = 464
  Height = 320
  Anchors = [akLeft, akTop, akRight, akBottom]
  TabOrder = 0
  DesignSize = (
    464
    320)
  object ClipListView: TListView
    Left = 0
    Top = 0
    Width = 464
    Height = 320
    Align = alClient
    BevelInner = bvLowered
    Columns = <
      item
        Caption = 'Clips'
        Width = 150
      end
      item
        Caption = 'Status'
        Width = -2
        WidthType = (
          -2)
      end
      item
        Caption = 'Format'
        Width = -2
        WidthType = (
          -2)
      end
      item
        Caption = 'In'
        Width = -2
        WidthType = (
          -2)
      end
      item
        Caption = 'Out'
        Width = -2
        WidthType = (
          -2)
      end
      item
        Caption = 'Duration'
        Width = -2
        WidthType = (
          -2)
      end
      item
        Caption = 'Description'
        Width = 100
      end
      item
        Caption = 'Created'
        Width = -2
        WidthType = (
          -2)
      end>
    Constraints.MinHeight = 200
    Constraints.MinWidth = 200
    HideSelection = False
    MultiSelect = True
    ReadOnly = True
    SortType = stText
    TabOrder = 0
    TabStop = False
    ViewStyle = vsReport
    OnChange = ListItemChanged
    OnDblClick = SawDoubleClick
    OnKeyPress = ClipListViewKeyPress
  end
  object SelectedClipChangedNotifier: TCheckBox
    Left = 328
    Top = 0
    Width = 17
    Height = 17
    TabOrder = 1
    Visible = False
  end
  object DoubleClickNotifier: TCheckBox
    Left = 344
    Top = 0
    Width = 17
    Height = 17
    TabOrder = 2
    Visible = False
  end
  object BusyArrows: TMTIBusy
    Left = 192
    Top = 184
    Width = 32
    Height = 32
    Anchors = [akLeft, akTop, akRight, akBottom]
  end
  object ArrowCheckTimer: TTimer
    Interval = 250
    OnTimer = ArrowCheckTimerTimer
    Left = 224
    Top = 184
  end
end
