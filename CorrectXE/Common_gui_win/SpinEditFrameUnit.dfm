object SpinEditFrame: TSpinEditFrame
  Left = 0
  Top = 0
  Width = 102
  Height = 22
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  OnMouseWheelDown = FrameMouseWheelDown
  OnMouseWheelUp = FrameMouseWheelUp
  DesignSize = (
    102
    22)
  object Edit: TEdit
    Left = 0
    Top = 0
    Width = 86
    Height = 22
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight, akBottom]
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = '99999'
    OnEnter = EditEnter
    OnExit = EditExit
    OnKeyPress = EditKeyPress
  end
  object DownButtonOutlinePanel: TPanel
    Left = 85
    Top = 10
    Width = 16
    Height = 11
    Anchors = [akTop, akRight]
    TabOrder = 1
    DesignSize = (
      16
      11)
    object DownButtonClippingPanel: TPanel
      Left = 1
      Top = 1
      Width = 14
      Height = 9
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        14
        9)
      object DownButton: TSpeedButton
        Left = -2
        Top = -2
        Width = 18
        Height = 13
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Anchors = [akLeft, akTop, akRight, akBottom]
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Glyph.Data = {
          2E020000424D2E02000000000000360000002800000012000000090000000100
          180000000000F8010000000000000000000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A0000006A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A0000000000000000006A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A5252525252525252526A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          0000000000000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A5252525252
          525252525252525252526A6A6A6A6A6A00006A6A6A0000000000000000000000
          000000000000000000006A6A6A6A6A6A52525252525252525252525252525252
          52525252526A6A6A000000000000000000000000000000000000000000000000
          0000000000525252525252525252525252525252525252525252525252525252
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        Layout = blGlyphBottom
        NumGlyphs = 2
        ParentFont = False
        Spacing = 0
        OnClick = DownButtonClick
        ExplicitWidth = 20
        ExplicitHeight = 15
      end
    end
  end
  object UpButtonOutlinePanel: TPanel
    Left = 85
    Top = 1
    Width = 16
    Height = 11
    Anchors = [akTop, akRight]
    Locked = True
    TabOrder = 2
    DesignSize = (
      16
      11)
    object UpButtonClippingPanel: TPanel
      Left = 1
      Top = 1
      Width = 14
      Height = 9
      Anchors = [akTop, akRight, akBottom]
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        14
        9)
      object UpButton: TSpeedButton
        Left = -2
        Top = -2
        Width = 18
        Height = 13
        Anchors = [akLeft, akTop, akRight, akBottom]
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Glyph.Data = {
          2E020000424D2E02000000000000360000002800000012000000090000000100
          180000000000F8010000000000000000000000000000000000006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A000000000000000000000000000000000000000000000000
          0000000000525252525252525252525252525252525252525252525252525252
          00006A6A6A0000000000000000000000000000000000000000006A6A6A6A6A6A
          5252525252525252525252525252525252525252526A6A6A00006A6A6A6A6A6A
          0000000000000000000000000000006A6A6A6A6A6A6A6A6A6A6A6A5252525252
          525252525252525252526A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A0000000000
          000000006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252525252525252526A
          6A6A6A6A6A6A6A6A00006A6A6A6A6A6A6A6A6A6A6A6A0000006A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A5252526A6A6A6A6A6A6A6A6A6A6A6A
          00006A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00006A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
          6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A0000}
        Layout = blGlyphBottom
        NumGlyphs = 2
        ParentFont = False
        StyleElements = [seFont, seClient]
        OnClick = UpButtonClick
        ExplicitWidth = 20
        ExplicitHeight = 15
      end
    end
  end
end
