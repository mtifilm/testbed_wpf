//---------------------------------------------------------------------------


#ifndef SmallLabeledProxyUnitH
#define SmallLabeledProxyUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <jpeg.hpp>

#include "ProxyBitmap.h"
#include "MTIBusy.h"
#include "timecode.h"

class ProxyDisplayer;

//---------------------------------------------------------------------------
class TSmallLabeledProxyFrame : public TFrame
{
__published:	// IDE-managed Components
    TPanel *ProxyPanel;
    TImage *ProxyImage;
    TPanel *ProxyLabelPanel;
    TPanel *BusyArrowPanel;
    TMTIBusy *BusyArrows;
    TPanel *DisabledLabelPanel;
    TPaintBox *ProxyPaintBox;
    void __fastcall ProxyPaintBoxPaint(TObject *Sender);

private:	// User declarations
    CTimecode TimecodeBeingShown;
    ProxyDisplayer *LastDisplayerUsed;   // QQQ EVIL
    AnsiString Label;
    bool AddTimecodeToLabel;
    bool DrawXOnProxy;

    void doSetLabel();
    
public:		// User declarations
    void Reset();
    void SetLabel(const AnsiString &label, bool addTimecode=false);
    void XOut(bool drawX) { DrawXOnProxy = drawX; };
    void ShowFrame(ProxyDisplayer *displayer, const CTimecode &timecode);
    void RedrawFrame();

    __fastcall TSmallLabeledProxyFrame(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TSmallLabeledProxyFrame *SmallLabeledProxyFrame;
//---------------------------------------------------------------------------
#endif
