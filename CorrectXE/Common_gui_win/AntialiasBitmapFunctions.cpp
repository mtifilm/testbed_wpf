//---------------------------------------------------------------------------

#pragma hdrstop

#include "AntialiasBitmapFunctions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
// --------------------Average---------------------John Mertus----Feb 2003-----

Graphics::TBitmap *Average(Graphics::TBitmap *inBitmap, int blockSize)

    // Generic routine that takes the input bitmap and averages
    // blockSize blocks to produce the Output bitmap.
    //
    // Input bitmaps must be 24 bits.
    // Output bitmap must be 1/blockSize, rounded down of the input
    //
    // ****************************************************************************
{
   Graphics::TBitmap *bmOut = new TBitmap();
   bmOut->PixelFormat = pf24bit;
   bmOut->Width = inBitmap->Width / blockSize;
   bmOut->Height = inBitmap->Height / blockSize;

   // Now average all the values in an SxS square
   Byte *ptr;
   for (int y = 0; y < inBitmap->Height; y += blockSize)
   {
      for (int x = 0; x < 3 * inBitmap->Width; x += 3 * blockSize)
      {
         int r = 0;
         int g = 0;
         int b = 0;
         for (int row = 0; row < blockSize; row++)
         {
            ptr = (Byte*)inBitmap->ScanLine[y + row] + x;
            for (int col = 0; col < 3 * blockSize; col += 3)
            {
               r += ptr[col];
               g += ptr[col + 1];
               b += ptr[col + 2];
            }
         }
         ptr = (Byte*)bmOut->ScanLine[y / blockSize] + x / blockSize;
         ptr[0] = r / (blockSize * blockSize);
         ptr[1] = g / (blockSize * blockSize);
         ptr[2] = b / (blockSize * blockSize);
      }
   }

   return bmOut;
}

// -----------------BlowupBitmap---------------------John Mertus----Jan 2017-----

Graphics::TBitmap *BlowupBitmap(Graphics::TBitmap *inBitmap, const TRect &rect, int blockSize)

// This returns a bitmap blownup by blockSize x blockSize of the area in
// inBitmap defined by the rectangle
{
   auto blowupBitmap = new Graphics::TBitmap();
   blowupBitmap->PixelFormat = pf24bit;
   blowupBitmap->Width = blockSize * rect.Width();
   blowupBitmap->Height = blockSize * rect.Height();

   // This upreses the image by blocksize
   TRect destRect(0, 0, blowupBitmap->Width, blowupBitmap->Height);
   blowupBitmap->Canvas->StretchDraw(destRect, inBitmap);
   return blowupBitmap;
}


// -------------AntiAliasedEllipse-----------------John Mertus----Jan 2017-----

void AntiAliasedEllipse(Graphics::TBitmap *bitmap, const TRect &inRect, TPen *pen, int blockSize)

    // Generic routine that takes the input bitmap and draws a rectange by inRect
    // bitmap is the input/output bitmap
    // inRect defines the size of the ellipse
    // Pen expresses the color, mode and pen size
    // blocksize is how much to blow up then average down.
    // The default of 4 is the best to use
    //
    // ****************************************************************************
{
   // We need to pad because the line is centered at the rectangle
   auto pad = blockSize;
   TRect rect(inRect.left - pad, inRect.top - pad, inRect.right + pad, inRect.bottom + pad);

   auto blowupBitmap = BlowupBitmap(bitmap, rect, blockSize);

   // This upreses  the image by blocksize
   TRect destRect(0, 0, blowupBitmap->Width, blowupBitmap->Height);

   // Draw the ellipse
   auto destCanvas = blowupBitmap->Canvas;
   destCanvas->CopyRect(destRect, bitmap->Canvas, rect);
   destCanvas->Brush->Style = bsClear;
   destCanvas->Pen->Color = pen->Color;
   destCanvas->Pen->Width = blockSize * pen->Width;
   destCanvas->Pen->Mode = pen->Mode;
   TRect ellipseRect(pad, pad, destRect.Width() - pad, destRect.Height() - pad);
   destCanvas->Ellipse(ellipseRect);

   // Now downres it.
   auto smallBitmap = Average(blowupBitmap, blockSize);

   // Copy it back
   bitmap->Canvas->Draw(rect.left, rect.top, smallBitmap);

   // Clean it out
   delete blowupBitmap;
   delete smallBitmap;
}

void AntiAliasedEllipse(Graphics::TCanvas *canvas, const TRect &inRect, int blockSize)

    // Generic routine that takes the input bitmap and draws a rectange by inRect
    // bitmap is the input/output bitmap
    // inRect defines the size of the ellipse
    // Pen expresses the color, mode and pen size
    // blocksize is how much to blow up then average down.
    // The default of 4 is the best to use
    //
    // ****************************************************************************
{
   // We need to pad because the line is centered at the rectangle
   auto pad = blockSize;
   auto pen = canvas->Pen;

   TRect rect(inRect.left - pad, inRect.top - pad, inRect.right + pad, inRect.bottom + pad);

   auto *bitmap = new TBitmap();
   bitmap->Assign(canvas);
   auto blowupBitmap = BlowupBitmap(bitmap, rect, blockSize);

   // This upreses  the image by blocksize
   TRect destRect(0, 0, blowupBitmap->Width, blowupBitmap->Height);

   // Draw the ellipse
   auto destCanvas = blowupBitmap->Canvas;
   destCanvas->CopyRect(destRect, bitmap->Canvas, rect);
   destCanvas->Brush->Style = bsClear;
   destCanvas->Pen->Color = pen->Color;
   destCanvas->Pen->Width = blockSize * pen->Width;
   destCanvas->Pen->Mode = pen->Mode;
   TRect ellipseRect(pad, pad, destRect.Width() - pad, destRect.Height() - pad);
   destCanvas->Ellipse(ellipseRect);

   // Now downres it.
   auto smallBitmap = Average(blowupBitmap, blockSize);

   // Copy it back
   canvas->Draw(rect.left, rect.top, smallBitmap);

   // Clean it out
   delete blowupBitmap;
   delete smallBitmap;
}
