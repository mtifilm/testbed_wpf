//---------------------------------------------------------------------------


#ifndef BinTreeViewUnitH
#define BinTreeViewUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>

#include <string>
using std::string;

//---------------------------------------------------------------------------
class TBinTreeViewFrame : public TFrame
{
__published:	// IDE-managed Components
    TPanel *BinListHeaderPanel;
    TImage *NewBinImage;
    TTreeView *BinTreeView;
    TCheckBox *SelectionChangedNotifier;
    TCheckBox *DoubleClickNotifier;
    void __fastcall SelectionChanged(TObject *Sender, TTreeNode *Node);
    void __fastcall SawDoubleClick(TObject *Sender);
   void __fastcall BinTreeViewExpanding(TObject *Sender, TTreeNode *Node, bool &AllowExpansion);


private:	// User declarations
    string _selectedBinPath;
    string _myIniFileName;

	void AddDummyBinTreeNodeChildIfNestedBins(string &binPath, TTreeNode * parentNode);
	void AddBinTreeNodeChildrenRecursively(string &path, TTreeNode* parentNode);
	void AddOneLevelOfBinTreeNodeChildren(string &binPath, TTreeNode * parentNode, bool forceRefresh = false);
	int  FindClipListIndexByClipName(const string &clipName);
	string getBinPathFromBinTreeNode(TTreeNode* node);
   TTreeNode *getBinTreeNodeFromBinPath(const string& path);

public:		// User declarations
    void   InitFromIniFile(const string &iniFileName);
    int    OK();
    void   RefreshBinTree();
    void   SetSelectedBin(const string& binPath);
    string GetSelectedBin();

    __fastcall TBinTreeViewFrame(TComponent* Owner);
//---------------------------------------------------------------------------

};
//---------------------------------------------------------------------------
//extern PACKAGE TBinTreeViewFrame *BinTreeViewFrame;
//---------------------------------------------------------------------------
#endif
