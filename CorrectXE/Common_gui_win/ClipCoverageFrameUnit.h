//---------------------------------------------------------------------------


#ifndef ClipCoverageFrameUnitH
#define ClipCoverageFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "ColorPanel.h"
//---------------------------------------------------------------------------
class TClipCoverageFrame : public TFrame
{
__published:	// IDE-managed Components
    TPanel *BackgroundPanel;
    TLabel *OutTimecodeLabel;
    TLabel *InTimecodeLabel;
    TLabel *InfoLabel;
    TLabel *WarningLabel;
    TPanel *FillerWellPanel;
   TColorPanel *FillerPanel;
    TLabel *TotalFrameCountLabel;
private:	// User declarations
    int  UsableClientWidth;
public:		// User declarations
    void Reset();
    void SetTimecodeLabels(const AnsiString &inLabel,
                           const AnsiString &outLabel);
    void SetCoverage(int total, int start, int stop);
    void SetWarning(const AnsiString &warningLabel);

    __fastcall TClipCoverageFrame(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TClipCoverageFrame *ClipCoverageFrame;
//---------------------------------------------------------------------------
#endif
