#include "timecode.h"

namespace {
//---------------------------------------------------------------------------

    const CTimecode UnsetTimecode(-1);
//---------------------------------------------------------------------------

    string CppStringFromAnsiString(const AnsiString &ansiString)
    {
        string retVal;
        char *cp = new char[ansiString.Length() + 1];

        strcpy(cp, ansiString.c_str());
        retVal = cp;
        delete[] cp;

        return retVal;
    }
//---------------------------------------------------------------------------

    void StripExtension(string &fileName, string &extension)
    {
        unsigned int dot = fileName.rfind('.');
        if (dot > 0) {
            extension = fileName.substr(dot + 1);
            fileName.erase(dot);
        } else {
            extension = "";
        }
    }
//---------------------------------------------------------------------------

}; // end of local namespace


 