//---------------------------------------------------------------------------

#ifndef AntialiasBitmapFunctionsH
#define AntialiasBitmapFunctionsH

#include <vcl.h>
#pragma hdrstop

//---------------------------------------------------------------------------

Graphics::TBitmap *Average(Graphics::TBitmap *inBitmap, int blockSize);
Graphics::TBitmap *BlowupBitmap(Graphics::TBitmap *inBitmap, const TRect &rect, int blockSize);
void AntiAliasedEllipse(Graphics::TBitmap *bitmap, const TRect &inRect, TPen *pen, int blockSize = 4);
void AntiAliasedEllipse(Graphics::TCanvas *canvas, const TRect &inRect, TPen *pen, int blockSize = 4);

#endif
