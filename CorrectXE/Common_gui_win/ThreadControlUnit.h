//---------------------------------------------------------------------------

#ifndef ThreadControlUnitH
#define ThreadControlUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "TrackEditFrameUnit.h"
//---------------------------------------------------------------------------
class TThreadControlDialog : public TForm
{
__published:	// IDE-managed Components
        TTrackEditFrame *ConcurrentFramesTrackEdit;
        TTrackEditFrame *ProcessingThreadsTrackEdit;
        TTrackEditFrame *SlicesPerThreadTrackEdit;
        TRadioButton *RowSlicesRadioButton;
        TRadioButton *ColumnSlicesRadioButton;
        TCheckBox *NoopCheckBox;
private:	// User declarations
public:		// User declarations
        __fastcall TThreadControlDialog(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TThreadControlDialog *ThreadControlDialog;
//---------------------------------------------------------------------------
#endif
