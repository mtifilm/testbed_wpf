object DisabledTriTimecodePanel: TDisabledTriTimecodePanel
  Left = 0
  Top = 0
  Width = 134
  Height = 76
  TabOrder = 0
  object DurationLabel: TLabel
    Left = 9
    Top = 56
    Width = 40
    Height = 13
    Caption = 'Duration'
  end
  object OutLabel: TLabel
    Left = 32
    Top = 31
    Width = 17
    Height = 13
    Caption = 'Out'
  end
  object InLabel: TLabel
    Left = 40
    Top = 8
    Width = 9
    Height = 13
    Caption = 'In'
  end
  object OutPanel: TPanel
    Left = 55
    Top = 27
    Width = 66
    Height = 21
    Alignment = taLeftJustify
    BevelInner = bvLowered
    BevelOuter = bvLowered
    Caption = '01:00:00:00'
    TabOrder = 0
  end
  object InPanel: TPanel
    Left = 55
    Top = 4
    Width = 66
    Height = 21
    Alignment = taLeftJustify
    BevelInner = bvLowered
    BevelOuter = bvLowered
    Caption = '01:00:00:00'
    TabOrder = 1
  end
  object DurationPanel: TPanel
    Left = 55
    Top = 52
    Width = 66
    Height = 21
    Alignment = taLeftJustify
    BevelInner = bvLowered
    BevelOuter = bvLowered
    Caption = '00:00:00:00'
    TabOrder = 2
  end
end
