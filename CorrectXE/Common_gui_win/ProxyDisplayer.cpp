
#include <vcl.h>

#include "ProxyDisplayer.h"

#include "ClipAPI.h"
#include "Convert.h"
#include "ImageFileMediaAccess.h"
#include "MediaFileIO.h"
#include "MTImalloc.h"
#include <memory>

//---------------------------------------------------------------------------

ProxyDisplayer::ProxyDisplayer()
{
    m_converter = new CConvert(1);
    m_handle = 0;
    m_framing = FRAMING_SELECT_VIDEO;
    m_haveOverrides = false;
}
//---------------------------------------------------------------------------

ProxyDisplayer::~ProxyDisplayer()
{
    reset();
    delete m_converter;
}
//---------------------------------------------------------------------------

namespace {

void bltProxy(ProxyBitmap &proxyBitmap, int width, int height,
              TCanvas *proxyCanvas)
{
    unsigned int *imageProxyBits = proxyBitmap.getBitmapFrameBuffer();
    if (imageProxyBits == 0) {
        return;
    }
    TRGBTriple q;
    Graphics::TBitmap *DataBitMap = new Graphics::TBitmap;

    DataBitMap->PixelFormat = pf24bit;
    DataBitMap->Height = height;
    DataBitMap->Width = width;

    for (int r=0; r < DataBitMap->Height; r++) {
        TRGBTriple *ptr = (TRGBTriple *)DataBitMap->ScanLine[r];
        for (int c = 0; c < DataBitMap->Width; c++) {
            unsigned int pixel = imageProxyBits[(r*DataBitMap->Width) + c];
            q.rgbtRed   = (pixel >> 16) & 0xFF;
            q.rgbtGreen = (pixel >> 8) & 0xFF;
            q.rgbtBlue  = pixel & 0xFF;
            ptr[c] = q;
        }
    }
    //proxyImage->Picture->Assign(DataBitMap);
    proxyCanvas->Draw(0,0,DataBitMap);
    delete DataBitMap;
}

void drawRedXOnCanvas(int width, int height, TCanvas *proxyCanvas)
{
  proxyCanvas->Pen->Color = clRed;
  proxyCanvas->MoveTo(0,0);
  proxyCanvas->LineTo(width, height);
  proxyCanvas->MoveTo(0, height);
  proxyCanvas->LineTo(width, 0);

}

} // local namespace
//---------------------------------------------------------------------------

void ProxyDisplayer::reset()
{
    CacheIterator iter;

    m_sourceName = "";
    for (iter = m_cache.begin(); iter != m_cache.end(); ++iter) {
        delete (*iter);
    }
    m_cache.clear();

    m_haveOverrides = false;
}
//---------------------------------------------------------------------------

void ProxyDisplayer::SetSourceName(const string &name)
{
    reset();                // flush the cache
    m_sourceName = name;
}
//---------------------------------------------------------------------------

void ProxyDisplayer::SetDisplayContext(HDC handle)
{
    m_handle = handle;
}
//---------------------------------------------------------------------------

int ProxyDisplayer::RenderProxy(const CTimecode &timecode, TImage *image,
                    bool drawX, bool forceDraw)
{
    int retVal = 0;

    if (forceDraw) {
        flushBitmapFromCache(timecode);
    }

    retVal = drawProxyOnCanvas(timecode, image->Width, image->Height,
                               image->Canvas);
    if (drawX) {
        drawRedXOnCanvas(image->Width, image->Height, image->Canvas);
    }

    return retVal;
}
//---------------------------------------------------------------------------

int ProxyDisplayer::RenderProxy(const CTimecode &timecode, TPaintBox *image,
                    bool drawX, bool forceDraw)
{
    if (m_sourceName.empty())
        return -1;   // no can do

    int retVal = 0;

    if (forceDraw) {
        flushBitmapFromCache(timecode);
    }

    retVal = drawProxyOnCanvas(timecode, image->Width, image->Height,
                             image->Canvas);
    if (drawX) {
        drawRedXOnCanvas(image->Width, image->Height, image->Canvas);
    }

    return retVal;
}
//---------------------------------------------------------------------------

void ProxyDisplayer::SetOverrides(const CSOverrides &overrides)
{
    m_overrides = overrides;
    m_haveOverrides = true;
}
//---------------------------------------------------------------------------

int ProxyDisplayer::drawProxyOnCanvas(const CTimecode &timecode,
                              int width, int height, TCanvas *canvas)
{
    int retVal = 0;
    ProxyBitmap *bitmap = getBitmap(timecode, width, height);

    if (bitmap != 0) {
        bltProxy(*bitmap, width, height, canvas);
    } else {
        retVal = -1;
    }

    return retVal;
}
//---------------------------------------------------------------------------

ProxyBitmap *ProxyDisplayer::getBitmap(const CTimecode &timecode,
                                       int width, int height)
{
    ProxyBitmap *bitmap = 0;
    CacheIterator iter;

    for (iter = m_cache.begin(); iter != m_cache.end(); ++iter) {
        if ((*iter)->timecode == timecode) {
            break;
        }
    }
    if (iter != m_cache.end()) {
        bitmap = (*iter)->bitmap;
    } else {
        CacheEntry *newEntry = new CacheEntry(m_converter);

        if (newEntry != 0) {
            if (newEntry->bitmap == 0) {
                delete newEntry;
            } else {
                newEntry->timecode = timecode;
                newEntry->bitmap->initBitmap(m_handle, width, height);
                if (0 == fillBitmap(*(newEntry->bitmap), timecode)) {
                    m_cache.push_back(newEntry);
                    bitmap = newEntry->bitmap;
                } else {
                    delete newEntry;
                    newEntry = 0;
                }
            }
        }
    }
    return bitmap;
}
//---------------------------------------------------------------------------

static int ComputeFrameCount(
	const string &sourceName,  // must be existing clip if FPS != 0
	const CTimecode &sourceIn,
	const CTimecode &sourceOut,
	const bool readVideoFramesFlag,
	int &sourceFrameCount)
{
#if 1
	sourceFrameCount = sourceOut - sourceIn;
	return 0;
#else
	 int retVal = 0;
	 sourceFrameCount = 0;

	 /*
	  * First we want to compute the number of frames to
	  * be read from the source. Two possibilities:
	  * 1. The FPS in the timecodes is 0. Just do the subtraction.
	  * 2. Get frame indexes from the appropriate clip Video Frame List,
	  *    and use those.
	  */
	 if (sourceIn.getFramesPerSecond() == 0 || sourceName == "") {
		  sourceFrameCount = (sourceOut - sourceIn);
	 } else {
		  int firstFrameIndex = 0;
		  int outFrameIndex = 0;
		  CMediaIO mio;
		  CTimecode inFrameTC(sourceIn);
		  CTimecode outFrameTC(sourceOut);

		  mio.strImageName = sourceName;
		  mio.iImageFraming = (readVideoFramesFlag? FRAMING_SELECT_VIDEO
																: FRAMING_SELECT_FILM);
		  retVal = mio.OpenForInput(MEDIA_CLASS_CLIP);
		  if (retVal != 0 || mio.vflp == NULL) {
				if (retVal == 0)
					 retVal = GENERIC_ERROR;
				goto error_out;
		  }

		  /*
			* Constrain the timecodes for sanity.
			*/
		  if (inFrameTC < mio.vflp->getInTimecode()) {
            inFrameTC = mio.vflp->getInTimecode();
				retVal = IMGTOOL_ERROR_BAD_IN_TIMECODE;
		  }
        if (outFrameTC >= mio.vflp->getOutTimecode()) {
				if (outFrameTC > mio.vflp->getOutTimecode()) {
					retVal = IMGTOOL_ERROR_BAD_OUT_TIMECODE;
				}
				outFrameTC = mio.vflp->getOutTimecode() + (-1);
				sourceFrameCount = 1;
		 }

        firstFrameIndex = mio.vflp->getFrameIndex(
												inFrameTC.getHours(),
												inFrameTC.getMinutes(),
												inFrameTC.getSeconds(),
												inFrameTC.getFrames());

		  outFrameIndex = mio.vflp->getFrameIndex(
												outFrameTC.getHours(),
												outFrameTC.getMinutes(),
												outFrameTC.getSeconds(),
												outFrameTC.getFrames());
		  /*
			* Get -1 for timecodes outside the clip
			*/
        if (firstFrameIndex < 0
				|| outFrameIndex < 0
            || outFrameIndex < firstFrameIndex)
		  {
            sourceFrameCount = 0;
            retVal = IMGTOOL_ERROR_BOGUS_PARAMETER;
				goto error_out;
        }

		  sourceFrameCount += outFrameIndex - firstFrameIndex;
    }

error_out:

    if (sourceFrameCount < 0)
		  sourceFrameCount = 0;

	 return retVal;
#endif
}


int ProxyDisplayer::fillBitmap(ProxyBitmap &bitmap, const CTimecode &timecode)
{
	int retVal = 0;

	CTimecode nextTC(timecode);
	int frameCount = 0;

	while (frameCount == 0 && retVal == 0)
	{
		nextTC = nextTC + 1;
		retVal = ComputeFrameCount(m_sourceName, timecode, nextTC, (m_framing == FRAMING_SELECT_VIDEO), frameCount);
	}

	if (retVal != 0)
	{
		return retVal;
	}

	// NOTE m_sourceName is a TEMPLATE.
	string filePath;
	CImageFileMediaAccess::GenerateFileName(m_sourceName, filePath, timecode.getAbsoluteTime());

	MediaFrameBufferSharedPtr frameBuffer;
	MediaFileIO mediaFileIO;
	retVal = mediaFileIO.readFrameBuffer(filePath, frameBuffer);
	if (retVal != 0)
	{
		return retVal;
	}

	// Check if user overrode some values.
	CImageFormat imageFormat = frameBuffer->getImageFormat();
	if (m_haveOverrides)
	{
		MTI_UINT16 componentValues[3];

		imageFormat.setGamma(m_overrides.gamma);

		if (m_overrides.colorSpace != IF_COLOR_SPACE_INVALID)
		{
			imageFormat.setColorSpace(m_overrides.colorSpace);
		}

		for (int i = 0; i < 3; ++i)
		{
			componentValues[i] = m_overrides.maxValue;
		}

		imageFormat.setComponentValueMax(componentValues);

		for (int i = 0; i < 3; ++i)
		{
			componentValues[i] = m_overrides.minValue;
		}

		imageFormat.setComponentValueMin(componentValues);
	}

	unsigned char *stupidBuf[2] = {(unsigned char *)frameBuffer->getImageDataPtr(), nullptr};
	bitmap.renderToBitmap(stupidBuf, &imageFormat);

	return 0;
}
//---------------------------------------------------------------------------

void ProxyDisplayer::flushBitmapFromCache(const CTimecode &timecode)
{
	 CacheIterator iter;

	 for (iter = m_cache.begin(); iter != m_cache.end(); ++iter) {
		  if ((*iter)->timecode == timecode) {
				break;
		  }
	 }
	 if (iter != m_cache.end()) {
		  delete(*iter);
		  m_cache.erase(iter);
	 }
}
//---------------------------------------------------------------------------
