//---------------------------------------------------------------------------

#ifndef ClipPickerUnitH
#define ClipPickerUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include "BinTreeViewUnit.h"
#include "ClipListViewUnit.h"
#include <string>
using std::string;

class CClipInitInfo;

//---------------------------------------------------------------------------
class TClipPickerForm : public TForm
{
__published:	// IDE-managed Components
    TSplitter *Splitter;
    TPanel *BottomPanel;
    TButton *OkButton;
    TButton *CancelButton;
    TLabel *ClipLabel;
    TClipListViewFrame *ClipListViewFrame;
    TBinTreeViewFrame *BinTreeViewFrame;
    TEdit *SelectedClipEdit;
    TLabel *ImageFormatLabel1;
    TEdit *FilterFormatEdit;
    void __fastcall BinTreeViewFrameSelectionChangedNotifierClick(
          TObject *Sender);
    void __fastcall ClipListViewFrameDoubleClickNotifierClick(
          TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall SelectedClipEditKeyPress(TObject *Sender, char &Key);
    void __fastcall ClipListViewFrameSelectedClipChangedNotifierClick(
          TObject *Sender);
    void __fastcall SelectedClipEditExit(TObject *Sender);

private:	// User declarations
    string BinName;
    string ClipName;
    string Format;

public:		// User declarations
    // THESE ARE NOT PRESENTLY USED
    enum EFlags {
        NO_FLAGS               = 0,
        BIN_MUST_EXIST         = 1,
        CLIP_MUST_EXIST        = 2,
        CLIP_MUST_NOT_EXIST    = 4,
        CLIP_MUST_MATCH_FORMAT = 8
    };
    static string PickAClip(EFlags flags,
                            const string *iniFilePath = 0,
                            const string *clipFilter = 0,
                            const string *defClipPath = 0);

protected:
    void Init(const string &iniFileName, const string &format);
    int OK();
    void SetBinName(const string &name);
    void SetClipName(const string &name);
    string GetBinName();
    string GetClipName();

    __fastcall TClipPickerForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TClipPickerForm *ClipPickerForm;
//---------------------------------------------------------------------------
#endif
