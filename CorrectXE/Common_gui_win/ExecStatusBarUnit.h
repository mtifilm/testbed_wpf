//---------------------------------------------------------------------------


#ifndef ExecStatusBarUnitH
#define ExecStatusBarUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>

#include "ToolProgressMonitor.h"
#include "ColorPanel.h"
#include <string>
#include <stack>
using std::string;
using std::stack;

class CHRTimer;
class CThreadLock;

//---------------------------------------------------------------------------

class TExecStatusBarFrame : public TFrame, public IToolProgressMonitor
{
__published:	// IDE-managed Components
    TPanel *BackPanel;
    TLabel *StatusMessageLabel;
    TLabel *ToolMessageValue;
    TLabel *StatusMessageValue;
    TPanel *ReadoutPanel;
    TLabel *TotalValue1;
    TLabel *TotalLabel1;
    TLabel *RemainingValue1;
    TLabel *RemainingLabel1;
    TLabel *PercentValue1;
    TLabel *ElapsedValue1;
    TLabel *ElapsedLabel1;
	TColorPanel *ProgressPanelDark;
    TPanel *DisabledPanel;
    TTimer *ElapsedTimer;
	TColorPanel *ProgressPanelLight;
    TLabel *PerFrameValue1;
    TLabel *TotalValue2;
    TLabel *TotalLabel2;
    TLabel *RemainingValue2;
    TLabel *RemainingLabel2;
    TLabel *ElapsedValue2;
    TLabel *ElapsedLabel2;
    TLabel *PerFrameValue2;
    TLabel *PercentValue2;
    TLabel *DisabledElapsedLabel;
    TLabel *DisabledRemainingLabel;
    TLabel *DisabledTotalLabel;
    TLabel *DisabledPerFrameValue;
    TLabel *DisabledPercentValue;
    void __fastcall ElapsedTimerTimer(TObject *Sender);

private:	// User declarations
    CHRTimer *_HRTimer;
    CThreadLock *_ThreadLock;
    int _FramesTotal;
	 int _BumpsPerFrame;
	 int _FramesElapsed;
    int _Bumps;
    double _ElapsedTimeInSeconds;
	 double _TimePerFrameInSeconds;
    enum { SB_STATE_IDLE, SB_STATE_RUNNING, SB_STATE_DONE } _State;
    AnsiString _StatusMessage;
    AnsiString _ToolMessage;
    stack<AnsiString> _StatusMessageStack;
    stack<std::tuple<int, int, int, int, int, double>> _StateStack;
    int _ToolMessageRightEdgeOffset;
    int _ProgressPanelDarkWidth;

    string formatShortDuration(double durationInSeconds);
    string formatLongDuration(double durationInSeconds);
    void kickTheTimer(void);
    void showMessages(void);
	 void setStatusMessageInternal(const char *newMessage);
	 void setFrameCountInternal(int newFrameCount, int newBumpsPerFrame, int preElapsedFrames, double timePerFrameInSeconds);
    void startProgressInternal(void);
    void bumpFrameInternal(int bumpCount);

public:		// User declarations
    __fastcall TExecStatusBarFrame(TComponent* Owner);
    virtual __fastcall ~TExecStatusBarFrame();

    virtual void SetIdle(bool setMessage=false);
    virtual void SetStatusMessage(const char *newMessage);
    virtual void PushStatusMessage(const char *newMessage);
    virtual void PopStatusMessage(void);
    virtual void SetToolMessage(const char *newMessage);
	 virtual void SetFrameCount(int newFrameCount, int newBumpsPerFrame=1, int preElapsedFrames=0, double timePerFrameInSeconds=0.0);
    virtual void StartProgress(void);
	 virtual void BumpProgress(int bumpCount=1);
	 virtual void StopProgress(bool processedToEnd=true);
    virtual void Push();
    virtual void Pop();
	 virtual double GetTimePerFrameInSeconds();

    // WARNING ONLY CALL FROM GUI THREAD
    void updateReadouts(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TExecStatusBarFrame *ExecStatusBarFrame;
//---------------------------------------------------------------------------
#endif
