#ifndef MTIDialogsH
#define MTIDialogsH
/*
    File: MTIDialogs.h
    Created March 21, 2002 by John Mertus

    The purpose of this is just to make the header files consistent
    for open/close/lseek between UNIX and WINDOW
*/
#include "machine.h"
//
//*****************Common Dialog Boxes******************************************
//
//  These boxes are just wrappers around the system specific boxes.
//  The problem is that a dialog box needs to be owned by the form that
//  displays the box.  But some dialog boxes are called by formless modules.
//
//  Ideally, no formless module should have a dialogbox, but there are some places
//  that these boxes are useful.
//
//   Thus, for a dialog box called from a window object that has a window handle
//  one should use the macro call; e.g., MTIErrorDialog.  This automatically
//  passes the handle down.   For a windowless object, one should add an _
//  For example, _MTIErrorDialog.
//
//   _MTIxxxDialogs can be, but should never be, used in forms.  The Macro
//  will give an error if used in the windowless object.
//
//
//

#define MTIInformationDialog _MTIInformationDialog
extern int _MTIInformationDialog(const string &msg);
extern int _MTIInformationDialog(HWND Handle, const string &msg);

#define MTIInformationDialogModeless _MTIInformationDialogModeless
extern int _MTIInformationDialogModeless(const string &msg);
extern int _MTIInformationDialogModeless(HWND Handle, const string &msg);

#define MTIErrorDialog _MTIErrorDialog
extern int _MTIErrorDialog(const string &msg);
extern int _MTIErrorDialog(HWND Handle, const string &msg);

#define MTIConfirmationDialog _MTIConfirmationDialog
extern int _MTIConfirmationDialog(const string &msg);
extern int _MTIConfirmationDialog(HWND Handle, const string &msg);

#define MTIWarningDialog _MTIWarningDialog
extern int _MTIWarningDialog(const string &msg);
extern int _MTIWarningDialog(HWND Handle, const string &msg);

#define MTICancelConfirmationDialog _MTICancelConfirmationDialog
extern int _MTICancelConfirmationDialog(const string &msg);
extern int _MTICancelConfirmationDialog(HWND Handle, const string &msg);

extern int _MTISevereErrorDialog(const string &);
extern int _MTISevereErrorDialogH(HWND Handle, const string &, const string &, int);

#define MTISevereErrorDialog _MTISevereErrorDialogHL
#define _MTISevereErrorDialog _MTISevereErrorDialogHL
extern int _MTISevereErrorDialogHL(const string &);
extern int _MTISevereErrorDialogHL(const string &, const string &, int);

#define MTISevereErrorRetryDialog _MTISevereErrorRetryDialogH
extern int _MTISevereErrorRetryDialogH(HWND Handle, const string &, const string &, int);

#define _MTISevereErrorRetryDialog _MTISevereErrorRetryDialogHL
extern int _MTISevereErrorRetryDialogHL(const string &, const string &, int);

// This is an ABORT/RETRY/CANCEL dialog
extern int MTIYouMustStopToolProcessingDialog();

//
//  Returns for the dialog boxes
#define MTI_DLG_OK 0
#define MTI_DLG_YES 0
#define MTI_DLG_CANCEL 1
#define MTI_DLG_RETRY 2
#define MTI_DLG_NO 3
#define MTI_DLG_ABORT 4
#define MTI_DLG_NO_ALL 5
#define MTI_DLG_YES_ALL 6

string MTIBrowseForFolder(const string &startFolder, const string &title);


#endif
