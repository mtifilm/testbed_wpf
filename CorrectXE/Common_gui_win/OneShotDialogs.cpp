// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "OneShotDialogs.h"
#include "IniFile.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TOneShotConfirmationDialog *OneShotConfirmationDialog;

// ---------------------------------------------------------------------------
__fastcall TOneShotConfirmationDialog::TOneShotConfirmationDialog
    (TComponent* Owner) : TForm(Owner)
{
}

__fastcall TOneShotConfirmationDialog::TOneShotConfirmationDialog
    (TComponent* Owner, const std::string &msg) : TForm(Owner)
{
    InformationLabel->Caption = msg.c_str();
    auto w = InformationLabel->Width;
    auto h = InformationLabel->Height;

    // This uses the constraint to keep the form from collasping in
    if (h <= CautionImage->Height)
    {
	InformationLabel->Top = CautionImage->Top + (InformationLabel->Height - h) / 2;
    }
    else
    {
	InformationLabel->Top = CautionImage->Top;
	CautionImage->Top = InformationLabel->Top + (h - CautionImage->Height) / 2;
    }

    ClientHeight = ClientHeight + h - CautionImage->Height;
    ClientWidth = InformationLabel->Left + w + (ClientWidth - NoButton->Left - NoButton->Width);

}

int TOneShotConfirmationDialog::OneShotShow(TComponent* Owner, const std::string &msg, OneShotDialogs::OneShotData &oneShotData)
{
    if (oneShotData.NoShow)
    {
	return oneShotData.NoShowResult;
    }

    auto dialog = new TOneShotConfirmationDialog(Owner, msg);
    Beep();
    auto result = MTI_DLG_CANCEL;

    // Change into MTI dialog results
    switch (dialog->ShowModal())
    {
        case mrYes:
          result = MTI_DLG_YES;
          break;

        case mrNo:
          result = MTI_DLG_NO;
          break;

        case mrCancel:
          result = MTI_DLG_CANCEL;
          break;

        default:
          MTIassert(false);
          result = MTI_DLG_CANCEL;
    }

    if (result != MTI_DLG_CANCEL)
    {
        oneShotData.NoShow = dialog->OneShotCheckBox->Checked;
        oneShotData.NoShowResult = result;
    }

    delete dialog;

    return result;
}
// ---------------------------------------------------------------------------

