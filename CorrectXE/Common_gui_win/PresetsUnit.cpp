//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PresetsUnit.h"

#include "PresetParameterModel.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TPresetsFrame *PresetsFrame;
//---------------------------------------------------------------------------
__fastcall TPresetsFrame::TPresetsFrame(TComponent* Owner)
	: TFrame(Owner)
{
	_presetButtonArray[0] = PresetButton1;
   _presetButtonArray[1] = PresetButton2;
   _presetButtonArray[2] = PresetButton3;
	_presetButtonArray[3] = PresetButton4;

	for (int i = 0; i < 4; ++i)
	{
		_presetButtonArray[i]->Tag = i;
	}
}
// ---------------------------------------------------------------------------

void TPresetsFrame::Initialize(IPresets *presets, const string &iniFileName)
{
	_presets = presets;
	_iniFileName = iniFileName;
	SET_CBHOOK(CurrentPresetHasChanged, _presets->SelectedIndexChanged);
	SET_CBHOOK(CurrentParametersHaveChanged, _presets->CurrentParametersChanged);

	int initialSelection = _presets->GetSelectedIndex();
	if (initialSelection >= 0 && initialSelection < 4)
	{
      _presets->SetSelectedIndexAndCopyParametersToCurrent(initialSelection);
	   _presetButtonArray[initialSelection]->Down = true;
	   UpdateGuiFromCurrentPreset();
	}
}
// ---------------------------------------------------------------------------

void TPresetsFrame::UpdateGuiFromCurrentPreset()
{
	MTIassert(_presets != nullptr);
	if (_presets == nullptr)
	{
		return;
	}

	bool parametersChanged = !_presets->DoesCurrentMatchSelected();
	SavePresetButton->Enabled = parametersChanged;
	SavePresetButton->Visible = parametersChanged;
//	DisabledSaveButton->Visible = !parametersChanged;
	_presetButtonArray[_presets->GetSelectedIndex()]->Down = true;
}
// ---------------------------------------------------------------------------

void __fastcall TPresetsFrame::PresetButtonClick(TObject *Sender)
{
	auto speedButton = dynamic_cast<TSpeedButton *>(Sender);

	MTIassert(_presets != nullptr);
	if (_presets == nullptr || speedButton == nullptr)
	{
		return;
	}

	auto index = (int)(speedButton->Tag);
	_presets->SetSelectedIndexAndCopyParametersToCurrent(index);

   // Only allow all preset buttons up at first tool show.
   if (PresetButton1->AllowAllUp)
   {
      for (index = 0; index < 4; ++index)
      {
         _presetButtonArray[index]->AllowAllUp = false;
      }
   }
}
//---------------------------------------------------------------------------

void __fastcall TPresetsFrame::SavePresetButtonClick(TObject *Sender)
{
   this->SavePresets();
}

// ---------------------------------------------------------------------------

void TPresetsFrame::SavePresets()
{
	MTIassert(_presets != nullptr);
	if (_presets == nullptr)
	{
		return;
	}

	// Copy the current values to the selected preset
	_presets->CopyCurrentParametersToSelectedPreset();
	_presets->WritePresetsToIni(_iniFileName);
	UpdateGuiFromCurrentPreset();
}
//---------------------------------------------------------------------------
void TPresetsFrame::CurrentPresetHasChanged(void *Sender)
{
	UpdateGuiFromCurrentPreset();
}
//---------------------------------------------------------------------------
void TPresetsFrame::CurrentParametersHaveChanged(void *Sender)
{
	UpdateGuiFromCurrentPreset();
}
//---------------------------------------------------------------------------

	__fastcall TPresetsFrame::~TPresetsFrame(void)
   {
      // Debugging, if you find this you can get rid of it
         int x = (int)this;
         int y = 10;
         int z = x*y;
   }
