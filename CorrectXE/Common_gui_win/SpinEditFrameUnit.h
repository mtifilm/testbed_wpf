//---------------------------------------------------------------------------


#ifndef SpinEditFrameUnitH
#define SpinEditFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cspin.h"
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <Dialogs.hpp>

#include "PropX.h"
using std::string;

//---------------------------------------------------------------------------

class TSpinEditFrame : public TFrame
{
__published:	// IDE-managed Components
        TEdit *Edit;
   TPanel *UpButtonClippingPanel;
   TPanel *DownButtonClippingPanel;
   TSpeedButton *DownButton;
   TSpeedButton *UpButton;
   TPanel *UpButtonOutlinePanel;
   TPanel *DownButtonOutlinePanel;
        void __fastcall EditEnter(TObject *Sender);
        void __fastcall EditExit(TObject *Sender);
        void __fastcall DownButtonClick(TObject *Sender);
        void __fastcall UpButtonClick(TObject *Sender);
        void __fastcall EditKeyPress(TObject *Sender, char &Key);
        void __fastcall FrameMouseWheelDown(TObject *Sender,
          TShiftState Shift, TPoint &MousePos, bool &Handled);
        void __fastcall FrameMouseWheelUp(TObject *Sender,
		  TShiftState Shift, TPoint &MousePos, bool &Handled);

private:	// User declarations
		int MinValue, MaxValue, OldValue;
        int _increment = 1;
		bool ActiveFlag;
		void FocusOnSpinButton(void);
		void SetValueInternal(int newValue);
		TNotifyEvent _OnChange;

public:		// User declarations
		__fastcall TSpinEditFrame(TComponent* Owner);

		void Activate(bool newActivated);
		void Enable(bool newEnabled);
		void SetRange(int newMin, int newMax);
        void SetRange(int newMin, int newMax, int newIncrement);
		void GetRange(int &currentMin, int &currentMax);
		__fastcall void SetValue(int newValue);
		__fastcall int  GetValue(void);

		CBHook SpinEditValueChange;

//		static TSpinEditFrame *New(TComponent* Owner, TWinControl *Parent,
//								   int left, int top, int width,
//								   const string& name);
//		static TSpinEditFrame *CreateFromTemplate(TComponent* owner,
//												  TCSpinEdit *spinEditTemplate,
//												  const string& name);

 		__property int Value = {read=GetValue, write=SetValue};
        __property int Increment = {read=_increment, write=_increment};
        __property TNotifyEvent OnChange = {read = _OnChange, write=_OnChange};
};
//---------------------------------------------------------------------------
extern PACKAGE TSpinEditFrame *SpinEditFrame;
//---------------------------------------------------------------------------
#endif
