//---------------------------------------------------------------------------

#include <vcl.h>
#include "MTIio.h"
#pragma hdrstop

#include "TrackEditFrameUnit.h"
#include "MTIKeyDef.h"
#include "SharedMap.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorPanel"
#pragma link "ColorLabel"
#pragma resource "*.dfm"
TTrackEditFrame *TrackEditFrame;
//---------------------------------------------------------------------------

#define INVALID_CANCEL_VALUE 0x80000000
#define DARKISH_RED ((TColor) 0x0000C0)
#define DARK_GREEN  ((TColor) 0x008000)
#define BRIGHT_RED clRed
#define BRIGHT_GREEN ((TColor) 0x00E000)

#define ACTIVE_TRACK_EDIT_SHARED_MAP_KEY "ActiveTrackEdit"
//---------------------------------------------------------------------------

__fastcall TTrackEditFrame::TTrackEditFrame(TComponent* Owner)
        : TFrame(Owner)
        , HighlightRightToLeftFlag(false)
        , InhibitNotification(false)
        , cancelValue(INVALID_CANCEL_VALUE)
        , inhibitUpdates(false)
        , redGreenEnableFlag(false)
        , redBelowThresholdFlag(true)
        , redGreenThreshold(0)
        , dontNotifyIfMouseButtonIsStillDown(false)
{
   DrawHighlight();
}
//---------------------------------------------------------------------------

void TTrackEditFrame::Enable(bool newEnabled)
{
   if (!newEnabled)
   {
       TitleLabel->Enabled = false;
       MinLabel->Enabled = false;
       MaxLabel->Enabled = false;
       TrackBar->Enabled = false;
       //TrackBar->Color = clBtnFace;       // Not accessible
       TrackBar->SliderVisible = false;
       TrackBar->SelStart = TrackBar->Position;
       TrackBar->SelEnd = TrackBar->Position;
       TrackBarDisabledPanel->Width = TrackBar->Width - 12; // MAGIC NUMBER QQQ
       TrackBarDisabledPanel->Visible = true;
	   Edit->Enabled = false;
//	   Edit->Color = clBtnFace;
	   EditDisabledPanel->Visible = false;
       RedGreenTick->Visible = false;
   }
   else
   {
       TitleLabel->Enabled = true;
       MinLabel->Enabled = true;
       MaxLabel->Enabled = true;
       TrackBar->Enabled = true;
       //TrackBar->Color = clWindow;       // Not accessible
       TrackBar->SliderVisible = true;
       DrawHighlight();
       TrackBarDisabledPanel->Visible = false;
       Edit->Enabled = true;
//       Edit->Color = clWindow;
	   EditDisabledPanel->Visible = false;
       RedGreenTick->Visible = redGreenEnableFlag;
   }
}
//---------------------------------------------------------------------------

void TTrackEditFrame::SetMinAndMax(int newMin, int newMax)
{
   // Stupidity - order of setting can matter!!
   if (newMin > TrackBar->Max)
   {
      TrackBar->Max = newMax;
      TrackBar->Min = newMin;
   }
   else
   {
      TrackBar->Min = newMin;
      TrackBar->Max = newMax;
   }

   DrawHighlight();
}
//---------------------------------------------------------------------------

void TTrackEditFrame::GetMinAndMax(int &currentMin, int &currentMax)
{
   currentMin = TrackBar->Min;
   currentMax = TrackBar->Max;
}
//---------------------------------------------------------------------------

void TTrackEditFrame::SetLabels(const string &newTitleLabel,
                                const string &newMinLabel,
                                const string &newMaxLabel)
{
   TitleLabel->Caption = newTitleLabel.c_str();
   MinLabel->Caption = newMinLabel.c_str();
   MaxLabel->Caption = newMaxLabel.c_str();
}
//---------------------------------------------------------------------------

void TTrackEditFrame::SetValue(int newPos)
{
   if (InvertEditBoxNumberFlag)
   {
      newPos = (TrackBar->Max - newPos) + TrackBar->Min;
   }

   if (newPos > TrackBar->Max)
      newPos = TrackBar->Max;
   if (newPos < TrackBar->Min)
      newPos = TrackBar->Min;

   if (TrackBar->Position != newPos)
   {
     InhibitNotification = true;
     TrackBar->Position = newPos;
     CopyTrackBarToEditBox();
     InhibitNotification = false;
   }

   UpdateRedGreenStuff();
}

int TTrackEditFrame::GetValue()
{
   return InvertEditBoxNumberFlag ? ((TrackBar->Max - TrackBar->Position) + TrackBar->Min) : TrackBar->Position;
}
//---------------------------------------------------------------------------

bool TTrackEditFrame::CopyTrackBarToEditBox()
{
   // We return TRUE if the edit box number actually changed.
   bool retVal = false;

   if (!inhibitUpdates)
   {
      AnsiString newTextForEditBox;
      if (InvertEditBoxNumberFlag)
      {
         // WTF!! Larry wants some of the trackbars to read out backwards!!!!
         newTextForEditBox =  (TrackBar->Max - TrackBar->Position) + TrackBar->Min;
      }
      else
      {
         newTextForEditBox = TrackBar->Position;
      }

      if (Edit->Text != newTextForEditBox)
      {
         inhibitUpdates = true;
         Edit->Text = newTextForEditBox;
         inhibitUpdates = false;

         if (EditDisabledLabel->Tag >= 0)
            EditDisabledLabel->Caption = TrackBar->Position;
         else
            EditDisabledLabel->Caption = "";

         retVal = true;
      }
   }

   UpdateRedGreenStuff();
   return retVal;
}
//---------------------------------------------------------------------------

bool TTrackEditFrame::CopyEditBoxToTrackBar()
{
   // We return TRUE if the track bar position actually changed.
   bool retVal = false;

   if (!inhibitUpdates)
   {
      int newTrackBarPosition;
      int editBoxNumber = Edit->Text.ToIntDef(0);
      if (InvertEditBoxNumberFlag)
      {
         // WTF!! Larry wants some of the trackbars to read out backwards!!!!
         newTrackBarPosition = (TrackBar->Max - editBoxNumber) + TrackBar->Min;
      }
      else
      {
         newTrackBarPosition = editBoxNumber;
      }

      if (TrackBar->Position != newTrackBarPosition)
      {
         inhibitUpdates = true;
         TrackBar->Position = newTrackBarPosition;
         inhibitUpdates = false;

         retVal = true;
      }
   }

   UpdateRedGreenStuff();
   return retVal;
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::TrackBarChange(TObject *Sender)
{
   DrawHighlight();
   UpdateRedGreenStuff();

   // The copy function returns true if the edit box actually changed.
   if (CopyTrackBarToEditBox())
   {
      short leftMouseButtonState = ::GetAsyncKeyState(VK_LBUTTON);
      bool leftMouseButtonIsDown = leftMouseButtonState < 0;  // if MSB is set
      DeferredUpdateTimer->Enabled = false;

      if (dontNotifyIfMouseButtonIsStillDown && leftMouseButtonIsDown)
      {
         // Mouse button is still down... restart the timer
         DeferredUpdateTimer->Enabled = true;
      }
      else
      {
         NotifyChanged();
         TrackEditValueChange.Notify();
      }
   }
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::EditEnter(TObject *Sender)
{
   cancelValue = TrackBar->Position;
   Edit->SelStart = 0;
   Edit->SelLength = Edit->Text.Length();
   TitleLabel->Font->Style = (TFontStyles() << fsBold);
   TrackEditGotFocus.Notify();
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::EditExit(TObject *Sender)
{
   int newEditBoxNumber = atoi(StringToStdString(Edit->Text).c_str());
   int oldPosition = TrackBar->Position;

   if (newEditBoxNumber > TrackBar->Max)
      newEditBoxNumber = TrackBar->Max;
   if (newEditBoxNumber < TrackBar->Min)
      newEditBoxNumber = TrackBar->Min;

   Edit->Text = newEditBoxNumber;

   // Copy function returns true if the track bar position actually changed.
   if (CopyEditBoxToTrackBar())
   {
      NotifyChanged();
      TrackEditValueChange.Notify();
   }

   Edit->SelStart = 0;
   Edit->SelLength = Edit->Text.Length();
   cancelValue = INVALID_CANCEL_VALUE;
   TitleLabel->Font->Style = TFontStyles();
   UpdateRedGreenStuff();
}
//---------------------------------------------------------------------------

void TTrackEditFrame::NotifyChanged(void)
{
   if (!InhibitNotification)
   {
      NotifyWidget->Checked = !NotifyWidget->Checked;
#if 0 // The checked seems to fire the OnClick, so don't notify twice!!
      if (NotifyWidget->OnClick != NULL)
         NotifyWidget->OnClick(NotifyWidget);
#endif
   }
}
//---------------------------------------------------------------------------

void TTrackEditFrame::DrawHighlight(void)
{
   if (HighlightRightToLeftFlag || (TrackBar->Tag < 0))
   {
      // Do SelEnd first to avoid SelEnd < SelStart
      TrackBar->SelEnd = TrackBar->Max;
      TrackBar->SelStart = TrackBar->Position;
   }
   else
   {
      TrackBar->SelStart = TrackBar->Min;
      TrackBar->SelEnd = TrackBar->Position;
   }
}
//---------------------------------------------------------------------------

void TTrackEditFrame::SetHighlightRightToLeft(bool newFlag)
{
   HighlightRightToLeftFlag = newFlag;
   DrawHighlight();
}
//---------------------------------------------------------------------------

void TTrackEditFrame::SetInvertEditBoxNumber(bool newFlag)
{
   InvertEditBoxNumberFlag = newFlag;
   CopyTrackBarToEditBox();
}
//---------------------------------------------------------------------------

void TTrackEditFrame::EnableRedGreenHack(bool enableFlag, bool redBelowThreshold)
{
   redGreenEnableFlag = enableFlag;
   redBelowThresholdFlag = redBelowThreshold;
   UpdateRedGreenStuff();
}
//---------------------------------------------------------------------------

void TTrackEditFrame::SetRedGreenThreshold(int threshold)
{
   redGreenThreshold = threshold;
   UpdateRedGreenStuff();
}
//---------------------------------------------------------------------------

void TTrackEditFrame::SetDontNotifyIfMouseButtonIsStillDown(bool flag)
{
   dontNotifyIfMouseButtonIsStillDown = flag;
}
//---------------------------------------------------------------------------

void TTrackEditFrame::UpdateRedGreenStuff()
{
   TColor editNumberColor = clWindowText;

   if (redGreenEnableFlag && TrackBar->Enabled)
   {
      RedGreenTick->Visible = true;

      if (redBelowThresholdFlag)
      {
         if (TrackBar->Position < redGreenThreshold)
         {
            editNumberColor = DARKISH_RED;
            RedGreenTick->Color = BRIGHT_RED;
         }
         else
         {
            editNumberColor = DARK_GREEN;
            RedGreenTick->Color = BRIGHT_GREEN;
         }
      }
      else
      {
         if (TrackBar->Position <= redGreenThreshold)
         {
            editNumberColor = DARK_GREEN;
            RedGreenTick->Color = BRIGHT_GREEN;
         }
         else
         {
            editNumberColor = DARKISH_RED;
            RedGreenTick->Color = BRIGHT_RED;
         }
      }

      // Compute position of red/green tick
      float trackBarRange = float(TrackBarMaxPositionTick->Left
								  - TrackBarMinPositionTick->Left + 1);
      float trackBarOffset = float(TrackBarMinPositionTick->Left);
      float valueRange = float(TrackBar->Max - TrackBar->Min + 1);
      float valueOffset = float(TrackBar->Min);

      RedGreenTick->Left = int((redGreenThreshold - valueOffset)
                           * (trackBarRange / valueRange) + trackBarOffset
                           + 0.5);

      // Compute height of red/green tick (try not to overlap the thumb)
      int thumbCenter = int((TrackBar->Position - valueOffset)
                           * (trackBarRange / valueRange) + trackBarOffset
                           + 0.5);
      int distance = (RedGreenTick->Left + 2) - thumbCenter;
      if (distance < 0) distance = -distance;
      RedGreenTick->Height = (distance > 3)? 8 : 6;   // QQQ double magic
   }
   else
   {
      RedGreenTick->Visible = false;
   }

   Edit->Font->Color = editNumberColor;
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::EditKeyPress(TObject *Sender, char &Key)
{
   const char ESC_CODE = 0x1B;
   const char CTRL_Z_CODE = 0x1A;
   const char DEL_CODE = 0x7F;

   if (Key == '\r')
   {
      Key = '\0';
      EditExit(Sender);
      //EditEnter(Sender);  // Selects all the text
      FocusOnTrackBar(); // No - select the trackbar instead
      return;
   }

   if (Key == ESC_CODE || Key == CTRL_Z_CODE)
   {
      if (cancelValue != (int)INVALID_CANCEL_VALUE)
      {
         TrackBar->Position = cancelValue;
         Edit->Text = cancelValue;  // redundant? QQQ
      }
      Key = '\0';
      EditExit(Sender);   // resets cancelValue
      //EditEnter(Sender);  // Selects all the text
      FocusOnTrackBar(); // No - select the trackbar instead
      return;
   }

   if ((Key >= ' ' && Key < '0') || (Key > '9' && Key < DEL_CODE))
   {
      Beep();
      Key = '\0';
   }
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::TrackBarEnter(TObject *Sender)
{
   TitleLabel->Font->Style = (TFontStyles() << fsBold);

   TTrackBar *senderAsTrackBar = dynamic_cast<TTrackBar *>(Sender);
   if (senderAsTrackBar != NULL)
   {
      TrackEditGotFocus.Notify();
      NotifyTrackBarEntered();
   }
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::TrackBarExit(TObject *Sender)
{
   TitleLabel->Font->Style = TFontStyles();

   TTrackBar *senderAsTrackBar = dynamic_cast<TTrackBar *>(Sender);
   if (senderAsTrackBar != NULL)
   {
      NotifyTrackBarExited();
   }
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::FrameMouseWheelDown(TObject *Sender,
      TShiftState Shift, TPoint &MousePos, bool &Handled)
{
//   if (TrackBar->Position > TrackBar->Min)
//     TrackBar->Position = TrackBar->Position - 1;
//   Handled = true;
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::FrameMouseWheelUp(TObject *Sender,
      TShiftState Shift, TPoint &MousePos, bool &Handled)
{
//   if (TrackBar->Position < TrackBar->Max)
//     TrackBar->Position = TrackBar->Position + 1;
//   Handled = true;
}
//---------------------------------------------------------------------------

void TTrackEditFrame::FocusOnTrackBar(void)
{
   TCustomForm *parentForm = NULL;
   TControl *currentObject = this;

   do
   {
     TControl *parentObject = currentObject->Parent;
     parentForm = dynamic_cast<TCustomForm *>(parentObject);
     currentObject = parentObject;
   }
   while (parentForm == NULL && currentObject != NULL);

   if (parentForm != NULL)
      parentForm->FocusControl(TrackBar);

}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::FrameResize(TObject *Sender)
{
   TrackBarMinPositionTick->Left = TrackBarDisabledPanel->Left + 2; // QQQ magic
   TrackBarMaxPositionTick->Left = TrackBarDisabledPanel->Left
                                 + TrackBarDisabledPanel->Width - 2 // QQQ magic
                                 - TrackBarMaxPositionTick->Width;
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::RedGreenTickClick(TObject *Sender)
{
   TrackBar->Position = redGreenThreshold;
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::DeferredUpdateTimerTimer(TObject *Sender)
{
   // Notify of changes if the mouse left button is not down
   short leftMouseButtonState = ::GetAsyncKeyState(VK_LBUTTON);
   bool leftMouseButtonIsDown = leftMouseButtonState < 0;  // if MSB is set
   if (!leftMouseButtonIsDown)
   {
      DeferredUpdateTimer->Enabled = false;

      //Mouse button was released - fire notifications
      NotifyChanged();
      TrackEditValueChange.Notify();
   }
}
//---------------------------------------------------------------------------

void TTrackEditFrame::TakeFocus(void)
{
   if (!Enabled || !Visible)
   {
      return;
   }

   FocusOnTrackBar();
}
//---------------------------------------------------------------------------

bool TTrackEditFrame::ProcessKeyDown(WORD Key, TShiftState Shift)
{
   TTrackEditFrame *activeTrackEdit = static_cast<TTrackEditFrame *>(SharedMap::retrieve(ACTIVE_TRACK_EDIT_SHARED_MAP_KEY));
   if (activeTrackEdit == NULL)
   {
      // Not one of ours.
      return false;
   }

   if (Key == MTK_RETURN)
   {
      activeTrackEdit->TrackEditEnterKeyPressed.Notify();
      return true;
   }

   TTrackBar *activeTrackBar = activeTrackEdit->TrackBar;

   if (Shift.Contains(ssAlt)
   || (Key != MTK_UP && Key != MTK_DOWN && Key != MTK_LEFT && Key != MTK_RIGHT))
   {
      return false;
   }

   // Arrow + Ctrl changes position by 1.
   // Arrow + Shift changes position by 1/10th of the range, minimum 1.
   // If the trackbar range (Max - Min) is less than 500, a naked arrow
   //        key changes position by 1/20th of the range, minimum 1.
   // Else naked arrow key changes the position by (position / 10),
   //        minimum 1. (Ugggh, per Larry - needs to match Paint radius)
   int positionChange;
   int trackBarRange = activeTrackBar->Max - activeTrackBar->Min + 1;
   if (Shift.Contains(ssCtrl))
   {
      positionChange = 1;
   }
   else if (Shift.Contains(ssShift))
   {
      positionChange = (trackBarRange <= 20) ? 2 : (trackBarRange / 10);
   }
   else if (trackBarRange >= 500)
   {
      // 10 vs 11 so the negative change matches the positive change!
      positionChange = 1 + (activeTrackBar->Position /
                      ((Key == MTK_UP || Key == MTK_RIGHT) ? 10 : 11));
   }
   else
   {
      positionChange = (trackBarRange <= 20)
                       ? 1
                       : ((trackBarRange <= 40)
                         ? 2
                         : (trackBarRange / 20));
   }

   // TEMP KLUDGE
   if ((positionChange == 15) || (positionChange == 30))
   {
      positionChange = positionChange/3;
   }

   int newPosition = activeTrackBar->Position + ((Key == MTK_UP || Key == MTK_RIGHT)
                                           ? positionChange
                                           : -positionChange);
   newPosition = std::min<int>(activeTrackBar->Max, newPosition);
   newPosition = std::max<int>(activeTrackBar->Min, newPosition);

   activeTrackBar->Position = newPosition;

   return true;
}
//---------------------------------------------------------------------------

bool TTrackEditFrame::ProcessKeyUp(WORD Key, TShiftState Shift)
{
   TTrackEditFrame *activeTrackEdit = static_cast<TTrackEditFrame *>(SharedMap::retrieve(ACTIVE_TRACK_EDIT_SHARED_MAP_KEY));
   if (activeTrackEdit == NULL)
   {
      // Not one of ours.
      return false;
   }

   if (Key == MTK_RETURN)
   {
      return true;
   }

   TTrackBar *activeTrackBar = activeTrackEdit->TrackBar;

   if (Shift.Contains(ssAlt)
   || (Key != MTK_UP && Key != MTK_DOWN && Key != MTK_LEFT && Key != MTK_RIGHT))
   {
      return false;
   }

   return true;
}
//---------------------------------------------------------------------------

void TTrackEditFrame::NotifyTrackBarEntered()
{
   SharedMap::add(ACTIVE_TRACK_EDIT_SHARED_MAP_KEY, (void *)this);
}
//---------------------------------------------------------------------------

void TTrackEditFrame::NotifyTrackBarExited()
{
   TTrackEditFrame *activeTrackEdit = static_cast<TTrackEditFrame *>(SharedMap::retrieve(ACTIVE_TRACK_EDIT_SHARED_MAP_KEY));
   if (activeTrackEdit == this)
   {
      SharedMap::remove(ACTIVE_TRACK_EDIT_SHARED_MAP_KEY);
   }
}
//---------------------------------------------------------------------------

void TTrackEditFrame::ClearActiveTrackBar()
{
   SharedMap::remove(ACTIVE_TRACK_EDIT_SHARED_MAP_KEY);
}
//---------------------------------------------------------------------------

void __fastcall TTrackEditFrame::TitleLabelClick(TObject *Sender)
{
   FocusOnTrackBar();
}
//---------------------------------------------------------------------------

