//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MediaLocationUnit.h"

#include "pTimecode.h"
#include "BinManager.h"
#include "Clip3.h"
#include "CommonGuiUtil.h"
#include "MTIDialogs.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//TMediaLocationFrame *MediaLocationFrame;
//---------------------------------------------------------------------------
__fastcall TMediaLocationFrame::TMediaLocationFrame(TComponent* Owner)
    : TFrame(Owner)
{
}
//---------------------------------------------------------------------------

void TMediaLocationFrame::Init(const string &defaultDiskSchemeName,
                               const string &clipSchemeName,
                               const string &protoClipName,
                               const bool defaultAddHandles)
{
    initDiskSchemeComboBox(defaultDiskSchemeName);
    ClipSchemeName = clipSchemeName;
    ProtoClipName = protoClipName;
    AddHandlesCheckBox->Checked = defaultAddHandles;
    //VideoStoreUsage1->DiskScheme = defaultDiskSchemeName.c_str();
    //VideoStoreUsage1->ClipScheme = clipSchemeName.c_str();
    //updateTimeRemainingOnDisk();
}
//---------------------------------------------------------------------------

string TMediaLocationFrame::GetDiskScheme()
{
    return CppStringFromAnsiString(DiskSchemeComboBox->Text);
}
//---------------------------------------------------------------------------

bool TMediaLocationFrame::GetAddHandlesFlag()
{
    return AddHandlesCheckBox->Checked;
}
//---------------------------------------------------------------------------

void TMediaLocationFrame::initDiskSchemeComboBox(const string &defaultName)
{
    // Build entries for Media Location Combo Box
    int retVal;
    DiskSchemeComboBox->Items->Clear();

    CDiskSchemeList diskSchemeList;
    retVal = diskSchemeList.ReadDiskSchemeFile();
    if (retVal != 0) {
        _MTIErrorDialog(Handle, "Could not open Disk Scheme File");
        return;
    }

    // Add a combo box item to the Media Location combo box for each
    // active disk scheme
    StringList activeDiskSchemeNames;
    activeDiskSchemeNames = diskSchemeList.GetActiveDiskSchemeNames();
    for (unsigned int i = 0; i < activeDiskSchemeNames.size(); ++i) {
        DiskSchemeComboBox->Items->Append(activeDiskSchemeNames[i].c_str());
    }

    DiskSchemeComboBox->ItemIndex = 0;
    if (defaultName != "") {
        for (StringList::iterator iter = activeDiskSchemeNames.begin();
             iter < activeDiskSchemeNames.end();
             ++iter)
        {
            if (*iter == defaultName) {
                DiskSchemeComboBox->ItemIndex =
                            iter - activeDiskSchemeNames.begin();
                break;
            }
        }
    }
}
//---------------------------------------------------------------------------

void TMediaLocationFrame::updateTimeRemainingOnDisk()
{
#if 1
    //VideoStoreUsage1->RefreshTimeRemaining();
#else // OLD WAY
    int retVal;
    PTimecode totalTime, largestTime;

    CClipInitInfo clipInitInfo;
    CVideoInitInfo *mainVideoInitInfo = NULL;
    int iHandle = 0;
    CTimecode timecodePrototype(0);
    CMediaInterface mediaInterface;
    CDiskSchemeList diskSchemeList;
    string diskSchemeName;
    CDiskScheme diskScheme;
    int maxLabelWidth = TotalSpaceValueLabel->Left - TotalSpaceLabel->Left - 8;

    /*
     * If label is too long to fit in space allotted, try tossing
     * information until it fits...
     */
    TotalSpaceLabel->Caption = AnsiString("Total space available on ")
                              + DiskSchemeComboBox->Text
                              + AnsiString(":");
    if (TotalSpaceLabel->Width > maxLabelWidth) {
        TotalSpaceLabel->Caption = "Total space available:";
    }

    LargestClipLabel->Caption = AnsiString("Largest possible ")
                              + ClipSchemeName.c_str()
                              + (ClipSchemeName.empty()? "" : " ")
                              + AnsiString("clip:");
    if (LargestClipLabel->Width > maxLabelWidth) {
        LargestClipLabel->Caption = AnsiString("Largest ")
                                  + ClipSchemeName.c_str()
                                  + (ClipSchemeName.empty()? "" : " ")
                                  + AnsiString(" clip:");
    }
    if (LargestClipLabel->Width > maxLabelWidth) {
        LargestClipLabel->Caption = "Largest possible clip:";
    }

    if (ProtoClipName != "")
    {
       CBinManager binMgr;
       auto protoClip = binMgr.openClip(ProtoClipName, &retVal);

       if (retVal == 0)
       {
          retVal = clipInitInfo.initFromClip(protoClip);
          binMgr.closeClip(protoClip);
       }
    }
    else
    {
       TotalSpaceValueLabel->Caption = "Unknown";
       LargestClipValueLabel->Caption = "Unknown";
       return;
    }
    if (retVal)
    {
       TotalSpaceValueLabel->Caption = "ERROR";
       LargestClipValueLabel->Caption = AnsiString(retVal);
       return;
    }

    mainVideoInitInfo = clipInitInfo.getMainVideoInitInfo();
    if (mainVideoInitInfo == 0) {
        // ERROR: not a video clip??!?
        TotalSpaceValueLabel->Caption = "ERROR";
        LargestClipValueLabel->Caption = "NO VIDEO";
        return;
    }
    iHandle = 2*mainVideoInitInfo->getHandleCount();
    timecodePrototype = clipInitInfo.getTimecodePrototype();

    totalTime = timecodePrototype;
    totalTime.setDropFrame(false);
    largestTime = totalTime;

    diskSchemeName = CppStringFromAnsiString(DiskSchemeComboBox->Text);
    retVal = diskSchemeList.ReadDiskSchemeFile();
    if (retVal == 0) {
        diskScheme = diskSchemeList.Find(diskSchemeName);
        if (diskScheme.name.empty()) {
            retVal = -1;
        }
    }
    if (retVal != 0) {
        // ERROR: Could not set media locations from Disk Scheme
        TotalSpaceValueLabel->Caption = "ERROR";
        LargestClipValueLabel->Caption = AnsiString(retVal);
        return;
    }

    retVal = mainVideoInitInfo->queryImageTimeRemaining(
                                            MEDIA_TYPE_RAW,
                                            diskScheme.mainVideoMediaLocation,
                                            totalTime, largestTime);
    if (retVal == 0) {
        if (largestTime.getAbsoluteTime() > iHandle)
            largestTime = largestTime + (-iHandle);
        else
            largestTime.setTime(0, 0, 0, 0);

        PTimecode PTotalTC(totalTime);
        PTimecode PLargestTC(largestTime);

        TotalSpaceValueLabel->Caption = string(PTotalTC).c_str();
        LargestClipValueLabel->Caption = string(PLargestTC).c_str();
    }
    else {
        TotalSpaceValueLabel->Caption = "ERROR";
        LargestClipValueLabel->Caption = AnsiString(retVal);
    }

#endif // 0 qqq
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TMediaLocationFrame::DiskSchemeComboBoxSelect(
      TObject *Sender)
{
    updateTimeRemainingOnDisk();
}
//---------------------------------------------------------------------------

void __fastcall TMediaLocationFrame::DiskSpaceTimerTimer(TObject *Sender)
{
    //updateTimeRemainingOnDisk();
}
//---------------------------------------------------------------------------
