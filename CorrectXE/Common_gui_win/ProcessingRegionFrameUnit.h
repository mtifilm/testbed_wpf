//---------------------------------------------------------------------------

#ifndef ProcessingRegionFrameUnitH
#define ProcessingRegionFrameUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TTFProcessingRegion : public TFrame
{
__published:	// IDE-managed Components
   TGroupBox *MattingGroupBox;
   TSpeedButton *TopUpButton;
   TSpeedButton *TopDownButton;
   TSpeedButton *BottomUpButton;
   TSpeedButton *BottomDownButton;
   TSpeedButton *LeftLeftButton;
   TSpeedButton *LeftRightButton;
   TSpeedButton *RightLeftButton;
   TSpeedButton *RightRightButton;
   TSpeedButton *ColorPickButton;
   TBitBtn *AllMatButton;
   TCheckBox *ShowMattingCBox;
   TCheckBox *ProcessingCBox;
   TCheckBox *LockMattingCBox;
   TPanel *ButtonPanel;
   void __fastcall BoundryButtonClick(TObject *Sender);
   void __fastcall AllMatButtonClick(TObject *Sender);
   void __fastcall LockMattingCBoxClick(TObject *Sender);


private:	// User declarations
	TNotifyEvent _onBoxChange;

   RECT _boundingBox;
   int _maxWidth;
   int _maxHeight;

public:		// User declarations
   __fastcall TTFProcessingRegion(TComponent* Owner);
   __property TNotifyEvent OnBoxChange = {read=_onBoxChange, write=_onBoxChange};

   RECT &GetBoundingBox();
   void SetBoundingBox(const RECT &boundingBox);
   void SetMinMax(int width, int height);
};
//---------------------------------------------------------------------------
extern PACKAGE TTFProcessingRegion *TFProcessingRegion;
//---------------------------------------------------------------------------
#endif
