object MoveInMarkDialog: TMoveInMarkDialog
  Left = 245
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Warning'
  ClientHeight = 180
  ClientWidth = 368
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  DesignSize = (
    368
    180)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 352
    Height = 121
    Anchors = [akLeft, akTop, akRight, akBottom]
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 48
    Top = 72
    Width = 284
    Height = 16
    Caption = 'What should I do with the tracking points?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 80
    Top = 48
    Width = 207
    Height = 16
    Caption = 'Mark IN is about to be moved!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object OKBtn: TButton
    Left = 23
    Top = 146
    Width = 93
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Move and Copy'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 247
    Top = 146
    Width = 93
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object Button1: TButton
    Left = 135
    Top = 146
    Width = 93
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Move and Clear'
    ModalResult = 7
    TabOrder = 2
  end
end
