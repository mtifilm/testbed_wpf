// mlm: Windows-generic stubs implementing MTIDialogs.h interface

#include "IniFile.h"
#include "MTIDialogs.h"
#include "MTIstringstream.h"

static HANDLE _ApplicationHandle = NULL;

HWND GetApplicationHandle(void)
//  This just returns a handle for the dialog boxes, it should be set to
// the Application->Handle or NULL.
//
//**************************************************************************
{
   return (HWND)_ApplicationHandle;
}

//*******************Error Dialog Boxes**************************************

int _MTISevereErrorRetryDialogHL(const string &msg, const string &file, int line)
/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  return _MTISevereErrorRetryDialogH(GetApplicationHandle(), msg, file, line);
}

int _MTISevereErrorRetryDialogH(HWND Handle, const string &msg, const string &file, int line)
/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  MTIostringstream os;
  os << msg << "\n" << "Fatal Error in " << file << ", Line: " << line;
  TRACE_MSG(errout << os.str() << " " << GetApplicationName());

  int Result = 0;

  return(Result);
}

int _MTISevereErrorDialogHL(const string &msg, const string &file, int line)
/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  return _MTISevereErrorDialogH(GetApplicationHandle(), msg, file, line);
}


int _MTISevereErrorDialogH(HWND Handle, const string &msg, const string &file, int line)
/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  MTIostringstream os;
  os << msg << "\n" << "Fatal Error in " << file << ", Line: " << line;
  TRACE_MSG(errout << os.str() << " " << GetApplicationName());

  int Result = 0;

  return Result;
}


int _MTIErrorDialog(const string &msg)
//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 1 error
//
//****************************************************************************
{
  return _MTIErrorDialog(GetApplicationHandle(), msg);
}


int _MTIErrorDialog(HWND Handle, const string &msg)
//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 1 error
//
//****************************************************************************
{

  TRACE_1(errout << msg);

  return MTI_DLG_OK;
}

int _MTIInformationDialog(const string &msg)
//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 1 error
//
//****************************************************************************
{
  return _MTIInformationDialog(GetApplicationHandle(), msg);
}

int _MTIInformationDialog(HWND Handle, const string &msg)
//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 1 error
//
//****************************************************************************
{
  TRACE_3(errout << msg);

  return(MTI_DLG_OK);
}

int _MTIInformationDialogModeless(const string &msg)
/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  return _MTIInformationDialogModeless(GetApplicationHandle(), msg);
}

int _MTIInformationDialogModeless(HWND Handle, const string &msg)
//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 3 error
//
//****************************************************************************
{
  return(MTI_DLG_OK);
}

int _MTIWarningDialog(const string &msg)
//  This displays an warning and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the OK button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
  return _MTIWarningDialog(GetApplicationHandle(), msg);
}

int _MTIWarningDialog(HWND Handle, const string &msg)
//  This displays an warning and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the OK button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{

  TRACE_2(errout << msg);

  int Result = MTI_DLG_OK;

  return(Result);

}

int _MTIConfirmationDialog(const string &msg)
//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_CANCEL on the NO button
//  Trace 2 error reported
//
//****************************************************************************
{
   return _MTIConfirmationDialog(GetApplicationHandle(), msg);
}

int _MTIConfirmationDialog(HWND Handle, const string &msg)
//  This asks the user for conformation and returns the button a user pressed.
//  Handle is the display handle to use
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_CANCEL on the NO button
//  Trace 2 error reported
//
//****************************************************************************
{
  int Result = MTI_DLG_OK;

  return(Result);
}

int _MTICancelConfirmationDialog(const string &msg)
//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_NO on the NO button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
   return _MTICancelConfirmationDialog(GetApplicationHandle(), msg);
}

int _MTICancelConfirmationDialog(HWND Handle, const string &msg)
//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_NO on the NO button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
	int Result = MTI_DLG_OK;

	return(Result);
}

int _MTIYesAllConfirmationDialog(HWND Handle, const string &msg)
//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_NO on the NO button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
	int Result = MTI_DLG_OK;

	return Result ;
}

int _MTIYesAllConfirmationDialog(const string &msg)
//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_NO on the NO button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
   return _MTIYesAllConfirmationDialog(GetApplicationHandle(), msg);
}

string _GetApplicationName(void)
//  This returns the Application name using ExeName.
//
//****************************************************************************
{
  return "";  // This will generate an error
}
