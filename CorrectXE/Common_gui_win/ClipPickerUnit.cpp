//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClipPickerUnit.h"
#include "MTIWinInterface.h"
#include "BinManager.h"
#include "CommonGuiUtil.h"
#include "ShowModalDialog.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BinTreeViewUnit"
#pragma link "ClipListViewUnit"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

TClipPickerForm *ClipPickerForm;
//---------------------------------------------------------------------------

/* static */
string TClipPickerForm::PickAClip(EFlags flags,  /* not used ?? QQQ */
                                  const string *iniFilePath,
                                  const string *clipFilter,
                                  const string *defClipPath)
{
    string retVal;
    CBinManager binMgr;
    string desiredFormat = ((clipFilter == 0)? string("*") : *clipFilter);
    string localIniFile = ((iniFilePath == 0)? string("") : *iniFilePath);

    if (ClipPickerForm == 0) {
        ClipPickerForm = new TClipPickerForm(Application);
        if (ClipPickerForm == 0) {
            return "";
        }
    }

    ClipPickerForm->Init(localIniFile, desiredFormat);

    if ((defClipPath != 0) && (*defClipPath != ""))
    {
        int iRet = 0;
        string defBinPath;
        string defClipName;

        /*
         * Set the suggested clip name
         */
        iRet = binMgr.splitClipFileName(*defClipPath, defBinPath, defClipName);
        if (iRet == 0) {
            ClipPickerForm->SetClipName(defClipName);
            ClipPickerForm->SetBinName(defBinPath);
        }
    }

    ShowModalDialog(ClipPickerForm);

    if (ClipPickerForm->ModalResult == mrOk
        && ClipPickerForm->GetClipName() != "")
    {
        ClipPickerForm->OK();

        retVal = AddDirSeparator(ClipPickerForm->GetBinName())
               + ClipPickerForm->GetClipName();
    }
    
    return retVal;
}
//---------------------------------------------------------------------------

__fastcall TClipPickerForm::TClipPickerForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void TClipPickerForm::Init(const string &iniFileName,
                           const string &format)
{
    BinTreeViewFrame->InitFromIniFile(iniFileName);
    ClipListViewFrame->InitFromIniFile(iniFileName);

    Format = format;
    if (format == "*") {
        FilterFormatEdit->Text = "Any format";
    } else {
        FilterFormatEdit->Text = format.c_str();
    }

    ClipListViewFrame->SetClipFormatFilter(format);
}
//---------------------------------------------------------------------------

int TClipPickerForm::OK()
{
    int retVal = 0;

    retVal = BinTreeViewFrame->OK();
    if (retVal == 0) {
        retVal = ClipListViewFrame->OK();
    }

    return retVal;
}
//---------------------------------------------------------------------------

void TClipPickerForm::SetBinName(const string &name)
{
    BinName = name;
}
//---------------------------------------------------------------------------

void TClipPickerForm::SetClipName(const string &name)
{
    ClipName = name;
}
//---------------------------------------------------------------------------

string TClipPickerForm::GetBinName()
{
    return BinName;
}
//---------------------------------------------------------------------------

string TClipPickerForm::GetClipName()
{
    return ClipName;
}
//---------------------------------------------------------------------------


void __fastcall TClipPickerForm::BinTreeViewFrameSelectionChangedNotifierClick(
      TObject *Sender)
{
    BinName = BinTreeViewFrame->GetSelectedBin();
    ClipListViewFrame->RebuildClipList(BinName);
}
//---------------------------------------------------------------------------

void __fastcall TClipPickerForm::ClipListViewFrameSelectedClipChangedNotifierClick(
      TObject *Sender)
{
    ClipName = ClipListViewFrame->GetSelectedClip();
    SelectedClipEdit->Text = ClipName.c_str();
}
//---------------------------------------------------------------------------

void __fastcall TClipPickerForm::ClipListViewFrameDoubleClickNotifierClick(
      TObject *Sender)
{
    ModalResult = mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TClipPickerForm::FormShow(TObject *Sender)
{
    {   CBusyCursor busy(true);
    
        BinTreeViewFrame->RefreshBinTree();
        if (BinName != "") {
            BinTreeViewFrame->SetSelectedBin(BinName);
            BinTreeViewFrame->RefreshBinTree();
        }
    }

    // don't enterRebuildClipList with busy cursor -- it spawns a
    // thread and the cursor never gets turned off...
    BinName = BinTreeViewFrame->GetSelectedBin();
    ClipListViewFrame->RebuildClipList(BinName);
    SelectedClipEdit->Text = ClipName.c_str();
}
//---------------------------------------------------------------------------

void __fastcall TClipPickerForm::SelectedClipEditKeyPress(TObject *Sender,
      char &Key)
{
    if (Key == Char(VK_RETURN)) {
        if (SelectedClipEdit->Text != "") {
            ClipName = CppStringFromAnsiString(SelectedClipEdit->Text);
            ModalResult = mrOk;
        } else {
            ModalResult = mrCancel;
        }
    }
}
//---------------------------------------------------------------------------


void __fastcall TClipPickerForm::SelectedClipEditExit(TObject *Sender)
{
    ClipName = CppStringFromAnsiString(SelectedClipEdit->Text);
}
//---------------------------------------------------------------------------

