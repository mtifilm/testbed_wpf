//---------------------------------------------------------------------------

#include <vcl.h>
#include "MTIio.h"
#pragma hdrstop

#include "CompactTrackEditFrameUnit.h"
#include "MTIKeyDef.h"
#include "SharedMap.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorPanel"
#pragma resource "*.dfm"
TCompactTrackEditFrame *CompactTrackEditFrame;
//---------------------------------------------------------------------------

#define INVALID_CANCEL_VALUE 0x80000000
#define ACTIVE_COMPACT_TRACK_EDIT_KEY "ActiveCompactTrackEdit"
//---------------------------------------------------------------------------

__fastcall TCompactTrackEditFrame::TCompactTrackEditFrame(TComponent* Owner)
        : TFrame(Owner)
        , HighlightRightToLeftFlag(false)
        , InhibitNotification(false)
        , cancelValue(INVALID_CANCEL_VALUE)
        , inhibitUpdates(false)
        , dontNotifyIfMouseButtonIsStillDown(false)
{
   DrawHighlight();
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::Enable(bool newEnabled)
{
   if (!newEnabled)
   {
      TitleLabel->Enabled            = false;
      TrackBar->Enabled              = false;
      TrackBar->SliderVisible        = false;
      TrackBar->SelStart             = TrackBar->Position;
      TrackBar->SelEnd               = TrackBar->Position;
      TrackBarDisabledPanel->Width   = TrackBar->Width - 12; // MAGIC NUMBER QQQ
      TrackBarDisabledPanel->Visible = true;
      Edit->Enabled                  = false;
      EditDisabledPanel->Visible     = false;
   }
   else
   {
      TitleLabel->Enabled     = true;
      TrackBar->Enabled       = true;
      TrackBar->SliderVisible = true;
      DrawHighlight();
      TrackBarDisabledPanel->Visible = false;
      Edit->Enabled                  = true;
      EditDisabledPanel->Visible     = false;
   }
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::SetMinAndMax(int newMin, int newMax)
{
   // Want to preserve relative Position
   int oldPosition = GetValue();
   int oldMin = TrackBar->Min;
   int oldMax = TrackBar->Max;

   // Stupidity - order of setting can matter!!
   if (newMin > TrackBar->Max)
   {
      TrackBar->Max = newMax;
      TrackBar->Min = newMin;
   }
   else
   {
      TrackBar->Min = newMin;
      TrackBar->Max = newMax;
   }

   int newPosition = (int) ((newMin + ((oldPosition - oldMin) * ((newMax - newMin) / (float)(oldMax - oldMin)))) + 0.5F);
   SetValue(newPosition);
   DrawHighlight();
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::GetMinAndMax(int &currentMin, int &currentMax)
{
   currentMin = TrackBar->Min;
   currentMax = TrackBar->Max;
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::SetLabel(const string &newTitleLabel)
{
   TitleLabel->Caption = newTitleLabel.c_str();
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::SetValue(int newPos)
{
   if (newPos > TrackBar->Max)
      newPos = TrackBar->Max;
   if (newPos < TrackBar->Min)
      newPos = TrackBar->Min;
   InhibitNotification = true;
   TrackBar->Position = newPos;
   CopyTrackBarToEditBox();
   InhibitNotification = false;
}

int TCompactTrackEditFrame::GetValue()
{
   return TrackBar->Position;
}
//---------------------------------------------------------------------------

bool TCompactTrackEditFrame::CopyTrackBarToEditBox()
{
   // We return TRUE if the edit box number actually changed.
   bool retVal = false;

   if (!inhibitUpdates)
   {
      AnsiString newTextForEditBox;
      if (InvertEditBoxNumberFlag)
      {
         // WTF!! Larry wants some of the trackbars to read out backwards!!!!
         newTextForEditBox =  (TrackBar->Max - TrackBar->Position) + TrackBar->Min;
      }
      else
      {
         newTextForEditBox = TrackBar->Position;
      }

      if (Edit->Text != newTextForEditBox)
      {
         inhibitUpdates = true;
         Edit->Text = newTextForEditBox;
         inhibitUpdates = false;

         if (EditDisabledLabel->Tag >= 0)
            EditDisabledLabel->Caption = TrackBar->Position;
         else
            EditDisabledLabel->Caption = "";

         retVal = true;
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

bool TCompactTrackEditFrame::CopyEditBoxToTrackBar()
{
   // We return TRUE if the track bar position actually changed.
   bool retVal = false;

   if (!inhibitUpdates)
   {
      int newTrackBarPosition;
      int editBoxNumber = Edit->Text.ToIntDef(0);
      if (InvertEditBoxNumberFlag)
      {
         // WTF!! Larry wants some of the trackbars to read out backwards!!!!
         newTrackBarPosition = (TrackBar->Max - editBoxNumber) + TrackBar->Min;
      }
      else
      {
         newTrackBarPosition = editBoxNumber;
      }

      if (TrackBar->Position != newTrackBarPosition)
      {
         inhibitUpdates = true;
         TrackBar->Position = newTrackBarPosition;
         inhibitUpdates = false;

         retVal = true;
      }
   }

   return retVal;
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::TrackBarChange(TObject *Sender)
{
   DrawHighlight();

   // The copy function returns true if the edit box actually changed.
   if (CopyTrackBarToEditBox())
   {
      short leftMouseButtonState = ::GetAsyncKeyState(VK_LBUTTON);
      bool leftMouseButtonIsDown = leftMouseButtonState < 0;  // if MSB is set
      DeferredUpdateTimer->Enabled = false;

      if (dontNotifyIfMouseButtonIsStillDown && leftMouseButtonIsDown)
      {
         // Mouse button is still down... restart the timer
         DeferredUpdateTimer->Enabled = true;
      }
      else
      {
         NotifyChanged();
      }
   }
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::EditEnter(TObject *Sender)
{
   cancelValue = TrackBar->Position;
   Edit->SelStart = 0;
   Edit->SelLength = Edit->Text.Length();
   TitleLabel->Font->Style = (TFontStyles() << fsBold);
   TrackEditGotFocus.Notify();
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::EditExit(TObject *Sender)
{
   int newEditBoxNumber = atoi(StringToStdString(Edit->Text).c_str());
   int oldPosition = TrackBar->Position;

   if (newEditBoxNumber > TrackBar->Max)
      newEditBoxNumber = TrackBar->Max;
   if (newEditBoxNumber < TrackBar->Min)
      newEditBoxNumber = TrackBar->Min;

   Edit->Text = newEditBoxNumber;

   // Copy function returns true if the track bar position actually changed.
   if (CopyEditBoxToTrackBar())
   {
      NotifyChanged();
   }

   Edit->SelStart = 0;
   Edit->SelLength = Edit->Text.Length();
   cancelValue = INVALID_CANCEL_VALUE;
   TitleLabel->Font->Style = TFontStyles();
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::NotifyChanged(void)
{
   // Legacy way
   if (!InhibitNotification)
   {
      NotifyWidget->Checked = !NotifyWidget->Checked;
   }

   // New way - should it be inside the "if (!InhibitNotification)"?
   TrackEditValueChange.Notify();
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::DrawHighlight(void)
{
   if (HighlightRightToLeftFlag || (TrackBar->Tag < 0))
   {
      // Do SelEnd first to avoid SelEnd < SelStart
      TrackBar->SelEnd = TrackBar->Max;
      TrackBar->SelStart = TrackBar->Position;
   }
   else
   {
      TrackBar->SelStart = TrackBar->Min;
      TrackBar->SelEnd = TrackBar->Position;
   }
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::SetHighlightRightToLeft(bool newFlag)
{
   HighlightRightToLeftFlag = newFlag;
   DrawHighlight();
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::SetInvertEditBoxNumber(bool newFlag)
{
   InvertEditBoxNumberFlag = newFlag;
   CopyTrackBarToEditBox();
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::SetDontNotifyIfMouseButtonIsStillDown(bool flag)
{
   dontNotifyIfMouseButtonIsStillDown = flag;
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::EditKeyPress(TObject *Sender, char &Key)
{
   const char ESC_CODE = 0x1B;
   const char CTRL_Z_CODE = 0x1A;
   const char DEL_CODE = 0x7F;

   if (Key == '\r')
   {
      Key = '\0';
      EditExit(Sender);
      //EditEnter(Sender);  // Selects all the text
      FocusOnTrackBar(); // No - select the trackbar instead
      return;
   }

   if (Key == ESC_CODE || Key == CTRL_Z_CODE)
   {
      if (cancelValue != (int)INVALID_CANCEL_VALUE)
      {
         TrackBar->Position = cancelValue;
         Edit->Text = cancelValue;  // redundant? QQQ
      }
      Key = '\0';
      EditExit(Sender);   // resets cancelValue
      //EditEnter(Sender);  // Selects all the text
      FocusOnTrackBar(); // No - select the trackbar instead
      return;
   }

   if ((Key >= ' ' && Key < '0') || (Key > '9' && Key < DEL_CODE))
   {
      Beep();
      Key = '\0';
   }
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::TrackBarEnter(TObject *Sender)
{
   TitleLabel->Font->Style = (TFontStyles() << fsBold);
   TrackEditGotFocus.Notify();
   NotifyTrackBarEntered();
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::TrackBarExit(TObject *Sender)
{
   TitleLabel->Font->Style = TFontStyles();
   NotifyTrackBarExited();
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::FrameMouseWheelDown(TObject *Sender,
      TShiftState Shift, TPoint &MousePos, bool &Handled)
{
//   if (TrackBar->Position > TrackBar->Min)
//     TrackBar->Position = TrackBar->Position - 1;
//   Handled = true;
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::FrameMouseWheelUp(TObject *Sender,
      TShiftState Shift, TPoint &MousePos, bool &Handled)
{
//   if (TrackBar->Position < TrackBar->Max)
//     TrackBar->Position = TrackBar->Position + 1;
//   Handled = true;
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::FocusOnTrackBar(void)
{
   TCustomForm *parentForm = NULL;
   TControl *currentObject = this;

   do
   {
     TControl *parentObject = currentObject->Parent;
     parentForm = dynamic_cast<TCustomForm *>(parentObject);
     currentObject = parentObject;
   }

   while (parentForm == NULL && currentObject != NULL);

   if (parentForm != NULL)
      parentForm->FocusControl(TrackBar);

}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::TakeFocus(void)
{
   if (!Enabled || !Visible)
   {
      return;
   }

   FocusOnTrackBar();
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::DeferredUpdateTimerTimer(TObject *Sender)
{
   // Notify of changes if the mouse left button is not down
   short leftMouseButtonState = ::GetAsyncKeyState(VK_LBUTTON);
   bool leftMouseButtonIsDown = leftMouseButtonState < 0;  // if MSB is set
   if (!leftMouseButtonIsDown)
   {
      DeferredUpdateTimer->Enabled = false;

      //Mouse button was released - fire notifications
      NotifyChanged();
   }
}
//---------------------------------------------------------------------------
void __fastcall TCompactTrackEditFrame::FrameMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
   TrackEditGotFocus.Notify();
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::FrameEnter(TObject *Sender)
{
   TrackEditGotFocus.Notify();
}
//---------------------------------------------------------------------------

void __fastcall TCompactTrackEditFrame::TitleLabelClick(TObject *Sender)
{
   FocusOnTrackBar();
}
//---------------------------------------------------------------------------

bool TCompactTrackEditFrame::ProcessKeyDown(WORD Key, TShiftState Shift)
{
   TCompactTrackEditFrame *activeFrame =
      static_cast<TCompactTrackEditFrame *>(SharedMap::retrieve(ACTIVE_COMPACT_TRACK_EDIT_KEY));

   if (activeFrame == NULL)
   {
      // Not one of ours.
      return false;
   }

   if (Key == MTK_RETURN)
   {
      activeFrame->TrackEditEnterKeyPressed.Notify();
      return true;
   }

   if (Shift.Contains(ssAlt)
   || (Key != MTK_UP && Key != MTK_DOWN && Key != MTK_LEFT && Key != MTK_RIGHT))
   {
      return false;
   }

   TTrackBar *activeTrackBar = activeFrame->TrackBar;

   // Arrow + Ctrl changes position by 1.
   // Arrow + Shift changes position by 1/10th of the range, minimum 1.
   // If the trackbar range (Max - Min) is less than 500, a naked arrow
   //        key changes position by 1/20th of the range, minimum 1.
   // Else naked arrow key changes the position by (position / 10),
   //        minimum 1. (Ugggh, per Larry - needs to match Paint radius)
   int positionChange;
   int trackBarRange = activeTrackBar->Max - activeTrackBar->Min + 1;
   if (Shift.Contains(ssCtrl))
   {
      positionChange = 1;
   }
   else if (Shift.Contains(ssShift))
   {
      positionChange = (trackBarRange <= 20) ? 2 : (trackBarRange / 10);
   }
   else if (trackBarRange >= 500)
   {
      // 10 vs 11 so the negative change matches the positive change!
      positionChange = 1 + (activeTrackBar->Position /
                      ((Key == MTK_UP || Key == MTK_RIGHT) ? 10 : 11));
   }
   else
   {
      positionChange = (trackBarRange <= 20)
                       ? 1
                       : ((trackBarRange <= 40)
                         ? 2
                         : (trackBarRange / 20));
   }

   int newPosition = activeTrackBar->Position + ((Key == MTK_UP || Key == MTK_RIGHT)
                                           ? positionChange
                                           : -positionChange);
   newPosition = std::min<int>(activeTrackBar->Max, newPosition);
   newPosition = std::max<int>(activeTrackBar->Min, newPosition);

   activeTrackBar->Position = newPosition;

   return true;
}
//---------------------------------------------------------------------------

bool TCompactTrackEditFrame::ProcessKeyUp(WORD Key, TShiftState Shift)
{
   TCompactTrackEditFrame *activeTrackEdit = static_cast<TCompactTrackEditFrame *>(SharedMap::retrieve(ACTIVE_COMPACT_TRACK_EDIT_KEY));
   if (activeTrackEdit == NULL)
   {
      // Not one of ours.
      return false;
   }

   if (Key == MTK_RETURN)
   {
      return true;
   }

   TTrackBar *activeTrackBar = activeTrackEdit->TrackBar;

   if (Shift.Contains(ssAlt)
   || (Key != MTK_UP && Key != MTK_DOWN && Key != MTK_LEFT && Key != MTK_RIGHT))
   {
      return false;
   }

   return true;
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::NotifyTrackBarEntered()
{
   SharedMap::add(ACTIVE_COMPACT_TRACK_EDIT_KEY, this);
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::NotifyTrackBarExited()
{
   TCompactTrackEditFrame *activeFrame =
      static_cast<TCompactTrackEditFrame *>(SharedMap::retrieve(ACTIVE_COMPACT_TRACK_EDIT_KEY));
   if (activeFrame == this)
   {
      SharedMap::remove(ACTIVE_COMPACT_TRACK_EDIT_KEY);
   }
}
//---------------------------------------------------------------------------

void TCompactTrackEditFrame::ClearActiveTrackBar()
{
   SharedMap::remove(ACTIVE_COMPACT_TRACK_EDIT_KEY);
}
//---------------------------------------------------------------------------



