//---------------------------------------------------------------------------

#ifndef RgbFileOptionsDialogH
#define RgbFileOptionsDialogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "VDoubleEdit.h"
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TRgbFileOptionsDialog : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *ColorSpaceGroupBox;
    TRadioButton *Log10RadioButton;
    TRadioButton *Linear10RadioButton;
    TGroupBox *PixelValueRangeGroupBox;
    VDoubleEdit *PixelValueRangeMinVDoubleEdit;
    VDoubleEdit *PixelValueRangeMaxVDoubleEdit;
    TUpDown *RangeMaxUpDown;
    TUpDown *RangeMinUpDown;
    TLabel *Label1;
    TLabel *Label2;
    TRadioButton *Linear8RadioButton;
    TRadioButton *RangeFullRadioButton;
    TRadioButton *RangeRestrictedRadioButton;
    TRadioButton *RangeCustomRadioButton;
    TGroupBox *Gamma;
    TRadioButton *Gamma10RadioButton;
    TRadioButton *Gamma17RadioButton;
    TRadioButton *GammaCustomRadioButton;
    VDoubleEdit *GammaVDoubleEdit;
    TUpDown *GammaCustomUpDown;
    TButton *CancelButton;
    TButton *OkButton;
    TButton *SetDefaultsButton;
    TRadioButton *Linear16RadioButton;
    TLabel *Label3;
    TGroupBox *ImportColorSpaceGroupBox;
    TRadioButton *ImportLogRadioButton;
    TRadioButton *ImportLinearRadioButton;
    TRadioButton *ImportSmpte240RadioButton;
    TRadioButton *ImportCcir709RadioButton;
    TLabel *BitsPerCOmponentLabel;
    TLabel *BitsPerComponentValueLabel;
    TRadioButton *ImportCcir601BgRadioButton;
    TRadioButton *ImportCcir601MRadioButton;
    TRadioButton *Gamma22222RadioButton;
    TRadioButton *Linear12RadioButton;
    TRadioButton *Log12RadioButton;
        TPanel *BackgroundPanel;
        TPanel *Panel1;
    void __fastcall SetDefaultsButtonClick(TObject *Sender);
    void __fastcall RangeMinUpDownClick(TObject *Sender,
          TUDBtnType Button);
    void __fastcall PixelValueRangeMaxVDoubleEditDoubleChange(
          TObject *Sender);
    void __fastcall GammaVDoubleEditDoubleChange(TObject *Sender);
    void __fastcall GammaCustomUpDownClick(TObject *Sender,
          TUDBtnType Button);
    void __fastcall RangeMaxUpDownClick(TObject *Sender,
          TUDBtnType Button);
    void __fastcall PixelValueRangeMinVDoubleEditDoubleChange(
          TObject *Sender);
    void __fastcall Linear10RadioButtonClick(TObject *Sender);
    void __fastcall Log10RadioButtonClick(TObject *Sender);
    void __fastcall Linear8RadioButtonClick(TObject *Sender);
    void __fastcall RangeFullRadioButtonClick(TObject *Sender);
    void __fastcall RangeRestrictedRadioButtonClick(TObject *Sender);
    void __fastcall RangeCustomRadioButtonClick(TObject *Sender);
    void __fastcall Gamma10RadioButtonClick(TObject *Sender);
    void __fastcall Gamma17RadioButtonClick(TObject *Sender);
    void __fastcall GammaCustomRadioButtonClick(TObject *Sender);
    void __fastcall Linear16RadioButtonClick(TObject *Sender);
    void __fastcall Gamma22222RadioButtonClick(TObject *Sender);
    void __fastcall Linear12RadioButtonClick(TObject *Sender);
    void __fastcall Log12RadioButtonClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
    unsigned int AllowedColorSpacesMask;
    int DefaultColorSpaceIndex;
    int DefaultRangeIndex;
    int DefaultRangeMin;
    int DefaultRangeMax;
    int DefaultGammaIndex;
    float DefaultCustomGamma;

    int CustomMin8;
    int CustomMin10;
    int CustomMin12;
    int CustomMin16;
    int CustomMax8;
    int CustomMax10;
    int CustomMax12;
    int CustomMax16;

    void ConformColorSpace();
    void ConformRange();
    void ConformGamma();

    int InhibitCallbacks;
public:		// User declarations
    void SetAllowedColorSpaces(int mask);
    void SetDefaultValues(
                int colorSpaceIndex,
                int rangeIndex,
                int rangeMin,
                int rangeMax,
                int gammaIndex,
                float customGamma);
    void SetDefaults();
    void GetValues(int &colorSpaceIndex, int &rangeMin, int &rangeMax,
                    float &gamma, bool &useCCIR709TransferFunction);

    __fastcall TRgbFileOptionsDialog(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TRgbFileOptionsDialog *RgbFileOptionsDialog;
//---------------------------------------------------------------------------

#define RGBOPT_CS_DEFAULT       1
#define RGBOPT_CS_NO_OVERRIDE   0
#define RGBOPT_CS_LINEAR_10     1
#define RGBOPT_CS_LOG_10        2
#define RGBOPT_CS_LINEAR_8      3
#define RGBOPT_CS_LINEAR_16     4
#define RGBOPT_CS_LINEAR_12     5
#define RGBOPT_CS_LOG_12        6

#define RGBOPT_CS_LINEAR_10_FLAG (1 << RGBOPT_CS_LINEAR_10)
#define RGBOPT_CS_LOG_10_FLAG    (1 << RGBOPT_CS_LOG_10)
#define RGBOPT_CS_LINEAR_8_FLAG  (1 << RGBOPT_CS_LINEAR_8)
#define RGBOPT_CS_LINEAR_16_FLAG (1 << RGBOPT_CS_LINEAR_16)
#define RGBOPT_CS_LINEAR_12_FLAG (1 << RGBOPT_CS_LINEAR_12)
#define RGBOPT_CS_LOG_12_FLAG    (1 << RGBOPT_CS_LOG_12)

#define RGBOPT_RANGE_DEFAULT     1
#define RGBOPT_RANGE_FULL        1
#define RGBOPT_RANGE_RESTRICTED  2
#define RGBOPT_RANGE_CUSTOM      3

#define RGBOPT_GAMMA_DEFAULT    1
#define RGBOPT_GAMMA_17         1
#define RGBOPT_GAMMA_10         2
#define RGBOPT_GAMMA_06         3
#define RGBOPT_GAMMA_CUSTOM     4
#define RGBOPT_GAMMA_22222      5

#define RGBOPT_GAMMA_CUSTOM_MIN         0.1
#define RGBOPT_GAMMA_CUSTOM_DEFAULT     1.0
#define RGBOPT_GAMMA_CUSTOM_MAX         3.0


#endif // RgbFileOptionsDialogH

