object TwoProxyWithSliderFrame: TTwoProxyWithSliderFrame
  Left = 0
  Top = 0
  Width = 302
  Height = 140
  TabOrder = 0
  DesignSize = (
    302
    140)
  object NewFrameProxyLabel: TLabel
    Left = 6
    Top = 111
    Width = 57
    Height = 13
    Caption = '99:99:99:99'
  end
  object OldFrameProxyLabel: TLabel
    Left = 240
    Top = 111
    Width = 57
    Height = 13
    Anchors = [akTop, akRight]
    Caption = '99:99:99:99'
  end
  object IdleArrow: TImage
    Left = 143
    Top = 56
    Width = 16
    Height = 16
    Anchors = [akLeft, akTop, akRight]
    Center = True
    Constraints.MaxHeight = 16
    Constraints.MaxWidth = 16
    Constraints.MinHeight = 16
    Constraints.MinWidth = 16
    Picture.Data = {
      07544269746D6170DE000000424DDE0000000000000076000000280000000D00
      00000D000000010004000000000068000000C40E0000C40E0000100000000000
      0000000000000000800000800000008080008000000080008000808000008080
      8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
      FF00FFFFFFFFFFFFF000FFFFFFF0FFFFF000FFFFFFF00FFFF000FFFFFFF000FF
      F000FFFFFFF0000FF000000000000000F0000000000000000000000000000000
      F000FFFFFFF0000FF000FFFFFFF000FFF000FFFFFFF00FFFF000FFFFFFF0FFFF
      F000FFFFFFFFFFFFF000}
    Transparent = True
  end
  object BusyArrows: TMTIBusy
    Left = 143
    Top = 56
    Width = 16
    Height = 16
    ArrowType = batSmall
    Anchors = [akLeft, akTop, akRight]
  end
  inline NewFrameProxyFrame: TSmallLabeledProxyFrame
    Left = 4
    Top = 4
    Width = 114
    Height = 106
    TabOrder = 1
  end
  object TrackBar: TTrackBar
    Left = 76
    Top = 117
    Width = 150
    Height = 20
    Anchors = [akLeft, akTop, akRight]
    Constraints.MaxHeight = 20
    Constraints.MaxWidth = 150
    Constraints.MinHeight = 20
    Constraints.MinWidth = 150
    Orientation = trHorizontal
    PageSize = 1
    Frequency = 1
    Position = 0
    SelEnd = 0
    SelStart = 0
    TabOrder = 0
    ThumbLength = 15
    TickMarks = tmBottomRight
    TickStyle = tsNone
  end
  inline OldFrameProxyFrame: TSmallLabeledProxyFrame
    Left = 184
    Top = 4
    Width = 114
    Height = 106
    Anchors = [akTop, akRight]
    TabOrder = 2
  end
end
