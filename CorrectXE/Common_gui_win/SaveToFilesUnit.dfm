object SaveToFilesFrame: TSaveToFilesFrame
  Left = 0
  Top = 0
  Width = 450
  Height = 66
  TabOrder = 0
  object FilePathLabel: TLabel
    Left = 20
    Top = 3
    Width = 41
    Height = 13
    Caption = 'File Path'
  end
  object TemplateLabel: TLabel
    Left = 17
    Top = 17
    Width = 44
    Height = 13
    Caption = 'Template'
  end
  object FirstFrameLabel1: TLabel
    Left = 42
    Top = 34
    Width = 21
    Height = 13
    Caption = 'First'
  end
  object FilesFirstFrameLabel2: TLabel
    Left = 34
    Top = 45
    Width = 30
    Height = 13
    Caption = 'Frame'
  end
  object PoundSignMsgLabel1: TLabel
    Left = 130
    Top = 34
    Width = 272
    Height = 13
    Caption = 'Hint: Set the above field to the path of the first file, with'
  end
  object PoundSignMsgLabel2: TLabel
    Left = 130
    Top = 47
    Width = 259
    Height = 13
    Caption = 'valid extension.  Example:  c:\images\myclip.0000.tga'
  end
  object PathComboBox: TComboBox
    Left = 68
    Top = 8
    Width = 328
    Height = 21
    Hint = 
      'A file path where the file name contains a string of #'#39's that in' +
      'dicate where the sequence number is'
    TabOrder = 0
    OnEnter = PathComboBoxEnter
    OnExit = PathComboBoxExit
    OnKeyPress = PathComboBoxKeyPress
    OnSelect = PathComboBoxSelect
  end
  object BrowseButton: TButton
    Left = 408
    Top = 8
    Width = 20
    Height = 20
    Caption = '...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BrowseButtonClick
  end
  object FirstFrameEdit: TEdit
    Left = 68
    Top = 37
    Width = 49
    Height = 21
    Hint = 
      'First number of the sequence (will replace the #'#39's in the templa' +
      'te, above)'
    TabOrder = 2
    Text = '0'
  end
  object SaveDialog: TSaveDialog
    FileName = '0000'
    Filter = 
      'Cineon format (*.cin)|*.cin|DPX format (*.dpx)|*.dpx|SGI format ' +
      '(*.sgi)|*.sgi|Raw format (*.raw)|*.raw|RGB format (*.rgb)|*.rgb|' +
      'Targa2 format (*.tga)|*.tga|TIFF format (*.tif)|*.tif|Windows Bi' +
      'tmap format (*.bmp)|*.bmp|YUV format (*.yuv)|*.yuv'
    FilterIndex = 2
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Select Destination Image File'
    Left = 400
    Top = 32
  end
end
