object OneShotConfirmationDialog: TOneShotConfirmationDialog
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Information'
  ClientHeight = 124
  ClientWidth = 218
  Color = clBtnFace
  Constraints.MinHeight = 163
  Constraints.MinWidth = 234
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    218
    124)
  PixelsPerInch = 96
  TextHeight = 13
  object CautionImage: TImage
    Left = 24
    Top = 16
    Width = 32
    Height = 32
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D49484452000000200000
      00200806000000737A7AF400000006624B474400FF00FF00FFA0BDA793000000
      097048597300000B1300000B1301009A9C180000000774494D4507E004190E11
      13DFF840330000001D69545874436F6D6D656E74000000000043726561746564
      20776974682047494D50642E6507000001524944415478DAC5943B6EC2501045
      9F9D14149474448A110D52F6C05A6065CE56923D20D12040221D650A0AF0C3D1
      18F17BF3BB16641A63249B73CE186731C600CFCF275DDC9F64E82DB25600DF03
      BA78BCFC07803FFBF9943E8F4AB8020E50DBC7EE8A6EF25BC0153080C6FE30A4
      D397055E010368ECAB1E9DE65BBC821FE0D23E6FBEABF00A7E801BFBD3A0157C
      0029FBD380157C008C7D9B0A7600C9BE45053B80628F56B00130F6AFEF74DCAF
      F10A3600C63E09E0ACA00308BBE7003C15740061F72C80A3820CA03CF91280B5
      820CA03CF92280B1020F60F8DF6B00960A3C80E17FAF02182AA4012C6F3D2380
      56210D607CEB9900940AF700467B0F8054E11EC068EF02102A5C0338ECDDC354
      B80670D82393AA700600EC3D2BE02A9C01007B3740A202013C72F74A050278F0
      EEA50A59DC9431CCA6A17A7B82FD45857C531F3FCA7A055F450CBBD5937EF966
      3A453802A0C4633620BD12AF0000000049454E44AE426082}
    Transparent = True
  end
  object InformationLabel: TLabel
    Left = 62
    Top = 35
    Width = 81
    Height = 13
    Caption = 'InformationLabel'
  end
  object YesButton: TButton
    Left = 54
    Top = 65
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Yes'
    Default = True
    ModalResult = 6
    TabOrder = 0
    ExplicitTop = 79
  end
  object NoButton: TButton
    Left = 135
    Top = 65
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&No'
    ModalResult = 7
    TabOrder = 1
    ExplicitTop = 79
  end
  object OneShotCheckBox: TCheckBox
    Left = 8
    Top = 100
    Width = 137
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Do not show this again'
    TabOrder = 2
    ExplicitTop = 114
  end
end
