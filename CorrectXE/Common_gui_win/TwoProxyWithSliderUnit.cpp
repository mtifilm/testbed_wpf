//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TwoProxyWithSliderUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "SmallLabeledProxyUnit"
#pragma link "MTIBusy"
#pragma resource "*.dfm"
//TTwoProxyWithSliderFrame *TwoProxyWithSliderFrame;
//---------------------------------------------------------------------------
__fastcall TTwoProxyWithSliderFrame::TTwoProxyWithSliderFrame(TComponent* Owner)
    : TFrame(Owner)
{
        BusyArrows->Visible = false;
        IdleArrow->Visible = true;
}
//---------------------------------------------------------------------------
void TTwoProxyWithSliderFrame::SetBusy(bool flag)
{
    if (flag) {
        BusyArrows->Visible = true;
        BusyArrows->Busy = true;
        IdleArrow->Visible = false;
    } else {
        BusyArrows->Visible = false;
        BusyArrows->Busy = false;
        IdleArrow->Visible = true;
    }
}
//---------------------------------------------------------------------------
void TTwoProxyWithSliderFrame::Enable(bool flag)
{
    if (flag) {
        TrackBar->Enabled = true;
        TrackBar->SliderVisible = true;
    } else {
        SetBusy(false);
        TrackBar->Enabled = false;
        TrackBar->SliderVisible = false;
        OldFrameProxyFrame->Reset();
        NewFrameProxyFrame->Reset();
        OldFrameProxyLabel->Caption = "";
        NewFrameProxyLabel->Caption = "";
    }
}
//---------------------------------------------------------------------------

