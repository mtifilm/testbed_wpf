object ClipPickerForm: TClipPickerForm
  Left = 597
  Top = 84
  BorderStyle = bsSizeToolWin
  Caption = 'Select a Clip '
  ClientHeight = 406
  ClientWidth = 629
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter: TSplitter
    Left = 154
    Top = 0
    Height = 338
    ExplicitHeight = 350
  end
  object BottomPanel: TPanel
    Left = 0
    Top = 338
    Width = 629
    Height = 68
    Align = alBottom
    Anchors = [akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      629
      68)
    object ClipLabel: TLabel
      Left = 8
      Top = 11
      Width = 20
      Height = 13
      Caption = 'Clip:'
    end
    object ImageFormatLabel1: TLabel
      Left = 8
      Top = 43
      Width = 67
      Height = 13
      Caption = 'Image Format:'
    end
    object OkButton: TButton
      Left = 563
      Top = 8
      Width = 66
      Height = 21
      Anchors = [akTop, akRight]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
    end
    object CancelButton: TButton
      Left = 563
      Top = 40
      Width = 66
      Height = 21
      Anchors = [akTop, akRight]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object SelectedClipEdit: TEdit
      Left = 80
      Top = 8
      Width = 265
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 2
      OnExit = SelectedClipEditExit
      OnKeyPress = SelectedClipEditKeyPress
    end
    object FilterFormatEdit: TEdit
      Left = 80
      Top = 40
      Width = 265
      Height = 21
      Enabled = False
      TabOrder = 3
      Text = 'Any format'
    end
  end
  inline ClipListViewFrame: TClipListViewFrame
    Left = 157
    Top = 0
    Width = 472
    Height = 338
    Align = alClient
    AutoSize = True
    TabOrder = 1
    ExplicitLeft = 157
    ExplicitWidth = 472
    ExplicitHeight = 338
    DesignSize = (
      472
      338)
    inherited ClipListView: TListView
      Width = 472
      Height = 338
      ExplicitWidth = 472
      ExplicitHeight = 338
    end
    inherited SelectedClipChangedNotifier: TCheckBox
      OnClick = ClipListViewFrameSelectedClipChangedNotifierClick
    end
    inherited DoubleClickNotifier: TCheckBox
      OnClick = ClipListViewFrameDoubleClickNotifierClick
    end
  end
  inline BinTreeViewFrame: TBinTreeViewFrame
    Left = 0
    Top = 0
    Width = 154
    Height = 338
    Align = alLeft
    TabOrder = 2
    ExplicitWidth = 154
    ExplicitHeight = 338
    inherited BinListHeaderPanel: TPanel
      Width = 154
      ExplicitWidth = 154
      inherited NewBinImage: TImage
        Left = 124
        Visible = False
        ExplicitLeft = 124
      end
    end
    inherited BinTreeView: TTreeView
      Width = 154
      Height = 318
      ExplicitWidth = 154
      ExplicitHeight = 318
    end
    inherited SelectionChangedNotifier: TCheckBox
      OnClick = BinTreeViewFrameSelectionChangedNotifierClick
    end
  end
end
