object ExecStatusBarFrame: TExecStatusBarFrame
  Left = 0
  Top = 0
  Width = 480
  Height = 38
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object BackPanel: TPanel
    Left = 0
    Top = 0
    Width = 480
    Height = 38
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      480
      38)
    object StatusMessageValue: TLabel
      Left = 60
      Top = 2
      Width = 18
      Height = 13
      Caption = 'Idle'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object StatusMessageLabel: TLabel
      Left = 16
      Top = 2
      Width = 40
      Height = 13
      Caption = 'Status:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ToolMessageValue: TLabel
      Left = 398
      Top = 2
      Width = 62
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'ToolMessage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 430
    end
    object ReadoutPanel: TPanel
      Left = 0
      Top = 18
      Width = 480
      Height = 20
      BevelOuter = bvLowered
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object ProgressPanelLight: TColorPanel
        Left = 2
        Top = 2
        Width = 476
        Height = 17
        BevelOuter = bvNone
        Color = clWindow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        object PercentValue1: TLabel
          Left = 0
          Top = 2
          Width = 29
          Height = 13
          Caption = '100%'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object ElapsedLabel1: TLabel
          Left = 148
          Top = 2
          Width = 46
          Height = 13
          Caption = 'Elapsed:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TotalLabel1: TLabel
          Left = 33
          Top = 2
          Width = 35
          Height = 13
          Caption = 'Total: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RemainingValue1: TLabel
          Left = 338
          Top = 2
          Width = 77
          Height = 13
          Caption = '999999 0:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object RemainingLabel1: TLabel
          Left = 274
          Top = 2
          Width = 63
          Height = 13
          Caption = 'Remaining:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object ElapsedValue1: TLabel
          Left = 195
          Top = 2
          Width = 77
          Height = 13
          Caption = '999999 0:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object PerFrameValue1: TLabel
          Left = 418
          Top = 2
          Width = 55
          Height = 13
          Caption = '0.000 sec/f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TotalValue1: TLabel
          Left = 69
          Top = 2
          Width = 77
          Height = 13
          Caption = '999999 0:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object ProgressPanelDark: TColorPanel
        Left = 2
        Top = 2
        Width = 476
        Height = 17
        BevelOuter = bvNone
        Color = cl3DDkShadow
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        object RemainingLabel2: TLabel
          Left = 274
          Top = 2
          Width = 63
          Height = 13
          Caption = 'Remaining:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object ElapsedLabel2: TLabel
          Left = 148
          Top = 2
          Width = 46
          Height = 13
          Caption = 'Elapsed:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TotalLabel2: TLabel
          Left = 33
          Top = 2
          Width = 35
          Height = 13
          Caption = 'Total: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TotalValue2: TLabel
          Left = 69
          Top = 2
          Width = 77
          Height = 13
          Caption = '999999 0:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object RemainingValue2: TLabel
          Left = 338
          Top = 2
          Width = 77
          Height = 13
          Caption = '999999 0:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object ElapsedValue2: TLabel
          Left = 195
          Top = 2
          Width = 77
          Height = 13
          Caption = '999999 0:00:00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object PerFrameValue2: TLabel
          Left = 418
          Top = 2
          Width = 55
          Height = 13
          Caption = '0.000 sec/f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object PercentValue2: TLabel
          Left = 0
          Top = 2
          Width = 29
          Height = 13
          Caption = '100%'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindow
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object DisabledPanel: TPanel
      Left = 0
      Top = 18
      Width = 480
      Height = 20
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object DisabledElapsedLabel: TLabel
        Left = 150
        Top = 4
        Width = 46
        Height = 13
        Caption = 'Elapsed:'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DisabledRemainingLabel: TLabel
        Left = 276
        Top = 4
        Width = 63
        Height = 13
        Caption = 'Remaining:'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DisabledTotalLabel: TLabel
        Left = 35
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Total: '
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DisabledPerFrameValue: TLabel
        Left = 420
        Top = 4
        Width = 51
        Height = 13
        Caption = '         sec/f'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DisabledPercentValue: TLabel
        Left = 14
        Top = 4
        Width = 11
        Height = 13
        Caption = '%'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object ElapsedTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = ElapsedTimerTimer
    Left = 178
    Top = 65512
  end
end
