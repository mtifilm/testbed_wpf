object PresetsFrame: TPresetsFrame
  Left = 0
  Top = 0
  Width = 177
  Height = 22
  AutoSize = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  DesignSize = (
    177
    22)
  object PresetPanel: TPanel
    Left = 0
    Top = 0
    Width = 177
    Height = 22
    Anchors = []
    AutoSize = True
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PresetLabel: TLabel
      Left = 0
      Top = 4
      Width = 40
      Height = 14
      Caption = 'Presets'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object PresetButton1: TSpeedButton
      Left = 46
      Top = 0
      Width = 23
      Height = 22
      Hint = 'Load Presets set #1 (1 key)'
      AllowAllUp = True
      GroupIndex = 1
      Caption = '1'
      Layout = blGlyphBottom
      OnClick = PresetButtonClick
    end
    object PresetButton2: TSpeedButton
      Tag = 1
      Left = 70
      Top = 0
      Width = 23
      Height = 22
      Hint = 'Load Presets set #2 (2 key)'
      AllowAllUp = True
      GroupIndex = 1
      Caption = '2'
      Layout = blGlyphBottom
      OnClick = PresetButtonClick
    end
    object PresetButton3: TSpeedButton
      Tag = 2
      Left = 94
      Top = 0
      Width = 23
      Height = 22
      Hint = 'Load Presets set #3 (3 key)'
      AllowAllUp = True
      GroupIndex = 1
      Caption = '3'
      Layout = blGlyphBottom
      OnClick = PresetButtonClick
    end
    object PresetButton4: TSpeedButton
      Tag = 3
      Left = 118
      Top = 0
      Width = 23
      Height = 22
      Hint = 'Load Presets set #4 (4 key)'
      AllowAllUp = True
      GroupIndex = 1
      Caption = '4'
      Layout = blGlyphBottom
      OnClick = PresetButtonClick
    end
    object DisabledSaveButton: TBitBtn
      Left = 142
      Top = 0
      Width = 35
      Height = 22
      Hint = 'Disabled because current settings match selected preset'
      Caption = 'Save'
      Enabled = False
      Layout = blGlyphBottom
      TabOrder = 1
    end
    object SavePresetButton: TBitBtn
      Left = 142
      Top = 0
      Width = 35
      Height = 22
      Hint = 'Save current settings to selected preset'
      Caption = 'Save'
      Enabled = False
      Layout = blGlyphBottom
      TabOrder = 0
      OnClick = SavePresetButtonClick
    end
  end
end
