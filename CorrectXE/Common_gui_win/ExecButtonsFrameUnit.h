//---------------------------------------------------------------------------


#ifndef ExecButtonsFrameUnitH
#define ExecButtonsFrameUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//#include "TB2Dock.hpp"
//#include "TB2Item.hpp"
//#include "TB2Toolbar.hpp"
#include "VTimeCodeEdit.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include "ColorPanel.h"

//---------------------------------------------------------------------------

enum EExecButtonId {
   EXEC_BUTTON_NONE = 20000,       // QQQ Must be unique across plugin tools
   EXEC_BUTTON_PREVIEW_FRAME,
   EXEC_BUTTON_PREVIEW_NEXT_FRAME, // no button for this presently
   EXEC_BUTTON_PREVIEW_ALL,
   EXEC_BUTTON_RENDER_FRAME,
   EXEC_BUTTON_RENDER_NEXT_FRAME,  // no button for this presently
   EXEC_BUTTON_RENDER_ALL,
   EXEC_BUTTON_PAUSE,
   EXEC_BUTTON_RESUME,
   EXEC_BUTTON_PAUSE_OR_RESUME,    // no actual button for this
   EXEC_BUTTON_STOP,
   EXEC_BUTTON_CAPTURE_PDL,
   EXEC_BUTTON_CAPTURE_PDL_ALL_EVENTS, // no separate button for this
   EXEC_BUTTON_GO_TO_RESUME_TIMECODE,
   EXEC_BUTTON_SET_RESUME_TC
};
//---------------------------------------------------------------------------

class TExecButtonsFrame : public TFrame
{
__published:	// IDE-managed Components
    TGroupBox *ResumeGroupBox;
    TSpeedButton *GotoTCButton;
    TSpeedButton *SetStartTCButton;
    VTimeCodeEdit *ResumeTCEdit;
	TColorPanel *ToolbarPanel;
    TSpeedButton *PreviewFrameButton;
    TSpeedButton *PreviewAllButton;
    TSpeedButton *RenderFrameButton;
    TSpeedButton *RenderAllButton;
    TSpeedButton *PauseButton;
    TSpeedButton *ResumeButton;
    TSpeedButton *StopButton;
    TSpeedButton *CapturePDLButton;
    TSpeedButton *ButtonPressedNotifier;
	TPanel *PreviewFrameButtonPanel;
	TPanel *PreviewAllButtonPanel;
	TPanel *RenderAllButtonPanel;
	TPanel *RenderFrameButtonPanel;
    void __fastcall ExecButtonClick(TObject *Sender);

private:	// User declarations
    EExecButtonId LastButtonClicked;
    
	 void setButtonEnabledProperty(EExecButtonId buttonId, TPanel *panel, bool flag);
    void setButtonVisibleProperty(EExecButtonId buttonId, bool flag);
	 TControl *findControlByTag(int tag, TPanel *panel);

public:		// User declarations
    __fastcall TExecButtonsFrame(TComponent* Owner);

    EExecButtonId GetLastButtonClicked(void);
    void EnableButton(EExecButtonId buttonId);
    void DisableButton(EExecButtonId buttonId);
    void ShowButton(EExecButtonId buttonId);
    void HideButton(EExecButtonId buttonId);
    void SetDownButton(EExecButtonId buttonId);
    EExecButtonId GetDownButton(void);
    void SetButtonHint(EExecButtonId buttonId, const string &newHint);
    string GetButtonHint(EExecButtonId buttonId);
    void EnableResumeWidget(void);
    void DisableResumeWidget(void);
    void SetResumeTimecode(PTimecode ptc);
    PTimecode GetResumeTimecode(void);
    bool RunCommand(EExecButtonId commandNumber);
	 bool IsButtonEnabled(EExecButtonId buttonId);
	 void SetButtonPressedNotifierCallback(TNotifyEvent callback);
};
//---------------------------------------------------------------------------

// NO! extern PACKAGE TExecButtonsFrame *ExecButtonsFrame;
//---------------------------------------------------------------------------
#endif
