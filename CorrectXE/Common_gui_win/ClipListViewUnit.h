//---------------------------------------------------------------------------


#ifndef ClipListViewUnitH
#define ClipListViewUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>

#include "Clip3.h"
#include "SafeClasses.h"
#include "MTIBusy.h"
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TClipListViewFrame : public TFrame
{
__published:	// IDE-managed Components
    TListView *ClipListView;
    TCheckBox *SelectedClipChangedNotifier;
    TCheckBox *DoubleClickNotifier;
    TMTIBusy *BusyArrows;
    TTimer *ArrowCheckTimer;
    void __fastcall SawDoubleClick(TObject *Sender);
    void __fastcall ListItemChanged(TObject *Sender, TListItem *Item,
          TItemChange Change);
    void __fastcall ClipListViewKeyPress(TObject *Sender, char &Key);
    void __fastcall ArrowCheckTimerTimer(TObject *Sender);
private:	// User declarations
    string MyIniFileName;
    string SelectedClipName;
    string SelectedBinPath;
    void   *BuilderThreadId;
    bool   BuilderIsRunning;
    CThreadLock BuildLock;
    string ClipFormatFilter;

    void buildClipList(const string &binPath);
    TListItem *addClipToClipList(const CClipInitInfo &clipInitInfo);
    StringList makeClipListRowValues(const CClipInitInfo &clipInitInfo);
    static void StartBuilding(void *vpAppData, void *vpReserved);
    void BuilderThread() ;

public:		// User declarations
    void InitFromIniFile(const string &iniFileName);
    int OK();
    void SetClipFormatFilter(const string &filter);
    void SetSelectedClip(const string& clipName);
    string GetSelectedClip();
    void RebuildClipList(const string &binPath);

    __fastcall TClipListViewFrame(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TClipListViewFrame *ClipListViewFrame;
//---------------------------------------------------------------------------
#endif


