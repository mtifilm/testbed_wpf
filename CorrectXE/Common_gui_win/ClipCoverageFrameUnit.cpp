//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ClipCoverageFrameUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ColorPanel"
#pragma resource "*.dfm"
//TClipCoverageFrame *ClipCoverageFrame;

//---------------------------------------------------------------------------
namespace {
    const int CLIENT_MARGIN = 2;
} ;
//---------------------------------------------------------------------------
__fastcall TClipCoverageFrame::TClipCoverageFrame(TComponent* Owner)
    : TFrame(Owner)
{
    Reset();
}
//---------------------------------------------------------------------------

void TClipCoverageFrame::Reset()
{
    InTimecodeLabel->Caption = "00:00:00:00";
    InTimecodeLabel->Enabled = false;
    OutTimecodeLabel->Caption = "00:00:00:00";
    OutTimecodeLabel->Enabled = false;
    FillerPanel->Visible = false;
    FillerPanel->Color = clAqua;
    InfoLabel->Visible = false;
    InfoLabel->Caption = "";
    TotalFrameCountLabel->Visible = false;
    WarningLabel->Visible = false;
    WarningLabel->Caption = "";

    UsableClientWidth = BackgroundPanel->Width
                      - 2 * CLIENT_MARGIN;
}
//---------------------------------------------------------------------------


void TClipCoverageFrame::SetTimecodeLabels(const AnsiString &inLabel,
                                           const AnsiString &outLabel)
{
    InTimecodeLabel->Caption = inLabel;
    InTimecodeLabel->Enabled = true;
    OutTimecodeLabel->Caption = outLabel;
    OutTimecodeLabel->Enabled = true;
}
//---------------------------------------------------------------------------

void TClipCoverageFrame::SetWarning(const AnsiString &warningLabel)
{
    WarningLabel->Caption = warningLabel;
    WarningLabel->Left = CLIENT_MARGIN;
    WarningLabel->Width = UsableClientWidth;
    WarningLabel->Alignment = taCenter;

    if (warningLabel == "") {

        if (InfoLabel->Caption != "") {
            InfoLabel->Visible = true;
        }
        FillerPanel->Color = clAqua;
        WarningLabel->Visible = false;

    } else {

        InfoLabel->Visible = false;
        FillerPanel->Color = clRed;
        WarningLabel->Visible = true;
    }
}
//---------------------------------------------------------------------------

void TClipCoverageFrame::SetCoverage(int total, int start, int stop)
{
    if (total > 0 && start < stop) {
        // QQQ Magic number 3 is total width of left and right borders
        int wellWidth = FillerWellPanel->Width - 3;
        int frameCount = stop - start;

        FillerPanel->Left = 1 + ((wellWidth * start) / total);
        FillerPanel->Width = (wellWidth * frameCount) / total;
        if (FillerPanel->Width < 1) {
            FillerPanel->Width = 1;
        }
        FillerPanel->Visible = true;

        TotalFrameCountLabel->Caption = AnsiString(total) + " Frames in Clip";
        TotalFrameCountLabel->Width = UsableClientWidth;
        TotalFrameCountLabel->Left = CLIENT_MARGIN;
        TotalFrameCountLabel->Alignment = taCenter;
        TotalFrameCountLabel->Visible = true;

        if (frameCount == 1) {
            InfoLabel->Caption =
                    AnsiString("Selected frame ") + AnsiString(start);
        } else if (frameCount == 2) {
            InfoLabel->Caption = AnsiString("Selected 2 frames (")
                               + AnsiString(start)
                               + AnsiString(" and ")
                               + AnsiString(start + 1)
                               + AnsiString(")");
        } else if (start == 0 && frameCount == total) {
            InfoLabel->Caption =
                    AnsiString("Selected all frames");
        } else {
            InfoLabel->Caption = AnsiString("Selected ")
                               + AnsiString(frameCount)
                               + AnsiString(" frames (")
                               + AnsiString(start)
                               + AnsiString(" through ")
                               + AnsiString(stop - 1)
                               + AnsiString(")");
        }
        InfoLabel->Left = CLIENT_MARGIN;
        InfoLabel->Width = UsableClientWidth;
        InfoLabel->Alignment = taCenter;
        if (!WarningLabel->Visible) {
            InfoLabel->Visible = true;
        }
    } else {
        FillerPanel->Visible = false;
        InfoLabel->Caption = "";
        InfoLabel->Visible = false;
        TotalFrameCountLabel->Visible = false;
    }
}
//---------------------------------------------------------------------------


