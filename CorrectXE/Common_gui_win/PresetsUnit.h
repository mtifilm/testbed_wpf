//---------------------------------------------------------------------------

#ifndef PresetsUnitH
#define PresetsUnitH
//---------------------------------------------------------------------------
#include "PresetParameterModel.h"
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TPresetsFrame : public TFrame
{
__published:	// IDE-managed Components
	TPanel *PresetPanel;
	TLabel *PresetLabel;
	TSpeedButton *PresetButton1;
	TSpeedButton *PresetButton2;
	TSpeedButton *PresetButton3;
	TSpeedButton *PresetButton4;
	TBitBtn *SavePresetButton;
	TBitBtn *DisabledSaveButton;
	void __fastcall PresetButtonClick(TObject *Sender);
	void __fastcall SavePresetButtonClick(TObject *Sender);
private:	// User declarations
	TSpeedButton * _presetButtonArray[4];
	IPresets * _presets = nullptr;
	string _iniFileName;

	DEFINE_CBHOOK(CurrentPresetHasChanged, TPresetsFrame);
	DEFINE_CBHOOK(CurrentParametersHaveChanged, TPresetsFrame);

public:		// User declarations
	__fastcall TPresetsFrame(TComponent* Owner);
	__fastcall ~TPresetsFrame(void);
	void Initialize(IPresets *presets, const string &iniFileName);
	void UpdateGuiFromCurrentPreset();
	void SavePresets();
};
//---------------------------------------------------------------------------
extern PACKAGE TPresetsFrame *PresetsFrame;
//---------------------------------------------------------------------------
#endif
