//---------------------------------------------------------------------------


#ifndef FourProxiesUnitH
#define FourProxiesUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "SmallLabeledProxyUnit.h"
//---------------------------------------------------------------------------
class TFourProxiesFrame : public TFrame
{
__published:	// IDE-managed Components
    TSmallLabeledProxyFrame *SmallLabeledProxyFrame4;
    TSmallLabeledProxyFrame *SmallLabeledProxyFrame1;
    TSmallLabeledProxyFrame *SmallLabeledProxyFrame2;
    TSmallLabeledProxyFrame *SmallLabeledProxyFrame3;
private:	// User declarations
public:		// User declarations
    __fastcall TFourProxiesFrame(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TFourProxiesFrame *FourProxiesFrame;
//---------------------------------------------------------------------------
#endif
