object ClipSelectorForm: TClipSelectorForm
  Left = 285
  Top = 250
  Width = 613
  Height = 452
  Caption = 'Select Clip '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 225
    Top = 0
    Width = 5
    Height = 387
    Cursor = crHSplit
  end
  object ProxyDisplayPanel: TPanel
    Left = 0
    Top = 0
    Width = 225
    Height = 387
    Align = alLeft
    Alignment = taLeftJustify
    BevelOuter = bvNone
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object BinTree: TTreeView
      Left = 0
      Top = 20
      Width = 225
      Height = 367
      Align = alClient
      HideSelection = False
      Indent = 19
      ReadOnly = True
      TabOrder = 0
      TabStop = False
    end
    object BinsFillerPanel: TPanel
      Left = 0
      Top = 0
      Width = 225
      Height = 20
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = '   Bins'
      TabOrder = 1
      DesignSize = (
        225
        20)
      object Image1: TImage
        Left = 200
        Top = 0
        Width = 17
        Height = 16
        Anchors = [akTop, akRight]
        Picture.Data = {
          07544269746D6170EE030000424DEE0300000000000036000000280000001200
          0000110000000100180000000000B8030000C40E0000C40E0000000000000000
          0000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000
          0000000000000000000000000000000000000000000000000000000000000000
          00C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000C8D0D400FFFFC8D0
          D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFF000000C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D40000C8D0D400000000FFFFC8D0D400FFFFC8D0D400FFFFC8
          D0D400FFFFC8D0D400FFFFC8D0D4000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          0000C8D0D4000000C8D0D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFF
          C8D0D400FFFF000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000
          00FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D40000
          00C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000C8D0D400FFFFC8D0
          D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFF000000C8D0D4C8D0D400
          0000C8D0D4C8D0D40000C8D0D400000000FFFFC8D0D400FFFFC8D0D400FFFFC8
          D0D400FFFFC8D0D400FFFFC8D0D4000000C8D0D4000000C8D0D4C8D0D4C8D0D4
          0000C8D0D4000000C8D0D400FFFFC8D0D400FFFFC8D0D400FFFFC8D0D400FFFF
          C8D0D400FFFF000000000000C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4000000
          000000000000000000000000000000000000000000000000000000000000C8D0
          D4C8D0D4000000C8D0D4000000C8D0D40000C8D0D4C8D0D400000000FFFFC8D0
          D400FFFFC8D0D4000000C8D0D4C8D0D4C8D0D4000000C8D0D4000000C8D0D4C8
          D0D4C8D0D4C8D0D40000C8D0D4C8D0D4C8D0D4000000000000000000000000C8
          D0D4C8D0D4C8D0D4000000C8D0D4000000C8D0D4000000C8D0D4C8D0D4C8D0D4
          0000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000C8D0D4C8D0D40000C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000
          00C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000C8D0D4C8D0D4C8D0D4C8D0D4C8D0
          D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D40000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8
          D0D4C8D0D4C8D0D4C8D0D4C8D0D4000000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          0000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
          C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D40000}
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 387
    Width = 605
    Height = 38
    Align = alBottom
    Anchors = [akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      605
      38)
    object ClipLabel: TLabel
      Left = 8
      Top = 13
      Width = 17
      Height = 13
      Caption = 'Clip'
    end
    object ComboBox1: TComboBox
      Left = 32
      Top = 10
      Width = 401
      Height = 21
      Anchors = [akLeft, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 0
      Text = 'ComboBox1'
    end
    object Button1: TButton
      Left = 451
      Top = 10
      Width = 66
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 1
    end
    object Button2: TButton
      Left = 531
      Top = 10
      Width = 66
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
    end
  end
  object ClipListView: TListView
    Left = 230
    Top = 0
    Width = 375
    Height = 387
    Align = alClient
    BevelInner = bvLowered
    Columns = <
      item
        Caption = 'Clips'
        Width = 150
      end
      item
        Caption = 'Status'
        Width = -1
        WidthType = (
          -1)
      end
      item
        Caption = 'Image Format'
        Width = -1
        WidthType = (
          -1)
      end
      item
        Caption = 'In'
        Width = -1
        WidthType = (
          -1)
      end
      item
        Caption = 'Out'
        Width = -1
        WidthType = (
          -1)
      end
      item
        Caption = 'Duration'
        Width = -1
        WidthType = (
          -1)
      end
      item
        Caption = 'Description'
        Width = -2
        WidthType = (
          -2)
      end
      item
        Caption = 'Created'
        Width = -1
        WidthType = (
          -1)
      end>
    HideSelection = False
    MultiSelect = True
    ReadOnly = True
    SortType = stText
    TabOrder = 2
    TabStop = False
    ViewStyle = vsReport
  end
end
