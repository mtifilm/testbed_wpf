#include "MTIDialogs.h"

#include "IniFile.h"
#include "MTIio.h"
#include "MTIstringstream.h"
#include "SharedMap.h"
#include "ShowModalDialog.h"

#include <windows.h>
#include <shobjidl.h>

#include <vcl.h>

// The callback attached to a button click must be a member function.
// Create a dummy class to fulfill that requirement.
#ifndef _MSC_VER
class Dummy : public TObject {
  public:
    void __fastcall MTICloseInformationDialogModeless(TObject *Sender);
};

void __fastcall Dummy::MTICloseInformationDialogModeless(TObject *Sender) {
    TForm *parent = static_cast<TForm*>(static_cast<TControl*>(Sender)->Parent);
    parent->Release();
    delete this;
}
#endif

//------------GetApplicationHandle-----------------John Mertus----Dec 2003---

   HWND GetApplicationHandle(void)

//  This just returns a handle for the dialog boxes, it should be set to
// the Application->Handle or NULL.
//
//**************************************************************************
{
#ifndef _MSC_VER
   HANDLE _ApplicationHandle = (HANDLE)SharedMap::retrieve("ApplicationHandle");
   if (_ApplicationHandle == NULL)
   {
      _ApplicationHandle = Application->Handle;
      SharedMap::add("ApplicationHandle", (void *)_ApplicationHandle);
   }

   return (HWND)_ApplicationHandle;
#else
   return (HWND)-1;
#endif
}

//*******************Error Dialog Boxes**************************************

/*------------_MTISevereErrorRetryDialog----------John Mertus----Oct 2001---*/

   int _MTISevereErrorRetryDialogHL(const string &msg, const string &file, int line)

/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  return _MTISevereErrorRetryDialogH(GetApplicationHandle(), msg, file, line);
}

   int _MTISevereErrorRetryDialog(const string &msg)
   {
	   return _MTISevereErrorRetryDialogHL(msg, __FILE__, __LINE__);
   }


/*------------_MTISevereErrorRetryDialog----------John Mertus----Oct 2001---*/

   int _MTISevereErrorRetryDialogH(HWND Handle, const string &msg, const string &file, int line)

/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  MTIostringstream os;
  os << msg << "\n" << "Fatal Error in " << file << ", Line: " << line;
  TRACE_MSG(errout << os.str() << " " << GetApplicationName());

#ifndef _MSC_VER

  int mb = MessageBox(Handle, os.str().c_str() , "Fatal Error", MB_ICONSTOP | MB_ABORTRETRYIGNORE | MB_TOPMOST);
  if (mb == IDABORT)
    {
      TRACE_0(errout << "Program aborted by user");
      exit(1);
    }

  int Result = 0;
  switch (mb)
    {
      case IDCANCEL:
        Result = MTI_DLG_CANCEL;
        break;

      case IDRETRY:
        Result = MTI_DLG_RETRY;
        break;

      default:
        Result = MTI_DLG_ABORT;       // Never happens
    }
#else //_MSC_VER is defined
  int Result = 0;
#endif

  return(Result);
}

/*------------_MTISevereErrorDialog----------John Mertus----Oct 2001---*/

   int _MTISevereErrorDialogHL(const string &msg, const string &file, int line)

/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  return _MTISevereErrorDialogH(GetApplicationHandle(), msg, file, line);
}

/*------------_MTISevereErrorDialog----------John Mertus----May 2013---*/

	int _MTISevereErrorDialogHL(const string &msg)

/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display using the file name and line              */
/*                                                                          */
/****************************************************************************/
{
	return _MTISevereErrorDialogHL(msg, __FILE__, __LINE__);
}

/*------------_MTISevereErrorDialog---------------John Mertus----Oct 2001---*/

   int _MTISevereErrorDialogH(HWND Handle, const string &msg, const string &file, int line)

/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  MTIostringstream os;
  os << msg << "\n" << "Fatal Error in " << file << ", Line: " << line;
  TRACE_MSG(errout << os.str() << " " << GetApplicationName());

#ifndef _MSC_VER

  os << "\nAbort Program?";
  int mb = MessageBox(Handle, os.str().c_str() , "Severe Error", MB_ICONSTOP | MB_YESNO | MB_TOPMOST);
  if (mb == IDYES)
    {
      TRACE_0(errout << "Program aborted by user");
      exit(1);
    }

  int Result = 0;
  switch (mb)
    {
      case IDNO:
        Result = MTI_DLG_CANCEL;
        break;

      default:
        Result = MTI_DLG_ABORT;       // Never happens
    }
#else //_MSC_VER is defined
  int Result = 0;
#endif

  return Result;
}


//------------_MTIErrorDialog------------------------John Mertus----Oct 2001---

   int _MTIErrorDialog(const string &msg)

//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 1 error
//
//****************************************************************************
{
  return _MTIErrorDialog(GetApplicationHandle(), msg);
}


//------------_MTIErrorDialog------------------------John Mertus----Oct 2001---

   int _MTIErrorDialog(HWND Handle, const string &msg)

//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 1 error
//
//****************************************************************************
{

  TRACE_1(errout << msg);
#ifndef _MSC_VER
  MessageBox((HWND)Handle, msg.c_str() , "Error", MB_ICONSTOP | MB_OK | MB_TOPMOST);
#endif
  return MTI_DLG_OK;
}

//------------_MTIInformationDialog------------------John Mertus----Oct 2001---

   int _MTIInformationDialog(const string &msg)

//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 1 error
//
//****************************************************************************
{
  return _MTIInformationDialog(GetApplicationHandle(), msg);
}

//------------_MTIInformationDialog------------------John Mertus----Oct 2001---

   int _MTIInformationDialog(HWND Handle, const string &msg)

//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 1 error
//
//****************************************************************************
{
  TRACE_3(errout << msg);
#ifndef _MSC_VER
  MessageBox(Handle, msg.c_str() , "Information", MB_ICONINFORMATION | MB_OK | MB_TOPMOST);
#endif
  return(MTI_DLG_OK);
}

/*------------_MTIInformationDialogModeless---------------John Mertus----Oct 2001---*/

   int _MTIInformationDialogModeless(const string &msg)

/*  This displays an error and returns the button a user pressed.           */
/*  msg is the message to display                                           */
/*  file is assumed to be the file name (from the __FILE__ macro)           */
/*  line is the line number (from the __LINE__ macro)                       */
/*                                                                          */
/****************************************************************************/
{
  return _MTIInformationDialogModeless(GetApplicationHandle(), msg);
}

//------------_MTIInformationDialogModeless----------Matt McClure----Apr 2004---

// TODO: add implementation(s) of this method for non-Windows
// platforms if necessary
   int _MTIInformationDialogModeless(HWND Handle, const string &msg)

//  This displays an error and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on all cases.
//  Trace 3 error
//
//****************************************************************************
{
#ifndef _MSC_VER

  TRACE_3(errout << msg);

  TMsgDlgButtons dlgButtons;
  dlgButtons << mbYes << mbHelp << mbOK;

  Forms::TForm *dlg = CreateMessageDialog(msg.c_str(), mtInformation, dlgButtons);

////TTT  dlg->FormStyle = fsStayOnTop;

  // Get access to the OnClick property.
  class TPublicControl : public TControl {
    public:
      __property OnClick;
  };

  TPublicControl *okButton =
      static_cast<TPublicControl*>(dlg->FindChildControl("OK"));
  Dummy *d = new Dummy;
  okButton->OnClick = d->MTICloseInformationDialogModeless;

  dlg->Show();
#endif
  return(MTI_DLG_OK);
}

//------------_MTIWarningDialog------------------------John Mertus----Oct 2001---

   int _MTIWarningDialog(const string &msg)

//  This displays an warning and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the OK button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
  return _MTIWarningDialog(GetApplicationHandle(), msg);
}

//------------_MTIWarningDialog------------------------John Mertus----Oct 2001---

   int _MTIWarningDialog(HWND Handle, const string &msg)

//  This displays an warning and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the OK button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{

  TRACE_2(errout << msg);
#ifndef _MSC_VER
  int mb = MessageBox(Handle, msg.c_str() , "Warning", MB_ICONWARNING | MB_OKCANCEL | MB_TOPMOST);
  int Result = 0;
  switch (mb)
    {
      case IDOK:
        Result = MTI_DLG_OK;
        break;

      case IDCANCEL:
        Result = MTI_DLG_CANCEL;
        break;
    }
#else
  int Result = MTI_DLG_OK;
#endif

  return(Result);

}

//------------_MTIConfirmationDialog-----------------John Mertus----Oct 2001---

   int _MTIConfirmationDialog(const string &msg)

//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_CANCEL on the NO button
//  Trace 2 error reported
//
//****************************************************************************
{
   return _MTIConfirmationDialog(GetApplicationHandle(), msg);
}

//------------_MTIConfirmationDialog-----------------John Mertus----Oct 2001---

   int _MTIConfirmationDialog(HWND Handle, const string &msg)

//  This asks the user for conformation and returns the button a user pressed.
//  Handle is the display handle to use
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_CANCEL on the NO button
//  Trace 2 error reported
//
//****************************************************************************
{
#ifndef _MSC_VER
  int mb = MessageBox((HWND)Handle, msg.c_str() , "Information", MB_ICONWARNING | MB_YESNO | MB_TOPMOST);
  int Result = 0;
  switch (mb)
    {
	  case IDYES:
        Result = MTI_DLG_OK;
        break;

      case IDNO:
        Result = MTI_DLG_CANCEL;
        break;
    }
#else
  int Result = MTI_DLG_OK;
#endif

  return(Result);
}

//------------_MTICancelConfirmationDialog-----------------John Mertus----Oct 2001---

int _MTICancelConfirmationDialog(const string &msg)

//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_NO on the NO button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
   return _MTICancelConfirmationDialog(GetApplicationHandle(), msg);
}

//------------_MTICancelConfirmationDialog-----------------John Mertus----Oct 2001---

int _MTICancelConfirmationDialog(HWND Handle, const string &msg)

//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_NO on the NO button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
#ifndef _MSC_VER
	int mb = MessageBox(Handle, msg.c_str() , "Information", MB_ICONWARNING | MB_YESNOCANCEL | MB_TOPMOST);
	int Result = 0;
	switch (mb)
	{
		case IDYES:
			Result = MTI_DLG_OK;
			break;

		case IDNO:
			Result = MTI_DLG_NO;
			break;

		case IDCANCEL:
			Result = MTI_DLG_CANCEL;
			break;
	}
#else
	int Result = MTI_DLG_OK;
#endif

	return(Result);
}
//----------MTIConfirmationYesAllDialog-------------John Mertus----Oct 2001---

   int _MTIYesAllConfirmationDialog(HWND Handle, const string &msg)

//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_NO on the NO button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
#ifndef _MSC_VER
	int mb = MessageBox(Handle, msg.c_str() , "Information", MB_ICONWARNING | MB_YESNOCANCEL | MB_TOPMOST);
	int Result = 0;
	switch (mb)
	{
		case IDYES:
			Result = MTI_DLG_OK;
			break;

		case IDNO:
			Result = MTI_DLG_NO;
			break;

		case IDCANCEL:
			Result = MTI_DLG_CANCEL;
			break;
	}
#else
	int Result = MTI_DLG_OK;
#endif

	return Result ;
}

//----------_MTIYesAllConfirmationDialog-------------John Mertus----Oct 2001---

   int _MTIYesAllConfirmationDialog(const string &msg)

//  This asks the user for conformation and returns the button a user pressed.
//  msg is the message to display
//
//  Return is MTI_DLG_OK on the YES button
//  Return is MTI_DLG_NO on the NO button
//  Return is MTI_DLG_CANCEL on the CANCEL button
//  Trace 2 error reported
//
//****************************************************************************
{
   return _MTIYesAllConfirmationDialog(GetApplicationHandle(), msg);
}

//---------------------------------------------------------------------------

int MTIYouMustStopToolProcessingDialog()
{
#ifndef _MSC_VER

  string message = "To take this action, you must stop tool processing.\n\n"
                   "If processing cannot be stopped, click 'Abort' to\n"
                   "terminate the program.";
  int mb = MessageBox(GetApplicationHandle(), message.c_str() , "Action Disabled", MB_ICONSTOP | MB_ABORTRETRYIGNORE | MB_TOPMOST);

  int result = 0;
  switch (mb)
    {
      default:
        result = MTI_DLG_CANCEL;
        break;

      case IDRETRY:
        result = MTI_DLG_RETRY;
        break;

      case IDABORT:
        TRACE_0(errout << "FATAL: Can't stop tool processing so program aborted by user!");
        exit(1);
    }
#else //_MSC_VER is defined
  int result = 0;
#endif

  return result;
}


//--------------BrowseForFolder-----------------------------------------

string MTIBrowseForFolder(const string &startFolder, const string &title)
{
	string folderPath;

	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (!SUCCEEDED(hr))
	{
		MTIErrorDialog("ERROR: Folder browser can't init COM!");
		return folderPath;
	}

	IFileOpenDialog *pFileOpen;

	// Create the FileOpenDialog object.
	hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL, IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));
	if (!SUCCEEDED(hr))
	{
		MTIErrorDialog("ERROR: Folder browser can't create COM object instance!");
		CoUninitialize();
		return folderPath;
	}

	// Show the Open dialog box. Non-success indicates the user canceled.
	hr = pFileOpen->SetOptions(FOS_NOCHANGEDIR | FOS_PICKFOLDERS | FOS_FORCEFILESYSTEM | FOS_NOVALIDATE | FOS_PATHMUSTEXIST | FOS_FILEMUSTEXIST);
	if (!SUCCEEDED(hr))
	{
		MTIErrorDialog("ERROR: Folder browser can't set options!");
	}

	hr = pFileOpen->Show(Application->Handle);
	if (SUCCEEDED(hr))
	{
		 IShellItem *pItem;
		 hr = pFileOpen->GetResult(&pItem);
		 if (SUCCEEDED(hr))
		 {
			 PWSTR pszFilePath;
			  hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
			  folderPath = WCharToStdString(pszFilePath);
			  pItem->Release();
		 }
	}

	pFileOpen->Release();
	CoUninitialize();

	return folderPath;
}

