//---------------------------------------------------------------------------

#ifndef ClipSelectorUnitH
#define ClipSelectorUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TClipSelectorForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *ProxyDisplayPanel;
    TSplitter *Splitter1;
    TPanel *Panel3;
    TListView *ClipListView;
    TTreeView *BinTree;
    TComboBox *ComboBox1;
    TButton *Button1;
    TButton *Button2;
    TPanel *BinsFillerPanel;
    TLabel *ClipLabel;
    TImage *Image1;
private:	// User declarations
public:		// User declarations
    __fastcall TClipSelectorForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
//extern PACKAGE TClipSelectorForm *ClipSelectorForm;
//---------------------------------------------------------------------------
#endif
