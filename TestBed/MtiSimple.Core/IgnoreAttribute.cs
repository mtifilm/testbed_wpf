﻿// <copyright file="IgnoreAttribute.cs" company="MTI Film">
// Copyright (c) 2011 by MTI Film, All Rights Reserved
// </copyright>
// <author>pfirth</author>
// <date>1/12/2011 11:22:05 AM</date>
// <summary>Class that contains metadata information about whether or not to ignore an item when binding in a GUI.</summary>

namespace MtiSimple.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Class that contains metadata information about whether or not to ignore an item
    /// when binding in a gui.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class IgnoreAttribute : Attribute
    {
        //////////////////////////////////////////////////
        // Const and readonly public fields
        //////////////////////////////////////////////////
        #region Const and readonly public fields
        #endregion

        //////////////////////////////////////////////////
        // Static private fields
        //////////////////////////////////////////////////    
        #region Static private fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the IgnoreAttribute class
        /// </summary>
        public IgnoreAttribute()
        {
            this.Ignore = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IgnoreAttribute"/> class.
        /// </summary>
        /// <param name="ignore">if set to <c>true</c> [ignore].</param>
        public IgnoreAttribute(bool ignore)
        {
            this.Ignore = ignore;
        }

        #endregion

        //////////////////////////////////////////////////
        // Static properties
        //////////////////////////////////////////////////
        #region Static properties
        #endregion

        //////////////////////////////////////////////////
        // Properties
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IgnoreAttribute"/> is ignore.
        /// </summary>
        public bool Ignore { get; set; }

        #endregion

        //////////////////////////////////////////////////
        // Public static methods
        //////////////////////////////////////////////////
        #region Public static methods
        #endregion

        //////////////////////////////////////////////////
        // Public methods
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private static methods
        //////////////////////////////////////////////////
        #region Private static methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
