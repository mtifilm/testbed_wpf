﻿// -----------------------------------------------------------------------
// <copyright file="Enums.cs" company="MTI Film LLC">
// Copyright (c) 2011 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>rancor\pfirth</author>
// <date>12/1/2011 1:15:54 PM</date>
// <summary>Enum definitions.</summary>
// -----------------------------------------------------------------------

namespace MtiSimple.Core
{
    using System.ComponentModel;

    #region Burn Entry Prototype Category

    /// <summary>
    /// This is the enum for the type of the burn - this is the version used by the Convey program
    /// </summary>
    public enum BurnEntryPrototype
    {
        /// <summary>
        /// Not defined
        /// </summary>
        Unknown,

        /// <summary>
        /// Audio Timecode
        /// </summary>
        AudioTimecode,

        /// <summary>
        /// Aux Timecode
        /// </summary>
        AuxTimecode,

        /// <summary>
        /// Picture Timecode
        /// </summary>
        PictureTimecode,

        /// <summary>
        /// Layoff timecode in 24 fps
        /// </summary>
        LayoffTimecode24,

        /// <summary>
        /// Layofff timecode in native format
        /// </summary>
        LayoffTimecode,

        /// <summary>
        /// Layoff timecode, drop frame format
        /// </summary>
        LayoffTimecodeDropFrame,

        /// <summary>
        /// Absolute timecode starting from 00:00:00.00
        /// </summary>
        AbsoluteTimecode,

        /// <summary>
        /// Zero-based frame number.
        /// </summary>
        FrameNumber,

        /// <summary>
        /// Key code enum
        /// </summary>
        Keykode,

        /// <summary>
        /// Keycode with pulldown
        /// </summary>
        KeykodePulldown,

        /// <summary>
        /// Keycode with no perferations
        /// </summary>
        KeykodeNoPerf,

        /// <summary>
        /// Ink Number
        /// </summary>
        InkNumber,

        /// <summary>
        /// Production company
        /// </summary>
        ProductionCompany,

        /// <summary>
        /// Project enum
        /// </summary>
        Project,

        /// <summary>
        /// Title enum
        /// </summary>
        Title,

        /// <summary>
        /// Production number
        /// </summary>
        ProductionNumber,

        /// <summary>
        /// Production date
        /// </summary>
        ProductionDate,

        /// <summary>
        /// Transfer date
        /// </summary>
        TransferDate,

        /// <summary>
        /// Field A enum
        /// </summary>
        FieldA,

        /// <summary>
        /// Field B enum
        /// </summary>
        FieldB,

        /// <summary>
        /// Field C enum
        /// </summary>
        FieldC,

        /// <summary>
        /// Camera List
        /// </summary>
        CameraList,

        /// <summary>
        /// Scene List
        /// </summary>
        SceneList,

        /// <summary>
        /// Sound List
        /// </summary>
        SoundList,

        /// <summary>
        /// Field D enum
        /// </summary>
        FieldD,

        /// <summary>
        ///  Field E enum
        /// </summary>
        FieldE,

        /// <summary>
        ///  Field F enum
        /// </summary>
        FieldF,

        /// <summary>
        ///  Field G enum
        /// </summary>
        FieldG,

        /// <summary>
        /// Field H enum
        /// </summary>
        FieldH,

        /// <summary>
        /// Field I enum
        /// </summary>
        FieldI,

        /// <summary>
        /// Audio File Name
        /// </summary>
        AudioFilename,

        /// <summary>
        /// Image File Name
        /// </summary>
        ImageFilename,

        /// <summary>
        /// The Reel Name
        /// </summary>
        ReelName,

        /// <summary>
        /// The Camera Roll
        /// </summary>
        CameraRoll,

        /// <summary>
        /// The Sound Roll
        /// </summary>
        SoundRoll,

        /// <summary>
        /// The Lab roll
        /// </summary>
        LabRoll,

        /// <summary>
        /// The Shoot Date
        /// </summary>
        ShootDate,

        /// <summary>
        /// Scene Take Sound
        /// </summary>
        SceneTakeSound,

        /// <summary>
        /// Camera Scene Take
        /// </summary>
        CameraSceneTake,

        /// <summary>
        /// Scene Take Camera
        /// </summary>
        SceneTakeCamera,

        /// <summary>
        /// Scene Take
        /// </summary>
        SceneTake,

        /// <summary>
        /// Play Speed
        /// </summary>
        Playspeed,

        /// <summary>
        /// Custom Fields
        /// </summary>
        Custom,

        /// <summary>
        /// Convey recipient
        /// </summary>
        ConveyRecipient,

        /// <summary>
        /// Total run time
        /// </summary>
        TotalRunTime
    }

    /// <summary>
    /// This is the new enum for the type of the burn - this is the version used by Cohog
    /// </summary>
    public enum BurnEntryPrototypeNew
    {
        /// <summary>
        /// Not defined
        /// </summary>
        Unknown,

        /// <summary>
        /// Project enum
        /// </summary>
        ProjectName,

        /// <summary>
        /// Production company
        /// </summary>
        ProductionCompany,

        /// <summary>
        /// Season enum
        /// </summary>
        Season,

        /// <summary>
        /// Specified Year enum
        /// </summary>
        Year,

        /// <summary>
        /// Production Name enum
        /// </summary>
        ProductionName,

        /// <summary>
        /// Production number
        /// </summary>
        ProductionNumber,

        /// <summary>
        /// Title enum
        /// </summary>
        Title,

        /// <summary>
        /// Job name enum
        /// </summary>
        JobName,

        /// <summary>
        /// Production date
        /// </summary>
        ProductionDate,

        /// <summary>
        /// Job date enum
        /// </summary>
        JobDate,

        /// <summary>
        /// The Reel Name
        /// </summary>
        ReelName,

        /// <summary>
        /// Total run time
        /// </summary>
        TotalRunTime,

        /// <summary>
        /// Number of clips in reel
        /// </summary>
        NumberOfClips,

        /// <summary>
        /// Number of scenes in reel
        /// </summary>
        NumberOfScenes,

        /// <summary>
        /// Number of camera rools in reel
        /// </summary>
        NumberOfCameraRolls,

        /// <summary>
        /// Number of sound rolls in reel
        /// </summary>
        NumberOfSoundRolls,

        /// <summary>
        /// List of scenes in reel
        /// </summary>
        SceneList,

        /// <summary>
        /// List of camera rolls in reel
        /// </summary>
        CameraRollList,

        /// <summary>
        /// List of sound rolls in reel
        /// </summary>
        SoundRollList,

        /// <summary>
        /// Recipient Name
        /// </summary>
        RecipientName,

        /// <summary>
        /// The clip name
        /// </summary>
        ClipName,

        /// <summary>
        /// Audio File Name
        /// </summary>
        AudioFilename,

        /// <summary>
        /// Image File Name
        /// </summary>
        ImageFilename,

        /// <summary>
        /// The Camera Roll
        /// </summary>
        CameraRoll,

        /// <summary>
        /// The Camera ID
        /// </summary>
        CameraId,

        /// <summary>
        /// The Sound Roll
        /// </summary>
        SoundRoll,

        /// <summary>
        /// The Scene
        /// </summary>
        Scene,

        /// <summary>
        /// The Take
        /// </summary>
        Take,

        /// <summary>
        /// Camera Scene Take
        /// </summary>
        CameraSceneTake,

        /// <summary>
        /// Scene Take Camera
        /// </summary>
        SceneTakeCamera,

        /// <summary>
        /// Scene Take Sound
        /// </summary>
        SceneTakeSound,

        /// <summary>
        /// Scene Take
        /// </summary>
        SceneTake,

        /// <summary>
        /// Comma-seperated list of comments
        /// </summary>
        Comments,

        /// <summary>
        /// Shoot frames per second
        /// </summary>
        ShootFps,

        /// <summary>
        /// Playback fps
        /// </summary>
        PlaybackFps,

        /// <summary>
        /// Clip Audio Timecode
        /// </summary>
        ClipAudioTimecode,

        /// <summary>
        /// Clip Audio Aux Timecode
        /// </summary>
        ClipAuxTimecode,

        /// <summary>
        /// Clip Picture Timecode
        /// </summary>
        ClipPictureTimecode,

        /// <summary>
        /// Clip Absolute timecode starting from 00:00:00.00
        /// </summary>
        ClipAbsoluteTimecode,

        /// <summary>
        /// Clip Zero-based framecount.
        /// </summary>
        ClipAbsoluteFrameCount,

        /// <summary>
        /// The clip file number
        /// </summary>
        ClipFileNumber,

        /// <summary>
        /// Clip Keykode
        /// </summary>
        ClipKeykode,

        /// <summary>
        /// Reel Record (Layoff) timecode in native format
        /// </summary>
        ReelRecordTimecode,

        /// <summary>
        /// Reel Record (Layoff) timecode, 24 fps
        /// </summary>
        ReelRecord24Timecode,

        /// <summary>
        /// Reel Record (Layoff) timecode, 25 fps
        /// </summary>
        ReelRecord25Timecode,

        /// <summary>
        /// Reel Record (Layoff) timecode, 30 fps, non-drop format
        /// </summary>
        ReelRecord30NonDropTimecode,

        /// <summary>
        /// Reel Record (Layoff) timecode, 30 fps, drop frame format
        /// </summary>
        ReelRecord30DropFrameTimecode,

        /// <summary>
        /// Reel Record (Layoff) timecode, 50 fps
        /// </summary>
        ReelRecord50Timecode,

        /// <summary>
        /// Reel Record (Layoff) timecode, 60 fps
        /// </summary>
        ReelRecord60Timecode,

        /// <summary>
        /// Reel Absolute timecode starting from 00:00:00.00
        /// </summary>
        ReelAbsoluteTimecode,

        /// <summary>
        /// Reel Zero-based framecount.
        /// </summary>
        ReelAbsoluteFrameCount,

        /// <summary>
        /// Custom Fields
        /// </summary>
        Custom,

        /// <summary>
        /// Watermark Image
        /// </summary>
        Watermark,

        /// <summary>
        /// The clip's tapename
        /// </summary>
        Tapename,

        /// <summary>
        /// mark in point image
        /// </summary>
        MarkIn,

        /// <summary>
        /// mark out point image
        /// </summary>
        MarkOut,

        /// <summary>
        /// audio sync point image
        /// </summary>
        Sync,

        /// <summary>
        /// clip start point image
        /// </summary>
        Start,

        /// <summary>
        /// clip end point image
        /// </summary>
        End,

        /// <summary>
        /// The absolute feet plus frames 35 mm 4 perf.
        /// </summary>
        ClipAbsoluteFeetPlusFrames,

        /// <summary>
        /// The clip total frame count.
        /// </summary>
        ClipTotalFrameCount,

        /// <summary>
        /// The clip duration.
        /// </summary>
        ClipDuration,

        /// <summary>
        /// The reel absolute feet plus frames 35 mm 4 perf.
        /// </summary>
        ReelAbsoluteFeetPlusFrames,

        /// <summary>
        /// The reel absolute feet plus frames 16 mm.
        /// </summary>
        ReelAbsoluteFeetPlusFrames16Mm,

        /// <summary>
        /// The clip absolute feet plus frames 16 mm.
        /// </summary>
        ClipAbsoluteFeetPlusFrames16Mm
    }

    /// <summary>
    /// Still Capture Type.  How the still was captured.
    /// Enum values are not sequential, for backwards compatability.
    /// </summary>
    public enum StillCaptureType
    {
        /// <summary>
        /// Grabbed still
        /// </summary>
        Grabbed = 1,

        /// <summary>
        /// Timeline, one still per clip
        /// </summary>
        Timeline = 2,

        /// <summary>
        /// Memory stills
        /// </summary>
        Memory = 3,

        /// <summary>
        /// Imported Stills
        /// </summary>
        ImportedStills = 5,

        /// <summary>
        /// Stills generated from imported LUTs
        /// </summary>
        Luts = 6,

        /// <summary>
        /// The imported CDLS
        /// </summary>
        ImportedCdl = 8,
    }

    /// <summary>
    /// Still View type.  What subset of still we are viewing
    /// </summary>
    public enum StillDisplayMode
    {
        /// <summary>
        /// Grabbed still
        /// </summary>
        [Description("Still Store")]
        Grabbed,

        /// <summary>
        /// Timeline, one still per clip
        /// </summary>
        [Description("Time Line")]
        Timeline,

        /// <summary>
        /// The favorite stills
        /// </summary>
        [Description("Favorites")]
        Favorite,

        /// <summary>
        /// Imported Stills
        /// </summary>
        [Description("Imported Stills")]
        ImportedStills,

        /// <summary>
        /// The imported CDLs
        /// </summary>
        [Description("Imported CDLs")]
        ImportedCdl,

        /// <summary>
        /// Stills generated from imported LUTs
        /// </summary>
        [Description("LUTs")]
        Luts,

        /// <summary>
        /// All stills
        /// </summary>
        [Description("All")]
        [Ignore(true)]
        All,
    }

    /// <summary>
    /// the file type for exporting stills
    /// </summary>
    public enum StillFileType
    {
        /// <summary>
        /// All file types
        /// </summary>
        [Description("All")]
        All = 0,

        /// <summary>
        /// Cortex files only
        /// </summary>
        [Description("Cortex")]
        Cortex = 1,

        /// <summary>
        /// Jpeg files only
        /// </summary>
        [Description("Jpeg")]
        Jpeg = 2,
    }

    /// <summary>
    /// Still sort order
    /// </summary>
    public enum SortStillsByEnum
    {
        /// <summary>
        /// The created
        /// </summary>
        Created,

        /// <summary>
        /// The cam scene take
        /// </summary>
        CamSceneTake,

        /// <summary>
        /// The scene take cam
        /// </summary>
        SceneTakeCam,

        /// <summary>
        /// The timecode
        /// </summary>
        Timecode,

        /// <summary>
        /// The comments
        /// </summary>
        Comments,

        /// <summary>
        /// The filename
        /// </summary>
        Filename,

        /// <summary>
        /// The episode scene take
        /// </summary>
        EpisodeSceneTake,
    }

    /// <summary>
    /// Clip sort order
    /// </summary>
    public enum SortClipsByEnum
    {
        /// <summary>
        /// Source timecode
        /// </summary>
        [Description("Timecode")]
        Timecode = 0,

        /// <summary>
        /// Scene, Take, Cam
        /// </summary>
        [Description("Scene")]
        Scene = 1,

        /// <summary>
        /// Camera, Scene, Take
        /// </summary>
        [Description("Camera")]
        Camera = 2,

        /// <summary>
        /// Scene, Cam, Take
        /// </summary>
        [Description("Scene / Camera")]
        SceneCamera = 3,

        /// <summary>
        /// sort by source filename
        /// </summary>
        [Description("Filename")]
        Filename = 4,
    }

    /// <summary>
    /// Imported event sort order
    /// </summary>
    public enum SortImportedEventsByEnum
    {
        /// <summary>
        /// Source timecode
        /// </summary>
        Timecode,

        /// <summary>
        /// the Scene
        /// </summary>
        Scene,

        /// <summary>
        /// The camera
        /// </summary>
        Camera,

        /// <summary>
        /// The camera
        /// </summary>
        Take,

        /// <summary>
        /// The description
        /// </summary>
        Description,

        /// <summary>
        /// The comments
        /// </summary>
        Comments,

        /// <summary>
        /// The SOP-SAT
        /// </summary>
        [Description("SOP-SAT")]
        SopSat,

        /// <summary>
        /// The tape name
        /// </summary>
        [Description("Tape")]
        TapeName,

        /// <summary>
        /// The reel
        /// </summary>
        Reel,
    }

    /// <summary>
    /// Still Store caption display
    /// </summary>
    public enum CaptionEnum
    {
        /// <summary>
        /// No caption
        /// </summary>
        [Description("None")]
        None = 0,

        /// <summary>
        /// Cam, Scene, Take
        /// </summary>
        [Description("Cam_Scene_Take")]
        CamSceneTake = 1,

        /// <summary>
        /// Scene, Camera, Take
        /// </summary>
        [Description("Scene_Take_Cam")]
        SceneTakeCam = 2,

        /// <summary>
        /// Source timecode
        /// </summary>
        [Description("Timecode")]
        Timecode = 3,

        /// <summary>
        /// user comment
        /// </summary>
        [Description("Comment")]
        Comment = 4,

        /// <summary>
        /// source image file name
        /// </summary>
        [Description("Filename")]
        Filename = 5,

        /// <summary>
        /// The episode number, scene, take
        /// </summary>
        [Description("Episode_Scene_Take")]
        EpisodeSceneTake = 6
    }

    /// <summary>
    /// Available Hash Type Algorithms
    /// </summary>
    public enum HashType
    {
        /// <summary>
        /// No hash
        /// </summary>
        [Description("None")]
        None = 0,

        /// <summary>
        /// Message Digest 5 (MD5)
        /// </summary>
        [Description("MD5")]
        Md5 = 1,

        /// <summary>
        /// Secure Hash Alorithm 1 (SHA1)
        /// </summary>
        [Ignore(true)]
        [Description("SHA1")]
        Sha1 = 2,

        /// <summary>
        /// Secure Hash Alorithm 256 (SHA256)
        /// </summary>
        [Ignore(true)]
        [Description("SHA256")]
        Sha256 = 3,

        /// <summary>
        /// Secure Hash Alorithm 384 (SHA384)
        /// </summary>
        [Ignore(true)]
        [Description("SHA384")]
        Sha384 = 4,

        /// <summary>
        /// sSecure Hash Alorithm 512 (SHA512)
        /// </summary>
        [Ignore(true)]
        [Description("SHA512")]
        Sha512 = 5,

        /// <summary>
        /// The xx hash.
        /// </summary>
        [Description("xxHash")]
        XxHash = 6,

        /// <summary>
        /// The MHL hash type.
        /// </summary>
        [Ignore(true)]
        Mhl = 7
    }

    /// <summary>
    /// Enum that defines the EDL report type.
    /// </summary>
    public enum EdlReportType
    {
        /// <summary>
        /// The video track 1 report type.
        /// </summary>
        [Description("V1")]
        VideoTrack1,

        /// <summary>
        /// The video track 2 report type.
        /// </summary>
        [Description("V2")]
        VideoTrack2,

        /// <summary>
        /// The checkerboard report type.
        /// </summary>
        [Description("V1 V2 Checkerboard")]
        Checkerboard
    }

    #endregion
}
