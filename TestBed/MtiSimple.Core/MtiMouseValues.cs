﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace MtiSimple.Core
{
    public class MtiMouseValues
    {
        public int R;
        public int G;
        public int B;
        public Point Position;

        public override string ToString()
        {
            return String.Format($"X:{Position.X:0000} Y:{Position.Y:0000}  |  R:{R:00000} G:{G:00000} B:{B:00000}");
        }
    }
}
