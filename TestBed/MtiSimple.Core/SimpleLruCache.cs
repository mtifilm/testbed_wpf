﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SimpleLruCache.cs" company="MTI Film LLC">
// Copyright (c) 2011 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>Wasabe-XL\mbraca</author>
// <date>11/19/2011 7:03:42 AM</date>
// <summary>Implements a simple thread-safe LRU cache</summary>
// --------------------------------------------------------------------------------------------------------------------

using MtiSimple.Core;

namespace MtiSimple.Core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    /// <summary>
    /// Implements a simple thread-safe LRU cache.
    /// </summary>
    /// <typeparam name="TKeyType">The type of the key type.</typeparam>
    /// <typeparam name="TValueType">The type of the value type.</typeparam>
    public class SimpleLruCache<TKeyType, TValueType> where TValueType : class
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// If the maximum number of entries is set to this, there is no maximum
        /// </summary>
        public const int NoMax = -1;

        /// <summary>
        /// The thread lock.
        /// </summary>
        private readonly FairSemaphore cacheLock = new FairSemaphore();

        /// <summary>
        /// THe cache entries.
        /// </summary>
        private readonly Dictionary<TKeyType, TValueType> cacheEntries;

        /// <summary>
        /// The age sequence, oldest to newest.
        /// </summary>
        private readonly LinkedList<TKeyType> ageSequence;

        /// <summary>
        /// THe maximum number of cache entries allowed.
        /// </summary>
        private readonly int maxNumberOfEntries;

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the SimpleLruCache class
        /// </summary>
        /// <param name="maxNumberOfEntries">The max number of entries.</param>
        public SimpleLruCache(int maxNumberOfEntries = NoMax)
        {
            this.maxNumberOfEntries = maxNumberOfEntries;
            this.cacheEntries = new Dictionary<TKeyType, TValueType>();
            this.ageSequence = new LinkedList<TKeyType>();
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets all of the keys.
        /// </summary>
        public List<TKeyType> Keys
        {
            get
            {
                this.cacheLock.Acquire();
                try
                {
                    return this.cacheEntries.Keys.ToList();
                }
                finally
                {
                    this.cacheLock.Release();
                }
            }
        }

        /// <summary>
        /// Gets all of the values.
        /// </summary>
        public List<TValueType> Values
        {
            get
            {
                this.cacheLock.Acquire();
                try
                {
                    return this.cacheEntries.Values.ToList();
                }
                finally
                {
                    this.cacheLock.Release();
                }
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Adds the value for the specified key.
        /// </summary>
        /// <param name="key">The key to add.</param>
        /// <param name="value">The value. to add</param>
        public void Add(TKeyType key, TValueType value)
        {
            this.cacheLock.Acquire();
            try
            {
                this.cacheEntries[key] = value;
                if (this.maxNumberOfEntries != NoMax && this.cacheEntries.Count > this.maxNumberOfEntries)
                {
                    this.RemoveLruEntry();
                }

                this.TouchInternal(key);
            }
            finally
            {
                this.cacheLock.Release();
            }
        }

        /// <summary>
        /// Retrieves the value for specified key.
        /// </summary>
        /// <param name="key">The key to the entry to retrieve.</param>
        /// <returns>
        /// The retrieved value.
        /// </returns>
        public TValueType Retrieve(TKeyType key)
        {
            this.cacheLock.Acquire();
            try
            {
                if (!this.cacheEntries.ContainsKey(key))
                {
                    return null;
                }

                this.TouchInternal(key);
                return this.cacheEntries[key];
            }
            finally
            {
                this.cacheLock.Release();
            }
        }

        /// <summary>
        /// Removes the entry with the specified key.
        /// </summary>
        /// <param name="key">The key to remove.</param>
        public void Remove(TKeyType key)
        {
            this.cacheLock.Acquire();
            try
            {
                if (!this.cacheEntries.ContainsKey(key))
                {
                    return;
                }

                var entry = this.cacheEntries[key];
                this.cacheEntries.Remove(key);
                this.ageSequence.Remove(key);

                var disposableObject = entry as IDisposable;
                if (disposableObject != null)
                {
                    disposableObject.Dispose();
                }
            }
            finally
            {
                this.cacheLock.Release();
            }
        }

        /// <summary>
        /// Removes the first (oldest) entry.
        /// </summary>
        public void RemoveFirst()
        {
            this.cacheLock.Acquire();
            try
            {
                if (this.ageSequence.Count == 0)
                {
                    return;
                }

                var key = this.ageSequence.First.Value;
                var entry = this.cacheEntries[key];
                this.cacheEntries.Remove(key);
                this.ageSequence.Remove(key);

                var disposableObject = entry as IDisposable;
                if (disposableObject != null)
                {
                    disposableObject.Dispose();
                }
            }
            finally
            {
                this.cacheLock.Release();
            }
        }

        /// <summary>
        /// Retrieves the value for specified key and removes the entry from the cache.
        /// </summary>
        /// <param name="key">The key to the entry to retrieve and remove.</param>
        /// <returns>
        /// The retrieved value.
        /// </returns>
        public TValueType RetrieveAndRemove(TKeyType key)
        {
            this.cacheLock.Acquire();
            try
            {
                if (!this.cacheEntries.ContainsKey(key))
                {
                    return null;
                }

                var entry = this.cacheEntries[key];
                this.cacheEntries.Remove(key);
                this.ageSequence.Remove(key);

                return entry;
            }
            finally
            {
                this.cacheLock.Release();
            }
        }

        /// <summary>
        /// Clears the cache..
        /// </summary>
        public void Clear()
        {
            this.cacheLock.Acquire();
            try
            {
                foreach (var key in this.cacheEntries.Keys)
                {
                    TValueType value;
                    this.cacheEntries.TryGetValue(key, out value);

                    var disposableObject = value as IDisposable;
                    if (disposableObject.IsNotNull())
                    {
                        disposableObject.Dispose();
                    }
                }

                this.cacheEntries.Clear();
                this.ageSequence.Clear();
            }
            finally
            {
                this.cacheLock.Release();
            }
        }

        /// <summary>
        /// Touches the entry with the specified key.
        /// </summary>
        /// <param name="key">The key to touch.</param>
        public void Touch(TKeyType key)
        {
            this.cacheLock.Acquire();
            try
            {
                this.TouchInternal(key);
            }
            finally
            {
                this.cacheLock.Release();
            }
        }

        /// <summary>
        /// Determines whether the cache contains the specified key.
        /// </summary>
        /// <param name="key">The key to check for.</param>
        /// <returns>
        ///   <c>true</c> if the cache contains key; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsKey(TKeyType key)
        {
            this.cacheLock.Acquire();
            try
            {
                if (!this.cacheEntries.ContainsKey(key))
                {
                    return false;
                }

                this.TouchInternal(key);
                return true;
            }
            finally
            {
                this.cacheLock.Release();
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Touches the entry with the specified key.
        /// </summary>
        /// <param name="key">The key to touch.</param>
        private void TouchInternal(TKeyType key)
        {
            this.ageSequence.Remove(key);
            this.ageSequence.AddLast(key);
        }

        /// <summary>
        /// Removes the LRU entry.
        /// </summary>
        private void RemoveLruEntry()
        {
            var oldest = this.ageSequence.First;
            if (oldest != null)
            {
                var key = oldest.Value;

                if (Extensions.ShouldWriteDebugLogMessage(1))
                {
                    TraceEventType.Information.LogDebugMessage(1, $"LRU CACHE booted oldest item {oldest.Value}");
                }

                this.ageSequence.RemoveFirst();

                if (!this.cacheEntries.ContainsKey(key))
                {
                    throw new NullReferenceException("LRU cache is broken");
                }

                var objectRemoved = this.cacheEntries[key];

                this.cacheEntries.Remove(key);

                // If the cached object was an IDisposable, call its Disposed() method before releasing it.
                var disposableObject = objectRemoved as IDisposable;
                if (disposableObject != null)
                {
                    disposableObject.Dispose();
                }
            }
        }

        #endregion
    }
}
