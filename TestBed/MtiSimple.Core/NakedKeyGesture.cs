﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NakedKeyGesture.cs" company="MTI Film LLC">
// Copyright (c) 2011 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>Wasabe-XL\mbraca</author>
// <date>11/1/2011 6:08:04 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiSimple.Core
{
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Implements a class that extends gestures to allow unmodified keys.
    /// </summary>
    public class NakedKeyGesture : KeyGesture
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the NakedKeyGesture class;  to work around restriction of parent constructor
        /// (it throws an exception for modifiers == ModifierKeys.None)  we pretend that the modifier is tha Alt key,
        /// but in the Matches override function we will actually look for None.
        /// </summary>
        /// <param name="key">The key.</param>
        public NakedKeyGesture(Key key)
            : base(key, ModifierKeys.Alt)
        {
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Determines whether this <see cref="T:System.Windows.Input.KeyGesture"/> matches the input associated with the specified <see cref="T:System.Windows.Input.InputEventArgs"/> object.
        /// </summary>
        /// <param name="targetElement">The target.</param>
        /// <param name="inputEventArgs">The input event data to compare this gesture to.</param>
        /// <returns>
        /// true if the event data matches this <see cref="T:System.Windows.Input.KeyGesture"/>; otherwise, false.
        /// </returns>
        public override bool Matches(object targetElement, InputEventArgs inputEventArgs)
        {
            var keyEventArgs = inputEventArgs as KeyEventArgs;

            if (keyEventArgs == null)
            {
                return false;
            }

            // Don't steal naked keys from text boxes. There's probably a few other controls we need to screen here!!
            if (keyEventArgs.KeyboardDevice.FocusedElement is TextBox || keyEventArgs.KeyboardDevice.FocusedElement is RichTextBox)
            {
                return false;
            }

            return keyEventArgs.Key == this.Key && keyEventArgs.KeyboardDevice.Modifiers == ModifierKeys.None;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return (int)this.Key;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
