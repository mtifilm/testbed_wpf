﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Globalization;
using System.Windows.Data;

namespace MtiSimple.Core
{ 
    public class SimpleTimecodeToStringConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////

        //////////////////////////////////////////////////
        // IValueConverter Members
        //////////////////////////////////////////////////
        #region IValueConverter Members

        /// <summary>
        /// Converts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>The appropriate string.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var timecode = value as SimpleTimecode;
            return timecode?.ToString();
        }

        /// <summary>
        /// Converts the back.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>An Exception.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var timecodeString = value as string;
            return (timecodeString == null) ? null : new SimpleTimecode(timecodeString);
        }

        #endregion
    }
}
