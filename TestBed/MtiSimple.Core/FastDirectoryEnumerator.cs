﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FastDirectoryEnumerator.cs" company="MTI Film LLC">
// Copyright (c) 2016 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>11/14/2016 9:59:57 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiSimple.Core
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Static class that defines the fast directory enumerator.
    /// </summary>
    public static class FastDirectoryEnumerator
    {
        /////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The directory cache.
        /// </summary>
        private static readonly SimpleLruCache<string, List<string>> DirectoryCache;

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes static members of the <see cref="FastDirectoryEnumerator"/> class.
        /// </summary>
        static FastDirectoryEnumerator()
        {
            DirectoryCache = new SimpleLruCache<string, List<string>>(10);
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Enumerates the files.
        /// </summary>
        /// <param name="directoryInfo">The directory information.</param>
        /// <returns>An enumerable of file infos.</returns>
        public static IEnumerable<FileInfo> EnumerateFiles(DirectoryInfo directoryInfo)
        {
            if (DirectoryCache.ContainsKey(directoryInfo.FullName))
            {
                DirectoryCache.Remove(directoryInfo.FullName);
            }

            var newFileList = new List<string>();
            foreach (var fileInfo in directoryInfo.EnumerateFiles())
            {
                newFileList.Add(fileInfo.FullName);
                yield return fileInfo;
            }

            DirectoryCache.Add(directoryInfo.FullName, newFileList);
        }

        /// <summary>
        /// Enumerates the files.
        /// </summary>
        /// <param name="directoryName">Name of the directory.</param>
        /// <param name="searchPattern">The search pattern.</param>
        /// <returns>An enumerable of files.</returns>
        public static IEnumerable<string> EnumerateFiles(string directoryName, string searchPattern)
        {
            var regex = new Regex(searchPattern);
            if (DirectoryCache.ContainsKey(directoryName))
            {
                var fileList = DirectoryCache.Retrieve(directoryName);
                foreach (var file in fileList)
                {
                    var fileNameOnly = Path.GetFileName(file);
                    if (string.IsNullOrWhiteSpace(fileNameOnly) || !regex.IsMatch(fileNameOnly))
                    {
                        continue;
                    }

                    yield return file;
                }

                yield break;
            }

            var newFileList = new List<string>();
            foreach (var file in Directory.EnumerateFiles(directoryName))
            {
                newFileList.Add(file);
                var fileNameOnly = Path.GetFileName(file);
                if (string.IsNullOrWhiteSpace(fileNameOnly) || !regex.IsMatch(fileNameOnly))
                {
                    continue;
                }

                yield return file;
            }

            DirectoryCache.Add(directoryName, newFileList);
        }

        /// <summary>
        /// Adds the directory to cache.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <param name="fileList">The file list.</param>
        public static void AddDirectoryToCache(string directoryPath, List<string> fileList)
        {
            if (DirectoryCache.ContainsKey(directoryPath))
            {
                DirectoryCache.Remove(directoryPath);
            }

            DirectoryCache.Add(directoryPath, fileList);
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
