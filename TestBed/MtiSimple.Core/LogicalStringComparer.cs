﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogicalStringComparer.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>rancor\pfirth</author>
// <date>6/11/2012 2:54:19 PM</date>
// <summary>Implements the StrCmpLogicalW function.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;

    /// <summary>
    /// Class that implements the StrCmpLogicalW comparison
    /// function.
    /// </summary>
    public class LogicalStringComparer : IComparer<string>
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the LogicalStringComparer class
        /// </summary>
        public LogicalStringComparer()
        {
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Compares the specified x.
        /// </summary>
        /// <param name="x">The string x.</param>
        /// <param name="y">The string y.</param>
        /// <returns>The comparison value.</returns>
        public int Compare(string x, string y)
        {
            return StrCmpLogicalW(x, y);
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// STRs the CMP logical W.
        /// </summary>
        /// <param name="x">The string x.</param>
        /// <param name="y">The string y.</param>
        /// <returns>The comparison value.</returns>
        [DllImport("shlwapi.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
        private static extern int StrCmpLogicalW(string x, string y);

        #endregion
    }
}
