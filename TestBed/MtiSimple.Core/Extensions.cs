﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="MTI Films">
//   Copyright (c) 2008 All Rights Reserved by MTI Films
// </copyright>
// <author>Convey Team</author>
// <date>2008-05-24</date>
// --------------------------------------------------------------------------------------------------------------------

using System.Reactive.Linq;
using System.Windows.Threading;
using MtiFilm.Core;

namespace MtiSimple.Core
{
    #region

    using System;
    using System.Collections;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Numerics;
  //  using System.Reactive.Linq;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Security.Cryptography;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;

    #endregion

    /// <summary>
    /// This contains all the basic extensions
    /// </summary>
    public static class Extensions
    {
        #region Static Fields

        /// <summary>
        /// The identifier prefix.
        /// </summary>
        public const string IdPrefix = "urn:uuid:";

        /// <summary>
        /// The ul prefix.
        /// </summary>
        public const string UlPrefix = "urn:smpte:ul:";

        /// <summary>
        /// The cortex product identifier
        /// </summary>
        public static readonly Guid CortexProductId = new Guid("17d37fcc-0982-4c2e-86b1-6905039cbb0f");

        /// <summary>
        /// The queue of debug messages to be processed in the background.
        /// </summary>
        private static readonly BlockingCollection<string> DebugLogMessageQueue;

        /// <summary>
        /// Serialize the debug log messages.
        /// </summary>
        private static readonly FairSemaphore DebugLogSemaphore = new FairSemaphore();

        /// <summary>
        /// Cache for the MTI_DEBUG_TRACE_ENABLE  environment variable
        /// </summary>
        private static readonly int? DebugTraceLevel;

        /// <summary>
        /// Storage for bin root.
        /// </summary>
        private static string binRootCache;

        /// <summary>
        /// Storage for bin root forward slash.
        /// </summary>
        private static string binRootForwardSlashCache;

        /// <summary>
        /// Storage for local machine ini filename.
        /// </summary>
        private static string localMachineIniFileNameCache;

        /// <summary>
        /// The time of the previous debug log message, so we can highlight gaps.
        /// </summary>
        private static DateTime previousDebugLogMessageTime;

        /// <summary>
        /// Cache for the MTI_TRACE_NO_METHOD environment variable
        /// </summary>
        private static bool? traceNoMethod;

        /// <summary>
        /// This is storage for the trace source
        /// </summary>
        private static TraceSource traceSource;

#if DEBUG

        /// <summary>
        /// The extension cache
        /// </summary>
        private static string extensionCache;
#endif

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="Extensions"/> class.
        /// </summary>
        static Extensions()
        {
            // Get the debug logging level from the environment
            var debugTraceEnabledEnvVar = Environment.GetEnvironmentVariable("MTI_DEBUG_TRACE_LEVEL");
            if (debugTraceEnabledEnvVar.IsNotNull())
            {
                switch (debugTraceEnabledEnvVar)
                {
                    case "0":
                        DebugTraceLevel = 0;
                        break;
                    case "1":
                        DebugTraceLevel = 1;
                        break;
                    case "2":
                        DebugTraceLevel = 2;
                        break;
                    case "3":
                        DebugTraceLevel = 3;
                        break;
                    default:
                        DebugTraceLevel = 4;
                        break;
                }
            }

            // The queue for debug messages.
            DebugLogMessageQueue = new BlockingCollection<string>();

            // The background thread to process debug messages
            var processDebugLogMessages = new Action(
                () =>
                    {
                        Thread.CurrentThread.Name = "Debug Message Logger";
                        Thread.CurrentThread.Priority = ThreadPriority.Lowest;

                        foreach (var message in DebugLogMessageQueue.GetConsumingEnumerable())
                        {
                          // TODO:  LogMessage(TraceEventType.Information, message);
                        }
                    });

            Task.Factory.StartNew(processDebugLogMessages, TaskCreationOptions.LongRunning);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether this instance is debug mode (vs. release mode).
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is debug mode; otherwise, <c>false</c>.
        /// </value>
        public static bool IsDebugMode
        {
            get
            {
#if DEBUG
                return true;
#else
                return false;
#endif
            }
        }

        /// <summary>
        /// Gets the MD5 hash separator.
        /// </summary>
        public static string Md5HashSeparator
        {
            get
            {
                return @"johnmertuswonthespellingbee";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Determines whether or not the debug log message should be written.
        /// </summary>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <returns>
        /// True if debug log message should be written.
        /// </returns>
        public static bool ShouldWriteDebugLogMessage(int level)
        {
            return DebugTraceLevel.HasValue && level <= DebugTraceLevel.Value;
        }

        /// <summary>
        /// Swaps the specified LHS.
        /// </summary>
        /// <typeparam name="T">
        /// Any type
        /// </typeparam>
        /// <param name="left">
        /// The left hand side.
        /// </param>
        /// <param name="right">
        /// The right hand size.
        /// </param>
        public static void Swap<T>(ref T left, ref T right)
        {
            T temp = left;
            left = right;
            right = temp;
        }


        /// <summary>
        /// Generate a total from a seed, a function applied to all but last, and the last element is a special function
        /// E.g., Aggregate("", (t,c) =&gt; t + c.ToDescription() + ",", (t,c)=&gt;t + c.ToDescription()
        /// </summary>
        /// <typeparam name="TSource">
        /// Source and return type
        /// </typeparam>
        /// <typeparam name="TAccumulate">
        /// The type to add
        /// </typeparam>
        /// <param name="source">
        /// the source sequence
        /// </param>
        /// <param name="seed">
        /// where to start
        /// </param>
        /// <param name="func">
        /// function applied to all but last element
        /// </param>
        /// <param name="funcLast">
        /// function applied to last element
        /// </param>
        /// <returns>
        /// The total of all functions
        /// </returns>
        public static TAccumulate Aggregate<TSource, TAccumulate>(
            this IEnumerable<TSource> source,
            TAccumulate seed,
            Func<TAccumulate, TSource, TAccumulate> func,
            Func<TAccumulate, TSource, TAccumulate> funcLast)
        {
            var total = seed;
            var leftEnumerator = source.GetEnumerator();

            // throw on a blank sequence
            if (!leftEnumerator.MoveNext())
            {
                throw new ArgumentOutOfRangeException("source", "Source cannot be empty");
            }

            // Save it
            var c = leftEnumerator.Current;

            // Loop applying func to all but last
            while (leftEnumerator.MoveNext())
            {
                var temp = total;
                total = func(temp, c);
                c = leftEnumerator.Current;
            }

            // Now do the last one;
            return funcLast(total, c);
        }

        /// <summary>
        /// Convert a big-endian 4 byte buffer to a float.  System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="startIndex">
        /// The start index, incremented by 4 bytes when done.
        /// </param>
        /// <returns>
        /// the floating point value
        /// </returns>
        public static float BytesToFloat(byte[] value, ref int startIndex)
        {
            SFourBytes s;
            s.Float0 = 0;
            if (startIndex > value.Length - 4)
            {
                return float.NaN;
            }

            s.Byte3 = value[startIndex++];
            s.Byte2 = value[startIndex++];
            s.Byte1 = value[startIndex++];
            s.Byte0 = value[startIndex++];
            return s.Float0;
        }

        /// <summary>
        /// Returns true if the number of FP numbers between arguments is within precision
        /// </summary>
        /// <param name="firstFp">
        /// The first fp number to test
        /// </param>
        /// <param name="secondFp">
        /// The second fp number
        /// </param>
        /// <param name="precision">
        /// The precision measured in fp numbers
        /// </param>
        /// <returns>
        /// <c>true</c> if within precision, <c>false</c> otherwise
        /// </returns>
        public static bool EqualsWithinPrecision(this float firstFp, float secondFp, int precision = 1)
        {
            // Make sure precision width is non-negative and small enough that the
            // default NAN won't compare as equal to anything.
            Debug.Assert((precision >= 0) && (precision < 4 * 1024 * 1024), "Precision is too large");

            // Find the bits in fp1
            var i1 = BitConverter.ToInt32(BitConverter.GetBytes(firstFp), 0);

            // Make i1 lexicographically ordered as a 2-complement integer
            // Required if fp1 and fp2 switch signs but are very close
            if (i1 < 0)
            {
                i1 = (int)(0x80000000 - (uint)i1);
            }

            // Make i2 lexicographically ordered as a 2-complement integer
            // Required if fp1 and fp2 switch signs but are very close
            var i2 = BitConverter.ToInt32(BitConverter.GetBytes(secondFp), 0);
            if (i2 < 0)
            {
                i2 = (int)(0x80000000 - (uint)i2);
            }

            // Return if the lowest bits are equal
            return Math.Abs(i1 - i2) <= precision;
        }

        /// <summary>
        /// Returns true if the number of FP numbers between arguments is within precision
        /// </summary>
        /// <param name="firstFp">
        /// The first fp number to test
        /// </param>
        /// <param name="secondFp">
        /// The second fp number
        /// </param>
        /// <param name="precision">
        /// The precision measured in fp numbers
        /// </param>
        /// <returns>
        /// <c>true</c> if within precision, <c>false</c> otherwise
        /// </returns>
        public static bool EqualsWithinPrecision(this double firstFp, double secondFp, int precision = 1)
        {
            // Make sure precision width is non-negative and small enough that the
            // default NAN won't compare as equal to anything.
            Debug.Assert((precision >= 0) && (precision < 4 * 1024 * 1024), "Precision is too large");

            // Find the bits in fp1
            var i1 = BitConverter.ToInt64(BitConverter.GetBytes(firstFp), 0);

            // Make i1 lexicographically ordered as a 2-complement long
            // Required if fp1 and fp2 switch signs but are very close
            if (i1 < 0)
            {
                i1 = (long)(0x8000000000000000 - (ulong)i1);
            }

            // Find eth bits in fp2
            var i2 = BitConverter.ToInt64(BitConverter.GetBytes(secondFp), 0);

            // Make i2 lexicographically ordered as a 2-complement long
            // Required if fp1 and fp2 switch signs but are very close
            if (i2 < 0)
            {
                i2 = (long)(0x8000000000000000 - (ulong)i2);
            }

            // Return if the lowest bits are equal
            var result = i1 - i2;
            return Math.Abs(result) <= precision;
        }

        /// <summary>
        /// Extension to float that converts to long within precision.
        /// </summary>
        /// <param name="fp">
        /// The double value to convert to long.
        /// </param>
        /// <param name="precision">
        /// The precision.
        /// </param>
        /// <returns>
        /// The resulting long value.
        /// </returns>
        public static long ToLongWithinPrecision(this float fp, int precision = 1)
        {
            var afp = Math.Abs(fp);
            var result = (long)afp;
            if (afp.EqualsWithinPrecision(result + 1, precision))
            {
                ++result;
            }

            if (fp < 0.0f)
            {
                result = -result;
            }

            return result;
        }

        /// <summary>
        /// Extension to double that converts to long within precision.
        /// </summary>
        /// <param name="fp">
        /// The double value to convert to long.
        /// </param>
        /// <param name="precision">
        /// The precision.
        /// </param>
        /// <returns>
        /// The resulting long value.
        /// </returns>
        public static long ToLongWithinPrecision(this double fp, int precision = 1)
        {
            var afp = Math.Abs(fp);
            var result = (long)afp;
            if (afp.EqualsWithinPrecision(result + 1, precision))
            {
                ++result;
            }

            if (fp < 0.0)
            {
                result = -result;
            }

            return result;
        }

        /// <summary>
        /// Convert a big-endian 2 byte buffer to a int16.  System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="startIndex">
        /// The start index, incremented by 4 bytes when done.
        /// </param>
        /// <returns>
        /// the int32 value
        /// </returns>
        public static short BytesToInt16(byte[] value, ref int startIndex)
        {
            STwoBytes s;
            s.Int0 = 0;
            if (startIndex > value.Length - 4)
            {
                return 0;
            }

            s.Byte1 = value[startIndex++];
            s.Byte0 = value[startIndex++];
            return s.Int0;
        }

        /// <summary>
        /// Convert a big-endian 4 byte buffer to a int32.  System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="startIndex">
        /// The start index, incremented by 4 bytes when done.
        /// </param>
        /// <returns>
        /// the int32 value
        /// </returns>
        public static int BytesToInt32(byte[] value, ref int startIndex)
        {
            SFourBytes s;
            s.Int0 = 0;
            if (startIndex > value.Length - 4)
            {
                return 0;
            }

            s.Byte3 = value[startIndex++];
            s.Byte2 = value[startIndex++];
            s.Byte1 = value[startIndex++];
            s.Byte0 = value[startIndex++];
            return s.Int0;
        }

        /// <summary>
        /// Convert a big-endian 2 byte buffer to a uint16.  System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="startIndex">
        /// The start index, incremented by 4 bytes when done.
        /// </param>
        /// <returns>
        /// the uint32 value
        /// </returns>
        public static ushort BytesToUint16(byte[] value, ref int startIndex)
        {
            STwoBytes s;
            s.Uint0 = 0;
            if (startIndex > value.Length - 4)
            {
                return 0;
            }

            s.Byte1 = value[startIndex++];
            s.Byte0 = value[startIndex++];
            return s.Uint0;
        }

        /// <summary>
        /// Convert a big-endian 4 byte buffer to a uint32.  System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="startIndex">
        /// The start index, incremented by 4 bytes when done.
        /// </param>
        /// <returns>
        /// the uint32 value
        /// </returns>
        public static uint BytesToUint32(byte[] value, ref int startIndex)
        {
            SFourBytes s;
            s.Uint0 = 0;
            if (startIndex > value.Length - 4)
            {
                return 0;
            }

            s.Byte3 = value[startIndex++];
            s.Byte2 = value[startIndex++];
            s.Byte1 = value[startIndex++];
            s.Byte0 = value[startIndex++];
            return s.Uint0;
        }

        /// <summary>
        /// Add spaces to separate the capitalized words in the string,
        /// i.e. insert a space before each uppercase letter that is
        /// either preceded by a lowercase letter or followed by a
        /// lowercase letter (but not for the first char in string).
        /// </summary>
        /// <param name="inputEnum">
        /// A string in PascalCase
        /// </param>
        /// <returns>
        /// A string broken apart into words
        /// </returns>
        public static string CamelCaseToWords(this Enum inputEnum)
        {
            return CamelCaseToWords(inputEnum.ToString());
        }

        /// <summary>
        /// Add spaces to separate the capitalized words in the string,
        /// i.e. insert a space before each uppercase letter that is
        /// either preceded by a lowercase letter or followed by a
        /// lowercase letter (but not for the first char in string).
        /// </summary>
        /// <param name="value">
        /// A string in PascalCase
        /// </param>
        /// <returns>
        /// A string broken apart into words
        /// </returns>
        public static string CamelCaseToWords(this string value)
        {
            if (value == null)
            {
                return null;
            }

            var r = new Regex("(?<=[a-z])(?<x>[A-Z])|(?<=.)(?<x>[A-Z])(?=[a-z])");
            return r.Replace(value, " ${x}");
        }

        // ReSharper disable InconsistentNaming

        /// <summary>
        /// Parses doubles from a string
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="expectedCount">The expected count.</param>
        /// <param name="doubles">The doubles.</param>
        /// <returns>
        /// true if successful
        /// </returns>
        public static bool TryParseDoubles(this string source, int expectedCount, out double[] doubles)
        {
            if (source.IsNullOrEmpty())
            {
                doubles = new double[0];
                return false;
            }

            var values = source.Split(',', ' ').Where(v => !v.IsNullOrEmpty()).ToArray();
            if (values.Length < expectedCount)
            {
                doubles = new double[0];
                return false;
            }

            var retVal = new double[expectedCount];
            for (var i = 0; i < expectedCount; i++)
            {
                if (!double.TryParse(values[i], NumberStyles.Any, CultureInfo.InvariantCulture, out var d))
                {
                    doubles = new double[0];
                    return false;
                }

                retVal[i] = d;
            }

            doubles = retVal;
            return true;
        }

        /// <summary>
        /// Creates the MD5 hash of a float array
        /// </summary>
        /// <param name="source">
        /// The source array
        /// </param>
        /// <returns>
        /// 32 character string that represents the MD5 hasth
        /// </returns>
        public static string ComputeMD5Hash(this float[] source)
        {
            var bytePcmData = new byte[source.Length * sizeof(float)];
            Buffer.BlockCopy(source, 0, bytePcmData, 0, bytePcmData.Length);
            return bytePcmData.ComputeMD5Hash();
        }

        /// <summary>
        /// Creates the MD5 hash of a integer array
        /// </summary>
        /// <param name="source">The source array</param>
        /// <returns>32 character string that represents the MD5 hasth</returns>
        public static string ComputeMD5Hash(this int[] source)
        {
            var bytePcmData = new byte[source.Length * sizeof(int)];
            Buffer.BlockCopy(source, 0, bytePcmData, 0, bytePcmData.Length);
            return bytePcmData.ComputeMD5Hash();
        }

        /// <summary>
        /// Creates the MD5 hash of a short array
        /// </summary>
        /// <param name="source">
        /// The source array
        /// </param>
        /// <returns>
        /// 32 character string that represents the MD5 hasth
        /// </returns>
        public static string ComputeMD5Hash(this short[] source)
        {
            var bytePcmData = new byte[source.Length * sizeof(short)];
            Buffer.BlockCopy(source, 0, bytePcmData, 0, bytePcmData.Length);
            return bytePcmData.ComputeMD5Hash();
        }

        /// <summary>
        /// Creates the MD5 hash of a short array
        /// </summary>
        /// <param name="source">
        /// The source array
        /// </param>
        /// <returns>
        /// 32 character string that represents the MD5 hasth
        /// </returns>
        public static string ComputeMD5Hash(this ushort[] source)
        {
            var bytePcmData = new byte[source.Length * sizeof(ushort)];
            Buffer.BlockCopy(source, 0, bytePcmData, 0, bytePcmData.Length);
            return bytePcmData.ComputeMD5Hash();
        }

        /// <summary>
        /// Returns a hex string of the MD5 hash
        /// </summary>
        /// <param name="hashData">
        /// a byte array
        /// </param>
        /// <returns>
        /// 32 character string
        /// </returns>
        public static string ComputeMD5Hash(this byte[] hashData)
        {
            return ComputeMD5Hashes(hashData);
        }

        /// <summary>
        /// Computes the MD5 hash.
        /// </summary>
        /// <param name="hashData">
        /// The hash data.
        /// </param>
        /// <returns>
        /// the 32 character hash string
        /// </returns>
        public static string ComputeMD5Hash(this string hashData)
        {
            return Encoding.ASCII.GetBytes(hashData).ComputeMD5Hash();
        }

        /// <summary>
        /// Returns a hex string of the MD5 hash
        /// </summary>
        /// <param name="hashData">
        /// a list of byte arrays
        /// </param>
        /// <returns>
        /// 32 character string
        /// </returns>
        public static string ComputeMD5HashValue(this byte[][] hashData)
        {
            return ComputeMD5Hashes(hashData);
        }

        /// <summary>
        /// Returns a hex string of the MD5 hash
        /// </summary>
        /// <param name="hashData">
        /// a list of byte arrays
        /// </param>
        /// <returns>
        /// 32 character string
        /// </returns>
        public static string ComputeMD5Hashes(params byte[][] hashData)
        {
            var md5 = MD5.Create();
            foreach (var dataBuffer in hashData)
            {
                md5.TransformBlock(dataBuffer, 0, dataBuffer.Length, null, 0);
            }

            // Finish it up
            md5.TransformFinalBlock(hashData[0], 0, 0);

            return md5.Hash.ToHex();
        }

        /// <summary>
        /// Computes the SHA-1 hash.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileSize">Size of the file.</param>
        /// <param name="bufferSize">Size of the buffer.</param>
        /// <param name="progress">The progress.</param>
        /// <returns>
        /// The SHA-1 hash.
        /// </returns>
        public static byte[] ComputeSha1Hash(string fileName, long fileSize, int bufferSize, IProgress<long> progress)
        {
            var readQueue = new BlockingCollection<byte[]>(100);
            var readTask = Task.Run(
                () =>
                    {
                        using (var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                        {
                            var bytesRead = 0L;
                            while (bytesRead < fileSize)
                            {
                                var buffer = new byte[bufferSize];
                                var read = fileStream.Read(buffer, 0, bufferSize);
                                bytesRead += read;
                                if (bytesRead == fileSize)
                                {
                                    var tail = new byte[read];
                                    Array.Copy(buffer, tail, read);
                                    readQueue.Add(tail);
                                }
                                else
                                {
                                    readQueue.Add(buffer);
                                }
                            }
                        }

                        readQueue.CompleteAdding();
                    });

            var hashTask = Task.Run(
                () =>
                    {
                        var hasher = SHA1.Create();
                        var bytesHashed = 0L;
                        while (bytesHashed < fileSize)
                        {
                            try
                            {
                                var buffer = readQueue.Take();
                                hasher.TransformBlock(buffer, 0, buffer.Length, null, 0);
                                bytesHashed += buffer.Length;
                                if (progress != null)
                                {
                                    progress.Report(buffer.Length);
                                }
                            }
                            catch (InvalidOperationException)
                            {
                                break;
                            }
                        }

                        hasher.TransformFinalBlock(new byte[0], 0, 0);
                        return hasher.Hash;
                    });

            readTask.Wait();
            hashTask.Wait();
            return hashTask.Result;
        }

        /// <summary>
        /// Converts the frames to TC24.
        /// </summary>
        /// <param name="frames">
        /// The frames.
        /// </param>
        /// <param name="roundUpToNearestSecond">
        /// if set to <c>true</c> [round up to nearest second].
        /// </param>
        /// <returns>
        /// The timecode as a string.
        /// </returns>
        public static string ConvertFramesToTC24(int frames, bool roundUpToNearestSecond)
        {
            return ConvertFramesToTimecode(frames, 24, roundUpToNearestSecond);
        }

        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Converts the frames to timecode.
        /// </summary>
        /// <param name="frames">
        /// The frames.
        /// </param>
        /// <param name="frameRate">
        /// The frame rate.
        /// </param>
        /// <param name="roundUpToNearestSecond">
        /// if set to <c>true</c> [round up to nearest second].
        /// </param>
        /// <param name="dropFrame">
        /// if set to <c>true</c> [drop frame].
        /// </param>
        /// <returns>
        /// The timecode as a string.
        /// </returns>
        public static string ConvertFramesToTimecode(
            int frames, int frameRate, bool roundUpToNearestSecond, bool dropFrame = false)
        {
            // Return bad value if called with frameRate of 0 instead of throwing a DivideByZero exception.
            if (frameRate == 0)
            {
                return "99:99:99:99";
            }

            var floatFrameRate = Convert.ToSingle(frameRate, CultureInfo.InvariantCulture);
            if (dropFrame)
            {
                var framesDiv = frames / 17982;
                var framesMod = frames % 17982;
                frames += (18 * framesDiv) + (2 * ((framesMod - 2) / 1798));
            }

            var timecodeFrames = frames % frameRate;
            var seconds = (int)(frames / floatFrameRate) % 60;
            var minutes = (int)((frames / floatFrameRate) / 60.0) % 60;
            var hours = (int)(((frames / floatFrameRate) / 60.0) / 60.0) % frameRate;

            if (roundUpToNearestSecond)
            {
                if (timecodeFrames > 0)
                {
                    seconds = (seconds + 1) % 60;
                    if (seconds == 0)
                    {
                        minutes = (minutes + 1) % 60;
                        if (minutes == 0)
                        {
                            hours = (hours + 1) % frameRate;
                        }
                    }
                }

                return string.Format("{0:D2}:{1:D2}:{2:D2}", hours, minutes, seconds);
            }

            return string.Format(
                dropFrame ? "{0:D2}:{1:D2}:{2:D2};{3:D2}" : "{0:D2}:{1:D2}:{2:D2}:{3:D2}",
                hours,
                minutes,
                seconds,
                timecodeFrames);
        }

        /// <summary>
        /// Copies the specified DLL to the Test Directory specified - but only if it's newer than the other file.
        /// </summary>
        /// <param name="dllName">
        /// The dll Name.
        /// </param>
        /// <param name="testDir">
        /// The test Dir.
        /// </param>
        public static void CopyDllToTestDirectory(string dllName, string testDir)
        {
            var sourceFile = GetSourceDllFullPath(dllName);
            var destinationFile = Path.Combine(testDir, dllName);

            if (!File.Exists(destinationFile) || IsFirstFileNewer(sourceFile, destinationFile))
            {
                File.Copy(sourceFile, destinationFile, true);
            }
        }

        /// <summary>
        /// Converts dB values to power factors
        /// </summary>
        /// <param name="decibles">
        /// the input dB values
        /// </param>
        /// <returns>
        /// an enumeration of scale values
        /// </returns>
        public static IEnumerable<float> DecibelToPower(this IEnumerable<float> decibles)
        {
            return from c in decibles select (c <= (float)decimal.MinValue) ? 0 : (float)Math.Pow(10, c / 10.0);
        }

        /// <summary>
        /// Converts dB values to scale factors
        /// </summary>
        /// <param name="decibles">
        /// the input dB values
        /// </param>
        /// <returns>
        /// an enumeration of scale values
        /// </returns>
        public static IEnumerable<float> DecibelToScale(this IEnumerable<float> decibles)
        {
            return from c in decibles select (c <= (float)decimal.MinValue) ? 0 : (float)Math.Pow(10, c / 20.0);
        }

        /// <summary>
        /// A replacement for Dispose that defers the Dispose until the thread is idle, for things that take a relatively long time to Dispose.
        /// </summary>
        /// <param name="objectToDispose">
        /// The object to dispose.
        /// </param>
        public static void DisposeSoon(this IDisposable objectToDispose)
        {
            if (objectToDispose == null)
            {
                return;
            }

            Dispatcher.CurrentDispatcher.BeginInvoke(
                DispatcherPriority.ContextIdle, new Action(objectToDispose.Dispose));
        }

        /// <summary>
        /// Extracts the specified value.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The extracted parts of the string.
        /// </returns>
        public static string[] Extract(string value)
        {
            // Remove any whitespace
            value = value.Replace(" ", string.Empty);

            var result = new string[3];
            var match = Regex.Match(value, @"([A-Z]*)(-?[\d]*)([A-Z]*)", RegexOptions.IgnoreCase);

            result[0] = match.Groups[1].Value.ToUpper();
            result[1] = match.Groups[2].Value;
            result[2] = match.Groups[3].Value.ToUpper();

            return result;
        }

        /// <summary>
        /// Place a float32 value into a big-endian byte array. System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="byteArray">
        /// The byte array.
        /// </param>
        /// <param name="startIndex">
        /// The starting index in the array.
        /// </param>
        public static void FloatToBytes(float value, byte[] byteArray, ref int startIndex)
        {
            SFourBytes s;
            s.Byte0 = 0;
            s.Byte1 = 0;
            s.Byte2 = 0;
            s.Byte3 = 0;
            if (startIndex > byteArray.Length - 4)
            {
                return;
            }

            s.Float0 = value;
            byteArray[startIndex++] = s.Byte3;
            byteArray[startIndex++] = s.Byte2;
            byteArray[startIndex++] = s.Byte1;
            byteArray[startIndex++] = s.Byte0;
        }

        // ReSharper disable InconsistentNaming

        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Gets the attribute from a field
        /// </summary>
        /// <typeparam name="T">
        /// What field to get type on
        /// </typeparam>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="name">
        /// The property name name.
        /// </param>
        /// <param name="attributeType">
        /// The attribute type.
        /// </param>
        /// <returns>
        /// Object that has this value or null
        /// </returns>
        public static object GetFieldAttribute<T>(this T value, string name, Type attributeType)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());
            var attributes = fieldInfo.GetCustomAttributes(attributeType, true);
            if (attributes.Length == 0)
            {
                throw new ArgumentException("No custom attribute specified", "attributeType");
            }

            var prop = attributeType.GetProperty(name);
            if (prop == null)
            {
                throw new ArgumentException("Invalid property for the custom attribute", "name");
            }

            return prop.GetValue(attributes[0], null);
        }


        /// <summary>
        /// Gets the innermost exception message text.
        /// </summary>
        /// <param name="ex">
        /// The exception.
        /// </param>
        /// <returns>
        /// A string representing the innermost exception text.
        /// </returns>
        public static string GetInnermostExceptionMessage(this Exception ex)
        {
            var innermostExceptionMessage = ex.Message;

            var innerException = ex.InnerException;
            while (innerException != null)
            {
                innermostExceptionMessage = innerException.Message;
                innerException = innerException.InnerException;
            }

            return innermostExceptionMessage;
        }

        /// <summary>
        /// Gets the innermost exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns>The innermost exception.</returns>
        public static Exception GetInnermostException(this Exception ex)
        {
            var innermostException = ex;
            var innerException = ex.InnerException;
            while (innerException != null)
            {
                innermostException = innerException;
                innerException = innerException.InnerException;
            }

            return innermostException;
        }

        /// <summary>
        /// Gets the logging directory.
        /// </summary>
        /// <returns>The Logging Directory.</returns>
        public static string GetLoggingDirectory()
        {
            var loggingDir = Environment.GetEnvironmentVariable("MTI_LOGGING_DIR") ?? @"C:\temp\";

            if (!loggingDir.EndsWith(@"\"))
            {
                loggingDir += @"\";
            }

            return loggingDir;
        }

        // ReSharper disable InconsistentNaming

        /// <summary>
        /// Gets the name of the MTI local machine ini file.
        /// </summary>
        /// <returns>Local machine ini file name.</returns>
        public static string GetMTILocalMachineIniFileName()
        {
            if (string.IsNullOrEmpty(localMachineIniFileNameCache))
            {
                var localConfigPath = Environment.GetEnvironmentVariable("CPMP_LOCAL_DIR", EnvironmentVariableTarget.Process) ?? string.Empty;
                localMachineIniFileNameCache = Path.Combine(localConfigPath, @"MTILocalMachine.ini");
            }

            return localMachineIniFileNameCache;
        }

        // ReSharper restore InconsistentNaming
        /// <summary>
        /// Gets the properly quoted process start info argument.
        /// </summary>
        /// <param name="processName">
        /// Name of the process.
        /// </param>
        /// <param name="argument">
        /// The argument.
        /// </param>
        /// <returns>
        /// The properly quoted string to pass as the ProcessStartInfo argument.
        /// </returns>
        public static string GetQuotedProcessStartInfoArgument(string processName, string argument)
        {
            // To properly handle arguments passed to "cmd.exe /c", the EXE and each argument which might have
            // spaces must be quoted and then the entire string must be quoted.
            processName = QuoteString(processName);
            argument = QuoteString(argument);

            var procPlusArgs = QuoteString(string.Format("{0} {1}", processName, argument));
            var processStartArgs = string.Format("/c {0}", procPlusArgs);

            return processStartArgs;
        }

        /// <summary>
        /// Gets the source DLL full path (including filename) for the current version of a DLL by searching the 3 possible directories.  The source directory
        /// will be different depending on whether this code is called from ReSharper Unit Testing or MSTest (from MSTest, it further depends on if being
        /// run from the GUI or via NCover.
        /// </summary>
        /// <param name="dllName">
        /// Base filename of the DLL.
        /// </param>
        /// <returns>
        /// Filename (including path) of the DLL.
        /// </returns>
        public static string GetSourceDllFullPath(string dllName)
        {
            var buildType = IsDebugMode ? "Debug" : "Release";

            var platform = IntPtr.Size == 8 ? "x64" : "x86";

            // For ReSharper & MSTest running under nCover.
            var directory1 = @"..\..\..\..\..\" + platform + @"\" + buildType;

            // For MSTest running in VS GUI.
            var directory2 = @"..\..\..\..\bin\" + platform + @"\" + buildType;

            // For MSTest running in nant
            var directory3 = @"..\..\bin\" + platform + @"\" + buildType;

            var list = new List<string> { Path.Combine(directory1, dllName), Path.Combine(directory2, dllName), Path.Combine(directory3, dllName) };

            var sourceFile = list.FirstOrDefault(File.Exists);
            if (sourceFile == null)
            {
                throw new IOException(
                   string.Format(
                       "Required Source DLL missing from\n'\t{0}'\n'\t{1}'\n'\t{2}'\n  CurrDir = '{3}'", list[0], list[1], list[2], Directory.GetCurrentDirectory()));
            }

            return sourceFile;
        }

        /// <summary>
        /// Gets the source DLL full path (including filename) for the specified platform of the IsLicense DLL (e.g. x64\IsLicense50.DLL) by searching the 3
        /// possible directories. The source directory will be different depending on whether this code is called from ReSharper Unit Testing or MSTest
        /// (from MSTest, it further depends on if being run from the GUI or via NCover.
        /// </summary>
        /// <param name="dllName">
        /// Base filename of the DLL.
        /// </param>
        /// <param name="platform">
        /// The platform - e.g. x86 or x64.
        /// </param>
        /// <returns>
        /// Filename (including path) of the DLL.
        /// </returns>
        public static string GetSourceDllFullPathIsLicense(string dllName, string platform)
        {
            var buildType = IsDebugMode ? "Debug" : "Release";

            // For ReSharper & MSTest running under nCover.
            var directory1 = @"..\..\..\..\..\" + platform + @"\" + buildType + @"\" + platform;

            // For MSTest running in VS GUI.
            var directory2 = @"..\..\..\..\bin\" + platform + @"\" + buildType + @"\" + platform;

            // For MSTest running in nant
            var directory3 = @"..\..\bin\" + platform + @"\" + buildType + @"\" + platform;

            var list = new List<string>();
            list.Add(Path.Combine(directory1, dllName));
            list.Add(Path.Combine(directory2, dllName));
            list.Add(Path.Combine(directory3, dllName));

            var sourceFile = list.FirstOrDefault(File.Exists);
            if (sourceFile == null)
            {
                throw new IOException(
                    string.Format(
                        "Required Source DLL missing from\n'\t{0}'\n'\t{1}'\n'\t{2}'\n  CurrDir={3}", list[0], list[1], list[2], Directory.GetCurrentDirectory()));
            }

            return sourceFile;
        }

        /// <summary>
        /// Gets the source DLL full path (including filename) for the x86 version of a DLL by searching the 3 possible directories.  The source directory
        /// will be different depending on whether this code is called from ReSharper Unit Testing or MSTest (from MSTest, it further depends on if being
        /// run from the GUI or via NCover.
        /// </summary>
        /// <param name="dllName">
        /// Base filename of the DLL.
        /// </param>
        /// <returns>
        /// Filename (including path) of the DLL.
        /// </returns>
        public static string GetSourceDllFullPathX86(string dllName)
        {
            var buildType = IsDebugMode ? "Debug" : "Release";

            // For ReSharper & MSTest running under nCover.
            var directory1 = @"..\..\..\..\..\x86\" + buildType;

            // For MSTest running in VS GUI.
            var directory2 = @"..\..\..\..\bin\x86\" + buildType;

            // For MSTest running in nant
            var directory3 = @"..\..\bin\x86\" + buildType;

            var list = new List<string>();
            list.Add(Path.Combine(directory1, dllName));
            list.Add(Path.Combine(directory2, dllName));
            list.Add(Path.Combine(directory3, dllName));

            var sourceFile = list.FirstOrDefault(File.Exists);
            if (sourceFile == null)
            {
                throw new IOException(
                    string.Format(
                        "Required Source DLL missing from\n'\t{0}'\n'\t{1}'\n'\t{2}'", list[0], list[1], list[2]));
            }

            return sourceFile;
        }

        /// <summary>
        /// Gets the type of the first element, throws InvalidOperationExecption if empty
        /// </summary>
        /// <param name="enumerable">
        /// the enumeration to check
        /// </param>
        /// <returns>
        /// The type of the first element
        /// </returns>
        public static Type GetTypeOfFirstElement(this IEnumerable enumerable)
        {
            // Get a row so we can use reflection to use its name
            var enumerate = enumerable.GetEnumerator();
            enumerate.MoveNext();

            // This will throw an execption if no current
            return enumerate.Current.GetType();
        }

        /// <summary>
        /// Gets the value from description.
        /// </summary>
        /// <typeparam name="T">
        /// An enum type.
        /// </typeparam>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <returns>
        /// An enum of type T.
        /// </returns>
        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum)
            {
                throw new InvalidOperationException("Type " + type + " is not an enum.");
            }

            foreach (var field in type.GetFields())
            {
                var attribute =
                    Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                    {
                        return (T)field.GetValue(null);
                    }
                }
                else
                {
                    if (field.Name == description)
                    {
                        return (T)field.GetValue(null);
                    }
                }
            }

            throw new ArgumentException("Not found.", "description");
        }

        /// <summary>
        /// Gets a list of values and descriptions.
        /// </summary>
        /// <param name="enumType">
        /// Type of the enum.
        /// </param>
        /// <returns>
        /// A list of key/value pairs with key the name of the enum and value the description
        /// </returns>
        public static List<KeyValuePair<string, int>> GetValuesAndDescription(Type enumType)
        {
            var pairList = new List<KeyValuePair<string, int>>();

            foreach (Enum enumValue in Enum.GetValues(enumType))
            {
                pairList.Add(new KeyValuePair<string, int>(ToDescription(enumValue), Convert.ToInt32(enumValue)));
            }

            return pairList;
        }

        /// <summary>
        /// Place a int16 value into a big-endian byte array. System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="byteArray">
        /// The byte array.
        /// </param>
        /// <param name="startIndex">
        /// The starting index in the array, incremented by 4 when done.
        /// </param>
        public static void Int16ToBytes(short value, byte[] byteArray, ref int startIndex)
        {
            STwoBytes s;
            s.Byte0 = 0;
            s.Byte1 = 0;
            if (startIndex > byteArray.Length - 2)
            {
                return;
            }

            s.Int0 = value;
            byteArray[startIndex++] = s.Byte1;
            byteArray[startIndex++] = s.Byte0;
        }

        /// <summary>
        /// Place a int32 value into a big-endian byte array. System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="byteArray">
        /// The byte array.
        /// </param>
        /// <param name="startIndex">
        /// The starting index in the array, incremented by 4 when done.
        /// </param>
        public static void Int32ToBytes(int value, byte[] byteArray, ref int startIndex)
        {
            SFourBytes s;
            s.Byte0 = 0;
            s.Byte1 = 0;
            s.Byte2 = 0;
            s.Byte3 = 0;
            if (startIndex > byteArray.Length - 4)
            {
                return;
            }

            s.Int0 = value;
            byteArray[startIndex++] = s.Byte3;
            byteArray[startIndex++] = s.Byte2;
            byteArray[startIndex++] = s.Byte1;
            byteArray[startIndex++] = s.Byte0;
        }

        /// <summary>
        /// Checks to see if an enumeration has no elements
        /// </summary>
        /// <param name="enumerable">
        /// the enumeration to check
        /// </param>
        /// <returns>
        /// true if rows contains no elements, false otherwise
        /// </returns>
        public static bool IsEmpty(this IEnumerable enumerable)
        {
            // Get a row so we can use reflection to use its name
            return !enumerable.GetEnumerator().MoveNext();
        }

        /// <summary>
        /// Determines whether [is null or empty] [the specified enumerable].
        /// </summary>
        /// <param name="enumerable">
        /// The enumerable.
        /// </param>
        /// <returns>
        /// <c>true</c> if [is null or empty] [the specified enumerable]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty(this IEnumerable enumerable)
        {
            return enumerable == null || enumerable.IsEmpty();
        }

        /// <summary>
        /// Determines whether the characters in the file name are valid file characters
        /// </summary>
        /// <param name="fileName">
        /// Name of the file.
        /// </param>
        /// <returns>
        /// <c>true</c> if is file name valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFileNameValid(string fileName)
        {
            var regex = new Regex("[" + Regex.Escape(new string(Path.GetInvalidFileNameChars())) + "]");
            return !regex.IsMatch(fileName);
        }

        /// <summary>
        /// Determines whether the first file is newer than the second file.
        /// </summary>
        /// <param name="firstFile">
        /// The first file.
        /// </param>
        /// <param name="secondFile">
        /// The second file.
        /// </param>
        /// <returns>
        /// <c>true</c> if [is first file newer] [the specified first file]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFirstFileNewer(string firstFile, string secondFile)
        {
            return File.GetLastWriteTime(firstFile) > File.GetLastWriteTime(secondFile);
        }

        /// <summary>
        /// Determines whether first value is within range of other 2 values.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="minimum">
        /// The minimum.
        /// </param>
        /// <param name="maximum">
        /// The maximum.
        /// </param>
        /// <returns>
        /// <c>true</c> if first value is within range of other 2 values otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInRange(this int value, int minimum, int maximum)
        {
            return value >= minimum && value <= maximum;
        }

        /// <summary>
        /// Determines whether first value is within range of other 2 values.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="minimum">
        /// The minimum.
        /// </param>
        /// <param name="maximum">
        /// The maximum.
        /// </param>
        /// <returns>
        /// <c>true</c> if first value is within range of other 2 values otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInRange(this double value, double minimum, double maximum)
        {
            return value >= minimum && value <= maximum;
        }

        /// <summary>
        /// See if an object is not null
        /// </summary>
        /// <param name="value">
        /// The value to check for null
        /// </param>
        /// <returns>
        /// <code>
        /// true
        /// </code>
        /// if value is not null
        /// <code>
        /// false
        /// </code>
        /// otherwise
        /// </returns>
        public static bool IsNotNull(this object value)
        {
            return value != null;
        }

        /// <summary>
        /// See if an object is null
        /// </summary>
        /// <param name="value">
        /// The value to check for null
        /// </param>
        /// <returns>
        /// <code>
        /// true
        /// </code>
        /// if value is null
        /// <code>
        /// false
        /// </code>
        /// otherwise
        /// </returns>
        public static bool IsNull(this object value)
        {
            return value == null;
        }

        /// <summary>
        /// Logs the debug message.
        /// </summary>
        /// <param name="eventType">
        /// Type of the event.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public static void LogDebugMessage(this TraceEventType eventType, int level, string message)
        {
            if (!ShouldWriteDebugLogMessage(level))
            {
                return;
            }

            DebugLogSemaphore.Acquire();
            try
            {
                var now = DateTime.Now;
                var elapsed = now - previousDebugLogMessageTime;
                var elapsedMilliseconds = elapsed.Duration().TotalMilliseconds;
                if (elapsedMilliseconds >= 10.0)
                {
                    var gapMessage =
                        string.Format(
                            "    ************ [{0:F3}]                                                   ",
                            elapsedMilliseconds / 1000.0);
                    DebugLogMessageQueue.Add(gapMessage);
                }

                previousDebugLogMessageTime = now;

                // Log the time because the trace message may not get written for a while,
                // so the auto-added time of day field is useless for debugging!!!
                // Throw the thread ID in as well
                var messageEx = string.Format(
                    @"{0:D3} {1:HH\:mm\:ss\.fff} {2}", Thread.CurrentThread.ManagedThreadId, DateTime.Now, message);

                // Messages are processed in the background.
                DebugLogMessageQueue.Add(messageEx);
            }
            finally
            {
                DebugLogSemaphore.Release();
            }
        }


        /// <summary>
        /// Returns the logging filename extension.
        /// </summary>
        /// <returns>Logging Extension.</returns>
        public static string LoggingExtension()
        {
#if DEBUG
            if (extensionCache.IsNullOrEmpty())
            {
                var now = DateTime.Now;
                extensionCache = string.Format(
                    "_{0}{1}{2}{3}{4}{5}.csv",
                    now.Year.ToString("D2"),
                    now.Month.ToString("D2"),
                    now.Day.ToString("D2"),
                    now.Hour.ToString("D2"),
                    now.Minute.ToString("D2"),
                    now.Second.ToString("D2"));
            }

            return extensionCache;
#else
            return ".csv";
#endif
        }

        /// <summary>
        /// Makes the comma delimated string surrounded by parenethsis
        /// </summary>
        /// <typeparam name="T">
        /// A parameter type
        /// </typeparam>
        /// <param name="enumerable">
        /// The enumerable.
        /// </param>
        /// <param name="includeParens">
        /// if set to <c>true</c> [include parens].
        /// </param>
        /// <returns>
        /// a comma delimited string; e.g., (1,2,3,4)
        /// </returns>
        public static string MakeCommaDelimitedString<T>(this IEnumerable<T> enumerable, bool includeParens = true)
        {
            var list = enumerable.ToList();
            if (list.IsEmpty())
            {
                return string.Empty;
            }

            if (includeParens)
            {
                return list.Aggregate("(", (a, c) => a + c.ToString() + ",", (a, c) => a + c.ToString() + ")");
            }

            return list.Aggregate(string.Empty, (a, c) => a + c.ToString() + ",", (a, c) => a + c.ToString());
        }

        /// <summary>
        /// Applies a function to each pairwise elements in two enumerable classes
        /// </summary>
        /// <typeparam name="T">
        /// Any reasonable type
        /// </typeparam>
        /// <param name="leftSequence">
        /// the source sequence
        /// </param>
        /// <param name="rightSequence">
        /// the sequence to merge from
        /// </param>
        /// <param name="mergeFunc">
        /// The function to apply to each pair
        /// </param>
        /// <returns>
        /// <code>
        /// IEnumerable
        /// </code>
        /// The merged function
        /// </returns>
        public static IEnumerable<T> Merge<T>(
            this IEnumerable<T> leftSequence, IEnumerable<T> rightSequence, Func<T, T, T> mergeFunc)
        {
            var leftEnumerator = leftSequence.GetEnumerator();
            var rightEnumerator = rightSequence.GetEnumerator();
            while (leftEnumerator.MoveNext() && rightEnumerator.MoveNext())
            {
                yield return mergeFunc(leftEnumerator.Current, rightEnumerator.Current);
            }
        }

        /// <summary>
        /// Applies a function to each pairwise elements in two enumerable classes
        /// </summary>
        /// <typeparam name="T">
        /// Any reasonable type
        /// </typeparam>
        /// <param name="leftSequence">
        /// the source sequence
        /// </param>
        /// <param name="rightSequence">
        /// the sequence to merge from
        /// </param>
        /// <param name="mergeFunc">
        /// The function to apply to each pair
        /// </param>
        /// <returns>
        /// <code>
        /// IEnumerable
        /// </code>
        /// The merged function
        /// </returns>
        public static IEnumerable<bool> Merge<T>(
            this IEnumerable<T> leftSequence, IEnumerable<T> rightSequence, Func<T, T, bool> mergeFunc)
        {
            var leftEnumerator = leftSequence.GetEnumerator();
            var rightEnumerator = rightSequence.GetEnumerator();
            while (leftEnumerator.MoveNext() && rightEnumerator.MoveNext())
            {
                yield return mergeFunc(leftEnumerator.Current, rightEnumerator.Current);
            }
        }

        /// <summary>
        /// Returns a KeyValuePair enumeration that has the key from the first sequence
        ///  and the value from the second
        /// </summary>
        /// <typeparam name="TLeft">
        /// Type of the keys
        /// </typeparam>
        /// <typeparam name="TRight">
        /// Type of the values
        /// </typeparam>
        /// <param name="leftSequence">
        /// Sequence of the keys
        /// </param>
        /// <param name="rightSequence">
        /// Sequence of the values
        /// </param>
        /// <param name="mergeFunc">
        /// The function that creates the pair from the inputs
        /// </param>
        /// <returns>
        /// A enumerable keyValuePair whose key is the first sequence and value is the second
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures",
            Justification = "Impossible to do cleanly without this, the use of var fixes the objection")]
        public static IEnumerable<KeyValuePair<TLeft, TRight>> Merge<TLeft, TRight>(
            this IEnumerable<TLeft> leftSequence,
            IEnumerable<TRight> rightSequence,
            Func<TLeft, TRight, KeyValuePair<TLeft, TRight>> mergeFunc)
        {
            var leftEnumerator = leftSequence.GetEnumerator();
            var rightEnumerator = rightSequence.GetEnumerator();
            while (leftEnumerator.MoveNext() && rightEnumerator.MoveNext())
            {
                yield return mergeFunc(leftEnumerator.Current, rightEnumerator.Current);
            }
        }

        /// <summary>
        /// Scale factors are similar to voltages ratios so we use 20*Log
        /// </summary>
        /// <param name="powers">
        /// the power factors to set
        /// </param>
        /// <returns>
        /// an enumeration of scale values
        /// </returns>
        public static IEnumerable<float> PowerToDecibel(this IEnumerable<float> powers)
        {
            return from c in powers select c.EqualsWithinPrecision(0f) ? -96 : (float)(10 * Math.Log10(Math.Abs(c)));
        }

        /// <summary>
        /// Puts quotes around an input string
        /// </summary>
        /// <param name="name">
        /// String to quote
        /// </param>
        /// <returns>
        /// A quoted string
        /// </returns>
        public static string QuoteString(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return "\"" + "\"";
            }

            name = name.Replace("/", @"\");
            return "\"" + name + "\"";
        }

        /// <summary>
        /// Quotes the string without replace slashes.
        /// </summary>
        /// <param name="name">
        /// String to quote.
        /// </param>
        /// <returns>
        /// A properly quoted string.
        /// </returns>
        public static string QuoteStringWithoutReplaceSlashes(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return "\"" + "\"";
            }

            return "\"" + name + "\"";
        }

        /// <summary>
        /// Create an enumeration [0...upperBound)
        /// </summary>
        /// <param name="upperBound">
        /// the non inclusive upper bound
        /// </param>
        /// <param name="startIndex">
        /// The start index.
        /// </param>
        /// <returns>
        /// An enumeration from 0 to upperBound-1
        /// </returns>
        public static IEnumerable<int> Range(this int upperBound, int startIndex = 0)
        {
            var result = startIndex;
            while (result < (upperBound + startIndex))
            {
                yield return result++;
            }
        }

        /// <summary>
        /// Create an enumeration [0...upperBound)
        /// </summary>
        /// <param name="upperBound">
        /// the non inclusive upper bound
        /// </param>
        /// <param name="startIndex">
        /// The start index.
        /// </param>
        /// <returns>
        /// An enumeration from 0 to upperBound-1
        /// </returns>
        public static IEnumerable<long> Range(this long upperBound, long startIndex = 0)
        {
            var result = startIndex;
            while (result < (upperBound + startIndex))
            {
                yield return result++;
            }
        }

        /// <summary>
        /// Compares collections and returns true if the collections, viewed as unorded sets, ignoring duplicates
        /// with no repetitions, are the same.
        /// </summary>
        /// <typeparam name="T">
        /// Any type that can be used in sets
        /// </typeparam>
        /// <param name="left">
        /// the source sequence
        /// </param>
        /// <param name="right">
        /// a sequence to test
        /// </param>
        /// <returns>
        /// <code>
        /// true
        /// </code>
        /// if the lists contain the same elements albeit not in the same order
        /// </returns>
        public static bool SameSetAs<T>(this IEnumerable<T> left, IEnumerable<T> right)
        {
            if (left == null)
            {
                return right == null;
            }

            // Left is not null, return false if right is null
            if (right == null)
            {
                return false;
            }

            // See if source is a subset of test
            var enumerable = left as IList<T> ?? left.ToList();
            var result = enumerable.All(c => right.Contains(c));
            if (result == false)
            {
                return false;
            }

            // It is, test is now a subset of source iff the two arrays as sets are equal
            return right.All(c => enumerable.Contains(c));
        }

        /// <summary>
        /// Replaces the last occurrence.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="find">
        /// The find.
        /// </param>
        /// <param name="replace">
        /// The replace.
        /// </param>
        /// <returns>
        /// Replaces the last occurrence of an instance in a string.
        /// </returns>
        public static string ReplaceLastOccurrence(this string source, string find, string replace)
        {
            var place = source.LastIndexOf(find, StringComparison.Ordinal);
            if (place < 0)
            {
                return source;
            }

            var result = source.Remove(place, find.Length).Insert(place, replace);
            return result;
        }

        /// <summary>
        /// Extension to BlockingCollection class to remove a specified item from the collection.
        /// </summary>
        /// <typeparam name="T">
        /// The type of items in the blocking collection.
        /// </typeparam>
        /// <param name="self">
        /// The blocking collection.
        /// </param>
        /// <param name="itemToRemove">
        /// The item to remove.
        /// </param>
        /// <returns>
        /// True if one or more items were removed, else false.
        /// </returns>
        public static bool Remove<T>(this BlockingCollection<T> self, T itemToRemove)
        {
            if (!self.Contains(itemToRemove))
            {
                return false;
            }

            lock (self)
            {
                T item;
                var keeperList = new List<T>();
                while (self.TryTake(out item))
                {
                    if (item.Equals(itemToRemove))
                    {
                        var disposable = item as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                    else
                    {
                        keeperList.Add(item);
                    }
                }

                foreach (var keeper in keeperList)
                {
                    self.Add(keeper);
                }
            }

            return true;
        }

        /// <summary>
        /// Scale factors are similar to voltages ratios so we use 20*Log
        /// </summary>
        /// <param name="scales">
        /// the scale factors to set
        /// </param>
        /// <returns>
        /// an enumeration of scale values
        /// </returns>
        public static IEnumerable<float> ScaleToDecibel(this IEnumerable<float> scales)
        {
            return from c in scales select c.EqualsWithinPrecision(0f) ? -96 : (float)(20 * Math.Log10(Math.Abs(c)));
        }

        /// <summary>
        /// Converts the list to a comma-separated string.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <returns>
        /// The comma-separated string.
        /// </returns>
        public static string ToCommaSeparated(this List<string> list)
        {
            return string.Join(",", list.ToArray());
        }

        /// <summary>
        /// Converts an enum value to its description attribute.
        /// If no description, it breaks apart the word at the camel case
        /// </summary>
        /// <param name="value">
        /// The value to extract the description
        /// </param>
        /// <returns>
        /// The description string
        /// </returns>
        public static string ToDescription(this Enum value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo == null)
            {
                return null;
            }

            var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }

            return value.CamelCaseToWords();
        }

        /// <summary>
        /// Converts a property info descriptor to its description attribute.
        /// If no description, it breaks apart the word at the camel case
        /// </summary>
        /// <param name="value">
        /// The value to extract the description
        /// </param>
        /// <returns>
        /// The description string
        /// </returns>
        public static string ToDescription(this MemberInfo value)
        {
            var attributes = (DescriptionAttribute[])value.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }

            return value.Name.CamelCaseToWords();
        }

        /// <summary>
        /// Takes a name and returns a value; e.g.,
        /// var enumValue = (PackingEnum)PackingEnum.NoPacking.ToEnum("Run Length Encoded");
        /// </summary>
        /// <param name="enumValue">
        /// Any enum value
        /// </param>
        /// <param name="value">
        /// The name of the description
        /// </param>
        /// <returns>
        /// An enum value corresponding to the description, if not an ArgumentException is thrown
        /// </returns>
        public static object ToEnum(this Enum enumValue, string value)
        {
            return ToEnum(enumValue.GetType(), value);
        }

        /// <summary>
        /// Takes an enum type and returns a value; e.g.,
        /// var enumValue = (PackingEnum)Extensions.ToEnum(typeof(PackingEnum), "Run Length Encoded");
        /// </summary>
        /// <param name="enumType">
        /// the typeof(enum)
        /// </param>
        /// <param name="value">
        /// The name of the description
        /// </param>
        /// <param name="ignoreCase">
        /// if set to <c>true</c> ignore case.
        /// </param>
        /// <returns>
        /// An enum value corresponding to the description, if not an ArgumentException is thrown
        /// </returns>
        /// <exception cref="System.ArgumentException">
        /// The string is not a description or value of the specified enum.
        /// </exception>
        public static object ToEnum(this Type enumType, string value, bool ignoreCase = false)
        {
            try
            {
                var names = Enum.GetNames(enumType);

                var enumName = names.First(c => ((Enum)Enum.Parse(enumType, c, ignoreCase)).ToDescription().Equals(value));
                return Enum.Parse(enumType, enumName);
            }
            catch (InvalidOperationException ex)
            {
                throw new ArgumentException("The string is not a description or value of the specified enum.", ex);
            }
        }

        /// <summary>
        /// Tries to parse a value as the given enum.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the result.
        /// </typeparam>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="ignoreCase">
        /// if set to <c>true</c> ignore case.
        /// </param>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <returns>
        /// An enum value
        /// </returns>
        public static bool EnumTryParse<T>(this string type, bool ignoreCase, out T result)
        {
            try
            {
                result = (T)Enum.Parse(typeof(T), type, ignoreCase);
                return true;
            }
            catch
            {
                result = default(T);
                return false;
            }
        }

        /// <summary>
        /// Convert bytes to lower case hex representations, 2 hex values each byte.
        /// </summary>
        /// <param name="source">
        /// a byte array
        /// </param>
        /// <returns>
        /// the lower case hex representation
        /// </returns>
        public static string ToHex(this byte[] source)
        {
            var hexStrings = from b in source select b.ToString("x2", CultureInfo.InvariantCulture);

            return string.Join(string.Empty, hexStrings.ToArray());
        }

        /// <summary>
        /// Convert an object to an int32
        /// Throw a Convert.ToInt32 error if not a valid int32
        /// </summary>
        /// <param name="value">
        /// the object to convert
        /// </param>
        /// <returns>
        /// <code>
        /// int
        /// </code>
        /// value of the object if exists, and exception is throw otherwise
        /// </returns>
        public static int ToInt32(this object value)
        {
            return Convert.ToInt32(value, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// To the observable.
        /// </summary>
        /// <typeparam name="T">
        /// Source INotifyPropertyChange
        /// </typeparam>
        /// <typeparam name="TR">
        /// The type of the TR.
        /// </typeparam>
        /// <param name="source">
        /// The target.
        /// </param>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <param name="initialValue">
        /// if set to <c>true</c> (default) start with initial value.
        /// </param>
        /// <returns>
        /// IObservable from property
        /// </returns>
        /// <exception cref="System.NotSupportedException">
        /// Only use expressions that call a single property
        /// </exception>
        public static IObservable<TR> ToObservable<T, TR>(
            this T source, Expression<Func<T, TR>> property, bool initialValue = true) where T : INotifyPropertyChanged
        {
            var f = property.Body as MemberExpression;

            // Sanity check
            if (f == null)
            {
                throw new NotSupportedException("Only use expressions that call a single property");
            }

            // Get the property name
            var propertyName = f.Member.Name;

            // Get the value to evaluate property
            var getTheValueFunction = property.Compile();
            return Observable.Create<TR>(
                o =>
                    {
                        var eventHandler = new PropertyChangedEventHandler(
                            (eventHandlerSource, propertyChangedArgs) =>
                                {
                                    if (propertyChangedArgs.PropertyName == null
                                        || propertyChangedArgs.PropertyName == propertyName)
                                    {
                                        o.OnNext(getTheValueFunction(source));
                                    }
                                });

                        source.PropertyChanged += eventHandler;

                        // Start with a value if desired
                        if (initialValue)
                        {
                            o.OnNext(getTheValueFunction(source));
                        }

                        return () => source.PropertyChanged -= eventHandler;
                    });
        }

        /// <summary>
        /// Truncates the specified source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="length">
        /// The length.
        /// </param>
        /// <returns>
        /// A truncated string.
        /// </returns>
        public static string Truncate(this string source, int length)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return string.Empty;
            }

            if (source.Length > length)
            {
                return source.Substring(0, length);
            }

            return source;
        }

        /// <summary>
        /// The reverse of CompareTo for sorting a list of comparables in descending order.
        /// </summary>
        /// <typeparam name="T">
        /// An IComparable type.
        /// </typeparam>
        /// <param name="self">
        /// The self.
        /// </param>
        /// <param name="comparand">
        /// The comparand.
        /// </param>
        /// <returns>
        /// -1 if we are greater than the comparand, or 1 if we are less, or 0 if equal.
        /// </returns>
        public static int ReverseCompareTo<T>(this T self, T comparand) where T : IComparable
        {
            return -self.CompareTo(comparand);
        }

        /// <summary>
        /// Place a uint16 value into a big-endian byte array. System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="byteArray">
        /// The byte array.
        /// </param>
        /// <param name="startIndex">
        /// The starting index in the array, incremented by 4 when done.
        /// </param>
        public static void Uint16ToBytes(ushort value, byte[] byteArray, ref int startIndex)
        {
            STwoBytes s;
            s.Byte0 = 0;
            s.Byte1 = 0;
            if (startIndex > byteArray.Length - 2)
            {
                return;
            }

            s.Uint0 = value;
            byteArray[startIndex++] = s.Byte1;
            byteArray[startIndex++] = s.Byte0;
        }

        /// <summary>
        /// Place a uint32 value into a big-endian byte array. System.BitConverter doesn't work here; it's the wrong endianness.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="byteArray">
        /// The byte array.
        /// </param>
        /// <param name="startIndex">
        /// The starting index in the array, incremented by 4 when done.
        /// </param>
        public static void Uint32ToBytes(uint value, byte[] byteArray, ref int startIndex)
        {
            SFourBytes s;
            s.Byte3 = 0;
            s.Byte2 = 0;
            s.Byte1 = 0;
            s.Byte0 = 0;
            if (startIndex > byteArray.Length - 4)
            {
                return;
            }

            s.Uint0 = value;
            byteArray[startIndex++] = s.Byte3;
            byteArray[startIndex++] = s.Byte2;
            byteArray[startIndex++] = s.Byte1;
            byteArray[startIndex++] = s.Byte0;
        }

        /// <summary>
        /// Returns a list of white spaces for the Split function
        /// </summary>
        /// <returns>List of white spaces</returns>
        public static char[] WhiteSpaces()
        {
            return new[] { ' ', '\t', '\r', '\n' };
        }

        /// <summary>
        /// Removes all leading or trailing whitespace.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// string trimmed of white space
        /// </returns>
        public static string TrimLeadingOrTrailingWhitespace(this string input)
        {
            return input?.TrimStart(WhiteSpaces()).TrimEnd(WhiteSpaces());
        }

        /// <summary>
        /// Trims the leading or trailing cursors positioning characters.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>A string trimmed of new lines.</returns>
        public static string TrimLeadingOrTrailingCursorPositioningCharacters(this string input)
        {
            var newLines = new[] { '\t', '\r', '\n' };
            return input?.TrimStart(newLines).TrimEnd(newLines);
        }

        /// <summary>
        /// Gets the padding digits string.
        /// </summary>
        /// <param name="paddingDigits">
        /// The padding digits.
        /// </param>
        /// <returns>
        /// The padding digits string.
        /// </returns>
        public static string GetPaddingDigitsString(int paddingDigits)
        {
            if (paddingDigits == 0)
            {
                return "00000000";
            }

            var count = 0;
            var paddingString = string.Empty;
            while (count < paddingDigits)
            {
                paddingString += "0";
                count++;
            }

            return paddingString;
        }

        /// <summary>
        /// Counts the number of consecutive occurrences.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="characterToCount">The character to count.</param>
        /// <returns>The number of consecutive occurrences.</returns>
        public static int CountNumberOfConsecutiveOccurrences(this string source, char characterToCount)
        {
            var currentCount = 0;
            var maxCount = -1;
            foreach (var character in source)
            {
                if (character != characterToCount)
                {
                    if (currentCount > maxCount)
                    {
                        maxCount = currentCount;
                    }

                    currentCount = 0;
                    continue;
                }

                currentCount++;
            }

            if (currentCount > maxCount)
            {
                maxCount = currentCount;
            }

            return maxCount;
        }

        /// <summary>
        /// Gets the output file name string format.
        /// </summary>
        /// <param name="outputFileName">Name of the output file.</param>
        /// <returns>The output file name string format.</returns>
        public static string GetFilePerFrameOutputFileNameStringFormat(string outputFileName)
        {
            var consecutiveCount = outputFileName.CountNumberOfConsecutiveOccurrences('#');
            if (consecutiveCount == 0)
            {
                return outputFileName;
            }

            var stringToReplace = new StringBuilder();
            var formatString = new StringBuilder();
            formatString.Append("{0:");
            var i = 0;
            while (i < consecutiveCount)
            {
                stringToReplace.Append("#");
                formatString.Append("0");
                i++;
            }

            formatString.Append("}");
            var replaceString = stringToReplace.ToString();
            if (string.IsNullOrWhiteSpace(replaceString))
            {
                return outputFileName;
            }

            return outputFileName.Replace(stringToReplace.ToString(), formatString.ToString());
        }

        /// <summary>
        /// Removes the file per frame digit padding string.
        /// </summary>
        /// <param name="outputFileName">Name of the output file.</param>
        /// <returns>A string without the padding string.</returns>
        public static string RemoveFilePerFrameDigitPaddingString(this string outputFileName)
        {
            var fileNameNoExtension = Path.GetFileNameWithoutExtension(outputFileName);
            if (string.IsNullOrWhiteSpace(fileNameNoExtension))
            {
                return string.Empty;
            }

            var consecutiveCount = fileNameNoExtension.CountNumberOfConsecutiveOccurrences('#');
            var stringToReplace = new StringBuilder();
            var i = 0;
            while (i < consecutiveCount)
            {
                stringToReplace.Append("#");
                i++;
            }

            var result = fileNameNoExtension.Replace(stringToReplace.ToString(), string.Empty);
            return result.TrimEnd(FileUtilities.ValidFilenameSeparators);
        }

        /// <summary>
        /// Dumps the class members, their types and values.
        /// </summary>
        /// <param name="obj">
        /// The object.
        /// </param>
        /// <param name="delimeter">
        /// The delimiter (default is carriage-return).
        /// </param>
        /// <returns>
        /// A delimiter separated string of the class members, types and values.
        /// </returns>
        public static string DumpClassMembers(this object obj, string delimiter = "\n")
        {
            if (obj == null)
            {
                return string.Empty;
            }

            var properties = obj.GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);

            var info = new StringBuilder();
            foreach (var prop in properties)
            {
                var value = prop.GetValue(obj, null);
                var pairInfo = string.Format("{0} ({1}): {2}", prop.Name, prop.PropertyType.Name, value);

                info.Append(pairInfo);
                info.Append(delimiter);
            }

            return info.ToString();
        }

        /// <summary>
        /// Convert frame number into number of samples for frame.
        /// </summary>
        /// <param name="frameNumber">
        /// The frame number.
        /// </param>
        /// <param name="frameRate">
        /// The frame rate.
        /// </param>
        /// <param name="audioSamplingRate">
        /// The audio sampling rate.
        /// </param>
        /// <returns>
        /// The number of samples for this frame.
        /// </returns>
        public static long FrameNumberToNumberOfSamplesForFrame(int frameNumber, double frameRate, double audioSamplingRate)
        {
            // We do difference because some frames might have more samples than others
            var thisFrameFirstSample = FrameNumberToPositionInSamples(frameNumber, frameRate, audioSamplingRate);
            var nextFrameFirstSample = FrameNumberToPositionInSamples(frameNumber + 1, frameRate, audioSamplingRate);
            return nextFrameFirstSample - thisFrameFirstSample;
        }

        /// <summary>
        /// Convert frame number into position in samples.
        /// </summary>
        /// <param name="frameNumber">
        /// The frame number.
        /// </param>
        /// <param name="frameRate">
        /// The frame rate.
        /// </param>
        /// <param name="audioSamplingRate">
        /// The audio sampling rate.
        /// </param>
        /// <returns>
        /// The position in samples for this frame.
        /// </returns>
        public static long FrameNumberToPositionInSamples(int frameNumber, double frameRate, double audioSamplingRate)
        {
            return (long)Math.Round((frameNumber / frameRate) * audioSamplingRate);
        }

        /// <summary>
        /// Adds the trailing whitespace.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <param name="whitespace">
        /// The whitespace.
        /// </param>
        /// <returns>
        /// A string with whitespace added.
        /// </returns>
        public static string AddTrailingWhitespace(this string input, int whitespace)
        {
            return input + new string(' ', whitespace);
        }

        /// <summary>
        /// Computes the mean of a list of doubles after eliminating outlier values.
        /// </summary>
        /// <param name="data">
        /// The list of doubles.
        /// </param>
        /// <returns>
        /// The mean of the sanitized list.
        /// </returns>
        public static double ComputeMeanWithoutOutliers(this IEnumerable<double> data)
        {
            var dataList = data.ToList();
            var minValue = dataList.Min();

            // Compute the deviation from the min value and the sum of squared deviations.
            var devSqSum = dataList.Select(datum => datum - minValue).Select(deviation => deviation * deviation).Sum();

            // Compute the standard deviation from the min value.
            var stdDev = Math.Sqrt(devSqSum / dataList.Count);

            // Whack the outliers.
            var sanitizedList = dataList.Where(datum => (datum <= (minValue + stdDev))).ToList();

            // Return the mean ignoring the outliers.
            return sanitizedList.Sum() / sanitizedList.Count;
        }

        /// <summary>
        /// Determines whether the specified input matches the wildcard pattern.
        /// </summary>
        /// <param name="wildcardPattern">
        /// The wildcard pattern.
        /// </param>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// True if they are a match.
        /// </returns>
        public static bool IsWildcardMatch(this string wildcardPattern, string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            var pattern = "^" + Regex.Escape(wildcardPattern).Replace("\\*", ".*").Replace("\\?", ".") + "$";
            var regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(input);
        }

        /// <summary>
        /// Gets a descendant XElement matching the local name.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// A XElement.
        /// </returns>
        public static XElement GetDescendantWithLocalName(this XElement parent, string localName)
        {
            return parent?.Descendants().FirstOrDefault(d => d.Name.LocalName == localName);
        }

        /// <summary>
        /// Gets an enumerable of descandant XElements matching the local name.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="localName">Name of the local.</param>
        /// <returns>An enumerable of XElements.</returns>
        public static IEnumerable<XElement> GetDescendantsWithLocalName(this XElement parent, string localName)
        {
            return parent?.Descendants().Where(d => d.Name.LocalName == localName);
        }

        /// <summary>
        /// Gets the element with the local name.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// A XElement.
        /// </returns>
        public static XElement GetElementWithLocalName(this XElement parent, string localName)
        {
            return parent?.Elements().FirstOrDefault(e => e.Name.LocalName == localName);
        }

        /// <summary>
        /// Removes the element with the local name
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="localName">Name of the local.</param>
        public static void RemoveElementWithLocalName(this XElement parent, string localName)
        {
            var element = parent?.Elements().FirstOrDefault(e => e.Name.LocalName == localName);
            element?.Remove();
        }

        /// <summary>
        /// Gets the elements with the local name.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// An enumerable of XElement.
        /// </returns>
        public static IEnumerable<XElement> GetElementsWithLocalName(this XElement parent, string localName)
        {
            return parent?.Elements().Where(e => e.Name.LocalName == localName);
        }

        /// <summary>
        /// Gets the element value string.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// The value as a string.
        /// </returns>
        public static string GetElementValueString(this XElement parent, string localName)
        {
            var element = parent.GetElementWithLocalName(localName);
            return element?.Value;
        }

        /// <summary>
        /// Gets the element value long.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="localName">Name of the local.</param>
        /// <returns>The value as a nullable long.</returns>
        public static long? GetElementValueLong(this XElement parent, string localName)
        {
            var element = parent.GetElementWithLocalName(localName);
            return element == null ? (long?)null : Convert.ToInt64(element.Value);
        }

        /// <summary>
        /// Gets the element value float.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="localName">Name of the local.</param>
        /// <returns>The value as a nullable float.</returns>
        public static float? GetElementValueFloat(this XElement parent, string localName)
        {
            var element = parent.GetElementWithLocalName(localName);
            return element == null ? (float?)null : Convert.ToSingle(element.Value, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Gets the element date time.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// The date time.
        /// </returns>
        public static DateTime GetElementDateTime(this XElement parent, string localName)
        {
            var element = parent.GetElementWithLocalName(localName);
            return element == null ? DateTime.MinValue : (DateTime)element;
        }

        /// <summary>
        /// Gets the descendant XElement value string.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <param name="removeHexPrefix">
        /// if set to <c>true</c> remove hexadecimal prefix.
        /// </param>
        /// <returns>
        /// The value as a string.
        /// </returns>
        public static string GetDescendantValueString(this XElement parent, string localName, bool removeHexPrefix = false)
        {
            var element = parent.GetDescendantWithLocalName(localName);
            return element == null ? null : (removeHexPrefix ? element.Value.Replace("0x", string.Empty) : element.Value);
        }

        /// <summary>
        /// Gets the descendant value bool.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="localName">Name of the local.</param>
        /// <returns>The boolean value.</returns>
        public static bool? GetDescendantValueBool(this XElement parent, string localName)
        {
            var value = parent.GetDescendantValueString(localName);
            if (!bool.TryParse(value, out var result))
            {
                return null;
            }

            return result;
        }

        /// <summary>
        /// Gets the descendant numerator denominator string.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// The numerator/denominator string.
        /// </returns>
        public static string GetDescendantNumeratorDenominatorString(this XElement parent, string localName)
        {
            var element = parent.GetDescendantWithLocalName(localName);
            if (element == null)
            {
                return null;
            }

            var numeratorElement = element.Elements().FirstOrDefault(e => e.Name.LocalName == "Numerator");
            var denominatorElement = element.Elements().FirstOrDefault(e => e.Name.LocalName == "Denominator");
            if (numeratorElement == null || denominatorElement == null)
            {
                return null;
            }

            return numeratorElement.Value + "/" + denominatorElement.Value;
        }

        /// <summary>
        /// Gets the descendant value ul.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// The UL.
        /// </returns>
        public static string GetDescendantValueUl(this XElement parent, string localName)
        {
            var valueString = parent.GetDescendantValueString(localName);
            if (string.IsNullOrWhiteSpace(valueString))
            {
                return null;
            }

            var valueStringNoPeriods = valueString.Replace(".", string.Empty);
            var prefix = valueStringNoPeriods.StartsWith(UlPrefix) ? string.Empty : UlPrefix;
            return prefix + Regex.Replace(valueStringNoPeriods, ".{8}", "$0.").TrimEnd('.');
        }

        /// <summary>
        /// Gets the descendant value UUID.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// The UUID.
        /// </returns>
        public static string GetDescendantValueUuid(this XElement parent, string localName)
        {
            var valueString = parent.GetDescendantValueString(localName);
            if (string.IsNullOrWhiteSpace(valueString))
            {
                return null;
            }

            valueString = valueString.Replace("-", string.Empty);
            valueString = valueString.Replace(IdPrefix, string.Empty);
            valueString = valueString.Replace(".", string.Empty);
            var guid = Guid.Parse(valueString);
            return IdPrefix + guid.ToString("D");
        }

        /// <summary>
        /// Gets the attribute value string.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// The value as a string.
        /// </returns>
        public static string GetAttributeValueString(this XElement parent, string localName)
        {
            var attribute = parent.GetAttributeWithLocalName(localName);
            return attribute == null ? null : attribute.Value;
        }

        /// <summary>
        /// Gets the attribute value ul.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// The UL.
        /// </returns>
        public static string GetAttributeValueUl(this XElement parent, string localName)
        {
            var valueString = parent.GetAttributeValueString(localName);
            if (string.IsNullOrWhiteSpace(valueString))
            {
                return null;
            }

            var valueStringNoPeriods = valueString.Replace(".", string.Empty);
            return UlPrefix + Regex.Replace(valueStringNoPeriods, ".{8}", "$0.").TrimEnd('.');
        }

        /// <summary>
        /// Gets the attribute value UUID.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// The UUID.
        /// </returns>
        public static string GetAttributeValueUuid(this XElement parent, string localName)
        {
            var valueString = parent.GetAttributeValueString(localName);
            if (string.IsNullOrWhiteSpace(valueString))
            {
                return null;
            }

            valueString = valueString.Replace(".", string.Empty);
            var guid = Guid.Parse(valueString);
            return IdPrefix + guid.ToString("D");
        }

        /// <summary>
        /// Gets the attribute with the local name.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="localName">
        /// Name of the local.
        /// </param>
        /// <returns>
        /// A XAttribute.
        /// </returns>
        public static XAttribute GetAttributeWithLocalName(this XElement parent, string localName)
        {
            return parent.Attributes().FirstOrDefault(d => d.Name.LocalName == localName);
        }

        /// <summary>
        /// Removes the attribute with the local name
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="localName">Name of the local.</param>
        public static void RemoveAttributeWithLocalName(this XElement parent, string localName)
        {
            var attribute = parent.Attributes().FirstOrDefault(d => d.Name.LocalName == localName);
            attribute?.Remove();
        }

        /// <summary>
        /// Converts a XElement to a XmlElement.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>A XmlElement.</returns>
        public static XmlElement ToXmlElement(this XElement element)
        {
            var document = new XmlDocument();
            document.LoadXml(element.ToString());
            return document.DocumentElement;
        }

        /// <summary>
        /// Converts a XElement to a XmlNode.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>A XmlNode.</returns>
        public static XmlNode ToXmlNode(this XElement element)
        {
            var document = new XmlDocument();
            document.LoadXml(element.ToString());
            return document.FirstChild;
        }

        /// <summary>
        /// Checks the comma separated list.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="size">The size.</param>
        /// <returns>True if right size, otherwise false.</returns>
        public static bool CheckCommaSeparatedList(this XElement element, int size)
        {
            if (string.IsNullOrWhiteSpace(element?.Value))
            {
                return false;
            }

            var commaSeparatedList = element.Value.Split(',');
            if (commaSeparatedList.Length != size)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks that the child exists and its value is not empty.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>True if the chil exists and its value is not empty.</returns>
        public static bool ChildExistsAndValueIsNotEmpty(this XElement element)
        {
            return element != null && !string.IsNullOrWhiteSpace(element.Value);
        }

        /// <summary>
        /// Converts a XmlElement to XElement.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>A XElement.</returns>
        public static XElement ToXElement(this XmlElement element)
        {
            return XElement.Parse(element.OuterXml);
        }

        /// <summary>
        /// Removes the namespace.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>An element with no namespace.</returns>
        public static XElement RemoveNamespace(this XElement element)
        {
            var res = new XElement(element.Name.LocalName, element.HasElements ? element.Elements().Select(el => RemoveNamespace(el)) : (object)element.Value);
            res.ReplaceAttributes(element.Attributes().Where(attr => !attr.IsNamespaceDeclaration));
            return res;
        }

        /// <summary>
        /// Replaces the namespace.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="newNamespace">The new namespace.</param>
        /// <returns>an element with new namespace</returns>
        public static XElement ReplaceNamespace(this XElement element, XNamespace newNamespace)
        {
            var res = new XElement(newNamespace + element.Name.LocalName, element.HasElements ? element.Elements().Select(el => ReplaceNamespace(el, newNamespace)) : (object)element.Value);
            res.ReplaceAttributes(element.Attributes().Where(attr => !attr.IsNamespaceDeclaration));
            return res;
        }

        /// <summary>
        /// Adds the MD5 hash of a float array
        /// </summary>
        /// <param name="md5">The MD5.</param>
        /// <param name="source">The source array</param>
        /// <param name="bits">The bits.</param>
        public static void AddToMd5Hash(this MD5 md5, float[] source, int bits = 0)
        {
            // To avoid too much memory on large arrays, break this into chunks
            const int HashBufferByteSize = 10000 * sizeof(float);
            var sourceSizeInBytes = source.Length * sizeof(float);
            var hashBuffer = new byte[HashBufferByteSize];
            var bytesToProcess = Math.Min(HashBufferByteSize, sourceSizeInBytes);
            var hashBufferByteIndex = 0;
            while (bytesToProcess > 0)
            {
                Buffer.BlockCopy(source, hashBufferByteIndex, hashBuffer, 0, bytesToProcess);
                md5.TransformBlock(hashBuffer, 0, bytesToProcess, null, 0);
                hashBufferByteIndex += bytesToProcess;
                bytesToProcess = Math.Min(HashBufferByteSize, sourceSizeInBytes - hashBufferByteIndex);
            }
        }

        /// <summary>
        /// Adds to MD5 hash.
        /// </summary>
        /// <param name="md5">The MD5.</param>
        /// <param name="source">The source.</param>
        public static void AddToMd5Hash(this MD5 md5, byte[] source)
        {
            if (source.Length > 0)
            {
                md5.TransformBlock(source, 0, source.Length, null, 0);
            }
        }

        /// <summary>
        /// Adds the MD5 hash of a float array
        /// </summary>
        /// <param name="md5">The MD5.</param>
        /// <param name="value">The value.</param>
        public static void AddToMd5Hash(this MD5 md5, double value)
        {
            var bytePcmData = new byte[sizeof(double)];
            Buffer.BlockCopy(new[] { value }, 0, bytePcmData, 0, bytePcmData.Length);
            md5.TransformBlock(bytePcmData, 0, bytePcmData.Length, null, 0);
        }

        /// <summary>
        /// Adds the MD5 hash of a float array
        /// </summary>
        /// <param name="md5">The MD5.</param>
        /// <param name="value">The value.</param>
        public static void AddToMd5Hash(this MD5 md5, int value)
        {
            var bytePcmData = new byte[sizeof(int)];
            Buffer.BlockCopy(new[] { value }, 0, bytePcmData, 0, bytePcmData.Length);
            md5.TransformBlock(bytePcmData, 0, bytePcmData.Length, null, 0);
        }

        /// <summary>
        /// Gets the byte array in string order.
        /// </summary>
        /// <param name="guid">The unique identifier.</param>
        /// <returns>A byte array.</returns>
        public static byte[] ToStringOrderByteArray(this Guid guid)
        {
            var outOfOrderByteArray = guid.ToByteArray();
            var orderedList = new List<byte>
            {
                outOfOrderByteArray[3],
                outOfOrderByteArray[2],
                outOfOrderByteArray[1],
                outOfOrderByteArray[0],
                outOfOrderByteArray[5],
                outOfOrderByteArray[4],
                outOfOrderByteArray[7],
                outOfOrderByteArray[6],
                outOfOrderByteArray[8],
                outOfOrderByteArray[9],
                outOfOrderByteArray[10],
                outOfOrderByteArray[11],
                outOfOrderByteArray[12],
                outOfOrderByteArray[13],
                outOfOrderByteArray[14],
                outOfOrderByteArray[15]
            };

            return orderedList.ToArray();
        }

        /// <summary>
        /// Converts the byte array to a guid string.
        /// </summary>
        /// <param name="byteArray">The byte array.</param>
        /// <returns>The guid string.</returns>
        public static string ToGuidString(this byte[] byteArray)
        {
            if (byteArray.Length != 16)
            {
                return string.Empty;
            }

            return
                $"{byteArray[0]:x2}{byteArray[1]:x2}{byteArray[2]:x2}{byteArray[3]:x2}-{byteArray[4]:x2}{byteArray[5]:x2}-{byteArray[6]:x2}{byteArray[7]:x2}-{byteArray[8]:x2}{byteArray[9]:x2}-{byteArray[10]:x2}{byteArray[11]:x2}{byteArray[12]:x2}{byteArray[13]:x2}{byteArray[14]:x2}{byteArray[15]:x2}";
        }

        /// <summary>
        /// Converts this byte array to a string.
        /// </summary>
        /// <param name="byteArray">The byte array.</param>
        /// <returns>The byte string.</returns>
        public static string ToByteString(this byte[] byteArray)
        {
            if (byteArray.Length != 16)
            {
                return string.Empty;
            }

            return
                $"{byteArray[0]:x2}{byteArray[1]:x2}{byteArray[2]:x2}{byteArray[3]:x2}{byteArray[4]:x2}{byteArray[5]:x2}{byteArray[6]:x2}{byteArray[7]:x2}{byteArray[8]:x2}{byteArray[9]:x2}{byteArray[10]:x2}{byteArray[11]:x2}{byteArray[12]:x2}{byteArray[13]:x2}{byteArray[14]:x2}{byteArray[15]:x2}";
        }

        /// <summary>
        /// Converts a hex string to a byte array.
        /// </summary>
        /// <param name="hex">The hexadecimal.</param>
        /// <returns>A byte array.</returns>
        public static byte[] ToByteArray(this string hex)
        {
            var numberChars = hex.Length;
            var bytes = new byte[numberChars / 2];
            for (int i = 0; i < numberChars; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }

            return bytes;
        }

        /// <summary>
        /// Gets the available free space.
        /// </summary>
        /// <param name="driveInfo">The drive information.</param>
        /// <param name="directory">The directory.</param>
        /// <returns>
        /// The amount of available space.
        /// </returns>
        public static long GetAvailableFreeSpace(this DriveInfo driveInfo, string directory)
        {
            if (Hardware.DriveFreeBytes(directory, out var freeSpace))
            {
                return (long)freeSpace;
            }

            return driveInfo.AvailableFreeSpace;
        }

        /// <summary>
        /// Gets the total space.
        /// </summary>
        /// <param name="driveInfo">The drive information.</param>
        /// <param name="directory">The directory.</param>
        /// <returns>The total size of the drive.</returns>
        public static long GetTotalSize(this DriveInfo driveInfo, string directory)
        {
            if (Hardware.DriveTotalBytes(directory, out var totalSize))
            {
                return (long)totalSize;
            }

            return driveInfo.TotalSize;
        }

        #endregion

        /// <summary>
        /// This contains all the basic extensions
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        private struct SFourBytes
        {
            /// <summary>
            /// the first byte
            /// </summary>
            [FieldOffset(0)]
            public byte Byte0;

            /// <summary>
            ///  the second byte
            /// </summary>
            [FieldOffset(1)]
            public byte Byte1;

            /// <summary>
            ///  the third byte
            /// </summary>
            [FieldOffset(2)]
            public byte Byte2;

            /// <summary>
            ///  this sure is tedious
            /// </summary>
            [FieldOffset(3)]
            public byte Byte3;

            /// <summary>
            /// the resulting int32
            /// </summary>
            [FieldOffset(0)]
            public int Int0;

            /// <summary>
            /// the resulting uint32
            /// </summary>
            [FieldOffset(0)]
            public uint Uint0;

            /// <summary>
            /// the resulting float
            /// </summary>
            [FieldOffset(0)]
            public float Float0;
        }

        /// <summary>
        /// This contains all the basic extensions
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        private struct STwoBytes
        {
            /// <summary>
            /// the first byte
            /// </summary>
            [FieldOffset(0)]
            public byte Byte0;

            /// <summary>
            ///  the second byte
            /// </summary>
            [FieldOffset(1)]
            public byte Byte1;

            /// <summary>
            /// the resulting int16
            /// </summary>
            [FieldOffset(0)]
            public short Int0;

            /// <summary>
            /// the resulting uint32
            /// </summary>
            [FieldOffset(0)]
            public ushort Uint0;
        }
    }
}