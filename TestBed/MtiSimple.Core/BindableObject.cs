// <copyright file="BindableObject.cs" company="Public Domain">
// Public Domain
// </copyright>
// <author>Unknown</author>
// <date>11/1/2009 1:59:49 PM</date>
// <summary>Implements the INotifyPropertyChange Interface</summary>

namespace MtiSimple.Core
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Implements the INotifyPropertyChanged interface and
    /// exposes a RaisePropertyChanged method for derived
    /// classes to raise the PropertyChange event.  The event
    /// arguments created by this class are cached to prevent
    /// managed heap fragmentation.
    /// </summary>
    [Serializable]
    public abstract class BindableObject : INotifyPropertyChanged
    {
        /// <summary>
        /// Backing store for the property argument cache
        /// </summary>
        private static readonly Dictionary<string, PropertyChangedEventArgs> EventArgCache;

        /// <summary>
        /// The lock for access to the  property argument cache
        /// </summary>
        private static readonly FairSemaphore EventArgCacheLock = new FairSemaphore();

        /// <summary>
        /// Backing store for the property argument cache
        /// </summary>
        [field: NonSerialized]
        private ConcurrentDictionary<string, object> objectCache;

        /// <summary>
        /// Gets or sets the name of the class changed property.
        /// </summary>
        /// <value>The name of the class changed property.</value>
        [field: NonSerialized]
        private string classChangedPropertyName;

        #region Constructors

        /// <summary>
        /// Initializes static members of the <see cref="BindableObject"/> class.
        /// </summary>
        static BindableObject()
        {
            // Used for a one time initialization
            EventArgCache = new Dictionary<string, PropertyChangedEventArgs>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindableObject" /> class.
        /// </summary>
        protected BindableObject()
        {
            this.objectCache = new ConcurrentDictionary<string, object>();
        }

        #endregion // Constructors

        #region Public Members

        /// <summary>
        /// Raised when a public property of this object is set.
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Returns an instance of PropertyChangedEventArgs for
        /// the specified property name.
        /// </summary>
        /// <param name="propertyName">The name of the property to create event args for.</param>
        /// <returns>A new property change argument</returns>
        public static PropertyChangedEventArgs GetPropertyChangedEventArgs(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException("propertyName cannot be null or empty.");
            }

            // Get the event args from the cache, creating them
            // and adding to the cache if necessary.
            EventArgCacheLock.Acquire();
            try
            {
                if (!EventArgCache.ContainsKey(propertyName))
                {
                    EventArgCache.Add(propertyName, new PropertyChangedEventArgs(propertyName));
                }

                return EventArgCache[propertyName];
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.InnerException.Message);
                return null;
            }
            finally
            {
                EventArgCacheLock.Release();
            }
        }

        #endregion // Public Members

        #region Protected Members

        /// <summary>
        /// Forces the property changed event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        public void ForcePropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;

            // Raise the PropertyChanged event.
            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Gets the current value.
        /// </summary>
        /// <typeparam name="T">A type with a default constructor</typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>The current value of the property</returns>
        /// <exception cref="System.ArgumentNullException">propertyName;propertyName can never be null</exception>
        protected T GetCurrentValue<T>([CallerMemberName] string propertyName = null)
            where T : new()
        {
            Debug.Assert(propertyName != null, "propertyName can never be null");

            if (this.objectCache == null)
            {
                this.objectCache = new ConcurrentDictionary<string, object>();
            }

            // Find the property it corresponds
            if (!this.objectCache.TryGetValue(propertyName, out var backingValue))
            {
                backingValue = new T();
                this.objectCache.TryAdd(propertyName, backingValue);
            }

            return (T)backingValue;
        }

        /// <summary>
        /// Sets the and raise on change.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="nonPropertyCheck">if set to <c>true</c> [non property check].</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>True if the property value was actually changed; otherwise false.</returns>
        protected bool SetAndRaiseOnChange(object value, bool nonPropertyCheck = false, [CallerMemberName] string propertyName = null)
        {
            Debug.Assert(propertyName != null, "propertyName can never be null");

            if (this.objectCache == null)
            {
                this.objectCache = new ConcurrentDictionary<string, object>();
            }

            // Find the property it corresponds to.  If doesn't exist add it
            if (!this.objectCache.TryGetValue(propertyName, out var backingValue))
            {
                this.objectCache.TryAdd(propertyName, value);
            }

            // if our current value was null, see if the new value is null
            if (backingValue == null)
            {
                // If so, nothing to do
                if (value == null)
                {
                    return false;
                }
            }
            else if (backingValue.Equals(value))
            {
                return false;
            }

            // Now set the value
            this.objectCache[propertyName] = value;

            // Raise the change
            if (nonPropertyCheck)
            {
                this.ForcePropertyChanged(propertyName);
            }
            else
            {
                this.RaisePropertyChanged(propertyName);
            }

            return true;
        }

        /// <summary>
        /// Gets the current value.
        /// </summary>
        /// <typeparam name="T">A type with a default constructor</typeparam>
        /// <param name="startingValue">A value of initial property.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>The current value of the property</returns>
        /// <exception cref="System.ArgumentNullException">propertyName;propertyName can never be null</exception>
        protected T GetCurrentNullableValue<T>(object startingValue = null, [CallerMemberName] string propertyName = null)
            where T : class
        {
            Debug.Assert(propertyName != null, "propertyName can never be null");

            if (this.objectCache == null)
            {
                this.objectCache = new ConcurrentDictionary<string, object>();
            }

            // Find the property it corresponds
            if (!this.objectCache.TryGetValue(propertyName, out var backingValue))
            {
                backingValue = startingValue;
                this.objectCache.TryAdd(propertyName, startingValue);
            }

            return (T)backingValue;
        }

        /// <summary>
        /// Sets the property and notify if changed.
        /// </summary>
        /// <typeparam name="T">A value object or one with new() constructor</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="propertyName">Name of the method.</param>
        /// <exception cref="System.ArgumentNullException">propertyName;propertyName can never be null</exception>
        [Obsolete("Use SetAndRaiseOnChange")]
        protected void ReferencedSetAndRaiseOnChange<T>(object value, [CallerMemberName] string propertyName = null)
            where T : class
        {
            Debug.Assert(propertyName != null, "propertyName can never be null");

            if (this.objectCache == null)
            {
                this.objectCache = new ConcurrentDictionary<string, object>();
            }

            // Find the property it corresponds
            if (!this.objectCache.TryGetValue(propertyName, out var backingValue))
            {
                this.objectCache.TryAdd(propertyName, null);
            }

            var currentValue = (T)backingValue;

            // if our values are the same, return
            if (currentValue == value)
            {
                return;
            }

            // Now set the value
            this.objectCache[propertyName] = value;

            // Raise the change
            this.RaisePropertyChanged(propertyName);
        }

        /// <summary>
        /// Enables the change event from the shallow binding object to propagate up to a
        /// single change event.
        /// </summary>
        /// <param name="currentValue">The current value.</param>
        /// <param name="value">The new value.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>The value to be used in the setter</returns>
        /// <remarks>This can be used only in the Set Property event for a bindable object in a bindable object</remarks>
        protected BindableObject EnablePropagationOfChangeEvents(BindableObject currentValue, BindableObject value, string propertyName)
        {
            // Do nothing if the change value is the same
            if (currentValue == value)
            {
                return currentValue;
            }

            // Remove the old change callback
            if (currentValue != null)
            {
                currentValue.PropertyChanged -= this.BindableObjectPropertyChanged;
            }

            value.PropertyChanged += this.BindableObjectPropertyChanged;
            value.classChangedPropertyName = propertyName;
            this.RaisePropertyChanged(propertyName);
            return value;
        }

        /// <summary>
        /// Derived classes can override this method to
        /// execute logic after a property is set. The
        /// base implementation does nothing.
        /// </summary>
        /// <param name="propertyName">
        /// The property which was changed.
        /// </param>
        protected virtual void AfterPropertyChanged(string propertyName)
        {
            this.PropertyChanged += OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        /// <summary>
        /// Attempts to raise the PropertyChanged event, and
        /// invokes the virtual AfterPropertyChanged method,
        /// regardless of whether the event was raised or not.
        /// </summary>
        /// <param name="propertyName">
        /// The property which was changed.
        /// </param>
        protected void RaisePropertyChanged([CallerMemberName] string propertyName="")
        {
            this.VerifyProperty(propertyName);
            this.RaisePropertyChanged(GetPropertyChangedEventArgs(propertyName));
        }

        protected void RaisePropertyChanged(string overridePropertyName, [CallerMemberName] string propertyName = "")
        {
            this.VerifyProperty(overridePropertyName);
            this.RaisePropertyChanged(GetPropertyChangedEventArgs(overridePropertyName));
        }

        /// <summary>
        /// Attempts to raise the PropertyChanged event, and
        /// invokes the virtual AfterPropertyChanged method,
        /// regardless of whether the event was raised or not.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        protected void RaisePropertyChanged(PropertyChangedEventArgs e)
        {
            // Raise the PropertyChanged event.
            this.PropertyChanged?.Invoke(this, e);

            this.AfterPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Ranges the property changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        protected void BindableObjectPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Raise the class change for the sender
            if (sender is BindableObject bindableObject)
            {
                if (string.IsNullOrWhiteSpace(this.classChangedPropertyName))
                {
                    this.RaisePropertyChanged(bindableObject.classChangedPropertyName);
                }
            }
        }

        #endregion // Protected Members

        /// <summary>
        /// Verifies the property, only used in debug mode
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        [Conditional("DEBUG")]
        private void VerifyProperty(string propertyName)
        {
            // fails in debug mode
            Type type = this.GetType();

            // Look for a public property with the specified name.
            PropertyInfo propertyInfo = null;
            while (type != null)
            {
                propertyInfo = type.GetProperty(
                    propertyName,
                    BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
                if (propertyInfo != null)
                {
                    break;
                }

                type = type.BaseType;
            }

            if (propertyInfo == null)
            {
                // The property could not be found,
                // so alert the developer of the problem.
                var msg = $"{propertyName} is not a public property of {this.GetType().FullName}";

                Debug.Fail(msg);
            }
        }
    }
}