﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileUtilities.cs" company="MTI Film, LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>Michael Russell</author>
// <date>3/20/2012 2:40:01 PM</date>
// <summary>Various File Utilities</summary>
// --------------------------------------------------------------------------------------------------------------------

using MtiFilm.Core;

namespace MtiSimple.Core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    /// <summary>
    /// Various File Utilities
    /// </summary>
    public class FileUtilities
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The list of video extensions we support.
        /// </summary>
        public static readonly List<string> VideoExtensions = new List<string> { ".ari", ".arx", ".braw", ".cine", ".dpx", ".dng", ".exr", ".sxr", ".mov", ".mxf", ".mp4", ".mts", ".r3d", ".mpg", ".m2v", ".jpg", ".jpeg", ".rmf", ".tif", ".tiff", ".cap", ".xml", ".vrw", ".j2c", ".png", ".avi", ".ts"};

        /// <summary>
        /// The list of video types with multiple files per clip.
        /// </summary>
        public static readonly List<string> VideoMultipleFilesPerClipExtensions = new List<string> { ".ari", ".arx", ",braw", ".dpx", ".dng", ".exr", ".sxr", ".r3d", ".jpg", ".jpeg", ".rmf", ".tif", ".tiff", ".vrw", ".j2c", ".png" };

        /// <summary>
        /// The list of audio extensions we support.
        /// </summary>
        public static readonly List<string> AudioExtensions = new List<string> { ".wav", ".aif", ".aiff", ".mp3", ".bwf" };

        /// <summary>
        /// The list of image file extensions we support
        /// </summary>
        public static readonly List<string> ImageExtensions = new List<string> { ".jpg", ",jpeg", ".dpx", ".dng", ".exr", ".sxr", ".rmf", ".tif", ".tiff", ".vrw", ".png" };

        /// <summary>
        /// The subtitle extensions we support.
        /// </summary>
        public static readonly List<string> SubtitleExtensions = new List<string> { ".xml", ".cap", ".tt", ".ttml", ".bdn", ".itt" };

        /// <summary>
        /// The valid filename separators
        /// </summary>
        public static readonly char[] ValidFilenameSeparators = { '.', '_', '-' };

        /// <summary>
        /// The report interval milliseconds.
        /// </summary>
        private const int ReportIntervalMilliseconds = 2000;

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////
        #region Private fields

        /// <summary>
        /// The import file filter.
        /// </summary>
        private static string importFileFilter;

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Creates the directory if needed.
        /// </summary>
        /// <param name="fullPath">The full path.</param>
        public static void CreateDirectoryIfNeeded(string fullPath)
        {
            if (fullPath != null && !Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }
        }

        /// <summary>
        /// Gets the full filename.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="subDirectory">The sub directory.</param>
        /// <param name="baseFilename">The base filename.</param>
        /// <returns>The full filename.</returns>
        public static string GetFullFilename(string path, string subDirectory, string baseFilename)
        {
            return string.IsNullOrWhiteSpace(subDirectory) ? Path.Combine(path, baseFilename) : Path.Combine(path, subDirectory, baseFilename);
        }

        /// <summary>
        /// Gets the subdirectory between the end of the path and the beginning of the actual filename.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="path">The path.</param>
        /// <returns>The subdirectory between the end of the path and the beginning of the actual filename.</returns>
        public static string GetSubDirectory(string filename, string path)
        {
            var subDirectory = string.Empty;

            if (Path.GetDirectoryName(filename) != path)
            {
                var tmpDirectory = Path.GetDirectoryName(filename);

                if (tmpDirectory != null)
                {
                    // + 1 is for the Path.DirectorySeparatorChar
                    subDirectory = tmpDirectory.Substring(path.Length + 1);
                    if (IsRootPathWithSeparator(path))
                    {
                        subDirectory = tmpDirectory.Substring(path.Length);
                    }
                }
            }

            return subDirectory;
        }

        /// <summary>
        /// Gets the sub directory for directory.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="path">The path.</param>
        /// <returns>The sub directory.</returns>
        public static string GetSubDirectoryForDirectory(string directory, string path)
        {
            var subDirectory = string.Empty;

            if (directory != path)
            {
                var tmpDirectory = directory;

                if (tmpDirectory != null)
                {
                    // + 1 is for the Path.DirectorySeparatorChar
                    subDirectory = tmpDirectory.Substring(path.Length + 1);
                    if (IsRootPathWithSeparator(path))
                    {
                        subDirectory = tmpDirectory.Substring(path.Length);
                    }
                }
            }

            return subDirectory;
        }

        /// <summary>
        /// Gets the last sub directory.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>The last sub directory.</returns>
        public static string GetLastSubDirectory(string path)
        {
            var pathParts = GetPathParts(path);
            if (pathParts.IsEmpty() || pathParts.Count == 1)
            {
                // This is the root
                return string.Empty;
            }

            return pathParts.Last();
        }

        /// <summary>
        /// Gets the filename prefix.
        /// </summary>
        /// <param name="filenameNoExtension">The filename no extension.</param>
        /// <returns>The filename prefix.</returns>
        public static string GetFilenamePrefix(string filenameNoExtension)
        {
            var filenamePrefix = filenameNoExtension;
            var regex = new Regex(@"(_|\.|-)+(\d+)$", RegexOptions.IgnoreCase);
            if (regex.IsMatch(filenameNoExtension))
            {
                var lastIndexOf = filenameNoExtension.LastIndexOfAny(ValidFilenameSeparators);
                if (lastIndexOf >= 0)
                {
                    filenamePrefix = filenameNoExtension.Remove(lastIndexOf);
                    return filenamePrefix;
                }
            }

            // Some files have ending digits with widthxheight. These aren't frame numbers.
            regex = new Regex(@"\d+x\d+$");
            if (regex.IsMatch(filenameNoExtension))
            {
                return filenamePrefix;
            }

            // Some files no not have a period or underscore separating the prefix from the sequence number
            regex = new Regex(@"\d+$");
            var num = regex.Match(filenameNoExtension).Value;
            if (num.Length >= 3)
            {
                return filenameNoExtension.Substring(0, filenameNoExtension.Length - num.Length);
            }

            return filenamePrefix;
        }

        /// <summary>
        /// Gets the filename ending digit count.
        /// </summary>
        /// <param name="filenameNoExtension">The filename no extension.</param>
        /// <returns>The filename ending digit count.</returns>
        public static int GetFilenameEndingDigitCount(string filenameNoExtension)
        {
            var filenamePrefix = GetFilenamePrefix(filenameNoExtension);
            if (string.IsNullOrWhiteSpace(filenamePrefix))
            {
                return filenameNoExtension.Length;
            }

            var digitsAndSeparator = filenameNoExtension.Replace(filenamePrefix, string.Empty);
            var indexOf = digitsAndSeparator.IndexOfAny(ValidFilenameSeparators);
            if (indexOf >= 0)
            {
                return digitsAndSeparator.Length - 1;
            }

            return digitsAndSeparator.Length;
        }

        /// <summary>
        /// Gets the filename digit separator.
        /// </summary>
        /// <param name="filenameNoExtension">The filename no extension.</param>
        /// <returns>The filename digit separator.</returns>
        public static string GetFilenameDigitSeparator(string filenameNoExtension)
        {
            var filenamePrefix = GetFilenamePrefix(filenameNoExtension);
            if (string.IsNullOrWhiteSpace(filenamePrefix))
            {
                return string.Empty;
            }

            var digitsAndSeparator = filenameNoExtension.Replace(filenamePrefix, string.Empty);
            var lastIndexOf = digitsAndSeparator.LastIndexOfAny(ValidFilenameSeparators);
            if (lastIndexOf >= 0)
            {
                return digitsAndSeparator[lastIndexOf].ToString(CultureInfo.InvariantCulture);
            }

            return string.Empty;
        }

        /// <summary>
        /// Compares two files by checking blocks of data.
        /// </summary>
        /// <param name="file1">The first file.</param>
        /// <param name="file2">The second file.</param>
        /// <returns>
        /// 'true' if files are the same
        /// </returns>
        public static bool AreFilesEqual(string file1, string file2)
        {
            const int BufferSize = 2048 * 2;

            var fileInfo1 = new FileInfo(file1);
            var fileinfo2 = new FileInfo(file2);

            // If the lengths of the files don't match, then clearly the files can't be the same.
            if (fileInfo1.Length != fileinfo2.Length)
            {
                return false;
            }

            var retVal = false;

            // Compare one block at a time
            using (var stream1 = fileInfo1.OpenRead())
            {
                using (var stream2 = fileinfo2.OpenRead())
                {
                    var buffer1 = new byte[BufferSize];
                    var buffer2 = new byte[BufferSize];

                    var done = false;

                    // Read / compare chunks
                    while (!done)
                    {
                        var count1 = stream1.Read(buffer1, 0, BufferSize);
                        var count2 = stream2.Read(buffer2, 0, BufferSize);

                        // If buffer sizes differ, then files are different.
                        if (count1 != count2)
                        {
                            done = true;
                        }
                        else
                        {
                            // If no bytes read, then done.
                            if (count1 == 0)
                            {
                                done = true;
                                retVal = true;
                            }
                            else
                            {
                                // Number of chunks to process.
                                var iterations = (int)Math.Ceiling((double)count1 / sizeof(long));

                                // Move thru the buffer 64-bits (one long) at a time and compare
                                for (var i = 0; i < iterations; i++)
                                {
                                    if (BitConverter.ToInt64(buffer1, i * sizeof(long)) != BitConverter.ToInt64(buffer2, i * sizeof(long)))
                                    {
                                        done = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return retVal;
        }

        /// <summary>
        /// Determines whether the first file is newer than the second file.
        /// </summary>
        /// <param name="file1">The first file.</param>
        /// <param name="file2">The second file.</param>
        /// <returns>
        ///   <c>true</c> if file1 is newer; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFileNewer(string file1, string file2)
        {
            if ((new FileInfo(file1)).LastAccessTimeUtc > (new FileInfo(file2)).LastAccessTimeUtc)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the common path part between two paths.
        /// </summary>
        /// <param name="oldPath">The oldPath.</param>
        /// <param name="newPath">The newPath.</param>
        /// <returns>The common path part.</returns>
        public static string GetMatchingTerminalPath(string oldPath, string newPath)
        {
            var matchingParts = GetMatchingTerminalPathParts(oldPath, newPath);
            return string.Join(new string(Path.DirectorySeparatorChar, 1), matchingParts);
        }

        /// <summary>
        /// Gets the matching terminal path parts count.
        /// </summary>
        /// <param name="path1">The path1.</param>
        /// <param name="path2">The path2.</param>
        /// <returns>The number of matching terminal path parts.</returns>
        public static int GetMatchingTerminalPathPartsCount(string path1, string path2)
        {
            return GetMatchingTerminalPathParts(path1, path2).Count;
        }

        /// <summary>
        /// Trims the matching terminal part of the two supplied paths
        /// </summary>
        /// <param name="oldPath">The old path.</param>
        /// <param name="newPath">The new path.</param>
        /// <returns>The part before the matching part at the end.</returns>
        public static string TrimMatchingTerminalPathPart(string oldPath, string newPath)
        {
            var oldPathParts = GetPathParts(oldPath);
            var newPathParts = GetPathParts(newPath);
            var uniqueParts = GetPathParts(newPath);

            for (var i = 0; i < oldPathParts.Count && i < newPathParts.Count; i++)
            {
                var oldPathPart = oldPathParts[oldPathParts.Count - i - 1];
                var newPathPart = newPathParts[newPathParts.Count - i - 1];

                if (oldPathPart == newPathPart)
                {
                    uniqueParts.RemoveAt(newPathParts.Count - i - 1);
                }
                else
                {
                    break;
                }
            }

            return string.Join(new string(Path.DirectorySeparatorChar, 1), uniqueParts);
        }

        /// <summary>
        /// Gets the common parent directory.
        /// </summary>
        /// <param name="path1">The path1.</param>
        /// <param name="path2">The path2.</param>
        /// <returns>The common parent directory.</returns>
        public static string GetCommonParentDirectory(string path1, string path2)
        {
            var path1Parts = GetPathParts(path1);
            var path2Parts = GetPathParts(path2);
            var commonParts = new List<string>();

            for (var i = 0; i < path1Parts.Count && i < path2Parts.Count; i++)
            {
                var path1Part = path1Parts[i];
                var path2Part = path2Parts[i];

                if (path1Part == path2Part)
                {
                    commonParts.Add(path1Part);
                }
                else
                {
                    break;
                }
            }

            if (commonParts.Count == 1)
            {
                return commonParts[0] + Path.DirectorySeparatorChar;
            }

            return string.Join(new string(Path.DirectorySeparatorChar, 1), commonParts);
        }

        /// <summary>
        /// Gets the empty directories.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <returns>A list of empty directories.</returns>
        public static List<string> GetEmptyDirectories(string directory)
        {
            var directoryList = new List<string> { directory };
            GetSubdirectories(directory, directoryList);
            var emptyDirectories = new List<string>();
            foreach (var path in directoryList)
            {
                try
                {
                    if (Directory.GetDirectories(path).Length == 0 && Directory.GetFiles(path).Length == 0)
                    {
                        emptyDirectories.Add(path);
                    }
                }
                catch (Exception exception)
                {
     //               TraceEventType.Warning.LogMessage("Unable to check " + path + ": " + exception.GetInnermostExceptionMessage());
                }
            }

            return emptyDirectories;
        }

        /// <summary>
        /// Recursively get all subdirectories.
        /// Optionally skips hidden directories.
        /// Avoids the problem of running into a directory without access privileges.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="directories">The directories.</param>
        /// <param name="allowHidden">if set to <c>true</c> allow hidden directories.</param>
        public static void GetSubdirectories(string path, List<string> directories, bool allowHidden = true)
        {
            if (directories == null || !Directory.Exists(path))
            {
                return;
            }

            IEnumerable<string> directoryList;
            try
            {
                directoryList = Directory.EnumerateDirectories(path, "*", SearchOption.TopDirectoryOnly);
            }
            catch (Exception ex)
            {
                if (ex is UnauthorizedAccessException)
                {
    //                TraceEventType.Error.LogMessage("Could not access directory: " + path);
                }
                else if (ex is DirectoryNotFoundException)
                {
     //               TraceEventType.Error.LogMessage("could not find directory: " + path);
                }
                else if (ex is IOException)
                {
      //              TraceEventType.Error.LogMessage("Could not access directory: " + path + ". Error: " + ex.GetInnermostExceptionMessage());
                }
                else if (ex is AggregateException aggregateException)
                {
                    foreach (var inner in aggregateException.InnerExceptions)
                    {
                        if (inner is UnauthorizedAccessException)
                        {
           //                 TraceEventType.Error.LogMessage("Could not access directory: " + path);
                            continue;
                        }

                        if (inner is DirectoryNotFoundException)
                        {
           //                 TraceEventType.Error.LogMessage("could not find directory: " + path);
                            continue;
                        }

                        if (inner is IOException)
                        {
         //                   TraceEventType.Error.LogMessage("Could not access directory: " + path + ". Error: " + ex.GetInnermostExceptionMessage());
                        }
                    }
                }

                throw;
            }

            foreach (var directory in directoryList)
            {
                try
                {
                    // ReSharper disable once UnusedVariable
                    var firstFile = Directory.EnumerateFiles(directory).AsParallel().FirstOrDefault();
                }
                catch (Exception ex)
                {
                    if (ex is UnauthorizedAccessException)
                    {
//TraceEventType.Error.LogMessage("Could not access directory: " + directory);
                        continue;
                    }

                    if (ex is DirectoryNotFoundException)
                    {
       //                 TraceEventType.Error.LogMessage("could not find directory: " + directory);
                        continue;
                    }

                    if (ex is IOException)
                    {
         //              TraceEventType.Error.LogMessage("Could not access directory: " + directory + ". Error: " + ex.GetInnermostExceptionMessage());
                        continue;
                    }

                    if (ex is AggregateException aggregateException)
                    {
                        foreach (var inner in aggregateException.InnerExceptions)
                        {
                            if (inner is UnauthorizedAccessException)
                            {
          //                      TraceEventType.Error.LogMessage("Could not access directory: " + directory);
                                continue;
                            }

                            if (inner is DirectoryNotFoundException)
                            {
          //                      TraceEventType.Error.LogMessage("could not find directory: " + directory);
                                continue;
                            }

                            if (inner is IOException)
                            {
           //                     TraceEventType.Error.LogMessage("Could not access directory: " + directory + ". Error: " + ex.GetInnermostExceptionMessage());
                            }
                        }

                        continue;
                    }

                    throw;
                }

                if (!allowHidden)
                {
                    var directoryInfo = new DirectoryInfo(directory);
                    if ((directoryInfo.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                    {
                        continue;
                    }
                }

                directories.Add(directory);
                GetSubdirectories(directory, directories, allowHidden);
            }
        }

        /// <summary>
        /// Makes multiple copies the specified file and simultaneously gets its hash using the given hash type algorithm.
        /// </summary>
        /// <param name="inputStream">The input stream.</param>
        /// <param name="destFilenames">The list of destination filenames.</param>
        /// <param name="hashType">The hash type to use</param>
        /// <param name="bytesCopied">The bytes copied.</param>
        /// <param name="progress">The progress.</param>
        /// <returns>
        /// the hash string
        /// </returns>
        public static string CopyMultipleAndHashSource(FileStream inputStream, List<string> destFilenames, HashType hashType, out long bytesCopied, IProgress<double> progress = null)
        {
            switch (hashType)
            {
                case HashType.None:
                    return CopyMultipleAndHashSource(inputStream, destFilenames, progress, out bytesCopied);
                case HashType.Md5:
                    return CopyMultipleAndHashSource(inputStream, destFilenames, progress, out bytesCopied, MD5.Create());
                case HashType.Sha1:
                    return CopyMultipleAndHashSource(inputStream, destFilenames, progress, out bytesCopied, SHA1.Create());
                case HashType.Sha256:
                    return CopyMultipleAndHashSource(inputStream, destFilenames, progress, out bytesCopied, SHA256.Create());
                case HashType.Sha384:
                    return CopyMultipleAndHashSource(inputStream, destFilenames, progress, out bytesCopied, SHA384.Create());
                case HashType.Sha512:
                    return CopyMultipleAndHashSource(inputStream, destFilenames, progress, out bytesCopied, SHA512.Create());
            }

            bytesCopied = 0L;
            return string.Empty;
        }

        /// <summary>
        /// Makes multiple copies the specified file and simultaneously gets its hash using the given hash algorithm.
        /// </summary>
        /// <param name="inputStream">The input stream.</param>
        /// <param name="destFilenames">The list of destination filenames.</param>
        /// <param name="progress">The progress.</param>
        /// <param name="bytesCopied">The bytes copied.</param>
        /// <param name="hashAlg">The hash algorithm to use</param>
        /// <returns>
        /// the hash string
        /// </returns>
        public static string CopyMultipleAndHashSource(FileStream inputStream, List<string> destFilenames, IProgress<double> progress, out long bytesCopied, HashAlgorithm hashAlg = null)
        {
            var progressStopwatch = Stopwatch.StartNew();
            var totalBytes = inputStream.Length;
            bytesCopied = 0L;

            // Create the output streams
            var outputStreams = new List<FileStream>();
            foreach (var destFilename in destFilenames)
            {
                try
                {
                    outputStreams.Add(new FileStream(destFilename, FileMode.Create, FileAccess.Write));
                }
                catch (FileNotFoundException ex)
                {
                    throw new FileNotFoundException(string.Format("FileNotFound Exception opening: {0}: {1}", destFilename, ex.GetInnermostExceptionMessage()));
                }
            }

            // reading at least 1MB at a time in first round of tests proved optimal
            var buffer = new byte[1024 * 1024];

            int bytesRead;

            try
            {
                bytesRead = inputStream.Read(buffer, 0, buffer.Length);
            }
            catch (FileNotFoundException ex)
            {
                throw new FileNotFoundException(string.Format("FileNotFound Exception reading: {0}: {1}", inputStream.Name, ex.GetInnermostExceptionMessage()));
            }
            catch (IOException ex)
            {
                throw new IOException(string.Format("I/O Exception reading: {0}: {1}", inputStream.Name, ex.GetInnermostExceptionMessage()));
            }

            while (bytesRead > 0)
            {
                if (hashAlg != null)
                {
                    hashAlg.TransformBlock(buffer, 0, bytesRead, null, 0);
                }

                foreach (var outputStream in outputStreams)
                {
                    try
                    {
                        outputStream.Write(buffer, 0, bytesRead);
                    }
                    catch (FileNotFoundException ex)
                    {
                        throw new FileNotFoundException(string.Format("FileNotFound Exception writing: {0}: {1}", outputStream.Name, ex.GetInnermostExceptionMessage()));
                    }
                    catch (IOException ex)
                    {
                        throw new IOException(string.Format("I/O Exception writing: {0}: {1}", outputStream.Name, ex.GetInnermostExceptionMessage()));
                    }
                }

                bytesCopied += bytesRead;
                if (progress != null && progressStopwatch.ElapsedMilliseconds > 2000)
                {
                    progress.Report((bytesCopied / (double)totalBytes) * 100.0);
                    progressStopwatch.Restart();
                }

                try
                {
                    bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                }
                catch (FileNotFoundException ex)
                {
                    throw new FileNotFoundException(string.Format("FileNotFound reading: {0}: {1}", inputStream.Name, ex.GetInnermostExceptionMessage()));
                }
                catch (IOException ex)
                {
                    throw new IOException(string.Format("I/O Exception reading: {0}: {1}", inputStream.Name, ex.GetInnermostExceptionMessage()));
                }
            }

            if (hashAlg != null)
            {
                hashAlg.TransformFinalBlock(buffer, 0, 0);
            }

            foreach (var outputStream in outputStreams)
            {
                try
                {
                    outputStream.Close();
                }
                catch (FileNotFoundException ex)
                {
                    throw new FileNotFoundException(string.Format("FileNotFound closing: {0}: {1}", outputStream.Name, ex.GetInnermostExceptionMessage()));
                }
                catch (IOException ex)
                {
                    throw new IOException(string.Format("I/O Exception closing: {0}: {1}", outputStream.Name, ex.GetInnermostExceptionMessage()));
                }
            }

            bytesCopied = inputStream.Length;
            return hashAlg == null ? string.Empty : HashBinaryToString(hashAlg.Hash);
        }

        /// <summary>
        /// Gets the hash with progress reporting.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="hashType">Type of the hash.</param>
        /// <param name="bytesVerified">The number of bytes verified.</param>
        /// <param name="progress">The progress.</param>
        /// <returns>The hash.</returns>
        public static string GetHashWithProgress(string filename, HashType hashType, out long bytesVerified, IProgress<double> progress = null)
        {
            if (hashType == HashType.None)
            {
                bytesVerified = 0;
                return string.Empty;
            }

            var progressStopwatch = Stopwatch.StartNew();

            try
            {
                using (var stream = File.OpenRead(filename))
                {
                    if (hashType == HashType.XxHash)
                    {
                        return GetHashWithProgressUsingXxHash(stream, progressStopwatch, out bytesVerified, progress);
                    }

                    return GetHashWithProgressUsingHashAlgorithm(hashType, stream, progressStopwatch, out bytesVerified, progress);
                }
            }
            catch (FileNotFoundException ex)
            {
                throw new FileNotFoundException($"FileNotFound Exception opening/reading: {filename}: {ex.GetInnermostExceptionMessage()}");
            }
            catch (IOException ex)
            {
                throw new IOException($"I/O Exception opening/reading: {filename}: {ex.GetInnermostExceptionMessage()}");
            }
        }

        /// <summary>
        /// Gets the hash of the specified file using the given hash type algorithm.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="hashType">The hash type to use</param>
        /// <returns>the hash string</returns>
        public static string GetHash(string filename, HashType hashType)
        {
            using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                // Create a new instance of the MD5CryptoServiceProvider object.
                var data = new byte[0];

                switch (hashType)
                {
                    case HashType.None:
                        return string.Empty;
                    case HashType.Md5:
                        data = new MD5CryptoServiceProvider().ComputeHash(stream);
                        break;
                    case HashType.Sha1:
                        data = new SHA1CryptoServiceProvider().ComputeHash(stream);
                        break;
                    case HashType.Sha256:
                        data = new SHA256CryptoServiceProvider().ComputeHash(stream);
                        break;
                    case HashType.Sha384:
                        data = new SHA384CryptoServiceProvider().ComputeHash(stream);
                        break;
                    case HashType.Sha512:
                        data = new SHA512CryptoServiceProvider().ComputeHash(stream);
                        break;
                }

                // Create a new Stringbuilder to collect the bytes.
                var hexString = new StringBuilder();

                // Loop through each byte of the hashed data and format each one as a hexadecimal string.
                foreach (var byteVal in data)
                {
                    hexString.Append(byteVal.ToString("x2"));
                }

                // Return the hexadecimal string.
                return hexString.ToString();
            }
        }

        /// <summary>
        /// Copies the specified file and simultaneously gets its hash using the given hash type algorithm.
        /// </summary>
        /// <param name="sourceFilename">The filename.</param>
        /// <param name="destFilename">The destination filename.</param>
        /// <param name="hashType">The hash type to use</param>
        /// <returns>the hash string</returns>
        public static string CopyAndHashSource(string sourceFilename, string destFilename, HashType hashType)
        {
            switch (hashType)
            {
                case HashType.None:
                    // TODO: exception?
                    return string.Empty;
                case HashType.Md5:
                    return CopyAndHashSource(sourceFilename, destFilename, MD5.Create());
                case HashType.Sha1:
                    return CopyAndHashSource(sourceFilename, destFilename, SHA1.Create());
                case HashType.Sha256:
                    return CopyAndHashSource(sourceFilename, destFilename, SHA256.Create());
                case HashType.Sha384:
                    return CopyAndHashSource(sourceFilename, destFilename, SHA384.Create());
                case HashType.Sha512:
                    return CopyAndHashSource(sourceFilename, destFilename, SHA512.Create());
            }

            // TODO: exception?
            return string.Empty;
        }

        /// <summary>
        /// Copies the specified file and simultaneously gets its hash using the given hash type algorithm.
        /// </summary>
        /// <param name="sourceFilename">The source filename.</param>
        /// <param name="destFilename">The destination filename.</param>
        /// <param name="hashAlg">The hash algorithm to use</param>
        /// <returns>the hash string</returns>
        public static string CopyAndHashSource(string sourceFilename, string destFilename, HashAlgorithm hashAlg)
        {
            using (var inputStream = new FileStream(sourceFilename, FileMode.Open, FileAccess.Read))
            {
                using (var outputStream = new FileStream(destFilename, FileMode.Create, FileAccess.Write))
                {
                    // reading at least 1MB at a time in first round of tests proved optimal
                    var buffer = new byte[1024 * 1024];

                    var read = inputStream.Read(buffer, 0, buffer.Length);

                    while (read > 0)
                    {
                        hashAlg.TransformBlock(buffer, 0, read, null, 0);
                        outputStream.Write(buffer, 0, read);
                        read = inputStream.Read(buffer, 0, buffer.Length);
                    }

                    hashAlg.TransformFinalBlock(buffer, 0, 0);
                    outputStream.Close();

                    return HashBinaryToString(hashAlg.Hash);
                }
            }
        }

        /// <summary>
        /// Converts the binary hash data to its string representation.
        /// </summary>
        /// <param name="hashdata">
        /// The hashdata byte array.
        /// </param>
        /// <returns>
        /// The string representation of the hash.
        /// </returns>
        public static string HashBinaryToString(byte[] hashdata)
        {
            // Create a new Stringbuilder to collect the bytes.
            var hexString = new StringBuilder();

            // Loop through each byte of the hashed data and format each one as a hexadecimal string.
            foreach (var byteVal in hashdata)
            {
                hexString.Append(byteVal.ToString("x2"));
            }

            // Return the hexadecimal string.
            return hexString.ToString();
        }

        /// <summary>
        /// Determines whether the specified file is a media file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>True iff the file is a media file.</returns>
        public static bool IsMediaFile(string filename)
        {
            var filenameOnly = Path.GetFileName(filename);
            if (string.IsNullOrWhiteSpace(filenameOnly))
            {
                return false;
            }

            return (IsVideoFile(filename) || IsAudioFile(filename)) && !filenameOnly.StartsWith(".");
        }

        /// <summary>
        /// Determines whether the specified filename is a video file by checking the extension.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        ///   <c>true</c> if the specified filename is a video file; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsVideoFile(string filename)
        {
            var extension = Path.GetExtension(filename);
            return IsVideoExtension(extension);
        }

        /// <summary>
        /// Determines whether or not the specified extension is a video extension.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// <c>true</c> if the specified extension is a video extension; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsVideoExtension(string extension)
        {
            var extensionToLower = extension.ToLower();
            return VideoExtensions.Contains(extensionToLower) || SubtitleExtensions.Contains(extensionToLower);
        }

        /// <summary>
        /// Determines whether the specified filename is an image file by checking the extension.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        ///   <c>true</c> if the specified filename is an image file; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsImageFile(string filename)
        {
            var extension = Path.GetExtension(filename);
            return IsImageExtension(extension);
        }

        /// <summary>
        /// Determines whether or not the specified extension is an image extension.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// <c>true</c> if the specified extension is an image extension; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsImageExtension(string extension)
        {
            var extensionToLower = extension.ToLower();
            return ImageExtensions.Contains(extensionToLower);
        }

        /// <summary>
        /// Determines whether the specified filename is an audio file by checking the extension.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        ///   <c>true</c> if the specified filename is an audio file; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAudioFile(string filename)
        {
            var extension = Path.GetExtension(filename);
            return IsAudioExtension(extension);
        }

        /// <summary>
        /// Determines whether [is audio extension] [the specified extension].
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// <c>true</c> if [is audio extension] [the specified extension]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAudioExtension(string extension)
        {
            var extensionToLower = extension.ToLower();
            return AudioExtensions.Contains(extensionToLower);
        }

        /// <summary>
        /// Gets the video files filter.
        /// </summary>
        /// <returns>The video file filter.</returns>
        public static string GetVideoFilesFilter()
        {
            var videoFilterFirstPart = new StringBuilder();
            var videoFilterSecondPart = new StringBuilder();
            videoFilterFirstPart.Append("All Video Files (");
            foreach (var extension in VideoExtensions)
            {
                var filter = "*" + extension;
                videoFilterFirstPart.Append(filter + ", ");
                videoFilterSecondPart.Append(filter + ";");
            }

            // Remove the unnecessary ", " and ";"
            var firstPartLength = videoFilterFirstPart.Length;
            videoFilterFirstPart.Remove(firstPartLength - 2, 2);
            var secondPartLength = videoFilterSecondPart.Length;
            videoFilterSecondPart.Remove(secondPartLength - 1, 1);

            // Append the )| to first part
            videoFilterFirstPart.Append(")|");
            return videoFilterFirstPart.Append(videoFilterSecondPart).ToString();
        }

        /// <summary>
        /// Gets the audio files filter.
        /// </summary>
        /// <returns>The audio file filter.</returns>
        public static string GetAudioFilesFilter()
        {
            var audioFilterFirstPart = new StringBuilder();
            var audioFilterSecondPart = new StringBuilder();
            audioFilterFirstPart.Append("All Audio Files (");
            foreach (var extension in AudioExtensions)
            {
                var filter = "*" + extension;
                audioFilterFirstPart.Append(filter + ", ");
                audioFilterSecondPart.Append(filter + ";");
            }

            // Remove the unnecessary ", " and ";"
            var firstPartLength = audioFilterFirstPart.Length;
            audioFilterFirstPart.Remove(firstPartLength - 2, 2);
            var secondPartLength = audioFilterSecondPart.Length;
            audioFilterSecondPart.Remove(secondPartLength - 1, 1);

            // Append the )| to first part
            audioFilterFirstPart.Append(")|");
            return audioFilterFirstPart.Append(audioFilterSecondPart).ToString();
        }

        /// <summary>
        /// Gets all files filter.
        /// </summary>
        /// <returns>the filter for all types.</returns>
        public static string GetAllFilesFilter()
        {
            if (!string.IsNullOrWhiteSpace(importFileFilter))
            {
                return importFileFilter;
            }

            var allFiles = GetAllSupportedFilesFilter();
            var video = GetVideoFilesFilter();
            var audio = GetAudioFilesFilter();
            var predefinedFilters = allFiles + "|" + video + "|" + audio;

            // Go through the extensions and make a filter for each
            var allExtensions = new List<string>(VideoExtensions);
            allExtensions.AddRange(AudioExtensions);
            foreach (var extension in VideoExtensions)
            {
                predefinedFilters += "|";
                var extensionFilter = new StringBuilder(extension + " Files");
                extensionFilter.Append(" (*" + extension + ")");
                extensionFilter.Append("|*" + extension);
                predefinedFilters += extensionFilter.ToString();
            }

            importFileFilter = predefinedFilters + "|All Files (*.*)|*.*";
            return importFileFilter;
        }

        /// <summary>
        /// Gets the subtitle files filter.
        /// </summary>
        /// <returns>The subtitle files filter.</returns>
        public static string GetSubtitleFilesFilter()
        {
            var subtitleFilterFirstPart = new StringBuilder();
            var subtitleFilterSecondPart = new StringBuilder();
            subtitleFilterFirstPart.Append("All Subtitle Files (");
            foreach (var extension in SubtitleExtensions)
            {
                var filter = "*" + extension;
                subtitleFilterFirstPart.Append(filter + ", ");
                subtitleFilterSecondPart.Append(filter + ";");
            }

            // Remove the unnecessary ", " and ";"
            var firstPartLength = subtitleFilterFirstPart.Length;
            subtitleFilterFirstPart.Remove(firstPartLength - 2, 2);
            var secondPartLength = subtitleFilterSecondPart.Length;
            subtitleFilterSecondPart.Remove(secondPartLength - 1, 1);

            // Append the )| to first part
            subtitleFilterFirstPart.Append(")|");
            return subtitleFilterFirstPart.Append(subtitleFilterSecondPart).ToString();
        }

        /// <summary>
        /// Gets the base filename.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>The base filename with no extension and no file index part for file sequences or multi-part files</returns>
        public static string GetBaseFilename(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
            {
                return filename;
            }

            var filenameNoExtension = Path.GetFileNameWithoutExtension(filename);
            var extension = Path.GetExtension(filename);

            var baseFilename = filenameNoExtension;
            if (IsMultiFilePerClip(extension))
            {
                baseFilename = GetFilenamePrefix(filenameNoExtension);
            }

            return baseFilename;
        }

        /// <summary>
        /// Determines whether the specified extension is a multi file per clip extension.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// <c>true</c> if the specified extension is multi file per clip; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsMultiFilePerClip(string extension)
        {
            var extensionToLower = extension.ToLower();
            return VideoMultipleFilesPerClipExtensions.Contains(extensionToLower);
        }

        /// <summary>
        /// Determines whether the specified extension is a file per frame sequence extension.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>True if the specified extension is a file per frame sequence extension.</returns>
        public static bool IsFilePerFrameSequence(string extension)
        {
            if (string.IsNullOrWhiteSpace(extension))
            {
                return false;
            }

            return IsMultiFilePerClip(extension)
                && extension.ToLower() != ".r3d"
                && extension.ToLower() != ".braw";
        }

        /// <summary>
        /// Determines whether the specified extension is a quicktime file extension.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// <c>true</c> if the specified extension is quicktime; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsQuicktimeFile(string extension)
        {
            var extensionToLower = extension.ToLower();
            return extensionToLower == ".mov";
        }

        /// <summary>
        /// Determines whether is phantom file.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>true if the given file extension is Phantom</returns>
        public static bool IsPhantomFile(string extension)
        {
            var extensionToLower = extension.ToLower();
            return extensionToLower == ".cine";
        }

        /// <summary>
        /// Determines whether the specified extension is a MXF file extension.
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>
        ///   <c>true</c> if extension is mxf; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsMxfFile(string extension)
        {
            var extensionToLower = extension.ToLower();
            return extensionToLower == ".mxf";
        }

        /// <summary>
        /// Determines whether the specified directory is a canon C300 directory.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// <c>true</c> if directory is canon C300; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsCanonC300Directory(string directory, string extension)
        {
            return extension.Equals(".mxf", StringComparison.InvariantCultureIgnoreCase)
                   && Regex.IsMatch(directory, "CONTENTS\\\\CLIPS", RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Determines whether the directory is a p2 directory.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// <c>true</c> if this is a p2 directory; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsP2Directory(string directory, string extension)
        {
            return extension.Equals(".mxf", StringComparison.InvariantCultureIgnoreCase)
                   && Regex.IsMatch(directory, "CONTENTS", RegexOptions.IgnoreCase)
                   && !directory.EndsWith("AVCLIP", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Determines whether or not this is a red quick time directory.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="extension">The extension.</param>
        /// <returns>
        ///   <c>true</c> if this directory is a red quicktime directory; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsRedQuickTimeDirectory(string directory, string extension)
        {
            return (extension.Contains(".MOV") || extension.Contains(".mov")) && directory.EndsWith(".RDC", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Gets the ordered files in a directory with specified extension.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="extension">The extension.</param>
        /// <returns>The ordered list of files.</returns>
        public static List<string> GetOrderedFiles(string directory, string extension)
        {
            var files = Directory.EnumerateFiles(directory, "*" + extension).Where(
                            f =>
                            {
                                var fileName = Path.GetFileName(f);
                                return !string.IsNullOrWhiteSpace(fileName) && !fileName.StartsWith("._");
                            }).OrderBy(f => f).ToList();

            return files;
        }

        /// <summary>
        /// Gets the associated p2 file list.
        /// </summary>
        /// <param name="p2VideoFilename">The p2 video filename.</param>
        /// <returns>The associated file list.</returns>
        public static List<string> GetAssociatedP2FileList(string p2VideoFilename)
        {
            var associatedFileList = new List<string>();
            var baseFilename = Path.GetFileNameWithoutExtension(p2VideoFilename);
            var directoryName = Path.GetDirectoryName(p2VideoFilename);
            if (string.IsNullOrWhiteSpace(directoryName))
            {
                return associatedFileList;
            }

            var xmlDirectory = directoryName.Replace("VIDEO", "CLIP");
            if (Directory.Exists(xmlDirectory))
            {
                associatedFileList.AddRange(Directory.GetFiles(xmlDirectory, baseFilename + ".XML"));
            }

            var audioDirectory = directoryName.Replace("VIDEO", "AUDIO");
            if (Directory.Exists(audioDirectory))
            {
                associatedFileList.AddRange(Directory.GetFiles(audioDirectory, baseFilename + "*.MXF"));
            }

            var iconDirectory = directoryName.Replace("VIDEO", "ICON");
            if (Directory.Exists(iconDirectory))
            {
                associatedFileList.AddRange(Directory.GetFiles(iconDirectory, baseFilename + "*"));
            }

            var proxyDirectory = directoryName.Replace("VIDEO", "PROXY");
            if (Directory.Exists(iconDirectory))
            {
                associatedFileList.AddRange(Directory.GetFiles(proxyDirectory, baseFilename + "*"));
            }

            var voiceDirectory = directoryName.Replace("VIDEO", "VOICE");
            if (Directory.Exists(iconDirectory))
            {
                associatedFileList.AddRange(Directory.GetFiles(voiceDirectory, baseFilename + "*"));
            }

            return associatedFileList;
        }
        /// <summary>
        /// Determines whether the file has an extension that matches.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="extensionToMatch">The extension automatic match.</param>
        /// <returns>True if extensions match, false otherwise.</returns>
        public static bool HasMatchingExtension(string filename, string extensionToMatch)
        {
            var extension = Path.GetExtension(filename);
            if (string.IsNullOrWhiteSpace(extension))
            {
                return false;
            }

            return extension.Equals(extensionToMatch, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Determines whether the string is a legal filename for an importable audio file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        ///   <c>true</c> if the string is a legal filename for an importable audio file; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsImportableAudioFilename(string filename)
        {
            // Verify file is WAV or AIFF by checking the extension.
            var ext = Path.GetExtension(filename);
            return string.Compare(".wav", ext, StringComparison.OrdinalIgnoreCase) == 0
                    || string.Compare(".aiff", ext, StringComparison.OrdinalIgnoreCase) == 0
                    || string.Compare(".aif", ext, StringComparison.OrdinalIgnoreCase) == 0
                    || string.Compare(".bwf", ext, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Deletes the file or folder.
        /// </summary>
        /// <param name="filename">The filename.</param>
        public static void DeleteFileOrFolder(string filename)
        {
            if (File.Exists(filename))
            {
                DeleteFileNoError(filename);
            }
            else if (Directory.Exists(filename))
            {
                try
                {
                    Directory.Delete(filename, true);
                }
                catch (Exception exception)
                {
        //            TraceEventType.Error.LogMessage("Failed to delete directory " + filename + ": " + exception.Message);
                }
            }
        }

        /// <summary>
        /// Deletes the files or folders.
        /// </summary>
        /// <param name="fileNames">The file names.</param>
        /// <param name="progress">The progress.</param>
        public static void DeleteFilesOrFolders(List<string> fileNames, IProgress<int> progress = null)
        {
            if (fileNames == null)
            {
                return;
            }

            var currentItem = 0;
            foreach (var fileName in fileNames)
            {
                currentItem++;
                progress?.Report(currentItem);
                DeleteFileOrFolder(fileName);
            }
        }

        /// <summary>
        /// Verifies the file copy then deletes the original.
        /// </summary>
        /// <param name="sourceFilename">The source filename.</param>
        /// <param name="destFilename">The dest filename.</param>
        public static void VerifyCopyDelete(string sourceFilename, string destFilename)
        {
            try
            {
                File.Copy(sourceFilename, destFilename, true);
            }
            catch (Exception exception)
            {
                //TraceEventType.Error.LogMessage(
                //    "Failed to copy file " + sourceFilename + " to " + destFilename + ": "
                //    + exception.GetInnermostExceptionMessage());
                return;
            }

            if (File.Exists(destFilename))
            {
                DeleteFileNoError(sourceFilename);
            }
            else
            {
   //             TraceEventType.Error.LogMessage("File copy failure " + destFilename + " does not exist.");
            }
        }

        /// <summary>
        /// Garbages the collect directory.
        /// </summary>
        /// <param name="directoryName">Name of the directory.</param>
        /// <param name="requiredFiles">The required files.</param>
        public static void GarbageCollectDirectory(string directoryName, List<string> requiredFiles)
        {
            var allAssetsInDirectory = Directory.EnumerateDirectories(directoryName).ToList();
            allAssetsInDirectory.AddRange(Directory.EnumerateFiles(directoryName));
            var filesToRemove = allAssetsInDirectory.Except(requiredFiles);
            foreach (var file in filesToRemove)
            {
                DeleteFileOrFolder(file);
            }
        }

        /// <summary>
        /// Gets the frame number from filename.
        /// </summary>
        /// <param name="filenameNoExtension">The filename no extension.</param>
        /// <returns>The frame number.</returns>
        public static int GetFrameNumberFromFilename(string filenameNoExtension)
        {
            var matches = Regex.Matches(filenameNoExtension, @"\d+");
            if (matches.IsEmpty())
            {
                return -1;
            }

            var numberMatches = matches.Count;
            try
            {
                return int.Parse(matches[numberMatches - 1].Value);
            }
            catch (Exception)
            {
                // a string with too many digits will result in an overflow exception.
                return -1;
            }
        }

        /// <summary>
        /// Gets the frame number from filename, as a string.
        /// </summary>
        /// <param name="filenameNoExtension">The filename no extension.</param>
        /// <returns>The frame number.</returns>
        public static string GetFrameNumberStringFromFilename(string filenameNoExtension)
        {
            var matches = Regex.Matches(filenameNoExtension, @"\d+");
            if (matches.IsEmpty())
            {
                return null;
            }

            var numberMatches = matches.Count;
            try
            {
                return matches[numberMatches - 1].Value;
            }
            catch (Exception)
            {
                // a string with too many digits will result in an overflow exception.
                return null;
            }
        }

        /// <summary>
        /// Gets the contiguous sequence list by first checking if first and last files don't make one sequence.
        /// </summary>
        /// <param name="sequence">The sequence.</param>
        /// <returns>A list of contiguous sequences.</returns>
        public static List<List<string>> GetContiguousSequenceListEx(List<string> sequence)
        {
            var sortedSequence = sequence.OrderBy(f => f, new LogicalStringComparer()).ToList();
            var numberOfFiles = sequence.Count;
            var sequenceList = new List<List<string>>();
            var firstFileName = sortedSequence.FirstOrDefault();
            if (string.IsNullOrWhiteSpace(firstFileName))
            {
                return sequenceList;
            }

            var lastFileName = sortedSequence.LastOrDefault();
            if (string.IsNullOrWhiteSpace(lastFileName))
            {
                return sequenceList;
            }

            if (firstFileName == lastFileName)
            {
                sequenceList.Add(new List<string> { firstFileName });
                return sequenceList;
            }

            var firstFileNameNoExtension = Path.GetFileNameWithoutExtension(firstFileName);
            if (string.IsNullOrWhiteSpace(firstFileNameNoExtension))
            {
                return sequenceList;
            }

            var lastFileNameNoExtension = Path.GetFileNameWithoutExtension(lastFileName);
            if (string.IsNullOrWhiteSpace(lastFileNameNoExtension))
            {
                return sequenceList;
            }

            var firstFrameNumber = GetFrameNumberFromFilename(firstFileNameNoExtension);
            var lastFrameNumber = GetFrameNumberFromFilename(lastFileNameNoExtension);
            if (lastFrameNumber == (firstFrameNumber + numberOfFiles - 1))
            {
                sequenceList.Add(sortedSequence);
                return sequenceList;
            }

            return GetContiguousSequenceList(sortedSequence, true);
        }

        /// <summary>
        /// Gets the contiguous sequence list.
        /// </summary>
        /// <param name="sequence">The sequence.</param>
        /// <param name="sorted">if set to <c>true</c> sorted.</param>
        /// <returns>
        /// A list of contiguous sequences.
        /// </returns>
        public static List<List<string>> GetContiguousSequenceList(List<string> sequence, bool sorted = false)
        {
            var previousFrameNumber = -1;
            var sequenceList = new List<List<string>>();
            var contiguousFileList = new List<string>();
            var sortedSequence = sequence;
            if (!sorted)
            {
                sortedSequence = sequence.OrderBy(f => f, new LogicalStringComparer()).ToList();
            }

            foreach (var file in sortedSequence)
            {
                var filenameNoExtension = Path.GetFileNameWithoutExtension(file);
                if (string.IsNullOrWhiteSpace(filenameNoExtension))
                {
                    continue;
                }

                var frameNumber = GetFrameNumberFromFilename(filenameNoExtension);
                if (previousFrameNumber < 0)
                {
                    contiguousFileList.Add(file);
                    previousFrameNumber = frameNumber;
                    if (frameNumber < 0)
                    {
                        sequenceList.Add(contiguousFileList.ToList());
                        contiguousFileList.Clear();
                    }

                    continue;
                }

                if (frameNumber == previousFrameNumber + 1)
                {
                    contiguousFileList.Add(file);
                    previousFrameNumber = frameNumber;
                    continue;
                }

                sequenceList.Add(contiguousFileList.ToList());
                previousFrameNumber = frameNumber;
                contiguousFileList.Clear();
                contiguousFileList.Add(file);
            }

            if (!contiguousFileList.IsNullOrEmpty())
            {
                sequenceList.Add(contiguousFileList);
            }

            return sequenceList;
        }

        /// <summary>
        /// Appends the directory separator to the root if necessary.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>The path with appended separator if necessary</returns>
        public static string AppendDirectorySeparatorIfNecessary(string path)
        {
            if (IsRootPathWithoutSeparator(path))
            {
                return path + Path.DirectorySeparatorChar;
            }

            return path;
        }

        /// <summary>
        /// Removes the directory separator when the path is not the root.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>The path with no directory separator at end when necessary.</returns>
        public static string RemoveDirectorySeparatorIfNecessary(string path)
        {
            if (IsRootPathWithSeparator(path))
            {
                return path;
            }

            return path.TrimEnd(new[] { Path.DirectorySeparatorChar });
        }

        /// <summary>
        /// Determines whether or not the specified path is a root path with separator.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>True if path is root, otherwise false.</returns>
        public static bool IsRootPathWithSeparator(string path)
        {
            var directoryInfo = new DirectoryInfo(path);
            return directoryInfo.Parent == null;
        }

        /// <summary>
        /// Determines whether or not the specified path is a root path without separator.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>True if path is root, otherwise false.</returns>
        public static bool IsRootPathWithoutSeparator(string path)
        {
            var pathRoot = Path.GetPathRoot(path);
            return path == pathRoot;
        }

        /// <summary>
        /// Gets the total size for all files in the specified directory.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="numFiles">The number files.</param>
        /// <returns>The total size for all files in the specified directory.</returns>
        public static long GetTotalSizeAndNumberOfFiles(string directory, out int numFiles)
        {
            // Get the info for the source
            var totalSize = 0L;
            numFiles = 0;

            if (!string.IsNullOrWhiteSpace(directory) && Directory.Exists(directory))
            {
                var allFiles = GetFileInfoList(directory);
                numFiles = allFiles.Count();

                // Total up the sizes of all files.
                totalSize += allFiles.Sum(fi => fi.Length);
            }

            return totalSize;
        }

        /// <summary>
        /// Gets thes FileInfo list.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="checkSubdirectories">if set to <c>true</c> check subdirectories.</param>
        /// <returns>
        /// The list of files in the specified directory excluding CopyJobCatalog files.
        /// </returns>
        public static List<FileInfo> GetFileInfoList(string directory, bool checkSubdirectories = true)
        {
            var directoryList = new List<string> { directory };
            if (checkSubdirectories)
            {
                GetSubdirectories(directory, directoryList);
            }

            // Get the FileInfo for all the files in the directory (and subdirectories)
            var fileInfoList = new List<FileInfo>();
            foreach (var dir in directoryList)
            {
                var directoryInfo = new DirectoryInfo(dir);
                fileInfoList.AddRange(FastDirectoryEnumerator.EnumerateFiles(directoryInfo));
            }

            return fileInfoList;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Gets all supported files filter.
        /// </summary>
        /// <returns>All supported files filter.</returns>
        private static string GetAllSupportedFilesFilter()
        {
            var videoFilterFirstPart = new StringBuilder();
            var videoFilterSecondPart = new StringBuilder();
            videoFilterFirstPart.Append("All Supported Files (");
            var allExtensions = new List<string>(VideoExtensions);
            allExtensions.AddRange(AudioExtensions);
            allExtensions.AddRange(SubtitleExtensions);
            foreach (var extension in allExtensions)
            {
                var filter = "*" + extension;
                videoFilterFirstPart.Append(filter + ", ");
                videoFilterSecondPart.Append(filter + ";");
            }

            // Remove the unnecessary ", " and ";"
            var firstPartLength = videoFilterFirstPart.Length;
            videoFilterFirstPart.Remove(firstPartLength - 2, 2);
            var secondPartLength = videoFilterSecondPart.Length;
            videoFilterSecondPart.Remove(secondPartLength - 1, 1);

            // Append the )| to first part
            videoFilterFirstPart.Append(")|");
            return videoFilterFirstPart.Append(videoFilterSecondPart).ToString();
        }

        /// <summary>
        /// Deletes the file noe error.
        /// </summary>
        /// <param name="filename">The filename.</param>
        private static void DeleteFileNoError(string filename)
        {
            // If file does not exist, ignore
            // This is because throws are expensive
            if (!File.Exists(filename))
            {
                return;
            }

            try
            {
                File.Delete(filename);
            }
            catch (Exception exception)
            {
 //               TraceEventType.Error.LogMessage("Failed to delete " + filename + ": " + exception.Message);
            }
        }

        /// <summary>
        /// Gets the path parts.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>The path parts.</returns>
        private static List<string> GetPathParts(string path)
        {
            return path.Split(new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        /// <summary>
        /// Gets the HasherAlgorithm for the specified hash type.
        /// </summary>
        /// <param name="hashType">Type of the hash.</param>
        /// <returns>The HasherAlgorithm for the specified HashType.</returns>
        private static HashAlgorithm GetHasherForHashType(HashType hashType)
        {
            switch (hashType)
            {
                case HashType.Md5:
                    return MD5.Create();
                case HashType.Sha1:
                    return SHA1.Create();
                case HashType.Sha256:
                    return SHA256.Create();
                case HashType.Sha384:
                    return SHA384.Create();
                case HashType.Sha512:
                    return SHA512.Create();
                default:
                    throw new InvalidOperationException(string.Format("CryptoProvider ({0}) is not implemented", hashType));
            }
        }

        /// <summary>
        /// Gets the matching terminal path parts.
        /// </summary>
        /// <param name="path1">The path1.</param>
        /// <param name="path2">The path2.</param>
        /// <returns>The matching terminal path parts.</returns>
        private static List<string> GetMatchingTerminalPathParts(string path1, string path2)
        {
            var path1Parts = GetPathParts(path1);
            var path2Parts = GetPathParts(path2);

            var matchingParts = new List<string>();

            for (var i = 0; i < path1Parts.Count && i < path2Parts.Count; i++)
            {
                var oldPathPart = path1Parts[path1Parts.Count - i - 1];
                var newPathPart = path2Parts[path2Parts.Count - i - 1];

                if (oldPathPart == newPathPart)
                {
                    matchingParts.Add(oldPathPart);
                }
                else
                {
                    break;
                }
            }

            matchingParts.Reverse();

            return matchingParts;
        }

        /// <summary>
        /// Gets the hash with progress using hash algorithm.
        /// </summary>
        /// <param name="hashType">Type of the hash.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="progressStopwatch">The progress stopwatch.</param>
        /// <param name="bytesVerified">The bytes verified.</param>
        /// <param name="progress">The progress.</param>
        /// <returns>
        /// The hash.
        /// </returns>
        private static string GetHashWithProgressUsingHashAlgorithm(
            HashType hashType,
            FileStream stream,
            Stopwatch progressStopwatch,
            out long bytesVerified,
            IProgress<double> progress = null)
        {
            using (var hasher = GetHasherForHashType(hashType))
            {
                int bytesRead;
                var totalBytesRead = 0L;
                var totalBytes = stream.Length;

                // Using value from above in CopyMultipleAndHashSource()
                var buffer = new byte[1024 * 1024];

                do
                {
                    bytesRead = stream.Read(buffer, 0, buffer.Length);
                    totalBytesRead += bytesRead;
                    hasher.TransformBlock(buffer, 0, bytesRead, null, 0);

                    if (progress != null && progressStopwatch.ElapsedMilliseconds > ReportIntervalMilliseconds)
                    {
                        progress.Report((totalBytesRead / (double)totalBytes) * 100.0);
                        progressStopwatch.Restart();
                    }
                }
                while (bytesRead != 0);

                hasher.TransformFinalBlock(buffer, 0, 0);

                var hexString = HashBinaryToString(hasher.Hash);
                bytesVerified = totalBytesRead;
                return hexString;
            }
        }

        /// <summary>
        /// Gets the hash with progress using xx hash.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="progressStopwatch">The progress stopwatch.</param>
        /// <param name="bytesVerified">The bytes verified.</param>
        /// <param name="progress">The progress.</param>
        /// <returns>The hash.</returns>
        private static string GetHashWithProgressUsingXxHash(
            FileStream stream,
            Stopwatch progressStopwatch,
            out long bytesVerified,
            IProgress<double> progress = null)
        {
            throw new NotImplementedException();
            //var xxHash = new xxHash();
            //xxHash.Init();

            //int bytesRead;
            //var totalBytesRead = 0L;
            //var totalBytes = stream.Length;

            //// Using value from above in CopyMultipleAndHashSource()
            //var buffer = new byte[1024 * 1024];

            //do
            //{
            //    bytesRead = stream.Read(buffer, 0, buffer.Length);
            //    totalBytesRead += bytesRead;
            //    xxHash.Update(buffer, bytesRead);

            //    if (progress != null && progressStopwatch.ElapsedMilliseconds > ReportIntervalMilliseconds)
            //    {
            //        progress.Report((totalBytesRead / (double)totalBytes) * 100.0);
            //        progressStopwatch.Restart();
            //    }
            //}
            //while (bytesRead != 0);

            //var hash = xxHash.Digest();
            //var hashBytes = BitConverter.GetBytes(hash).Reverse().ToArray();
            //var hexString = HashBinaryToString(hashBytes);
            //bytesVerified = totalBytesRead;
            //return hexString;
        }

        #endregion
    }
}
