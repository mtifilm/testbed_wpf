﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MtiSimple.Core
{
    public class SimpleTimecodeToFrameConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////

        //////////////////////////////////////////////////
        // IValueConverter Members
        //////////////////////////////////////////////////
        #region IValueConverter Members

        /// <summary>
        /// Converts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>The appropriate string.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var timecode = value as SimpleTimecode;
            return timecode?.Frame;
        }

        /// <summary>
        /// Converts the back.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>An Exception.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // This is NOT correct as the digits are wrong
            // However the control will legalize the value to correct string 
            var frameIndex = System.Convert.ToDouble(value);
            return new SimpleTimecode((int)frameIndex, 6);
        }

        #endregion
    }
}
