// DpxToMatlab.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "Ippheaders.h"
#include <filesystem>
#include <string>
#include <cctype>
#include "DevIO.h"

bool caseInSensStringCompare(std::string &str1, std::string &&str2)
{
	return ((str1.size() == str2.size()) && std::equal(str1.begin(), str1.end(), str2.begin(), [](char & c1, char & c2) {
		return (c1 == c2 || std::toupper(c1) == std::toupper(c2));
	}));
}

int main(int argc, char *argv[])
{
//	string inpath = "D:\\CorrectData\\Stab Files\\FourDaysWonder\\";
//	string outPath = "D:\\Temp\\FourDaysWonder\\";
//	string inpath = "D:\\CorrectData\\Flicker\\2k\\";
//	string outPath = "D:\\Temp\\Alpha3\\";
//	string inpath = "D:\\CorrectData\\moon_over_miami\\";
//	string outPath = "D:\\Temp\\Mom\\";
//	string inpath = "Y:\\correct\\PowerAndGlory\\";
//	string outPath = "D:\\Temp\\PowerAndGlory\\";
	string inpath = R"(D:\CorrectData\ShineTests\seinfeld_alpha_size_problem\)";
	string outPath = R"(D:\Temp\ShineTests\seinfeld_alpha_size_problem\)";
//	string inpath = R"(D:\CorrectData\Flicker\do_testow_mti_pancerni\)";
//	string outPath = R"(D:\Temp\Flicker\do_testow_mti_pancerni\)";

	namespace fs = std::filesystem;

	// This is wrong, we need regular expression but it works 
	int sequenceStart = 5671;
	int start = 5671;
	int duration = 392;

	int count = sequenceStart;

	// These two start at first one
	sequenceStart = 0;
	start = 0;
	count = 0;

	for (auto& p : fs::directory_iterator(inpath))
	{
		auto extension = p.path().extension().string();
		if (caseInSensStringCompare(extension, ".dpx"))
		{
			if ((count >= start) && (count < (start + duration)))
			{
				auto c = p.path().filename().replace_extension(".mat");
				auto inFileName = p.path().string();
				auto outFileName = outPath + c.string();
				DevIO::readDpxFile(inFileName, true);
				auto [input16u, bitDepth] = DevIO::readDpxFile(inFileName, true);

//				input16u = input16u({ 215, 80, input16u.getWidth() - 215, input16u.getHeight() - 280 });
				auto d = p.path().filename().replace_extension("").extension().string();
				auto varName = string("dpxImage");// +d.substr(1);
				
				MatIO::saveListToMatFile(outFileName,input16u,varName);
				std::cout << "Saving " << outFileName << '\n';
			}

			count++;
		}
	}

	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
