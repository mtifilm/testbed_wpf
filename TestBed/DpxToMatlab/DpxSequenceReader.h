#ifndef DpxSequenceReaderH
#define DpxSequenceReaderH
#pragma once
#include "Ippheaders.h"
#include <string>
#include <filesystem>
#include <optional>
#include <regex> 
#include "DevIO.h"

// This simple class assumes there is a sequence of DPX files in a directory
// This file names end in a timecode e.g.,  C3P_R02_0172808.dpx or  C3P_R02_0172808_dummy.dpx
// The shot start is the dpx file number
// duration is just the shot duration
// The index is just a counter but will match dpx sequence numbers 
// to make the translation between DRS_NOVA and testing easily
// 
class DpxSequenceReader
{
public:
   DpxSequenceReader(const std::string &inPath, bool normalize16u = true) :
      _inPath(inPath), _clipSequenceStartIndex(0), _normalize16u(normalize16u)
   {
      readFileNames();
      setShotIndexesStartDuration(_clipSequenceStartIndex, int(_fileNames.size()));
   }

   std::optional<Ipp16uArray> getNextImage()
   {
      if ((_nextIndex >= _shotStartIndex + _duration) || (_nextIndex < _shotStartIndex))
      {
         return std::nullopt;
      }

      return getImage16u(_nextIndex++);
   }

   void restartReading()
   {
      _nextIndex = _shotStartIndex;
   }

   // 0 is first frame of shot
   void seekToShotIndex(int shotIndex)
   {
      _nextIndex = _shotStartIndex + shotIndex;
   }

   bool isIndexInShot(int index) const
   {
      return (index >= _shotStartIndex + _duration) || (index < _shotStartIndex);
   }

   // return out of bounds( < -) if no index match
   int getIndexFromTimecode(int timecode)
   {
      std::regex rx(R"((\d+)\D*\.dpx$)");
      std::smatch m;

      auto index = 0;
      for (const auto &fileName : _fileNames)
      {
         std::regex_search(fileName, m, rx);
         std::istringstream is(m[1]);
         int number;
         is >> number;

         // Ugly programming but I don't care
         if (timecode == number)
         {
            return index;
         }

         index++;
      }

      return -1;
   }

   bool seekToTimecode(int timecode)
   {
      auto index = getIndexFromTimecode(timecode);
      if (isIndexInShot(index))
      {
         _nextIndex = index;
         return true;
      }

      return false;
   }

   // This resets the sequence to start reads first image
   // Same as restartReading/getNextImage
   Ipp16uArray getFirstImage()
   {
      restartReading();
      return getImage16u(_nextIndex++);
   }

   // This gets a 16u image, bit depth normalized to 16
   // However, keep it for possible future expansion
   Ipp16uArray getImage16u(int indexFromStart)
   {
      Ipp16uArray result;
      std::tie(result, _bitDepth) = DevIO::readDpxFile(_fileNames[indexFromStart], _normalize16u);

      // Strip off alpha if it exists
      if (result.getComponents() == 4)
      {
         auto planes = result.toPlanar();
         MtiPlanar planes3 = { planes[0], planes[1], planes[2] };
		 result = planes3.toArray();
      }

      result.setOriginalBits(_bitDepth);
      return result;
   }

   Ipp32fArray getNormalizedImage(int indexFromStart)
   {
      // This is inefficient as hell.
      auto result = Ipp32fArray(getImage16u(indexFromStart));
      const float scale = 1.0f / ((1 << getBitDepth()) - 1);
      result *= scale;
      return result;
   }

   // This resets the sequence to start reads first image
   // Same as restartReading/getNextImage
   Ipp32fArray getFirstNormalizedImage()
   {
      restartReading();
      return getNormalizedImage(_nextIndex++);
   }

   std::optional<Ipp32fArray> getNextNormalizedImage()
   {
      if ((_nextIndex >= _shotStartIndex + _duration) && (_nextIndex >= _shotStartIndex))
      {
         return getNormalizedImage(_nextIndex++);
      }

      return std::nullopt;
   }

   void setShotIndexesStartDuration(int shotStartIndex, int duration)
   {
      _duration = std::min<int>(duration, int(_fileNames.size() - shotStartIndex));
      _shotStartIndex = shotStartIndex;
      _nextIndex = _shotStartIndex;
   }

   // End, as usual is exclusive
   bool setShotTimecodesStartEnd(int startTimeCode, int endTimeCode = INT_MAX)
   {
      if (startTimeCode >= endTimeCode)
      {
         return false;
      }

      auto startIndex = getIndexFromTimecode(startTimeCode);
      if (startIndex < 0)
      {
         return false;
      }

      auto endIndex = getIndexFromTimecode(endTimeCode);
      auto duration = endIndex >= 0 ? endIndex - startIndex : int(_fileNames.size()) - startIndex;
      setShotIndexesStartDuration(startIndex, duration);

      return true;
   }

   // The shots are set
   int shotDuration() const
   {
      return _duration;
   }

   int getNextIndexToRead() const
   {
      return _nextIndex;
   }

   int getLastReadIndex() const
   {
      return _nextIndex - 1;
   }

   std::string getLastReadFileName() const
   {
      return _fileNames[getLastReadIndex()];
   }

   bool shotFinished() const
   {
      return _nextIndex >= _shotStartIndex + _duration;
   }

   int getShotDuration() const
   {
      return _duration;
   }

   int getBitDepth() const
   {
      return _bitDepth;
   }

   bool getNormalized16u() const { return _normalize16u; }
   void setNormalized16u(const bool value) { _normalize16u = value; }

private:
   void readFileNames()
   {
      _fileNames.clear();
      for (auto& p : std::filesystem::directory_iterator(_inPath))
      {
         auto extension = p.path().extension().string();
         if (EqualIgnoreCase(extension, ".dpx"))
         {
            _fileNames.push_back(p.path().string());
         }
      }
   }

private:
   int _nextIndex = 0;

   std::string _inPath;
   int _clipSequenceStartIndex = 0;
   int _shotStartIndex = 0;
   int _duration = 0;
   int _bitDepth = 16;
   vector<std::string> _fileNames;
   bool _normalize16u = true;
};

#endif