#pragma once
#include "BaseToolProcessor.h"

// We actually don't need to export this
class MTI_TOOL_PROCESSOR_API ExampleToolProcessor :
	public BaseToolProcessor
{
public:
	ExampleToolProcessor();
	~ExampleToolProcessor();
	

	// Preprocess section
	int initPreprocessing(int pass) override;
	int CancelPreprocessingAsync() override;
	int preprocessFrame(int pass, const std::string &fileKey, const SimpleImageFrame& imageFrame, int currentIndex) override;
	int preprocessingCompleted(int pass) override;

	// Process section
	int initProcessing(int frames) override;
	int cancelProcessingAsync() override;
	int processFrame(const std::string &fileKey, const SimpleImageFrame& imageFrame, int currentIndex) override;
	int processingCompleted() override;
	
protected:
	vector<double> 	_redAverage;
	vector<double> 	_greenAverage;
	vector<double> 	_blueAverage;
};

// This factory would create different tools
// However, we only have one tool here
// Not sure if this is the best way
static BaseToolProcessor *ToolProcessorFactory()
{
	return new ExampleToolProcessor();
}

