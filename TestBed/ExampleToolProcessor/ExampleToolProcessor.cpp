#include "ExampleToolProcessor.h"
#include "../DpxToMatlab/DpxSequenceReader.h"

ExampleToolProcessor::ExampleToolProcessor()
{
}


ExampleToolProcessor::~ExampleToolProcessor()
{
}


int ExampleToolProcessor::initPreprocessing(int pass)
{
	// The following values are set by the GUI
	// and are available at all times for the tool
	const auto totalFrames = getTotalFrames();
	const auto processSize = getProcessingSize();

	// Size defines the channels
	const auto channels = processSize.depth;

	// Clear the example preprocessor data
	_redAverage.clear();
	_greenAverage.clear();
	_blueAverage.clear();
	return 0;
}

int ExampleToolProcessor::CancelPreprocessingAsync()
{
	// This is run in parallel to preprocessFrame
	// Its more useful to clean up when processing done is called
	// However, one might want to kill threads that are stuck
	return 0;
}

int ExampleToolProcessor::preprocessFrame(int pass, const std::string &fileKey, const SimpleImageFrame& imageFrame, int currentIndex)
{
	// SimpleImageFrame has the RBG and Alpha.  Alpha is empty if the DPX has no alpha
	// Find the image, you are guaranteed the image size is the processSize of the init
	const auto imageArray = imageFrame.image;
	
	// find the Average of each channel
	// Just an example of what can be done.
	auto norm = imageArray.sum();
	const auto pixels = imageArray.area();

	// Check for mono
	if (imageArray.getComponents() == 1)
	{
		_redAverage.push_back(norm[0] / pixels);
		_greenAverage.push_back(norm[0] / pixels);
		_blueAverage.push_back(norm[0] / pixels);
	}
	else
	{
		_redAverage.push_back(norm[0] / pixels);
		_greenAverage.push_back(norm[1] / pixels);
		_blueAverage.push_back(norm[2] / pixels);
	}

	// IppArray data() is a pointer to start of data; however the data may not be contiguous
	// The following is an meaningless example how to address the rows directly
	// NOTE: All pointers are valid ONLY when the frameArray16u lives.
	auto total = 0.0;
	for (auto r = 0; r < imageArray.getHeight(); r++)
	{
		// rows are always contiguous;
		auto rowPtr = imageArray.getRowPointer(r);
		for (auto c = 0; c < imageArray.getHeight(); c++)
		{
			for (auto d = 0; d < imageArray.getComponents(); d++)
			{
				// If users aborts
				if (getCancellationPending())
				{
					return -1;
				}
				
				total += rowPtr[d];
			}
		}
	}

	// Finally, any frame data can be accessed, frame data is RGB (or mono) with an alpha image, which may be empty
	const auto nextFrame = this->getInputImageFrameFromIndex(currentIndex + 1).image;
	if (nextFrame.isEmpty())
	{
		// index was outside of marked range.
	}

	// IppArrays supports parallel processing using stripes
	// However, the intel libraries are usually too fast to make much difference
	// Just an example
	//   Convert a 16u to 32f array using strips
	// Note: the imageArray size and the _processingSize must  have the same width and height
	Ipp32fArray ipp32fTestArray(imageArray.getSize());
	IpaStripeStream iss;

	// The size you want to work on
	iss << imageArray.getSize();

	// What you want to execute per strip
	iss << [&](const MtiRect &roi)
	{
		// Assignment (=) only moves a few pointers
		// the <<= operator is used to move data and convert data from one array to another
		// the IppArray(roi) is an IppArray selects a new reference IppArray to the data defined by the ROI 
		ipp32fTestArray(roi) <<= imageArray(roi).toNormalized32f();

		// Note:  <<= is a conversion/copy without normalization
		// we can write stripedOutput(roi) <<= frameArray16u(roi)
	};

	// Now execute it
	iss.stripe();
	// To debug, you can replace iss.stripe() by
	// iss.run() which runs synchronously in calling thread

	return 0;
}

int ExampleToolProcessor::preprocessingCompleted(int pass)
{
	// This is an example to show how the preprocessing data can be written to a matlab file
	// Not a lot can be supported can be written, mainly IppArrays and vectors.
	// Since IppArrays don't support doubles, we natively support a double vector
	MatIO::saveListToMatFile("C:\\temp\\ExamplePreprocessData.mat", _redAverage, "averageRed", _blueAverage, "averageBlue", _greenAverage, "averageGreen");

	const auto firstImage = this->getInputImageFrameFromIndex(0).image;
	const auto resizedImage = firstImage.resizeAntiAliasing({ 512, (firstImage.getHeight() * 512)/firstImage.getWidth() }).toGray();
	AppendVariablesToMatFile1("C:\\temp\\ExamplePreprocessData.mat", resizedImage);
	return 0;
}

////// This is the process section
int ExampleToolProcessor::initProcessing(int frames)
{
	// The class contains the preprocessor data, however we can load it exists
	// Since we don't use it, we will ignore it
/*
	try
	{
		_redAverage = MatIO::readVectorIpp64f("C:\\temp\\ExamplePreprocessData.mat", "averageRed");
		_blueAverage = MatIO::readVectorIpp64f("C:\\temp\\ExamplePreprocessData.mat", "averageBlue");
		_greenAverage = MatIO::readVectorIpp64f("C:\\temp\\ExamplePreprocessData.mat", "averageGreen");

		// This reads the first 16u array found
		auto resizedImage = MatIO::readIpp16uArray("C:\\temp\\ExamplePreprocessData.mat");
		
	}
	catch (const std::exception &ex)
	{
		std::cerr << ex.what() << std::endl;
		return -2;
	}
	catch (...)
	{
		std::cerr << "Unknown Error" << std::endl;
		return -1;
	}
*/
	return 0;
}

int ExampleToolProcessor::cancelProcessingAsync()
{
	return 0;
}

int ExampleToolProcessor::processFrame(const std::string& fileKey, const SimpleImageFrame& imageFrame,
                                       int currentIndex)
{
	// SimpleImageFrame has the RBG and Alpha.  Alpha is empty if the DPX has no alpha
	// Find the image
	const auto imageArray = imageFrame.image;
	const auto alphaArray = imageFrame.alpha;
	
	// Convert to normalized float
	// We don't have to do this since 16u has a mirror but this just a demo
	// WARNING if you want to directly access the floatArray.data() pointer
	//         remember it is ONLY GUARANTEED valid while floatArray is in scope
	// NOTE: Normalization used the standard normalization used by OpenCV which is
	//  (x - min)/(max - min) where max is 65355 for 16u.
	auto floatArray = imageArray.toNormalized32f();

	// The min and max values are between [0, 1] because its normalized
	auto[minValue, maxValues] = floatArray.minMax();

	// Do some processing on it
	floatArray = floatArray.mirror(ippAxsBoth);

	// This puts the processed file into storage
	// I/O is always in 16u so we must convert
	saveOutputFrame(fileKey, floatArray.toNormalized16u());
	return 0;
}

int ExampleToolProcessor::processingCompleted()
{
	return 0;
}
