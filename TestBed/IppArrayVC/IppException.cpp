//#include "machine.h"
//#include "corecodelibint.h"
#include "ippcore.h"
#include "IppException.h"
#include <string>
#include <vector>
using std::string;
using std::vector;

std::string IppException::GetErrorMessage()
{
    return ippGetStatusString((IppStatus)this->_status);
}

IppException::IppException(IppStatus status) : _status(status)
{}

IppException::~IppException()
{
}

const char *IppException::what() const
{
    return ippGetStatusString((IppStatus)this->_status);
}

