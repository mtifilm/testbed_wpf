#include "ImageBinSorter.h"
#include "IpaStripeStream.h"


ImageBinSorter::ImageBinSorter()
{
	_edges.resize(UINT16_MAX+1);
}

const vector<Ipp32fArray>& ImageBinSorter::sortDataToBins(const Ipp16uArray& keyImage, const Ipp32fArray& dataImage,
	int numberOfBins)
{
	sortIndexes(keyImage, numberOfBins);
	gatherAndSort(dataImage);
	return _bins32f;
}

const vector<Ipp32fArray>& ImageBinSorter::sortDataToBins(const Ipp32fArray& keyImage, const Ipp32fArray& dataImage,
	int numberOfBins)
{
	sortIndexes(keyImage, numberOfBins);
	gatherAndSort(dataImage);
	return _bins32f;
}

void ImageBinSorter::sortIndexes(const Ipp16uArray& image, int numberOfBins)
{
	if (image.getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Only 2D images supported")
	}

	// Resize index storage if needed
	const int totalPixels = image.area();
	if (_pixelValueIndexes.getWidth() != totalPixels)
	{
		_pixelValueIndexes = Ipp32sArray(MtiSize(totalPixels));
	}
	
	allocateBinStorage(numberOfBins, int(totalPixels));
	

	// This throw away histogram is just because we need to know the size of each
	// bin beforehand. 
	auto edges = FastHistogramIpp::compute(image, 65536, -0.5, 65535.5)[0];
	
	_edges[0] = edges[0];  // Always zero
	for (auto c : range(edges.size() - 1))
	{
		_edges[c + 1] = edges[c + 1] + _edges[c];
	}

	const auto storagePtr = _pixelValueIndexes.data();
	vector<Ipp32s> count2(UINT16_MAX, 0);
	Ipp32s idx = 0;
	for (auto r = 0; r < image.getHeight(); r++)
	{
		auto sp = image.getRowPointer(r);
		for (auto c = 0; c < image.getWidth(); c++)
		{
			const auto v = *sp++;
			*(storagePtr + _edges[v] + count2[v]) = idx;
			idx++;
			count2[v]++;
		}
	}
}

// Warning only works with normalized arrays
void ImageBinSorter::sortIndexes(const Ipp32fArray& image, int numberOfBins)
{
	if (image.getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Only 2D images supported")
	}

	// Resize index storage if needed
	const int totalPixels = image.area();
	if (_pixelValueIndexes.getWidth() != totalPixels)
	{
		_pixelValueIndexes = Ipp32sArray(MtiSize(totalPixels));
	}

	allocateBinStorage(numberOfBins, int(totalPixels));

	// This throw away histogram is just because we need to know the size of each
	// bin beforehand. 
   auto delta = 1.0f / 65536.0f / 2.0f;
	auto edges = FastHistogramIpp::compute(image, 65536, -delta, 1.0f - delta)[0];
	_edges[0] = edges[0];  // Always zero
	for (auto c : range(edges.size() - 1))
	{
		_edges[c + 1] = edges[c + 1] + _edges[c];
	}

	const auto storagePtr = _pixelValueIndexes.data();
	vector<Ipp32s> count2(UINT16_MAX, 0);
	Ipp32s idx = 0;
	for (auto r = 0; r < image.getHeight(); r++)
	{
		auto sp = image.getRowPointer(r);
		for (auto c = 0; c < image.getWidth(); c++)
		{
			const auto v = int(65535 * *sp++) & 0xFFFF;

			// There is a slight round off error, this means a few bins are not
			// indexed correctly, we could fix them up or just set index to 0
         *(storagePtr + _edges[v] + count2[v]) = idx;
			idx++;
			count2[v]++;
		}
	}
}

void ImageBinSorter::gatherAndSort(const Ipp32fArray& dataImage)
{
	IpaStripeStream iss;
	auto f = [&](const int i)
	{
		auto intervalPtr = _binIntervals[i].data();
		auto binPtr = _bins32f[i].data();
		const auto dataImagePtr = dataImage.data();

		// Some minor optimization
		if (dataImage.isContiguous())
		{
			for (auto c = 0; c < _binIntervals[i].getWidth(); c++)
			{
				*binPtr++ = *(dataImagePtr + *intervalPtr++);
			}
		}
		else
		{
			for (auto c = 0; c < _binIntervals[i].getWidth(); c++)
			{
				*binPtr++ = dataImagePtr[*intervalPtr++];
			}
		}

		IppThrowOnError(ippsSortAscend_32f_I(_bins32f[i].data(), _bins32f[i].getWidth()));
	};

	iss << _numberOfBins;
 	iss << efu_job(f);  // CAST is only because Embarcardo has a bug in its resolution
	iss.stripe();
}

void ImageBinSorter::allocateBinStorage(int numberOfBins, int totalPixels)
{
	if ((numberOfBins == _numberOfBins) && (totalPixels == int(_pixelValueIndexes.getWidth())))
	{
		return;
	}

	_numberOfBins = numberOfBins;

	const auto xInc = double(_pixelValueIndexes.getWidth()) / double(_numberOfBins);
	const auto _pixelValueIndexesPtr = _pixelValueIndexes.data();
	auto lowIndex = 0;
	for (auto i = 0; i < _numberOfBins; i++)
	{
		const int highIndex = int(round((i + 1) * xInc));
		_binIntervals.push_back({ { highIndex - lowIndex }, _pixelValueIndexesPtr + lowIndex });
		_bins32f.emplace_back(highIndex - lowIndex);
		lowIndex = highIndex;
	}

	// Sanity Check, can be removed
	auto total = 0;
	for (auto &bin : _binIntervals)
	{
		total += bin.getWidth();
	}

	MTIassert(total == totalPixels);
}

