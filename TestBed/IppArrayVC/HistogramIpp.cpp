#include "HistogramIpp.h"
#include "IppException.h"
#include "ippcore.h"
#include "array"
#include "IppMatIO.h"

	/// <summary>
	/// Initializes a new _instance of the <see cref="HistogramMcv" /> class.
	/// </summary>
	/// <param name="size">The size.</param>
	/// <param name="dataType">Type of the data.</param>
	/// <param name="bins">The bins.</param>
	/// <param name="dataMin">The data minimum.</param>
	/// <param name="dataMax">The data maximum.</param>
    HistogramIpp::HistogramIpp(const IppiSize &size, int channels, IppDataType dataType, int bins, float dataMin, float dataMax)
	{
		_size = size;
		_bins = bins;
		_dataType = dataType;
		_channels = channels;
		try
		{
			findActualMinMax(dataMin, dataMax, getDataType());
			_dataMin = dataMin;
			_dataMax = dataMax;
			initialize();
		}
		catch (const std::exception& ex)
		{
			freeAllMemory();
			throw ex;
		}
	}

	void HistogramIpp::findActualMinMax(float &dmin, float &dmax, IppDataType depth) const
	{
		if (dmin >= dmax)
		{
			// use defaults if the same
			switch (depth)
			{
			case ipp8u:
				dmin = 0;
				dmax = IPP_MAX_8U + 1.0f;
				break;

			case ipp16u:
				dmin = 0;
				dmax = IPP_MAX_16U + 1.0f;
				break;

			case ipp16s:
				dmin = IPP_MIN_16S;
				dmax = IPP_MAX_16S + 1.0f;
				break;

			case ipp32f:
				dmin = IPP_MINABS_32F;
				dmax = IPP_MAXABS_32F; // Is this legal???
				break;

			default:
				THROW_MTI_RUNTIME_ERROR(string(__func__) + ": datatype not supported");
			}
		}
	}

	/// <summary>
	/// Computes the specified image from an IppArray image
	/// </summary>
	/// <param name="image">The opencv image.</param>
	/// <returns>std.vector&lt;_Ty, _Alloc&gt;.</returns>
	vector<vector<Ipp32u>> HistogramIpp::compute(const Ipp32fArray &image)
	{
		if (image.getDataType() != _dataType)
		{
			THROW_MTI_RUNTIME_ERROR(string(__func__) + "Image types sizes don't match");
		}

		if (image.getComponents() != _channels)
		{
			THROW_MTI_RUNTIME_ERROR(string(__func__) + "Number of channels don't match");
		}

		if ((image.getSize().width != _size.width) || (image.getSize().height != _size.height))
		{
			THROW_MTI_RUNTIME_ERROR(string(__func__) + "ROI sizes don't match");
		}

		if (_histogramData == nullptr)

		{
			_histogramData = new Ipp32uP[_channels];
		}

		vector<vector<Ipp32u>> result;
		for (auto c = 0; c < _channels; c++)
		{
			result.emplace_back(_bins);
			_histogramData[c] = result.back().data();
		}

		compute_32f(image, _histogramData);
		return result;
	}

	Ipp32sArray HistogramIpp::computeF(const Ipp32fArray & image)
	{
		if (image.getDataType() != _dataType)
		{
			throw std::runtime_error(string(__func__) + "Image types sizes don't match");
		}

		if (image.getComponents() != _channels)
		{
			throw std::runtime_error(string(__func__) + "Number of channels don't match");
		}

		if ((image.getSize().width != _size.width) || (image.getSize().height != _size.height))
		{
			throw std::runtime_error(string(__func__) + "ROI sizes don't match");
		}

		if (_histogramData == nullptr)

		{
			_histogramData = new Ipp32uP[_channels];
		}

		Ipp32sArray result({ _bins, _channels });
		for (auto c = 0; c < _channels; c++)
		{
			_histogramData[c] = (Ipp32u *)result.getRowPointer(c);
		}

		compute_32f(image, _histogramData);

		return result;
	}

	vector<vector<Ipp32u>> HistogramIpp::compute(const Ipp16uArray &image)
	{
		if (image.getDataType() != _dataType)
		{
			THROW_MTI_RUNTIME_ERROR(string(__func__) + "Image types sizes don't match");
		}

		if (image.getComponents() != _channels)
		{
			THROW_MTI_RUNTIME_ERROR(string(__func__) + "Number of channels don't match");
		}

		if ((image.getSize().width != _size.width) || (image.getSize().height != _size.height))
		{
			THROW_MTI_RUNTIME_ERROR(string(__func__) + "ROI sizes don't match");
		}

		if (_histogramData == nullptr)

		{
			_histogramData = new Ipp32uP[_channels];
		}

		vector<vector<Ipp32u>> result;
		for (auto c = 0; c < _channels; c++)
		{
			result.emplace_back(_bins);
			_histogramData[c] = result.back().data();
		}

		compute_16u(image, _histogramData);

		return result;
	}

	vector<vector<Ipp32u>> HistogramIpp::compute(const Ipp8uArray &image)
	{
		if (image.getDataType() != _dataType)
		{
			THROW_MTI_RUNTIME_ERROR(string(__func__) + "Image types sizes don't match");
		}

		if (image.getComponents() != _channels)
		{
			THROW_MTI_RUNTIME_ERROR(string(__func__) + "Number of channels don't match");
		}

		if ((image.getSize().width != _size.width) || (image.getSize().height != _size.height))
		{
			THROW_MTI_RUNTIME_ERROR(string(__func__) + "ROI sizes don't match");
		}

		if (_histogramData == nullptr)

		{
			_histogramData = new Ipp32uP[_channels];
		}

		vector<vector<Ipp32u>> result;
		for (auto c = 0; c < _channels; c++)
		{
			result.emplace_back(_bins);
			_histogramData[c] = result.back().data();
		}

		compute_8u(image, _histogramData);

		return result;
	}

	vector<vector<Ipp32u>> HistogramIpp::compute(const Ipp32sArray &image)
	{
		// Covert to 32f and then do histogram, this is
		// a waste of memory and copying but we can do it properly later
		auto image32f = Ipp32fArray(image);
		return compute(image32f);
	}

	vector<Ipp32u> HistogramIpp::counts(const vector<vector<Ipp32u>> &histograms)
	{
		vector<Ipp32u> result;

		// Sum up counts
		for (auto& histogram : histograms)
		{
			auto const p = reinterpret_cast<const Ipp32s *> (histogram.data());
			auto len = int(histogram.size());
			Ipp32s sum;
			IppThrowOnError(ippsSum_32s_Sfs(p, len, &sum, 0));
			result.push_back((Ipp32u)sum);
		}

		return result;
	}

	class HRTimer
	{
	public:
		HRTimer()
		{
			start();
		}

		void start()
		{
			_started = true;
			_start = std::chrono::system_clock::now();
		}

		void stop()
		{
			_started = false;
			_end = std::chrono::system_clock::now();
		}

		double ElapsedMilliseconds()
		{
			auto end = _end;
			if (_started == false)
			{
				end = std::chrono::system_clock::now();
			}

			return std::chrono::duration_cast<std::chrono::microseconds>(end - _start).count() / 1000.0;
		}

	private:
		bool _started;
		std::chrono::system_clock::time_point _start;
		std::chrono::system_clock::time_point _end;
	};

	vector<vector<Ipp32f>> HistogramIpp::computeProbability(const Ipp32fArray &image)
	{
		auto histograms = compute(image);
		auto sums = counts(histograms);
		vector<vector<Ipp32f>> result;

		// The speed depends on the number of bins and what
		// CPU is runing.  Ipp and histogram are about as
		// fast.  

		//  IPP code
		//for (auto i = 0; i < histograms.size(); i++)
		//{
		//	// Ipp is probably not faster
		//	auto &histogram = histograms[i];
		//	result.emplace_back(histogram.size());
		//	auto &b = result.back();
		//	auto p = reinterpret_cast<Ipp32s *>(histogram.data());
		//	ippsConvert_32s32f(p, b.data(), int(histogram.size()));
		//	ippsDivC_32f_I(float(sums[i]), b.data(), int(histogram.size()));
		//}

		for (auto i = 0; i < histograms.size(); i++)
		{
			// Ipp is probably not faster
			auto &histogram = histograms[i];
			auto d = 1.0/double(sums[i]);
			result.emplace_back(histogram.size());
			auto bp = result.back().data();
			auto p = reinterpret_cast<Ipp32s *>(histogram.data());
			for (auto j = 0; j < histogram.size(); j++)
			{
				*bp++ = Ipp32f(*p++ *d);
			}
		}

		return result;
	}

	void HistogramIpp::compute_16u(const Ipp16uArray &image, Ipp32u *histogramData[])
	{
		// calculate histogram_
		IppiSize roiSize = { image.getSize().width, image.getSize().height };

		auto sourceStep = (int)image.getRowPitchInBytes();
		auto source = image.data();

		switch (_channels)
		{
		case 1:
			IppThrowOnError(ippiHistogram_16u_C1R(source, sourceStep, roiSize, histogramData[0], _histogramObject, _opaqueBuffer));
			break;

		case 3:
			IppThrowOnError(ippiHistogram_16u_C3R(source, sourceStep, roiSize, histogramData, _histogramObject, _opaqueBuffer));
			break;

		default:
			THROW_MTI_RUNTIME_ERROR(string(__func__) + " Unsupported number of channels");
		}
	}

	//void HistogramIpp::compute_32s(const Ipp16uArray &image, Ipp32u *histogramData[])
	//{
	//	// calculate histogram_
	//	IppiSize roiSize = { image.getSize().width, image.getSize().height };

	//	auto sourceStep = (int)image.getRowPitchInBytes();
	//	auto source = image.data();

	//	// We are going to convert to 32f and return that, this is a waste
	//	// but we can do it properly later
	//	auto image32f = Ipp32fArray()
	//	switch (_channels)
	//	{
	//	case 1:
	//		IppThrowOnError(ippiHistogram_32s_C1R(source, sourceStep, roiSize, histogramData[0], _histogramObject, _opaqueBuffer));
	//		break;

	//	case 3:
	//		IppThrowOnError(ippiHistogram_16u_C3R(source, sourceStep, roiSize, histogramData, _histogramObject, _opaqueBuffer));
	//		break;

	//	default:
	//		THROW_MTI_RUNTIME_ERROR(string(__func__) + " Unsupported number of channels");
	//	}
	//}

	//void HistogramIpp::compute_16s(const Ipp32fArray &image, Ipp32u *histogramData[])
	//{
	//	// calculate histogram_
	//	IppiSize roiSize = { image.getSize().width, image.getSize().height };

	//	auto sourceStep = image.getRowPitchInBytes();
	//	auto source = (Ipp16s *)image.data();

	//	switch (_channels)
	//	{
	//	case 1:
	//		IppThrowOnError(ippiHistogram_16s_C1R(source, sourceStep, roiSize, histogramData[0], _histogramObject, _opaqueBuffer));
	//		break;

	//	case 3:
	//		IppThrowOnError(ippiHistogram_16s_C3R(source, sourceStep, roiSize, histogramData, _histogramObject, _opaqueBuffer));
	//		break;

	//	default:
	//		THROW_MTI_RUNTIME_ERROR(string(__func__) + " Unsupported number of channels");
	//	}
	//}

	void HistogramIpp::compute_32f(const Ipp32fArray &image, Ipp32u *histogramData[])
	{
		// calculate histogram_
		IppiSize roiSize = { image.getSize().width, image.getSize().height };

		auto sourceStep = (int)image.getRowPitchInBytes();
		auto source = (Ipp32f *)image.data();

		switch (_channels)
		{
		case 1:
			IppThrowOnError(ippiHistogram_32f_C1R(source, sourceStep, roiSize, histogramData[0], _histogramObject, _opaqueBuffer));
			break;

		case 3:
			IppThrowOnError(ippiHistogram_32f_C3R(source, sourceStep, roiSize, histogramData, _histogramObject, _opaqueBuffer));
			break;

		default:
			THROW_MTI_RUNTIME_ERROR(string(__func__) + " Unsupported number of channels");
		}
	}

	void HistogramIpp::compute_8u(const Ipp8uArray &image, Ipp32u *histogramData[])
	{
		// calculate histogram
		IppiSize roiSize = { image.getSize().width, image.getSize().height };

		auto sourceStep = (int)image.getRowPitchInBytes();
		auto source = (Ipp8u *)image.data();

		switch (_channels)
		{
		case 1:
			IppThrowOnError(ippiHistogram_8u_C1R(source, sourceStep, roiSize, histogramData[0], _histogramObject, _opaqueBuffer));
			break;

		case 3:
			IppThrowOnError(ippiHistogram_8u_C3R(source, sourceStep, roiSize, histogramData, _histogramObject, _opaqueBuffer));
			break;

		default:
			THROW_MTI_RUNTIME_ERROR(string(__func__) + " Unsupported number of channels");
		}
	}

	/// <summary>
	/// Initializes this _instance using the constructor parameters
	/// </summary>
	void HistogramIpp::initialize()
	{
		auto ippDataType = getDataType();

		_lowerLevel = new Ipp32f[_channels];
		_upperLevel = new Ipp32f[_channels];

		_levels = new int[_channels];
		for (auto c = 0; c < _channels; c++)
		{
			_levels[c] = _bins + 1;
			_lowerLevel[c] = _dataMin;
			_upperLevel[c] = _dataMax;
		}

		int sizeObject, sizeBuffer;

		// get sizes for spec and buffer
		IppiSize roiSize = {_size.width, _size.height };

		// Initialize the histogram's data structures
		IppThrowOnError(ippiHistogramGetBufferSize(ippDataType, roiSize, _levels, _channels, 1, &sizeObject, &sizeBuffer));
		_histogramObject = (IppiHistogramSpec *)ippsMalloc_8u(sizeObject);
		_opaqueBuffer = (Ipp8u*)ippsMalloc_8u(sizeBuffer);

		// initialize levels
		IppThrowOnError(ippiHistogramUniformInit(ippDataType, _lowerLevel, _upperLevel, _levels, _channels, _histogramObject));

		// Clean up temporary memory
		delete[] _lowerLevel;
		delete[] _upperLevel;
		delete[] _levels;

		_lowerLevel = nullptr;
		_upperLevel = nullptr;
		_levels = nullptr;
	}

	/// <summary>
	/// Determines whether the two histogram are similar in size, bins, data range, depth and channels.  Note
	/// this means they will work on the same image.
	/// </summary>
	/// <param name="size">The size.</param>
	/// <param name="dataType">Type of the data.</param>
	/// <param name="bins">The bins.</param>
	/// <param name="dataMin">The data minimum.</param>
	/// <param name="dataMax">The data maximum.</param>
	/// <returns>true if they compute the same</returns>
	bool HistogramIpp::isSameAs(const IppiSize &size, int channels, IppDataType dataType, int bins, float dataMin, float dataMax) const
	{
		float dmin;
		float dmax;
		findActualMinMax(dmin, dmax, dataType);

		return (getChannels() == channels) &&
			(getDataType() == dataType) &&
			(getBins() == bins) &&
			(getDataMin() == dmin) &&
			(getDataMax() == dmax) &&
			(getSize().height == size.height) &&
			(getSize().width == size.width);
	}

	void HistogramIpp::freeAllMemory()
	{
		ippsFree(_histogramObject);
		ippsFree(_opaqueBuffer);

		// Clean up memory
		delete[] _lowerLevel;
		delete[] _upperLevel;
		delete[] _levels;
		delete[] _histogramData;

		_lowerLevel = nullptr;
		_upperLevel = nullptr;
		_levels = nullptr;
		_histogramObject = nullptr;
		_opaqueBuffer = nullptr;
		_histogramData = nullptr;
	}

	vector<float> HistogramIpp::getLevels() const
	{
		Ipp32f *ppLevels[3];
		vector<vector<float>> levels;
		for (auto i = 0; i < _channels; i++)
		{
			levels.emplace_back(_bins + 1);
			ppLevels[i] = (Ipp32f *)levels[i].data();
		}

		// We have at most 3 channels, so we don't care if there are less
		auto sts = ippiHistogramGetLevels(_histogramObject, ppLevels);
		vector<float> results(_bins + 1);
		memcpy_s(results.data(), results.size() * sizeof(float), ppLevels[0], results.size() * sizeof(float));
		return results;
	}