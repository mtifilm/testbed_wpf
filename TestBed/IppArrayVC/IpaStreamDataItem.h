#pragma once

#include "Ippheaders.h"
#include <functional>
#include <stack>
#include <cassert>

class MTI_IPPARRAY_API IpaStreamDataItem
{
public:
	IpaStreamDataItem() = default;

	IpaStreamDataItem(Ipp64f value)
	{
		_scalarType = ipp64f;
		_scalarValue._double = value;
	}

	IpaStreamDataItem(Ipp32f value)
	{
		_scalarType = ipp32f;
		_scalarValue._float = value;
	}

	IpaStreamDataItem(Ipp32s value)
	{
		_scalarType = ipp32s;
		_scalarValue._int = value;
	}

	// This is not correct, a size_t should not be a signed int
	// TODO  Fix this.
	IpaStreamDataItem(size_t value)
	{
		_scalarType = ipp32s;
		_scalarValue._int = int(value);
	}

	IpaStreamDataItem(Ipp16u value)
	{
		_scalarType = ipp16u;
		_scalarValue._unsignedShort = value;
	}

	IpaStreamDataItem(Ipp8u value)
	{
		_scalarType = ipp8u;
		_scalarValue._byte = value;
	}

	IpaStreamDataItem(const Ipp8uArray &rhs)
	{
		_arrayType = ipp8u;
		_ipp8uArray = rhs;
	}

	IpaStreamDataItem(const Ipp16uArray &rhs)
	{
		_arrayType = ipp16u;
		_ipp16uArray = rhs;
	}

	IpaStreamDataItem(const Ipp32fArray &rhs)
	{
		_arrayType = ipp32f;
		_ipp32fArray = rhs;
	}

	IpaStreamDataItem(const Ipp32sArray &rhs)
	{
		_arrayType = ipp32s;
		_ipp32sArray = rhs;
	}

	[[nodiscard]] IppDataType getArrayType() const { return _arrayType; }
	[[nodiscard]] IppDataType getScalarType() const { return _scalarType; }

	[[nodiscard]] bool isArray() const { return _arrayType != ippUndef && _scalarType == ippUndef; }

	[[nodiscard]] Ipp64f getIpp64f() const;
	[[nodiscard]] Ipp32f getIpp32f() const;
	[[nodiscard]] Ipp32s getIpp32s() const;
	[[nodiscard]] Ipp16u getIpp16u() const;
	[[nodiscard]] Ipp8u  getIpp8u() const;

	[[nodiscard]] Ipp8uArray getIpp8uArray() const;
	[[nodiscard]] Ipp16uArray getIpp16uArray() const;
	[[nodiscard]] Ipp32fArray getIpp32fArray() const;
	[[nodiscard]] Ipp32sArray getIpp32sArray() const;

private:
	IppDataType _arrayType = ippUndef;
	IppDataType _scalarType = ippUndef;

	union ScalarValue
	{
		Ipp64f _double;
		Ipp32f _float;
		Ipp32s _int;
		Ipp16u _unsignedShort;
		Ipp8u  _byte;
	} _scalarValue;

	Ipp8uArray _ipp8uArray;
	Ipp16uArray _ipp16uArray;
	Ipp32fArray _ipp32fArray;
	Ipp32sArray _ipp32sArray;
};

