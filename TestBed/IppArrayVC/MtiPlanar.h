#pragma once
#ifndef MtiPlanarH
#define MtiPlanarH

#include "MtiIppExtensions.h"

template<class T>
class MtiPlanar final
{
public:
   MtiPlanar() = default;

   MtiPlanar(const MtiSize &size)
   {
      for (auto c : range(size.depth))
      {
         push_back(T({ size.width, size.height }));
      }
   }

   MtiPlanar(std::initializer_list<T> data)
   {
      for (auto &l : data)
      {
         push_back(l);
      }
   }

   MtiPlanar(const vector<T> &data)
   {
      for (auto &l : data)
      {
         push_back(l);
      }
   }

   ~MtiPlanar() = default;

   MtiPlanar& operator = (const MtiPlanar &planes)
   {
      if (this == &planes)
      {
         return *this;
      }

      clear();
      for (auto &p : planes)
      {
         push_back(p);
      }

      return *this;
   }

   MtiPlanar operator () (const MtiRect &roi) const
   {
      MtiPlanar result;
      for (auto &p : _planes)
      {
         result.push_back(p(roi));
      }

      return result;
   }

   bool operator==(const MtiPlanar& rhs) const
   {
      return _planeSize == rhs._planeSize
         && _planes == rhs._planes;
   }

   bool operator!=(const MtiPlanar& rhs) const
   {
      return !(*this == rhs);
   }

   MtiPlanar& operator = (const vector<T> &planes)
   {
      clear();
      for (auto &p : planes)
      {
         push_back(p);
      }

      return *this;
   }

   [[nodiscard]] T median() const
   {
      vector<T> temp;
      // To do, find proper way
      for (auto &plane : _planes)
      {
         temp.push_back(plane.duplicate());
      }

      bubbleSortB(temp);
      return temp[size() / 2];
   }

   static vector<int> pingPongIndices(int pad, int centerSize)
   {
      auto direction = 1;
      auto position = 0;
      auto compare = centerSize - 1;
      vector<int> result;
      for (auto i = 0; i < pad; i++)
      {
         result.push_back(position);
         if (position == compare)
         {
            direction = -direction;
            compare = direction < 0 ? 0 : centerSize - 1;
         }
         else
         {
            position += direction;
         }
      }

      return result;
   }

   static vector<int> findPaddingIndices(int beginSize, int centerSize, int endSize)
   {
      vector<int> result(beginSize);
      auto beginIndices = pingPongIndices(beginSize, centerSize);

      // Copy in reverse
      for (auto i : range(beginSize))
      {
         result[beginSize - i - 1] = beginIndices[i];
      }

      // Copy center
      for (auto i : range(centerSize))
      {
         result.push_back(i);
      }

      auto endIndices = pingPongIndices(endSize, centerSize);

      // Copy but reverse index
      for (auto i : range(endSize))
      {
         result.push_back(centerSize - 1 - endIndices[i]);
      }

      return result;
   }

   MtiPlanar duplicateAndPadEnds(int beginPad, int endPad, MtiSelectMode copyType = MtiSelectMode::mirror)
   {
      auto newSize = getPlanarSize();
      newSize.depth += beginPad + endPad;
      auto n = int(size());

      MtiPlanar result;
      switch (copyType)
      {
      case MtiSelectMode::mirror:
      {
         auto paddingIndices = findPaddingIndices(beginPad, size(), endPad);
         for (auto i : paddingIndices)
         {
            result.push_back(_planes[i]);
         }
      }
      break;

      default:
         THROW_MTI_RUNTIME_ERROR("Unsupported padding type");
      }

      return result;
   }

   MtiPlanar(const MtiPlanar& other)
      :_planeSize(other._planeSize), _planes(other._planes)
   {
   }

   MtiPlanar(MtiPlanar&& other) noexcept
      : _planeSize(std::move(other._planeSize)), _planes(std::move(other._planes))
   {
   }

   MtiPlanar& operator=(MtiPlanar&& other) noexcept
   {
      if (this == &other)
      {
         return *this;
      }

      _planeSize = std::move(other._planeSize);
      _planes = std::move(other._planes);
      return *this;
   }

   void clear()
   {
      _planes.clear();
   }

   void push_back(const T& value)
   {
      // Emplace does a values check
      emplace_back(value);
   }

   [[nodiscard]] const T &back() const
   {
      return _planes.back();
   }

   [[nodiscard]] const T &front() const
   {
      return _planes.front();
   }

   T &back()
   {
      return _planes.back();
   }

   T &front()
   {
      return _planes.front();
   }

   // WARNING: In order to make this look like a vector, we use size to be
   // the number of planes, not the size of the planar object or the plane
   [[nodiscard]] size_t size() const
   {
      return _planes.size();
   }

   T& operator[] (int idx)
   {
      return _planes[idx];
   }

   const T &operator[] (int idx) const
   {
      return _planes[idx];
   }

   // Size of individual plane
   [[nodiscard]] MtiSize getPlaneSize() const
   {
      return _planeSize;
   }

   // Size of each plane, depth is number of planes
   [[nodiscard]] MtiSize getPlanarSize() const
   {
      return { _planeSize.width, _planeSize.height, int(size()) };
   }

   typename std::vector<T>::iterator begin() noexcept
   {
      return _planes.begin();
   }

   typename std::vector<T>::iterator end() noexcept
   {
      return _planes.end();
   }

   [[nodiscard]] typename std::vector<T>::const_iterator begin() const noexcept
   {
      return _planes.begin();
   }

   [[nodiscard]] typename std::vector<T>::const_iterator end() const noexcept
   {
      return _planes.end();
   }

   void emplace_back(const T& value)
   {
      if (value.getComponents() != 1)
      {
         throw std::runtime_error("Planes can have only one channel");
      }

      if (size() == 0)
      {
         _planeSize = value.getSize();
      }
      else
      {
         if (getPlaneSize() != value.getSize())
         {
            throw std::runtime_error("Every plane must be same size and type");
         }
      }

      _planes.emplace_back(value);
   }

   void zero()
   {
      for (T plane : *this)
      {
         plane.zero();
      }
   }

   T trace(const MtiPoint &p)
   {
      T result(int(this->size()));
      auto iter = result.begin();

      for (T plane : *this)
      {
         *iter++ = plane[MtiPoint(p.x, p.y)];
      }

      return result;
   }

   void insertTrace(const MtiPoint &p, const T &trace)
   {
      if (getPlanarSize().depth != trace.getWidth() || trace.getHeight() != 1)
      {
         THROW_MTI_RUNTIME_ERROR("trace size does not match target size");
      }

      auto iterator = trace.cbegin();

      for (T plane : *this)
      {
         plane[MtiPoint(p.x, p.y)] = *iterator++;
      }
   }

   [[nodiscard]] MtiPlanar<T> cumulativeSum() const
   {
      MtiPlanar<T> result;
      if (this->size() == 0)
      {
         return result;
      }

      T a0;
      a0 <<= _planes[0];
      result.push_back(a0);

      for (auto i = 1; i < size(); i++)
      {
         auto sumArray = result[i - 1] + _planes[i];
         result.push_back(sumArray);
      }

      return result;
   }

   void cumulativeSumInplace()
   {
      if (_planes.size() == 0)
      {
         return;
      }

      for (auto i = 1; i < this->size(); i++)
      {
         _planes[i] += _planes[i - 1];
      }
   }

   void sortInPlace()
   {
      auto n = size();
      for (auto i = 0; i < n - 1; i++)
      {
         for (auto j = 0; j < n - i - 1; j++)
         {
            SortB(_planes[j], _planes[j + 1]);
         }
      }
   }

   T toArray()
   {
	   T result;
	   result.copyFromPlanar(*this);
	   return result;
   }


private:
   MtiSize _planeSize;
   vector<T> _planes;
};
#endif