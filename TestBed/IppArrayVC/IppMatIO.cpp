#include "IppMatIO.h"
#include "Ippheaders.h"
#include "matio_private.h"

vector <std::string> getMatVariableNames(_mat_t *mat)
{
	vector <std::string> results;
	size_t n;
	char** dir = Mat_GetDir(mat, &n);
	if (dir == nullptr)
	{
		return results;
	}

	for (auto i = 0; i < n; ++i)
	{
		if (dir[i] == nullptr)
		{
			continue;
		}

		results.push_back(dir[i]);
	}

	return results;
}

vector <std::string> MatIO::getVariableNames(_mat_t *mat)
{
	return getMatVariableNames(mat);
}

template<typename T, typename AT>
int _saveToMat(_mat_t *mat, const AT &inArray, const std::string & varName)
{
	auto height = inArray.getHeight();
	auto width = inArray.getWidth();
	auto channels = inArray.getComponents();

	// this is very inefficient, but I don't care
	auto planes = inArray.toPlanar();

	// We need to the output contiguous
	auto rawData = new T[height * width * channels];

	auto rawDataPointer = rawData;
	for (auto &plane : planes)
	{
		auto pt = plane.transpose();
		for (auto p : pt.rowIterator())
		{
			memcpy(rawDataPointer, p, pt.getRowPitchInBytes());
			rawDataPointer += pt.getRowPitchInBytes() / sizeof(T);
		}
	}

	size_t dimSize[] = { size_t(height), size_t(width), size_t(channels) };
	auto dimensions = channels == 1 ? 2 : 3;

	matio_classes class_type;
	enum matio_types data_type;

	switch (inArray.getDataType())
	{
	case ipp8u:
		class_type = MAT_C_UINT8;
		data_type = MAT_T_UINT8;
		break;

	case ipp16u:
		class_type = MAT_C_UINT16;
		data_type = MAT_T_UINT16;
		break;

	case ipp16s:
		class_type = MAT_C_INT16;
		data_type = MAT_T_INT16;
		break;

	case ipp32s:
		class_type = MAT_C_INT32;
		data_type = MAT_T_INT32;
		break;

	case ipp32f:
		class_type = MAT_C_SINGLE;
		data_type = MAT_T_SINGLE;
		break;

	default:
		throw std::runtime_error("Unsupported data type");
	}

	auto matvar = Mat_VarCreate
	(
		varName.c_str(),
		class_type,
		data_type,
		dimensions,
		dimSize,
		rawData,
		MAT_F_DONT_COPY_DATA
	);

	auto err = Mat_VarWrite(mat, matvar, matio_compression::MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);
	delete[] rawData;

	return err;
}

template<typename T, typename AT>
int _saveAsMat(const std::string & fileName, const AT &inArray, const std::string & varName)
{
	auto mat = Mat_CreateVer(fileName.c_str(), nullptr, MAT_FT_DEFAULT);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not create: '") + fileName + "'");
	}

	return _saveToMat<T>(mat, inArray, varName);

	return Mat_Close(mat);
}

int MatIO::appendToMat(_mat_t *mat, const Ipp8uArray &inArray, const std::string &varName)
{
	return _saveToMat<Ipp8u>(mat, inArray, varName);
}

int MTI_IPPARRAY_API MatIO::appendToMat(_mat_t * mat, const Ipp16uArray & inArray, const std::string & varName)
{
	return _saveToMat<Ipp16u>(mat, inArray, varName);
}

int MTI_IPPARRAY_API MatIO::appendToMat(_mat_t * mat, const Ipp32sArray & inArray, const std::string & varName)
{
	return _saveToMat<Ipp32s>(mat, inArray, varName);
}

int MTI_IPPARRAY_API MatIO::appendToMat(_mat_t * mat, const Ipp32fArray & inArray, const std::string & varName)
{
	return _saveToMat<Ipp32f>(mat, inArray, varName);
}

int MatIO::appendToMat(_mat_t * mat, int value, const std::string & varName)
{
	size_t dimSize[] = { size_t(1) };

	auto matvar = Mat_VarCreate
	(
		varName.c_str(),
		MAT_C_INT32,
		MAT_T_INT32,
		size_t(1),
		dimSize,
		&value,
		MAT_F_DONT_COPY_DATA
	);

	auto err = Mat_VarWrite(mat, matvar, matio_compression::MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);

	return err;
}

int MatIO::appendToMat(_mat_t * mat, float value, const std::string & varName)
{
	size_t dimSize[] = { size_t(1) };

	auto matvar = Mat_VarCreate
	(
		varName.c_str(),
		MAT_C_SINGLE,
		MAT_T_SINGLE,
		size_t(1),
		dimSize,
		&value,
		MAT_F_DONT_COPY_DATA
	);

	auto err = Mat_VarWrite(mat, matvar, matio_compression::MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);

	return err;
}

int MatIO::appendToMat(_mat_t * mat, double value, const std::string & varName)
{
	size_t dimSize[] = { size_t(1) };

	auto matvar = Mat_VarCreate
	(
		varName.c_str(),
		MAT_C_DOUBLE,
		MAT_T_DOUBLE,
		size_t(1),
		dimSize,
		&value,
		MAT_F_DONT_COPY_DATA
	);

	auto err = Mat_VarWrite(mat, matvar, matio_compression::MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);

	return err;
}

int MatIO::appendToMat(_mat_t * mat, const std::string & value, const std::string & varName)
{
	size_t dimSize[] = { 1, value.size() };
	auto data = (void *)value.c_str();
	auto matvar = Mat_VarCreate
	(
		varName.c_str(),
		MAT_C_CHAR,
		MAT_T_UINT8,
		2,
		dimSize,
		data,
		MAT_F_DONT_COPY_DATA
	);

	auto err = Mat_VarWrite(mat, matvar, matio_compression::MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);

	return err;
}

int MatIO::appendToMat(_mat_t * mat, vector<Ipp32f>& value, const std::string & varName)
{
	const Ipp32fArray out({ int(value.size()), 1, 1 }, value.data());
	return MatIO::appendToMat(mat, out, varName);
}

int MatIO::appendToMat(_mat_t * mat, vector<Ipp64f>& value, const std::string & varName)
{
	auto width = value.size();
	auto rawDataPointer = value.data();

	size_t dimSize[] = { size_t(1), size_t(width), size_t(1) };
	auto dimensions = 2;

	matio_classes class_type;
	enum matio_types data_type;

	class_type = MAT_C_DOUBLE;
	data_type = MAT_T_DOUBLE;

	auto matvar = Mat_VarCreate
	(
		varName.c_str(),
		class_type,
		data_type,
		dimensions,
		dimSize,
		rawDataPointer,
		MAT_F_DONT_COPY_DATA
	);

	auto err = Mat_VarWrite(mat, matvar, matio_compression::MAT_COMPRESSION_NONE);
	Mat_VarFree(matvar);

	return err;
}

int MatIO::appendToMat(_mat_t * mat, vector<Ipp32s>& value, const std::string & varName)
{
	return appendToMatVector(mat, value, varName);
}

int MatIO::appendToMat(_mat_t * mat, vector<Ipp32u>& value, const std::string & varName)
{
	return appendToMatVector(mat, value, varName);
}

int MatIO::appendToMat(_mat_t * mat, vector<Ipp16u>& value, const std::string & varName)
{
	return appendToMatVector(mat, value, varName);
}

int MatIO::appendToMat(_mat_t * mat, vector<Ipp8u>& value, const std::string & varName)
{
	return appendToMatVector(mat, value, varName);
}

int MatIO::appendToMat(_mat_t * mat, const MtiRect & value, const std::string & varName)
{
	matvar_t *matvar[5];
	matvar_t *struct_matvar = nullptr;
	size_t dims[2] = { 1,1 };

	auto matvar_class = MAT_C_INT32;
	auto data_type = MAT_T_INT32;
	matvar[0] = Mat_VarCreate("x", matvar_class, data_type, 2, dims, (void *)&value.x, MAT_F_DONT_COPY_DATA);
	matvar[1] = Mat_VarCreate("y", matvar_class, data_type, 2, dims, (void *)&value.y, MAT_F_DONT_COPY_DATA);

	matvar[2] = Mat_VarCreate("width", matvar_class, data_type, 2, dims, (void *)&value.width, MAT_F_DONT_COPY_DATA);
	matvar[3] = Mat_VarCreate("height", matvar_class, data_type, 2, dims, (void *)&value.height, MAT_F_DONT_COPY_DATA);
	matvar[4] = nullptr;

	struct_matvar = Mat_VarCreate(varName.c_str(), MAT_C_STRUCT, MAT_T_STRUCT, 2, dims, matvar, 0);
	const auto err = Mat_VarWrite(mat, struct_matvar, matio_compression::MAT_COMPRESSION_NONE);

	// This frees the array of matvar
	Mat_VarFree(struct_matvar);

	return err;
}

// Returns a vector whose data is planer, contiguous and transposed from the
// interleaved,  ROI data of an IppArray.
template<typename T, typename U>
vector<U> getMatLabArrayFromIppArray(const T &inArray)
{
	// this is very inefficient, but I don't care
	auto planes = inArray.toPlanar();

	// We need to the output vector
	vector<U> result(inArray.volume());
	auto rawDataPointer = result.data();
	for (auto &plane : planes)
	{
		auto pt = plane.transpose();
		for (auto p : pt.rowIterator())
		{
			memcpy(rawDataPointer, p, pt.getRowPitchInBytes());
			rawDataPointer += pt.getRowPitchInBytes() / sizeof(U);
		}
	}

	return result;
}

int MatIO::appendToMat(_mat_t *mat, const vector<Ipp32fArray> &value, const std::string &varName)
{
	// Do nothing if empty
	if (value.size() == 0)
	{
		return 0;
	}

	matvar_t *cellmatvar;
	vector<matvar_t *> cellfields;

	for (auto i : range(value.size()))
	{
		auto &a = value[i];
		auto height = a.getHeight();
		auto width = a.getWidth();
		auto channels = a.getComponents();

		auto rawData = getMatLabArrayFromIppArray<Ipp32fArray, Ipp32f>(a);
		size_t dimSize[] = { size_t(height), size_t(width), size_t(channels) };
		auto dimensions = channels == 1 ? 2 : 3;

		matio_classes class_type = MAT_C_SINGLE;
		enum matio_types data_type = MAT_T_SINGLE;

		auto matvar = Mat_VarCreate
		(
			nullptr,
			class_type,
			data_type,
			dimensions,
			dimSize,
			a.data(),
			MAT_F_DONT_COPY_DATA
		);

		cellfields.push_back(matvar);
	}

	cellfields.push_back(nullptr);

	size_t dims[] = { value.size(), 1 };
	cellmatvar = Mat_VarCreate(varName.c_str(), MAT_C_CELL, MAT_T_CELL, 2,
		dims, cellfields.data(), 0);

	auto err = Mat_VarWrite(mat, cellmatvar, matio_compression::MAT_COMPRESSION_NONE);

	// This frees up the cellfields vector entries
	Mat_VarFree(cellmatvar);

	return err;
}

int MatIO::appendToMat(_mat_t *mat, const vector<Ipp32sArray> &value, const std::string &varName)
{
   // Do nothing if empty
   if (value.empty())
   {
      return 0;
   }

   matvar_t *cellmatvar;
   vector<matvar_t *> cellfields;

   for (auto i : range(value.size()))
   {
      auto &a = value[i];
      auto height = a.getHeight();
      auto width = a.getWidth();
      auto channels = a.getComponents();

      auto rawData = getMatLabArrayFromIppArray<Ipp32sArray, Ipp32s>(a);
      size_t dimSize[] = { size_t(height), size_t(width), size_t(channels) };
      auto dimensions = channels == 1 ? 2 : 3;

      matio_classes class_type = MAT_C_INT32;
      enum matio_types data_type = MAT_T_INT32;

      auto matvar = Mat_VarCreate
      (
         nullptr,
         class_type,
         data_type,
         dimensions,
         dimSize,
         a.data(),
         MAT_F_DONT_COPY_DATA
      );

      cellfields.push_back(matvar);
   }

   cellfields.push_back(nullptr);

   size_t dims[] = { value.size(), 1 };
   cellmatvar = Mat_VarCreate(varName.c_str(), MAT_C_CELL, MAT_T_CELL, 2,
      dims, cellfields.data(), 0);

   auto err = Mat_VarWrite(mat, cellmatvar, matio_compression::MAT_COMPRESSION_NONE);

   // This frees up the cellfields vector entries
   Mat_VarFree(cellmatvar);

   return err;
}

// Saves a Planer vector to a matlab array
// Remember planer is vector of 1 channel and same size arrays
int MatIO::appendToMat(_mat_t *mat, const MtiPlanar<Ipp32fArray> &value, const std::string &varName)
{
	// Do nothing if empty
	if (value.size() == 0)
	{
		return 0;
	}

	auto width = value[0].getWidth();
	auto height = value[0].getHeight();
	auto pseudoChannels = value.size();

	auto outputSizePixels = width * height * pseudoChannels;
	vector<Ipp32f> tempMem(outputSizePixels);

	auto outputSizeByte = outputSizePixels * sizeof(Ipp32f);
	auto outputData = tempMem.data();

	for (auto i : range(value.size()))
	{
		auto &a = value[i];
		if ((a.getComponents() != 1) || (a.getHeight() != height) || (a.getWidth() != width))
		{
			MTIostringstream os;
			os << "Planer Arrays must have only one channel and same shape, " << "expected (" << MtiSize({ width, height, 1 }) << "), actual " << a.getSize() << "!";
			throw std::runtime_error(os.str());
		}

		auto x = a.toVector()[0];

		auto rawData = getMatLabArrayFromIppArray<Ipp32fArray, Ipp32f>(a);
		memcpy(outputData, rawData.data(), rawData.size() * sizeof(Ipp32f));
		outputData += rawData.size();
	}

	size_t dimSize[] = { size_t(height), size_t(width), size_t(pseudoChannels) };
	auto dimensions = 3;

	matio_classes class_type = MAT_C_SINGLE;
	enum matio_types data_type = MAT_T_SINGLE;

	auto matvar = Mat_VarCreate
	(
		varName.c_str(),
		class_type,
		data_type,
		dimensions,
		dimSize,
		tempMem.data(),
		MAT_F_DONT_COPY_DATA
	);

	auto err = Mat_VarWrite(mat, matvar, matio_compression::MAT_COMPRESSION_NONE);

	// Clean up
	Mat_VarFree(matvar);

	return err;
}

int MatIO::appendToMat(_mat_t* mat, const MtiPlanar<Ipp32sArray>& value, const std::string& varName)
{
   // Do nothing if empty
   if (value.size() == 0)
   {
      return 0;
   }

   auto width = value[0].getWidth();
   auto height = value[0].getHeight();
   auto pseudoChannels = value.size();

   auto outputSizePixels = width * height * pseudoChannels;
   vector<Ipp32s> tempMem(outputSizePixels);

   auto outputSizeByte = outputSizePixels * sizeof(Ipp32f);
   auto outputData = tempMem.data();

   for (auto i : range(value.size()))
   {
      auto &a = value[i];
      if ((a.getComponents() != 1) || (a.getHeight() != height) || (a.getWidth() != width))
      {
         MTIostringstream os;
         os << "Planer Arrays must have only one channel and same shape, " << "expected (" << MtiSize({ width, height, 1 }) << "), actual " << a.getSize() << "!";
         throw std::runtime_error(os.str());
      }

      auto x = a.toVector()[0];

      auto rawData = getMatLabArrayFromIppArray<Ipp32sArray, Ipp32s>(a);
      memcpy(outputData, rawData.data(), rawData.size() * sizeof(Ipp32s));
      outputData += rawData.size();
   }

   size_t dimSize[] = { size_t(height), size_t(width), size_t(pseudoChannels) };
   auto dimensions = 3;

   matio_classes class_type = MAT_C_SINGLE;
   enum matio_types data_type = MAT_T_SINGLE;

   auto matvar = Mat_VarCreate
   (
      varName.c_str(),
      class_type,
      data_type,
      dimensions,
      dimSize,
      tempMem.data(),
      MAT_F_DONT_COPY_DATA
   );

   auto err = Mat_VarWrite(mat, matvar, matio_compression::MAT_COMPRESSION_NONE);

   // Clean up
   Mat_VarFree(matvar);

   return err;
}

// Saves a Planer vector to a matlab array
// Remember planer is vector of 1 channel and same size arrays
int MatIO::appendToMat(_mat_t *mat, const MtiPlanar<Ipp16uArray> &value, const std::string &varName)
{
	// Do nothing if empty
	if (value.size() == 0)
	{
		return 0;
	}

	auto width = value[0].getWidth();
	auto height = value[0].getHeight();
	auto pseudoChannels = value.size();

	auto outputSizePixels = width * height * pseudoChannels;
	vector<Ipp16u> tempMem(outputSizePixels);

	auto outputSizeByte = outputSizePixels * sizeof(Ipp16u);
	auto outputData = tempMem.data();

	for (auto i : range(value.size()))
	{
		auto &a = value[i];
		if ((a.getComponents() != 1) || (a.getHeight() != height) || (a.getWidth() != width))
		{
			MTIostringstream os;
			os << "Planer Arrays must have only one channel and same shape, " << "expected (" << MtiSize({ width, height, 1 }) << "), actual " << a.getSize() << "!";
			throw std::runtime_error(os.str());
		}

		auto x = a.toVector()[0];

		auto rawData = getMatLabArrayFromIppArray<Ipp16uArray, Ipp16u>(a);
		memcpy(outputData, rawData.data(), rawData.size() * sizeof(Ipp16u));
		outputData += rawData.size();
	}

	size_t dimSize[] = { size_t(height), size_t(width), size_t(pseudoChannels) };
	auto dimensions = 3;

	matio_classes class_type = MAT_C_UINT16;
	enum matio_types data_type = MAT_T_UINT16;

	auto matvar = Mat_VarCreate
	(
		varName.c_str(),
		class_type,
		data_type,
		dimensions,
		dimSize,
		tempMem.data(),
		MAT_F_DONT_COPY_DATA
	);

	auto err = Mat_VarWrite(mat, matvar, matio_compression::MAT_COMPRESSION_NONE);

	// Clean up
	Mat_VarFree(matvar);

	return err;
}
int MatIO::closeMatList(_mat_t * mat)
{
	return Mat_Close(mat);
}

_mat_t * MatIO::openMatList(const std::string & fileName)
{
	auto mat = Mat_CreateVer(fileName.c_str(), nullptr, MAT_FT_DEFAULT);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not open list: '") + fileName + "'");
	}

	return mat;
}

_mat_t * MatIO::appendMatList(const std::string & fileName)
{
	// See if file exists, if not just create it
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDWR);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not append: '") + fileName + "'");
	}

	return mat;
}

vector<std::string> MatIO::getVariableNames(const std::string & fileName)
{
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDWR);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not open to get name: '") + fileName + "'");
	}

	auto result = MatIO::getVariableNames(mat);
	Mat_Close(mat);
	return result;
}

#define MATIO_READVARAIBLE(VAR)\
VAR MatIO::read##VAR(const std::string & fileName, const std::string & varName)\
{\
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDWR);\
	if (mat == nullptr)\
	{\
		throw std::runtime_error(std::string("Could not open for reading: '") + fileName + "'");\
	}\
\
	auto result = MatIO::read##VAR(mat, varName);\
	Mat_Close(mat);\
\
	return result;\
}

MATIO_READVARAIBLE(Ipp8uArray)
MATIO_READVARAIBLE(Ipp16uArray)
MATIO_READVARAIBLE(Ipp32fArray)
MATIO_READVARAIBLE(Ipp32sArray)

#define MATIO_READARRAY(VAR)\
VAR MatIO::read##VAR(const std::string & fileName)\
{\
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDWR);\
	if (mat == nullptr)\
	{\
		throw std::runtime_error(std::string("Could not open for reading 2: '") + fileName + "'");\
	}\
\
	auto result = MatIO::read##VAR(mat);\
	Mat_Close(mat);\
\
	return result;\
}

MATIO_READARRAY(Ipp8uArray)
MATIO_READARRAY(Ipp16uArray)
MATIO_READARRAY(Ipp32fArray)
MATIO_READARRAY(Ipp32sArray)


//vector<matvar_t *> MatIO::getVariableInfo(_mat_t * mat)
//{
//	vector<matvar_t *> results;
//
//	Mat_Rewind(mat);
//
//	matvar_t *matvar = nullptr;
//	do
//	{
//		matvar = Mat_VarReadNextInfo(mat);
//		if (matvar == nullptr)
//		{
//			break;
//		}
//		results.push_back(matvar);
//	} while (true);
//
//	return results;
//}

template<typename A>
A _readIppArray(_mat_t * mat, const std::string & varName, matio_classes matClass)
{
	auto matvar = Mat_VarReadInfo(mat, varName.c_str());
	if (matvar == nullptr)
	{
		MTIostringstream os;
		os << "No such name '" << varName << "' in matlab file  '" << mat->filename << "'";
		throw std::runtime_error(os.str());
	}

	if (matvar->class_type != matClass)
	{
		MTIostringstream os;
		os << "Variable '" << varName << "' in matlab file  '" << mat->filename << "' is not of correct type";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	if ((matvar->rank == 1) || (matvar->rank > 3))
	{
		MTIostringstream os;
		os << "Variable '" << varName << "' in matlab file  '" << mat->filename << "' is not an IppArray";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto channels = matvar->rank == 2 ? 1 : int(matvar->dims[2]);
	if ((channels < 1) || (channels > 3))
	{
		MTIostringstream os;
		os << "Variable '" << varName << "' in matlab file  '" << mat->filename << "' has incorrect dimensions";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto err = Mat_VarReadDataAll(mat, matvar);
	if (err != 0)
	{
		MTIostringstream os;
		os << "Could not read '" << varName << "' in matlab file  '" << mat->filename << "'";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto height = int(matvar->dims[0]);
	auto width = int(matvar->dims[1]);
	auto depth = matvar->rank < 3 ? 1 : int(matvar->dims[2]);

	A result;
	MtiPlanar<A> planes;
	auto data = static_cast<decltype(result.data())>(matvar->data);
	auto planeSizeInPixels = matvar->nbytes / (depth * matvar->data_size);
	assert(planeSizeInPixels == (matvar->dims[0] * matvar->dims[1]));
	for (auto c = 0; c < channels; c++)
	{
		// NOTE: we must transpose, so width/height are reversed here!
		A P({ height, width, 1 }, data);
		planes.push_back(P);
		data += planeSizeInPixels;
	}

	if (channels == 1)
	{
		result = planes[0];
		result = result.transpose();
	}
	else
	{
		result.copyFromPlanar(planes);
		result = result.transpose();
	}

	Mat_VarFree(matvar);

	return result;
}

Ipp8uArray MatIO::readIpp8uArray(_mat_t *mat, const std::string &varName)
{
	return _readIppArray<Ipp8uArray>(mat, varName, MAT_C_UINT8);
}

bool MatIO::readIppArray(_mat_t *mat, Ipp8uArray &ippArray, const std::string &varName)
{
	ippArray = readIpp8uArray(mat, varName);
	return !ippArray.isEmpty();
}

bool MatIO::readIppArray(const std::string & fileName, Ipp8uArray & ippArray, const std::string & varName)
{
	ippArray = readIpp8uArray(fileName, varName);
	return !ippArray.isEmpty();
}

Ipp16uArray MatIO::readIpp16uArray(_mat_t *mat, const std::string &varName)
{
	return _readIppArray<Ipp16uArray>(mat, varName, MAT_C_UINT16);
}

bool MatIO::readIppArray(_mat_t *mat, Ipp16uArray &ippArray, const std::string &varName)
{
	ippArray = readIpp16uArray(mat, varName);
	return !ippArray.isEmpty();
}

bool MatIO::readIppArray(const std::string & fileName, Ipp16uArray & ippArray, const std::string & varName)
{
	ippArray = readIpp16uArray(fileName, varName);
	return !ippArray.isEmpty();
}

Ipp32fArray MatIO::readIpp32fArray(_mat_t *mat, const std::string &varName)
{
	return _readIppArray<Ipp32fArray>(mat, varName, MAT_C_SINGLE);
}

Ipp32sArray MatIO::readIpp32sArray(_mat_t *mat, const std::string &varName)
{
	return _readIppArray<Ipp32sArray>(mat, varName, MAT_C_INT32);
}

bool MatIO::readIppArray(_mat_t *mat, Ipp32fArray &ippArray, const std::string &varName)
{
	ippArray = readIpp32fArray(mat, varName);
	return !ippArray.isEmpty();
}


bool MatIO::readIppArray(const std::string & fileName, Ipp32fArray & ippArray, const std::string & varName)
{
	ippArray = readIpp32fArray(fileName, varName);
	return !ippArray.isEmpty();
}

bool MatIO::readIppArray(_mat_t* mat, Ipp32sArray &ippArray, const std::string& varName)
{
	ippArray = readIpp32sArray(mat, varName);
	return !ippArray.isEmpty();
}

bool MatIO::readIppArray(const std::string & fileName, Ipp32sArray & ippArray, const std::string & varName)
{
	ippArray = readIpp32sArray(fileName, varName);
	return !ippArray.isEmpty();
}

Ipp32fArray MatIO::readIpp32fArrayFromDouble(const std::string & fileName, const std::string & varName)
{
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDWR);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not open for reading Ipp32f: '") + fileName + "'");
	}

	auto result = MatIO::readIpp32fArrayFromDouble(mat, varName);
	Mat_Close(mat);
	return result;
}

Ipp32fArray MatIO::readIpp32fArrayFromDouble(const std::string & fileName)
{
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDWR);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not open: '") + fileName + "'");
	}

	auto result = MatIO::readIpp32fArrayFromDouble(mat);
	Mat_Close(mat);
	return result;
}


// This checks if the var is an IppArray, that is defined by having 
// at most 4 channels (RGB A).  If so, return 0.  If an array with more than channels, return
// the number of channels.
int doesMatContainIppArray(_mat_t *mat, const std::string &varName, matio_classes matClass)
{
	auto matvar = Mat_VarReadInfo(mat, varName.c_str());
	if (matvar == nullptr)
	{
		return -1;
	}

	if (matvar->class_type != matClass)
	{
		Mat_VarFree(matvar);
		return -2;
	}

	if ((matvar->rank == 1) || (matvar->rank > 3))
	{
		Mat_VarFree(matvar);
		return -3;
	}

	auto channels = matvar->rank == 2 ? 1 : int(matvar->dims[2]);
	if ((channels < 1) || (channels > 4))
	{
		Mat_VarFree(matvar);
		return channels;
	}

	// This may be incorrect, do we need to check data type?
	Mat_VarFree(matvar);
	return 0;
}

int doesMatContainIppPlaner(_mat_t *mat, const string &name, matio_classes matClass)
{
	// Really bad programming, because it depends upon doeMatContainIppArray failing in a certain way
	auto result = doesMatContainIppArray(mat, name, matClass);
	if ((result == 0) || (result > 4))
	{
		return 0;
	}

	return result;
}

///////////////////////////// PLANER READS ///////////////////////////////////////

template<typename T>
MtiPlanar<T> _readIppPlaner(_mat_t * mat, const std::string & varName, matio_classes matClass)
{
	auto matvar = Mat_VarReadInfo(mat, varName.c_str());
	if (matvar == nullptr)
	{
		MTIostringstream os;
		os << "No such name '" << varName << "' in matlab file  '" << mat->filename << "'";
		throw std::runtime_error(os.str());
	}

	if (matvar->class_type != matClass)
	{
		MTIostringstream os;
		os << "Variable '" << varName << "' in matlab file  '" << mat->filename << "' is not of correct type";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	if (matvar->rank != 3)
	{
		MTIostringstream os;
		os << "Variable '" << varName << "' in matlab file  '" << mat->filename << "' is not an MtiPlaner";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto err = Mat_VarReadDataAll(mat, matvar);
	if (err != 0)
	{
		MTIostringstream os;
		os << "Could not read '" << varName << "' in matlab file  '" << mat->filename << "'";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto height = int(matvar->dims[0]);
	auto width = int(matvar->dims[1]);
	auto rank = int(matvar->dims[2]);

	T result;

	MtiPlanar<T> planes;
	auto data = (decltype(result.data()))matvar->data;
	auto planeSizeInPixels = matvar->nbytes / (rank * matvar->data_size);
	assert(planeSizeInPixels == (matvar->dims[0] * matvar->dims[1]));

	// Convert to double to single, an error T is not single
	if ((matClass == MAT_C_DOUBLE) && (typeid(T) != typeid(Ipp32fArray)))
	{
		Mat_VarFree(matvar);
		throw std::runtime_error("Unsupported type: can only convert double matlab to single planer");
	}

	// In case we have to convert 64f to 32f
	Ipp32f *tmpBuffer = nullptr;
	if (matClass == MAT_C_DOUBLE)
	{
		auto n = matvar->nbytes / matvar->data_size;
		tmpBuffer = ippsMalloc_32f(n);

		// Memory leak on throw but should never happen
		IppThrowOnError(ippsConvert_64f32f((Ipp64f *)data, tmpBuffer, n));
		data = tmpBuffer;
	}

	for (auto c = 0; c < rank; c++)
	{
		T plane({ height, width, 1 }, data);
		planes.emplace_back(plane.transpose());
		data += planeSizeInPixels;
	}

	Mat_VarFree(matvar);
	ippsFree(tmpBuffer);

	return planes;
}

MtiPlanar<Ipp32fArray> MatIO::readIpp32fPlanar(_mat_t * mat, const std::string & varName)
{
	auto matvar = Mat_VarReadInfo(mat, varName.c_str());
	auto classType = matvar->class_type;
	Mat_VarFree(matvar);

	if (classType == MAT_C_SINGLE)
	{
		return _readIppPlaner<Ipp32fArray>(mat, varName, MAT_C_SINGLE);
	}

	return _readIppPlaner<Ipp32fArray>(mat, varName, MAT_C_DOUBLE);
}

MtiPlanar<Ipp32fArray> MatIO::readIpp32fPlanar(const std::string &fileName, const std::string &varName)
{
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDONLY);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not open: '") + fileName + "'");
	}

	auto result = MatIO::readIpp32fPlanar(mat, varName);
	Mat_Close(mat);
	return result;
}

MtiPlanar<Ipp32fArray> MatIO::readIpp32fPlanar(const std::string & fileName)
{
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDONLY);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not open: '") + fileName + "'");
	}

	auto result = readIpp32fPlanar(mat);
	Mat_Close(mat);

	return result;
}

// helper function to read a double or int as int
int readIntFromField(matvar_t *matvar, const string &fieldName)
{
	auto fieldVar = Mat_VarGetStructField(matvar, (void *)fieldName.data(), MAT_BY_NAME, 0);
	if (fieldVar == nullptr)
	{
		MTIostringstream os;
		os << "No such field name '" << "x" << "' in matlab variable  '" << matvar->name << "'";
		throw std::runtime_error(os.str());
	}

	switch (fieldVar->data_type)
	{
	case MAT_T_DOUBLE:
		return int(*static_cast<double *>(fieldVar->data));

	case MAT_T_INT32:
		return int(*static_cast<int32_t *>(fieldVar->data));

	default:
		throw std::runtime_error("Structure field of not compatible data type");
	}
}

MtiRect MatIO::readMtiRect(const std::string& fileName, const std::string& varName)
{
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDONLY);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not open: '") + fileName + "'");
	}

	auto matvar = Mat_VarReadInfo(mat, varName.c_str());
	if (matvar == nullptr)
	{
		MTIostringstream os;
		os << "No such name '" << varName << "' in matlab file  '" << mat->filename << "'";
		throw std::runtime_error(os.str());
	}

	auto err = Mat_VarReadDataAll(mat, matvar);
	if (err != 0)
	{
		MTIostringstream os;
		os << "Could not read '" << varName << "' in matlab file  '" << mat->filename << "'";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	MtiRect result;
	result.x = readIntFromField(matvar, "x");
	result.y = readIntFromField(matvar, "y");
	result.width = readIntFromField(matvar, "width");
	result.height = readIntFromField(matvar, "height");

	return result;
}

vector<double> MatIO::readVectorIpp64f(_mat_t *mat, const std::string& varName)
{
	auto matvar = Mat_VarReadInfo(mat, varName.c_str());
	if (matvar == nullptr)
	{
		MTIostringstream os;
		os << "No such name '" << varName << "' in matlab file  '" << mat->filename << "'";
		throw std::runtime_error(os.str());
	}

	if (matvar->class_type != MAT_C_DOUBLE)
	{
		MTIostringstream os;
		os << "Variable '" << varName << "' in matlab file  '" << mat->filename << "' is not of correct type";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto err = Mat_VarReadDataAll(mat, matvar);
	if (err != 0)
	{
		MTIostringstream os;
		os << "Could not read '" << varName << "' in matlab file  '" << mat->filename << "'";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto height = int(matvar->dims[0]);
	auto width = int(matvar->dims[1]);
	auto depth = matvar->rank == 2 ? 1 : int(matvar->dims[2]);

	auto sizeInDoubles = height * width * depth;
	vector<double> result(sizeInDoubles);
	auto data = static_cast<Ipp64f *>(matvar->data);
	auto planeSizeInPixels = matvar->nbytes / (depth * matvar->data_size);
	assert(planeSizeInPixels == (matvar->dims[0] * matvar->dims[1]));
	memcpy_s(result.data(), result.size() * sizeof(Ipp64f), matvar->data, matvar->nbytes);

	// Now if width or height is 1, we are done
	if ((height > 1) && (width > 1))
	{
		// data exists in matvar->data		
		// copy to result.data()
		auto destData = result.data();
		
		// Ugly, transpose each plane
		for (auto c = 0; c < depth; c++)
		{
			// NOTE: matlab is row contiguous
			for (auto h = 0; h < width; h++)
			{
				for (auto r = 0; r < height; r++)
				{
					destData[r*width + h] = data[h*height + r];
				}
			}

			data += planeSizeInPixels;
			destData += planeSizeInPixels;
		}
	}
	
	Mat_VarFree(matvar);
	return result;
}

vector<double> MatIO::readVectorIpp64f(const std::string& fileName, const std::string& varName)
{
	auto mat = Mat_Open(fileName.c_str(), MAT_ACC_RDWR);
	if (mat == nullptr)
	{
		throw std::runtime_error(std::string("Could not open for reading Ipp32f: '") + fileName + "'");
	}

	auto result = readVectorIpp64f(mat, varName);
	Mat_Close(mat);
	return result;
}

MtiPlanar<Ipp32fArray> MatIO::readIpp32fPlanar(_mat_t *mat)
{
	auto names = MatIO::getVariableNames(mat);
	for (auto &name : names)
	{
		if (doesMatContainIppPlaner(mat, name, MAT_C_SINGLE) == 0)
		{
			return readIpp32fPlanar(mat, name);
		}

		if (doesMatContainIppPlaner(mat, name, MAT_C_DOUBLE) == 0)
		{
			return readIpp32fPlanar(mat, name);
		}
	}

	MTIostringstream os;
	os << "No single or double array in matlab file'" << mat->filename << "'";
	throw std::runtime_error(os.str());
}

Ipp8uArray MatIO::readIpp8uArray(_mat_t *mat)
{
	auto names = MatIO::getVariableNames(mat);
	for (auto &name : names)
	{
		if (doesMatContainIppArray(mat, name, MAT_C_UINT8) == 0)
		{
			return _readIppArray<Ipp8uArray>(mat, name, MAT_C_UINT8);
		}
	}

	return Ipp8uArray();
}

Ipp16uArray MatIO::readIpp16uArray(_mat_t *mat)
{
	auto names = MatIO::getVariableNames(mat);
	for (auto &name : names)
	{
		if (doesMatContainIppArray(mat, name, MAT_C_UINT16) == 0)
		{
			return _readIppArray<Ipp16uArray>(mat, name, MAT_C_UINT16);
		}
	}

	return Ipp16uArray();
}

Ipp32fArray MatIO::readIpp32fArray(_mat_t *mat)
{
	auto names = MatIO::getVariableNames(mat);
	for (auto &name : names)
	{
		if (doesMatContainIppArray(mat, name, MAT_C_SINGLE) == 0)
		{
			return _readIppArray<Ipp32fArray>(mat, name, MAT_C_SINGLE);
		}

		if (doesMatContainIppArray(mat, name, MAT_C_DOUBLE) == 0)
		{
			return readIpp32fArrayFromDouble(mat, name);
		}
	}

	return Ipp32fArray();
}

Ipp32sArray MatIO::readIpp32sArray(_mat_t* mat)
{
	auto names = MatIO::getVariableNames(mat);
	for (auto &name : names)
	{
		if (doesMatContainIppArray(mat, name, MAT_C_INT32) == 0)
		{
			return _readIppArray<Ipp32sArray>(mat, name, MAT_C_INT32);
		}
	}

	return Ipp32sArray();
}

Ipp32fArray MatIO::readIpp32fArrayFromDouble(_mat_t * mat, const std::string & varName)
{
	auto matvar = Mat_VarReadInfo(mat, varName.c_str());
	if (matvar == nullptr)
	{
		MTIostringstream os;
		os << "No such name '" << varName << "' in matlab file  '" << mat->filename << "'";
		throw std::runtime_error(os.str());
	}

	if (matvar->class_type != MAT_C_DOUBLE)
	{
		MTIostringstream os;
		os << "Variable '" << varName << "' in matlab file  '" << mat->filename << "' is not of correct type";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	if ((matvar->rank == 1) || (matvar->rank > 3))
	{
		MTIostringstream os;
		os << "Variable '" << varName << "' in matlab file  '" << mat->filename << "' is not an IppArray";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto channels = matvar->rank == 2 ? 1 : int(matvar->dims[2]);
	if ((channels < 1) || (channels > 3))
	{
		MTIostringstream os;
		os << "Variable '" << varName << "' in matlab file  '" << mat->filename << "' has incorrect dimensions";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto err = Mat_VarReadDataAll(mat, matvar);
	if (err != 0)
	{
		MTIostringstream os;
		os << "Could not read '" << varName << "' in matlab file  '" << mat->filename << "'";
		Mat_VarFree(matvar);
		throw std::runtime_error(os.str());
	}

	auto height = int(matvar->dims[0]);
	auto width = int(matvar->dims[1]);
	auto depth = matvar->rank < 3 ? 1 : int(matvar->dims[2]) ;

	MtiPlanar<Ipp32fArray> planes;
	auto data = static_cast<Ipp64f *>(matvar->data);
	auto planeSizeInPixels = matvar->nbytes / (depth * matvar->data_size);
	for (auto c = 0; c < channels; c++)
	{
		// NOTE: we must transpose, so widht/height are reversed here!
		Ipp32fArray P({ height, width, 1 });
		planes.emplace_back(P);

		// Convert here
		for (auto p : planes[c].rowIterator())
		{
			for (auto col = 0; col < planes[c].getWidth(); col++)
			{
				*p++ = float(*data++);
			}
		}
	}

	Ipp32fArray result;
	if (channels == 1)
	{
		result = planes[0];
		result = result.transpose();
	}
	else
	{
		result.copyFromPlanar(planes);
		result = result.transpose();
	}

	Mat_VarFree(matvar);

	return result;
}

Ipp32fArray MatIO::readIpp32fArrayFromDouble(_mat_t *mat)
{
	auto names = MatIO::getVariableNames(mat);
	for (auto &name : names)
	{
		if (doesMatContainIppArray(mat, name, MAT_C_DOUBLE) == 0)
		{
			return readIpp32fArrayFromDouble(mat, name);
		}
	}

	return Ipp32fArray();
}
