#pragma once
#ifndef IPASTRIPESTREAMH
#define IPASTRIPESTREAMH

#include "IppArray.h"
#include "IpaStreamDataItem.h"
#include <thread>
#include <vector>
#include <functional>
#include <stack>
#include <cassert>

// This is the processing streams for the IppArrays.
// Look at IppStripStreamTest.cpp for examples.
//
//  The basic idea is an IppArray broken apart into a JOB (e.g. 16) stripes 
//  each THREADS (e.g. 8) of these jobs are run simultaneously.
//
//  For example, suppose one wished to convert a Ipp16uArray in16u to a Ipp32fArray out32f
//
//  In line this would be
//     Ipp32fArray out32f;   // A null (unspecified) array
//     out32f <<= in16u;     // do conversion.
//
//  To strip this in the most trivial way, one would write
//		Ipp32fArray out32f(in16u.getSize());        // Striped arrays must be preallocated
//      IpaStripeStream iss;
//      iss << in16u.getSize();                      // Push the area we want to work on
//      iss << [&](const MtiRect &roi)              // Create lambda expression, which because of the & 
//             {                                     // allows access to all variables in the class and local varaiables
//                 out32f(roi) <<= in16u(roi);       // Note: Exctly the same as inline but on an ROI
//             };                                    // Don't forget the ;           
//
//      iss.stripe();                                // Execute the lambda with default number of threads and jobs.
//													 // Control returns AFTER all strips are processed
//                                                   // These usually exatly the the number of processors in hardware
//                                                   // In the future, hints will be used to cause otimal choice
//
//   In order to override the number of jobs
//       iss.stripe(24);                             // Threads default to number of processors available
//   
//   To specify the jobs and threads
//        iss.stripe(24, 8);
//
//    Note: the number of jobs should be set to a integral multiple of threads if each stripe has the same work to be done.
//
//    Another way, note rather than an ROI, send in a striped array.
//       iss << in16u;
//       iss << [&out32f](Ipp16uArray &in)
//             {                                     // only allow access ot out32f
//                 out32f(in.getRoi()) <<= in;       // Note: out32 is FULL array, in is striped (sub) array
//             };                                    // Don't forget the ;           
//
//       iss.stripe();
//
//    Finally if both arrays are of the same type
//       iss << in16u << out16u;
//       iss << [&](Ipp16uArray &in, Ipp16uArray &out)
//             {                                     
//                 out <<= in + 10;                  // Note: in and out are striped arrays
//             };   
// 
//    Below is better because the above allocates a temp array before move
//       iss << [&](Ipp16uArray &in, Ipp16uArray &out)
//             {         
//                 auto outIter = out.begin();
//                 for (auto f : in)
//                 {
//                    *outIter++ = f + 10;            // Works ONLY because ROIs are same size and shape
//                                                    // NOTE: this is NOT the same as above because in+10 saturates and f+10 rollsover
//                 }
//             };   
//
//    Even better, but requires two access of memory
//       iss << [](Ipp16uArray &in, Ipp16uArray &out)
//             {         
//                 out <<= in;                         // Do a copy
//                 out += 10;                          // Add in place
//             };   
//
//    Final note, if has a member function, private or public, one can just call it in the lambda
//    E.g.
//    private:
//       void process(Ipp32fArray &in);       // member function
//
//   In the code
//
//       iss << [&](Ipp32fArray &in)
//             {                                     
//                 process(in);                  // Note: process must work on stripes
//             };         
//
//    Note:  For inline running, one can use iss.run() which executes under calling thread and one big ROI.
//
//   currently supported lambda expressions, any capture expression
//   
//       [](int jobNumber)						 Number of calls to make defined by number of stripes
//												 E.g. iss.stripe(1000); will call 0 to 999 
//                                               WARNING run calls it exactly ONE time 
//
//       [](const MtiRect &roi)				     Either MtiSize or MtiRect 
//       [](const MtiRect &roi, int jobNumber)   Either MtiSize or MtiRect 
//
//	     [](IppArray &array)					  IppArray
//	     [](IppArray &array, jobNumber);	      IppArray

//  NOTE: Do to a Embarcadero compiler bug, the lambda expressions give an ambiguous error and 
//  need to be cast to the actual type, to aid this the following things are defined
//  NOTE: efu stands for Embarcadero F*** Up
#define efu_job(T) (std::function<void(int)>)T
#define efu_roi(T) (std::function<void(const MtiRect &)>)T
#define efu_roi_job(T) (std::function<void(const MtiRect &, int)>)T
#define efu_16u(T) (std::function<void(Ipp16uArray &)>)T
#define efu_32f(T) (std::function<void(Ipp32fArray &)>)T
#define efu_32s(T) (std::function<void(Ipp32sArray &)>)T

// The CallType will go away in the future DO NOT USE!!!!
// TODO: split into just call arg types
enum class CallType
{
	unary8u,
	unary16u,
	unary32f,
	unary32s,
	unaryRoi,
	unaryJob,
	binary8u8u,
	binary16u16u,
	binary32f32f,
};

class MTI_IPPARRAY_API functionalItem
{
public:
	virtual void execute(int jobNumber) {}
	virtual void execute(Ipp8uArray rhs) {}
	virtual void execute(Ipp16uArray rhs) {}
	virtual void execute(Ipp32fArray rhs) {}
	virtual void execute(Ipp32sArray rhs) {}
	virtual void execute(Ipp8uArray rhs, int jobNumber) {}
	virtual void execute(Ipp16uArray rhs, int jobNumber) {}
	virtual void execute(Ipp32fArray rhs, int jobNumber) {}
	virtual void execute(Ipp32sArray rhs, int jobNumber) {}

	virtual void execute(Ipp8uArray in, Ipp8uArray out) {}
	virtual void execute(Ipp16uArray in, Ipp16uArray out) {}
	virtual void execute(Ipp32fArray in, Ipp32fArray out) {}

	virtual void execute(MtiRect &roi) {}
	virtual void execute(MtiRect &roi, int jobNumber) {}

	[[nodiscard]] CallType getCallType() const { return _callType; }
	[[nodiscard]] IppDataType getDataType() const { return _dataType; }

	[[nodiscard]] bool isUnary() const;
	[[nodiscard]] bool isBinary() const;
	[[nodiscard]] bool isRoi() const;
	[[nodiscard]] bool isNoArg() const;
	[[nodiscard]] bool hasJobArg() const { return _hasJobArg; }
protected:
	CallType _callType;
	IppDataType _dataType;
	bool _hasJobArg = false;
};

struct stripeParams
{
	functionalItem *f = nullptr;
	IpaStreamDataItem data0;
	IpaStreamDataItem data1;
	MtiRect roi;
};

// This extends the IppDataTypes
#define ippiRoi IppDataType(IppDataType::ippUndef -1)

template<typename T>
class functionalItemClass : public functionalItem
{
public:
	functionalItemClass(std::function<void(T &image)> func, CallType callType, IppDataType dataType)
	{
		_funcU = func;
		_callType = callType;
		_dataType = dataType;
	}

	functionalItemClass(std::function<void(T &image, int jobNumber)> func, CallType callType, IppDataType dataType)
	{
		_funcUJ = func;
		_callType = callType;
		_dataType = dataType;
		_hasJobArg = true;
	}

	functionalItemClass(std::function<void(T &in, T &out)> func, CallType callType, IppDataType dataType)
	{
		_funcB = func;
		_callType = callType;
		_dataType = dataType;
		_hasJobArg = false;
	}

	void execute(T rhs)
	{
		_funcU(rhs);
	}

	void execute(T rhs, int jobNumber)
	{
		_funcUJ(rhs, jobNumber);
	}

	void execute(T in, T out)
	{
		_funcB(in, out);
	}

private:
	std::function<void(T &)> _funcU = nullptr;
	std::function<void(T &, int)> _funcUJ = nullptr;
	std::function<void(T &, T &)> _funcB = nullptr;
};

class functionalJobClass : public functionalItem
{
public:
	functionalJobClass(std::function<void(int jobNumber)> func)
	{
		_funcJ = func;
		_callType = CallType::unaryJob;
		_dataType = ipp32s;
		_hasJobArg = true;
	}

	void execute(int jobNumber)
	{
		_funcJ(jobNumber);
	}

private:
	std::function<void(int)> _funcJ = nullptr;
};

class functionalItemRoiClass : public functionalItem
{
public:
	functionalItemRoiClass(std::function<void(MtiRect &)> func)
	{
		_funcRoi = func;
		_callType = CallType::unaryRoi;
		_dataType = ippiRoi;
		_hasJobArg = false;
	}

	functionalItemRoiClass(std::function<void(MtiRect &, int)> func, int dummy)
	{
		_funcRoi2 = func;
		_callType = CallType::unaryRoi;
		_dataType = ippiRoi;
		_hasJobArg = true;
	}

	void execute(MtiRect &roi)
	{
		_funcRoi(roi);
	}

	void execute(MtiRect &roi, int jobNumber)
	{
		_funcRoi2(roi, jobNumber);
	}

private:
	std::function<void(MtiRect &roi)> _funcRoi = nullptr;
	std::function<void(MtiRect &roi, int jobNumber)> _funcRoi2 = nullptr;
};

class MTI_IPPARRAY_API IpaStripeStream
{
public:
	IpaStripeStream();
	~IpaStripeStream();

	// Data pushes
	//template<typename T>
	//IpaStripeStream &operator<<(T& rhs)
	//{
	//	_dataStack.emplace_back(rhs);
	//	return *this;
	//}

	IpaStripeStream &operator << (const Ipp8uArray& rhs);
	IpaStripeStream &operator << (const Ipp16uArray& rhs);
	IpaStripeStream &operator << (const Ipp32fArray& rhs);
	IpaStripeStream &operator << (const Ipp32sArray& rhs);

	IpaStripeStream &operator << (Ipp64f value);
	IpaStripeStream &operator << (Ipp32f value);
	IpaStripeStream &operator << (size_t value);
	IpaStripeStream &operator << (Ipp32s value);
	IpaStripeStream &operator << (Ipp16u value);
	IpaStripeStream &operator << (Ipp8u value);

	IpaStripeStream &operator << (const MtiRect &roi);
	IpaStripeStream &operator << (const MtiSize &size);

	// Function pushes

	// This is not complete, we need to figure out how to use variadic templates
	// The current implementation is VERY ad hoc
	IpaStripeStream &operator << (std::function<void(Ipp8uArray&image)> func);
	IpaStripeStream &operator << (std::function<void(Ipp16uArray&image)> func);
	IpaStripeStream &operator << (std::function<void(Ipp32fArray&image)> func);
	IpaStripeStream &operator << (std::function<void(Ipp32sArray&image)> func);

	IpaStripeStream &operator << (std::function<void(Ipp8uArray&image, int)> func);
	IpaStripeStream &operator << (std::function<void(Ipp16uArray&image, int)> func);
	IpaStripeStream &operator << (std::function<void(Ipp32fArray&image, int)> func);
	IpaStripeStream &operator << (std::function<void(Ipp32sArray&image, int)> func);

	IpaStripeStream &operator << (std::function<void(Ipp8uArray&in, Ipp8uArray &out)> func);
	IpaStripeStream &operator << (std::function<void(Ipp16uArray&in, Ipp16uArray &out)> func);
	IpaStripeStream &operator << (std::function<void(Ipp32fArray&in, Ipp32fArray &out)> func);

	IpaStripeStream & operator<<(std::function<void(int jobNumber)> func);
	IpaStripeStream & operator<<(std::function<void(const MtiRect &stripeRoi)> func);
	IpaStripeStream & operator<<(std::function<void(const MtiRect &stripeRoi, int jobNumber)> func);

	void throwIfNoDataMatch(IppDataType dt);

	IpaStripeStream duplicate(const MtiRect &rect);
	IpaStripeStream run();
	IpaStripeStream stripe(int stripes = 0, int threads = 0);
	void clear();

	[[nodiscard]] int getMaxStripes() const { return _maxStripes; }

private:
	IpaStripeStream runUnary();
	IpaStripeStream runUnaryJob();
	IpaStripeStream runRoi();
	IpaStripeStream runBinary();

	std::vector<functionalItem *> _functionStack;
	std::vector<IpaStreamDataItem> _dataStack;

   int _maxStripes = 64;
};
#endif

