#include "MtiRuntimeError.h"
#include <sstream>

MtiRuntimeError::MtiRuntimeError(const std::string &message) : std::runtime_error(message)
{
	// This is to make Braca happy
	_errorMessage = message;
}
