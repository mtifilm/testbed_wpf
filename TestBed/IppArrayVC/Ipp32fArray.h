#pragma once
#ifndef Ipp32fArrayH
#define Ipp32fArrayH

#include "IppArray.h"

class Ipp8uArray;
class Ipp16uArray;
class Ipp32sArray;

class MTI_IPPARRAY_API Ipp32fArray : public IppArray<Ipp32f>
{

public:
	Ipp32fArray() = default;

	Ipp32fArray(const MtiSize &mtiSize) : IppArray <Ipp32f>(mtiSize) {}
	Ipp32fArray(const MtiSize &mtiSize, Ipp32f *source, bool copyData = false) : IppArray(mtiSize, source, copyData) {}

protected:
	Ipp32fArray(const IppArray<Ipp32f> & rhs, const IppiRect &roi) : IppArray(rhs, roi) {}

public:

	/// Specific functions
	void set(const vector<Ipp32f> &value);
	void normalDistribution(double mean, double stddev);

	// This doesn't belong here
	static Ipp32fArray normalPdf(int n, double mean, double stddev);
	static void normalPdf(int n, double mean, double stddev, Ipp32fArray &pdfArray);

	void uniformDistribution(float low, float high);
	[[nodiscard]] vector<Ipp64f> sum() const;
	[[nodiscard]] vector<Ipp32f> median(const IppiRect &roi) const;
	[[nodiscard]] vector<Ipp32f> median() const;

	// This returns the medium of all channels lumped together
	// It uses the quick medium algorithm.
	[[nodiscard]] Ipp32f medianQuick1D() const { return medianQuick1D(*this); }

	void copyFromPod(Ipp32f *source, int size);

	[[nodiscard]] Ipp32fArray duplicate(const IppiRect &roi) const
	{
		Ipp32fArray result;
		result <<= (*this)(roi);
		return result;
	}

	[[nodiscard]] Ipp32fArray duplicate() const
	{
		Ipp32fArray result;
		result <<= *this;
		return result;
	}

	// reshape the first TWO components of the array
	// WRONG DO NOT USE!!! API NOT DEFINED  MEMORY MANAGEMENT WRONG
	[[nodiscard]] Ipp32fArray reshape(const MtiSize &size) const
	{
		if (area() != size.area())
		{
			THROW_MTI_RUNTIME_ERROR("Number of elements must match")
		}

		return Ipp32fArray({ size, getComponents() }, data());
	}

	// This just mimics the Matlab function
	[[nodiscard]] Ipp32fArray fliplr() const { return mirror(IppiAxis::ippAxsVertical); }
	[[nodiscard]] Ipp32fArray mirror(IppiAxis flip) const;

	// Conversion operators
	Ipp32fArray &operator <<=(const IppArray<Ipp8u>& rhs) { convertFrom(rhs); return *this; }
	Ipp32fArray &operator <<=(const IppArray<Ipp16u>& rhs) { convertFrom(rhs); return *this; }
	Ipp32fArray &operator <<=(const IppArray<Ipp32s>& rhs) { convertFrom(rhs); return *this; }
	Ipp32fArray &operator <<=(const IppArray<Ipp32f>& rhs) { convertFrom(rhs); return *this; }
	void convertFrom(const std::vector<Ipp64f>& rhs, const MtiSize rhsSize);
	Ipp32fArray &operator <<=(const std::vector<Ipp64f> &rhs);

	// Arithmetic, add an array and an array
	Ipp32fArray operator +(const IppArray<Ipp32f> &rhs) const;
	Ipp32fArray operator -(const IppArray<Ipp32f> &rhs) const;
	Ipp32fArray operator *(const IppArray<Ipp32f> &rhs) const;
	Ipp32fArray operator /(const IppArray<Ipp32f> &rhs) const;

	// Arithmetic, add an array and a scaler 
	Ipp32fArray operator +(const Ipp32f scalar) const;
	Ipp32fArray operator -(const Ipp32f scalar) const;
	Ipp32fArray operator *(const Ipp32f scalar) const;
	Ipp32fArray operator /(const Ipp32f scalar) const;

	// Arithmetic, add an array in place 
	Ipp32fArray &operator +=(IppArray<Ipp32f> &rhs);
	Ipp32fArray &operator -=(IppArray<Ipp32f> &rhs);
	Ipp32fArray &operator *=(IppArray<Ipp32f> &rhs);
	Ipp32fArray &operator /=(IppArray<Ipp32f> &rhs);

	// Arithmetic, add an scaler in place 
	Ipp32fArray &operator +=(const Ipp32f scalar);
	Ipp32fArray &operator -=(const Ipp32f scalar);
	Ipp32fArray &operator *=(const Ipp32f scalar);
	Ipp32fArray &operator /=(const Ipp32f scalars);

	Ipp32fArray operator()(const MtiRect &roi) const { return Ipp32fArray(*this, roi); }
	Ipp32fArray operator()(const MtiRect &probeRoi, MtiSelectMode copyType, bool utilizeExtrinsicData = false) const;

	// Warning, this takes the 2D indices from one plane and returns a IppArray
	// of same size as indices but depth of calling array
   // Thus a 3D image of a vector returns r0, b0, g0, r1, b1, g1, ....
	[[nodiscard]] Ipp32fArray fromPixelIndices(const IppArray<Ipp32s> &indices) const;
	[[nodiscard]] std::vector<Ipp32f> fromPixelIndices(const vector<Ipp32s> &indices) const;

	// Absolute ROI 
	[[nodiscard]] Ipp32fArray selectFromBase(const IppiRect &roi) const { Ipp32fArray result = *this; result.resetRoi(roi); return result; }

	[[nodiscard]] Ipp32fArray sharpen() const;
	[[nodiscard]] Ipp32fArray transpose() const;
	[[nodiscard]] Ipp32fArray toGray() const;
	Ipp32fArray toGray(const float weights[]) const;

	// Ad hoc calls, need to be somewhere else
	void Y2RGB(const Ipp32fArray & src);
	void exportRgbFromNormalizedYuv(unsigned short *p, int dataMax);
	[[nodiscard]] MtiPlanar<Ipp32fArray> toPlanar() const;
	void copyFromPlanar(const MtiPlanar<Ipp32fArray> &planes);

	[[nodiscard]] vector<Ipp64f> L1Norm() const;
	[[nodiscard]] vector<Ipp64f> L2Norm() const;
	[[nodiscard]] vector<Ipp64f> mean() const;
	[[nodiscard]] vector<Ipp64f> dotProduct(const IppArray<Ipp32f> &rhs) const;

	void L1NormDiff(const IppArray<Ipp32f> &rhs, Ipp64f result[]) const;
	void L2NormDiff(const IppArray<Ipp32f> &rhs, Ipp64f result[]) const;

	void sqrtInPlace();
	Ipp32fArray sqrt();

	void importNormalizedYuvFromRgb(unsigned short *p, int rows, int cols, int dataMax);
	void importOneNormalizedChannelFromRgb(unsigned short *p, int rows, int cols, int whichChannel, int dataMax);

	[[nodiscard]] Ipp32fArray resize(IppiSize newSize) const;
	[[nodiscard]] Ipp32fArray resizeAntiAliasing(IppiSize newSize) const;

	// Returns an array of the entire data 
	[[nodiscard]] Ipp32fArray baseArray() const
	{
		auto result = *this;
		result.resetRoi({ { 0,0 }, getBaseSize() });
		return result;
	}

	[[nodiscard]] Ipp32fArray crossCorrNorm(const IppArray<Ipp32f> &smallImage, IppiROIShape roiShape = IppiROIShape::ippiROIFull) const;
	void maxAndIndex(Ipp32f *maxValues, MtiPoint *indices) const;
	[[nodiscard]] std::pair<std::vector<Ipp32f>, std::vector<Ipp32f>> minMax() const;
	[[nodiscard]] std::pair<std::vector<Ipp32f>, std::vector<MtiPoint>> maxAndIndex() const;

	[[nodiscard]] Ipp32fArray solve(const Ipp32fArray &rhs) const;

	// This subpixel resamples an ROI of the array into the same ROI of the target
	// if there is data available, that data will be used
	void lanczosResampleRoi(Ipp32fArray &target, const IppiRect &roi, float clampLow, float clampHigh, float rowOffset, float colOffset);

	[[nodiscard]] Ipp32fArray applyBoxFilter(const IppiSize &boxSize) const;
	MtiRect applyBoxFilterValid(const IppiSize &boxSize, Ipp32fArray &normArray, Ipp8uArray &scratchArray) const;
	MtiRect applyBoxFilter(const IppiSize &boxSize, Ipp32fArray &outArray, Ipp8uArray &scratchArray) const;

	[[nodiscard]] Ipp32fArray applyBilateralFilter(int radius) const;
	[[nodiscard]] Ipp32fArray applyAutoContrastFilter() const;

	// This assume equally spaced x-spacing
    Ipp32fArray applyLut(const MtiPlanar<Ipp32fArray>& lut, Ipp8uArray &scratch, float maxLevel = 1.0f) const;
    void applyLutInPlace(const MtiPlanar<Ipp32fArray>& lut, Ipp8uArray &scratch, float maxLevel = 1.0f) const;

	// This takes x-spacing from levels
	Ipp32fArray applyLut(const MtiPlanar<Ipp32fArray>& lut, const Ipp32fArray &levels, Ipp8uArray &scratch) const;
	void applyLutInPlace(const MtiPlanar<Ipp32fArray>& lut, const Ipp32fArray &levels, Ipp8uArray &scratch) const;

	// ITU Rec.709 specification gamma, only supported from 2019 
	//Ipp32fArray gamma709(float minValue = 0.0f, float maxValue = 1.0f) const;
	//void gamma709InPlace(float minValue = 0.0f, float maxValue = 1.0f) const;

	[[nodiscard]] Ipp32fArray equalizeHistogram() const;
	Ipp32fArray dilate(const Ipp8uArray &mask, Ipp8uArray &scratchArray) const;
	Ipp32fArray erode(const Ipp8uArray &mask, Ipp8uArray &scratchArray) const;

	Ipp32fArray convSame(const IppArray &kernel, Ipp8uArray &scratchArray);
	Ipp32fArray convFullReturnCenter(const IppArray &kernel, Ipp32fArray &outArray, Ipp8uArray &scratchArray);

	[[nodiscard]] vector < std::pair<Ipp32f, Ipp32s>> sortWithIndex() const;
	[[nodiscard]] Ipp16uArray toNormalized16u() const;

	// Compute *this = min(*this, sourceArray)
	void minEveryInplace(const Ipp32fArray &sourceArray);
	void absDiff(const Ipp32fArray &operandArray, Ipp32fArray &targetArray);

   // Compare to constant.
   // IppCmpOp = ippCmpLess, ippCmpLessEq, ippCmpEq, ippCmpGreaterEq, or ippCmpGreater.
	[[nodiscard]] Ipp8uArray compareC(Ipp32f c, IppCmpOp cmpOp) const;

    static Ipp32fArray logLut(int rows, float maxValue, float startValue = 100);
	static Ipp32fArray linspace(float startValue, float endValue, int width);
	explicit operator Ipp16uArray() const;
  	explicit operator Ipp32sArray() const;
	explicit operator Ipp8uArray() const;

protected:
	void convertFrom(const IppArray<Ipp8u> &rhs);
	void convertFrom(const IppArray<Ipp16u> &rhs);
	void convertFrom(const IppArray<Ipp32s> &rhs);
	void convertFrom(const IppArray<Ipp32f> &rhs);

	// These need to be moved somewhere
	// medianQuick should be templated.
	// No time now
public:
	static Ipp32f median(vector<Ipp32f> &values);
	static Ipp32f medianSort(vector<Ipp32f> &values);
	static Ipp32f medianQuick(vector<Ipp32f> &values);
	static Ipp32f medianQuick1D(const Ipp32fArray &values);
};

// This is a dummy class to get around some unknown bug
// this bug makes it impossible to use ImageWatcher visualizer
class Ipp32fArrayVisualizer : public Ipp32fArray
{
public:
	Ipp32fArrayVisualizer(const Ipp32fArray &image) : Ipp32fArray(image)
	{
	}
};
#endif // _FLOAT_ARRAY_H_
