#pragma once
#ifndef IppPlanerH
#define IppPlanerH

template<class T>
class IppPlaner : public vector<T>
{
public:
	void push_back(const T& value)
	{
		this->emplace_back(value);
	}

	void emplace_back(const T& value)
	{
		if (value.getComponents() != 1)
		{
			throw std::runtime_error("Planes can have only one channel");
		}

		if (this->size() == 0)
		{
			this->_shape = value.getShape();
		}
		else
		{
			if (this->_shape != value.getShape())
			{
				throw std::runtime_error("Every plane must be same size and type");
			}
		}

		vector<T>::emplace_back(value);
	}

	IppPlaner<T> cumsum()
	{
		IppPlaner<T> result;
		if (this->size() == 0)
		{
			return result;
		}

		T a0;
		a0 <<= this->at(0);
		result.push_back(a0);

		for (auto i = 1; i < this->size(); i++)
		{
			auto sumArray = result[i - 1] + this->at(i);
			result.push_back(sumArray);
		}

		return result;
	}

	void cumsumInplace()
	{
		if (this->size() == 0)
		{
			return;
		}

		for (auto i = 1; i < this->size(); i++)
		{
			this->at(i) += this->at(i-1);
		}
	}

private:
	MtiShape _shape;
};
#endif