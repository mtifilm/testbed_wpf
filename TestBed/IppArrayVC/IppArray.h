#pragma once
#ifndef IppArrayH
#define IppArrayH

// We may want to locally disable this warning
#pragma warning( disable : 4251)
#pragma warning( disable : 26451)

// This is a template base for the instantiations of the IppArrays of type Ipp8u, Ipp16u, Ipp32s, Ipp32f
// Ideally these arrays would all be just the template.
//
//  Concepts:  An IppArray represents an image, allocates, deallocates and shares its own memory. It has
//  underlying methods that call the Intel IPP libraries.  1, 3 and 4 planes are supported.
//  An IppArray is more of a value type than a class type, thus IppArrays can not be newed, deleted or an address taken of.
//  Furthermore, one should only work with the instantiated arrays (Ipp8uArray, Ipp16uArray, Ipp32sArray and Ipp32fArray)
//  not the template versions
//
//  Although these share a great deal with the ideas of OpenCV arrays, they are written in C++ 17 and are
//  more restricted.  This means they can be easier use and faster.
//
//  Creating.  IppArrays used shared memory or can wrap existing memory.
//
//  Basic method, creates memory from a MtiSize mtiSize struct
//     Ipp..Array image(mtiSize)    
//     Examples:  Ipp32fArray image({1440, 2000})   Default is one channel
//                Ippi16uArray  shortImage({ 480, 720, 3)};
//
//  Basic method, wraps memory
//     Ipp..Array  image(mtiSize, *p)   *p is same type as Array and must the the correct size
//     Examples  Ipp32f *p = malloc(2000 * 1440 * 1 * sizeof(Ipp32f));
//               Ipp32fArray image(2000, 1440, 1, p);  p is NOT destroyed, cannot be deleted until image goes away
//               Ipp32fArray image(2000, 1440, 1, p, true);  Creates memory, copies p to the memory
//

enum class MtiSelectMode
{
   valid = 0,
   intersect = 1,
   mirror = 2,
   replicate = 3,
   circular = 4,
   normalize = 16
};

#ifndef __BORLANDC__
#include <sstream>
#define MTIostringstream std::ostringstream
#endif

#include <sstream>
#include <ostream>
#include <vector>
#include <iomanip>
#include <cstddef>
#include <memory>
#include <string>
#include <algorithm>
#include <random>

#include "ipp.h"
#include "MtiIppExtensions.h"
#include "IppException.h"
#include "Windows.h"
#include "MtiRuntimeError.h"
#include "MtiPlanar.h"

using std::string;
using std::vector;

extern IppDataType MTI_IPPARRAY_API findDataType(const string &name);
extern int MTI_IPPARRAY_API findDataSize(IppDataType);

// Forward defs
//class Ipp8uArray;
//class Ipp16uArray;
//class Ipp32fArray;
//class Ipp32sArray;

// Start if IppArray base template
template<class T>
struct DataWrapper
{
public:
   T * dataPtr = nullptr;
   int rowPitchInBytes = 0;
   int typeSize = 0;
	
   virtual ~DataWrapper()
   {
      if (_ownsData)
      {
         ippFree(dataPtr);
      }

      dataPtr = nullptr;
      rowPitchInBytes = 0;
   }

   DataWrapper(T *source, int pitch)
   {
      _ownsData = false;
      dataPtr = source;
      rowPitchInBytes = pitch;
	  typeSize = sizeof(T);
   }

   DataWrapper(int height, int width, int channels)
   {
      rowPitchInBytes = width * channels * sizeof(T);
	  typeSize = sizeof(T);
      dataPtr = reinterpret_cast<T *>(ippMalloc(height * rowPitchInBytes));
      _ownsData = true;
   }

   [[nodiscard]] IppDataType getDataType() const { return _dataType; }

private:
   // if ownsData is true, the memory is released
   bool _ownsData = true;
   IppDataType _dataType = ::findDataType(typeid(T).name());
};


template<class T>
class IppArray
{

public:

   class rIterator : public std::iterator<std::input_iterator_tag, T *>
   {
   public:
      rIterator() = default;
      rIterator(const IppArray<T> *a, int r)
      {
         _a = a;
         _row = r;
      }

      rIterator(const rIterator& mit) : _a(mit._a), _row(mit._row) {}
      rIterator& operator++() { ++_row; return *this; }
      rIterator operator++(int) { rIterator tmp(*this); operator++(); return tmp; }
      bool operator==(const rIterator& rhs) const { return _row == rhs._row; }
      bool operator!=(const rIterator& rhs) const { return _row != rhs._row; }
      T* operator*() { return _a->getRowPointer(_row); }
      void setQ(const IppArray<T> *a, int r)
      {
         _a = a;
         _row = r;
      }

   private:
      const IppArray<T> *_a;
      int _row;
   };

   class linearIterator : public std::iterator<std::random_access_iterator_tag, T>
   {
   public:
      linearIterator() = delete;

      linearIterator(const IppArray<T> *a, int initialIndex = 0)
      {
         _a = a;
         initFromIndex(initialIndex);
      }

      linearIterator(const linearIterator& rhs)
         : _a(rhs._a)
         , _currentPtr(rhs._currentPtr)
         , _currentRowFirstIndex(rhs._currentRowFirstIndex)
         , _rowFirstPtr(rhs._rowFirstPtr)
         , _rowLastPtr(rhs._rowLastPtr)
      {}

      linearIterator& operator++()
      {
         if (++_currentPtr > _rowLastPtr)
         {
            // Note: This depends on getIndex() still working even though
            //       we went past row end!
            initFromIndex(getIndex());
         }

         return *this;
      }

      linearIterator operator++(int)
      {
         linearIterator tmp(*this);
         operator++();
         return tmp;
      }

      linearIterator& operator--()
      {
         if (--_currentPtr < _rowFirstPtr)
         {
            // Note: This depends on getIndex() still working even though
            //       we went past row start!
            initFromIndex(getIndex());
         }

         return *this;
      }

      linearIterator operator--(int)
      {
         linearIterator tmp(*this);
         operator--();
         return tmp;
      }

      linearIterator& operator+=(const int &n)
      {
         _currentPtr += n;
         if (_currentPtr < _rowFirstPtr || _currentPtr > _rowLastPtr)
         {
            // Note: This depends on getIndex() still working even though
            //       we went outside the row first/last range!
            initFromIndex(getIndex());
         }

         return *this;
      }

      linearIterator operator+(const int &n) const
      {
         return linearIterator(_a, getIndex() + n);
      }

      linearIterator& operator-=(const int &n)
      {
         return operator+=(-n);
      }

      linearIterator operator-(const int &n) const
      {
         return linearIterator(_a, getIndex() - n);
      }

      bool operator==(const linearIterator& rhs) const
      {
         return getIndex() == rhs.getIndex();
      }

      bool operator!=(const linearIterator& rhs) const
      {
         return getIndex() != rhs.getIndex();
      }

      bool operator>(const linearIterator& rhs) const
      {
         return getIndex() > rhs.getIndex();
      }

      bool operator>=(const linearIterator& rhs) const
      {
         return getIndex() >= rhs.getIndex();
      }

      bool operator<(const linearIterator& rhs) const
      {
         return getIndex() < rhs.getIndex();
      }

      bool operator<=(const linearIterator& rhs) const
      {
         return getIndex() <= rhs.getIndex();
      }

      T& operator*()
      {
         return *_currentPtr;
      }

      T* operator->() const
      {
         return _currentPtr;
      }

      T& operator[](const int &i) const
      {
         // Random access
         auto rc = i / _a->getComponents();
         const int comp = i % _a->getComponents();
         const int row = rc / _a->getWidth();
         const int col = rc % _a->getWidth();
         T *ptr = _a->getElementPointer({ col, row, comp });
         return *ptr;
      }

      void initFromIndex(int rcc)
      {
         auto rc = rcc / _a->getComponents();
         int comp = rcc % _a->getComponents();
         int row = rc / _a->getWidth();
         int col = rc % _a->getWidth();
         _currentPtr = _a->getElementPointer({ col, row, comp });
         _currentRowFirstIndex = row * _a->getWidth() * _a->getComponents();
         _rowFirstPtr = _a->getRowPointer(row);
         _rowLastPtr = _a->getElementPointer({ _a->getWidth() - 1, row, _a->getComponents() - 1 });
      }

   private:
      const IppArray<T> *_a = nullptr;

      // _rowFirstPtr <= _currentPtr <= rowLastPtr
      T *_currentPtr = nullptr;
      int _currentRowFirstIndex = 0;
      T *_rowFirstPtr = nullptr;
      T *_rowLastPtr = nullptr;

      [[nodiscard]] int getIndex() const
      {
         return int(_currentRowFirstIndex + (_currentPtr - _rowFirstPtr));
      }
   };

   linearIterator begin()
   {
      return linearIterator(this);
   }

   linearIterator end()
   {
      // An iterator pointing at the 0th column of (the )last row + 1).
      return linearIterator(this, getHeight() * getWidth() * getComponents());
   }

   [[nodiscard]] linearIterator cbegin() const
   {
      return linearIterator(this);
   }

   [[nodiscard]] linearIterator cend() const
   {
      // An iterator pointing at the 0th column of (the )last row + 1).
      return linearIterator(this, getHeight() * getWidth() * getComponents());
   }

   /////////////////////////////////////////
   // Usual copy constructor
   //IppArray(const IppArray<T>& rhs)
   //   : _sharedDataPtr(rhs._sharedDataPtr)
   //   , _roi(rhs._roi)
   //   , _baseRoi(rhs._baseRoi)
   //   , _originalBits(rhs._originalBits)
   //   , _components(rhs._components)
   //   //		, _isDirty(rhs._isDirty)                       // TO BE DETERMINED, SHOULD THIS HAPPEN?
   //   , _isContiguous(rhs._isContiguous)
   //   , _firstRoiElementPtr(rhs._firstRoiElementPtr)
   //{
   //}

   IppArray() = default;

   IppArray(const MtiSize &s)
   {
      initNoData(s.height, s.width, s.depth);
      initSharedData();
   }

   // Creates an array from memory
   // copyData = false means array doesn't own memory
   // copyData = true means array copies data
   IppArray(const MtiSize &p, T *source, bool copyData = false)
   {
      const auto height = p.height;
      const auto width = p.width;
      const auto channels = p.depth;

      initNoData(height, width, channels);

      if (copyData)
      {
         initSharedData();
         copyFromMemory(source);
      }
      else
      {
         // DataWrapper  doesn't own memory
         auto dataWrapper = new DataWrapper<T>(source, width * sizeof(T)*channels);
         _sharedDataPtr = std::shared_ptr<DataWrapper<T>>(dataWrapper);
         _firstRoiElementPtr = _sharedDataPtr->dataPtr;
      }

      _isContiguous = width * channels * sizeof(T) == getRowPitchInBytes();
   }

   [[nodiscard]] int getComponents() const
   {
      return _components;
   }

   [[nodiscard]] int getRowPitchInBytes() const
   {
      return _sharedDataPtr->rowPitchInBytes;
   }

#ifdef __BORLANDC__
   //[[deprecated("Replace with getHeight")]]
   int getRows() const
   {
      return _roi.height;
   }

   //[[deprecated("Replace with getWidth")]]
   int getCols() const
   {
      return _roi.width;
   }
#endif

   [[nodiscard]] int getHeight() const
   {
      return _roi.height;
   }

   [[nodiscard]] int getWidth() const
   {
      return _roi.width;
   }

   [[nodiscard]] int area() const
   {
      return getHeight() * getWidth();
   }

   [[nodiscard]] int volume() const
   {
      return getHeight() * getWidth() * getComponents();
   }

   [[nodiscard]] MtiSize getSize() const { return { getWidth(), getHeight(), getComponents() }; }

   T &operator[](int index)
   {
      if (isContiguous())
      {
         return *(data() + index);
      }

      // begin() is a linear operator thus is expensive to build
      return *(begin() + index);
   }

   const T &operator[](int index) const
   {
      if (isContiguous())
      {
         return *(data() + index);
      }

      // begin() is a linear operator thus is expensive to build
      return *(cbegin() + index);
   }

   T &operator[](const MtiPoint &p) { return *getElementPointer(p); }
   const T &operator[](const MtiPoint &p) const { return *getElementPointer(p); }

   // return the point relative to the ROI
   [[nodiscard]] MtiPoint ind2sub(int index) const
   {
      return getSize().ind2sub(index);
   }

   [[nodiscard]] std::vector<MtiPoint> ind2sub(const std::vector<int> &indices) const
   {
      return getSize().ind2sub(indices);
   }

   [[nodiscard]] int sub2ind(const MtiPoint &p) const
   {
      return p.y * getWidth()* getComponents() + p.x* getComponents() + p.z;
   }

   // Does the rect overlap with ROI
   [[nodiscard]] bool doesRoiOverlap(const IppiRect roi) const
   {
      IppiPoint l1 = { _roi.x + _roi.width - 1, _roi.y + roi.height - 1 };
      IppiPoint l2 = { roi.x + roi.width - 1, roi.y + roi.height - 1 };
      auto r1 = _roi;
      auto r2 = roi;

      // If one rectangle is on left side of other
      if (l1.x < r2.x || l2.x < r1.x)
      {
         return false;
      }

      // If one rectangle is above other
      if (l1.y < r2.y || l2.y < r1.y)
      {
         return false;
      }

      return true;
   }

   [[nodiscard]] bool isConjoined(const IppArray<T> &rhs) const
   {
      if (baseData() != rhs.baseData())
      {
         return false;
      }

      return doesRoiOverlap(rhs.getRoi());
   }

   // This makes an invalid array valid
   void reinstate()
   {
      _isDirty = false;
   }

   [[nodiscard]] IppDataType getDataType() const { return _sharedDataPtr->getDataType(); }
   [[nodiscard]] int getDataTypeSize() const { return ::findDataSize(getDataType()); }

   // This release all data like a destructor
   void clear()
   {
      if (isEmpty())
      {
         return;
      }

      _sharedDataPtr = nullptr;
      _firstRoiElementPtr = nullptr;
      _roi = { 0,0,0,0 };
      _baseSize = { 0,0 };
      _components = 0;
      _isDirty = false;
   }

   class RowLoop
   {
   public:
      RowLoop(const IppArray<T> *a, int start, int end)
      {
         _begin.setQ(a, start);
         _end.setQ(a, end);
      }

      rIterator begin() { return _begin; }
      rIterator end() { return _end; }

   private:
      rIterator _begin;
      rIterator _end;
   };

   [[nodiscard]] RowLoop rowIterator() const
   {
      return rowIterator(0, getHeight());
   }

   [[nodiscard]] RowLoop rowIterator(int startRow, int endRow) const
   {
      RowLoop result(this, startRow, endRow);
      return result;
   }

   [[nodiscard]] range rowRange() const
   {
      return rowRange(0, getHeight());
   }

   [[nodiscard]] range rowRange(int startRow, int endRow) const
   {
      range result(startRow, endRow);
      return result;
   }

   [[nodiscard]] range colRange() const
   {
      return rowRange(0, getWidth());
   }

   [[nodiscard]] range colRange(int startCol, int endCol) const
   {
      range result(startCol, endCol);
      return result;
   }

   [[nodiscard]] std::shared_ptr<DataWrapper<T>> getSharedDataPtr() const { return _sharedDataPtr; }

   [[nodiscard]] T *data() const
   {
      if (!_sharedDataPtr)
      {
         return nullptr;
      }

      //if (_isDirty)
      //{
      //	THROW_MTI_RUNTIME_ERROR("Attempting to address an invalid array");
      //}

      return _firstRoiElementPtr;
   }

   [[nodiscard]] T *getRowPointer(int row) const
   {
      if (!_sharedDataPtr)
      {
         return nullptr;
      }

      //	if (_isDirty)
      //	{
      ////		THROW_MTI_RUNTIME_ERROR("Attempting to address an invalid array");
      //	}

      const auto pixelOffsetBytes = row * getRowPitchInBytes();
      auto p = (Ipp8u *)(_firstRoiElementPtr)+pixelOffsetBytes;

      return reinterpret_cast<T *>(p);
   }

   [[nodiscard]] T *getElementPointer(const MtiPoint &mtiPoint) const
   {
      const auto col = mtiPoint.x;
      const auto row = mtiPoint.y;
      const auto comp = mtiPoint.z;

      if (!_sharedDataPtr)
      {
         return nullptr;
      }

      //if (_isDirty)
      //{
      //	THROW_MTI_RUNTIME_ERROR("Attempting to address an invalid array");
      //}

      // ***WARNING*** do not simplify because row pitch in bytes may not be
      // divisible by sizeof(T).
      const auto pixelSizeBytes = getComponents() * sizeof(T);
      const auto pixelOffsetBytes = (col * pixelSizeBytes) + (row * getRowPitchInBytes());
      auto p = reinterpret_cast<Ipp8u *>(_firstRoiElementPtr) + pixelOffsetBytes + (comp * sizeof(T));
      return reinterpret_cast<T *>(p);
   }

   // This returns the base data pointer
   [[nodiscard]] T *baseData() const
   {
      if (!_sharedDataPtr)
      {
         return nullptr;
      }

      return _sharedDataPtr->dataPtr;
   }

   // Returns true if the array has no data
   [[nodiscard]] bool isEmpty() const
   {
      return data() == nullptr || _roi.isEmpty();
   }

   [[nodiscard]] bool isContiguous() const
   {
      // same as getRowPitchInBytes() == getWidth() * getComponents() * int(sizeof(T));
      return _isContiguous;
   }

   [[nodiscard]] bool isDirty() const
   {
      return _isDirty;
   }

   [[nodiscard]] virtual vector<vector<T>> toVector(const IppiRect &roi) const
   {
      auto area = roi.height * roi.width;
      vector<vector<T>> result;
      for (auto i = 0; i < getComponents(); i++)
      {
         result.emplace_back(area);
      }

      int j = 0;
      for (auto r = 0; r < roi.height; r++)
      {
         auto p = getElementPointer({ roi.x , r + roi.y });
         for (auto c = 0; c < roi.width; c++)
         {
            for (auto i = 0; i < getComponents(); i++)
            {
               result[i][j] = *p++;
            }

            j++;
         }
      }

      return result;
   }

   [[nodiscard]] virtual vector<vector<T>> toVector() const
   {
      return toVector({ 0, 0, getWidth(), getHeight() });
   }

   virtual void iota()
   {
      for (auto r = 0; r < getHeight(); r++)
      {
         auto rp = getRowPointer(r);
         for (auto c = 0; c < getWidth(); c++)
         {
            for (auto comp = 0; comp < getComponents(); comp++)
            {
               *rp++ = static_cast<T>(r * getWidth() + c);
            }
         }
      }
   }

   virtual void iota(T start)
   {
      for (auto r = 0; r < getHeight(); r++)
      {
         auto rp = getRowPointer(r);
         for (auto c = 0; c < getWidth(); c++)
         {
            for (auto comp = 0; comp < getComponents(); comp++)
            {
               *rp++ = T(r*getWidth() + c + start);
            }
         }
      }
   }

   virtual void iota(vector<T> start)
   {
      for (auto r = 0; r < getHeight(); r++)
      {
         auto rp = getRowPointer(r);
         for (auto c = 0; c < getWidth(); c++)
         {
            for (auto comp = 0; comp < getComponents(); comp++)
            {
               *rp++ = T(r * getWidth() + c + start[comp]);
            }
         }
      }
   }
   virtual void zero()
   {
      vector<T> zeros(getComponents(), (T)0);
      set(zeros);
   }

   // This is a set but needs to overridden for speed
   virtual void set(const vector<T> &value)
   {
	   for (auto rp : rowIterator())
	   {
		   for (auto c : range(getWidth()))
		   {
			   for (auto d : range(getComponents()))
			   {
				   *rp++ = value[d];
			   }
		   }
	   }
   }

   bool operator ==(const IppArray<T> &rhs) const
   {
      return isEqual(rhs);
   }

   bool operator !=(const IppArray<T> &rhs) const
   {
      return !isEqual(rhs);
   }

   // Mirror fill is not a relative ROI but an absolute ROI
   // Todo: make relative
   void mirrorFill(const MtiRect &roi)
   {
	   const FactorRects factorRects(getRoi(), roi, true);
      if (factorRects.isFactoringNeeded() == false)
      {
         return;
      }

      // The left/right fill must be done before top bottom
      mirrorFillCenterLeft(factorRects);
      mirrorFillCenterRight(factorRects);
      mirrorFillTop(factorRects);
      mirrorFillBottom(factorRects);
   }

   // Mirror fill is not a relative ROI but an absolute ROI
   // Todo: make relative
   void duplicateFill(const MtiRect &roi)
   {
      FactorRects factorRects(getRoi(), roi, true);
      if (factorRects.isFactoringNeeded() == false)
      {
         return;
      }

      // The left/right fill must be done before top bottom
      duplicateFillCenterLeft(factorRects);
      duplicateFillCenterRight(factorRects);
      duplicateFillTop(factorRects);
      duplicateFillBottom(factorRects);
   }

   // Its an error to get an address of an IppArray.  However gtest needs the operator to get info
   // about byte values when dumping and error.  This allows us to use gtest natively
#ifndef GTEST_INCLUDE_GTEST_GTEST_H_
   IppArray<T> * operator &() const = delete;
   IppArray<T> * operator &() = delete;
#endif
	
   void * operator new(size_t size) = delete;
   void operator delete (void * p) = delete;

   // This is data put in because of a mistake in internal format 
   // This is also used for normalization.
   [[nodiscard]] int getOriginalBits() const { return _originalBits; }
   void setOriginalBits(int bits) { _originalBits = bits; }

   [[nodiscard]] T getMin() const { return _minValue; }
   void setMin(T value) { _minValue = value; }

   [[nodiscard]] T getMax() const { return _maxValue; }
   void setMax(T value) { _maxValue = value; }
	
   void setDefaultsFromBits(int bits)
   {
	   _originalBits = bits;
	   _minValue = T(0);
	   _maxValue = T((1 << bits) - 1);
   }

   void setMetadata(const IppArray<T> &rhs)
   {
	   _originalBits = rhs._originalBits;
	   _minValue = rhs._minValue;
	   _maxValue = rhs._maxValue;
   }

   // This allows "null" arrays to be filled
   // False if storage already existed, true otherwise
   bool createStorageIfEmpty(size_t height, size_t width, size_t channels,  int bits,  const T &minValue,  const T &maxValue)
   {
      if (isEmpty() == false)
      {
         return false;
      }

      initNoData(int(height), int(width), int(channels));
      initSharedData();

   	  // set metadata
	  setOriginalBits(bits);
	  setMin(minValue);
	  setMax(maxValue);
      return true;
   }

   bool createStorageIfEmpty(const MtiSize &arraySpecs,  int bits,  const T &minValue,  const T &maxValue)
   {
      return createStorageIfEmpty(arraySpecs.height, arraySpecs.width, arraySpecs.depth, bits, minValue, maxValue);
   }

   bool createStorageIfEmpty(const IppArray<T> &rhs)
   {
      return createStorageIfEmpty(rhs.getHeight(), rhs.getWidth(), rhs.getComponents(), rhs.getOriginalBits(), rhs.getMin(), rhs.getMax());
   }

   void throwOnArraySizeChannelMismatch(const MtiSize &expected, const MtiSize &specs) const
   {
      if (expected.width != specs.width || expected.height != specs.height || expected.depth != specs.depth)
      {
         MTIostringstream os;
         os << "ERROR: Array specs do not agree, " << "expected Specs (" << expected << "), available specs (" << specs << ")!";
         THROW_MTI_RUNTIME_ERROR(os.str());
      }
   }

   void throwOnArraySizeChannelMismatch(const IppArray<T> &ippArray) const
   {
      throwOnArraySizeChannelMismatch(getSize(), ippArray.getSize());
   }

   [[nodiscard]] const MtiRect &getRoi() const
   {
      return _roi;
   }

   // End HELPER functions
protected:
	
	// This is a blind copy assuming the data size is correct
	void copyFromMemory(T *data)
	{
		// In reality, this is only called from a constructor so it must
		// be contiguous, but that is not part of the spec (yet)
		if (isContiguous())
		{
			const auto n = getHeight() * getRowPitchInBytes();

			// Why i'm using _s here is a joke
			memcpy_s(this->data(), n, data, n);
			return;
		}

		auto roi = getRoi();
		auto dp = data;
		auto nComponents = roi.width * this->getComponents();
		auto bytesPerLine = getRowPitchInBytes();

		for (auto r = 0; r < roi.height; r++)
		{
			auto sp = getRowPointer(r);
			memcpy_s(dp, bytesPerLine, sp, bytesPerLine);
			dp += nComponents;
		}
	}

   [[nodiscard]] MtiSize getBaseSize() const
   {
      return _baseSize;
   }

   // Creates a sub-array: same as operator () 
   IppArray(const IppArray<T>& rhs, const IppiRect &roi)
      : _sharedDataPtr(rhs._sharedDataPtr)
      , _roi(rhs.getRoi())
      , _baseSize(rhs._baseSize)
	  , _originalBits(rhs._originalBits)
	  , _minValue(rhs._minValue)
	  , _maxValue(rhs._maxValue)
      , _components(rhs._components)
   {
      throwOnSubRoiMismatch(roi);

      // Need only check width and height bounds
      _roi = { roi.x + _roi.x, roi.y + _roi.y, roi.width, roi.height };

	   // An empty selection returns empty
	  if (_roi.isEmpty())
	  {
		  return;
	  }
		
      auto startInBytes = (_roi.y * getRowPitchInBytes()) + (_roi.x * _components * sizeof(T));
      auto ptrInBytes = (Ipp8u *)_sharedDataPtr->dataPtr + startInBytes;
      _firstRoiElementPtr = reinterpret_cast<T *>(ptrInBytes);
      _isContiguous = getWidth() * getComponents() * sizeof(T) == getRowPitchInBytes();
   }

	// Not currently used
   void invalidateIfConjoined(const IppArray<T> &lhs, IppArray<T> &rhs)
   {
      rhs._isDirty = rhs.isConjoined(lhs);
   }

   [[nodiscard]] bool isEqual(const IppArray<T> &rhs) const
   {
      if (getSize() != rhs.getSize()) 
      {
         return false;
      }

      for (auto r = 0; r < getHeight(); r++)
      {
         auto rp = getRowPointer(r);
         auto sp = rhs.getRowPointer(r);

         for (auto c = 0; c < getWidth()*getComponents(); c++)
         {
            if (rp[c] != sp[c])
            {
               return false;
            }
         }
      }

      return true;
   }

   void throwOnSubRoiMismatch(const MtiRect &roi) const
   {
      // Exit if an empty array
      if (roi.isEmpty())
      {
         return;
      }

      auto &iRoi = getRoi();
      if (roi.x < 0 || roi.width < 0 || (roi.x + roi.width) > iRoi.width
         || roi.y < 0 || roi.height < 0 || (roi.y + roi.height) > iRoi.height)
      {
         MTIostringstream os;
         os << "ERROR: ROI must define a proper sub-array!" << std::endl << "Relative ROI (" << roi << ") is not contained in ROI (" << iRoi << ")!" << std::endl;
         THROW_MTI_RUNTIME_ERROR(os.str());
      }
   }

   void throwComponentError() const
   {
      THROW_MTI_RUNTIME_ERROR("Invalid number of components");
   }

   void throwNotImplemented() const
   {
      THROW_MTI_RUNTIME_ERROR("Code not implemented");
   }

   void throwOnComponentsSupportedError() const
   {
      if (getComponents() != 1 && getComponents() != 3 && getComponents() != 4)
      {
         throwComponentError();
      }
   }

   void initNoData(int height, int width, int channels)
   {
      _roi.x = _roi.y = 0;
      _roi.width = width;
      _roi.height = height;
      _components = channels;
      _baseSize = {width, height};

      if (_roi.height <= 0 || _roi.width <= 0)
      {
         THROW_MTI_RUNTIME_ERROR("IppArray dimensions must be > 0!");
      }

      if (getComponents() != 1 && getComponents() != 3 && getComponents() != 4)
      {
         THROW_MTI_RUNTIME_ERROR("IppArray channels must be 1, 3 or 4 !");
      }
   }

   void initSharedData()
   {
      if (_sharedDataPtr)
      {
         THROW_MTI_RUNTIME_ERROR("Data has allready been allocated");
      }

      // This assumes initNoData has been called
      int height = _roi.height;
      int width = _roi.width;
      int channels = _components;

      auto dataWrapper = new DataWrapper<T>(height, width, channels);
      _sharedDataPtr = std::shared_ptr<DataWrapper<T>>(dataWrapper);
      _firstRoiElementPtr = _sharedDataPtr->dataPtr; // same as data
      _isContiguous = width * channels * sizeof(T) == getRowPitchInBytes();
   }

   [[nodiscard]] const std::shared_ptr<DataWrapper<T>> &getDataWrapper() const { return _sharedDataPtr; }

   void setInvalid(bool value) { _isDirty = value; }

   // This changes the Roi of the base
   // Not a very good idea
   void resetRoi(const MtiRect &roi) {
      _roi = { std::min<int>(0, roi.x), std::min<int>(0, roi.y),
      std::min<int>(std::max<int>(0, _baseSize.width - roi.x), roi.width),
      std::min<int>(std::max<int>(0, _baseSize.height - roi.y), roi.height)
      };
   }

private:
   MtiRect _roi = { 0,0,0,0 };
   MtiSize _baseSize = { 0,0 };
   std::shared_ptr<DataWrapper<T>> _sharedDataPtr;

   // These two are referenced to the _sharedDataPtr
   T *_firstRoiElementPtr = nullptr;
   int _components = 0;
   bool _isContiguous = false;
	
	// This is some metadata.  Used in conversions and visualization
   int _originalBits = 16;
   T _minValue = 0;
   T _maxValue = 1;
   // End metadata

   // TODO:
   // To remind me that I need to implement this
   bool _isDirty = false;

// TODO: These need to be removed to another .h file
 private:
   void mirrorFillCenterLeft(const FactorRects &factorRects) const
   {
      if (factorRects.isFactoringNeeded() == false)
      {
         return;
      }

      // fill the center left
      auto d = getComponents();
      if (factorRects.cl.isEmpty() == false)
      {
         // For each row, reverse it
         auto ul = factorRects.center.tl();
         for (auto r = 0; r < factorRects.cl.height; r++)
         {
            auto m = factorRects.center.width;
            auto p = getElementPointer({ ul.x, ul.y + r });
            auto q = p - d;

            for (auto c = 0; c < factorRects.cl.width; c++)
            {
               // loops are very expensive and don't optimize well, so do a test as tests are almost free
               if (d == 1)
               {
                  *q-- = *p;
               }
               else
               {
                  // Copy a pixel
                  for (auto i = 0; i < d; i++)
                  {
                     q[i] = p[i];
                  }

                  q -= d;
               }

               // if run off out of info, just duplicate last line
               if (--m > 0)
               {
                  p += d;
               }
            }
         }
      }
   }

   void mirrorFillCenterRight(const FactorRects &paddingRects)
   {
      // fill the center right
      auto d = getComponents();
      if (paddingRects.cr.isEmpty() == false)
      {
         // For each row, reverse it
         auto ur = paddingRects.center.tr();
         for (auto r = 0; r < paddingRects.cr.height; r++)
         {
            auto m = paddingRects.center.width;

            // ur is exclusive, so the real data starts one back
            auto p = getElementPointer({ ur.x - 1, ur.y + r });
            auto q = p + d;

            for (auto c = 0; c < paddingRects.cr.width; c++)
            {
               // loops are very expensive and don't optimize well, so do a test as tests are almost free
               if (d == 1)
               {
                  *q++ = *p;
               }
               else
               {
                  // Copy a pixel
                  for (auto i = 0; i < d; i++)
                  {
                     *q++ = p[i];
                  }
               }

               // if run off on info, just duplicate last line
               if (--m > 0)
               {
                  p -= d;
               }
            }
         }
      }
   }

   void mirrorFillTop(const FactorRects &factorRects)
   {
      // Fill the top
      auto d = getComponents();
      if (factorRects.top.isEmpty() == false)
      {
         // For each row, reverse it
         // This strongly assumes factorRects are correct
         auto m = factorRects.top.width;

         for (auto r = 0; r < factorRects.top.height; r++)
         {
            auto sr = factorRects.center.y + std::min<int>(r, factorRects.center.height - 1);
            auto s = getElementPointer({ factorRects.full.x, sr });
            auto t = getElementPointer({ factorRects.top.x, factorRects.top.height - r - 1 });
            for (auto c = 0; c < factorRects.full.width * d; c++)
            {
               *t++ = *s++;
            }
         }
      }
   }

   void mirrorFillBottom(const FactorRects &factorRects)
   {
      // Fill the top
      auto d = getComponents();
      if (factorRects.bottom.isEmpty() == false)
      {
         // For each row, reverse it
         // This strongly assumes factorRects are correct
         auto m = factorRects.full.width;

         for (auto r = 0; r < factorRects.bottom.height; r++)
         {
            auto sr = std::max<int>(factorRects.center.bl().y - r - 1, factorRects.center.y);
            auto s = getElementPointer({ factorRects.full.x, sr });
            auto t = getElementPointer({ factorRects.bottom.x, factorRects.bottom.y + r });
            for (auto c = 0; c < factorRects.full.width * d; c++)
            {
               *t++ = *s++;
            }
         }
      }
   }

   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   void duplicateFillCenterLeft(const FactorRects &factorRects)
   {
      if (factorRects.isFactoringNeeded() == false)
      {
         return;
      }

      // fill the center left
      auto d = getComponents();
      if (factorRects.cl.isEmpty() == false)
      {
         // For each row, duplicate the pixel to right
         auto ul = factorRects.center.tl();
         for (auto r = 0; r < factorRects.cl.height; r++)
         {
            auto m = factorRects.center.width;
            auto p = getElementPointer({ ul.x, ul.y + r });
            auto q = p - d;

            for (auto c = 0; c < factorRects.cl.width; c++)
            {
               // loops are very expensive and don't optimize well, so do a test as tests are almost free
               if (d == 1)
               {
                  *q-- = *p;
               }
               else
               {
                  // Copy a pixel
                  for (auto i = 0; i < d; i++)
                  {
                     q[i] = p[i];
                  }

                  q -= d;
               }
            }
         }
      }
   }

   void duplicateFillCenterRight(const FactorRects &paddingRects)
   {
      // fill the center right
      auto d = getComponents();
      if (paddingRects.cr.isEmpty() == false)
      {
         // For each row, reverse it
         auto ur = paddingRects.center.tr();
         for (auto r = 0; r < paddingRects.cr.height; r++)
         {
            auto m = paddingRects.center.width;

            // ur is exclusive, so the real data starts one back
            auto p = getElementPointer({ ur.x - 1, ur.y + r });
            auto q = p + d;

            for (auto c = 0; c < paddingRects.cr.width; c++)
            {
               // loops are very expensive and don't optimize well, so do a test as tests are almost free
               if (d == 1)
               {
                  *q++ = *p;
               }
               else
               {
                  // Copy a pixel
                  for (auto i = 0; i < d; i++)
                  {
                     *q++ = p[i];
                  }
               }
            }
         }
      }
   }

   void duplicateFillTop(const FactorRects &factorRects)
   {
      // Fill the top
      auto d = getComponents();
      if (factorRects.top.isEmpty() == false)
      {
         // For each row, reverse it
         // This strongly assumes factorRects are correct
         auto m = factorRects.top.width;

         for (auto r = 0; r < factorRects.top.height; r++)
         {
            auto s = getElementPointer({ factorRects.full.x, factorRects.center.y });
            auto t = getElementPointer({ factorRects.top.x, factorRects.top.height - r - 1 });
            for (auto c = 0; c < factorRects.full.width * d; c++)
            {
               *t++ = *s++;
            }
         }
      }
   }

   void duplicateFillBottom(const FactorRects &factorRects)
   {
      // Fill the top
      auto d = getComponents();
      if (factorRects.bottom.isEmpty() == false)
      {
         // For each row, reverse it
         // This strongly assumes factorRects are correct
         auto m = factorRects.full.width;

         for (auto r = 0; r < factorRects.bottom.height; r++)
         {
            auto s = getElementPointer({ factorRects.full.x, factorRects.center.y + factorRects.center.height - 1 });
            auto t = getElementPointer({ factorRects.bottom.x, factorRects.bottom.y + r });
            for (auto c = 0; c < factorRects.full.width * d; c++)
            {
               *t++ = *s++;
            }
         }
      }
   }
};

// This is a dummy class to get around some unknown bug
// this bug makes it impossible to use ImageWatcher visualizer
template<typename T>
class IppVisualizer : public T
{
public:
	IppVisualizer(const T &image) : T(image)
	{
	}
};
#endif
