#ifndef IppAgnosticH
#define IppAgnosticH

/////////////////////// ipp_malloc //////////////////////////////

/////////////////////// SET //////////////////////////////
inline IppStatus ippiSet_C1R(Ipp8u value, Ipp8u* pDst, int dstStep, IppiSize roiSize)
{
	return ippiSet_8u_C1R(value, pDst, dstStep, roiSize);
}

inline IppStatus ippiSet_C1R(Ipp16u value, Ipp16u* pDst, int dstStep, IppiSize roiSize)
{
	return ippiSet_16u_C1R(value, pDst, dstStep, roiSize);
}

inline IppStatus ippiSet_C1R(Ipp32f value, Ipp32f* pDst, int dstStep, IppiSize roiSize)
{
	return ippiSet_32f_C1R(value, pDst, dstStep, roiSize);
}

inline IppStatus ippiSet_C3R(const Ipp8u value[3], Ipp8u* pDst, int dstStep, IppiSize roiSize)
{
	return ippiSet_8u_C3R(value, pDst, dstStep, roiSize);
}

inline IppStatus ippiSet_C3R(const Ipp16u value[3], Ipp16u* pDst, int dstStep, IppiSize roiSize)
{
	return ippiSet_16u_C3R(value, pDst, dstStep, roiSize);
}

inline IppStatus ippiSet_C3R(const Ipp32f value[3], Ipp32f* pDst, int dstStep, IppiSize roiSize)
{
	return ippiSet_32f_C3R(value, pDst, dstStep, roiSize);
}

/////////////////////// Subtrace //////////////////////////////
inline IppStatus ippiSub_C1R(const Ipp8u* pSrc1, int src1Step, const Ipp8u* pSrc2, int src2Step, Ipp8u* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiSub_8u_C1RSfs(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize, 0);
}

inline IppStatus ippiSub_C1R(const Ipp16u* pSrc1, int src1Step, const Ipp16u* pSrc2, int src2Step, Ipp16u* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiSub_16u_C1RSfs(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize, 0);
}

inline IppStatus ippiSub_C1R(const Ipp32f* pSrc1, int src1Step, const Ipp32f* pSrc2, int src2Step, Ipp32f* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiSub_32f_C1R(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize);
}

inline IppStatus ippiSub_C3R(const Ipp8u* pSrc1, int src1Step, const Ipp8u* pSrc2, int src2Step, Ipp8u* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiSub_8u_C3RSfs(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize, 0);
}

inline IppStatus ippiSub_C3R(const Ipp16u* pSrc1, int src1Step, const Ipp16u* pSrc2, int src2Step, Ipp16u* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiSub_16u_C3RSfs(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize, 0);
}

inline IppStatus ippiSub_C3R(const Ipp32f* pSrc1, int src1Step, const Ipp32f* pSrc2, int src2Step, Ipp32f* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiSub_32f_C3R(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize);
}

/////////////////////// ADD //////////////////////////////
inline IppStatus ippiAdd_C1R(const Ipp8u* pSrc1, int src1Step, const Ipp8u* pSrc2, int src2Step, Ipp8u* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiAdd_8u_C1RSfs(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize, 0);
}

inline IppStatus ippiAdd_C1R(const Ipp16u* pSrc1, int src1Step, const Ipp16u* pSrc2, int src2Step, Ipp16u* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiAdd_16u_C1RSfs(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize, 0);
}

inline IppStatus ippiAdd_C1R(const Ipp32f* pSrc1, int src1Step, const Ipp32f* pSrc2, int src2Step, Ipp32f* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiAdd_32f_C1R(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize);
}

inline IppStatus ippiAdd_C3R(const Ipp8u* pSrc1, int src1Step, const Ipp8u* pSrc2, int src2Step, Ipp8u* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiAdd_8u_C3RSfs(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize, 0);
}

inline IppStatus ippiAdd_C3R(const Ipp16u* pSrc1, int src1Step, const Ipp16u* pSrc2, int src2Step, Ipp16u* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiAdd_16u_C3RSfs(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize, 0);
}

inline IppStatus ippiAdd_C3R(const Ipp32f* pSrc1, int src1Step, const Ipp32f* pSrc2, int src2Step, Ipp32f* pDst,
	int dstStep, IppiSize roiSize)
{
	return ippiAdd_32f_C3R(pSrc1, src1Step, pSrc2, src2Step, pDst, dstStep, roiSize);
}

/////////////////////// DOTPRODUCT //////////////////////////////
inline IppStatus ippiDotProd_C1R(const Ipp32f* pSrc1, int src1Step, const Ipp32f* pSrc2, int src2Step, IppiSize roiSize, Ipp64f *pDp)
{
	return ippiDotProd_32f64f_C1R(pSrc1, src1Step, pSrc2, src2Step, roiSize, pDp, ippAlgHintFast);
}

inline IppStatus ippiDotProd_C3R(const Ipp32f* pSrc1, int src1Step, const Ipp32f* pSrc2, int src2Step, IppiSize roiSize, Ipp64f pDp[3])
{
	return ippiDotProd_32f64f_C3R(pSrc1, src1Step, pSrc2, src2Step, roiSize, pDp, ippAlgHintFast);
}

/////////////////////// L2 Norm //////////////////////////////
inline IppStatus ippiNorm_L2_C1R(const Ipp32f* pSrc, int srcStep, IppiSize roiSize, Ipp64f* pValue)
{
	return ippiNorm_L2_32f_C1R(pSrc, srcStep, roiSize, pValue, ippAlgHintFast);
}

inline IppStatus ippiNorm_L2_C3R(const Ipp32f* pSrc, int srcStep, IppiSize roiSize, Ipp64f value[3])
{
	return ippiNorm_L2_32f_C3R(pSrc, srcStep, roiSize, value, ippAlgHintFast);
}

// This applies a value to the the array
#define IPP_ARRAY_APPLY_VALUE(IPP_ARRAY, NAME, IPP_VALUE, IPP_FCN)\
	void IPP_ARRAY::NAME(const vector<IPP_VALUE> &value)\
{\
	auto size = getSize();\
	if ((size.width == 0) || (size.height == 0))\
	{\
			return;\
	}\
	switch (getComponents())\
	{\
	case 1:\
		if (value.size() < 1) THROW_MTI_RUNTIME_ERROR("Not enought values"); \
		IppThrowOnError( IPP_FCN ## _C1R(value[0], data(), getRowPitchInBytes(), size));\
		break;\
\
	case 3:\
		if (value.size() < 3) THROW_MTI_RUNTIME_ERROR("Not enought values"); \
		IppThrowOnError(IPP_FCN ## _C3R(value.data(), data(), getRowPitchInBytes(), size));\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
}

// This 
#define IPP_ARRAY_X_ROI(IPP_ARRAY, NAME, IPP_FCN)\
	IPP_ARRAY IPP_ARRAY::NAME(const IppiRect &roi) const\
	{\
		IPP_ARRAY result(roi.height, roi.width, getComponents());\
		IppiSize roiSize = { roi.width, roi.height };\
		auto dataPtr = getRowPointer(roi.y, roi.x);\
\
		switch (getComponents())\
		{\
		case 1:\
			IppThrowOnError(IPP_FCN##_C1R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));\
			break;\
\
		case 3:\
			IppThrowOnError(IPP_FCN##_C3R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));\
			break;\
\
		case 4:\
			IppThrowOnError(IPP_FCN##_C4R(dataPtr, getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), roiSize));\
			break;\
\
		default:\
			throwComponentError();\
			break;\
		}\
\
		return result;\
	}

// This 
#define IPP_ARRAY_INPLACE(IPP_ARRAY, NAME, IPP_FCN)\
	void IPP_ARRAY::NAME(const IPP_ARRAY &operand)\
{\
		throwOnArraySizeChannelMismatch(operand);\
		IppiSize roiSize = getSize();\
\
		switch (getComponents())\
		{\
		case 1:\
			IppThrowOnError(IPP_FCN##_C1IR(operand.data(), operand.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));\
			break;\
\
		case 3:\
			IppThrowOnError(IPP_FCN##_C3IR(operand.data(), operand.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));\
			break;\
\
		case 4:\
			IppThrowOnError(IPP_FCN##_C4IR(operand.data(), operand.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));\
			break;\
\
		default:\
			throwComponentError();\
			break;\
		}\
	}

// Operator macros 
//   Inplace is like A +=B , no INPLACE is like C = A+B;
//   Saturate means the integer values do not wrap
//   Value is a scaler, ARRAY is an array

//		IPP_OPERATOR_ARRAY_INPLACE_SATURATE
//		IPP_OPERATOR_ARRAY_INPLACE
//		IPP_OPERATOR_ARRAY_SATURATE
//		IPP_OPERATOR_ARRAY

//		IPP_OPERATOR_SCALAR_INPLACE_SATURATE
//		IPP_OPERATOR_SCALAR_INPLACE
//		IPP_OPERATOR_SCALAR_SATURATE
//		IPP_OPERATOR_SCALAR

// Operator ARRAY and VALUE with saturaturation and scale 0
#define IPP_OPERATOR_ARRAY_INPLACE_SATURATE(IPP_ARRAY, OP, IPP_VALUE, IPP_FCN)\
IPP_ARRAY &IPP_ARRAY::operator OP(IppArray<IPP_VALUE> &rhs) \
{\
	throwOnArraySizeChannelMismatch(rhs);\
	auto size = getSize();\
\
	switch (getComponents())\
	{\
	case 1:\
		IppThrowOnError(IPP_FCN##_C1RSfs(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), data(), getRowPitchInBytes(), size, 0));\
		break;\
\
	case 3:\
		IppThrowOnError(IPP_FCN##_C3RSfs(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), data(), getRowPitchInBytes(), size, 0));\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
\
	invalidateIfConjoined(*this, rhs);\
\
	return *this;\
}

// Operator ARRAY and ARRAY inplace
#define IPP_OPERATOR_ARRAY_INPLACE(IPP_ARRAY, OP, IPP_VALUE, IPP_FCN)	\
IPP_ARRAY &IPP_ARRAY::operator OP(IppArray<IPP_VALUE> &rhs)\
{\
	throwOnArraySizeChannelMismatch(rhs);\
	auto size = getSize();\
\
	switch (getComponents())\
	{\
	case 1:\
		IppThrowOnError(IPP_FCN##_C1R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), data(), getRowPitchInBytes(), size));\
		break;\
\
	case 3:\
		IppThrowOnError(IPP_FCN##_C3R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), data(), getRowPitchInBytes(), size));\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
\
	invalidateIfConjoined(*this, rhs);\
\
	return *this;\
}


// Operator ARRAY and SCALAR inplace
#define IPP_OPERATOR_SCALAR_INPLACE(IPP_ARRAY, OP, IPP_VALUE, IPP_FCN)	\
IPP_ARRAY &IPP_ARRAY::operator OP(const IPP_VALUE value) \
{\
	auto size = getSize();\
	IPP_VALUE valueArray[] = {value, value, value};\
\
	switch (getComponents())\
	{\
	case 1:\
		IppThrowOnError(IPP_FCN##_C1R(data(), getRowPitchInBytes(), value, data(), getRowPitchInBytes(), size));\
		break;\
\
	case 3:\
		IppThrowOnError(IPP_FCN##_C3R(data(), getRowPitchInBytes(), valueArray, data(), getRowPitchInBytes(), size));\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
\
	return *this;\
}

// Operator ARRAY and ARRAY with saturation
#define IPP_OPERATOR_ARRAY_SATURATE(IPP_ARRAY, OP, IPP_VALUE, IPP_FCN)\
IPP_ARRAY IPP_ARRAY::operator OP(const IppArray<IPP_VALUE> &rhs) const \
{\
	throwOnArraySizeChannelMismatch(rhs);\
	auto size = getSize();\
	auto &roi = getRoi();\
\
	IPP_ARRAY result({roi.width, roi.height, getComponents()}); \
\
	switch (getComponents())\
	{\
	case 1:\
		IppThrowOnError(IPP_FCN##_C1RSfs(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), size, 0));\
		break;\
\
	case 3:\
		IppThrowOnError(IPP_FCN##_C3RSfs(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), size, 0));\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
\
	return result;\
}


// Operator ARRAY and SCALAR inplace saturation
#define IPP_OPERATOR_SCALAR_INPLACE_SATURATE(IPP_ARRAY, OP, IPP_VALUE, IPP_FCN)	\
IPP_ARRAY &IPP_ARRAY::operator OP(const IPP_VALUE value) \
{\
	auto size = getSize();\
	IPP_VALUE valueArray[] = {value, value, value};\
\
	switch (getComponents())\
	{\
	case 1:\
		IppThrowOnError(IPP_FCN##_C1RSfs(data(), getRowPitchInBytes(), value, data(), getRowPitchInBytes(), size, 0));\
		break;\
\
	case 3:\
		IppThrowOnError(IPP_FCN##_C3RSfs(data(), getRowPitchInBytes(), valueArray, data(), getRowPitchInBytes(), size, 0));\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
\
	return *this;\
}

// Operator ARRAY and SCALAR output ARRAY
#define IPP_OPERATOR_SCALAR_SATURATE(IPP_ARRAY, OP, IPP_VALUE, IPP_FCN)	\
IPP_ARRAY IPP_ARRAY::operator OP(const IPP_VALUE scalar) const \
{\
	auto size = getSize();\
	auto &roi = getRoi();\
\
	IPP_ARRAY result({roi.width, roi.height, getComponents()}); \
\
	switch (getComponents())\
	{\
	case 1:\
		IppThrowOnError(IPP_FCN##_C1RSfs(data(), getRowPitchInBytes(), scalar, result.data(), result.getRowPitchInBytes(), size, 0));\
		break;\
\
	case 3:\
		{\
			IPP_VALUE scalars[3] = { scalar, scalar, scalar }; \
			IppThrowOnError(IPP_FCN##_C3RSfs(data(), getRowPitchInBytes(), scalars, result.data(), result.getRowPitchInBytes(), size, 0)); \
		}\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
\
	return result;\
}


// Operator ARRAY and ARRAY output ARRAY
#define IPP_OPERATOR_ARRAY(IPP_ARRAY, OP, IPP_VALUE, IPP_FCN)	\
IPP_ARRAY IPP_ARRAY::operator OP(const IppArray<IPP_VALUE> &rhs) const \
{\
	throwOnArraySizeChannelMismatch(rhs);\
	auto size = getSize();\
	auto &roi = getRoi();\
\
	IPP_ARRAY result({roi.width, roi.height, getComponents()}); \
\
	switch (getComponents())\
	{\
	case 1:\
		IppThrowOnError(IPP_FCN##_C1R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), size));\
		break;\
\
	case 3:\
		IppThrowOnError(IPP_FCN##_C3R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), size));\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
\
	return result;\
}

// Operator ARRAY and SCALAR output ARRAY
#define IPP_OPERATOR_SCALAR(IPP_ARRAY, OP, IPP_VALUE, IPP_FCN)	\
IPP_ARRAY IPP_ARRAY::operator OP(const IPP_VALUE scalar) const \
{\
	auto size = getSize();\
	auto &roi = getRoi();\
\
	IPP_ARRAY result({roi.width, roi.height, getComponents()}); \
\
	switch (getComponents())\
	{\
	case 1:\
		IppThrowOnError(IPP_FCN##_C1R(data(), getRowPitchInBytes(), scalar, result.data(), result.getRowPitchInBytes(), size));\
		break;\
\
	case 3:\
		{\
			Ipp32f scalars[3] = { scalar, scalar, scalar }; \
			IppThrowOnError(IPP_FCN##_C3R(data(), getRowPitchInBytes(), scalars, result.data(), result.getRowPitchInBytes(), size)); \
		}\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
\
	return result;\
}

#define IPP_CONVERT_ARRAY_TO_ARRAY(IPP_ARRAY, IPP_RHS_TYPE, IPP_FCN)\
void IPP_ARRAY::convertFrom(const IppArray<IPP_RHS_TYPE> &rhs)\
{\
if (rhs.getHeight() == 0 || rhs.getWidth() == 0)\
{\
		return;\
}\
	this->createStorageIfEmpty(rhs.getSize(), rhs.getOriginalBits(), rhs.getMin(), rhs.getMax());\
\
auto roiSize = getSize();\
auto rhsSize = rhs.getSize();\
\
if (roiSize != rhsSize)\
{\
	THROW_MTI_RUNTIME_ERROR("Dimensions of IppArray operands must match!");\
}\
switch (getComponents())\
{\
case 1:\
	IppThrowOnError(IPP_FCN##_C1R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));\
	break;\
\
case 3:\
	IppThrowOnError(IPP_FCN##_C3R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));\
	break;\
\
case 4:\
	IppThrowOnError(IPP_FCN##_C4R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));\
	break;\
\
default:\
	throwComponentError();\
}\
}

#define IPP_COPY_ARRAY_TO_ARRAY(IPP_ARRAY, IPP_RHS_TYPE, IPP_FCN)\
void IPP_ARRAY::convertFrom(IppArray<IPP_RHS_TYPE> &rhs)\
{\
if (rhs.getHeight() == 0 || rhs.getWidth() == 0)\
{\
		return;\
}\
	this->createStorageIfEmpty(rhs.getHeight(), rhs.getWidth(), rhs.getComponents());\
\
auto roiSize = getSize();\
auto rhsSize = rhs.getSize();\
\
if (roiSize != rhsSize)\
{\
	THROW_MTI_RUNTIME_ERROR("Dimensions of IppArray operands must match!");\
}\
\
switch (getComponents())\
{\
case 1:\
	IppThrowOnError(IPP_FCN##_C1R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));\
	break;\
\
case 3:\
	IppThrowOnError(IPP_FCN##_C3R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));\
	break;\
\
case 4:\
	IppThrowOnError(IPP_FCN##_C4R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize));\
	break;\
\
default:\
	throwComponentError();\
}\
}

#define IPP_CONVERT_ARRAY_TO_ARRAY_WITH_ROUND(IPP_ARRAY, IPP_RHS_TYPE, IPP_FCN)\
void IPP_ARRAY::convertFrom(const IppArray<IPP_RHS_TYPE> &rhs)\
{\
if (rhs.getHeight() == 0 || rhs.getWidth() == 0)\
{\
		return;\
}\
this->createStorageIfEmpty(rhs.getSize(), rhs.getOriginalBits(), rhs.getMin(), rhs.getMax());\
\
auto roiSize = getSize();\
auto rhsSize = rhs.getSize();\
\
if (roiSize != rhsSize)\
{\
	THROW_MTI_RUNTIME_ERROR("Dimensions of IppArray operands must match!");\
}\
\
switch (getComponents())\
{\
case 1:\
	IppThrowOnError(IPP_FCN##_C1R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize, IppRoundMode::ippRndNear));\
	break;\
\
case 3:\
	IppThrowOnError(IPP_FCN##_C3R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize, IppRoundMode::ippRndNear));\
	break;\
\
case 4:\
	IppThrowOnError(IPP_FCN##_C4R(rhs.data(), rhs.getRowPitchInBytes(), data(), getRowPitchInBytes(), roiSize, IppRoundMode::ippRndNear));\
	break;\
\
default:\
	throwComponentError();\
}\
}

#define IPP_CONVERT_OPERATOR_BODY \
{\
convertFrom(rhs);\
\
return *this;\
}

#define IPP_ARRAY_ARRAY_ARRAY(IPP_ARRAY, NAME, IPP_VALUE, IPP_FCN)	\
void IPP_ARRAY::NAME(IPP_ARRAY &input, IPP_ARRAY &result)\
{\
	throwOnArraySizeChannelMismatch(input);\
	throwOnArraySizeChannelMismatch(result);\
	auto size = getSize();\
\
	switch (getComponents())\
	{\
	case 1:\
		IppThrowOnError(IPP_FCN##_C1R(data(), getRowPitchInBytes(), input.data(), input.getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), size));\
		break;\
\
	case 3:\
		IppThrowOnError(IPP_FCN##_C3R(data(), getRowPitchInBytes(), input.data(), input.getRowPitchInBytes(), result.data(), result.getRowPitchInBytes(), size));\
		break;\
\
	default:\
		throwComponentError();\
		break;\
	}\
\
	invalidateIfConjoined(*this, result);\
\
}

#endif
