#include "IpaStripeStream.h"
#include <algorithm>
#include <thread>
#include <iostream>

#ifdef __BORLANDC__
// use bthreads
#include "SynchronousThreadRunner.h"
#include "SysInfo.h"
#define AvailableProcessors SysInfo::AvailableProcessorCount
#else
// use std threads
#include "StdSynchronousThreadRunner.h"
#define AvailableProcessors std::thread::hardware_concurrency
#endif

// Because we cannot easily perform implicit type conversion for scalers
// write these routines
#define GET_SCALAR_VALUE(T, S)\
	switch (S)\
{\
		case ipp64f:\
			return (T)_scalarValue._double;\
\
		case ipp32f:\
			return (T)_scalarValue._float;\
\
		case ipp32s:\
			return (T)_scalarValue._int;\
\
		case ipp16u:\
			return (T)_scalarValue._unsignedShort;\
\
		case ipp8u:\
			return  (T)_scalarValue._byte;\
\
		default:\
			THROW_MTI_RUNTIME_ERROR("Illegal type conversion");\
}

Ipp64f IpaStreamDataItem::getIpp64f() const
{
	GET_SCALAR_VALUE(Ipp64f, getScalarType());
}

Ipp32f IpaStreamDataItem::getIpp32f() const
{
	GET_SCALAR_VALUE(Ipp32f, getScalarType());
}

Ipp32s IpaStreamDataItem::getIpp32s() const
{
	GET_SCALAR_VALUE(Ipp32s, getScalarType());
}

Ipp16u IpaStreamDataItem::getIpp16u() const
{
	GET_SCALAR_VALUE(Ipp16u, getScalarType());
}

Ipp8u IpaStreamDataItem::getIpp8u() const
{
	GET_SCALAR_VALUE(Ipp8u, getScalarType());
}

IpaStripeStream::IpaStripeStream()
{
}


IpaStripeStream::~IpaStripeStream()
{
}

void IpaStripeStream::clear()
{
	// Clear data;
	_dataStack.clear();
	_functionStack.clear();
}

//IpaStripeStream &IpaStripeStream::operator<<(Ipp8uArray& rhs)
//{
//	_dataStack.emplace_back(rhs);
//	return *this;
//}
//
//IpaStripeStream &IpaStripeStream::operator<<(Ipp16uArray& rhs)
//{
//	_dataStack.emplace_back(rhs);
//	return *this;
//}
//
//IpaStripeStream &IpaStripeStream::operator<<(Ipp32fArray& rhs)
//{
//	_dataStack.emplace_back(rhs);
//	return *this;
//}

IpaStripeStream &IpaStripeStream::operator<<(const Ipp8uArray& rhs)
{
	_dataStack.emplace_back(rhs);
	return *this;
}

IpaStripeStream &IpaStripeStream::operator<<(const Ipp16uArray& rhs)
{
	_dataStack.emplace_back(rhs);
	return *this;
}

IpaStripeStream &IpaStripeStream::operator<<(const Ipp32fArray& rhs)
{
	_dataStack.emplace_back(rhs);
	return *this;
}

IpaStripeStream &IpaStripeStream::operator<<(const Ipp32sArray& rhs)
{
	_dataStack.emplace_back(rhs);
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp16uArray &image)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp16uArray>(func, CallType::unary16u, ipp16u));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp8uArray &image)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp8uArray>(func, CallType::unary8u, ipp8u));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp32fArray &image)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp32fArray>(func, CallType::unary32f, ipp32f));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp32sArray &image)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp32sArray>(func, CallType::unary32s, ipp32s));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp16uArray &, int)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp16uArray>(func, CallType::unary16u, ipp16u));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp8uArray &, int)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp8uArray>(func, CallType::unary8u, ipp8u));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp32fArray &, int)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp32fArray>(func, CallType::unary32f, ipp32f));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp32sArray &, int)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp32sArray>(func, CallType::unary32s, ipp32s));
	return *this;
}


IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp16uArray &in, Ipp16uArray &out)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp16uArray>(func, CallType::binary16u16u, ipp16u));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp8uArray &in, Ipp8uArray &out)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp8uArray>(func, CallType::binary8u8u, ipp8u));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(Ipp32fArray &in, Ipp32fArray &out)> func)
{
	_functionStack.push_back(new functionalItemClass<Ipp32fArray>(func, CallType::binary32f32f, ipp32f));
	return *this;

}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(int jobNumber)> func)
{
	_functionStack.push_back(new functionalJobClass(func));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(const MtiRect &stripeRoi)> func)
{
	_functionStack.push_back(new functionalItemRoiClass(func));
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(std::function<void(const MtiRect &stripeRoi, int jobNumber)> func)
{
	_functionStack.push_back(new functionalItemRoiClass(func, 0));
	return *this;
}


IpaStripeStream & IpaStripeStream::operator<<(Ipp64f value)
{
	_dataStack.emplace_back(value);
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(Ipp32f value)
{
	_dataStack.emplace_back(value);
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(Ipp32s value)
{
	_dataStack.emplace_back(value);
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(size_t value)
{
	_dataStack.emplace_back(value);
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(Ipp16u value)
{
	_dataStack.emplace_back(value);
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(Ipp8u value)
{
	_dataStack.emplace_back(value);
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(const MtiRect &roi)
{
	// Push in reverse order
	_dataStack.emplace_back(roi.height);
	_dataStack.emplace_back(roi.width);
	_dataStack.emplace_back(roi.y);
	_dataStack.emplace_back(roi.x);
	return *this;
}

IpaStripeStream & IpaStripeStream::operator<<(const MtiSize &size)
{
	// A size is really an ROI based at (0,0)
	// This allows for easier striping of arrays with non-trivial ROIs
	return *this << MtiRect(0, 0, size.width, size.height);
}

//IpaStripeStream & IpaStripeStream::operator<<(const IppiRect &roi)
//{
//	// Push in reverse order
//	_dataStack.emplace_back(roi.height);
//	_dataStack.emplace_back(roi.width);
//	_dataStack.emplace_back(roi.y);
//	_dataStack.emplace_back(roi.x);
//	return *this;
//}

void IpaStripeStream::throwIfNoDataMatch(IppDataType dt)
{
	if (_dataStack.empty())
	{
		THROW_MTI_RUNTIME_ERROR("No data available on IpaStripeStream stack");
	}

	auto &di = _dataStack.back();
	if (di.isArray() == false)
	{
		if (di.getScalarType() != dt)
		{
			std::ostringstream os;
			os << "Expected scalar of type " << dt << " found type << di.getScalarType()";
			THROW_MTI_RUNTIME_ERROR(os.str());
		}

		return;
	}

	if (di.getArrayType() != dt)
	{
		std::ostringstream os;
		os << "Expected array of type " << dt << " found type << di.getArrayType()";
		THROW_MTI_RUNTIME_ERROR(os.str());
	}
}

IpaStripeStream IpaStripeStream::runUnary()
{
	auto fi = _functionStack.back();
	_functionStack.pop_back();

	auto dt = fi->getDataType();
	throwIfNoDataMatch(dt);
	auto di = _dataStack.back();
	_dataStack.pop_back();

	switch (dt)
	{
	case ipp8u:
		fi->execute(di.getIpp8uArray());
		break;

	case ipp16u:
		fi->execute(di.getIpp16uArray());
		break;

	case ipp32f:
		fi->execute(di.getIpp32fArray());
		break;

	case ipp32s:
		fi->execute(di.getIpp32sArray());
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Unsupported data type");
	}

	return *this;
}

IpaStripeStream IpaStripeStream::runUnaryJob()
{
	auto fi = _functionStack.back();
	_functionStack.pop_back();

	// Only one data type
	auto dt = fi->getDataType();
	throwIfNoDataMatch(dt);
	auto di = _dataStack.back();
	_dataStack.pop_back();

	auto loops = di.getIpp32s();
	for (auto i : range(loops))
	{
		fi->execute(i);
	}

	return *this;
}

IpaStripeStream IpaStripeStream::runRoi()
{
	auto fi = _functionStack.back();
	_functionStack.pop_back();

	MtiRect roi;

	throwIfNoDataMatch(ipp32s);
	roi.x = _dataStack.back().getIpp32s();
	_dataStack.pop_back();

	throwIfNoDataMatch(ipp32s);
	roi.y = _dataStack.back().getIpp32s();
	_dataStack.pop_back();

	throwIfNoDataMatch(ipp32s);
	roi.width = _dataStack.back().getIpp32s();
	_dataStack.pop_back();

	throwIfNoDataMatch(ipp32s);
	roi.height = _dataStack.back().getIpp32s();
	_dataStack.pop_back();

	switch (fi->getCallType())
	{
	case CallType::unaryRoi:
		if (fi->hasJobArg())
		{
			fi->execute(roi, 0);
		}
		else
		{
			fi->execute(roi);
		}
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Unsupported data type");
	}

	return *this;
}

IpaStripeStream IpaStripeStream::runBinary()
{
	auto fi = _functionStack.back();
	_functionStack.pop_back();

	auto dt = fi->getDataType();
	throwIfNoDataMatch(dt);
	auto din = _dataStack.back();
	_dataStack.pop_back();

	throwIfNoDataMatch(dt);
	auto dout = _dataStack.back();
	_dataStack.pop_back();

	switch (dt)
	{
	case ipp8u:
		fi->execute(din.getIpp8uArray(), din.getIpp8uArray());
		break;

	case ipp16u:
		fi->execute(din.getIpp16uArray(), din.getIpp16uArray());
		break;

	case ipp32f:
		fi->execute(din.getIpp32fArray(), din.getIpp32fArray());
		break;

	default:
		THROW_MTI_RUNTIME_ERROR("Unsupported data type");
	}

	return *this;
}

IpaStripeStream IpaStripeStream::run()
{
	while (!_functionStack.empty())
	{
		auto fi = _functionStack.back();
		switch (fi->getCallType())
		{
		case CallType::unary8u:
		case CallType::unary16u:
		case CallType::unary32f:
		case CallType::unary32s:
			runUnary();
			break;

		case CallType::binary8u8u:
		case CallType::binary16u16u:
		case CallType::binary32f32f:
			runBinary();
			break;

		case CallType::unaryRoi:
			runRoi();
			break;

		case CallType::unaryJob:
			runUnaryJob();
			break;

		default:
			THROW_MTI_RUNTIME_ERROR("No such call type");
		}
	}

	return *this;
}

IpaStripeStream IpaStripeStream::duplicate(const MtiRect &rect)
{
	IpaStripeStream iss;
	THROW_MTI_RUNTIME_ERROR("Not implemented");
	return iss;
}

int jobDispatcher(void *params, int job, int totalJobs)
{
	auto par = (stripeParams *)params;
	if (par == nullptr)
	{
		return -1012;
	}

	auto fi = par->f;
	auto &di = par->data0;
	auto &di1 = par->data1;
	auto dt = fi->getDataType();

	switch (fi->getCallType())
	{
	case CallType::unary8u:
	case CallType::unary16u:
	case CallType::unary32f:
	case CallType::unary32s:
		// This needs to be fixed, but kludge it for now and program like kurt
		switch (dt)
		{
		case ipp8u:
		{
			auto dataArray = di.getIpp8uArray();
			auto roi = SynchronousThreadRunner::findSliceRoi(job, totalJobs, dataArray.getSize());
			if (fi->hasJobArg())
			{
				fi->execute(dataArray(roi), job);
			}
			else
			{
				fi->execute(dataArray(roi));
			}
		}
		break;

		case ipp16u:
		{
			auto dataArray = di.getIpp16uArray();
			auto roi = SynchronousThreadRunner::findSliceRoi(job, totalJobs, dataArray.getSize());
			if (fi->hasJobArg())
			{
				fi->execute(dataArray(roi), job);
			}
			else
			{
				fi->execute(dataArray(roi));
			}
		}
		break;

		case ipp32f:
		{
			auto dataArray = di.getIpp32fArray();
			auto roi = SynchronousThreadRunner::findSliceRoi(job, totalJobs, dataArray.getSize());
			if (fi->hasJobArg())
			{
				fi->execute(dataArray(roi), job);
			}
			else
			{
				fi->execute(dataArray(roi));
			}
		}

		break;

		default:
			THROW_MTI_RUNTIME_ERROR("Unsupported data type");
		}

		break;

	case CallType::unaryRoi:
	{
		auto &inputRoi = par->roi;
		MtiRect roi = SynchronousThreadRunner::findSliceRoi(job, totalJobs, { inputRoi.width, inputRoi.height });
		roi.x += inputRoi.x;
		roi.y += inputRoi.y;

		switch (fi->getCallType())
		{
		case CallType::unaryRoi:
			if (fi->hasJobArg())
			{
				fi->execute(roi, job);
			}
			else
			{
				fi->execute(roi);
			}
			break;

		default:
			THROW_MTI_RUNTIME_ERROR("Unsupported data type");
		}
	}
	break;

	case CallType::binary8u8u:
	case CallType::binary16u16u:
	case CallType::binary32f32f:
		// This needs to be fixed, but kludge it for now and program like kurt
		// This needs to be fixed, but kludge it for now and program like kurt
		switch (dt)
		{
		case ipp8u:
		{
			auto dataArray = di.getIpp8uArray();
			auto dataArrayIn = di1.getIpp8uArray();
			auto roi = SynchronousThreadRunner::findSliceRoi(job, totalJobs, dataArray.getSize());
			fi->execute(dataArrayIn(roi), dataArray(roi));
		}
		break;

		case ipp16u:
		{
			auto dataArray = di.getIpp16uArray();
			auto dataArrayIn = di1.getIpp16uArray();
			auto roi = SynchronousThreadRunner::findSliceRoi(job, totalJobs, dataArray.getSize());
			fi->execute(dataArrayIn(roi), dataArray(roi));
		}
		break;

		case ipp32f:
		{
			auto dataArray = di.getIpp32fArray();
			auto dataArrayIn = di1.getIpp32fArray();
			auto roi = SynchronousThreadRunner::findSliceRoi(job, totalJobs, dataArray.getSize());
			fi->execute(dataArrayIn(roi), dataArray(roi));
		}
		break;

		default:
			THROW_MTI_RUNTIME_ERROR("Unsupported data type");
		}

		break;

	case CallType::unaryJob:
		fi->execute(job);
		break;

	}

	return 0;
}

IpaStripeStream IpaStripeStream::stripe(int stripes, int threads)
{
	if (threads == 0)
	{
		threads = AvailableProcessors();

		// fall back, this should never happen
		if (threads == 0)
		{
			threads = 8;
		}
	}

	if (stripes == 0)
	{
		stripes = threads;
	}

	// Avoid too many because a 2k becomes too small
	 // probably just allow people to shoot themselves in the foot.
	auto actualStripes = std::min<int>(stripes, _maxStripes);

	while (!_functionStack.empty())
	{
		stripeParams params;
		auto fi = _functionStack.back();
		_functionStack.pop_back();
		params.f = fi;

		// Ugly dispatcher we don't need to do this but will clean up later
		if (fi->isNoArg())
		{
			// On no args, no limit on maximum stripes
			auto dt = fi->getDataType();
			throwIfNoDataMatch(dt);

			actualStripes = _dataStack.back().getIpp32s();
			_dataStack.pop_back();
		}
		else if (fi->isRoi())
		{
			params.roi.x = _dataStack.back().getIpp32s();
			_dataStack.pop_back();

			params.roi.y = _dataStack.back().getIpp32s();
			_dataStack.pop_back();

			params.roi.width = _dataStack.back().getIpp32s();
			_dataStack.pop_back();

			params.roi.height = _dataStack.back().getIpp32s();
			_dataStack.pop_back();
		}
		else if (fi->isBinary())
		{
			auto dt = fi->getDataType();
			throwIfNoDataMatch(dt);

			params.data0 = _dataStack.back();
			_dataStack.pop_back();

			throwIfNoDataMatch(dt);
			params.data1 = _dataStack.back();
			_dataStack.pop_back();
		}
		else if (fi->isUnary())
		{
			auto dt = fi->getDataType();
			throwIfNoDataMatch(dt);

			params.data0 = _dataStack.back();
			_dataStack.pop_back();
		}
		else
		{
			THROW_MTI_RUNTIME_ERROR("Unknown call type");
		}

		SynchronousThreadRunner stripeRunner(actualStripes, (void *)&params, jobDispatcher);
		stripeRunner.Run();
	}

	return *this;
}

bool functionalItem::isUnary() const
{
	return (_callType == CallType::unary8u) ||
		(_callType == CallType::unary16u) ||
		(_callType == CallType::unary32f) ||
		(_callType == CallType::unary32s);
}

bool functionalItem::isBinary() const
{
	return (_callType == CallType::binary8u8u) ||
		(_callType == CallType::binary16u16u) ||
		(_callType == CallType::binary32f32f);
}

bool functionalItem::isRoi() const
{
	return _callType == CallType::unaryRoi;
}

bool functionalItem::isNoArg() const
{
	return _callType == CallType::unaryJob;
}
