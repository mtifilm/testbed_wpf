// This is NOT designed to be included by itself but
// included only in IppArray.h
#pragma once
#ifndef Ipp16uArrayH
#define Ipp16uArrayH

#include "IppArray.h"

class MTI_IPPARRAY_API Ipp16uArray : public IppArray<Ipp16u>
{

public:
	Ipp16uArray() = default;

	Ipp16uArray(const MtiSize &shape) : IppArray <Ipp16u>(shape) { setDefaultsFromBits(16); }
	Ipp16uArray(const MtiSize &shape, Ipp16u *source, bool copyData = false) : IppArray(shape, source, copyData) {}

protected:
	Ipp16uArray(const IppArray<Ipp16u> & rhs, const IppiRect &roi) : IppArray(rhs, roi)
	{
		setMetadata(rhs);
	}

public:

	/// Specific functions
	void set(const vector<Ipp16u> &value);
	void normalDistribution(double mean, double stddev);
	void uniformDistribution(Ipp16u low, Ipp16u high);
	[[nodiscard]] vector<Ipp64f> sum() const;
	[[nodiscard]] vector<Ipp16u> median(const IppiRect &roi) const;
	[[nodiscard]] vector<Ipp16u> median() const;

	void copyFromPod(Ipp16u *source, int size);

	[[nodiscard]] Ipp16uArray duplicate(const IppiRect &roi) const
	{
		Ipp16uArray result;
		result <<= (*this)(roi);
		result.setOriginalBits(getOriginalBits());
		return result;
	}

	[[nodiscard]] Ipp16uArray duplicate() const
	{
		Ipp16uArray result;
		result <<= *this;
		result.setOriginalBits(getOriginalBits());
		return result;
	}

	Ipp16uArray &operator <<=(const IppArray<Ipp8u>& rhs) { convertFrom(rhs); return *this; }
	Ipp16uArray &operator <<=(const IppArray<Ipp16u>& rhs) { convertFrom(rhs); return *this; }
	Ipp16uArray &operator <<=(const IppArray<Ipp32s>& rhs) { convertFrom(rhs); return *this; }
	Ipp16uArray &operator <<=(const IppArray<Ipp32f>& rhs) { convertFrom(rhs); return *this; }

	// Arithmetic, add an array and an array
	Ipp16uArray operator +(const IppArray<Ipp16u> &rhs) const;
	Ipp16uArray operator -(const IppArray<Ipp16u> &rhs) const;
	Ipp16uArray operator *(const IppArray<Ipp16u> &rhs) const;
	Ipp16uArray operator / (const IppArray<Ipp16u> &rhs) const;

	// Arithmetic, add an array and a scaler 
	Ipp16uArray operator +(const Ipp16u scalar) const;
	Ipp16uArray operator -(const Ipp16u scalar) const;
	Ipp16uArray operator *(const Ipp16u scalar) const;
	Ipp16uArray operator / (const Ipp16u scalar) const;

	// Arithmetic, add an array in place 
	Ipp16uArray &operator +=(IppArray<Ipp16u> &rhs);
	Ipp16uArray &operator -=(IppArray<Ipp16u> &rhs);
	Ipp16uArray &operator *=(IppArray<Ipp16u> &rhs);
	Ipp16uArray &operator /=(IppArray<Ipp16u> &rhs);

	// Arithmetic, add an scaler in place 
	Ipp16uArray &operator +=(const Ipp16u scalar);
	Ipp16uArray &operator -=(const Ipp16u scalar);
	Ipp16uArray &operator *=(const Ipp16u scalar);
	Ipp16uArray &operator /=(const Ipp16u scalars);

	void minEveryInplace(const Ipp16uArray &operand);

	Ipp16uArray operator()(const MtiRect &roi) const { return Ipp16uArray(*this, roi); } 
	Ipp16uArray operator()(const MtiRect &probeRoi, MtiSelectMode copyType, bool utilizeExtrinsicData = false) const;

   // Warning, this takes the 2D indices from one plane and returns a IppArray
// of same size as indices but depth of calling array
// Thus a 3D image of a vector returns r0, b0, g0, r1, b1, g1, ....
	[[nodiscard]] Ipp16uArray fromPixelIndices(const IppArray<Ipp32s> &indices) const;
	[[nodiscard]] std::vector<Ipp16u> fromPixelIndices(const vector<Ipp32s> &indices) const;

	[[nodiscard]] Ipp16uArray sharpen() const;
	[[nodiscard]] Ipp16uArray transpose() const;
	[[nodiscard]] Ipp16uArray toGray() const;
	Ipp16uArray toGray(const float weights[]) const;

	[[nodiscard]] Ipp16uArray fliplr() const { return mirror(IppiAxis::ippAxsVertical); }
	[[nodiscard]] Ipp16uArray mirror(IppiAxis flip) const;

	void Y2RGB(const Ipp16uArray & src);

	[[nodiscard]] MtiPlanar<Ipp16uArray> toPlanar() const;
	void exportToInterleave3(Ipp16uArray &dest);

	void copyFromPlanar(const MtiPlanar <Ipp16uArray> &planes);

	[[nodiscard]] vector<Ipp64f> L1Norm() const;
	[[nodiscard]] vector<Ipp64f> L2Norm() const;
	[[nodiscard]] vector<Ipp64f> L1NormDiff(const IppArray<Ipp16u> &rhs) const;
	[[nodiscard]] vector<Ipp64f> L2NormDiff(const IppArray<Ipp16u> &rhs) const;

	void importNormalizedYuvFromRgb(unsigned short *p, int rows, int cols, int dataMax);
	void importOneNormalizedChannelFromRgb(unsigned short *p, int rows, int cols, int whichChannel, int dataMax);

	[[nodiscard]] Ipp16uArray applyBoxFilter(IppiSize boxSize) const;
	void absDiff(const Ipp16uArray & input, Ipp16uArray &result);

	[[nodiscard]] Ipp32fArray crossCorrNorm(const IppArray<Ipp16u> &kernalImage, IppiROIShape roiShape = IppiROIShape::ippiROIFull) const;
	void maxAndIndex(Ipp16u *maxValues, MtiPoint *indices) const;
	[[nodiscard]] std::pair<std::vector<Ipp16u>, std::vector<MtiPoint>> maxAndIndex() const;

	[[nodiscard]] vector<Ipp64f> mean() const;
	[[nodiscard]] vector<Ipp64f> dotProduct(const IppArray &rhs) const;

	[[nodiscard]] Ipp16uArray resize(IppiSize newSize) const;
	[[nodiscard]] Ipp16uArray resizeAntiAliasing(IppiSize newSize) const;

	// This assume equally spaced x-spacing
	Ipp16uArray applyLut(const MtiPlanar<Ipp32sArray>& lut, Ipp8uArray &scratch, int maxLevel = 65535) const;
	void applyLutInPlace(const MtiPlanar<Ipp32sArray>& lut, Ipp8uArray &scratch, int maxLevel = 65535) const;

	// This takes x-spacing from levels
	Ipp16uArray applyLut(const MtiPlanar<Ipp32sArray>& lut, const Ipp32sArray &levels, Ipp8uArray &scratch) const;
	void applyLutInPlace(const MtiPlanar<Ipp32sArray>& lut, const Ipp32sArray &levels, Ipp8uArray &scratch) const;
	
	// Returns an array of the entire data 
	[[nodiscard]] Ipp16uArray baseArray() const
	{
		auto result = *this;
		result.resetRoi({ { 0,0 }, getBaseSize() });
		return result;
	}

	[[nodiscard]] Ipp16uArray stripAlpha() const;
	[[nodiscard]] ::std::pair<Ipp16uArray, Ipp16uArray> splitAlpha() const;
	Ipp16uArray addAlpha(const Ipp16uArray& alphaArray) const;
	void L1NormDiff(const IppArray<Ipp16u> &rhs, Ipp64f result[]) const;
	void L2NormDiff(const IppArray<Ipp16u> &rhs, Ipp64f result[]) const;

	[[nodiscard]] Ipp32fArray toNormalized32f() const;
	explicit operator Ipp32fArray() const;
	explicit operator Ipp32sArray() const;
	explicit operator Ipp8uArray() const;


protected:

	// Conversion operators
	void convertFrom(const IppArray<Ipp8u> &rhs);
	void convertFrom(const IppArray<Ipp16u> &rhs);
	void convertFrom(const IppArray<Ipp32s> &rhs);
	void convertFrom(const IppArray<Ipp32f> &rhs);

	// These need to be moved somewhere
	// medianQuick should be templated.
	// No time now
public:
	static Ipp16u median(vector<Ipp16u> &values);
	static Ipp16u medianSort(vector<Ipp16u> &values);
	static Ipp16u medianQuick(vector<Ipp16u> &values);
};

// This is a dummy class to get around some unknown bug
// this bug makes it impossible to use ImageWatcher visualizer
class Ipp16uVisualizer : public Ipp16uArray
{
public:
	Ipp16uVisualizer(const Ipp16uArray &image) : Ipp16uArray(image)
	{
	}
};
#endif 


