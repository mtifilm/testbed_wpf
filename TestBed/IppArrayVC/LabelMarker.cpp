#include <numeric>
#include "LabelMarker.h"

LabelMarker::LabelMarker(IppiNorm norm)
{
	_connectNorm = norm;
}

Ipp32sArray LabelMarker::label(const Ipp8uArray & inputImage)
{
	if (inputImage.getComponents() != 1)
	{
		THROW_MTI_RUNTIME_ERROR("Labeling only works on 1D arrays")
	}

	int bufSize = 0;
	IppThrowOnError(ippiLabelMarkersGetBufferSize_8u32s_C1R(inputImage.getSize(), &bufSize));
	if (bufSize > _tempBuffer.getWidth())
	{
		_tempBuffer = Ipp8uArray(bufSize);
	}

	Ipp32sArray marker(inputImage.getSize());

	auto status = ippiLabelMarkers_8u32s_C1R(
		inputImage.data(),
		inputImage.getRowPitchInBytes(),
		marker.data(),
		marker.getRowPitchInBytes(), 
		inputImage.getSize(),
		_minLabel, 
		_maxLabel, 
		_connectNorm, 
		&_numberOfLabels,
		_tempBuffer.data());

	return marker;
}

void LabelMarker::setMinMaxLabel(int minLabel, int maxLabel)
{
	_minLabel = minLabel;
	_maxLabel = maxLabel;
}

vector<vector<int>> LabelMarker::getClusterIndices(const Ipp32sArray &labelMask, int numberOfLabels, int minSize, int maxSize) const
{
   vector<vector<int>> clusterIndices;

   if (numberOfLabels == 0)
	{
      return clusterIndices;
   }

	auto labelMaskFloat = Ipp32fArray(labelMask);

   ////////////////////////////////////////////////////////////////////////////
   // This is an ad hoc index sorter, no time to make a general interface
   // to a sparse sorter (lots of zeros we ignore)
   auto clusterSize = FastHistogramIpp::compute(labelMaskFloat, numberOfLabels + 1, 0, numberOfLabels + 1.0f)[0];
   Ipp32sArray nonZeroBinData({ numberOfLabels }, reinterpret_cast<Ipp32s *>(clusterSize.data()) + 1);
   auto totalNonZeroPixels = int(nonZeroBinData.sum()[0]);

   // Pixel values
   Ipp32sArray pixelValueIndexes(MtiSize(totalNonZeroPixels + 1));
   pixelValueIndexes.set({ -1 });
   vector<int> edges(clusterSize.size());
   edges[0] = 0;
   for (size_t i = 1; i < edges.size(); i++)
   {
      edges[i] = edges[i - 1] + clusterSize[i];
   }

   // We are skipping the first value
   const auto storagePtr = pixelValueIndexes.data() + 1;
   vector<Ipp32s> count2(numberOfLabels, 0);
   Ipp32s idx = 0;
   for (auto r = 0; r < labelMaskFloat.getHeight(); r++)
   {
      auto sp = labelMaskFloat.getRowPointer(r);
      for (auto c = 0; c < labelMaskFloat.getWidth(); c++)
      {
         // v is really integers
         auto v = int(*sp++);
         if (v > numberOfLabels)
         {
            v = numberOfLabels;
         }

         if (v > 0)
         {
            v = v - 1;
            *(storagePtr + edges[v] + count2[v]) = idx;
            count2[v]++;
         }

         idx++;
      }
   }

   // Add one to edges to skip 0 bin
   for (auto &v : edges)
   {
      v += 1;
   }

   clusterIndices.reserve(numberOfLabels);

   for (auto i = 0; i < numberOfLabels; i++)
   {
      auto s0 = edges[i + 1] - edges[i];
      if ((s0 >= minSize) && (s0 <= maxSize))
      {
         vector<int> pv;
         pv.resize(s0);
         for (auto k = edges[i]; k < edges[i + 1]; k++)
         {
            pv[k - edges[i]] = pixelValueIndexes[k];
         }

         clusterIndices.push_back(pv);
      }
   }

   // End of ad-hoc bin sorter
   ////////////////////////////////////////////////////////////////////////////

   return clusterIndices;
}

vector<vector<int>> LabelMarker::getEquivalenceIndices(const Ipp16uArray &image) const
{
	vector<vector<int>> equiValenceIndices;
	auto maxNumberOfValues = 0xFFFF;
	auto binSizes = FastHistogramIpp::compute(image, maxNumberOfValues + 1, 0.0f, maxNumberOfValues + 1.0f)[0];
	Ipp32sArray nonZeroBinData({ maxNumberOfValues + 1}, reinterpret_cast<Ipp32s *>(binSizes.data()));
	auto totalPixels = image.area();

	// Pixel values
	Ipp32sArray pixelValueIndexes(MtiSize(totalPixels + 1));
	pixelValueIndexes.set({ -1 });
	vector<int> edges(binSizes.size()+1);
   edges[0] = 0;
	std::partial_sum(begin(binSizes), end(binSizes), begin(edges) + 1);

	// We are skipping the first value
	const auto storagePtr = pixelValueIndexes.data();
	vector<Ipp32s> count2(maxNumberOfValues, 0);
	Ipp32s idx = 0;
	for (auto r = 0; r < image.getHeight(); r++)
	{
		auto sp = image.getRowPointer(r);
		for (auto c = 0; c < image.getWidth(); c++)
		{
			auto v = *sp++;
			if (v > maxNumberOfValues)
			{
				v = maxNumberOfValues;
			}

			*(storagePtr + edges[v] + count2[v]) = idx;
			count2[v]++;
			
			idx++;
		}
	}

	// Find number of nonzero edges
	// This is to avoid too many allocations
	auto numberOfNonzeroBins = std::accumulate
	(
		begin(binSizes),
		end(binSizes), 
		0,
		[](const int l, const int r) { return r == 0 ? l : l + 1; }
	);
//   auto numberOfNonzeroBins = 100;
	equiValenceIndices.reserve(numberOfNonzeroBins);

	for (auto i = 0; i < maxNumberOfValues; i++)
	{
		auto s0 = edges[i + 1] - edges[i];
		if (s0 != 0)
		{
			vector<int> pv;
			pv.resize(s0);
			for (auto k = edges[i]; k < edges[i + 1]; k++)
			{
				pv[k - edges[i]] = pixelValueIndexes[k];
			}

			equiValenceIndices.push_back(pv);
		}
	}

	return equiValenceIndices;
}

