#ifndef polyRegularizeH
#define polyRegularizeH
#pragma once
#include "IppArrayLibInt.h"
#include "Ippheaders.h"

namespace MtiMath
{
	Ipp16uArray MTI_IPPARRAY_API polyRegularize(const Ipp16uArray &aIn, const Ipp16uArray &bIn, int mm, int MM, double epsilon, int order);
}

#endif


