#ifndef IppMatIOH
#define IppMatIOH

#include "IppHeaders.h"

// forward reference, this just avoids making libmatio public
// However we may want to make it pubic at some point
struct _mat_t;

class MTI_IPPARRAY_API MatIO
{
public:
	template<typename ... T>
	static int saveListToMatFile(const std::string &fileName, T ... args)
	{
		return appendListToMat(openMatList(fileName), args...) ;
	}

	template<typename ... T>
	static int appendListToMatFile(const std::string &fileName, T ... args)
	{
		return appendListToMat(appendMatList(fileName), args...);
		//return DoesFileExist(fileName) 
		//    ? appendListToMat(appendMatList(fileName), args...)
		//    : appendListToMat(openMatList(fileName), args...);
	}

	static vector<std::string> getVariableNames(const std::string &fileName);

	static Ipp8uArray readIpp8uArray(const std::string &fileName, const std::string &varName);
	static Ipp8uArray readIpp8uArray(const std::string &fileName);
	static bool readIppArray(const std::string &fileName, Ipp8uArray &ippArray, const std::string &varName);

	static Ipp16uArray readIpp16uArray(const std::string &fileName, const std::string &varName);
	static Ipp16uArray readIpp16uArray(const std::string &fileName);
	static bool readIppArray(const std::string &fileName, Ipp16uArray &ippArray, const std::string &varName);

	static Ipp32fArray readIpp32fArray(const std::string &fileName, const std::string &varName);
	static Ipp32fArray readIpp32fArray(const std::string &fileName);
	static bool readIppArray(const std::string &fileName, Ipp32fArray &ippArray, const std::string &varName);

	static Ipp32sArray readIpp32sArray(const std::string &fileName, const std::string &varName);
	static Ipp32sArray readIpp32sArray(const std::string &fileName);
	static bool readIppArray(const std::string &fileName, Ipp32sArray &ippArray, const std::string &varName);
	
	static Ipp32fArray readIpp32fArrayFromDouble(const std::string &fileName, const std::string &varName);
	static Ipp32fArray readIpp32fArrayFromDouble(const std::string &fileName);

	static MtiPlanar<Ipp32fArray> readIpp32fPlanar(const std::string &fileName, const std::string &varName);
	static MtiPlanar<Ipp32fArray> readIpp32fPlanar(const std::string &fileName);

	static MtiRect readMtiRect(const std::string &fileName, const std::string &varName);

	static vector<double> readVectorIpp64f(const std::string &fileName, const std::string &varName);

private:

	// To add a new type of variable for writing, you only need to add a function here
	static int appendToMat(_mat_t *mat, const Ipp8uArray &inArray, const std::string &varName);
	static int appendToMat(_mat_t *mat, const Ipp16uArray &inArray, const std::string &varName);
	static int appendToMat(_mat_t *mat, const Ipp32sArray &inArray, const std::string &varName);
	static int appendToMat(_mat_t *mat, const Ipp32fArray &inArray, const std::string &varName);

	static int appendToMat(_mat_t *mat, int value, const std::string &varName);
	static int appendToMat(_mat_t *mat, float value, const std::string & varName);
	static int appendToMat(_mat_t *mat, double value, const std::string & varName);
	static int appendToMat(_mat_t *mat, const std::string &value, const std::string &varName);
	static int appendToMat(_mat_t *mat, vector<Ipp64f> &value, const std::string &varName);
	static int appendToMat(_mat_t *mat, vector<Ipp32f> &value, const std::string &varName);
	static int appendToMat(_mat_t *mat, vector<Ipp32u> &value, const std::string &varName);
	static int appendToMat(_mat_t *mat, vector<Ipp32s> &value, const std::string &varName);
	static int appendToMat(_mat_t *mat, vector<Ipp16u> &value, const std::string &varName);
	static int appendToMat(_mat_t *mat, vector<Ipp8u> &value, const std::string &varName);

	static int appendToMat(_mat_t *mat, const MtiRect &value, const std::string &varName);
	static int appendToMat(_mat_t *mat, const vector<Ipp32fArray> &value, const std::string &varName);
    static int appendToMat(_mat_t* mat, const vector<Ipp32sArray>& value, const std::string& varName);
    static int appendToMat(_mat_t *mat, const MtiPlanar<Ipp32fArray> &value, const std::string &varName);
    static int appendToMat(_mat_t *mat, const MtiPlanar<Ipp32sArray> &value, const std::string &varName);
	static int appendToMat(_mat_t *mat, const MtiPlanar<Ipp16uArray> &value, const std::string &varName);
	
private:

	// These should not be changed
	static int closeMatList(_mat_t *mat);
	static _mat_t *openMatList(const std::string &fileName);
	static _mat_t *appendMatList(const std::string &fileName);

	// This is the pesudo-recursive step 
	// Remember templates are expanded via recursion at compile time
	template<typename T0, typename T1, typename ... T>
	static int appendListToMat(_mat_t *mat, T0 ippArray, T1 name, T ... args)
	{
		auto err = appendToMat(mat, ippArray, name);
		if (err != 0)
		{
			return err;
		}

		return appendListToMat(mat, args ...);
	}

	// this terminates the recursion
	static int appendListToMat(_mat_t *mat)
	{
		return closeMatList(mat);
	}

	// Read functions
	static vector<std::string> getVariableNames(_mat_t *mat);

	static bool readIppArray(_mat_t *mat, Ipp8uArray &ippArray, const std::string &varName);
	static bool readIppArray(_mat_t *mat, Ipp16uArray &ippArray, const std::string &varName);
	static bool readIppArray(_mat_t *mat, Ipp32fArray &ippArray, const std::string &varName);
	static bool readIppArray(_mat_t *mat, Ipp32sArray &ippArray, const std::string &varName);
	
	static Ipp8uArray readIpp8uArray(_mat_t *mat, const std::string &varName);
	static Ipp8uArray readIpp8uArray(_mat_t *mat);
	static Ipp16uArray readIpp16uArray(_mat_t *mat, const std::string &varName);
	static Ipp16uArray readIpp16uArray(_mat_t *mat);
	static Ipp32fArray readIpp32fArray(_mat_t *mat, const std::string &varName);
	static Ipp32fArray readIpp32fArray(_mat_t *mat);
	static Ipp32sArray readIpp32sArray(_mat_t *mat, const std::string &varName);
	static Ipp32sArray readIpp32sArray(_mat_t *mat);

	static Ipp32fArray readIpp32fArrayFromDouble(_mat_t *mat, const std::string &varName);
	static Ipp32fArray readIpp32fArrayFromDouble(_mat_t * mat);

	static MtiPlanar<Ipp32fArray> readIpp32fPlanar(_mat_t * mat);
	static MtiPlanar<Ipp32fArray> readIpp32fPlanar(_mat_t * mat, const std::string &varName);

	template<typename T>
	static int appendToMatVector(_mat_t * mat, vector<T>& value, const std::string & varName)
	{
		vector<Ipp32f> value32f;
		for (auto &f : value)
		{
			value32f.emplace_back(Ipp32f(f));
		}

		const Ipp32fArray out({ int(value32f.size()), 1, 1 }, value32f.data());
		return MatIO::appendToMat(mat, out, varName);
	}

	static vector<double> readVectorIpp64f(_mat_t* mat, const std::string& varName);
};

#define SaveVariablesToMatFile1(F, A0) MatIO::saveListToMatFile(F, A0, #A0)
#define SaveVariablesToMatFile2(F, A0, A1) MatIO::saveListToMatFile(F, A0, #A0, A1, #A1)
#define SaveVariablesToMatFile3(F, A0, A1, A2) MatIO::saveListToMatFile(F, A0, #A0, A1, #A1, A2, #A2)
#define SaveVariablesToMatFile4(F, A0, A1, A2, A3) MatIO::saveListToMatFile(F, A0, #A0, A1, #A1, A2, #A2, A3, #A3)

#define AppendVariablesToMatFile1(F, A0) MatIO::appendListToMatFile(F, A0, #A0)
#define AppendVariablesToMatFile2(F, A0, A1) MatIO::appendListToMatFile(F, A0, #A0, A1, #A1)
#define AppendVariablesToMatFile3(F, A0, A1, A2) MatIO::appendListToMatFile(F, A0, #A0, A1, #A1, A2, #A2)
#define AppendVariablesToMatFile4(F, A0, A1, A2, A3) MatIO::appendListToMatFile(F, A0, #A0, A1, #A1, A2, #A2, A3, #A3)

#endif

