#pragma once
#include "chrono"
#include "iostream"
class CHRTimer
{
public:
	CHRTimer();
	void start();
	void stop();

	double elapsedMilliseconds();
	double intervalMilliseconds();

	friend std::ostream& operator<<(std::ostream& os, CHRTimer &hrt);
	friend std::wostream& operator<<(std::wostream& os, CHRTimer &hrt);
private:
	bool _started;
	std::chrono::system_clock::time_point _start;
	std::chrono::system_clock::time_point _intervalStart;
	std::chrono::system_clock::time_point _end;
};

