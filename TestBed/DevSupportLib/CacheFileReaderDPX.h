#pragma once
#include "MtiCore.h"
#include "IppArray.h"
#include <map>

class CacheFileReaderDPX
{
public:
	CacheFileReaderDPX();
	virtual ~CacheFileReaderDPX();

private:
	std::map<std::string fileName, Ipp16uArray &dataArray>();
};

