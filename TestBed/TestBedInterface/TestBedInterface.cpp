// TestBedInterface.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <MtiCore.h>
#include "SourceMedia.h"
#include "DevIO.h"
#include "CacheFileReader.h"
#include "BaseToolProcessor.h"
#include "ExampleToolProcessor.h"

// We never import these
#define MTI_TESTBED_INTERFACE __declspec(dllexport)

// A global that controls where the data comes from
// If false from the file itself, true from the processed list.
extern "C"
{
	MTI_TESTBED_INTERFACE int InitCache()
	{
		CacheFileReader::getInstance()->initCache();
		return 0;
	}

	MTI_TESTBED_INTERFACE int CloseCache()
	{
		try
		{
			delete CacheFileReader::getInstance();
		}
		catch (const std::exception &ex)
		{
			// We should log exception here
			std::cerr << "Error at " << __LINE__ << " in " << __func__ << "\nError: " << ex.what();
			return -2;
		}
		catch (...)
		{
			// Unknown exception
			std::cerr << "Unknown Error at " << __LINE__ << " in " << __func__;
			return 0x70000001;
		}

		return 0;
	}

	struct SimpleImageDescriptor
	{
		MtiSize size;
		int rowPitchInBytes;
		Ipp16u *data;
		int32_t originalBits;
		Ipp16u *alphaData;
	};

	struct SimpleRect
	{
		int X;
		int Y;
		int Width;
		int Height;
	};

	MTI_TESTBED_INTERFACE int GetFileInfo(char *fileName, void *simpleImageDescriptor)
	{
		auto imageDescriptor = reinterpret_cast<SimpleImageDescriptor *>(simpleImageDescriptor);
		const auto ippArray = CacheFileReader::getInstance()->getDisplayImage(fileName);
		const auto alphaArray = CacheFileReader::getInstance()->getInputAlpha(fileName);
		imageDescriptor->size = ippArray.getSize();
		imageDescriptor->rowPitchInBytes = ippArray.getRowPitchInBytes();
		imageDescriptor->originalBits = ippArray.getOriginalBits();

		// THIS IS VERY DANGEROUS
		// In production this needs to be a get/release
		// i.e., a separate in use list and some sort of destructor on C# objects to release
		// No time to do it right. For now the cache will make it safe but not idiot proof.
		// VERY POOR PROGRAMMING, DO NOT USE IN ANY PRODUCTION CODE
		imageDescriptor->data = ippArray.data();
		imageDescriptor->alphaData = alphaArray.data();

		return 0;
	}

	static BaseToolProcessor *_toolProcessor = nullptr;
	
	MTI_TESTBED_INTERFACE int CreateTool()
	{
		delete _toolProcessor;
		_toolProcessor = ToolProcessorFactory();
		return 0;
	}
	
	MTI_TESTBED_INTERFACE int InitToolProcessingNative(char *fileName, SimpleRect *rect, int frames)
	{
		if (_toolProcessor == nullptr)
		{
			CreateTool();
		}
		
		const auto processArray16u = CacheFileReader::getInstance()->getDisplayImage(fileName, { rect->X, rect->Y, rect->Width, rect->Height});
		return _toolProcessor->initToolProcessing(processArray16u, frames);
	}

	// Preprocessor section
	MTI_TESTBED_INTERFACE int InitPreprocessingNative(int pass, char **fileNamesPtr, int frames, SimpleRect *rect, int index)
	{
		try
		{
			if (_toolProcessor == nullptr)
			{
				CreateTool();
			}

			vector<string> fileNames;
			for (const auto i : range(frames))
			{
				fileNames.emplace_back(fileNamesPtr[i]);
			}

			return _toolProcessor->initPreprocessingBase(pass, fileNames, { rect->X, rect->Y, rect->Width, rect->Height });
		}
		catch (const std::exception &ex)
		{
			std::cerr << ex.what() << std::endl;
			return -2;
		}
		catch (...)
		{
			return -1;
		}
	}
	
	MTI_TESTBED_INTERFACE int CancelPreprocessingAsyncNative()
	{
		return _toolProcessor->cancelPreprocessingAsyncBase();
	}

	MTI_TESTBED_INTERFACE int PreprocessFrameNative(int pass, char *fileName, SimpleRect *rect, int index)
	{
		try
		{
			const MtiRect roi = { rect->X, rect->Y, rect->Width, rect->Height };
			const auto imageFrame = CacheFileReader::getInstance()->getInputFrame(fileName, roi);
			return _toolProcessor->preprocessFrameBase(pass, string(fileName), imageFrame,  index);
		}
		catch (const std::exception &ex)
		{
			std::cerr << ex.what() << std::endl;
			return -2;
		}
		catch (...)
		{
			return -1;
		}
		
		return 0;
	}

	MTI_TESTBED_INTERFACE int PreprocessingCompletedNative(int pass)
	{
		return _toolProcessor->preprocessingCompletedBase(pass);
	}

	MTI_TESTBED_INTERFACE int SetPreviewModeNative(bool view)
	{
		CacheFileReader::getInstance()->setPreviewMode(view);
		return 0;
	}

	// Processor section
	MTI_TESTBED_INTERFACE int InitProcessingNative(char **fileNamesPtr, int frames, SimpleRect *rect, int index, bool saveFrames, char *subfolder)
	{
		try
		{
			if (_toolProcessor == nullptr)
			{
				CreateTool();
			}

			vector<string> fileNames;
			for (const auto i : range(frames))
			{
				fileNames.emplace_back(fileNamesPtr[i]);
			}
			
			return _toolProcessor->initProcessingBase(fileNames, {rect->X, rect->Y, rect->Width, rect->Height}, saveFrames, subfolder);
		}
		catch (const std::exception &ex)
		{
			std::cerr << ex.what() << std::endl;
			return -2;
		}
		catch (...)
		{
			return -1;
		}
	}

	
	MTI_TESTBED_INTERFACE int CancelProcessingAsyncNative()
	{
		return _toolProcessor->cancelProcessingAsyncBase();
	}

	MTI_TESTBED_INTERFACE int ProcessFrameNative(char *fileName, SimpleRect *rect, int index)
	{
		try
		{
			const MtiRect roi = { rect->X, rect->Y, rect->Width, rect->Height };
			const auto imageFrame = CacheFileReader::getInstance()->getInputFrame(fileName, roi);
			return _toolProcessor->processFrameBase(string(fileName), imageFrame, index);
		}
		catch (const std::exception &ex)
		{
			std::cerr << ex.what() << std::endl;
			return -2;
		}
		catch (...)
		{
			return -1;
		}

		return 0;
	}

	MTI_TESTBED_INTERFACE int ProcessingCompletedNative()
	{
		return _toolProcessor->processingCompletedBase();
	}
}


