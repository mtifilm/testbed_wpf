﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MtiSimple.Core;

// These really need to be defined in a common place but for now just keep in model
namespace WpfCTestBed.Model
{
    /// <summary>
    /// TODO: create a structure that reflect the IppArray data storage.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SimpleImageDescriptor
    {
        public MtiSize<int> Size { get; set; }
        public int RowPitchInBytes { get; set; }
        public IntPtr DataPtr { get; set; }
        public int OriginalBits { get; set; }
        public IntPtr AlphaPtr { get; set; }


        public IntPtr GetRowPointer(int r)
        {
            return IntPtr.Add(DataPtr, r * RowPitchInBytes);
        }

        // TODO: the 2 is bad, the ImageDescriptor should contain format
        public IntPtr GetRowPointer(int r, int c)
        {
            return IntPtr.Add(DataPtr, r * RowPitchInBytes + 2 * Size.Depth * c);
        }

        public IntPtr GetAlphaRowPointer(int r)
        {
            return IntPtr.Add(AlphaPtr, r * RowPitchInBytes);
        }

        // TODO: the 2 is bad, the ImageDescriptor should contain format
        public IntPtr GetAlphaRowPointer(int r, int c)
        {
            return IntPtr.Add(AlphaPtr, r * RowPitchInBytes + 2 * Size.Depth * c);
        }

        public override string ToString()
        {
            if (AlphaPtr == IntPtr.Zero)
            {
                return this.Size.ToString();
            }

            return this.Size.ToString() + "A";

        }
        /// <summary>
        /// This "selects" a sub image.
        /// TODO: we need to emulate the IppSelect, this is just crop
        /// </summary>
        /// <param name="subRectangle"></param>
        /// <returns></returns>
        public SimpleImageDescriptor Select(MtiRect<int> subRectangle)
        {
            var x = Math.Max(subRectangle.X, 0);
            var y = Math.Max(subRectangle.Y, 0);
            var w = subRectangle.Width + subRectangle.X - x;
            var h = subRectangle.Height + subRectangle.Y - y;
 
            w = ((x + w) > Size.Width) ? Size.Width - x : w;
            h = ((y + h) > Size.Height) ? Size.Height - x : h;

            var result = new SimpleImageDescriptor
            {
                Size = new MtiSize<int>(w, h),
                DataPtr = this.GetRowPointer(x,y)
            };

            return result;
        }
    }
}
