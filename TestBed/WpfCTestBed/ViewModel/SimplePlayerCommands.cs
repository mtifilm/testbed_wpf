﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WpfCTestBed.View;

namespace WpfCTestBed.ViewModel
{
    /// </summary>
    public static class SimplePlayerCommands
    {
        /// <summary>
        /// Common command with parameter
        /// </summary>
        public static readonly RoutedCommand MediaPlayerCommand =
            new RoutedCommand("MediaPlayerCommand", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// JogForward Command (F)
        /// </summary>
        public static readonly RoutedCommand JogForwardCommand =
            new RoutedCommand("JogForward", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// JogForward Command (S)
        /// </summary>
        public static readonly RoutedCommand JogBackwardCommand =
            new RoutedCommand("JogBackward", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// JogForward Command (F)
        /// </summary>
        public static readonly RoutedCommand MarkEntireClipCommand =
            new RoutedCommand("Mark Entire Clip", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// JogForward Command (F)
        /// </summary>
        public static readonly RoutedCommand MarkInCommand = new RoutedCommand("MarkIn", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// JogForward Command (S)
        /// </summary>
        public static readonly RoutedCommand
            MarkOutCommand = new RoutedCommand("MarkOut", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// JogForward Command (F)
        /// </summary>
        public static readonly RoutedCommand GotoMarkInCommand =
            new RoutedCommand("Goto MarkIn", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// JogForward Command (S)
        /// </summary>
        public static readonly RoutedCommand GotoMarkOutCommand =
            new RoutedCommand("Goto MarkOut", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Stop command (C key or STOP button)
        /// </summary>
        public static readonly RoutedCommand StopCommand = new RoutedCommand("Stop", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Play command (PLAY button)
        /// </summary>
        public static readonly RoutedCommand PlayForwardCommand =
            new RoutedCommand("PlayForward", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Play command (PLAY button)
        /// </summary>
        public static readonly RoutedCommand PlayForwardLoopCommand =
            new RoutedCommand("Play Marks Forward and Loop", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Play command (PLAY button)
        /// </summary>
        public static readonly RoutedCommand PlayReverseLoopCommand =
            new RoutedCommand("Play Marks in Reverse and Loop", typeof(MediaPlayerKeyRouter));


        /// <summary>
        /// Play reverse command (PLAY REVERSE button)
        /// </summary>
        public static readonly RoutedCommand PlayReverseCommand =
            new RoutedCommand("PlayReverse", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Toggle play or stop command (Spacebar)
        /// </summary>
        public static readonly RoutedCommand PlayForwardOrStopCommand =
            new RoutedCommand("PlayForwardOrStop", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Play or play faster command (V key)
        /// </summary>
        public static readonly RoutedCommand PlayForwardOrFastForwardCommand =
            new RoutedCommand("PlayForwardOrFastForward", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Play reverse or faster reverse (X key)
        /// </summary>
        public static readonly RoutedCommand PlayReverseOrFastReverseCommand =
            new RoutedCommand("PlayReverseOrFastReverse", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Restore original size
        /// </summary>
        public static readonly RoutedCommand RestoreOriginalSizeCommand =
            new RoutedCommand("Restore to Original Size", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Start the grab
        /// </summary>
        public static readonly RoutedCommand StartImageGrabCommand =
            new RoutedCommand("Start Image Grab", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// End the grab
        /// </summary>
        public static readonly RoutedCommand EndImageGrabCommand =
            new RoutedCommand("End Image Grab", typeof(MediaPlayerKeyRouter));

        /// <summary>
        /// Zoom Command
        /// </summary>
        public static readonly RoutedCommand ZoomToRoiCommand =
            new RoutedCommand("Zoom to Roi", typeof(TestBedViewModel.TestBedViewModel));

        /// <summary>
        /// Reset ROI to full image
        /// </summary>
        public static readonly RoutedCommand ResetRoiCommand =
            new RoutedCommand("Reset Roi", typeof(TestBedViewModel.TestBedViewModel));
        
    }

    public static class ToolProcessCommands
    {
        /// <summary>
        /// Start/stop commands
        /// </summary>
        public static readonly RoutedCommand PreprocessStartCommand =
            new RoutedCommand("Start Preprocess", typeof(Window));

        public static readonly RoutedCommand PreprocessCancelCommand =
            new RoutedCommand("Cancel Preprocess", typeof(Window));

        public static readonly RoutedCommand ProcessStartCommand =
            new RoutedCommand("Start Process", typeof(Window));

        public static readonly RoutedCommand ProcessCancelCommand =
            new RoutedCommand("Cancel Process", typeof(Window));

        public static readonly RoutedCommand ToggleProcessedViewCommand =
            new RoutedCommand("Toggle Processed View", typeof(Window));
    }
}
