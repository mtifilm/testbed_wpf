﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.XPath;
using Microsoft.Win32;
using MtiSimple.Core;
using Newtonsoft.Json;
using TestBed.ManagedApi;
using WpfCTestBed.View;
using WpfCTestBed.ViewModel;

namespace WpfCTestBed
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.MainViewModel = new TestBedViewModel.TestBedViewModel(MediaPlayerControl);
            this.DataContext = this.MainViewModel;
            this.LoadSettings();

            // Media player bindings are handled here
            this.SetCommandBindings(this.CommandBindings);
            this.SetKeyBindings(this.InputBindings);
            MediaPlayerKeyRouter.ActiveMediaPlayerChanged += this.HandleActiveMediaPlayerChangedEvent;
            MediaPlayerKeyRouter.ActiveMediaPlayer = MediaPlayerControl;
        }

        private void SetKeyBindings(InputBindingCollection inputBindings)
        {
            MediaPlayerKeyRouter.SetKeyBindings(this.InputBindings);
        }

        private void SetCommandBindings(CommandBindingCollection commandBindings)
        {
            MediaPlayerKeyRouter.SetCommandBindings(this.CommandBindings);

            commandBindings.Add(new CommandBinding(ToolProcessCommands.ToggleProcessedViewCommand, CommandBinding_OnExecuted,
                CommandBinding_OnCanExecute));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.ZoomToRoiCommand, CommandBinding_OnExecuted,
                    CommandBinding_OnCanExecute));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.ResetRoiCommand, CommandBinding_OnExecuted,
                CommandBinding_OnCanExecute));

            commandBindings.Add(new CommandBinding(ToolProcessCommands.PreprocessStartCommand,
                CommandBinding_OnExecuted));

            commandBindings.Add(new CommandBinding(ToolProcessCommands.PreprocessCancelCommand,
                CommandBinding_OnExecuted));

            commandBindings.Add(new CommandBinding(ToolProcessCommands.ProcessStartCommand,
                CommandBinding_OnExecuted));

            commandBindings.Add(new CommandBinding(ToolProcessCommands.ProcessCancelCommand,
                CommandBinding_OnExecuted));
        }

        /// <summary>
        /// Handles the active media player changed event.
        /// </summary>
        /// <param name="newActiveMediaPlayer">The new active media player.</param>
        private void HandleActiveMediaPlayerChangedEvent(SimplePlayerControl newActiveMediaPlayer)
        {
            if (newActiveMediaPlayer == null)
            {
                return;
            }

            this.MediaPlayerControl = newActiveMediaPlayer;
        }


        private void NewClip_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void NewClip_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // We should do this in designer
            var openFileDialog = new OpenFileDialog
            {
                Title = "Browse to media folder and select any DPX file",
                DefaultExt = "dpx",
                Filter = "DPX files (*.dpx)|*.dpx",
                CheckFileExists = true,
            };

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var pathName = System.IO.Path.GetDirectoryName(openFileDialog.FileName);
                    var simpleClip = SimpleClip.Factory(pathName);

                    var editSimpleClipDialog = new EditSimpleClipDialog(simpleClip, MainViewModel, false);
                    var result = editSimpleClipDialog.ShowDialog();
                    if (result == true)
                    {
                        // We save it here in case the user aborts the program
                        this.MainViewModel.SelectedClip = simpleClip;
                        this.SaveSettings();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"New Clip Failed\n\nError message: {ex.Message}");
                }
            }
        }

        public void SaveSettings()
        {
            Properties.Settings.Default.FormWidth = this.Width;
            Properties.Settings.Default.FormHeight = this.Height;
            this.MainViewModel.SaveSettings();
            Properties.Settings.Default.Save();
        }

        private void LoadSettings()
        {
            this.Width = Properties.Settings.Default.FormWidth;
            this.Height = Properties.Settings.Default.FormHeight;
        }

        public TestBedViewModel.TestBedViewModel MainViewModel { get; set; }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            SaveSettings();
            NativeMethodsTestBed.CloseCache();
        }

        private void EditClip_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(this.MainViewModel?.SelectedClip?.ClipName))
            {
                return;
            }

            var jsonObject = JsonConvert.SerializeObject(this.MainViewModel?.SelectedClip);
            var tempClip = JsonConvert.DeserializeObject<SimpleClip>(jsonObject);

            var editSimpleClipDialog = new EditSimpleClipDialog(tempClip, MainViewModel);
            if (editSimpleClipDialog.ShowDialog() == true)
            {
                this.MainViewModel.ReplaceSimpleClip(this.MainViewModel?.SelectedClip, tempClip);
                this.MainViewModel.SelectedClip = tempClip;
            }
        }

        private void EditClip_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !String.IsNullOrWhiteSpace(this.MainViewModel?.SelectedClip?.ClipName);
        }

        private void DeleteClip_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var clipName = this.MainViewModel?.SelectedClip?.ClipName;
            if (String.IsNullOrWhiteSpace(clipName))
            {
                return;
            }

            var result = MessageBox.Show("Do you want to delete " + clipName, "Delete Clip", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                this.MainViewModel.DeleteClipName(this.MainViewModel?.SelectedClip?.ClipName);
            }
        }

        private void DeleteClip_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !String.IsNullOrWhiteSpace(this.MainViewModel?.SelectedClip?.ClipName);
        }

        private Stopwatch zKeyTimer = new Stopwatch();

        private void Window1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Z) && (zKeyTimer.IsRunning == false))
            {
                if (e.IsRepeat == false)
                {
                    zKeyTimer.Restart();
                    MediaPlayerControl.FrameImage.Cursor = ((FrameworkElement)this.Resources["CursorMagnify"]).Cursor;
                }
            }

            if (e.Key == Key.U)
            {
                if (e.IsRepeat == false)
                {
                    // For now this does nothing
                    SimplePlayerCommands.StartImageGrabCommand.Execute("Start Grab (U)S", MediaPlayerControl);
                    MediaPlayerControl.FrameImage.Cursor = ((FrameworkElement)this.Resources["CursorGrab"]).Cursor;
                }

                e.Handled = true;
            }
        }

        private void Window1_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Z)
            {
                if (zKeyTimer.ElapsedMilliseconds < 250)
                {
                    SimplePlayerCommands.RestoreOriginalSizeCommand.Execute("RestoreSize", MediaPlayerControl);
                }

                if (e.IsRepeat == false)
                {
                    zKeyTimer.Stop();
                    MediaPlayerControl.FrameImage.Cursor = null;
                }
            }


            if (e.Key == Key.U)
            {
                if (e.IsRepeat == false)
                {
                    // For now this does nothing
                    SimplePlayerCommands.EndImageGrabCommand.Execute("End Grab", MediaPlayerControl);
                    e.Handled = true;
                    MediaPlayerControl.FrameImage.Cursor = null;
                }
            }
        }

        private void CommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            // Should not be done this way, move the the MediaPlayerControl
            if (e.Command == SimplePlayerCommands.ZoomToRoiCommand)
            {
                this.MediaPlayerControl.ZoomToRoi();
                return;
            }

            if (e.Command == SimplePlayerCommands.ResetRoiCommand)
            {
                this.MediaPlayerControl.ResetRoi();
                return;
            }

            // TODO: remove user control in viewmodel and fix this
            this.MainViewModel.OnCommandExecuted(sender, e);
        }

        private void CommandBinding_OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {

            if (e.Command == ToolProcessCommands.PreprocessStartCommand)
            {
                this.MainViewModel?.CanExecuteCommand(sender, e);
                return;
            }


            if (e.Command == ToolProcessCommands.PreprocessCancelCommand)
            {
                this.MainViewModel?.CanExecuteCommand(sender, e);
                return;
            }

            if (e.Command == SimplePlayerCommands.ZoomToRoiCommand)
            {
                e.CanExecute = true;
                return;
            }

            if (e.Command == SimplePlayerCommands.ResetRoiCommand)
            {
                e.CanExecute = true;
                return;
            }
        }

        private void Grid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                e.Handled = true;
                MediaPlayerControl.Focus();
            }
        }

        private void Grid_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                e.Handled = true;
            }

        }

        private void OpenFolerExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (MainViewModel?.SelectedClip == null)
            {
                return;
            }

            Process.Start(MainViewModel.SelectedClip.MediaLocation);
        }

        private void OpenFolderCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = MainViewModel?.SelectedClip != null;
        }
    }
}

