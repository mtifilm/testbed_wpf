﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MtiSimple.Core;
using WpfCTestBed.Annotations;
using WpfCTestBed.Model;
using WpfCTestBed.Properties;
using WpfCTestBed.ViewModel;

/*
 * A note on naming locations, mentally append PixelIndex to these 
 *   Screen is the relative display position relative to top left
 *      thus 0,0 is the top left pixel displayed
 *   ScreenImage represents the image displayed on the screen
 *      it is Screen scaled by the ImageSize/Screen size
 *      0,0 is top pixel displayed not first pixel of image
 *      This is the index of the image - image index of the upper left
 *   ImageIndex is the index into the full image ignoring depth
 *      10,20 references the same pixel location in monochrome or RGB
 */
namespace WpfCTestBed.View
{
    /// <summary>
    /// Interaction logic for SimplePlayerControl.xaml
    /// This isn't done correctly as this really is a view and viewmodel 
    /// </summary>
    public partial class SimplePlayerControl : UserControl, INotifyPropertyChanged
    {
        public SimplePlayerControl()
        {
            this.SimplePlayer = new SimplePlayer();
            this.SimplePlayer.PropertyChanged += SimplePlayerOnPropertyChanged;
            InitializeComponent();
            this.LoadSettings();

            // Set the CommandBindings
            this.SetCommandBindings(this.CommandBindings);
            this.SetKeyBindings(this.InputBindings);

            this.PropertyChanged += SimplePlayerControlOnPropertyChanged;
        }
        
        private void SimplePlayerControlOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ProcessRoi":
                    this.DrawProcessRectangle();
                    this.Source.ProcessingRoi = this.ProcessRoi;
                    break;
                case "Y":
                case "X":
                case "Width":
                case "Height":
                    this.DrawProcessRectangle();
                    break;
            }
        }

        private void LoadSettings()
        {
            this.ShowProcessRoi = Settings.Default.ShowProcessRoi;
            this.LockProcessRoi = Settings.Default.LockProcessRoi;
        }

        public SimplePlayer SimplePlayer { get; set; }

        /// <summary>
        /// Dependency for Source
        /// </summary>
        public SimpleClip Source
        {
            get => (SimpleClip)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
                DependencyProperty.Register("Source",
                    typeof(SimpleClip),
                    typeof(SimplePlayerControl),
                    new PropertyMetadata(null, new PropertyChangedCallback(OnSourceChanged)));

        private static void OnSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var simplePlayerControl = d as SimplePlayerControl;
            simplePlayerControl?.InitFromSource(e.NewValue as SimpleClip);
        }

        private void InitFromSource(SimpleClip newClip)
        {
            if (this.SimplePlayer?.Source != null)
            {
                this.SimplePlayer.Source.PropertyChanged -= this.SimplePlayerOnPropertyChanged;
            }

            this.SimplePlayer.Source = newClip;

            if (SimplePlayer.Source != null)
            {
                this.SimplePlayer.Source.PropertyChanged += this.SimplePlayerOnPropertyChanged;
                this.ProcessRoi = newClip.ProcessingRoi;
            }

            this.DrawAll();
        }

        /// <summary>
        /// Dependency for CurrentMouseValues
        /// </summary>
        public static readonly DependencyProperty CurrentPixelValueProperty =
        DependencyProperty.Register("CurrentPixelValue",
        typeof(MtiMouseValues),
        typeof(SimplePlayerControl),
        new PropertyMetadata(null, OnCurrentPixelValueChanged));

        private Point _mouseGrabPoint;
        private MtiRoi<int> _processRoi = new MtiRoi<int>();
        private Point _mouseStretchPoint;
        private bool _stretchingRoiMode;
        private bool _showProcessRoi;
        private bool _lockProcessRoi;

        public MtiMouseValues CurrentPixelValue
        {
            get => (MtiMouseValues)GetValue(CurrentPixelValueProperty);
            set => SetValue(CurrentPixelValueProperty, value);
        }

        private static void OnCurrentPixelValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var simplePlayerControl = d as SimplePlayerControl;
            if (simplePlayerControl == null)
            {
                return;
            }
        }

        public void SetCommandBindings(CommandBindingCollection commandBindings)
        {
            MediaPlayerKeyRouter.SetCommandBindings(commandBindings);
        }

        private void CanExecuteCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void OnCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            //if (e.Command == ZoomToRoiCommand)
            //{

            //}
        }

        public void SetKeyBindings(InputBindingCollection inputBindings)
        {
            MediaPlayerKeyRouter.SetKeyBindings(inputBindings);
        }

        private void UpdatePixelValueDisplay(object sender, MouseEventArgs e)
        {
            if (this.Source == null)
            {
                return;
            }

            // TODO: we need to abstract the source, not assume a writeable bitmap
            var writableBitmap = FrameImage?.Source as WriteableBitmap;
            if (writableBitmap == null)
            {
                return;
            }

            var mousePos = e.GetPosition(FrameImage);
            var screenToBitmapCoordinate = this.ScreenToBitmapCoordinate(mousePos);
            var p = this.ScreenToFullImageCoordinate(mousePos);

            var newValues = new MtiMouseValues()
            {
                Position =new Point(screenToBitmapCoordinate.X + SimplePlayer.ImageRoi.X, screenToBitmapCoordinate.Y + SimplePlayer.ImageRoi.Y)
            };

            writableBitmap.Lock();
            var bb = writableBitmap.BackBuffer;
            var channels = this.Source.ImageDescriptor.Size.Depth;

            if (writableBitmap.Width <= screenToBitmapCoordinate.X || writableBitmap.Height <= screenToBitmapCoordinate.Y)
            {
                var dd = this.ScreenToFullImageCoordinate(mousePos);
                Debug.WriteLine(writableBitmap.Width);
            }

            unsafe
            {
                var pp = bb + (writableBitmap.BackBufferStride * (int)screenToBitmapCoordinate.Y) + (int)screenToBitmapCoordinate.X * 2* channels;
                newValues.R = *(ushort*) (pp);
                if (channels == 1)
                {
                    newValues.G = newValues.R;
                    newValues.B = newValues.R;
                }
                else
                {
                    newValues.G = *(ushort*)(pp + 2);
                    newValues.B = *(ushort*)(pp + 4);
                }
            }

            writableBitmap.Unlock();

            this.CurrentPixelValue = newValues;
        }
        /// <summary>
        /// This is (0,0) in upper right
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private Point ScreenToScreenImageCoordinate(Point p)
        {
            var xScale = (this.SimplePlayer.ImageRoi.Width) / this.FrameImage.ActualWidth;
            var yScale = (this.SimplePlayer.ImageRoi.Height) / this.FrameImage.ActualHeight;

            var x = (int)MtiMath.Clamp(xScale * p.X, 0, FrameImage.Source.Width) - 1;
            var y = (int)MtiMath.Clamp(yScale * p.Y, 0, FrameImage.Source.Height) - 1; ;

            return new Point(x, y);
        }

        private Point ScreenToBitmapCoordinate(Point p)
        {
            // Bitmap and screen start at zero, so no offsets
            var bb = this.FrameImage.Source as WriteableBitmap;
            if (bb == null)
            {
                return new Point(0,0);
            }

            var xScale = (bb.Width) / this.FrameImage.ActualWidth;
            var yScale = (bb.Height) / this.FrameImage.ActualHeight;

            var x = (int)MtiMath.Clamp(xScale * p.X, 0, bb.Width - 1);
            var y = (int)MtiMath.Clamp(yScale * p.Y, 0, bb.Height - 1); ;

            return new Point(x, y);
        }

        public Point ScreenToFullImageCoordinate(Point p)
        {
            var screenToScreenImageCoordinate = ScreenToScreenImageCoordinate(p);
            var x = (int)MtiMath.Clamp(screenToScreenImageCoordinate.X + this.SimplePlayer.ImageRoi.X, 0, SimplePlayer.Source.ImageDescriptor.Size.Width-1);
            var y = (int)MtiMath.Clamp(screenToScreenImageCoordinate.Y + this.SimplePlayer.ImageRoi.Y, 0, SimplePlayer.Source.ImageDescriptor.Size.Height-1);

            return new Point(x, y);
        }

        public Point FullImageToScreenCoordinateEx(Point p)
        {
            // If source is null, just return a 0,0
            if (this.SimplePlayer?.Source == null)
            {
                return new Point(0, 0);
            }

            var xScale = this.FrameImage.ActualWidth / this.SimplePlayer.ImageRoi.Width;
            var yScale = this.FrameImage.ActualHeight / this.SimplePlayer.ImageRoi.Height;

            var x = (p.X - this.SimplePlayer.ImageRoi.X) * xScale;
            var y = (p.Y - this.SimplePlayer.ImageRoi.Y) * yScale;

            return new Point(x, y);
        }
        public Point FullImageToScreenCoordinateEx(MtiPoint<int> p)
        {
            return this.FullImageToScreenCoordinateEx(new Point(p.X, p.Y));
        }

        public bool GrabImageMode { get; set; }

        private void ImageMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Z) == false)
            {
                return;
            }

            // Is 120 fixed, TODO: Find actual value
            // Each delta move by 5%
            var delta = (e.Delta / 120.0f) * 0.1f;
            var percent = Math.Max(0, 1 - delta);
            var fw = SimplePlayer.Source.ImageDescriptor.Size.Width;
            var fh = SimplePlayer.Source.ImageDescriptor.Size.Height;
            var mp = this.ScreenToFullImageCoordinate(e.GetPosition(FrameImage));
            var r = this.SimplePlayer.ImageRoi;
            var w = percent * r.Width;
            var h = percent * r.Height;

            if (w < 0.05 * fw)
            {
                return;
            }

            var x = MtiMath.Clamp( mp.X - percent*(mp.X - r.X), 0, fw - w);
            var y = MtiMath.Clamp( mp.Y - percent*(mp.Y - r.Y), 0, fh - h);

            x = MtiMath.Clamp(x / fw, 0, 1);
            y = MtiMath.Clamp(y / fh, 0, 1);
            w = MtiMath.Clamp(w / fw, 0, 1);
            h = MtiMath.Clamp(h / fh, 0, 1);
            this.SimplePlayer.NormalizedScreenImageRoi = new MtiRect<float>()
                {
                    X = (float)x,
                    Y = (float)y,
                    Width = w,
                    Height = h
                };
        }

        private void FrameImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // See if we are in grab mode
            // TODO: THe grab image mode should be set externally
            if (Keyboard.IsKeyDown(Key.U))
            {
                this.GrabImageMode = true;
                this._mouseGrabPoint = e.GetPosition(FrameImage);
                FrameImage.Cursor = ((FrameworkElement)this.Resources["CursorGrabbing"]).Cursor;
                return;
            }

            // We are stretching a rectangle
            if (this.LockProcessRoi == false  && this.ShowProcessRoi)
            {
                this._mouseStretchPoint = ScreenToFullImageCoordinate(e.GetPosition(FrameImage));
                this._stretchingRoiMode = true;
            }
        }

        private void DrawProcessRectangle()
        {
            if (this.ShowProcessRoi == false)
            {
                return;
            }

            var p = this.ProcessRoi;

            var p1 = FullImageToScreenCoordinateEx(p.TopLeft());
            var p2 = FullImageToScreenCoordinateEx(p.BottomRight());

            Canvas.SetTop(DisplayProcessRectangle, p1.Y);
            Canvas.SetLeft(DisplayProcessRectangle, p1.X);
            DisplayProcessRectangle.Width = Math.Max(0, p2.X - p1.X);
            DisplayProcessRectangle.Height = Math.Max(0, p2.Y - p1.Y);
        }

        private void FrameImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.GrabImageMode)
            {
                this.GrabImageMode = false;
                FrameImage.Cursor = ((FrameworkElement)this.Resources["CursorGrab"]).Cursor;
            }

            if (this._stretchingRoiMode)
            {
                this._stretchingRoiMode = false;
            }
        }

        private void FrameImage_MouseMove(object sender, MouseEventArgs e)
        {
            UpdatePixelValueDisplay(sender, e);

            if (this.GrabImageMode)
            {
                this.MoveGrabImage(e.GetPosition(FrameImage));
                return;
            }

            if (this._stretchingRoiMode)
            {
                var mp = e.GetPosition(FrameImage);
                var imagePoint = ScreenToFullImageCoordinate(mp);
                this.ProcessRoi = new MtiRoi<int>()
                {
                    X = (int) this._mouseStretchPoint.X,
                    Y = (int) this._mouseStretchPoint.Y,
                    Width = (int) (imagePoint.X - this._mouseStretchPoint.X),
                    Height = (int) (imagePoint.Y - this._mouseStretchPoint.Y),
                };
            }
        }

        private void MoveGrabImage(Point newPoint)
        {
            var deltaPoint = newPoint - this._mouseGrabPoint;
            var fw = SimplePlayer.Source.ImageDescriptor.Size.Width;
            var fh = SimplePlayer.Source.ImageDescriptor.Size.Height;

            var r = this.SimplePlayer.ImageRoi;
            var x = MtiMath.Clamp(r.X + deltaPoint.X, 0, fw-r.Width);
            var y = MtiMath.Clamp(r.Y + deltaPoint.Y, 0, fh-r.Height); 

            this.SimplePlayer.NormalizedScreenImageRoi = new MtiRect<float>()
            {
                X = (float)x/fw,
                Y = (float)y/fh,
                Width = SimplePlayer.NormalizedScreenImageRoi.Width,
                Height = SimplePlayer.NormalizedScreenImageRoi.Height
            };

            this._mouseGrabPoint = newPoint;
        }

        // Since a process ROI is clip based, we will use 
        // an ScreenRectangle is in Screen Coordinates and may be out of bounds
        public MtiRoi<int> ProcessRoi
        {
            get => this._processRoi;

            set
            {
                if (this._processRoi != value)
                {
                    // This allows propagation of the properties inside the _processRoi
                    this._processRoi.PropertyChanged -= SimplePlayerControlOnPropertyChanged;
                    this._processRoi = value;
                    this._processRoi.PropertyChanged += SimplePlayerControlOnPropertyChanged;
                    this.OnPropertyChanged();
                }
            }
        }

        public MtiRect<float> ScreenProcessRoi
        {
            get
            {
                var p0 = FullImageToScreenCoordinateEx(
                    new Point(this.ProcessRoi.X, this.ProcessRoi.Y));

                var br = this.ProcessRoi.BottomRight();
                var p1 = FullImageToScreenCoordinateEx(new Point(br.X, br.Y));

                return new MtiRect<float>
                {
                    X = (float)p0.X,
                    Y = (float)p0.Y,
                    Width = (float)(p1.X - p0.X),
                    Height = (float)(p1.Y - p0.Y)
                };
            }
        }

        public bool ShowProcessRoi
        {
            get => this._showProcessRoi;

            set
            {
                if (this._showProcessRoi != value)
                {
                    this._showProcessRoi = value;
                    OnPropertyChanged();
                    this.DrawProcessRectangle();
                }
            }
        }

        public bool LockProcessRoi
        {
            get => this._lockProcessRoi;

            set
            {
                if (this._lockProcessRoi != value)
                {
                    this._lockProcessRoi = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SimplePlayerOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(SimplePlayer.NormalizedScreenImageRoi):
                    this.DrawProcessRectangle();
                    break;

                case nameof(SimpleClip.MarkInTimecode):
                case nameof(SimpleClip.MarkOutTimecode):
                    this.DrawMarks();
                    break;
            }
        }

        private void DrawMarks()
        {
           // HACK This is TEMPORARY, a Timeline needs to be created with binding
           try
           {
               this.MarkInLine.X1 =
                   (this.TimeLine.ActualWidth * (this.Source.MarkInTimecode.Frame - this.Source.ClipInTimecode.Frame)) / this.Source.ClipFrames;
               this.MarkOutLine.X1 =
                   (this.TimeLine.ActualWidth * (this.Source.MarkOutTimecode.Frame - this.Source.ClipInTimecode.Frame)) / this.Source.ClipFrames;
           }
           catch (Exception e)
           {
               Debug.WriteLine(e);
           }
        }

        public void SaveSettings()
        {
            Settings.Default.ShowProcessRoi = this.ShowProcessRoi;
            Settings.Default.LockProcessRoi = this.LockProcessRoi;
        }

        private void FrameImage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.DrawAll();
        }

        private void DrawAll()
        {
            DrawProcessRectangle();
            DrawMarks();
        }

        public void ZoomToRoi()
        {
            this.SimplePlayer.NormalizedScreenImageRoi = new MtiRect<float>()
            {
                X = (float)this.ProcessRoi.X / Source.ImageDescriptor.Size.Width,
                Y = (float)this.ProcessRoi.Y / Source.ImageDescriptor.Size.Height,
                Width = (float)this.ProcessRoi.Width / Source.ImageDescriptor.Size.Width,
                Height = (float)this.ProcessRoi.Height / Source.ImageDescriptor.Size.Height,
            };
        }

        public void ResetRoi()
        {
            if (Source == null)
            {
                return;
            }

            this.ProcessRoi = new MtiRoi<int>()
            {
                X = 0,
                Y = 0,
                Width = Source.ImageDescriptor.Size.Width,
                Height = Source.ImageDescriptor.Size.Height,
            };
        }
    }

}
