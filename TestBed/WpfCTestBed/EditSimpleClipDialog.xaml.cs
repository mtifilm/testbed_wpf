﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfCTestBed.ViewModel;

namespace WpfCTestBed
{
    /// <summary>
    /// Interaction logic for EditSimpleClipDialog.xaml
    /// We should have a viewmodel below but this is crap code
    /// </summary>
    public partial class EditSimpleClipDialog : Window
    {
        public EditSimpleClipDialog(SimpleClip simpleClip, TestBedViewModel.TestBedViewModel testBedViewModel, bool editMode = true)
        {
            _testBedViewModel = testBedViewModel;
            _simpleClip = simpleClip;
            _initialClipName = simpleClip.ClipName;
            _editMode = editMode;

            // Set up the GUI 
            DataContext = simpleClip;
            InitializeComponent();
        }

        private void OkButtonClick(object sender, RoutedEventArgs e)
        {
            if (_simpleClip.ClipName == String.Empty)
            {
                MessageBox.Show("Clip Name cannot be Empty");
                return;
            }

            if (_editMode == false || _initialClipName.ToLower() != _simpleClip.ClipName.ToLower())
            {
                if (_testBedViewModel.SimpleClips.Any(s => s.ClipName.ToLower() == _simpleClip.ClipName.ToLower()))
                {
                    MessageBox.Show("A clip named '" + _simpleClip.ClipName + "' already exists");
                    return;
                }
            }

            if (_editMode == false)
            {
                _testBedViewModel.SimpleClips.Add(_simpleClip);
            }

            DialogResult = true;
        }

        private TestBedViewModel.TestBedViewModel _testBedViewModel;
        private SimpleClip _simpleClip;
        private string _initialClipName;
        private bool _editMode;
    }
}
