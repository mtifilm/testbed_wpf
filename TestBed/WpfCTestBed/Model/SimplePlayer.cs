﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using MtiSimple.Core;
using WpfCTestBed.Annotations;
using WpfCTestBed.ViewModel;

namespace WpfCTestBed.Model
{
    // Because I don't have time to figure out the MediaElement and MemoryStream
    // Not really a model but is (should be) self contained
    public class SimplePlayer : UserControl, INotifyPropertyChanged
    {
        public SimplePlayer()
        {
            this._playOutTimer.Interval = TimeSpan.FromMilliseconds(1);
            this._playOutTimer.Tick += this.TimerTick;
        }

        /// <summary>
        /// Current frame dependency processing
        /// </summary>
        /// 
        public static readonly DependencyProperty CurrentFrameImageProperty =
            DependencyProperty.Register("CurrentFrameImage",
                typeof(WriteableBitmap),
                typeof(SimplePlayer));

        public WriteableBitmap CurrentFrameImage
        {
            get => (WriteableBitmap)GetValue(CurrentFrameImageProperty);
            private set => SetValue(CurrentFrameImageProperty, value);
        }

        private void InitFromClip()
        {
            if (Source == null)
            {
                this.CurrentTimecode = null;
                return;
            }

            // We must have a valid display rectangle
            this.NormalizedScreenImageRoi = new MtiRect<float>()
            {
                X = 0,
                Y = 0,
                Width = 1,
                Height = 1
            };

            // Set current timecode
            this.CurrentTimecode = Source.ClipInTimecode;
        }

        public MtiRect<int> ImageRoi =>
            new MtiRect<int>()
            {
                X = (int) (NormalizedScreenImageRoi.X * Source.ImageDescriptor.Size.Width),
                Y = (int) (NormalizedScreenImageRoi.Y * Source.ImageDescriptor.Size.Height),
                Width = (int) (NormalizedScreenImageRoi.Width * Source.ImageDescriptor.Size.Width),
                Height = (int) (NormalizedScreenImageRoi.Height * Source.ImageDescriptor.Size.Height)
            };

        /// <summary>
        /// Source dependency property
        /// </summary>
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source",
                typeof(SimpleClip),
                typeof(SimplePlayer),
                new PropertyMetadata(null, OnSourceChanged));

        private static void OnSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as SimplePlayer)?.InitFromClip();
        }

        public SimpleClip Source
        {
            get => (SimpleClip)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        /// <summary>
        /// CurrentTimecode Dependency section
        /// </summary>
        public static readonly DependencyProperty CurrentTimecodeProperty =
            DependencyProperty.Register("CurrentTimecode",
                typeof(SimpleTimecode),
                typeof(SimplePlayer),
                new PropertyMetadata(new SimpleTimecode(), OnCurrentTimecodeChanged, OnCoerceValueCallback));

        protected static void OnCurrentTimecodeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var simplePlayer = d as SimplePlayer;
            var frameTimecode = e.NewValue as SimpleTimecode;
            simplePlayer?.DisplayFrame(frameTimecode);
        }

        private static SimpleTimecode OnCoerceValueCallback(DependencyObject d, object basevalue)
        {
            var simplePlayer = d as SimplePlayer;
            if (simplePlayer?.Source == null)
            {
                return basevalue as SimpleTimecode;
            }

            var legalTimecode = simplePlayer?.Source.LegalizeTimecodeToClip(basevalue as SimpleTimecode);
            return legalTimecode;
        }

        public SimpleTimecode CurrentTimecode
        {
            get => (SimpleTimecode)GetValue(CurrentTimecodeProperty);
            set => SetValue(CurrentTimecodeProperty, value);
        }

        /// <summary>
        /// NormalizedDisplayRoi Dependency section
        /// </summary>
        public static readonly DependencyProperty NormalizedScreenImageRoiProperty =
            DependencyProperty.Register("NormalizedScreenImageRoi",
                typeof(MtiRect<float>),
                typeof(SimplePlayer),
                new PropertyMetadata(new MtiRect<float>(), OnNormalizedScreenRoi));

        private static void OnNormalizedScreenRoi(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var simplePlayer = d as SimplePlayer;
            simplePlayer?.DisplayFrame(simplePlayer.CurrentTimecode);
            simplePlayer?.OnPropertyChanged(nameof(NormalizedScreenImageRoi));
            simplePlayer?.OnPropertyChanged(nameof(ImageRoi));
        }

        public MtiRect<float> NormalizedScreenImageRoi
        {
            get => (MtiRect<float>)GetValue(NormalizedScreenImageRoiProperty);
            set => SetValue(NormalizedScreenImageRoiProperty, value);
        }

        private string _fileDisplayed;
        public string FileDisplayed
        {
            get => _fileDisplayed;
            set
            {
                if (_fileDisplayed != value)
                {
                    _fileDisplayed = value;
                    OnPropertyChanged();
                }
            }
        }

        private void DisplayFrame(SimpleTimecode simpleTimecode)
        {
            if (simpleTimecode == null)
            {
                // TO DO CREATE A BLACK FRAME
                this.CurrentFrameImage = null;
                return;
            }

            try
            {
                // Legal timecode may be one past range
                if (simpleTimecode.Frame >= Source.ClipOutTimecode.Frame)
                {
                    this.CurrentFrameImage = this.CreateEndOfFrameBitmap();
                    return;
                }

                this.FileDisplayed = this.Source.FullFileName(simpleTimecode);
                this.CurrentFrameImage = ImageAsBitmap.LoadWriteableBitmap(this.CurrentFrameImage, this.Source.FullFileName(simpleTimecode), this.ImageRoi);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private WriteableBitmap CreateEndOfFrameBitmap()
        {
            int imageWidth = Source.ImageDescriptor.Size.Width;
            int imageHeight = Source.ImageDescriptor.Size.Height;
       
            // Create the Rectangle
            DrawingVisual visual = new DrawingVisual();
            DrawingContext context = visual.RenderOpen();

            // the EM depends on the size 
            var emSize = imageWidth / 20;
            var cornerRadius = imageWidth/24;
            var pixelsPerDip = VisualTreeHelper.GetDpi(this).PixelsPerDip;
            var ft = new FormattedText(
                "End of Clip",
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface("Ariel"),
                emSize,
                Brushes.White,
                pixelsPerDip);

            context.DrawRectangle(Brushes.White, null, new Rect(0, 0, imageWidth, imageHeight));
            context.DrawRoundedRectangle(Brushes.Black, null, new Rect(0,0, imageWidth, imageHeight), cornerRadius, cornerRadius);
            var x = (imageWidth - ft.Width) / 2;
            var y = (imageHeight - ft.Height) / 2;
            context.DrawText(ft, new Point(x, y));
            context.Close();

            // Create the Bitmap and render the rectangle onto it.
            RenderTargetBitmap bmp = new RenderTargetBitmap(imageWidth, imageHeight, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(visual);

            var writableBitmap = new WriteableBitmap(imageWidth, imageHeight, 96, 96, PixelFormats.Pbgra32, null);
            writableBitmap.Lock();
            bmp.CopyPixels(new Int32Rect(0, 0, bmp.PixelWidth, bmp.PixelHeight),
                writableBitmap.BackBuffer,
                writableBitmap.BackBufferStride * writableBitmap.PixelHeight, writableBitmap.BackBufferStride);

            writableBitmap.AddDirtyRect(new Int32Rect(0, 0, imageWidth, imageHeight));
            writableBitmap.Unlock();

            return writableBitmap;
        }

        private void StartTimers()
        {
            this._playOutTimer.Start();
            this._playOutStopwatch.Restart();
            this._pacerStopwatch.Restart();
            this._oldStart = this._pacerStopwatch.ElapsedMilliseconds;
            this.framesPlayed = 0;
        }

        private void StopTimers()
        {
            this._playOutTimer.Stop();
            this._playOutStopwatch.Stop();
        }

        public void HandleMediaPlayerCommand(ICommand command)
        {
            if (command == SimplePlayerCommands.StopCommand)
            {
                this._playingCommand = SimplePlayerCommands.StopCommand;
            }
            else if (command == SimplePlayerCommands.JogForwardCommand)
            {
                JogForward();
            }
            else if (command == SimplePlayerCommands.JogBackwardCommand)
            {
                JogBackward();
            }
            else if (command == SimplePlayerCommands.GotoMarkInCommand)
            {
                this.CurrentTimecode = Source.MarkInTimecode;
            }
            else if (command == SimplePlayerCommands.GotoMarkOutCommand)
            {
                this.CurrentTimecode = Source.MarkOutTimecode;
            }
            else if (command == SimplePlayerCommands.MarkInCommand)
            {
                Source.MarkInTimecode = this.CurrentTimecode;
            }
            else if (command == SimplePlayerCommands.MarkOutCommand)
            {
                Source.MarkOutTimecode = this.CurrentTimecode;
            }
            else if (command == SimplePlayerCommands.MarkEntireClipCommand)
            {
                Source.MarkInTimecode = this.Source.ClipInTimecode;
                Source.MarkOutTimecode = this.Source.ClipOutTimecode;
            }
            else if (command == SimplePlayerCommands.PlayForwardCommand)
            {
                this._playingCommand = SimplePlayerCommands.PlayForwardCommand;
                this.StartTimers();
            }
            else if (command == SimplePlayerCommands.PlayReverseCommand)
            {
                this._playingCommand = SimplePlayerCommands.PlayReverseCommand;
                this.StartTimers();
            }
            else if (command == SimplePlayerCommands.PlayForwardLoopCommand)
            {
                this._playOutTimer.Stop();
                if (Source.MarksAreValid() == false)
                {
                    return;
                }
                this.CurrentTimecode = this.Source.MarkInTimecode;
                this._playingCommand = SimplePlayerCommands.PlayForwardLoopCommand;
                this.StartTimers();
            }
            else if (command == SimplePlayerCommands.PlayReverseLoopCommand)
            {
                this._playOutTimer.Stop();
                this.CurrentTimecode = this.Source.MarkOutTimecode;
                this._playingCommand = SimplePlayerCommands.PlayReverseLoopCommand;
                this.StartTimers();
            }
            else if (command == SimplePlayerCommands.RestoreOriginalSizeCommand)
            {
                this.NormalizedScreenImageRoi = new MtiRect<float>()
                {
                    X = 0,
                    Y = 0,
                    Height = 1,
                    Width = 1
                };
            }
        }

        private readonly DispatcherTimer _playOutTimer = new DispatcherTimer(DispatcherPriority.Render);
        private readonly Stopwatch _playOutStopwatch = new Stopwatch();
        private readonly Stopwatch _pacerStopwatch = new Stopwatch();


        // This represents the playing mode
        private RoutedCommand _playingCommand;
        private int framesPlayed;

        private double _actualSpeed;
        private long _oldStart;

        void TimerTick(object sender, EventArgs e)
        {
            // TODO: we should have a queue of commands here so we don't miss any
            if (_playingCommand == null)
            {
                // We should never get here
                this.StopTimers();
                return;
            }

            // Fake pacer
            var currentStart = this._pacerStopwatch.ElapsedMilliseconds;

            // Fudge to make average go faster
            if ((this._pacerStopwatch.ElapsedMilliseconds - this._oldStart)  < 1000.0/25)
            {
                return;
            }

            _oldStart = currentStart;

            this.framesPlayed++;
            this.ActualSpeed = 1000.0 * this.framesPlayed/ this._playOutStopwatch.ElapsedMilliseconds;
            if (_playingCommand == SimplePlayerCommands.PlayForwardCommand)
            {
                // Playing forward goes until it reaches the end
                this.JogForward();
                if (this.PlayerJustBeforeTimecode(this.Source.ClipOutTimecode))
                {
                    this.StopTimers();
                }
            }
            else if (_playingCommand == SimplePlayerCommands.PlayReverseCommand)
            {
                this.JogBackward();
                if (this.PlayerAtOrBeforeTimecode(this.Source.ClipInTimecode))
                {
                    this.StopTimers();
                }
            }
            else if (_playingCommand == SimplePlayerCommands.PlayForwardLoopCommand)
            {
                // Playing forward goes until it reaches the end
                this.JogForward();
                if (this.PlayerJustBeforeTimecode(this.Source.MarkOutTimecode))
                {
                    this.CurrentTimecode = this.Source.MarkInTimecode;
                }
            }
            else if (_playingCommand == SimplePlayerCommands.PlayReverseLoopCommand)
            {
                this.JogBackward();
                if (this.PlayerAtOrBeforeTimecode(this.Source.MarkInTimecode))
                {
                    this.CurrentTimecode = this.Source.MarkOutTimecode;
                }
            }
            else if (_playingCommand == SimplePlayerCommands.StopCommand)
            {
                this._playingCommand = null;
                this.StopTimers();
            }
        }

        public double ActualSpeed
        {
            get => this._actualSpeed;

            set
            {
                if (this._actualSpeed != value)
                {
                    this._actualSpeed = value;
                    this.OnPropertyChanged();
                }
            }
        }

        private bool PlayerAtOrBeforeTimecode(SimpleTimecode timecode)
        {
            return this.CurrentTimecode.Frame <= timecode.Frame;
        }

        private bool PlayerJustBeforeTimecode(SimpleTimecode timecode)
        {
            return (this.CurrentTimecode.Frame + 1) >= timecode.Frame;
        }

        public void JogForward()
        {
            this.CurrentTimecode = Source.LegalizeTimecodeToClip(new SimpleTimecode(this.CurrentTimecode.Frame + 1, Source.TimecodeDigits));
        }

        public void JogBackward()
        {
            this.CurrentTimecode = Source.LegalizeTimecodeToClip(new SimpleTimecode(this.CurrentTimecode.Frame - 1, Source.TimecodeDigits));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Redraw()
        {
            this.DisplayFrame(this.CurrentTimecode);
        }
    }
}
