﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net.Configuration;
using System.Runtime.CompilerServices;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using MtiSimple.Core;
using TestBed.ManagedApi;

namespace WpfCTestBed.Model
{
    public static class ImageAsBitmap
    {
        public static WriteableBitmap LoadWriteableBitmap(WriteableBitmap currentFrameImage, string fileName, MtiRect<int> imageRoi)
        {
            var imageDescriptor = new SimpleImageDescriptor();

            var status = NativeMethodsTestBed.GetFileInfo(fileName, ref imageDescriptor);
            if (status != 0)
            {
                throw new Exception("Unable to load '" + fileName + "'");
            }

            if (imageDescriptor.Size.Depth == 1)
            {
                return LoadWriteableBitmapMono(currentFrameImage, fileName, imageDescriptor, imageRoi);
            }
            else if (imageDescriptor.Size.Depth == 3)
            {
                return LoadWriteableBitmapRgb(currentFrameImage, fileName, imageDescriptor, imageRoi);
            }
            else if (imageDescriptor.Size.Depth == 4)
            {
                return LoadWriteableBitmapRgbA(currentFrameImage, fileName, imageDescriptor, imageRoi);
            }
            else
            {
                throw new Exception("Only mono or RBG supported");
            }
        }

        private static WriteableBitmap LoadWriteableBitmapMono(WriteableBitmap writeableBitmap, string fileName, SimpleImageDescriptor imageDescriptor, MtiRect<int> imageRoi)
        {
            if (writeableBitmap == null)
            {
                writeableBitmap = CreateBlankWriteableBitmap(imageDescriptor, imageRoi);
            }

            // Check consistency
            if (writeableBitmap.Width != imageRoi.Width ||
                writeableBitmap.Height != imageRoi.Height ||
                writeableBitmap.Format != PixelFormats.Gray16)
            {
                writeableBitmap = CreateBlankWriteableBitmap(imageDescriptor, imageRoi);
            }

            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                writeableBitmap.Lock();
                unsafe
                {
                    // Get a pointer to the back buffer.
                    var bytesPerImageRow = 2 * imageDescriptor.Size.Depth * imageRoi.Width;
                    foreach (var row in Enumerable.Range(0, imageRoi.Height))
                    {
                        // Find the address of the pixel to draw.
                        var imagePtr = imageDescriptor.GetRowPointer(row + imageRoi.Y, imageRoi.X).ToPointer();
                        var backBufferPtr = (writeableBitmap.BackBuffer + row * writeableBitmap.BackBufferStride).ToPointer();
                        Buffer.MemoryCopy(imagePtr, backBufferPtr, bytesPerImageRow, bytesPerImageRow);
                    }
                }

                writeableBitmap.AddDirtyRect(new Int32Rect(0, 0, imageRoi.Width, imageRoi.Height));
            }
            finally
            {
                // Release the back buffer and make it available for display.
                writeableBitmap.Unlock();
            }

            //Debug.WriteLine("Bitmap Time " + sw.ElapsedMilliseconds.ToString());
            return writeableBitmap;
        }

        private static WriteableBitmap LoadWriteableBitmapRgb(WriteableBitmap writeableBitmap, string fileName, SimpleImageDescriptor imageDescriptor, MtiRect<int> imageRoi)
        {
            if (writeableBitmap == null)
            {
                writeableBitmap = CreateBlankWriteableBitmap(imageDescriptor, imageRoi);
            }

            // Check consistency
            if (writeableBitmap.Width != imageRoi.Width ||
                writeableBitmap.Height != imageRoi.Height ||
                writeableBitmap.Format != PixelFormats.Rgb48)
            {
                writeableBitmap = CreateBlankWriteableBitmap(imageDescriptor, imageRoi);
            }

            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                writeableBitmap.Lock();
                unsafe
                {
                    // Get a pointer to the back buffer.
                    var bytesPerImageRow = 2 * imageDescriptor.Size.Depth * imageRoi.Width;
                    foreach (var row in Enumerable.Range(0, imageRoi.Height))
                    {
                        // Find the address of the pixel to draw.
                        var imagePtr = imageDescriptor.GetRowPointer(row + imageRoi.Y, imageRoi.X).ToPointer();
                        var backBufferPtr = (writeableBitmap.BackBuffer + row * writeableBitmap.BackBufferStride).ToPointer();
                        Buffer.MemoryCopy(imagePtr, backBufferPtr, bytesPerImageRow, bytesPerImageRow);
                    }
                }

                writeableBitmap.AddDirtyRect(new Int32Rect(0, 0, imageRoi.Width, imageRoi.Height));
            }
            finally
            {
                // Release the back buffer and make it available for display.
                writeableBitmap.Unlock();
            }

            //Debug.WriteLine("Bitmap Time " + sw.ElapsedMilliseconds.ToString());
            return writeableBitmap;
        }

        private static WriteableBitmap LoadWriteableBitmapRgbA(WriteableBitmap writeableBitmap, string fileName, SimpleImageDescriptor imageDescriptor, MtiRect<int> imageRoi)
        {
            if (writeableBitmap == null)
            {
                writeableBitmap = CreateBlankWriteableBitmap(imageDescriptor, imageRoi);
            }

            // Check consistency
            if (writeableBitmap.Width != imageRoi.Width ||
                writeableBitmap.Height != imageRoi.Height ||
                writeableBitmap.Format != PixelFormats.Rgba64)
            {
                writeableBitmap = CreateBlankWriteableBitmap(imageDescriptor, imageRoi);
            }

            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                writeableBitmap.Lock();
                unsafe
                {
                    // Get a pointer to the back buffer.
                    var bytesPerImageRow = 2 * imageDescriptor.Size.Depth * imageRoi.Width;
                    foreach (var row in Enumerable.Range(0, imageRoi.Height))
                    {
                        // Find the address of the pixel to draw.
                        var imagePtr = imageDescriptor.GetRowPointer(row + imageRoi.Y, imageRoi.X).ToPointer();
                        var backBufferPtr = (writeableBitmap.BackBuffer + row * writeableBitmap.BackBufferStride).ToPointer();
                        Buffer.MemoryCopy(imagePtr, backBufferPtr, bytesPerImageRow, bytesPerImageRow);
                    }
                }

                writeableBitmap.AddDirtyRect(new Int32Rect(0, 0, imageRoi.Width, imageRoi.Height));
            }
            finally
            {
                // Release the back buffer and make it available for display.
                writeableBitmap.Unlock();
            }

            //Debug.WriteLine("Bitmap Time " + sw.ElapsedMilliseconds.ToString());
            return writeableBitmap;
        }



        private static WriteableBitmap CreateBlankWriteableBitmap(SimpleImageDescriptor imageDescriptor, MtiRect<int> imageRoi)
        {
            PixelFormat pixelFormat;

            switch (imageDescriptor.Size.Depth)
            {
                case 1:
                    pixelFormat = PixelFormats.Gray16;
                        break;

                case 3:
                    pixelFormat = PixelFormats.Rgb48;
                    break;

                case 4:
                    pixelFormat = PixelFormats.Rgba64;
                    break;

                default:
                    throw new Exception("Unsupported pixel format");
            }

            return new WriteableBitmap(
                imageRoi.Width,
                imageRoi.Height,
                96,
                96,
                pixelFormat,
                null);
        }
    }
}
