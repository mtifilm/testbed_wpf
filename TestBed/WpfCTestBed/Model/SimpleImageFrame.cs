﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfCTestBed.Model
{
    // This just contains some information about the DPX file
    // Used to pass into C interface
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct SimpleImageFrame
    {
        public Int32 Width;
        public Int32 Height;
        public Int32 Depth;
        public Int32 X;
        public Int32 Y;
        public Int32 RoiWidth;
        public Int32 RoiHeight;
    }
}
