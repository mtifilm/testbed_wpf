﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Xml.XPath;
using MtiSimple.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TestBed.ManagedApi;
using WpfCTestBed.Annotations;
using WpfCTestBed.Model;

namespace WpfCTestBed.ViewModel
{
    // This contains a few items that define a clip
    public class SimpleClip : BindableObject
    {
        // There are 6 timecode to a clip.
        // The first are the start of the DPX file sequence (MediaStartTimecode)
        // The second is the total number of DPX files (TotalMediaFiles)
        // The next two define a clip
        //   The ClipInTimecode, where the clip starts
        //   The ClipFrames, which is the total number of frames in clip, and must be >= 0
        // The ClipFrames + clipStartTimecode never exceeds the last frame timecode
        public SimpleClip()
        {
        }

        public static SimpleClip Factory(string mediaLocation)
        {
            SimpleClip clip = new SimpleClip { MediaLocation = mediaLocation };

            // Find all DPX files
            // Our media is just a list of file names
            clip.FileNames = SimpleClip.ReadMedia(clip.MediaLocation);

            // No files found, this is an error, clips can be empty but not media
            clip.TotalMediaFiles = clip.FileNames.Length;
            if (clip.TotalMediaFiles == 0)
            {
                throw new Exception("No DPX files found");
            }

            var rx = new Regex(@"(.*?)(\d+)\D*\.dpx$", RegexOptions.IgnoreCase);
            var matches = rx.Matches(clip.FileNames.First());
            if (matches.Count != 1)
            {
                throw new Exception("Could not find time code, files must be of the form <TEXT><nnnn>.dpx");
            }

            if (matches[0].Success == false)
            {
                throw new Exception("Empty time code, files must be of the form <TEXT><nnnn>.dpx");
            }

            // Create a name, make sure its unique
            // We should really use a regex to create name but this is just non-production 90%
            clip.ClipName = matches[0].Groups[1].Value.Trim('_');
            clip.ClipName = matches[0].Groups[1].Value.Trim('.');

            if (clip.ClipName == String.Empty)
            {
                clip.ClipName = Path.GetFileName(clip.MediaLocation);
            }

            //  Set start
            var value = matches[0].Groups[2].Value;
            clip.TimecodeDigits = value.Length;
            clip.MediaInTimecode = new SimpleTimecode(value, clip.TimecodeDigits);

            // To avoid someone messing with SimpleTimecode class
            Debug.Assert(clip.TimecodeDigits == clip.MediaInTimecode.Precision);

            // Now end
            // WARNING: We really should check if we have a true sequence
            // But that requires a range class.
            matches = rx.Matches(clip.FileNames.Last());
            var endMedia = Int32.Parse(matches[0].Groups[2].Value) + 1;
            clip.MediaOutTimecode = new SimpleTimecode(endMedia, clip.TimecodeDigits);

            // Default is to make clip entire media
            // WARNING must set backing variable here.
            clip.ClipInTimecode = clip.MediaInTimecode;
            clip.ClipOutTimecode = clip.MediaOutTimecode;
            clip.MarkInTimecode = clip.ClipInTimecode;
            clip.MarkOutTimecode = clip.ClipOutTimecode;

            // Now read first file and set format
            var simpleImageDescriptor = new SimpleImageDescriptor();
            var status = NativeMethodsTestBed.GetFileInfo(clip.FullFileName(0), ref simpleImageDescriptor);
            if (status != 0)
            {
                throw new Exception("Error reading DPX file format, status = " + status.ToString());
            }

            clip.ImageDescriptor = simpleImageDescriptor;

            clip.ProcessingRoi = new MtiRoi<int>()
            { X = 0, Y = 0, Width = clip.ImageDescriptor.Size.Width, Height = clip.ImageDescriptor.Size.Height };
            return clip;
        }

        public String FullFileName(int i)
        {
            if (this.FileNames == null)
            {
                this.FileNames = ReadMedia(this.MediaLocation);
            }

            return Path.Combine(MediaLocation, this.FileNames[i]);
        }

        public String FullFileName(SimpleTimecode frameTimecode)
        {
            if (frameTimecode == null)
            {
                return String.Empty;
            }

            // This is an on demand media load
            if (this.FileNames == null)
            {
                this.FileNames = ReadMedia(this.MediaLocation);
            }

            var legalTimecode = LegalizeTimecodeToMedia(frameTimecode);
            var index = Math.Max(0, legalTimecode.Frame - this.MediaInTimecode.Frame);
            return Path.Combine(MediaLocation, this.FileNames[index]);
        }

        private static string[] ReadMedia(string mediaLocation)
        {
            // Find all DPX files
            var names = Directory.EnumerateFiles(mediaLocation)
                .Select(p => Path.GetFileName(p))
                .Where(fn => Path.GetExtension(fn).ToLower() == ".dpx")
                .OrderBy(x => x)
                .ToArray();

            return names;
        }

        public string ClipName { get; set; }

        [JsonProperty]
        public string MediaLocation { get; private set; }

        [JsonIgnore]
        public String[] FileNames { get; set; }

        [JsonProperty]
        public SimpleImageDescriptor ImageDescriptor { get; private set; }

        [JsonIgnore]
        public SimpleTimecode ClipInTimecode
        {
            get => _clipInTimecode;

            set
            {
                if (MediaInTimecode == null)
                {
                    _clipInTimecode = value;
                    return;
                }

                _clipInTimecode = LegalizeTimecodeToMedia(value);
                this.RaisePropertyChanged();
                this.RaisePropertyChanged(nameof(ClipFrames));
            }
        }

        [JsonIgnore]
        public SimpleTimecode ClipOutTimecode
        {
            get => _clipOutTimecode;

            set
            {
                _clipOutTimecode = LegalizeTimecodeToMedia(value);
                this.RaisePropertyChanged();
                this.RaisePropertyChanged(nameof(ClipFrames));
            }
        }

        [JsonIgnore]
        public SimpleTimecode MarkInTimecode
        {
            get => _markInTimecode;

            set
            {
                // TODO: Make SimpleTimecode equable
                if (_markInTimecode?.Frame != value.Frame)
                {
                    _markInTimecode = LegalizeTimecodeToClip(value);
                    this.RaisePropertyChanged();
                    this.RaisePropertyChanged(nameof(MarkedFrames));
                }
            }
        }

        [JsonIgnore]
        public SimpleTimecode MarkOutTimecode
        {
            get => _markOutTimecode;

            set
            {
                // TODO: Make SimpleTimecode equable
                if (_markOutTimecode?.Frame != value.Frame)
                {
                    _markOutTimecode = LegalizeTimecodeToClip(value);
                    this.RaisePropertyChanged();
                    this.RaisePropertyChanged(nameof(MarkedFrames));
                }
            }
        }

        [JsonProperty]
        public int TimecodeDigits { get; private set; }

        [JsonProperty]
        // This is really this.MediaOutTimecode.Frame - this.MediaInTimecode.Frame;
        // But we keep it as a property because we might want to check for consistency on a clip deserialization
        public int TotalMediaFiles { get; private set; }

        [JsonIgnore]
        public SimpleTimecode MediaInTimecode
        {
            get => this._mediaInTimecode;
            private set
            {
                if (_mediaInTimecode == value)
                {
                    return;
                }

                _mediaInTimecode = new SimpleTimecode(value.Frame, this.TimecodeDigits);
                this.RaisePropertyChanged();
            }
        }

        [JsonIgnore]
        public SimpleTimecode MediaOutTimecode
        {
            get => this._mediaOutTimecode;
            private set
            {
                if (_mediaOutTimecode == value)
                {
                    return;
                }

                _mediaOutTimecode = new SimpleTimecode(value.Frame, this.TimecodeDigits);
                this.RaisePropertyChanged();
            }
        }

        [JsonIgnore]
        public int ClipFrames => this.ClipOutTimecode.Frame - this.ClipInTimecode.Frame;

        public int? MarkedFrames
        {
            get
            {
                if (this.MarksAreValid() == false)
                {
                    return null;
                }

                return this.MarkOutTimecode.Frame - this.MarkInTimecode.Frame;
            }
        }

        [JsonProperty]
        public MtiRoi<int> ProcessingRoi { get; set; } = new MtiRoi<int>();

        [JsonProperty]
        private SimpleTimecode _clipInTimecode;

        [JsonProperty]
        private SimpleTimecode _clipOutTimecode;

        [JsonProperty]
        private SimpleTimecode _mediaInTimecode;

        [JsonProperty]
        private SimpleTimecode _mediaOutTimecode;

        [JsonProperty]
        private SimpleTimecode _markInTimecode;

        [JsonProperty]
        private SimpleTimecode _markOutTimecode;

        /// <summary>
        /// This takes a string and try to make a legal timecode out of it
        /// If the string is well formed, a timecode between the in and out is returned
        /// If not, a empty string is returned
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public SimpleTimecode LegalizeTimecodeToMedia(SimpleTimecode value)
        {
            if (value == null)
            {
                return null;
            }

            var result = value.Frame;
            var legal = Math.Min(Math.Max(result, this.MediaInTimecode.Frame), this.MediaOutTimecode.Frame + 1);
            return new SimpleTimecode(legal, this.TimecodeDigits);
        }

        public SimpleTimecode LegalizeTimecodeToClip(SimpleTimecode value)
        {
            if (value == null)
            {
                return null;
            }

            var result = value.Frame;
            var legal = Math.Min(Math.Max(result, this.ClipInTimecode.Frame), this.ClipOutTimecode.Frame + 1);
            return new SimpleTimecode(legal, this.TimecodeDigits);
        }

        public bool MarksAreValid()
        {
            return this.MarkInTimecode?.Frame < this.MarkOutTimecode?.Frame;
        }

        public SimpleClip Copy()
        {
            return (SimpleClip)this.MemberwiseClone();
        }

        public string FullFileNameFromMarkIn(int i)
        {
            return FullFileName(this.MarkInTimecode.Frame - this.MediaInTimecode.Frame + i);
        }

        public string[] GetMarkedFileNames()
        {
            List<string> names = new List<string>();
            foreach (var i in Enumerable.Range(0, this.MarkedFrames.Value))
            {
                names.Add(this.FullFileNameFromMarkIn(i));
            }

            return names.ToArray();
        }
    }
}

