#include "CacheFileReader.h"
#include "../DpxToMatlab/DpxSequenceReader.h"

SimpleImageFrame CacheFileReader::getInputFrame(const std::string& fileKey)
{
	const auto entry = _inputCache.find(fileKey);
	if (entry != _inputCache.end())
	{
		return entry->second;
	}

	// Read it
	auto[ippArray, bits] = DevIO::readDpxFile(fileKey);
	auto[rgbArray, alphaArray] = ippArray.splitAlpha();
	_inputCache[fileKey] = { rgbArray, alphaArray };

	return _inputCache[fileKey];
}

SimpleImageFrame CacheFileReader::getInputFrame(const std::string& fileKey, const MtiRect &roi)
{
	const auto imageFrame = getInputFrame(fileKey);
	return { imageFrame.image(roi, MtiSelectMode::intersect), imageFrame.alpha(roi,  MtiSelectMode::intersect) };
}

SimpleImageFrame CacheFileReader::getOutputFrame(const std::string& fileKey)
{
	const auto outputEntry = _outputCache.find(fileKey);
	if (outputEntry != _outputCache.end())
	{
		return outputEntry->second;
	}

	return { Ipp16uArray(), Ipp16uArray() };
}

SimpleImageFrame CacheFileReader::getOutputFrame(const std::string& fileKey, const MtiRect &roi)
{
	const auto imageFrame = getOutputFrame(fileKey);
	return { imageFrame.image(roi, MtiSelectMode::intersect), imageFrame.alpha(roi, MtiSelectMode::intersect) };
}


SimpleImageFrame CacheFileReader::getDisplayFrame(const std::string& fileKey)
{
	// This dup code avoids a few empty allocation, is it worth it?
	if (_previewMode)
	{
		const auto outputEntry = _outputCache.find(fileKey);
		if (outputEntry != _outputCache.end())
		{
			return outputEntry->second;
		}
	}

	return getInputFrame(fileKey);
}

Ipp16uArray CacheFileReader::getDisplayImage(const std::string& fileKey)
{
	return getDisplayFrame(fileKey).image;
}

// GetFile either does input or processed, this get only the input(clip) file
Ipp16uArray CacheFileReader::getInputImage(const std::string& fileKey)
{
	return getDisplayFrame(fileKey).image;
}

Ipp16uArray CacheFileReader::getInputImage(const std::string& fileKey, const MtiRect &roi)
{
	return getInputImage(fileKey)(roi);
}


Ipp16uArray CacheFileReader::getDisplayImage(const std::string& fileKey, const MtiRect &roi)
{
	return getDisplayImage(fileKey)(roi);
}

// For now this doesn't do much
int CacheFileReader::initCache()
{
	return clearCache();
}

int CacheFileReader::clearCache()
{
	_inputCache.clear();
	_outputCache.clear();
	return 0;
}

void CacheFileReader::setPreviewMode(bool previewMode)
{
	_previewMode = previewMode;
}

void CacheFileReader::ClearOutputCache()
{
	_outputCache.clear();
}

void CacheFileReader::setOutputFile(const string& fileKey, const Ipp16uArray& outputArray)
{
	_outputCache[fileKey] = { outputArray, getInputAlpha(fileKey) };
}

Ipp16uArray CacheFileReader::getOutputImage(const string& fileKey)
{
	return getOutputFrame(fileKey).image;
}

Ipp16uArray CacheFileReader::getOutputAlpha(const string& fileKey)
{
	return getOutputFrame(fileKey).alpha;
}

Ipp16uArray CacheFileReader::getInputAlpha(const string& fileKey)
{
	return getInputFrame(fileKey).alpha;
}

