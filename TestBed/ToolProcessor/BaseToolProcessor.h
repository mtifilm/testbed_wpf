#pragma once

#include  "ToolProcessorInt.h"
#include "IppHeaders.h"
#include <map>
#include "IpaStripeStream.h"
#include "SimpleImageFrame.h"

struct SimpleImageFrame;

// Note:  This is NOT the way the tools will be defined in SuperNova
// but it makes code development easier because the preprocess data
// can easier be shared.
// This was ONLY done for expediency. 
class MTI_TOOL_PROCESSOR_API BaseToolProcessor
{
public:
	BaseToolProcessor();
	virtual ~BaseToolProcessor();

	SimpleImageFrame getInputImageFrameFromIndex(int relativeIndex);

	// Global init, not used
	virtual int initToolProcessing(const Ipp16uArray& frameArray, int frames);

	// Preprocessing section, this is read only for files
	virtual int initPreprocessing(int pass);
	virtual int CancelPreprocessingAsync();
	virtual int preprocessFrame(int pass, const std::string& fileName, const SimpleImageFrame& imageFrame,
		int currentIndex);
	virtual int preprocessingCompleted(int pass);

	// Processing section, this produces new output arrays
	virtual int initProcessing(int frames);
	virtual int cancelProcessingAsync();
	virtual int processFrame(const std::string& fileName, const SimpleImageFrame& imageFrame,
		int currentIndex);
	virtual int processingCompleted();

	[[nodiscard]] int getTotalFrames() const { return _frames; }
	[[nodiscard]] MtiSize getBaseImageSize() const { return _size; }
	[[nodiscard]] MtiSize getProcessingSize() const { return _processingRoi.getSize(); }
	[[nodiscard]] int getCurrentIndex() const { return _currentIndex; }
	[[nodiscard]] bool getCancellationPending() const { return _cancellationPending; }
	void setCancellationPending(bool value) { _cancellationPending = value; }

	void saveOutputFrame(const string &fileKey, const Ipp16uArray &frameData) const;
	[[nodiscard]] static Ipp16uArray getOutputFrame(const string &fileName);

	// Note we need to move these to protected by fixing calling methods
		// Global init, not used
	int initToolProcessingBase(const Ipp16uArray& frameArray, int frames);

	// preprocessing section
	int initPreprocessingBase(int pass, const vector<string>& fileNames, const MtiRect& roi);
	int preprocessFrameBase(int pass, const std::string& fileName, const SimpleImageFrame& imageFrame, int currentIndex);
	int preprocessingCompletedBase(int pass);
	int cancelPreprocessingAsyncBase();

	// processing section;
	int initProcessingBase(const vector<string>& fileNames, const MtiRect& roi, bool saveFrames, string &&subfolder);
	int processFrameBase(const std::string& fileName, const SimpleImageFrame& imageFrame, int currentIndex);
	int processingCompletedBase();
	int cancelProcessingAsyncBase();

private:
	int _frames = 0;
	vector<string> _fileNames;
	MtiSize _size;
	MtiRect _processingRoi;
	int _currentIndex = 0;
	bool _cancellationPending = false;
	bool _saveFrames;
	string _subFolder;
};

