#pragma once
#include "IppHeaders.h"

// Contains just the RGB (or mono) image and an alpha channel
// There is little use in templating this
struct SimpleImageFrame
{
	Ipp16uArray image;
	Ipp16uArray alpha;
};

