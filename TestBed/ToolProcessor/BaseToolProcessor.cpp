#include "stdafx.h"
#include "BaseToolProcessor.h"
#include "CacheFileReader.h"
#include "filesystem"
#include "DevIO.h"

BaseToolProcessor::BaseToolProcessor()
{
}


BaseToolProcessor::~BaseToolProcessor()
{
}

int BaseToolProcessor::initToolProcessing(const Ipp16uArray& frameArray, int frames)
{
	return 0;
}

int BaseToolProcessor::initPreprocessing(int pass)
{
	return 0;
}

int BaseToolProcessor::CancelPreprocessingAsync()
{
	return 0;
}

int BaseToolProcessor::preprocessFrame(int pass, const std::string& fileName, const SimpleImageFrame& imageFrame,
                                       int currentIndex)
{
	return 0;
}

int BaseToolProcessor::preprocessingCompleted(int pass)
{
  return 0;
}

int BaseToolProcessor::preprocessFrameBase(int pass, const std::string& fileName, const SimpleImageFrame& imageFrame,
                                           int currentIndex)
{
	_currentIndex = currentIndex;
	return preprocessFrame(pass, fileName, imageFrame, currentIndex);
}

int BaseToolProcessor::preprocessingCompletedBase(int pass)
{
	return preprocessingCompleted(pass);
}

int BaseToolProcessor::initProcessingBase(const vector<string>& fileNames, const MtiRect& roi, bool saveFrames, string &&subfolder)
{
	_frames = int(fileNames.size());
	_fileNames = fileNames;
	_processingRoi = roi;
	CacheFileReader::getInstance()->ClearOutputCache();
	const auto simpleFrameImage = getInputImageFrameFromIndex(0);
	_size = simpleFrameImage.image.getSize();

	_saveFrames = saveFrames;
	_subFolder = subfolder;
	return initProcessing(_frames);
}

int BaseToolProcessor::processFrameBase(const std::string& fileName, const SimpleImageFrame& imageFrame, int currentIndex)
{
	_currentIndex = currentIndex;
	return processFrame(fileName, imageFrame, currentIndex);
}

int BaseToolProcessor::processingCompletedBase()
{
	return processingCompleted();
}

int BaseToolProcessor::cancelProcessingAsyncBase()
{
	setCancellationPending(true);
	return cancelProcessingAsync();
}

// This returns the array of size processing region
SimpleImageFrame BaseToolProcessor::getInputImageFrameFromIndex(int relativeIndex)
{
	// Return empty array if nothing
	if ((relativeIndex < 0) || relativeIndex >= int(_fileNames.size()))
	{
		return { Ipp16uArray(), Ipp16uArray() };
	}
	
	return CacheFileReader::getInstance()->getInputFrame(_fileNames[relativeIndex], _processingRoi);
}

int BaseToolProcessor::initPreprocessingBase(int pass, const vector<string>& fileNames, const MtiRect& roi)
{
	_frames = int(fileNames.size());
	_fileNames = fileNames;
	_processingRoi = roi;
	CacheFileReader::getInstance()->ClearOutputCache();
	const auto frameArray = getInputImageFrameFromIndex(0);
	_size = frameArray.image.getSize();
	return initPreprocessing(pass);
}

int BaseToolProcessor::processFrame(const std::string& fileName, const SimpleImageFrame& imageFrame,
                                    int currentIndex)
{
	return 0;
}

int BaseToolProcessor::processingCompleted()
{
	return 0;
}

void BaseToolProcessor::saveOutputFrame(const string &fileKey, const Ipp16uArray& frameData) const
{
	// The processed array must be of same depth as the output
	// This is very inefficient and unstable as it assumes all files are the same
	const auto inputFrame = CacheFileReader::getInstance()->getInputFrame(fileKey);
	const auto rgbOutputArray = inputFrame.image.duplicate();
	rgbOutputArray(_processingRoi) <<= frameData;
	
	CacheFileReader::getInstance()->setOutputFile(fileKey, rgbOutputArray);

	// We use a global save flag here because in a true cache output files MUST be saved
	// But in the testbed it is too slow
	if (_saveFrames == false)
	{
		return;
	}
	
	// Ugly code to find output from file key, that is the name
	const auto pos = fileKey.find_last_of('\\');
	const string folder = fileKey.substr(0, pos + 1) + _subFolder;
	const string slashPlusFilename = fileKey.substr(pos, fileKey.size());

	const auto alpha = CacheFileReader::getInstance()->getInputAlpha(fileKey);
	auto outputArray = rgbOutputArray;
	if (alpha.isEmpty() == false)
	{
		outputArray = rgbOutputArray.addAlpha(alpha);
	}
	
	// Create folder if folder doesn't exist
	std::filesystem::create_directory(folder);
	const string outFileName = folder + slashPlusFilename;
	DevIO::writeDpxFile(fileKey, outputArray, outFileName);
}

Ipp16uArray BaseToolProcessor::getOutputFrame(const string &fileName)
{
	return CacheFileReader::getInstance()->getOutputImage(fileName);
}

int BaseToolProcessor::cancelProcessingAsync()
{
	return 0;
}

int BaseToolProcessor::initProcessing(int frames)
{
	return 0;
}

int BaseToolProcessor::initToolProcessingBase(const Ipp16uArray& frameArray, int frames)
{
	_frames = frames;
	return initToolProcessing(frameArray, frames);
}

int BaseToolProcessor::cancelPreprocessingAsyncBase()
{
	setCancellationPending(true);
	return CancelPreprocessingAsync();
}