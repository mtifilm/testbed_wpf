﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FocusBehavior.cs" company="MTI Film LLC">
// Copyright (c) 2011 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>rancor\pfirth</author>
// <date>10/13/2011 1:22:15 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.Windows;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;

    /// <summary>
    /// Sets up focusing behavior.
    /// </summary>
    public static class FocusBehavior
    {
        /// <summary>
        /// The click keyboard focus dependency property.
        /// </summary>
        public static readonly DependencyProperty ClickKeyboardFocusTargetProperty =
            DependencyProperty.RegisterAttached(
            "ClickKeyboardFocusTarget", 
            typeof(IInputElement), 
            typeof(FocusBehavior),
            new PropertyMetadata(OnClickKeyboardFocusTargetChanged));

        /// <summary>
        /// Dependency property to set focus based on some variable.
        /// </summary>
        public static readonly DependencyProperty IsFocusedProperty =
         DependencyProperty.RegisterAttached(
          "IsFocused", 
          typeof(bool),
          typeof(FocusBehavior),
          new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

        /// <summary>
        /// Gets the click keyboard focus target.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The focus target.</returns>
        public static IInputElement GetClickKeyboardFocusTarget(DependencyObject obj)
        {
            return (IInputElement)obj.GetValue(ClickKeyboardFocusTargetProperty);
        }

        /// <summary>
        /// Sets the click keyboard focus target.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetClickKeyboardFocusTarget(DependencyObject obj, IInputElement value)
        {
            obj.SetValue(ClickKeyboardFocusTargetProperty, value);
        }

        /// <summary>
        /// Gets the is focused.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>Is focused property.</returns>
        public static bool GetIsFocused(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsFocusedProperty);
        }

        /// <summary>
        /// Sets the is focused.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetIsFocused(DependencyObject obj, bool value)
        {
            obj.SetValue(IsFocusedProperty, value);
        }

        /// <summary>
        /// Called when [click keyboard focus target changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnClickKeyboardFocusTargetChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var button = sender as ButtonBase;
            if (button == null)
            {
                return;
            }

            if (e.OldValue == null && e.NewValue != null)
            {
                button.Click += OnButtonClick;
            }
            else if (e.OldValue != null && e.NewValue == null)
            {
                button.Click -= OnButtonClick;
            }
        }

        /// <summary>
        /// Called when [button click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void OnButtonClick(object sender, RoutedEventArgs e)
        {
            var target = GetClickKeyboardFocusTarget((ButtonBase)sender);
            Keyboard.Focus(target);
        }

        /// <summary>
        /// Called when [is focused property changed].
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnIsFocusedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var uie = (UIElement)d;
            if ((bool)e.NewValue)
            {
                uie.Focus(); // Don't care about false values.
            }
        }
    }
}
