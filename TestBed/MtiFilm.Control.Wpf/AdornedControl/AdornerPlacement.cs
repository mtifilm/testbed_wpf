﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdornerPlacement.cs" company="MTI Film LLC">
// Copyright (c) 2016 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>1/7/2016 3:24:52 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf.AdornedControl
{
    /// <summary>
    /// Enum that defines the adorner placement.
    /// </summary>
    public enum AdornerPlacement
    {
        /// <summary>
        /// The inside placement.
        /// </summary>
        Inside,

        /// <summary>
        /// The outside placement.
        /// </summary>
        Outside
    }
}
