﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Animations.cs" company="MTI Film, LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>12/7/2012 12:09:36 PM</date>
// <summary>Various UIElement Animations.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Windows;
    using System.Windows.Media.Animation;

    /// <summary>
    /// Various UIElement Animations.
    /// </summary>
    public class Animations
    {
        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Fades out the UIElement.
        /// </summary>
        /// <param name="uiElement">The UI element.</param>
        public static void FadeOut(UIElement uiElement)
        {
            uiElement.Visibility = Visibility.Visible;
            AnimateOpacity(uiElement, 3, 1, false);
        }

        /// <summary>
        /// Animates the specified UIElement by changing the opacity.
        /// </summary>
        /// <param name="uiElement">The UI element.</param>
        /// <param name="seconds">The seconds.</param>
        /// <param name="repeats">The number of repeats.</param>
        /// <param name="autoReverse">if set to <c>true</c> [auto reverse].</param>
        /// <remarks>
        /// We go from 2 to 0 because starting at 2 means it stays at full opacity for half of the time.
        /// </remarks>
        public static void AnimateOpacity(UIElement uiElement, double seconds, int repeats, bool autoReverse)
        {
            var doubleAnimation = new DoubleAnimation
            {
                From = 2,
                To = 0,
                Duration = new Duration(TimeSpan.FromSeconds(seconds)),
                AutoReverse = autoReverse,
                RepeatBehavior = new RepeatBehavior(repeats)
            };

            uiElement.BeginAnimation(UIElement.OpacityProperty, doubleAnimation);
        }

        #endregion
    }
}
