﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GridViewSort.cs" company="MTI Film LLC">
// Copyright (c) 2013 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>12/19/2013 2:07:21 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

using MtiSimple.Core;

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Class that allows for grid view sorting by clicking on column headers.
    /// Taken from: http://www.thomaslevesque.com/2009/03/27/wpf-automatically-sort-a-gridview-when-a-column-header-is-clicked/
    /// </summary>
    public class GridViewSort
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The command property
        /// </summary>
        public static readonly DependencyProperty CommandProperty = DependencyProperty.RegisterAttached(
            "Command",
            typeof(ICommand),
            typeof(GridViewSort),
            new UIPropertyMetadata(
                null,
                (o, e) =>
                {
                    var itemsControl = o as ItemsControl;
                    if (itemsControl == null || GetAutoSort(itemsControl))
                    {
                        // Don't change click handler if AutoSort enabled
                        return;
                    }

                    if (e.OldValue != null && e.NewValue == null)
                    {
                        itemsControl.RemoveHandler(ButtonBase.ClickEvent, new RoutedEventHandler(HandleColumnHeaderClick));
                    }

                    if (e.OldValue == null && e.NewValue != null)
                    {
                        itemsControl.AddHandler(ButtonBase.ClickEvent, new RoutedEventHandler(HandleColumnHeaderClick));
                    }
                }));

        /// <summary>
        /// The auto sort property
        /// </summary>
        public static readonly DependencyProperty AutoSortProperty = DependencyProperty.RegisterAttached(
            "AutoSort",
            typeof(bool),
            typeof(GridViewSort),
            new UIPropertyMetadata(
                false,
                (o, e) =>
                {
                    var listView = o as ListView;
                    if (listView == null || GetCommand(listView) != null)
                    {
                        return;
                    }

                    var oldValue = (bool)e.OldValue;
                    var newValue = (bool)e.NewValue;
                    if (oldValue && !newValue)
                    {
                        listView.RemoveHandler(ButtonBase.ClickEvent, new RoutedEventHandler(HandleColumnHeaderClick));
                    }

                    if (!oldValue && newValue)
                    {
                        listView.AddHandler(ButtonBase.ClickEvent, new RoutedEventHandler(HandleColumnHeaderClick));
                    }
                }));

        /// <summary>
        /// The property name property
        /// </summary>
        public static readonly DependencyProperty PropertyNameProperty = DependencyProperty.RegisterAttached(
            "PropertyName",
            typeof(string),
            typeof(GridViewSort),
            new UIPropertyMetadata(null));

        /// <summary>
        /// The show sort glyph property
        /// </summary>
        public static readonly DependencyProperty ShowSortGlyphProperty = DependencyProperty.RegisterAttached(
            "ShowSortGlyph",
            typeof(bool),
            typeof(GridViewSort),
            new UIPropertyMetadata(true));

        /// <summary>
        /// The sort glyph ascending property
        /// </summary>
        public static readonly DependencyProperty SortGlyphAscendingProperty = DependencyProperty.RegisterAttached(
            "SortGlyphAscending",
            typeof(ImageSource),
            typeof(GridViewSort),
            new UIPropertyMetadata(null));

        /// <summary>
        /// The sort glyph descending property
        /// </summary>
        public static readonly DependencyProperty SortGlyphDescendingProperty = DependencyProperty.RegisterAttached(
            "SortGlyphDescending",
            typeof(ImageSource),
            typeof(GridViewSort),
            new UIPropertyMetadata(null));

        /// <summary>
        /// The sorted column header property
        /// </summary>
        public static readonly DependencyProperty SortedColumnHeaderProperty = DependencyProperty.RegisterAttached(
            "SortedColumnHeader",
            typeof(GridViewColumnHeader),
            typeof(GridViewSort),
            new UIPropertyMetadata(null));

        /// <summary>
        /// The use custom sort property
        /// </summary>
        public static readonly DependencyProperty UseCustomSortProperty = DependencyProperty.RegisterAttached(
            "UseCustomSort",
            typeof(bool),
            typeof(GridViewSort),
            new UIPropertyMetadata(false));

        /// <summary>
        /// The custom sort type property
        /// </summary>
        public static readonly DependencyProperty CustomSortTypeProperty = DependencyProperty.RegisterAttached(
            "CustomSortType",
            typeof(Type),
            typeof(GridViewSort),
            new UIPropertyMetadata(null));

        /// <summary>
        /// The custom sort direction property
        /// </summary>
        public static readonly DependencyProperty SortDirectionProperty = DependencyProperty.RegisterAttached(
            "SortDirection",
            typeof(ListSortDirection),
            typeof(GridViewSort),
            new UIPropertyMetadata(ListSortDirection.Ascending));

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////

        #region Private fields

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////

        #region Constructors

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////

        #region Properties

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////

        #region Public methods

        /// <summary>
        /// Gets the command.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The command property.</returns>
        public static ICommand GetCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(CommandProperty);
        }

        /// <summary>
        /// Sets the command.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">The value.</param>
        public static void SetCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(CommandProperty, value);
        }

        /// <summary>
        /// Gets the automatic sort.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The auto sort property.</returns>
        public static bool GetAutoSort(DependencyObject obj)
        {
            return (bool)obj.GetValue(AutoSortProperty);
        }

        /// <summary>
        /// Sets the automatic sort.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">if set to <c>true</c> auto sort.</param>
        public static void SetAutoSort(DependencyObject obj, bool value)
        {
            obj.SetValue(AutoSortProperty, value);
        }

        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The property name property.</returns>
        public static string GetPropertyName(DependencyObject obj)
        {
            return (string)obj.GetValue(PropertyNameProperty);
        }

        /// <summary>
        /// Sets the name of the property.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">The value.</param>
        public static void SetPropertyName(DependencyObject obj, string value)
        {
            obj.SetValue(PropertyNameProperty, value);
        }

        /// <summary>
        /// Gets the show sort glyph.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The show sort glyph property.</returns>
        public static bool GetShowSortGlyph(DependencyObject obj)
        {
            return (bool)obj.GetValue(ShowSortGlyphProperty);
        }

        /// <summary>
        /// Sets the show sort glyph.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">if set to <c>true</c> show sort glyph.</param>
        public static void SetShowSortGlyph(DependencyObject obj, bool value)
        {
            obj.SetValue(ShowSortGlyphProperty, value);
        }

        /// <summary>
        /// Gets the sort glyph ascending.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The sort glyph ascending property.</returns>
        public static ImageSource GetSortGlyphAscending(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(SortGlyphAscendingProperty);
        }

        /// <summary>
        /// Sets the sort glyph ascending.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">The value.</param>
        public static void SetSortGlyphAscending(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(SortGlyphAscendingProperty, value);
        }

        /// <summary>
        /// Gets the sort glyph descending.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The sort glyph descending property.</returns>
        public static ImageSource GetSortGlyphDescending(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(SortGlyphDescendingProperty);
        }

        /// <summary>
        /// Sets the sort glyph descending.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">The value.</param>
        public static void SetSortGlyphDescending(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(SortGlyphDescendingProperty, value);
        }

        /// <summary>
        /// Gets the sorted column header.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The sorted column header property.</returns>
        public static GridViewColumnHeader GetSortedColumnHeader(DependencyObject obj)
        {
            return (GridViewColumnHeader)obj.GetValue(SortedColumnHeaderProperty);
        }

        /// <summary>
        /// Sets the sorted column header.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">The value.</param>
        public static void SetSortedColumnHeader(DependencyObject obj, GridViewColumnHeader value)
        {
            obj.SetValue(SortedColumnHeaderProperty, value);
        }

        /// <summary>
        /// Gets the use custom sort property.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The use custom sort property.</returns>
        public static bool GetUseCustomSort(DependencyObject obj)
        {
            return (bool)obj.GetValue(UseCustomSortProperty);
        }

        /// <summary>
        /// Sets the use custom sort property.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">The value.</param>
        public static void SetUseCustomSort(DependencyObject obj, bool value)
        {
            obj.SetValue(UseCustomSortProperty, value);
        }

        /// <summary>
        /// Gets the type of the custom sort.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The custom sort type.</returns>
        public static Type GetCustomSortType(DependencyObject obj)
        {
            return (Type)obj.GetValue(CustomSortTypeProperty);
        }

        /// <summary>
        /// Sets the type of the custom sort.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">The value.</param>
        public static void SetCustomSortType(DependencyObject obj, Type value)
        {
            obj.SetValue(CustomSortTypeProperty, value);
        }

        /// <summary>
        /// Gets the ancestor.
        /// </summary>
        /// <typeparam name="T">The ancestor type.</typeparam>
        /// <param name="reference">The reference.</param>
        /// <returns>The ancestor of this reference.</returns>
        public static T GetAncestor<T>(DependencyObject reference) where T : DependencyObject
        {
            var parent = VisualTreeHelper.GetParent(reference);
            while (!(parent is T))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            if (parent != null)
            {
                return (T)parent;
            }

            return null;
        }

        /// <summary>
        /// Applies the sort.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="listView">The list view.</param>
        /// <param name="sortedColumnHeader">The sorted column header.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="changeDirection">if set to <c>true</c> change direction.</param>
        public static void ApplySort(
            ListCollectionView view,
            string propertyName,
            ListView listView,
            GridViewColumnHeader sortedColumnHeader,
            ListSortDirection direction,
            bool changeDirection = true)
        {
            var currentSortedColumnHeader = GetSortedColumnHeader(listView);
            if (currentSortedColumnHeader != null)
            {
                RemoveSortGlyph(currentSortedColumnHeader);
                if (currentSortedColumnHeader.Equals(sortedColumnHeader) && changeDirection)
                {
                    var currentCustomSortDirection = GetSortDirection(sortedColumnHeader.Column);
                    direction = currentCustomSortDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending;
                }
            }

            if (view == null)
            {
                return;
            }

            if (view.SortDescriptions.Count > 0)
            {
                var currentSort = view.SortDescriptions[0];
                if (currentSort.PropertyName == propertyName && changeDirection)
                {
                    direction = currentSort.Direction == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending;
                }

                view.SortDescriptions.Clear();
            }

            if (!string.IsNullOrWhiteSpace(propertyName))
            {
                var useCustomSort = GetUseCustomSort(sortedColumnHeader.Column);
                var customSortType = GetCustomSortType(listView);
                if (useCustomSort && customSortType != null)
                {
                    var customSort = (IComparer)Activator.CreateInstance(customSortType, propertyName, direction);
                    view.CustomSort = customSort;
                }
                else
                {
                    view.SortDescriptions.Add(new SortDescription(propertyName, direction));
                }

                if (GetShowSortGlyph(listView))
                {
                    AddSortGlyph(
                        sortedColumnHeader,
                        direction,
                        direction == ListSortDirection.Ascending ? GetSortGlyphAscending(listView) : GetSortGlyphDescending(listView));
                }

                SetSortDirection(sortedColumnHeader.Column, direction);
                SetSortedColumnHeader(listView, sortedColumnHeader);
            }
        }

        /// <summary>
        /// Applies the sort.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="listView">The list view.</param>
        /// <param name="sortedColumnHeader">The sorted column header.</param>
        /// <param name="direction">The direction.</param>
        public static void ApplyNonTogglingSort(ListCollectionView view, string propertyName, ListView listView, GridViewColumnHeader sortedColumnHeader, ListSortDirection direction)
        {
            var currentSortedColumnHeader = GetSortedColumnHeader(listView);
            if (currentSortedColumnHeader != null)
            {
                RemoveSortGlyph(currentSortedColumnHeader);
            }

            if (view == null)
            {
                return;
            }

            view.SortDescriptions.Clear();

            if (!string.IsNullOrWhiteSpace(propertyName))
            {
                var useCustomSort = GetUseCustomSort(sortedColumnHeader.Column);
                var customSortType = GetCustomSortType(listView);
                if (useCustomSort && customSortType != null)
                {
                    var customSort = (IComparer)Activator.CreateInstance(customSortType, propertyName, direction);
                    view.CustomSort = customSort;
                }
                else
                {
                    view.SortDescriptions.Add(new SortDescription(propertyName, direction));
                }

                if (GetShowSortGlyph(listView))
                {
                    AddSortGlyph(
                        sortedColumnHeader,
                        direction,
                        direction == ListSortDirection.Ascending ? GetSortGlyphAscending(listView) : GetSortGlyphDescending(listView));
                }

                SetSortDirection(sortedColumnHeader.Column, direction);
                SetSortedColumnHeader(listView, sortedColumnHeader);
            }
        }

        /// <summary>
        /// Gets the sort direction.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The custom sort direction.</returns>
        public static ListSortDirection GetSortDirection(DependencyObject obj)
        {
            return (ListSortDirection)obj.GetValue(SortDirectionProperty);
        }

        /// <summary>
        /// Sets the sort direction.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">The value.</param>
        public static void SetSortDirection(DependencyObject obj, ListSortDirection value)
        {
            obj.SetValue(SortDirectionProperty, value);
        }

        /// <summary>
        /// Resets the sort header.
        /// </summary>
        /// <param name="listView">The list view.</param>
        public static void ResetSortHeader(ListView listView)
        {
            var currentSortedColumnHeader = GetSortedColumnHeader(listView);
            if (currentSortedColumnHeader != null)
            {
                RemoveSortGlyph(currentSortedColumnHeader);
                SetSortedColumnHeader(listView, null);
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////

        #region Private methods

        /// <summary>
        /// Handles the column header click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private static void HandleColumnHeaderClick(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            if (headerClicked == null || headerClicked.Column == null)
            {
                return;
            }

            var propertyName = GetPropertyName(headerClicked.Column);
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                return;
            }

            var listView = GetAncestor<ListView>(headerClicked);
            if (listView == null)
            {
                return;
            }

            var selectedItems = ListBoxHelper.GetSelectedItems(listView);
            IList selectedItemsCopy = null;
            if (!selectedItems.IsNullOrEmpty())
            {
                var enumerate = selectedItems.GetEnumerator();
                enumerate.MoveNext();
                var itemType = enumerate.Current.GetType();
                var listType = typeof(List<>).MakeGenericType(itemType);
                selectedItemsCopy = (IList)Activator.CreateInstance(listType);
                foreach (var item in selectedItems)
                {
                    selectedItemsCopy.Add(item);
                }
            }

            var command = GetCommand(listView);
            if (command != null)
            {
                if (command.CanExecute(propertyName))
                {
                    command.Execute(propertyName);
                }
            }
            else if (GetAutoSort(listView))
            {
                var listCollectionView = (ListCollectionView)CollectionViewSource.GetDefaultView(listView.ItemsSource);
                ApplySort(listCollectionView, propertyName, listView, headerClicked, ListSortDirection.Ascending);
            }

            if (selectedItemsCopy == null)
            {
                return;
            }

            foreach (var selectedItem in selectedItemsCopy)
            {
                if (listView.SelectedItems.Contains(selectedItem))
                {
                    continue;
                }

                listView.SelectedItems.Add(selectedItem);
            }
        }

        /// <summary>
        /// Adds the sort glyph.
        /// </summary>
        /// <param name="columnHeader">The column header.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="sortGlyph">The sort glyph.</param>
        private static void AddSortGlyph(GridViewColumnHeader columnHeader, ListSortDirection direction, ImageSource sortGlyph)
        {
            var adornerLayer = AdornerLayer.GetAdornerLayer(columnHeader);
            adornerLayer.Add(new SortGlyphAdorner(columnHeader, direction, sortGlyph));
        }

        /// <summary>
        /// Removes the sort glyph.
        /// </summary>
        /// <param name="columnHeader">The column header.</param>
        private static void RemoveSortGlyph(GridViewColumnHeader columnHeader)
        {
            var adornerLayer = AdornerLayer.GetAdornerLayer(columnHeader);
            var adorners = adornerLayer.GetAdorners(columnHeader);
            if (adorners == null)
            {
                return;
            }

            foreach (var adorner in adorners)
            {
                if (adorner is SortGlyphAdorner)
                {
                    adornerLayer.Remove(adorner);
                }
            }
        }

        #endregion
    }
}
