﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MultiplyAndAddConverter.cs" company="MTI Film LLC">
// Copyright (c) 2011 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>turing\mti</author>
// <date>9/14/2011 12:03:21 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Converts a value by multiplying and adding. The format of the parameter is either "a,b"
    /// which will return (value * a) + b, or just "a" which will return (value * a).
    /// </summary>
    public class MultiplyAndAddConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors
        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion

        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return 1;
            }

            var v = System.Convert.ToDouble(value, CultureInfo.InvariantCulture);
            var p = parameter.ToString().Split(',');
            if (p.Length < 2)
            {
                // Alternate delimiter because it's hard to use commas in converter parameters.
                p = parameter.ToString().Split('_');
            }

            var a = System.Convert.ToDouble(p[0], CultureInfo.InvariantCulture);
            var b = 0.0;
            if (p.Length > 1)
            {
                b = System.Convert.ToDouble(p[1], CultureInfo.InvariantCulture);
            }

            return (v * a) + b;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter == null)
            {
                return 0;
            }

            try
            {
                var v = System.Convert.ToDouble(value, CultureInfo.InvariantCulture);
                var p = parameter.ToString().Split(',');
                if (p.Length < 2)
                {
                    p = parameter.ToString().Split('_');
                }

                var a = System.Convert.ToDouble(p[0], CultureInfo.InvariantCulture);
                var b = 0.0;
                if (p.Length > 1)
                {
                    b = System.Convert.ToDouble(p[1], CultureInfo.InvariantCulture);
                }

                return (v - b) / a;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        #endregion
    }
}
