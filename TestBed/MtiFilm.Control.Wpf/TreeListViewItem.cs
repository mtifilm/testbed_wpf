﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TreeListViewItem.cs" company="MTI Film LLC">
// Copyright (c) 2013 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>8/13/2013 11:55:44 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Class that defines a tree list view item.
    /// </summary>
    public class TreeListViewItem : TreeViewItem
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// The level
        /// </summary>
        private int level = -1;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets the level.
        /// </summary>
        public int Level
        {
            get
            {
                if (this.level == -1)
                {
                    var parent = ItemsControlFromItemContainer(this) as TreeListViewItem;
                    this.level = (parent != null) ? parent.Level + 1 : 0;
                }

                return this.level;
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Creates a new <see cref="T:System.Windows.Controls.TreeViewItem" /> to use to display the object.
        /// </summary>
        /// <returns>
        /// A new <see cref="T:System.Windows.Controls.TreeViewItem" />.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeListViewItem();
        }

        /// <summary>
        /// Determines whether an object is a <see cref="T:System.Windows.Controls.TreeViewItem" />.
        /// </summary>
        /// <param name="item">The object to evaluate.</param>
        /// <returns>
        /// true if <paramref name="item" /> is a <see cref="T:System.Windows.Controls.TreeViewItem" />; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeListViewItem;
        }

        #endregion
    }
}
