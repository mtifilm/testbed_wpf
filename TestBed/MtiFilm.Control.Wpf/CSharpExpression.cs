﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CSharpExpression.cs" company="MTI Film, LLC">
// Copyright (c) 2011 by MTI Film, LLC.  All Rights Reserved
// </copyright>
// <author>John Mertus</author>
// <date>1/30/2011 6:49:46 AM</date>
// <summary>
// This is a XAMAL Markup to make using C# expressions easier
// Usage for binding a lable to a slider dividing value in two is
//   xmlns:w="clr-namespace:MtiFilm.Control.Wpf;assembly=MtiFilm.Control.Wpf"
//   <Label Content="{Binding ElementName=slider1, Path=Value, Converter={w:CSharpExpression '(double)value/2.0'}}" />
// If a less than sign is in the expression, use the usual &lt;  etc, so to compute dB
//   Converter={w:CSharpExpression '(V &lt;= 0) ? double.MinValue : 20 * Math.Log10(V)'}
//
// Collapse the label if the slider value is greater than 1
//   <Label Content="Stupid" Visibility="{Binding ElementName=slider1, Path=Value, Converter={w:CSharpExpression '(double)value > 1 ? Visibility.Collapsed : Visibility.Visible'}}" />
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Windows.Markup;

    /// <summary>
    /// This creates a markkup expression to make usage in XML easier
    /// </summary>
    public class CSharpExpression : MarkupExtension
    {        
        /// <summary>
        /// Backing store for script
        /// </summary>
        private readonly string expression;

        /// <summary>
        /// Backing store for script
        /// </summary>
        private readonly string backExpression;

        /// <summary>
        /// Initializes a new instance of the <see cref="CSharpExpression"/> class.
        /// </summary>
        /// <param name="expression">The forward expersion.</param>
        /// <param name="inverse">The inverse expression.</param>
        public CSharpExpression(string expression, string inverse)
        {
            this.expression = expression;
            this.backExpression = inverse;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CSharpExpression"/> class.
        /// </summary>
        /// <param name="expression">The forward expersion.</param>
        public CSharpExpression(string expression)
        {
            this.expression = expression;
        }

        /// <summary>
        /// When implemented in a derived class, returns an object that is set as the value of the target property for this markup extension.
        /// </summary>
        /// <param name="serviceProvider">Object that can provide services for the markup extension.</param>
        /// <returns>
        /// The object value to set on the property where the extension is applied.
        /// </returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new CSharpExpressionConverter(this.expression, this.backExpression);
        }
    }
}