﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CSharpExpressionConverter.cs" company="MTI Film, LLC">
// Copyright (c) 2011 by MTI Film, LLC.  All Rights Reserved
// </copyright>
// <author>Ockham\mertus</author>
// <date>3/14/2011 6:21:18 AM</date>
// <summary>
// This converter evaluates a C# expression.  It caches the compiled value to make it 
// more efficient
// You have two variables available to the expression, the first is 'value' that is the value sent into the converter
// the second is the parameter 'A'.
// value is set to a double V if possible, and A is set to a double A if possible, otherwise it is 0.
// Be VERY careful about knowing what the binding object types are.  
//
// Usage:
//    xmlns:w="clr-namespace:Bliss.Wpf;assembly=Bliss.Wpf"
//    Define the converter DivideByThree that is bound to a TextBox
//      w:CSharpExpressionConverter Expression="(double)value/3.0" BackExpression="3.0*System.Convert.ToDouble(value)" x:Key="DivideByThree"
//      w:CSharpExpressionConverter Expression="V/3.0" BackExpression="3.0*V" x:Key="DivideByThree"
//      w:CSharpExpressionConverter Expression="(double)value/A" BackExpression="A*System.Convert.ToDouble(value)" A="4"  x:Key="DivideByFour"
//
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Windows;
    using System.Windows.Data;

    using Microsoft.CSharp;

    /// <summary>
    /// This creates a value converter, see CSharpExpression for how to
    /// use the converter.
    /// </summary>
    public class CSharpExpressionConverter : DependencyObject, IValueConverter
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The dependency property for an A object
        /// </summary>
        public static readonly DependencyProperty AProperty =
            DependencyProperty.Register("A", typeof(object), typeof(CSharpExpressionConverter));

        /// <summary>
        /// Storage for the compilied converters
        /// </summary>
        private IValueConverter forwardConverter;

        /// <summary>
        /// Storage for the compilied converters
        /// </summary>
        private IValueConverter backConverter;

        /// <summary>
        /// Backing store for the expression string
        /// </summary>
        private string expression;

        /// <summary>
        /// Backing store for the inverse expression
        /// </summary>
        private string backExpression;
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CSharpExpressionConverter"/> class.
        /// </summary>
        public CSharpExpressionConverter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CSharpExpressionConverter"/> class.
        /// </summary>
        /// <param name="expression">The forward expression.</param>
        /// <param name="backExpression">The back expression.</param>
        public CSharpExpressionConverter(string expression = null, string backExpression = null)
        {
            this.Expression = expression;
            this.BackExpression = backExpression;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        
        /// <summary>
        /// Gets or sets the A object value.
        /// </summary>
        /// <value>The A object.</value>
        public object A
        {
            get { return (object)this.GetValue(AProperty); }
            set { this.SetValue(AProperty, value); }
        }

        /// <summary>
        /// Gets or sets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public string Expression
        {
            get
            {
                return this.expression;
            }

            set
            {
                if (this.expression != value)
                {
                    this.expression = value;
                    if (this.expression != null)
                    {
                        this.forwardConverter = this.CreateConverter(this.expression);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the back expression.
        /// </summary>
        /// <value>The back expression.</value>
        public string BackExpression
        {
            get
            {
                return this.backExpression;
            }

            set
            {
                if (this.backExpression != value)
                {
                    this.backExpression = value;
                    if (this.backExpression != null)
                    {
                        this.backConverter = this.CreateConverter(this.backExpression);
                    }
                }
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (this.forwardConverter != null)
            {
                return this.forwardConverter.Convert(value, targetType, this.A, culture);
            }

            return null;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (this.backConverter != null)
            {
                return this.backConverter.Convert(value, targetType, this.A, culture);
            }

            return null;
        }
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Generates the complete source.
        /// </summary>
        /// <param name="expression">The c sharp expression.</param>
        /// <returns>A complete converter class wrapped around the expression and back expression</returns>
        private static string CreateSourceCodeFromExpression(string expression)
        {
            // There is no real reason to use a IValueConvert here!
            // However, one needs an object return with an evaluation, so using a custom
            // class would require more work!
            // Create the format string to parse, {{ are necessary for the format string to 
            // enter a literal {
            string classSourceCode = @"
                namespace Bliss.Wpf
                {{ 
                    using System;
                    using System.Globalization;
                    using System.Windows.Data;
                    using System.Windows;

                    public class CSharpConverter : IValueConverter
                    {{
                       public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
                       {{
                           if (value == null)
                           {{
                               return null;
                           }}

                           // See if A is a double, if so extract it
                           double A = 0;
                           if (parameter != null)
                           {{
                               double.TryParse(parameter.ToString(), NumberStyles.Any, CultureInfo.CurrentCulture, out A);
                           }}

                           // See if value is a double, if so extract it
                           double V = 0;
                           double.TryParse(value.ToString(), NumberStyles.Any, CultureInfo.CurrentCulture, out V);

                           return {0};
                       }}
                    
                       public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
                       {{ 
                           return null;
                       }}
                    }}  
                }}";

            // Return lines to make error reporting easier
            return string.Format(classSourceCode, expression);
        }

        /// <summary>
        /// Creates the converter.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns>A value converter parsing the expression</returns>
        private IValueConverter CreateConverter(string expression)
        {
            // Create a provider, to improve efficiency one would make a 
            // static class that cached this provider
            var provider = new CSharpCodeProvider();

            // specify compiler parameters
            var compilerParameters = new CompilerParameters
            {
                CompilerOptions = "/target:library /optimize",
                GenerateExecutable = false,
                GenerateInMemory = true,
                IncludeDebugInformation = false
            };

            // Ok, this is REALLY ugly, when in the designer, some extra assemblies are
            // in the CurrentDomain, some are dynamic so the assembly.Location fails
            // just ASSUME the dynamic come after, so stop when we hit one.  UGH!
            try
            {
                var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (var assembly in assemblies)
                {
                    var assemblyLocation = assembly.Location;
                    if (assemblyLocation.Contains("Windows"))
                    {
                        compilerParameters.ReferencedAssemblies.Add(assemblyLocation);
                    }
                }
            }
            catch
            {
            }

            // create the source and compile it
            var completeSource = CreateSourceCodeFromExpression(expression);
            var compiledCode = provider.CompileAssemblyFromSource(compilerParameters, completeSource);

            // Display the compiler errors if debug mode is on
            if (compiledCode.Errors.HasErrors)
            {
                this.DisplayCompilerErrors(completeSource, compiledCode);
                return null;
            }

            // Instantiate the CSharpConverter
            try
            {
                // Find it in the assemply, although there is only one
                var converterType = compiledCode.CompiledAssembly.GetTypes().First(c => c.Name == "CSharpConverter");

                // Create it
                return converterType.InvokeMember("ctor", BindingFlags.CreateInstance, null, new object(), null) as IValueConverter;
            }
            catch (Exception ex)
            {
                this.DebugMessage(ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Outputs a message box in debug mode
        /// </summary>
        /// <param name="message">The message.</param>
        [Conditional("DEBUG")]
        private void DebugMessage(string message)
        {
            MessageBox.Show(message);
        }

        /// <summary>
        /// Displays the compiler errors.
        /// </summary>
        /// <param name="completeSource">The complete source.</param>
        /// <param name="compiledCode">The compiled code.</param>
        [Conditional("DEBUG")]
        private void DisplayCompilerErrors(string completeSource, CompilerResults compiledCode)
        {
            foreach (CompilerError err in compiledCode.Errors)
            {
                var lines = completeSource.Split(new[] { "\n" }, StringSplitOptions.None);
                var error = new StringBuilder();
                error.AppendLine("Error found while compiling converter.");
                error.AppendLine(err.ErrorText);
                error.AppendLine("in line: " + lines[err.Line - 1]);
                this.DebugMessage(error.ToString());
            }
        }
        #endregion
    }
}
