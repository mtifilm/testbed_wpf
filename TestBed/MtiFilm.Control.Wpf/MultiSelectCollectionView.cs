﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MultiSelectCollectionView.cs" company="Mti Film, LLC">
// Copyright (c) 2011 by MTI Film, All Rights Reserved
// </copyright>
// <summary>
//   Defines the MultiSelectCollectionView type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;

    /// <summary>
    /// The multi select collection view class.
    /// </summary>
    /// <typeparam name="T">
    /// The type of items in the collection.
    /// </typeparam>
    public class MultiSelectCollectionView<T> : ListCollectionView, IMultiSelectCollectionView
    {
        /// <summary>
        /// The storage for the list of selected controls.
        /// </summary>
        private readonly List<Selector> controls = new List<Selector>();

        /// <summary>
        /// The storage for the ignore selection changed property.
        /// </summary>
        private bool ignoreSelectionChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiSelectCollectionView&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="list">The underlying collection, which must implement <see cref="T:System.Collections.IList"/>.</param>
        public MultiSelectCollectionView(IList list)
            : base(list)
        {
            this.SelectedItems = new ObservableCollection<T>();
        }

        /// <summary>
        /// Gets the selected items.
        /// </summary>
        public ObservableCollection<T> SelectedItems { get; private set; }

        /// <summary>
        /// Sets the selection.
        /// </summary>
        /// <param name="selector">The selector.</param>
        public void SetSelection(Selector selector)
        {
            var multiSelector = selector as MultiSelector;
            var listBox = selector as ListBox;

            if (multiSelector != null)
            {
                multiSelector.SelectedItems.Clear();

                foreach (T item in this.SelectedItems)
                {
                    multiSelector.SelectedItems.Add(item);
                }
            }
            else if (listBox != null)
            {
                listBox.SelectedItems.Clear();

                foreach (T item in this.SelectedItems)
                {
                    listBox.SelectedItems.Add(item);
                }
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the control control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        public void ControlSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!this.ignoreSelectionChanged)
            {
                bool changed = false;

                this.ignoreSelectionChanged = true;

                try
                {
                    foreach (T item in e.AddedItems)
                    {
                        if (!this.SelectedItems.Contains(item))
                        {
                            this.SelectedItems.Add(item);
                            changed = true;
                        }
                    }

                    foreach (T item in e.RemovedItems)
                    {
                        if (this.SelectedItems.Remove(item))
                        {
                            changed = true;
                        }
                    }

                    if (changed)
                    {
                        foreach (var control in this.controls)
                        {
                            if (control != sender)
                            {
                                this.SetSelection(control);
                            }
                        }
                    }
                }
                finally
                {
                    this.ignoreSelectionChanged = false;
                }
            }

            e.Handled = true;
        }

        /// <summary>
        /// Adds the control.
        /// </summary>
        /// <param name="selector">The selector.</param>
        void IMultiSelectCollectionView.AddControl(Selector selector)
        {
            this.controls.Add(selector);
            this.SetSelection(selector);
            selector.SelectionChanged += this.ControlSelectionChanged;
        }

        /// <summary>
        /// Removes the control.
        /// </summary>
        /// <param name="selector">The selector.</param>
        void IMultiSelectCollectionView.RemoveControl(Selector selector)
        {
            if (this.controls.Remove(selector))
            {
                selector.SelectionChanged -= this.ControlSelectionChanged;
            }
        }
    }
}
