﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TreeListView.cs" company="MTI Film LLC">
// Copyright (c) 2013 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>8/13/2013 11:54:35 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Class that defines a tree list view.
    /// </summary>
    public class TreeListView : TreeView
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Creates a <see cref="T:System.Windows.Controls.TreeViewItem" /> to use to display content.
        /// </summary>
        /// <returns>
        /// A new <see cref="T:System.Windows.Controls.TreeViewItem" /> to use as a container for content.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeListViewItem();
        }

        /// <summary>
        /// Determines whether the specified item is its own container or can be its own container.
        /// </summary>
        /// <param name="item">The object to evaluate.</param>
        /// <returns>
        /// true if <paramref name="item" /> is a <see cref="T:System.Windows.Controls.TreeViewItem" />; otherwise, false.
        /// </returns>
        protected override bool
                           IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeListViewItem;
        }

        #endregion
    }
}
