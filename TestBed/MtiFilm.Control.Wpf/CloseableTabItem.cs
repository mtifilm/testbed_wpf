﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CloseableTabItem.cs" company="MTI Film LLC">
// Copyright (c) 2017 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MSI\Admin</author>
// <date>11/14/2017 10:07:25 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Class that defines a closeable tab item.
    /// </summary>
    /// <seealso cref="System.Windows.Controls.TabItem" />
    public class CloseableTabItem : TabItem
    {
        /////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The close tab event.
        /// </summary>
        public static readonly RoutedEvent CloseTabEvent =
            EventManager.RegisterRoutedEvent("CloseTab", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CloseableTabItem));

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors
        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Occurs when close tab.
        /// </summary>
        public event RoutedEventHandler CloseTab
        {
            add => this.AddHandler(CloseTabEvent, value);
            remove => this.RemoveHandler(CloseTabEvent, value);
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call <see cref="M:System.Windows.FrameworkElement.ApplyTemplate" />.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var closeButton = this.GetTemplateChild("PART_Close") as Button;
            if (closeButton == null)
            {
                return;
            }

            closeButton.Click += this.HandleCloseButtonClick;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Handles the close button click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void HandleCloseButtonClick(object sender, RoutedEventArgs e)
        {
            this.RaiseEvent(new RoutedEventArgs(CloseTabEvent, this));
        }

        #endregion
    }
}