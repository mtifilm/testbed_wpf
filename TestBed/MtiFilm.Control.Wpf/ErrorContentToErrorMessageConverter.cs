﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ErrorContentToErrorMessageConverter.cs" company="MTI Film, LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>11/13/2012 12:36:28 PM</date>
// <summary>
// Splits ErrorContent into Type and Message and returns Message as a string.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Splits ErrorContent into Type and Message and returns Message as a string.
    /// </summary>
    [ValueConversion(typeof(string), typeof(string))]
    public class ErrorContentToErrorMessageConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // IValueConverter Members
        //////////////////////////////////////////////////

        #region IValueConverter Members

        /// <summary>
        /// Converts the specified message from the ErrorContent.  If the ErrorContent does not contain the ":" separator between type and message,
        /// then the passed in string is just returned.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>The appropriate string.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return string.Empty;
            }

            // If value doesn't contain the ":" separator, then just return it as is.
            if (!value.ToString().Contains(":"))
            {
                return value.ToString();
            }

            // Return the part after the ":"
            return value.ToString().Split(':')[1];
        }

        /// <summary>
        /// Converts back.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>An Exception.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}