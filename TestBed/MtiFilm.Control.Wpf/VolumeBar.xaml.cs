﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VolumeBar.xaml.cs" company="MTI Film">
//   Copyright (c) 2011 All Rights Reserved by MTI Film
// </copyright>
// <summary>
//   Builds a volume bar
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using MtiSimple.Core;

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;


    using Color = System.Windows.Media.Color;

    /// <summary>
    /// Interaction logic for VolumeBar.xaml
    /// </summary>
    public partial class VolumeBar : INotifyPropertyChanged
    {
        /// <summary>
        /// Usual dependency property
        /// </summary>
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value",
            typeof(double),
            typeof(VolumeBar),
            new PropertyMetadata(default(double)));

        /// <summary>
        /// Usual dependency property
        /// </summary>
        public static readonly DependencyProperty TicMarginProperty = DependencyProperty.Register(
            "TicMargin",
            typeof(Thickness),
            typeof(VolumeBar),
            new PropertyMetadata(null));

        /// <summary>
        /// Usual dependency property
        /// </summary>
        public static readonly DependencyProperty MuteProperty = DependencyProperty.Register(
            "Mute",
            typeof(bool),
            typeof(VolumeBar),
            new PropertyMetadata(false, OnMuteChanged));

        /// <summary>
        /// Usual dependency property
        /// </summary>
        public static readonly DependencyProperty BarClickedProperty = DependencyProperty.Register(
            "BarClicked",
            typeof(MouseButtonEventArgs),
            typeof(VolumeBar),
            new PropertyMetadata(null, OnBarClickedChanged));

        /// <summary>
        /// The bar gradient brush property.
        /// </summary>
        public static readonly DependencyProperty BarGradientBrushProperty = DependencyProperty.Register(
            "BarGradientBrush",
            typeof(LinearGradientBrush),
            typeof(VolumeBar),
            new PropertyMetadata(null));

        /// <summary>
        /// The tick color property.
        /// </summary>
        public static readonly DependencyProperty TickColorProperty = DependencyProperty.Register(
            "TickColor",
            typeof(SolidColorBrush),
            typeof(VolumeBar),
            new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));

        /// <summary>
        /// The show scale property.
        /// </summary>
        public static readonly DependencyProperty ShowScaleProperty = DependencyProperty.Register(
            "ShowScale",
            typeof(bool),
            typeof(VolumeBar),
            new PropertyMetadata(false, OnShowScalePropertyChanged));

        /// <summary>
        /// The almost peaked tick color.
        /// </summary>
        private readonly Brush almostPeakedTickColor = new SolidColorBrush(Color.FromRgb(255, 127, 39));

        /// <summary>
        /// The peaked tick color.
        /// </summary>
        private readonly Brush peakedTickColor = new SolidColorBrush(Color.FromRgb(237, 28, 36));

        /// <summary>
        /// The scale tag.
        /// </summary>
        private readonly string scaleTag = @"SCALE_TAG";

        /// <summary>
        /// Backing store for active event
        /// </summary>
        private bool active;

        /// <summary>
        /// Backing store for the DbValue;
        /// </summary>
        private double decibleValue;

        /// <summary>
        /// Backing store for the weight;
        /// </summary>
        private double weightValue;

        /// <summary>
        /// Backing store for the minimum Db of the display
        /// </summary>
        private double minimumDb;

        /// <summary>
        /// The decay weight
        /// </summary>
        private double weight;

        /// <summary>
        /// The volume scale position dictionary.
        /// </summary>
        private Dictionary<double, double> volumeScalePositionDictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="VolumeBar"/> class.
        /// </summary>
        public VolumeBar()
        {
            this.InitializeComponent();
            this.DataContext = this;
            this.MinimumDb = -96;
            this.weight = this.MinimumDb;
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the tic margin.
        /// </summary>
        /// <value>The value.</value>
        public Thickness TicMargin
        {
            get
            {
                return (Thickness)this.GetValue(TicMarginProperty);
            }

            set
            {
                this.SetValue(TicMarginProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public double Value
        {
            get
            {
                return (double)this.GetValue(ValueProperty);
            }

            set
            {
                this.SetValue(ValueProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="VolumeBar"/> is mute.
        /// </summary>
        /// <value><c>true</c> if mute; otherwise, <c>false</c>.</value>
        public bool Mute
        {
            get
            {
                return (bool)this.GetValue(MuteProperty);
            }

            set
            {
                this.SetValue(MuteProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="VolumeBar"/> is mute.
        /// </summary>
        /// <value><c>true</c> if mute; otherwise, <c>false</c>.</value>
        public MouseButtonEventArgs BarClicked
        {
            get
            {
                return (MouseButtonEventArgs)this.GetValue(BarClickedProperty);
            }

            set
            {
                this.SetValue(BarClickedProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the bar gradient brush.
        /// </summary>
        public LinearGradientBrush BarGradientBrush
        {
            get
            {
                return (LinearGradientBrush)this.GetValue(BarGradientBrushProperty);
            }

            set
            {
                this.SetValue(BarGradientBrushProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the color of the tick.
        /// </summary>
        public SolidColorBrush TickColor
        {
            get
            {
                return (SolidColorBrush)this.GetValue(TickColorProperty);
            }

            set
            {
                this.SetValue(TickColorProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to show scale.
        /// </summary>
        public bool ShowScale
        {
            get
            {
                return (bool)this.GetValue(ShowScaleProperty);
            }

            set
            {
                this.SetValue(ShowScaleProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the minimum db.
        /// </summary>
        /// <value>The minimum db.</value>
        public double MinimumDb
        {
            get
            {
                return this.minimumDb;
            }

            set
            {
                if (this.minimumDb.EqualsWithinPrecision(value))
                {
                    return;
                }

                this.minimumDb = value;

                if (this.TickColor != null)
                {
                    // Create the brush so the meters have the correct colors for -20 and -6 db
                    var linearGradientBrush = new LinearGradientBrush { StartPoint = new Point(0.5, 1), EndPoint = new Point(0.4, 0) };

                    linearGradientBrush.GradientStops.Add(new GradientStop(this.TickColor.Color, 0));
                    var y = 1.0 + (20.0 / this.minimumDb);
                    linearGradientBrush.GradientStops.Add(new GradientStop(this.TickColor.Color, y));
                    linearGradientBrush.GradientStops.Add(new GradientStop(Color.FromRgb(255, 127, 39), y));

                    var r = 1.0 + (6.0 / this.minimumDb);
                    linearGradientBrush.GradientStops.Add(new GradientStop(Color.FromRgb(255, 127, 39), r));
                    linearGradientBrush.GradientStops.Add(new GradientStop(Color.FromRgb(237, 28, 36), r));
                    linearGradientBrush.GradientStops.Add(new GradientStop(Color.FromRgb(237, 28, 36), 1.0));

                    this.BackgroundBorder.Background = linearGradientBrush;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="VolumeBar" /> is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if active; otherwise, <c>false</c>.
        /// </value>
        public bool Active
        {
           get
           {
               return this.active;
           }

            set
            {
                if (this.active != value)
                {
                    this.active = value;
                    this.Border.Opacity = this.active ? 1.0 : 0.5;
                    this.RaisePropertyChanged("Active");
                }
            }
        }

        /// <summary>
        /// Gets or sets the dB value.
        /// </summary>
        /// <value>The dB valued.</value>
        public double ValuedB
        {
            get
            {
                return this.decibleValue;
            }

            set
            {
                // if 0, we are not yet visible, do nothing
                if (this.ActualHeight <= 0)
                {
                    return;
                }

                this.decibleValue = value;

                // Compute the height to move the tick
                this.Value = Math.Max(this.ActualHeight * (1 - (value / this.MinimumDb)), 0);

                // Do a linear falloff 
                this.weight = Math.Max(this.Value, this.weight - (0.16 * (this.weight - this.Value)));

                // See if we have changed.
                if (Math.Abs(this.weightValue - this.weight) < 10E-6)
                {
                    return;
                }

                // Say we have set the weight
                this.weightValue = this.weight;

                this.TicMargin = new Thickness(0, 0, 0, this.weight);

                // Change the color of the tic
                var v = this.MinimumDb * (1 - (this.weight / this.ActualHeight));
                if (v < -20.0)
                {
                    this.TicBorder.Background = this.TickColor;
                }
                else if (v < -6.0)
                {
                    this.TicBorder.Background = this.almostPeakedTickColor;
                }
                else
                {
                    this.TicBorder.Background = this.peakedTickColor;
                }
            }
        }

        /// <summary>
        /// Gets or sets the volume scale position dictionary.
        /// </summary>
        public Dictionary<double, double> VolumeScalePositionDictionary
        {
            get
            {
                return this.volumeScalePositionDictionary;
            }

            set
            {
                if (this.volumeScalePositionDictionary == value)
                {
                    return;
                }

                this.volumeScalePositionDictionary = value;
                this.RaisePropertyChanged("VolumeScalePositionDictionary");
            }
        }

        /// <summary>
        /// Attempts to raise the PropertyChanged event, and
        /// invokes the virtual AfterPropertyChanged method,
        /// regardless of whether the event was raised or not.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                // Raise the PropertyChanged event.
                var e = new PropertyChangedEventArgs(propertyName);
                this.PropertyChanged(this, e);
            }
        }

        /// <summary>
        /// Called when the mute status is changed.
        /// </summary>
        /// <param name="d">The dependancy property</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnMuteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var volumeBar = d as VolumeBar;
            if (volumeBar == null)
            {
                return;
            }

            if (!volumeBar.Mute)
            {
                volumeBar.BackgroundBorder.Background = volumeBar.BarGradientBrush;
                volumeBar.TicBorder.Background = volumeBar.BarGradientBrush;
            }
            else
            {
                volumeBar.BackgroundBorder.Background = Brushes.DimGray;
                volumeBar.TicBorder.Background = Brushes.DimGray;
            }

            volumeBar.RaisePropertyChanged("Mute");
        }

        /// <summary>
        /// Called when a bar is clicked.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnBarClickedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var volumeBar = d as VolumeBar;
            if (volumeBar != null)
            {
                volumeBar.RaisePropertyChanged("BarClicked");
            }
        }

        /// <summary>
        /// Called when the show scale property is changed.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnShowScalePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var volumeBar = d as VolumeBar;
            if (volumeBar == null)
            {
                return;
            }

            if (!volumeBar.ShowScale)
            {
                volumeBar.Border.BorderBrush = Brushes.White;
                volumeBar.Border.BorderThickness = new Thickness(1, 0, 0, 1);
                return;
            }

            volumeBar.Border.BorderBrush = Brushes.White;
            volumeBar.Border.BorderThickness = new Thickness(1);
            volumeBar.DrawScale();
        }

        /// <summary>
        /// Handles the mouse up event on the border.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void BorderMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            this.BarClicked = e;
        }

        /// <summary>
        /// Draws the ticks.
        /// </summary>
        private void DrawScale()
        {
            if (this.ActualHeight <= 0)
            {
                return;
            }

            var controlsToRemove = new List<StackPanel>();
            foreach (var child in this.BarGrid.Children)
            {
                var stackPanel = child as StackPanel;
                if (stackPanel == null)
                {
                    continue;
                }

                var stackPanelTagString = stackPanel.Tag as string;
                if (stackPanelTagString == null)
                {
                    continue;
                }

                if (stackPanelTagString != this.scaleTag)
                {
                    continue;
                }

                controlsToRemove.Add(stackPanel);
            }

            foreach (var control in controlsToRemove)
            {
                this.BarGrid.Children.Remove(control);
            }

            var rangeList = new[] { this.MinimumDb, -60, -55, -50, -45, -40, -35, -30, -25, -20, -15, -10, -5, 0 };
            var tempDictionary = new Dictionary<double, double>();
            foreach (var value in rangeList)
            {
                var position = this.ActualHeight * (1 - (value / this.MinimumDb));
                if (position > 0)
                {
                    this.AddDbTick(position);
                    tempDictionary.Add(value, position);
                }
            }

            this.VolumeScalePositionDictionary = tempDictionary;
        }

        /// <summary>
        /// Adds the db tick.
        /// </summary>
        /// <param name="position">The position.</param>
        private void AddDbTick(double position)
        {
            var tickWidth = this.Width;
            var stackPanel = new StackPanel
                                {
                                    Orientation = Orientation.Horizontal,
                                    Tag = this.scaleTag,
                                    VerticalAlignment = VerticalAlignment.Bottom,
                                    HorizontalAlignment = HorizontalAlignment.Left
                                };
            var foreground = Brushes.White;
            var horizontalTick = new Line
                                     {
                                         X1 = 0,
                                         X2 = tickWidth,
                                         Y1 = 0,
                                         Y2 = 0,
                                         Stroke = foreground,
                                         StrokeThickness = 1,
                                         VerticalAlignment = VerticalAlignment.Bottom
                                     };
            stackPanel.Children.Add(horizontalTick);
            stackPanel.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            if (position < 0 || position > this.ActualHeight)
            {
                return;
            }

            stackPanel.Margin = new Thickness(0, 0, 0, position);
            this.BarGrid.Children.Add(stackPanel);
        }

        /// <summary>
        /// Handles the size changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs"/> instance containing the event data.</param>
        private void HandleSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!this.ShowScale)
            {
                return;
            }

            this.DrawScale();
        }
    }
}
