﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimecodeMaskedTextBox.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>Wasabe-XL\mbraca</author>
// <date>4/20/2012 12:21:13 PM</date>
// <summary>A hack to add some features I need for timecode to the masked text box.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// A hack to add some features I need for timecode to the masked text box.
    /// TODO: Would be nice to have a real timecode widget!
    /// </summary>
    public class TimecodeMaskedTextBox : MaskedTextBox
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// Dependency property to store the mask to apply to the textbox
        /// </summary>
        public static readonly DependencyProperty IsActiveProperty =
            DependencyProperty.Register("IsActive", typeof(bool), typeof(TimecodeMaskedTextBox), new UIPropertyMetadata(false, OnIsActiveChanged));

        /// <summary>
        /// Dependency property to store the mask to apply to the textbox
        /// </summary>
        public static readonly DependencyProperty NormalColorProperty =
            DependencyProperty.Register("NormalColor", typeof(Brush), typeof(TimecodeMaskedTextBox), new UIPropertyMetadata(new SolidColorBrush(Colors.White), OnNormalColorChanged));

        /// <summary>
        /// Dependency property to store the mask to apply to the textbox
        /// </summary>
        public static readonly DependencyProperty ActiveColorProperty =
            DependencyProperty.Register("ActiveColor", typeof(Brush), typeof(TimecodeMaskedTextBox), new UIPropertyMetadata(new SolidColorBrush(Colors.White), OnActiveColorChanged));
        
        /// <summary>
        /// The timecode mask.
        /// </summary>
        private const string TimecodeMask = "00:00:00:00";

        /// <summary>
        /// The standard thickness of the border.
        /// </summary>
        private static readonly Thickness InitialBorderThickness = new Thickness(3, 3, 3, 3);

        /// <summary>
        /// The standard padding.
        /// </summary>
        private static readonly Thickness InitialPadding = new Thickness(5, 1, 0, 0);

        /// <summary>
        /// A black brush for unhighlighted border.
        /// </summary>
        private static readonly Brush Black = new SolidColorBrush(Colors.Black);

        /// <summary>
        /// A white brush for highlighted border.
        /// </summary>
        private static readonly Brush White = new SolidColorBrush(Colors.White);

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the TimecodeMaskedTextBox class
        /// </summary>
        public TimecodeMaskedTextBox()
        {
            this.Mask = TimecodeMask;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the normal (non-active) foreground color.
        /// </summary>
        /// <value>
        /// The normal foreground color..
        /// </value>
        public Brush NormalColor
        {
            get
            {
                return (Brush)this.GetValue(NormalColorProperty);
            }

            set
            {
                this.SetValue(NormalColorProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the active foreground color .
        /// </summary>
        /// <value>
        /// The active foreground color.
        /// </value>
        public Brush ActiveColor
        {
            get
            {
                return (Brush)this.GetValue(ActiveColorProperty);
            }

            set
            {
                this.SetValue(ActiveColorProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is active; otherwise, <c>false</c>.
        /// </value>
        public bool IsActive
        {
            get
            {
                return (bool)this.GetValue(IsActiveProperty);
            }

            set
            {
                this.SetValue(IsActiveProperty, value);
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Called when the IsActive dependency property changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnIsActiveChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var me = d as TimecodeMaskedTextBox;
            if (me == null)
            {
                return;
            }

            if (me.IsActive)
            {
                me.Foreground = me.ActiveColor;
                me.BorderBrush = me.ActiveColor;
            }
            else
            {
                me.Foreground = me.NormalColor;
                me.BorderBrush = Black;
            }
        }

        /// <summary>
        /// Called when the NormalColor dependency property changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnNormalColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var me = d as TimecodeMaskedTextBox;
            if (me == null)
            {
                return;
            }

            if (!me.IsActive)
            {
                me.Foreground = me.NormalColor;
            }
        }

        /// <summary>
        /// Called when the ActiveColor dependency property changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnActiveColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var me = d as TimecodeMaskedTextBox;
            if (me == null)
            {
                return;
            }

            if (me.IsActive)
            {
                me.Foreground = me.ActiveColor;
                me.BorderBrush = me.ActiveColor;
            }
        }

        #endregion
    }
}
