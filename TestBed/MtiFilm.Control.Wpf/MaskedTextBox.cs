﻿// <copyright file="MaskedTextBox.cs" company="MTI Film">
// Copyright (c) 2011 by MTI Film, All Rights Reserved (Taken from Avalon Controls Library)
// </copyright>
// <author>rancor\pfirth</author>
// <date>10/21/2011 9:28:58 AM</date>
// <summary>An Avalon Controls Library Class</summary>

namespace MtiFilm.Control.Wpf
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Masked textbox control is a text box that can have a mask for the text
    /// </summary>
    public class MaskedTextBox : TextBox
    {
        /// <summary>
        /// Dependency property to store the mask to apply to the textbox
        /// </summary>
        public static readonly DependencyProperty MaskProperty =
            DependencyProperty.Register("Mask", typeof(string), typeof(MaskedTextBox), new UIPropertyMetadata(null, MaskChanged));

        /// <summary>
        /// Initializes static members of the <see cref="MaskedTextBox"/> class.
        /// </summary>
        static MaskedTextBox()
        {
            // override the meta data for the Text Proeprty of the textbox 
            var metaData = new FrameworkPropertyMetadata { CoerceValueCallback = ForceText };
            TextProperty.OverrideMetadata(typeof(MaskedTextBox), metaData);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaskedTextBox"/> class.
        /// </summary>
        public MaskedTextBox()
        {
            // cancel the paste and cut command
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, null, CancelCommand));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Cut, null, CancelCommand));
        }

        #region Properties

        /// <summary>
        /// Gets the MaskTextProvider for the specified Mask
        /// </summary>
        public MaskedTextProvider MaskProvider
        {
            get
            {
                MaskedTextProvider maskProvider = null;
                if (this.Mask != null)
                {
                    maskProvider = new MaskedTextProvider(this.Mask, null, true, '0', '\0', false);
                    maskProvider.Set(this.Text);
                }

                return maskProvider;
            }
        }

        /// <summary>
        /// Gets or sets the mask to apply to the textbox
        /// </summary>
        public string Mask
        {
            get
            {
                return (string)this.GetValue(MaskProperty);
            }

            set
            {
                this.SetValue(MaskProperty, value);
            }
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Masks the changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        protected static void MaskChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            // make sure to update the text if the mask changes
            var textBox = (MaskedTextBox)sender;
            textBox.RefreshText(textBox.MaskProvider, 0);
        }

        /// <summary>
        /// override this method to replace the characters enetered with the mask
        /// </summary>
        /// <param name="e">Arguments for event</param>
        protected override void OnPreviewTextInput(TextCompositionEventArgs e)
        {
            // if the text is readonly do not add the text
            if (this.IsReadOnly)
            {
                e.Handled = true;
                return;
            }

            var position = SelectionStart;
            var provider = this.MaskProvider;
            if (position == 0 || position < Text.Length)
            {
                position = this.GetNextCharacterPosition(position);

                if (this.SelectionLength > 0)
                {
                    MaskedTextResultHint resultHint;
                    provider.Replace(e.Text, position, position + this.SelectionLength - 1, out position, out resultHint);
                    position++;
                }
                else
                {
                    if (provider.InsertAt(e.Text, position))
                    {
                        position++;
                    }
                }

                position = this.GetNextCharacterPosition(position);
            }

            this.RefreshText(provider, position);
            e.Handled = true;

            base.OnPreviewTextInput(e);
        }

        /// <summary>
        /// override the key down to handle delete of a character
        /// </summary>
        /// <param name="e">Arguments for the event</param>
        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            base.OnPreviewKeyDown(e);
            var provider = this.MaskProvider;
            var position = this.SelectionStart;
            if (this.SelectionLength == Text.Length && (e.Key == Key.Delete || e.Key == Key.Back))
            {
                provider.Clear();
                this.RefreshText(provider, position);
                e.Handled = true;
                return;
            }

            if (e.Key == Key.Delete && position < Text.Length)
            {
                // handle the delete key
                if (provider.RemoveAt(position))
                {
                    this.RefreshText(provider, position);
                }

                e.Handled = true;
            }
            else if (e.Key == Key.Space)
            {
                if (provider.InsertAt(" ", position))
                {
                    this.RefreshText(provider, position);
                }

                e.Handled = true;
            }
            else if (e.Key == Key.Back)
            {
                // handle the back space
                if (position > 0)
                {
                    position--;
                    if (provider.Replace(provider.PromptChar, position))
                    {
                        this.RefreshText(provider, position);
                    }
                }

                e.Handled = true;
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Forces the text.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="value">The value.</param>
        /// <returns>The display string for the value.</returns>
        private static object ForceText(DependencyObject sender, object value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var textBox = (MaskedTextBox)sender;
            if (textBox.Mask != null)
            {
                var provider = new MaskedTextProvider(textBox.Mask, null, true, '0', '\0', false);
                provider.Set((string)value);
                return provider.ToDisplayString();
            }

            return value;
        }

        /// <summary>
        /// Cancels the command.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private static void CancelCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            e.Handled = true;
        }

        /// <summary>
        /// Refreshes the text.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="position">The position.</param>
        private void RefreshText(MaskedTextProvider provider, int position)
        {
            this.Text = provider.ToDisplayString();
            this.SelectionStart = position;
            this.SelectionLength = 1;
        }

        /// <summary>
        /// Gets the next character position.
        /// </summary>
        /// <param name="startPosition">The start position.</param>
        /// <returns>The next character position.</returns>
        private int GetNextCharacterPosition(int startPosition)
        {
            var position = this.MaskProvider.FindEditPositionFrom(startPosition, true);
            return position == -1 ? startPosition : position;
        }

        #endregion
    }
}
