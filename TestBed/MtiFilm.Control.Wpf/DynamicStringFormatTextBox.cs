﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DynamicStringFormatTextBox.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>rancor\pfirth</author>
// <date>4/13/2012 1:35:02 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Class for a textbox with a dynamic string format.
    /// </summary>
    public class DynamicStringFormatTextBox : TextBox
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// Dependency property to store string format to apply to the textbox
        /// </summary>
        public static readonly DependencyProperty StringFormatProperty =
            DependencyProperty.Register("StringFormat", typeof(string), typeof(DynamicStringFormatTextBox), new UIPropertyMetadata(string.Empty, StringFormatChanged));

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the DynamicStringFormatTextBox class
        /// </summary>
        public DynamicStringFormatTextBox()
        {
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the string format to apply to the textbox
        /// </summary>
        public string StringFormat
        {
            get
            {
                return (string)this.GetValue(StringFormatProperty);
            }

            set
            {
                this.SetValue(StringFormatProperty, value);
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Strings the format changed.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void StringFormatChanged(DependencyObject dp, DependencyPropertyChangedEventArgs e)
        {
            // make sure to update the text if the mask changes
            var textBox = (DynamicStringFormatTextBox)dp;
            textBox.Text = string.Format(textBox.StringFormat, textBox.Text);
        }

        #endregion
    }
}
