﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProductTypeToVisibilityConverter.cs" company="MTI Film, LLC">
// Copyright (c) 2013 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\michael.russell</author>
// <date>2013-10-31 09:40:27</date>
// <summary>Converts the product type to visible or not.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// Converts the product type to visible or not.
    /// </summary>
    public class ProductTypeToVisibilityConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the visible when.
        /// </summary>
        public string VisibleWhen { get; set; }

        /// <summary>
        /// Gets or sets the item list.
        /// </summary>
        public string ItemList { get; set; }

        #endregion

        //////////////////////////////////////////////////
        // IValueConverter methods
        //////////////////////////////////////////////////
        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (this.ItemList == null || value == null)
            {
                return Visibility.Collapsed;
            }

            // Always return Collapsed if we get the value "Unknown" - which means that the ProductType wasn't properly set.
            if (value.ToString() == "Unknown")
            {
                return Visibility.Collapsed;
            }

            // Get the value which determines if the user wants Visible if Equal to the ItemList or Collapsed
            bool showWhenVisible;

            switch (this.VisibleWhen)
            {
                case "Equal":
                    showWhenVisible = true;
                    break;
                case "NotEqual":
                    showWhenVisible = false;
                    break;
                default:
                    return Visibility.Collapsed;
            }

            // See if the value is contained in the ItemList.
            var itemList = this.ItemList.Split(',');

            var contains = itemList.Contains(value.ToString());

            // Return the desired value.
            Visibility retval;

            if (showWhenVisible)
            {
                retval = contains ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                retval = contains ? Visibility.Collapsed : Visibility.Visible;
            }

            return retval;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
