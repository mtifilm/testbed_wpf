﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MultiSelect.cs" company="Mti Film, LLC">
// Copyright (c) 2011 by MTI Film, All Rights Reserved  
// </copyright>
// <summary>
//   The multi select class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.Windows;
    using System.Windows.Controls.Primitives;

    /// <summary>
    /// The multi select class.
    /// </summary>
    public static class MultiSelect
    {
        /// <summary>
        /// The is enabled dependency property.
        /// </summary>
        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.RegisterAttached(
            "IsEnabled", typeof(bool), typeof(MultiSelect), new UIPropertyMetadata(IsEnabledChanged));

        /// <summary>
        /// Initializes static members of the <see cref="MultiSelect"/> class. 
        /// </summary>
        static MultiSelect()
        {
            Selector.ItemsSourceProperty.OverrideMetadata(typeof(Selector), new FrameworkPropertyMetadata(ItemsSourceChanged));
        }

        /// <summary>
        /// Gets the is enabled.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns>True if the target is enabled.</returns>
        public static bool GetIsEnabled(Selector target)
        {
            return (bool)target.GetValue(IsEnabledProperty);
        }

        /// <summary>
        /// Sets the is enabled.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetIsEnabled(Selector target, bool value)
        {
            target.SetValue(IsEnabledProperty, value);
        }

        /// <summary>
        /// Determines whether [is enabled changed] [the specified sender].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        public static void IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var selector = sender as Selector;
            var collectionView = selector.ItemsSource as IMultiSelectCollectionView;

            if (selector != null && collectionView != null)
            {
                if ((bool)e.NewValue)
                {
                    collectionView.AddControl(selector);
                }
                else
                {
                    collectionView.RemoveControl(selector);
                }
            }
        }

        /// <summary>
        /// Itemses the source changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        public static void ItemsSourceChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var selector = sender as Selector;

            if (GetIsEnabled(selector))
            {
                var oldCollectionView = e.OldValue as IMultiSelectCollectionView;
                var newCollectionView = e.NewValue as IMultiSelectCollectionView;

                if (oldCollectionView != null)
                {
                    oldCollectionView.RemoveControl(selector);
                }

                if (newCollectionView != null)
                {
                    newCollectionView.AddControl(selector);
                }
            }
        }
    }
}
