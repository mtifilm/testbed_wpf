﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BoolToTextDecorationCollectionConverter.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\hans.lehmann</author>
// <date>9/11/2012 3:55:49 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// A bool to font decoration collection convert that allows one to define different types of converters
    /// </summary>
    /// <remarks>
    /// Define a converter resouce by specifying the TrueValue and FalseValue and rename it to something reasonable
    /// See the example.
    /// </remarks>
    /// <example>
    /// BoolToTextDecorationCollectionConverter x:Key="BoolToDecorationConverter" TrueValue="UnderlineCollection" FalseValue="RegularCollection"
    /// </example>
    [ValueConversion(typeof(bool), typeof(TextDecorationCollection))]
    public class BoolToTextDecorationCollectionConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BoolToTextDecorationCollectionConverter"/> class.
        /// </summary>
        public BoolToTextDecorationCollectionConverter()
        {
            // set defaults
            var underline = new TextDecoration
                {
                    Pen = new Pen(Brushes.DarkGray, 1),
                    PenThicknessUnit = TextDecorationUnit.FontRecommended
                };

            var textDecorationCollection = new TextDecorationCollection { underline };

            this.FalseValue = textDecorationCollection;
            this.TrueValue = new TextDecorationCollection();
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the true value.
        /// </summary>
        /// <value>The true value.</value>
        public TextDecorationCollection TrueValue { get; set; }

        /// <summary>
        /// Gets or sets the false value.
        /// </summary>
        /// <value>The false value.</value>
        public TextDecorationCollection FalseValue { get; set; }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
            {
                return null;
            }

            return (bool)value ? this.TrueValue : this.FalseValue;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (object.Equals(value, this.TrueValue))
            {
                return true;
            }

            if (object.Equals(value, this.FalseValue))
            {
                return false;
            }

            return DependencyProperty.UnsetValue;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
