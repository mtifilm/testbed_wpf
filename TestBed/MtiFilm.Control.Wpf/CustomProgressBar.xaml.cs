﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomProgressBar.xaml.cs" company="MTI Film">
//   Copyright (c) 2016 by MTI Film, All Rights Reserved">
// </copyright>
// <summary>
//   Interaction logic for CustomProgressBar.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace MtiFilm.Control.Wpf
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for CustomProgressBar.xaml
    /// </summary>
    public partial class CustomProgressBar
    {
        /// <summary>
        /// The value property
        /// </summary>
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(double), typeof(CustomProgressBar), DefaultValue());

        /// <summary>
        /// The minimum property
        /// </summary>
        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register("Minimum", typeof(double), typeof(CustomProgressBar), new PropertyMetadata(default(double)));

        /// <summary>
        /// The maximum property
        /// </summary>
        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register("Maximum", typeof(double), typeof(CustomProgressBar), new PropertyMetadata(default(double)));

        /// <summary>
        /// The text brush property
        /// </summary>
        public static readonly DependencyProperty TextBrushProperty = DependencyProperty.Register("TextBrush", typeof(Brush), typeof(CustomProgressBar), new PropertyMetadata(default(Brush)));

        /// <summary>
        /// The background fill property
        /// </summary>
        public static readonly DependencyProperty BackgroundFillProperty = DependencyProperty.Register("BackgroundFill", typeof(Brush), typeof(CustomProgressBar), DefaultBackgroundFill());

        /// <summary>
        /// The progress fill property
        /// </summary>
        public static readonly DependencyProperty ProgressFillProperty = DependencyProperty.Register("ProgressFill", typeof(Brush), typeof(CustomProgressBar), DefaultProgressFill());

        /// <summary>
        /// The text property
        /// </summary>
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(CustomProgressBar), new PropertyMetadata(string.Empty));

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomProgressBar"/> class.
        /// </summary>
        public CustomProgressBar()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Text display type
        /// </summary>
        public enum TextDisplay
        {
            /// <summary>
            /// The none
            /// </summary>
            None,

            /// <summary>
            /// The valueonly
            /// </summary>
            Valueonly,

            /// <summary>
            /// The valuewith percentage
            /// </summary>
            ValuewithPercentage,

            /// <summary>
            /// The valuewith percentage complete
            /// </summary>
            ValuewithPercentageComplete
        }

        /// <summary>
        /// Gets or sets the progress fill.
        /// </summary>
        public Brush ProgressFill
        {
            get => (Brush)this.GetValue(ProgressFillProperty);
            set => this.SetValue(ProgressFillProperty, value);
        }

        /// <summary>
        /// Gets or sets the background fill.
        /// </summary>
        public Brush BackgroundFill
        {
            get => (Brush)this.GetValue(BackgroundFillProperty);
            set => this.SetValue(BackgroundFillProperty, value);
        }

        /// <summary>
        /// Gets or sets the text brush
        /// </summary>
        public Brush TextBrush
        {
            get => (Brush)this.GetValue(TextBrushProperty);
            set => this.SetValue(TextBrushProperty, value);
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public double Value
        {
            get => (double)this.GetValue(ValueProperty);
            set => this.SetValue(ValueProperty, value);
        }

        /// <summary>
        /// Gets or sets the minimum.
        /// </summary>
        public double Minimum
        {
            get => (double)this.GetValue(MinimumProperty);
            set => this.SetValue(MinimumProperty, value);
        }

        /// <summary>
        /// Gets or sets the maximum.
        /// </summary>
        public double Maximum
        {
            get => (double)this.GetValue(MaximumProperty);
            set => this.SetValue(MaximumProperty, value);
        }

        /// <summary>
        /// Gets or sets the Text
        /// </summary>
        public string Text
        {
            get => (string)this.GetValue(TextProperty);
            set => this.SetValue(TextProperty, value);
        }

        /// <summary>
        /// Gets or sets the type of the tool tip.
        /// </summary>
        public TextDisplay ToolTipType { get; set; }

        /// <summary>
        /// Gets or sets the type of the text.
        /// </summary>
        public TextDisplay TextType { get; set; }

        /// <summary>
        /// Defaults the value.
        /// </summary>
        /// <returns>the default value</returns>
        private static PropertyMetadata DefaultValue()
        {
            return new PropertyMetadata((double)0, (obj, chng) =>
            {
                var current = (CustomProgressBar)obj;
                current.Rectangle.Width = (double)chng.NewValue;

                switch (current.ToolTipType)
                {
                    case TextDisplay.None:
                        current.Rectangle.ToolTip = null;
                        break;
                    case TextDisplay.Valueonly:
                        current.Rectangle.ToolTip = $"{chng.NewValue:f1}";
                        break;
                    case TextDisplay.ValuewithPercentage:
                        current.Rectangle.ToolTip = $"{chng.NewValue:f1} %";
                        break;
                    case TextDisplay.ValuewithPercentageComplete:
                        current.Rectangle.ToolTip = $"{chng.NewValue:f1} % Complete";
                        break;
                }

                switch (current.TextType)
                {
                    case TextDisplay.None:
                        current.Text = null;
                        break;
                    case TextDisplay.Valueonly:
                        current.Text = $"{chng.NewValue:f1}";
                        break;
                    case TextDisplay.ValuewithPercentage:
                        current.Text = $"{chng.NewValue:f1} %";
                        break;
                    case TextDisplay.ValuewithPercentageComplete:
                        current.Text = $"{chng.NewValue:f1} % Complete";
                        break;
                }
            });
        }

        /// <summary>
        /// Defaults the background fill.
        /// </summary>
        /// <returns>the default background fill</returns>
        private static PropertyMetadata DefaultBackgroundFill()
        {
            return new PropertyMetadata(
                new LinearGradientBrush(
                    new GradientStopCollection
                                        {
                                            new GradientStop
                                            {
                                                Color = Brushes.White.Color,
                                                Offset = 0
                                            },
                                            new GradientStop
                                            {
                                                Color = Brushes.LightPink.Color,
                                                Offset = 0.5
                                            },
                                            new GradientStop
                                            {
                                                Color = Brushes.Pink.Color,
                                                Offset = 0.75
                                            }
                                        },
                    new Point(0.5, 0),
                    new Point(0.5, 1)),
                (obj, chng) =>
                {
                    var current = (CustomProgressBar)obj;
                    current.Grid.Background = (Brush)chng.NewValue;
                });
        }

        /// <summary>
        /// Defaults the progress fill.
        /// </summary>
        /// <returns>the default progress fill</returns>
        private static PropertyMetadata DefaultProgressFill()
        {
            return new PropertyMetadata(
                new LinearGradientBrush(
                    new GradientStopCollection
                                        {
                                                    new GradientStop
                                                    {
                                                        Color = Brushes.Silver.Color,
                                                        Offset = 0
                                                    },
                                                    new GradientStop
                                                    {
                                                        Color = Brushes.Red.Color,
                                                        Offset = 0.5
                                                    },
                                                    new GradientStop
                                                    {
                                                        Color = Brushes.DarkRed.Color,
                                                        Offset = 0.75
                                                    }
                                        },
                    new Point(0.5, 0),
                    new Point(0.5, 1)),
                (obj, chng) =>
                {
                    var current = (CustomProgressBar)obj;
                    current.Rectangle.Fill = (Brush)chng.NewValue;
                });
        }
    }
}
