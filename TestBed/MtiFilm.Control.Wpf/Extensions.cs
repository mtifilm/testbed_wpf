﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="MTI Film LLC">
// Copyright (c) 2015 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>12/1/2015 10:47:08 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Microsoft.Win32;

    /// <summary>
    /// Defines various WPF control extensions.
    /// </summary>
    public static class Extensions
    {
        /////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors
        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Scrolls item to the center of the view.
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="item">The item.</param>
        public static void ScrollToCenterOfView(this ItemsControl itemsControl, object item)
        {
            // Scroll immediately if possible
            if (!itemsControl.TryScrollToCenterOfView(item))
            {
                // Otherwise wait until everything is loaded and then scroll
                if (itemsControl is ListBox)
                {
                    ((ListBox)itemsControl).ScrollIntoView(item);
                }

                itemsControl.Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() => { itemsControl.TryScrollToCenterOfView(item); }));
            }
        }

        /// <summary>
        /// Finds the parent.
        /// </summary>
        /// <typeparam name="T">The type of the parent.</typeparam>
        /// <param name="child">The child.</param>
        /// <returns>The parent.</returns>
        public static T FindParent<T>(this DependencyObject child) where T : DependencyObject
        {
            // get parent item
            var parentObject = VisualTreeHelper.GetParent(child);

            // we've reached the end of the tree
            if (parentObject == null)
            {
                return null;
            }

            // check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }

            return FindParent<T>(parentObject);
        }

        /// <summary>
        /// Gets the effective width.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns>The effective width.</returns>
        public static double GetEffectiveWidth(this FrameworkElement visualElement)
        {
            return !double.IsNaN(visualElement.Width) && visualElement.Width > 0
                                ? visualElement.Width
                                : !double.IsNaN(visualElement.ActualWidth)
                                    ? visualElement.ActualWidth
                                    : 0;
        }

        /// <summary>
        /// Gets the effective height.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns>The effective height.</returns>
        public static double GetEffectiveHeight(this FrameworkElement visualElement)
        {
            return !double.IsNaN(visualElement.Height) && visualElement.Height > 0
                                ? visualElement.Height
                                : !double.IsNaN(visualElement.ActualHeight)
                                    ? visualElement.ActualHeight
                                    : 0;
        }

        /// <summary>
        /// Determines whether the specified element is visible to the user.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="container">The container.</param>
        /// <returns>True if visible, otherwise false.</returns>
        public static bool IsUserVisible(this FrameworkElement element, FrameworkElement container)
        {
            if (!element.IsVisible)
            {
                return false;
            }

            var bounds = element.TransformToAncestor(container).TransformBounds(new Rect(0.0, 0.0, element.ActualWidth, element.ActualHeight));
            var rect = new Rect(0.0, 0.0, container.ActualWidth, container.ActualHeight);
            return rect.IntersectsWith(bounds);
        }

        /// <summary>
        /// Removes from parent.
        /// </summary>
        public static void RemoveFromParent(this FrameworkElement element)
        {
            var panel = element.Parent as Panel;
            panel?.Children.Remove(element);
        }


        /// <summary>
        /// Shows the dialog and maintain topmost.
        /// </summary>
        /// <param name="openFileDialog">The open file dialog.</param>
        /// <param name="parentWindow">The parent window.</param>
        /// <returns>The dialog result.</returns>
        public static bool? ShowDialogAndMaintainTopmost(this OpenFileDialog openFileDialog, Window parentWindow)
        {
            // a little hack to keep the folderBrowser dialog from pushing this window behind the main cortex window.
            parentWindow.Topmost = false;
            var result = openFileDialog.ShowDialog();
            parentWindow.Topmost = true;
            parentWindow.Focus();
            return result;
        }

        /// <summary>
        /// Shows the dialog and maintain topmost.
        /// </summary>
        /// <param name="saveFileDialog">The save file dialog.</param>
        /// <param name="parentWindow">The parent window.</param>
        /// <returns>The dialog result.</returns>
        public static bool? ShowDialogAndMaintainTopmost(this SaveFileDialog saveFileDialog, Window parentWindow)
        {
            parentWindow.Topmost = false;
            var result = saveFileDialog.ShowDialog();
            parentWindow.Topmost = true;
            parentWindow.Focus();
            return result;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Tries to scroll the item into the center of the view.
        /// </summary>
        /// <param name="itemsControl">The items control.</param>
        /// <param name="item">The item.</param>
        /// <returns>True if successful, otherwise false.</returns>
        private static bool TryScrollToCenterOfView(this ItemsControl itemsControl, object item)
        {
            // Find the container
            var container = itemsControl.ItemContainerGenerator.ContainerFromItem(item) as UIElement;
            if (container == null)
            {
                return false;
            }

            // Find the ScrollContentPresenter
            ScrollContentPresenter presenter = null;
            for (Visual visual = container; visual != null && visual != itemsControl; visual = VisualTreeHelper.GetParent(visual) as Visual)
            {
                presenter = visual as ScrollContentPresenter;
                if (presenter != null)
                {
                    break;
                }
            }

            if (presenter == null)
            {
                return false;
            }

            // Find the IScrollInfo
            var scrollInfo = !presenter.CanContentScroll
                                 ? presenter
                                 : presenter.Content as IScrollInfo ?? FirstVisualChild(presenter.Content as ItemsPresenter) as IScrollInfo ?? presenter;

            // Compute the center point if the container relative to the scroll info
            var size = container.RenderSize;
            var center = container.TransformToAncestor((Visual)scrollInfo).Transform(new Point(size.Width / 2, size.Height / 2));
            center.Y += scrollInfo.VerticalOffset;
            center.X += scrollInfo.HorizontalOffset;

            // Adjust for logical scrolling
            if (scrollInfo is StackPanel || scrollInfo is VirtualizingStackPanel)
            {
                var logicalCenter = itemsControl.ItemContainerGenerator.IndexFromContainer(container) + 0.5;
                var orientation = scrollInfo is StackPanel ? ((StackPanel)scrollInfo).Orientation : ((VirtualizingStackPanel)scrollInfo).Orientation;
                if (orientation == Orientation.Horizontal)
                {
                    center.X = logicalCenter;
                    if (scrollInfo.CanHorizontallyScroll)
                    {
                        scrollInfo.SetHorizontalOffset(CenteringOffset(center.X, scrollInfo.ViewportWidth, scrollInfo.ExtentWidth));
                    }
                }
                else
                {
                    center.Y = logicalCenter;
                    if (scrollInfo.CanVerticallyScroll)
                    {
                        var centeringOffset = CenteringOffset(center.Y, scrollInfo.ViewportHeight, scrollInfo.ExtentHeight);
                        scrollInfo.SetVerticalOffset(centeringOffset);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the centering offset.
        /// </summary>
        /// <param name="center">The center.</param>
        /// <param name="viewport">The viewport.</param>
        /// <param name="extent">The extent.</param>
        /// <returns>The centering offset.</returns>
        private static double CenteringOffset(double center, double viewport, double extent)
        {
            return Math.Min(extent - viewport, Math.Max(0, center - (viewport / 2)));
        }

        /// <summary>
        /// Gets the first visual child.
        /// </summary>
        /// <param name="visual">The visual.</param>
        /// <returns>The first visual child.</returns>
        private static DependencyObject FirstVisualChild(Visual visual)
        {
            if (visual == null)
            {
                return null;
            }

            if (VisualTreeHelper.GetChildrenCount(visual) == 0)
            {
                return null;
            }

            return VisualTreeHelper.GetChild(visual, 0);
        }

        #endregion
    }
}
