﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMultiSelectCollectionView.cs" company="Mti Film, LLC">
// Copyright (c) 2011 by MTI Film, All Rights Reserved
// </copyright>
// <summary>
//   The multi select collection view interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.Windows.Controls.Primitives;

    /// <summary>
    /// The multi select collection view interface.
    /// </summary>
    public interface IMultiSelectCollectionView
    {
        /// <summary>
        /// Adds the control.
        /// </summary>
        /// <param name="selector">The selector.</param>
        void AddControl(Selector selector);

        /// <summary>
        /// Removes the control.
        /// </summary>
        /// <param name="selector">The selector.</param>
        void RemoveControl(Selector selector);
    }
}
