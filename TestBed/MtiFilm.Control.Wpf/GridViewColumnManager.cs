﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GridViewColumnManager.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>rancor\pfirth</author>
// <date>10/16/2012 1:23:06 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Class that manages a grid view column.
    /// </summary>
    public class GridViewColumnManager
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// Determines if a grid view column is visible.
        /// </summary>
        public static readonly DependencyProperty IsVisibleProperty = DependencyProperty.RegisterAttached(
            "IsVisible", typeof(bool), typeof(GridViewColumnManager), new UIPropertyMetadata(true, OnIsVisibleChanged));

        /// <summary>
        /// The grid view column's min width property.
        /// </summary>
        public static readonly DependencyProperty MinWidthProperty = DependencyProperty.RegisterAttached(
            "MinWidth", typeof(int), typeof(GridViewColumnManager), new UIPropertyMetadata(0, OnMinWidthChanged));

        /// <summary>
        /// Keeps track of the original column widths.
        /// </summary>
        private static readonly Dictionary<GridViewColumn, double> OriginalColumnWidths = new Dictionary<GridViewColumn, double>();

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Gets the is visible property.
        /// </summary>
        /// <param name="dependencyObject">The dependency object.</param>
        /// <returns>The is visible property.</returns>
        public static bool GetIsVisible(DependencyObject dependencyObject)
        {
            return (bool)dependencyObject.GetValue(IsVisibleProperty);
        }

        /// <summary>
        /// Sets the is visible property.
        /// </summary>
        /// <param name="dependencyObject">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetIsVisible(DependencyObject dependencyObject, bool value)
        {
            dependencyObject.SetValue(IsVisibleProperty, value);
        }

        /// <summary>
        /// Gets the min width property.
        /// </summary>
        /// <param name="dependencyObject">The dependency object.</param>
        /// <returns>The min width property.</returns>
        public static int GetMinWidth(DependencyObject dependencyObject)
        {
            return (int)dependencyObject.GetValue(MinWidthProperty);
        }

        /// <summary>
        /// Sets the min width property.
        /// </summary>
        /// <param name="dependencyObject">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetMinWidth(DependencyObject dependencyObject, int value)
        {
            dependencyObject.SetValue(MinWidthProperty, value);
        }

        /// <summary>
        /// Clears the width of the grid view column original.
        /// </summary>
        /// <param name="gridViewColumn">The grid view column.</param>
        public static void ClearGridViewColumnOriginalWidth(GridViewColumn gridViewColumn)
        {
            OriginalColumnWidths.Remove(gridViewColumn);
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Called when min width property is changed.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnMinWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var gridViewColumn = d as GridViewColumn;
            if (gridViewColumn == null)
            {
                return;
            }

            ((INotifyPropertyChanged)gridViewColumn).PropertyChanged += HandleGridViewColumnOnPropertyChanged;
        }

        /// <summary>
        /// Handles the grid view columns on property changed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="propertyChangedEventArgs">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void HandleGridViewColumnOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName != "Width")
            {
                return;
            }

            var gridViewColumn = sender as GridViewColumn;
            if (gridViewColumn == null)
            {
                return;
            }

            var minWidth = GetMinWidth(gridViewColumn);
            var isVisible = GetIsVisible(gridViewColumn);
            if (gridViewColumn.Width < minWidth && isVisible)
            {
                // Since we know we are changing the column width, unsubscribe so we don't get called back again.
                // It shouldn't make any different because we won't get in here, but good practice.
                ((INotifyPropertyChanged)gridViewColumn).PropertyChanged -= HandleGridViewColumnOnPropertyChanged;
                gridViewColumn.Width = minWidth;
                ((INotifyPropertyChanged)gridViewColumn).PropertyChanged += HandleGridViewColumnOnPropertyChanged;
            }
        }

        /// <summary>
        /// Called when is visible property is changed.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnIsVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var gridViewColumn = d as GridViewColumn;
            if (gridViewColumn == null)
            {
                return;
            }

            if (!GetIsVisible(gridViewColumn))
            {
                OriginalColumnWidths[gridViewColumn] = gridViewColumn.Width;
                gridViewColumn.Width = 0;
                return;
            }

            const double Epsilon = 0.0001;
            if (Math.Abs(gridViewColumn.Width - 0) < Epsilon)
            {
                if (!OriginalColumnWidths.ContainsKey(gridViewColumn))
                {
                    gridViewColumn.Width = GetDefaultColumnWidth(gridViewColumn);
                }
                else
                {
                    gridViewColumn.Width = OriginalColumnWidths[gridViewColumn] > GetMinWidth(gridViewColumn)
                        ? OriginalColumnWidths[gridViewColumn]
                        : GetDefaultColumnWidth(gridViewColumn);
                }
            }
        }

        /// <summary>
        /// Gets the default width of the column.
        /// </summary>
        /// <param name="gridViewColumn">The grid view column.</param>
        /// <returns>The default column width.</returns>
        private static double GetDefaultColumnWidth(GridViewColumn gridViewColumn)
        {
            if (gridViewColumn.Header.ToString() == "Cam__Sc__Tk")
            {
                return 140;
            }

            if (gridViewColumn.Header.ToString() == "Pic Path")
            {
                return 200;
            }

            if (gridViewColumn.Header.ToString() == "Aud Path")
            {
                return 200;
            }

            if (gridViewColumn.Header.ToString() == "Video")
            {
                return 200;
            }

            if (gridViewColumn.Header.ToString() == "SR")
            {
                return 50;
            }

            if (gridViewColumn.Header.ToString() == "Audio")
            {
                return 120;
            }

            if (gridViewColumn.Header.ToString() == "Start")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "End")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Duration")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "In")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Out")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "In-Out")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Record In")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Record Out")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Tapename")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Aud Start")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Aud End")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Aud In")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Aud Out")
            {
                return 90;
            }

            if (gridViewColumn.Header.ToString() == "Reels")
            {
                return 140;
            }

            if (gridViewColumn.Header.ToString() == "File Type")
            {
                return 75;
            }

            if (gridViewColumn.Header.ToString() == "Codec")
            {
                return 75;
            }

            if (gridViewColumn.Header.ToString() == "Resolution")
            {
                return 100;
            }

            if (gridViewColumn.Header.ToString() == "Audio Channels")
            {
                return 100;
            }

            if (gridViewColumn.Header.ToString() == "Framerate")
            {
                return 75;
            }

            return 0;
        }

        #endregion
    }
}
