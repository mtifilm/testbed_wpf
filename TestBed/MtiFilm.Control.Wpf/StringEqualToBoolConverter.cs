﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringEqualToBoolConverter.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>turing\mti</author>
// <date>2/9/2012 4:45:08 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Windows.Data;

    /// <summary>
    /// This takes the parameter string and sees if it matches the bound string.  
    /// If the value is set true, the string is set otherwise the unset string is returned
    /// </summary>
    ////    <ValueConverter1:StringEqualToBoolConverter x:Key="BoldConverter" CheckedString="Bold" UncheckedString="None" />
    ////    
    public class StringEqualToBoolConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the StringEqualToBoolConverter class
        /// </summary>
        public StringEqualToBoolConverter()
        {
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the checked string.
        /// </summary>
        /// <value>The checked string.</value>
        public string CheckedString { get; set; }

        /// <summary>
        /// Gets or sets the unchecked string.
        /// </summary>
        /// <value>The unchecked string.</value>
        public string UncheckedString { get; set; }
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return false;
            }

            return value.ToString() == this.CheckedString;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return this.UncheckedString;
            }

            if (value is bool)
            {
                return (bool)value ? this.CheckedString : this.UncheckedString;
            }

            return this.UncheckedString;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion

        #region IValueConverter Members

        #endregion
    }
}
