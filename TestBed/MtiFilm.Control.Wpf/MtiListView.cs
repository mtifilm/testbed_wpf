﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MtiListView.cs" company="MTI Film LLC">
// Copyright (c) 2016 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>5/24/2016 10:34:04 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Class that defines the MTI list view.
    /// </summary>
    public class MtiListView : ListView
    {
        /////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors
        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Creates and returns a new <see cref="T:System.Windows.Controls.ListViewItem" /> container.
        /// </summary>
        /// <returns>
        /// A new <see cref="T:System.Windows.Controls.ListViewItem" /> control.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new MtiListViewItem();
        }

        #endregion
    }
}
