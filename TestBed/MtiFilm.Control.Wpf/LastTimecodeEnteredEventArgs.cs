﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LastTimecodeEnteredEventArgs.cs" company="MTI Film LLC">
// Copyright (c) 2016 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>5/18/2016 2:46:10 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;

    /// <summary>
    /// Class that defines the last timecode entered event args.
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    public class LastTimecodeEnteredEventArgs : EventArgs
    {
        /////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LastTimecodeEnteredEventArgs"/> class.
        /// </summary>
        /// <param name="enterPressed">if set to <c>true</c> enter pressed.</param>
        public LastTimecodeEnteredEventArgs(bool enterPressed)
        {
            this.EnterPressed = enterPressed;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets a value indicating whether or not enter pressed.
        /// </summary>
        public bool EnterPressed { get; private set; }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
