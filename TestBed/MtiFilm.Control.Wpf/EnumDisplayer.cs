﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnumDisplayer.cs" company="MTI Film">
//   Copyright (c) 2011 All Rights Reserved by MTI Film
// </copyright>
// <summary>
//   Defines the EnumDisplayer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows.Automation.Provider;

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Globalization;
    using System.Reflection;
    using System.Windows.Data;
    using MtiSimple.Core;

    ////  If AudioMixerType is an enum, define the Converter as
    ////         <mtiControl:EnumDisplayer Type="{x:Type MtiFilmCore:AudioMixerType}" x:Key="AudioMixerConverter" />
    ////
    ////  To use in a ComboBox and bind the selected to a AudioMixerType property called MixerType
    ////         <ComboBox ItemsSource="{Binding Source={StaticResource AudioMixerConverter}, Path=KeyValueList}"
    ////         SelectedValue="{Binding MixerType}" SelectedValuePath="Key" DisplayMemberPath="Value" />
    //// 
     
    /// <summary>
    /// Defines the enum displayer value converter.
    /// </summary>
    public class EnumDisplayer : IValueConverter
    {
        /// <summary>
        /// The enum type.
        /// </summary>
        private Type type;

        /// <summary>
        /// The values to display.
        /// </summary>
        private IDictionary displayValues;

        /// <summary>
        /// The reverse of the display values.
        /// </summary>
        private IDictionary reverseValues;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumDisplayer"/> class.
        /// </summary>
        public EnumDisplayer()
        {
            this.SortDisplayValues = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumDisplayer"/> class.
        /// </summary>
        /// <param name="type">The type of the enum.</param>
        public EnumDisplayer(Type type)
        {
            this.Type = type;
            this.SortDisplayValues = false;
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public Type Type
        {
            get 
            { 
                return this.type; 
            }

            set
            {
                if (!value.IsEnum)
                {
                    throw new ArgumentException(@"parameter is not an Enumermated type", "value");
                }

                this.type = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [sort display values].
        /// </summary>
        public bool SortDisplayValues { get; set; }

        /// <summary>
        /// Gets the display names.
        /// </summary>
        public ReadOnlyCollection<string> DisplayNames
        {
            get
            {
                var displayValuesType = typeof(Dictionary<,>).GetGenericTypeDefinition().MakeGenericType(typeof(string), this.type);
                this.reverseValues = (IDictionary)Activator.CreateInstance(displayValuesType);
                this.displayValues = (IDictionary)Activator.CreateInstance(typeof(Dictionary<,>).GetGenericTypeDefinition().MakeGenericType(this.type, typeof(string)));

                var fields = this.type.GetFields(BindingFlags.Public | BindingFlags.Static);
                foreach (var field in fields)
                {
                    var a = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    var i = (IgnoreAttribute[])field.GetCustomAttributes(typeof(IgnoreAttribute), false);

                    if (GetIgnoreValue(i))
                    {
                        continue;
                    }

                    var enumValue = field.GetValue(null);
                    var displayString = GetDisplayStringValue(a);
                    if (string.IsNullOrWhiteSpace(displayString))
                    {
                        displayString = ((Enum)enumValue).ToString();
                    }

                    this.displayValues.Add(enumValue, displayString);
                    this.reverseValues.Add(displayString, enumValue);
                }

                var displayValueList = new List<string>((IEnumerable<string>)this.displayValues.Values);
                if (this.SortDisplayValues)
                {
                    displayValueList.Sort();
                }

                return displayValueList.AsReadOnly();
            }
        }

        /// <summary>
        /// Gets the key value list.
        /// </summary>
        /// <value>The key value list.</value>
        public IDictionary KeyValueList
        {
            get
            {
                var displayValuesType = typeof(Dictionary<,>).GetGenericTypeDefinition().MakeGenericType(typeof(string), this.type);
                this.reverseValues = (IDictionary)Activator.CreateInstance(displayValuesType);
                this.displayValues = (IDictionary)Activator.CreateInstance(typeof(Dictionary<,>).GetGenericTypeDefinition().MakeGenericType(this.type, typeof(string)));

                var fields = this.type.GetFields(BindingFlags.Public | BindingFlags.Static);
                foreach (var field in fields)
                {
                    var a = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    var i = (IgnoreAttribute[])field.GetCustomAttributes(typeof(IgnoreAttribute), false);

                    if (!GetIgnoreValue(i))
                    {
                        var enumValue = field.GetValue(null);
                        var displayString = GetDisplayStringValue(a);
                        if (string.IsNullOrWhiteSpace(displayString))
                        {
                            displayString = ((Enum)enumValue).ToString();
                        }

                        this.displayValues.Add(enumValue, displayString);
                        this.reverseValues.Add(displayString, enumValue);
                    }
                }

                return this.displayValues;
            }
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return string.Empty;
            }

            return this.displayValues == null ? string.Empty : this.displayValues[value];
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            return this.reverseValues[value];
        }

        /// <summary>
        /// Gets the display string value.
        /// </summary>
        /// <param name="a">The description attributes.</param>
        /// <returns>The display string.</returns>
        private static string GetDisplayStringValue(DescriptionAttribute[] a)
        {
            if (a == null || a.Length == 0)
            {
                return null;
            }

            DescriptionAttribute da = a[0];
            return da.Description;
        }

        /// <summary>
        /// Gets the ignore value.
        /// </summary>
        /// <param name="i">The ignore attributes.</param>
        /// <returns>True if we should ignore value, false otherwise.</returns>
        private static bool GetIgnoreValue(IgnoreAttribute[] i)
        {
            if (i == null || i.Length == 0)
            {
                return false;
            }

            IgnoreAttribute ia = i[0];
            return ia.Ignore;
        }
    }
}
