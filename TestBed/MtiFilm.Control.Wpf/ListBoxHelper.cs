﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListBoxHelper.cs" company="MTI Film LLC">
// Copyright (c) 2014 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>5/20/2014 11:41:26 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.Collections;
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Static class that defines list box helper properties.
    /// </summary>
    public static class ListBoxHelper
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The selected items property
        /// </summary>
        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.RegisterAttached(
            "SelectedItems",
            typeof(IList),
            typeof(ListBoxHelper),
            new FrameworkPropertyMetadata(null, OnSelectedItemsChanged));

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    

        #region Private fields

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////

        #region Constructors

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////

        #region Properties

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////

        #region Public methods

        /// <summary>
        /// Gets the selected items.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <returns>The selected items.</returns>
        public static IList GetSelectedItems(DependencyObject d)
        {
            return (IList)d.GetValue(SelectedItemsProperty);
        }

        /// <summary>
        /// Sets the selected items.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="value">The value.</param>
        public static void SetSelectedItems(DependencyObject d, IList value)
        {
            d.SetValue(SelectedItemsProperty, value);
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////

        #region Private methods

        /// <summary>
        /// Called when the selected items property is changed.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnSelectedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var listBox = (ListBox)d;
            ResetSelectedItems(listBox);
            listBox.SelectionChanged += delegate { ResetSelectedItems(listBox); };
        }

        /// <summary>
        /// Resets the selected items.
        /// </summary>
        /// <param name="listBox">The list box.</param>
        private static void ResetSelectedItems(ListBox listBox)
        {
            var selectedItems = GetSelectedItems(listBox);
            if (selectedItems == null)
            {
                return;    
            }

            selectedItems.Clear();
            if (listBox.SelectedItems != null)
            {
                foreach (var selectedItem in listBox.SelectedItems)
                {
                    selectedItems.Add(selectedItem);
                }
            }
        }

        #endregion
    }
}
