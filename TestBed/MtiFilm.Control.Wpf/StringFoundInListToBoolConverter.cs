﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringFoundInListToBoolConverter.cs" company="MTI Film, LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>10/31/2012 10:21:12 PM</date>
// <summary>Checks a list of strings to see if value is in list.  Returns "true" if it is.</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Linq;
    using System.Windows.Data;

    /// <summary>
    /// Sees if a string matches a list of strings
    /// </summary>
    public class StringFoundInListToBoolConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the item list.
        /// </summary>
        /// <value>The item list.</value>
        public string ItemList { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to invert the result (return the NOT of the result).
        /// </summary>
        public bool InvertResult { get; set; }

        #endregion

        //////////////////////////////////////////////////
        // IValueConverter Members
        //////////////////////////////////////////////////
        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (this.ItemList == null || value == null)
            {
                return false;
            }

            var itemList = this.ItemList.Split(',');

            var contains = itemList.Contains(value.ToString());

            if (this.InvertResult)
            {
                return !contains;
            }

            return contains;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
