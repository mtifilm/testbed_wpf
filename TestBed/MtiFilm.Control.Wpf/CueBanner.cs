﻿// <copyright file="CueBanner.cs" company="MTI Film">
// Copyright (c) 2011 by MTI Film, All Rights Reserved
// </copyright>
// <author>BlissDev2\Mertus</author>
// <date>11/8/2011 2:29:07 PM</date>
// <summary>Implements the CueBanner attatched property class, StyleCop safe</summary>

//// This class was influenced by a web article by Jason Kemp. although none of the actual code is his
////
//// To use, add the namespace to the xamal namespace definitions
////    xmlns:m="clr-namespace:MtiFilm.Control.Wpf;assembly=MtiFilm.Control.Wpf
//// Then the cuebanner can be added as follows
////            <TextBox m:CueBanner.Content="This is a cue banner"></TextBox>
//// 
//// The Content can be complex, as showed below
////            <TextBox >
////               <m:CueBanner.Content>
////                   <StackPanel Orientation="Horizontal">
////                       <Image Source="/CohogGui;component/Resources/Image/RightArrow.png" HorizontalAlignment="Left" VerticalAlignment="Stretch" Opacity="0.70">
////                       </Image>
////                       <TextBlock Foreground="Gray" VerticalAlignment="Top" FontSize="14" Margin="0,4,0,0">Enter some data you idiot</TextBlock>
////                       <TextBlock Text={Binding Banner, NotifyOnTargetUpdated=True} />
////                   </StackPanel>
////               </m:CueBanner.Content>
////           </TextBox>
////
////  Notice if you use a binding to the cue banner, you must use the NotifyOnTargetUpdated=True property of the binding
//// 
////  Finally there are more properties for CueBanner, Opacity, HoriziontalAlignment, Foreground, and FontStyle
////  However, execpt for Opacity, these are only used when the content is a simple string

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Dynamic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Media;

    /// <summary>
    /// This creates a cue banner for wpf controls
    /// </summary>
    public static class CueBanner
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////

        #region Const and readonly fields

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    

        #region Private fields

        /// <summary>
        /// The Content property
        /// </summary>
        public static readonly DependencyProperty ContentProperty = DependencyProperty.RegisterAttached(
            "Content",
            typeof(object),
            typeof(CueBanner),
            new FrameworkPropertyMetadata(string.Empty, ContentPropertyChanged));

        /// <summary>
        /// The Opacity property
        /// </summary>
        public static readonly DependencyProperty OpacityProperty = DependencyProperty.RegisterAttached(
            "Opacity",
            typeof(double),
            typeof(CueBanner),
            new FrameworkPropertyMetadata(0.75, ContentPropertyChanged));

        /// <summary>
        /// The Cue Horiziontal Alignment property
        /// </summary>
        public static readonly DependencyProperty HorizontalAlignmentProperty = DependencyProperty.RegisterAttached(
            "HorizontalAlignment",
            typeof(HorizontalAlignment),
            typeof(CueBanner),
            new FrameworkPropertyMetadata(HorizontalAlignment.Left, ContentPropertyChanged));

        /// <summary>
        /// The cue font style property
        /// </summary>
        public static readonly DependencyProperty FontStyleProperty = DependencyProperty.RegisterAttached(
            "FontStyle",
            typeof(FontStyle),
            typeof(CueBanner),
            new FrameworkPropertyMetadata(FontStyles.Italic, ContentPropertyChanged));

        /// <summary>
        /// The cue foreground property
        /// </summary>
        public static readonly DependencyProperty ForeGroundProperty = DependencyProperty.RegisterAttached(
            "ForeGround",
            typeof(Brush),
            typeof(CueBanner),
            new FrameworkPropertyMetadata(Brushes.Gray, ContentPropertyChanged));

        /// <summary>
        /// Storage for the object to controls
        /// </summary>
        private static readonly Dictionary<object, ItemsControl> ItemsControls = new Dictionary<object, ItemsControl>();

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////

        #region Constructors

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////

        #region Properties

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////

        #region Public methods

        /// <summary>
        /// Gets the cue opacity.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>The cue opacity</returns>
        public static double GetOpacity(Control control)
        {
            return (double)control.GetValue(OpacityProperty);
        }

        /// <summary>
        /// Sets the cue opacity.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="value">The opacity value.</param>
        public static void SetOpacity(Control control, object value)
        {
            control.SetValue(OpacityProperty, value);
        }

        /// <summary>
        /// Sets the cue horizontal alignment.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="value">The cue text alignment value.</param>
        public static void SetHorizontalAlignment(Control control, object value)
        {
            control.SetValue(HorizontalAlignmentProperty, value);
        }

        /// <summary>
        /// Gets the cue horizontal alignment.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>The cue text alignment</returns>
        public static HorizontalAlignment GetHorizontalAlignment(Control control)
        {
            return (HorizontalAlignment)control.GetValue(HorizontalAlignmentProperty);
        }

        /// <summary>
        /// Gets the cue Content.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>The cue object</returns>
        public static object GetContent(Control control)
        {
            return control.GetValue(ContentProperty);
        }

        /// <summary>
        /// Sets the cue Content.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="value">The value.</param>
        public static void SetContent(Control control, object value)
        {
            control.SetValue(ForeGroundProperty, value);
        }

        /// <summary>
        /// Sets the cue foreground.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="value">The value.</param>
        public static void SetForeground(Control control, object value)
        {
            control.SetValue(ContentProperty, value);
        }

        /// <summary>
        /// Gets the cue foreground.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>The foreground brush</returns>
        public static Brush GetForeground(Control control)
        {
            return (Brush)control.GetValue(ForeGroundProperty);
        }

        /// <summary>
        /// Gets the cue font style (Italic, normal, oblique).
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>The cue font style</returns>
        public static FontStyle GetFontStyle(Control control)
        {
            return (FontStyle)control.GetValue(FontStyleProperty);
        }

        /// <summary>
        /// Sets the cue font style (Italic, normal, oblique)
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="value">The value.</param>
        public static void SetFontStyle(Control control, object value)
        {
            control.SetValue(FontStyleProperty, value);
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////

        #region Private methods

        /// <summary>
        /// Cues the banner property changed.
        /// </summary>
        /// <param name="dependencyObject">The d.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ContentPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var control = (Control)dependencyObject;
            control.Loaded -= ControlLoaded;
            control.Loaded += ControlLoaded;

            if (dependencyObject is TextBox)
            {
                control.GotFocus -= ControlGotFocus;
                control.LostFocus -= ControlLoaded;

                control.GotFocus += ControlGotFocus;
                control.LostFocus += ControlLoaded;

                control.IsVisibleChanged += ControlIsVisibleChanged;
                (dependencyObject as TextBox).TextChanged -= TextHasChanged;
                (dependencyObject as TextBox).TextChanged += TextHasChanged;
            }
            else if (dependencyObject is ComboBox)
            {
                control.GotFocus -= ControlGotFocus;
                control.LostFocus -= ControlLoaded;

                control.GotFocus += ControlGotFocus;
                control.LostFocus += ControlLoaded;
            }
            else if (dependencyObject is ItemsControl)
            {
                control.GotFocus -= ControlGotFocus;
                control.LostFocus -= ControlLoaded;

                control.GotFocus += ControlGotFocus;
                control.LostFocus += ControlLoaded;

                control.IsEnabledChanged += ControlIsEnabledChanged;
                var itemControl = (ItemsControl)dependencyObject;

                // See if we had already process a content
                if (!ItemsControls.ContainsKey(itemControl.ItemContainerGenerator))
                {
                    // for Items property
                    itemControl.ItemContainerGenerator.ItemsChanged += ItemsChanged;
                    ItemsControls.Add(itemControl.ItemContainerGenerator, itemControl);

                    // for ItemsSource property
                    var prop = DependencyPropertyDescriptor.FromProperty(
                        ItemsControl.ItemsSourceProperty, itemControl.GetType());
                    prop.AddValueChanged(itemControl, ItemsSourceChanged);
                }

                var sp = e.NewValue as StackPanel;
                if (sp != null)
                {
                    foreach (var c in sp.Children)
                    {
                        var textBlock = c as TextBlock;
                        if (textBlock != null)
                        {
                            var b = new Binding();
                            b.Source = textBlock;
                            b.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                            b.Path = new PropertyPath("text");
                            control.SetBinding(TextBox.TextProperty, b);
                        }
                    }
                }
            }

            // Force the change in the banner.
            if (ShouldShowBanner(control))
            {
                RemoveBanner(control);
                ShowBanner(control);
            }
        }

        /// <summary>
        /// Controls the is visible changed.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ControlIsVisibleChanged(object dp, DependencyPropertyChangedEventArgs e)
        {
            var textControl = dp as TextBox;
            if (textControl != null)
            {
                // If we startup with text, the control should be removed
                if (textControl.Text != string.Empty)
                {
                    RemoveBanner(textControl);
                    return;
                }
            }

            var comboControl = dp as ComboBox;
            if (comboControl != null)
            {
                // If we startup with text, the control should be removed
                if (comboControl.Text != string.Empty)
                {
                    RemoveBanner(textControl);
                    return;
                }
            }

            var control = textControl ?? dp as Control;
            if (control != null)
            {
                if ((bool)e.NewValue)
                {
                    ShowBanner(control);
                }
                else
                {
                    RemoveBanner(control);
                }
            }
        }

        /// <summary>
        /// Itemses the source changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private static void ItemsSourceChanged(object sender, EventArgs e)
        {
            var c = (ItemsControl)sender;

            if (c.ItemsSource != null)
            {
                var i = c.ItemsSource.Cast<object>().Count();
                if (i > 0)
                {
                    RemoveBanner(c);
                }
                else
                {
                    ////             ShowBanner(c);
                }
            }
            else
            {
                ShowBanner(c);
            }
        }

        /// <summary>
        /// Itemses the changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.Primitives.ItemsChangedEventArgs"/> instance containing the event data.</param>
        private static void ItemsChanged(object sender, ItemsChangedEventArgs e)
        {
            ItemsControl control;
            if (!ItemsControls.TryGetValue(sender, out control))
            {
                return;
            }

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                RemoveBanner(control);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                // only process listboxes, this is wrong but there appears no way to
                // find the count
                var listBox = control as ListBox;
                if (listBox == null)
                {
                    RemoveBanner(control);
                    return;
                }

                if (listBox.Items.Count > 0)
                {
                    RemoveBanner(control);
                }
                else
                {
                    ShowBanner(control);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                ShowBanner(control);
            }
        }

        /// <summary>
        /// Handles the GotFocus event of the control control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void ControlGotFocus(object sender, RoutedEventArgs e)
        {
            var c = (Control)sender;

            // Remove any banner that is shown
            if (ShouldShowBanner(c))
            {
                RemoveBanner(c);
            }
        }

        /// <summary>
        /// Controls the is enabled changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void ControlIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var c = (Control)sender;

            // Remove any banner that is shown
            if (ShouldShowBanner(c))
            {
                ShowBanner(c);
            }
            else
            {
                RemoveBanner(c);
            }
        }

        /// <summary>
        /// Handles the Text change event of the control control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void TextHasChanged(object sender, RoutedEventArgs e)
        {
            var control = (TextBox)sender;
            var c1 = control.Parent as UIElement;

            if (!string.IsNullOrEmpty(control.Text))
            {
                RemoveBanner(control);
            }
            else if (ShouldShowBanner(control))
            {
                ShowBanner(control);
            }
            else
            {
                RemoveBanner(control);
            }
        }

        /// <summary>
        /// Controls the loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void ControlLoaded(object sender, RoutedEventArgs e)
        {
            var control = (Control)sender;

            if (control.IsFocused)
            {
                return;
            }

            if (ShouldShowBanner(control))
            {
                ShowBanner(control);
            }
        }

        /// <summary>
        /// Removes the cue banner.
        /// </summary>
        /// <param name="control">The control.</param>
        private static void RemoveBanner(UIElement control)
        {
            var layer = AdornerLayer.GetAdornerLayer(control);

            // Control layer may not have been created 
            if (layer == null)
            {
                return;
            }

            var adorners = layer.GetAdorners(control);
            if (adorners == null)
            {
                return;
            }

            foreach (var adorner in adorners)
            {
                if (adorner is BannerAdorner)
                {
                    adorner.Visibility = Visibility.Hidden;
                    layer.Remove(adorner);
                }
            }
        }

        /// <summary>
        /// Shows the cue banner.
        /// </summary>
        /// <param name="control">The control.</param>
        private static void ShowBanner(Control control)
        {
            var layer = AdornerLayer.GetAdornerLayer(control);

            // Control layer may not have been created 
            if (layer == null)
            {
                return;
            }

            // Remove any cue banner that existed before
            RemoveBanner(control);

            // Don't show it if we are not visible
            if (!control.IsVisible)
            {
                return;
            }

            layer.Add(new BannerAdorner(control, GetContent(control)));
        }

        /// <summary>
        /// Should we show show cue banner.
        /// </summary>
        /// <param name="control">The control</param>
        /// <returns>True if the banner should be shown</returns>
        private static bool ShouldShowBanner(Control control)
        {
            // ugly check if listbox
            if (control is ListBox)
            {
                return !(control as ListBox).HasItems;
            }

            // Check if text based
            var dp = GetDependencyProperty(control);
            return dp == null || control.GetValue(dp).Equals(string.Empty);
        }

        /// <summary>
        /// Gets the dependency property.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>The property type of a text box</returns>
        private static DependencyProperty GetDependencyProperty(Control control)
        {
            if (control is ComboBox)
            {
                return ComboBox.TextProperty;
            }

            if (control is TextBoxBase)
            {
                return TextBox.TextProperty;
            }

            return null;
        }

        #endregion

        /// <summary>
        /// The adorner class for the Cue banner
        /// </summary>
        internal class BannerAdorner : Adorner
        {
            /// <summary>
            /// Backing store for the content presenter
            /// </summary>
            private readonly ContentPresenter contentPresenter;

            /// <summary>
            /// Initializes a new instance of the <see cref="BannerAdorner"/> class.
            /// </summary>
            /// <param name="adornedElement">The adorned element.</param>
            /// <param name="cueBanner">The cue banner.</param>
            public BannerAdorner(UIElement adornedElement, object cueBanner)
                : base(adornedElement)
            {
                this.IsHitTestVisible = false;
                
                // Kluge to fix adorner being shown.  TODO: fix correctly
                var opacity = GetOpacity(this.Control) * (this.Control.IsEnabled ? 1 : 0.0);

                // If we have a simple string, Create an adorner that matches font et all
                if (cueBanner is string)
                {
                    dynamic c = this.Control is TextBox ? (dynamic)(this.Control as TextBox) : this.Control as ComboBox;
                    c = c ?? this.Control as ListBox;
                    if (c != null)
                    {
                        var cueBannerString = cueBanner as string;
                        var typeface = new Typeface(c.FontFamily, GetFontStyle(this.Control), c.FontWeight, c.FontStretch);
                        var size = MeasureTextWidth(typeface, c.FontSize, cueBannerString);

                        // Create the banner to display
                        var textBlock = new TextBlock
                            {
                                Text = cueBannerString,
                                FontSize = c.FontSize,
                                Foreground = GetForeground(this.Control),
                                FontStyle = GetFontStyle(this.Control)
                            };

                        // Set the adorner content presenter
                        // TODO: set HorziontalAlignment correctly for combo box.  
                        this.contentPresenter = new ContentPresenter
                            {
                                Content = textBlock,
                                Opacity = opacity,
                                SnapsToDevicePixels = true,
                                HorizontalAlignment = GetHorizontalAlignment(this.Control),
                                Margin = new Thickness(4, size.Baseline - size.Height, 0, 0)
                            };
                    }

                    return;
                }

                // Now we are a complex content, so just let WPF display it
                // The user controls everything about the display
                var frameworkElement = cueBanner as FrameworkElement;
                if ((frameworkElement != null) && frameworkElement.DataContext == null)
                {
                    frameworkElement.DataContext = this.Control.DataContext;
                    frameworkElement.TargetUpdated += new EventHandler<DataTransferEventArgs>(this.BannerTargetUpdated);
                }

                this.contentPresenter = new ContentPresenter
                    {
                        Content = cueBanner,
                        Opacity = opacity,
                        SnapsToDevicePixels = true,
                        Margin = new Thickness(0)
                    };
            }

            //////////////////////////////////////////////////
            // Public Properties, all static before non-static
            //////////////////////////////////////////////////

            #region Properties

            /// <summary>
            /// Gets the number of visual child elements within this element.
            /// </summary>
            /// <returns>The number of visual child elements for this element.</returns>
            protected override int VisualChildrenCount
            {
                get
                {
                    return 1;
                }
            }

            /// <summary>
            /// Gets the control.
            /// </summary>
            private Control Control
            {
                get
                {
                    return (Control)this.AdornedElement;
                }
            }

            #endregion

            //////////////////////////////////////////////////
            // Public methods, all static before non-static
            //////////////////////////////////////////////////

            #region Public methods

            /// <summary>
            /// Overrides <see cref="M:System.Windows.Media.Visual.GetVisualChild(System.Int32)"/>, and returns a child at the specified index from a collection of child elements.
            /// </summary>
            /// <param name="index">The zero-based index of the requested child element in the collection.</param>
            /// <returns>The requested child element. This should not return null; if the provided index is out of range, an exception is thrown.</returns>
            protected override Visual GetVisualChild(int index)
            {
                return this.contentPresenter;
            }

            /// <summary>
            /// Implements any custom measuring behavior for the adorner.
            /// </summary>
            /// <param name="constraint">A size to constrain the adorner to.</param>
            /// <returns>A <see cref="T:System.Windows.Size"/> object representing the amount of layout space needed by the adorner.</returns>
            protected override Size MeasureOverride(Size constraint)
            {
                this.contentPresenter.Measure(this.Control.RenderSize);
                return this.Control.RenderSize;
            }

            /// <summary>
            /// When overridden in a derived class, positions child elements and determines a size for a <see cref="T:System.Windows.FrameworkElement"/> derived class.
            /// </summary>
            /// <param name="finalSize">The final area within the parent that this element should use to arrange itself and its children.</param>
            /// <returns>The actual size used.</returns>
            protected override Size ArrangeOverride(Size finalSize)
            {
                this.contentPresenter.Arrange(new Rect(finalSize));
                return finalSize;
            }

            /// <summary>
            /// Measures the width of the text.
            /// </summary>
            /// <param name="typeface">The typeface.</param>
            /// <param name="charSize">Size of the em.</param>
            /// <param name="text">The text to measure</param>
            /// <returns>The size in current units</returns>
            private static dynamic MeasureTextWidth(Typeface typeface, double charSize, string text)
            {
                GlyphTypeface glyphTypeface;
                typeface.TryGetGlyphTypeface(out glyphTypeface);

                dynamic result = new ExpandoObject();

                // if string is empty, return a space value
                if (string.IsNullOrEmpty(text))
                {
                    text = " ";
                }

                // Measure it
                result.Width = text.Select(ch => glyphTypeface.CharacterToGlyphMap[ch]).Select(glyph => glyphTypeface.AdvanceWidths[glyph]).Sum() * charSize;
                result.Height =
                    text.Select(ch => glyphTypeface.CharacterToGlyphMap[ch]).Select(
                        glyph => glyphTypeface.AdvanceHeights[glyph]).Max() * charSize;

                result.Baseline = glyphTypeface.Baseline * charSize;

                return result;
            }

            /// <summary>
            /// Banners the target updated.
            /// </summary>
            /// <param name="sender">The sender.</param>
            /// <param name="e">The <see cref="System.Windows.Data.DataTransferEventArgs"/> instance containing the event data.</param>
            private void BannerTargetUpdated(object sender, DataTransferEventArgs e)
            {
                // Sometimes the adorner banner is not yet created
                var layer = AdornerLayer.GetAdornerLayer(this);
                if (layer != null)
                {
                    layer.Update();
                }
            }
            #endregion
        }
    }
}