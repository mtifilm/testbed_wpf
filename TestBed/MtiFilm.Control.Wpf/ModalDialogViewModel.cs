﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModalDialogViewModel.cs" company="MTI Film LLC">
// Copyright (c) 2013 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>rancor\pfirth</author>
// <date>1/15/2013 4:39:00 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

using MtiSimple.Core;

namespace MtiFilm.Control.Wpf
{
    using System.Threading;

    /// <summary>
    /// Class that defines a modal dialog viewmodel.
    /// </summary>
    public class ModalDialogViewModel : BindableObject
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// The current item
        /// </summary>
        private int currentItem;

        /// <summary>
        /// The items count
        /// </summary>
        private int itemsCount;

        /// <summary>
        /// The operation
        /// </summary>
        private string operation;

        /// <summary>
        /// The show progress flag.
        /// </summary>
        private bool showProgress;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ModalDialogViewModel"/> class.
        /// </summary>
        public ModalDialogViewModel()
        {
            this.ShowProgress = true;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether or not to show progress.
        /// </summary>
        public bool ShowProgress
        {
            get
            {
                return this.showProgress;
            }

            set
            {
                if (value == this.showProgress)
                {
                    return;
                }

                this.showProgress = value;
                this.RaisePropertyChanged("ShowProgress");
            }
        }

        /// <summary>
        /// Gets or sets the current item.
        /// </summary>
        public int CurrentItem
        {
            get
            {
                return this.currentItem;
            }

            set
            {
                if (value == this.currentItem)
                {
                    return;
                }

                this.currentItem = value;
                this.RaisePropertyChanged("CurrentItem");
            }
        }

        /// <summary>
        /// Gets or sets the items count.
        /// </summary>
        public int ItemsCount
        {
            get
            {
                return this.itemsCount;
            }

            set
            {
                if (value == this.itemsCount)
                {
                    return;
                }

                this.itemsCount = value;
                this.RaisePropertyChanged("ItemsCount");
            }
        }

        /// <summary>
        /// Gets or sets the operation.
        /// </summary>
        public string Operation
        {
            get
            {
                return this.operation;
            }

            set
            {
                if (value == this.operation)
                {
                    return;
                }

                this.operation = value;
                this.RaisePropertyChanged("Operation");
            }
        }

        /// <summary>
        /// Gets or sets the cancellation token source.
        /// </summary>
        public CancellationTokenSource CancellationTokenSource
        {
            get
            {
                return this.GetCurrentNullableValue<CancellationTokenSource>();
            }

            set
            {
                this.SetAndRaiseOnChange(value);
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Reports the progress.
        /// </summary>
        /// <param name="localCurrentItem">The local current item.</param>
        public void ReportProgress(int localCurrentItem)
        {
            this.CurrentItem = localCurrentItem;
        }

        /// <summary>
        /// Updates the items count.
        /// </summary>
        /// <param name="localItemsCount">The local items count.</param>
        public void UpdateItemsCount(int localItemsCount)
        {
            this.ItemsCount = localItemsCount;
            if (this.ItemsCount == 0)
            {
                this.ShowProgress = false;
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
