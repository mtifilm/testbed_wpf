﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ButtonsProperties.cs" company="MTI Film LLC">
// Copyright (c) 2017 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\hans.lehmann</author>
// <date>4/12/2017 5:16:24 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf.NumericUpDownTextBox
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Button properties for NumericUpDownTextBox class
    /// </summary>
    public class ButtonsProperties
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The is pressed default
        /// </summary>
        private readonly Brush isPressedDefault = new SolidColorBrush(Colors.DarkMagenta);

        /// <summary>
        /// The is mouse over default
        /// </summary>
        private readonly Brush isMouseOverDefault = new SolidColorBrush(Colors.Gray);

        /// <summary>
        /// The corner radius default
        /// </summary>
        private readonly CornerRadius cornerRadiusDefault = new CornerRadius(3);

        /// <summary>
        /// The parent
        /// </summary>
        private readonly NumericUpDownTextBox parent;

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Wpf.NumericUpDownTextBox.ButtonsProperties"/> class.
        /// </summary>
        /// <param name="textBox">The text box.</param>
        public ButtonsProperties(NumericUpDownTextBox textBox)
        {
            this.parent = textBox;
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        
        /// <summary>
        /// Gets the background.
        /// </summary>
        public Brush Background
        {
            get
            {
                return this.parent.ButtonBackground ?? this.parent.Background;
            }
        }

        /// <summary>
        /// Gets the foreground.
        /// </summary>
        public Brush Foreground
        {
            get
            {
                return this.parent.ButtonForeground ?? this.parent.Foreground;
            }
        }

        /// <summary>
        /// Gets the border brush.
        /// </summary>
        public Brush BorderBrush
        {
            get
            {
                return this.parent.ButtonBorderBrush ?? this.parent.BorderBrush;
            }
        }

        /// <summary>
        /// Gets the border thickness.
        /// </summary>
        public Thickness BorderThickness
        {
            get
            {
                return this.parent.ButtonBorderThickness ?? this.parent.BorderThickness;
            }
        }

        /// <summary>
        /// Gets the corner radius.
        /// </summary>
        public CornerRadius CornerRadius
        {
            get
            {
                return this.parent.ButtonCornerRadius ?? this.cornerRadiusDefault;
            }
        }

        /// <summary>
        /// Gets the is pressed background.
        /// </summary>
        public Brush IsPressedBackground
        {
            get
            {
                return this.parent.ButtonPressedBackground ?? this.isPressedDefault;
            }
        }

        /// <summary>
        /// Gets the is mouse over background.
        /// </summary>
        public Brush IsMouseOverBackground
        {
            get
            {
                return this.parent.ButtonMouseOverBackground ?? this.isMouseOverDefault;
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        
        /// <summary>
        /// Notifies the property changed.
        /// </summary>
        /// <param name="info">The information.</param>
        internal void NotifyPropertyChanged(string info)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
