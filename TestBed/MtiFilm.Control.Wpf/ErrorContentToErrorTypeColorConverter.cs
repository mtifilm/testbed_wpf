﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ErrorContentToErrorTypeColorConverter.cs" company="MTI Film, LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>11/13/2012 2:45:24 PM</date>
// <summary>
// Splits ErrorContent into Type and Message and returns Type as a color (ErrorColor for Error, WarningColor for Warning).
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Splits ErrorContent into Type and Message and returns Type as a color (ErrorColor for Error, WarningColor for Warning).
    /// Default is ErrorColor if no Type found.
    /// </summary>
    [ValueConversion(typeof(string), typeof(SolidColorBrush))]
    public class ErrorContentToErrorTypeColorConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ErrorContentToErrorTypeColorConverter class.
        /// </summary>
        public ErrorContentToErrorTypeColorConverter()
        {
            // Set default colors - can be overridden in XAML.
            this.WarningColor = Brushes.Yellow;
            this.ErrorColor = Brushes.Red;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the warning color.
        /// </summary>
        /// <value>The true value.</value>
        public SolidColorBrush WarningColor { get; set; }

        /// <summary>
        /// Gets or sets the error color.
        /// </summary>
        public SolidColorBrush ErrorColor { get; set; }

        #endregion

        //////////////////////////////////////////////////
        // IValueConverter Members
        //////////////////////////////////////////////////

        #region IValueConverter Members

        /// <summary>
        /// Converts the specified value into the appropriate color.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>The appropriate color.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return this.ErrorColor;
            }

            // If value doesn't contain the ":" separator, then return ErrorColor.
            if (!value.ToString().Contains(":"))
            {
                return this.ErrorColor;
            }

            // Return the appropriate color - everything but "WARNING" gets ErrorColor.
            var type = value.ToString().Split(':')[0].ToUpper();
            return type == "WARNING" ? this.WarningColor : this.ErrorColor;
        }

        /// <summary>
        /// Converts back.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>An Exception.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}