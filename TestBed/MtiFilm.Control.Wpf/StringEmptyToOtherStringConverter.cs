﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringEmptyToOtherStringConverter.cs" company="MTI Film, LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>10/15/2012 11:46:32 AM</date>
// <summary>
// Compares passed string to string.Empty.  If empty, returns StringIfEmpty, else returns StringIfNotEmpty.
// Useful for ToolTips based on another value.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Compares passed string to string.Empty.  If empty, returns StringIfEmpty, else returns StringIfNotEmpty.  Useful for ToolTips based on another value.
    /// </summary>
    /// <example>
    /// <![CDATA[<ValueConverter1:StringEmptyToOtherStringConverter x:Key="Prefix" TrueValue="Add Prefix" FalseValue="No Prefix Defined" />]]>
    /// </example>
    public class StringEmptyToOtherStringConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StringEmptyToOtherStringConverter"/> class.
        /// </summary>
        public StringEmptyToOtherStringConverter()
        {
            this.StringIfEmpty = "Empty";
            this.StringIfNotEmpty = "Not Empty";
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties
        //////////////////////////////////////////////////
        #region Public Properties

        /// <summary>
        /// Gets or sets the string if empty.
        /// </summary>
        /// <value>
        /// The string if empty.
        /// </value>
        public string StringIfEmpty { get; set; }

        /// <summary>
        /// Gets or sets the string if not empty.
        /// </summary>
        /// <value>The string if not empty.</value>
        public string StringIfNotEmpty { get; set; }

        #endregion

        //////////////////////////////////////////////////
        // IValueConverter Members
        //////////////////////////////////////////////////
        #region IValueConverter Members

        /// <summary>
        /// Converts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>The appropriate string.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return this.StringIfEmpty;
            }

            if (value.ToString() == string.Empty)
            {
                return this.StringIfEmpty;
            }

            return this.StringIfNotEmpty;
        }

        /// <summary>
        /// Converts the back.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="parameter">The parameter.</param>
        /// <param name="culture">The culture.</param>
        /// <returns>An Exception.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
