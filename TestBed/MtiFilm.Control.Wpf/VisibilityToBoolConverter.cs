﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VisibilityToBoolConverter.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>pure07\david.mcclure</author>
// <date>8/9/2012 4:36:00 PM</date>
// <summary>Converts a visibility to a boolean</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// A visibility to bool converter
    /// </summary>
    /// <remarks>
    /// Define a converter resouce by specifying the ReturnTrueIfValue
    /// See the example.
    /// The legal values are the values of Visibility, Visible, Hidden, Collapsed
    /// </remarks>
    /// <example>
    /// VisibilityToBoolConverter x:Key="VisibleToTrueConverter" ReturnTrueIfValue="Visible"
    /// </example>
    [ValueConversion(typeof(Visibility), typeof(bool))]
    public sealed class VisibilityToBoolConverter : IValueConverter
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the VisibilityToBoolConverter class
        /// </summary>
        public VisibilityToBoolConverter()
        {
            // set defaults  
            this.ReturnTrueIfValue = Visibility.Visible;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the true value.
        /// </summary>
        /// <value>The true value.</value>
        public Visibility ReturnTrueIfValue { get; set; }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>A converted value. If the method returns null, the valid null value is used.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (object.Equals(value, this.ReturnTrueIfValue))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>A converted value. If the method returns null, the valid null value is used.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
            {
                return (Visibility)this.ReturnTrueIfValue;
            }

            return DependencyProperty.UnsetValue;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
