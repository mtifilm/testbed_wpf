﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DockPanelSplitter.cs" company="MtiFilm">
//   Copyright 2012 by MtiFilm, LLC
// </copyright>
// <summary>
//   DockPanelSplitter Control allows resizing DockPanel.Dock elements
// </summary>
// --------------------------------------------------------------------------------------------------------------------

 /*
 CodePlex project
 http://wpfcontrols.codeplex.com

 DockPanelSplitter is a splitter control for DockPanels.
 Add the DockPanelSplitter after the element you want to resize.
 Set the DockPanel.Dock to define which edge the splitter should work on.

 Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.

 Step 1a) Using this custom control in a XAML file that exists in the current project.
 Add this XmlNamespace attribute to the root element of the markup file where it is 
 to be used:

     xmlns:OsDps="clr-namespace:DockPanelSplitter"


 Step 1b) Using this custom control in a XAML file that exists in a different project.
 Add this XmlNamespace attribute to the root element of the markup file where it is 
 to be used:

     xmlns:MyNamespace="clr-namespace:DockPanelSplitter;assembly=DockPanelSplitter"

 You will also need to add a project reference from the project where the XAML file lives
 to this project and Rebuild to avoid compilation errors:

     Right click on the target project in the Solution Explorer and
     "Add Reference"->"Projects"->[Select this project]


 Step 2)
 Go ahead and use your control in the XAML file.

     <MyNamespace:DockPanelSplitter/>
*/

namespace MtiFilm.Control.Wpf
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// DockPanelSplitter Control allows resizing DockPanel.Dock elements
    /// </summary>
    public class DockPanelSplitter : Control
    {
        /// <summary>
        /// Read only Dependency Property to store the Proportional Resize value
        /// </summary>
        public static readonly DependencyProperty ProportionalResizeProperty = DependencyProperty.Register(
            "ProportionalResize", typeof(bool), typeof(DockPanelSplitter), new UIPropertyMetadata(true));

        /// <summary>
        /// Read only property to store the thickness values
        /// </summary>
        public static readonly DependencyProperty ThicknessProperty = DependencyProperty.Register(
            "Thickness", typeof(double), typeof(DockPanelSplitter), new UIPropertyMetadata(4.0, ThicknessChanged));

        /// <summary>
        /// The starting drag point
        /// </summary>
        private Point startDragPoint;

        /// <summary>
        /// The element to resize
        /// </summary>
        private FrameworkElement element;

        /// <summary>
        /// Desired width of the element, can be less than minwidth
        /// </summary>
        private double width;

        /// <summary>
        /// Current desired height of the element, can be less than minheight
        /// </summary>
        private double height;

        /// <summary>
        /// Current width of parent element, used for proportional resize
        /// </summary>
        private double previousParentWidth;

        /// <summary>
        /// Current height of parent element, used for proportional resize
        /// </summary>
        private double previousParentHeight;

        /// <summary>
        /// Initializes static members of the <see cref="DockPanelSplitter"/> class.
        /// </summary>
        static DockPanelSplitter()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DockPanelSplitter), new FrameworkPropertyMetadata(typeof(DockPanelSplitter)));

            // override the Background property
            BackgroundProperty.OverrideMetadata(typeof(DockPanelSplitter), new FrameworkPropertyMetadata(Brushes.Transparent));

            // override the Dock property to get notifications when Dock is changed
            DockPanel.DockProperty.OverrideMetadata(typeof(DockPanelSplitter), new FrameworkPropertyMetadata(Dock.Left, DockChanged));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanelSplitter"/> class.
        /// </summary>
        public DockPanelSplitter()
        {
            this.Loaded += this.DockPanelSplitterLoaded;
            this.Unloaded += this.DockPanelSplitterUnloaded;

            this.UpdateHeightOrWidth();
        }

        /// <summary>
        /// Gets a value indicating whether the splitter orientation is horizontal
        /// </summary>
        public bool IsHorizontal
        {
            get
            {
                Dock dock = DockPanel.GetDock(this);
                return dock == Dock.Top || dock == Dock.Bottom;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the target element will resize proportionally with the parent container
        /// Set to false if you don't want the element to be resized when the parent is resized.
        /// </summary>
        public bool ProportionalResize
        {
            get
            {
                return (bool)this.GetValue(ProportionalResizeProperty);
            }

            set
            {
                this.SetValue(ProportionalResizeProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the value for the thickness of the splitter
        /// </summary>
        public double Thickness
        {
            get
            {
                return (double)this.GetValue(ThicknessProperty);
            }

            set
            {
                this.SetValue(ThicknessProperty, value);
            }
        }

        /// <summary>
        /// The DockPanelSplitterLoaded event handler
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The <see cref="RoutedEventArgs"/> instance containing the event data.
        /// </param>
        public void DockPanelSplitterLoaded(object sender, RoutedEventArgs e)
        {
            Panel dp = Parent as Panel;
            if (dp == null)
            {
                return;
            }

            // Subscribe to the parent's size changed event
            dp.SizeChanged += this.ParentSizeChanged;

            // Store the current size of the parent DockPanel
            this.previousParentWidth = dp.ActualWidth;
            this.previousParentHeight = dp.ActualHeight;

            // Find the target element
            this.UpdateTargetElement();
        }

        /// <summary>
        /// The DockPanelSplitterUnloaded event handler
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The <see cref="RoutedEventArgs"/> instance containing the event data.
        /// </param>
        public void DockPanelSplitterUnloaded(object sender, RoutedEventArgs e)
        {
            Panel dp = Parent as Panel;
            if (dp == null)
            {
                return;
            }

            // Unsubscribe
            dp.SizeChanged -= this.ParentSizeChanged;
        }

        /// <summary>
        /// Adjusts the Width of the Target Element
        /// </summary>
        /// <param name="dx">
        /// The desired change in width
        /// </param>
        /// <param name="dock">
        /// The docking position
        /// </param>
        /// <returns>
        /// The actual change in width
        /// </returns>
        public double AdjustWidth(double dx, Dock dock)
        {
            if (dock == Dock.Right)
            {
                dx = -dx;
            }

            this.width += dx;
            this.SetTargetWidth(this.width);

            return dx;
        }

        /// <summary>
        /// Adjusts the Height of the Target Element
        /// </summary>
        /// <param name="dy">
        /// The desired change in height
        /// </param>
        /// <param name="dock">
        /// The docking position
        /// </param>
        /// <returns>
        /// The actual change in height
        /// </returns>
        public double AdjustHeight(double dy, Dock dock)
        {
            if (dock == Dock.Bottom)
            {
                dy = -dy;
            }

            this.height += dy;
            this.SetTargetHeight(this.height);

            return dy;
        }

        #region Protected fields

        /// <summary>
        /// The OnMouseEnter event handler
        /// </summary>
        /// <param name="e">
        /// The <see cref="MouseEventArgs"/> instance containing the event data.
        /// </param>
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            if (!this.IsEnabled)
            {
                return;
            }

            this.Cursor = this.IsHorizontal ? Cursors.SizeNS : Cursors.SizeWE;
        }

        /// <summary>
        /// The OnMouseDown event handler
        /// </summary>
        /// <param name="e">
        /// The <see cref="MouseEventArgs"/> instance containing the event data.
        /// </param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (!this.IsEnabled)
            {
                return;
            }

            if (!this.IsMouseCaptured)
            {
                this.startDragPoint = e.GetPosition(Parent as IInputElement);
                this.UpdateTargetElement();
                if (this.element != null)
                {
                    this.width = this.element.ActualWidth;
                    this.height = this.element.ActualHeight;
                    this.CaptureMouse();
                }
            }

            base.OnMouseDown(e);
        }

        /// <summary>
        /// The OnMouseMove event handler
        /// </summary>
        /// <param name="e">
        /// The <see cref="MouseEventArgs"/> instance containing the event data.
        /// </param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (this.IsMouseCaptured)
            {
                Point currentPoint = e.GetPosition(Parent as IInputElement);
                Point delta = new Point(currentPoint.X - this.startDragPoint.X, currentPoint.Y - this.startDragPoint.Y);
                Dock dock = DockPanel.GetDock(this);

                if (this.IsHorizontal)
                {
                    delta.Y = this.AdjustHeight(delta.Y, dock);
                }
                else
                {
                    delta.X = this.AdjustWidth(delta.X, dock);
                }

                bool isBottomOrRight = dock == Dock.Right || dock == Dock.Bottom;

                // When docked to the bottom or right, the position has changed after adjusting the size
                if (isBottomOrRight)
                {
                    this.startDragPoint = e.GetPosition(Parent as IInputElement);
                }
                else
                {
                    this.startDragPoint = new Point(this.startDragPoint.X + delta.X, this.startDragPoint.Y + delta.Y);
                }
            }

            base.OnMouseMove(e);
        }

        /// <summary>
        /// The OnMouseUp event handler
        /// </summary>
        /// <param name="e">
        /// The <see cref="MouseEventArgs"/> instance containing the event data.
        /// </param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (this.IsMouseCaptured)
            {
                this.ReleaseMouseCapture();
            }

            base.OnMouseUp(e);
        }

        #endregion Protected fields

        #region Private fields

        /// <summary>
        /// The DockChanged event handler.
        /// </summary>
        /// <param name="d">
        /// The dependency object.
        /// </param>
        /// <param name="e">
        /// The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.
        /// </param>
        private static void DockChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((DockPanelSplitter)d).UpdateHeightOrWidth();
        }

        /// <summary>
        /// The ThicknessChanged event handler
        /// </summary>
        /// <param name="d">
        /// The dependency object.
        /// </param>
        /// <param name="e">
        /// The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.
        /// </param>
        private static void ThicknessChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((DockPanelSplitter)d).UpdateHeightOrWidth();
        }

        /// <summary>
        /// Updates the thickness of the splitter
        /// </summary>
        private void UpdateHeightOrWidth()
        {
            if (this.IsHorizontal)
            {
                this.Height = this.Thickness;
                this.Width = double.NaN;
            }
            else
            {
                this.Width = this.Thickness;
                this.Height = double.NaN;
            }
        }

        /// <summary>
        /// Update the target element (the element the DockPanelSplitter works on)
        /// </summary>
        private void UpdateTargetElement()
        {
            Panel dp = Parent as Panel;
            if (dp == null)
            {
                return;
            }

            int i = dp.Children.IndexOf(this);

            // The splitter cannot be the first child of the parent DockPanel
            // The splitter works on the 'older' sibling 
            if (i > 0 && dp.Children.Count > 0)
            {
                this.element = dp.Children[i - 1] as FrameworkElement;
            }
        }

        /// <summary>
        /// Sets the Target Elements Minimum Width
        /// </summary>
        /// <param name="newWidth">
        /// The minimum width for the target element
        /// </param>
        private void SetTargetWidth(double newWidth)
        {
            if (newWidth < this.element.MinWidth)
            {
                newWidth = this.element.MinWidth;
            }

            if (newWidth > this.element.MaxWidth)
            {
                newWidth = this.element.MaxWidth;
            }

            // todo - constrain the width of the element to the available client area
            Panel dp = this.Parent as Panel;
            Dock dock = DockPanel.GetDock(this);
            MatrixTransform t = this.element.TransformToAncestor(dp) as MatrixTransform;
            if (dock == Dock.Left && newWidth > dp.ActualWidth - t.Matrix.OffsetX - this.Thickness)
            {
                newWidth = dp.ActualWidth - t.Matrix.OffsetX - this.Thickness;
            }

            this.element.Width = newWidth;
        }

        /// <summary>
        /// Sets the Target Elements Minimum Height
        /// </summary>
        /// <param name="newHeight">
        /// The minimum height for the target element
        /// </param>
        private void SetTargetHeight(double newHeight)
        {
            if (newHeight < this.element.MinHeight)
            {
                newHeight = this.element.MinHeight;
            }

            if (newHeight > this.element.MaxHeight)
            {
                newHeight = this.element.MaxHeight;
            }

            // todo - constrain the height of the element to the available client area
            Panel dp = Parent as Panel;
            Dock dock = DockPanel.GetDock(this);
            MatrixTransform t = this.element.TransformToAncestor(dp) as MatrixTransform;
            if (dock == Dock.Top && newHeight > dp.ActualHeight - t.Matrix.OffsetY - this.Thickness)
            {
                newHeight = dp.ActualHeight - t.Matrix.OffsetY - this.Thickness;
            }

            this.element.Height = newHeight;
        }

        /// <summary>
        /// The ParentSizeChanged event handler
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The <see cref="SizeChangedEventArgs"/> instance containing the event data.
        /// </param>
        private void ParentSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!this.ProportionalResize)
            {
                return;
            }

            DockPanel dp = Parent as DockPanel;
            if (dp == null)
            {
                return;
            }

            double sx = dp.ActualWidth / this.previousParentWidth;
            double sy = dp.ActualHeight / this.previousParentHeight;

            if (!double.IsInfinity(sx))
            {
                this.SetTargetWidth(this.element.Width * sx);
            }

            if (!double.IsInfinity(sy))
            {
                this.SetTargetHeight(this.element.Height * sy);
            }

            this.previousParentWidth = dp.ActualWidth;
            this.previousParentHeight = dp.ActualHeight;
        }

        #endregion
    }
}
