﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetControlInfo.cs" company="MTI Film, LLC">
// Copyright (c) 2011 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>horta\russell</author>
// <date>11/29/2011 6:03:03 PM</date>
// <summary>Get various Control Information</summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Get various Control Information.
    /// </summary>
    public static class GetControlInfo
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields
        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Gets the control's position.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>A Point representing the location of the control relative to its window.</returns>
        public static Point GetControlPosition(Control control)
        {
            var locationToScreen = control.PointToScreen(new Point(0, 0));
            var source = PresentationSource.FromVisual(control);

            if (source != null && source.CompositionTarget != null)
            {
                return source.CompositionTarget.TransformFromDevice.Transform(locationToScreen);
            }
            else
            {
                return new Point(0, 0);
            }
        }

        /// <summary>
        /// Sets the size of the dialog's Position and Size based on a passed in UserControl with Button at the bottom.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="userControlBasis">The user control basis.</param>
        /// <param name="control">The control basis.</param>
        public static void SetDialogPositionAndSize(Window window, Control userControlBasis, Control control = null)
        {
            var relativePoint = GetControlPosition(userControlBasis);

            window.Top = relativePoint.Y;
            window.Left = relativePoint.X;
            window.Width = userControlBasis.ActualWidth;
            window.Height = userControlBasis.ActualHeight;

            if (control != null)
            {
                var controlSpace = control.ActualHeight + control.Margin.Top + control.Margin.Bottom;
                window.Height += controlSpace;
            }
        }

        /// <summary>
        /// Sets the size of the dialog position and.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="userControlBasis">The user control basis.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public static void SetDialogPositionAndSize(Window window, Control userControlBasis, double width, double height)
        {
            var relativePoint = GetControlPosition(userControlBasis);

            window.Top = relativePoint.Y;
            window.Left = relativePoint.X;
            window.Width = width;
            window.Height = height;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods
        #endregion
    }
}
